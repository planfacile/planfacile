#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#standard

#option{fr}
#option{utf8}

#option{auteur}
#define{auteur}{Pappy}

#option{date}
#define{date}{Dimanche 4 juin 2006}

#options{Type de sortie}
#case{LaTeX}
	#define{PlanFacile}{#{\sc @PROJECT_NAME@#}}
	#option{titre}
	#define{titre}{Définitions standard de #PlanFacile}
	#define{LaTeX}{\LaTeX }
#case{HTML}
	#define{PlanFacile}{<strong>@PROJECT_NAME@</strong>}
	#option{titre}
	#define{titre}{Définitions standard de @PROJECT_NAME@}
	#define{LaTeX}{LaTeX}
#end

#idea{definitionsstandard}Définitions standard#text
#end

#idea{objectifs}Objectifs#text
L'objectif principal des définitions standard#dep{10}{definitionsstandard}{#depref}{}# est de simplifier l'utilisation de #PlanFacile . En effet, il est
fréquent de vouloir produire un document lisible par un formateur de texte répandu #apposition{comme #LaTeX#espace ou un navigateur web}. Dans ce cas, il serait
pénible de devoir préciser comment générer le document de sortie, et ce pour chaque nouveau code source. Les définitions standard, en fournissant un ensemble de
commandes cohérent, permet donc de débarrasser le rédacteur de ces considérations techniques.
#§
Un autre objectif est de permettre à un seul et même code source de pouvoir être produit sous plusieurs formats de sortie, et ce sans aucune modification, en
apportant des commandes donnant un aspect générique aux codes sources.
#end

#idea{utilisation}Utilisation#text#dep{objectifs}{}{}
L'utilisation des définitions standard#dep{10}{definitionsstandard}{#depref}{}# est extrêmement simple : il suffit d'ajouter la commande
#verbatim{|}{##standard}# au code source, et ce de préférence au début. A cette commande, il suffira d'ajouter différentes commandes pour préciser à
#PlanFacile# quelles sont les définitions qu'il doit utiliser, parmi toutes celles qui sont présentes.
#end

#idea{support}Ce qui est supporté par les définitions standard#text
Les définitions standard#dep{10}{definitionsstandard}{#depref}{}# supportent un certain nombre de choses dé#cesure cor#cesure ré#cesure lées qui pourtant se
doivent de fonctionner ensemble.
#end

#idea{langues}Langues supportées#text
Pouvant supporter#dep{10}{support}{#depref}{}# plusieurs langues, les définitions standard#dep{definitionsstandard}{#depref}{}# sont sensibles à la langue dans
laquelle elles sont utilisées.
#§
Actuellement, seul le français est véritablement supporté, mais d'autres langues telles que l'anglais et le japonais devraient faire leur prochaine apparition.
#end

#idea{charset}Jeux de caractères supportés#text
Le support#dep{10}{support}{#depref}{}# des jeux de caractères, même relatif à la langue#dep{5}{langues}{#depref}{}# utilisée, est réalisé.
#§
D'une manière assez générale, les jeux de caractères actuellement supportés sont l'UTF-8, l'ISO-8859 et l'ASCII.
#end

#idea{formatsortie}Formats de sortie#text#dep{charset}{}{}#comment{Permet de ne pas séparer langues et charsets << dépendance purement artificielle}
Bien sûr, comme annoncé dans les objectifs#dep{1}{objectifs}{#depref}{}, le support#dep{10}{support}{#depref}{}# de plusieurs formats de sortie est présent dans
les définitions standard#dep{5}{definitionsstandard}{#depref}{}.
#§
Pour le moment, seuls le format #LaTeX#espace et le format XHTML 1.0 sont supportés. Dans un futur relativement proche, les formats XML et nroff devraient être
ajoutés.
#end

#idea{francais}Français#text
Lors de la rédaction d'un code source en français, utilisant les définitions standard#dep{1}{definitionsstandard}{#depref}{}, il peut être utile de préciser la
langue#dep{10}{langues}{#depref}{}# en ajoutant l'option #verbatim{|}{##option#{fr#}}.
#§
Si le format de sortie#dep{5}{formatsortie}{#depref}{}# supporte lui aussi plusieurs langues#dep{10}{langues}{}{}, alors l'utilisation des définitions
standard#dep{1}{definitionsstandard}{}{}# permettront de transmettre cette information au formateur de texte. De même, de nombreuses macros porteront un nom
français, pour permettre aux non-anglophones une mémorisation simplifiée de ces macros.
#end

#idea{ascii}ASCII#text
Pour indiquer un code source encodé#dep{10}{charset}{#depref}{}# en ASCII, il faudra définir l'option #verbatim{|}{##option#{ascii#}}.
#end

#idea{utf8}UTF-8#text
Pour indiquer un code source encodé#dep{10}{charset}{#depref}{}# en UTF-8, il faudra définir l'option #verbatim{|}{##option#{utf8#}}.
#end

#idea{iso}ISO#text
Pour indiquer un code source encodé#dep{10}{charset}{#depref}{}# en ISO, il faudra définir l'option #verbatim{|}{##option#{iso#}}. #PlanFacile# saura retrouver,
grâce aux informations de langue#dep{5}{langues}{#depref}{}, de quelle variante d'ISO il s'agit.
#end

#idea{LaTeX}#LaTeX#text
Pour que #PlanFacile# génère#dep{10}{formatsortie}{#depref}{}# du #LaTeX#espace en utilisant#dep{utilisation}{#depref}{}# les définitions
standard#dep{5}{definitionsstandard}{#depref}{}, il suffit simplement d'ajouter l'option #verbatim{|}{##option#{LaTeX#}}.
#end

#idea{LaTeXfr}#LaTeX#espace en français#text
Utilisé en français#dep{5}{francais}{#depref}{}, le format #LaTeX#dep{10}{LaTeX}{#depref}{}# comprend plusieurs options et macros permettant de paramétrer le
document produit par #PlanFacile .
#§
On trouve tout d'abord de quoi préciser le titre du document et son auteur :
#verbatimlong{
##option#{titre#}#verbligne
##define#{titre#}#{titre du document#}#verbligne
#verbligne
##option#{auteur#}#verbligne
##define#{auteur#}#{auteur du document#}
}
Les options permettent de signaler que l'on désire utiliser un titre et/ou un auteur, et les définitions de macros permettent de passer les valeurs
correspondantes aux définitions standard#dep{1}{definitionsstandard}{#depref}{}.
#§
Ensuite, on trouve différentes options :
#description{#items
{#item{la classe de document générée}{#verbatim{|}{##option#{article#}}, #verbatim{|}{##option#{rapport#}}# ou #verbatim{|}{##option#{livre#}}# ;}}
{#item{la taille de la police de caractères}{#verbatim{|}{##option#{10pt#}}, #verbatim{|}{##option#{11pt#}}# ou #verbatim{|}{##option#{12pt#}}# ;}}
{#item{la table des matières}{peut être ajoutée en début de document, en ajoutant l'option #verbatim{|}{##option#{table des matières#}}# ;}}
{#item{la page de garde de base de #LaTeX}{peut être ajoutée grâce à l'option #verbatim{|}{##option#{page de garde#}}# ;}}
{#item{la possibilité d'ajouter des packages #LaTeX}{en définissant l'option #verbatim{|}{##option#{packages personnels#}}# et la macro #verbatim{|}{packages}#
avec le code #LaTeX# nécessaire à l'ajout des packages.}}
}
#end

#idea{HTML}HTML#text
De même que le #LaTeX#dep{3}{LaTeX}{#depref}{}, la génération#dep{10}{formatsortie}{#depref}{}# d'un document en HTML en utilisant#dep{utilisation}{#depref}{}#
les définitions standard#dep{5}{definitionsstandard}{#depref}{}# s'indique grâce à la définition de l'option #verbatim{|}{##option#{HTML#}}.
#end

#idea{HTMLfr}HTML en français#text
En utilisation conjointe avec le français#dep{5}{francais}{#depref}{}, le format HTML#dep{10}{HTML}{#depref}{}# rend disponible quelques options et macros.
#§
On trouve tout d'abord de quoi préciser le titre du document et son auteur, comme ce que l'on trouve en #LaTeX#dep{8}{LaTeXfr}{#depref}{# (Vu plus haut)}# :
#verbatimlong{
##option#{titre#}#verbligne
##define#{titre#}#{titre du document#}#verbligne
#verbligne
##option#{auteur#}#verbligne
##define#{auteur#}#{auteur du document#}
}
#§
De plus, on trouve de quoi définir l'icône et le style de la page HTML :
#verbatimlong{
##option#{icone#}#verbligne
##define#{typeicone#}#{type (extention du fichier)#}#verbligne
##define#{icone#}#{fichier image#}#verbligne
#verbligne
##option#{style#}#verbligne
##define#{style#}#{fichier CSS#}
}
Un exemple de ces éléments sera sans nul doute pour préciser l'utilisation de ces derniers :
#verbatimlong{
##option#{icone#}#verbligne
##define#{typeicone#}#{png#}#verbligne
##define#{icone#}#{favicon#}#verbligne
#verbligne
##option#{style#}#verbligne
##define#{style#}#{style.css#}
}
#end

#idea{macrosoptionsgeneriques}Macros et options génériques#text
Cette section présente un ensemble d'options et de macro présentes#dep{10}{support}{#depref}{}# dans les définitions
standard#dep{1}{definitionsstandard}{#depref}{}# indépendantes du format de sortie#dep{5}{formatsortie}{#depref}{}# choisi.
#end

#idea{macrosoptionsgeneriquesfr}Macros et options génériques en français#text
Comme les formats de sortie#dep{5}{formatsortie}{#depref}{}, les macros et options generiques#dep{10}{macrosoptionsgeneriques}{#depref}{}# sont influencées par
la langue#dep{5}{langues}{#depref}{}# et le jeu de caractères#dep{5}{charset}{#depref}{}# employés.
#end

#idea{ideesgeneriquesfr}Idées génériques#text
Parmi les idées automatiques, les idées génériques sont #apposition{et de loin}# les moins importantes et certaines, voire toutes, peuvent être supprimées sans
dénaturer le document généré. A cette fin, il suffit de définir l'option#dep{1}{macrosoptionsgeneriquesfr}{#depref}{}# #verbatim{|}{##option#{sans générique#}}
# pour supprimer la résolution par défaut des idées génériques, et relancer la compilation avec l'option de ligne de commande #verbatim{|}{-A}. Cette opération
n'empêche en rien la résolution ponctuelle d'une ou plusieurs idées génériques.
#end

#idea{listesfr}Listes#text
Dans le texte d'une idée, il peut être utile de placer des listes. Pour permettre l'introduction de ces listes dans le texte, les définitions
standard#dep{1}{definitionsstandard}{#depref}{}# apportent quelques macros#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# :
#verbatimlong{
##listeapuce#{##items#verbligne
#{item 1#}#verbligne
#{item 2#}#verbligne
...#verbligne
#{item n#}#verbligne
#}#verbligne
#verbligne
##enumeration#{##items#verbligne
#{item 1#}#verbligne
#{item 2#}#verbligne
...#verbligne
#{item n#}#verbligne
#}#verbligne
#verbligne
##description#{##items#verbligne
#{##item#{item 1#}#{texte 1#}#}#verbligne
#{##item#{item 2#}#{texte 2#}#}#verbligne
...#verbligne
#{##item#{item n#}#{texte n#}#}#verbligne
#}
}
où les mots #verbatim{|}{item}# et #verbatim{|}{texte}# suivis d'un numéro peuvent être remplacé par le texte désiré.
#end

#idea{paragraphefr}Changement de paragraphe#text
Pour aérer le texte d'une idée un peu longue, les définitions standard#dep{1}{definitionsstandard}{#depref}{}# proposent la
macro#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# #verbatim{|}{##§}# afin de marquer un changement de paragraphe.
#end

#idea{quotationfr}Discours rapporté#text
Il peut être particulièrement appréciable de transmettre du texte qui ne devra pas être formaté. Dans ce cas, les
macros#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# :
#verbatimlong{
##verbatim#{marqueur de fin#}#{texte verbatim#}#verbligne
##verbatimlong#{texte verbatim##verbligne#verbligne
sur plusieurs##verbligne#verbligne
lignes.#verbligne
#}
}
réalisent cela pour le rédacteur. Le marqueur de fin, un seul caractère de préférence, sert au formateur de texte pour connaître la portée du texte non formaté,
mais peut ne pas être utilisé selon le format de sortie#dep{5}{formatsortie}{#depref}{}.
#§
De même, une citation peut être insérée grâce à la macro#dep{10}{macrosoptionsgeneriquesfr}{}{}# :
#verbatimlong{
##citation#{citation à rapporter#}
}
#end

#idea{cesurefr}Césures#text
Le formateur de texte peut avoir des difficultés à couper certains mots. Dans ce cas, il suffira d'indiquer au formateur où couper les mots en ajoutant la
macro#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# #verbatim{|}{##cesure}# au milieu du mot, comme par exemple le mot :
#verbatimlong{
fi##cesure cel##cesure le
}
#end

#idea{appositionfr}Apposition#text
Pour un texte en apposition, les définitions standard#dep{1}{definitionsstandard}{#depref}{}# prévoient la macro#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}
# :
#verbatimlong{
##apposition#{texte apposé#}
}
#end

#idea{caracteresspeciauxfr}Caractères spéciaux#text
Parfois, certains caractères particuliers nécessitent une écriture particulière dans le code produit pour être correctement affichés. Ceux-ci sont dont présents
sous forme de macros#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# :
#verbatimlong{
##oe#verbligne
##...#verbligne
##espace#verbligne
##dièse#verbligne
##accouvrante#verbligne
##accfermante
}
#end

#idea{anglais}Anglais#text
Lors de la rédaction d'un code source en anglais, utilisant les définitions standard#dep{1}{definitionsstandard}{#depref}{}, il peut être utile de préciser la
langue#dep{10}{langues}{#depref}{}# en ajoutant l'option #verbatim{|}{##option#{en#}}.
#§
Si le format de sortie#dep{5}{formatsortie}{#depref}{}# supporte lui aussi plusieurs langues#dep{10}{langues}{}{}, alors l'utilisation des définitions
standard#dep{1}{definitionsstandard}{}{}# permettront de transmettre cette information au formateur de texte.
#§
Si la langue n'est pas spécifiée, le français#dep{francais}{#depref}{}# est choisie par défaut.
#end

#idea{LaTeXen}#LaTeX#espace en anglais#text#dep{LaTeXfr}{#depref}{}#comment{Fr avant car langue par défaut}
Utilisé en anglais#dep{5}{anglais}{#depref}{}, le format #LaTeX#dep{10}{LaTeX}{#depref}{}# comprend plusieurs options et macros permettant de paramétrer le
document produit par #PlanFacile .
#§
On trouve tout d'abord de quoi préciser le titre du document et son auteur :
#verbatimlong{
##option#{Title#}#verbligne
##define#{Title#}#{titre du document#}#verbligne
#verbligne
##option#{Author#}#verbligne
##define#{Author#}#{auteur du document#}
}
Les options permettent de signaler que l'on désire utiliser un titre et/ou un auteur, et les définitions de macros permettent de passer les valeurs
correspondantes aux définitions standard#dep{1}{definitionsstandard}{#depref}{}.
#§
Ensuite, on trouve différentes options :
#description{#items
{#item{la classe de document générée}{#verbatim{|}{##option#{Article#}}, #verbatim{|}{##option#{Report#}}# ou #verbatim{|}{##option#{Book#}}# ;}}
{#item{la taille de la police de caractères}{#verbatim{|}{##option#{10pt#}}, #verbatim{|}{##option#{11pt#}}# ou #verbatim{|}{##option#{12pt#}}# ;}}
{#item{la table des matières}{peut être ajoutée en début de document, en ajoutant l'option #verbatim{|}{##option#{Table of contents#}}# ;}}
{#item{la page de garde de base de #LaTeX}{peut être ajoutée grâce à l'option #verbatim{|}{##option#{Front page#}}# ;}}
{#item{la possibilité d'ajouter des packages #LaTeX}{en définissant l'option #verbatim{|}{##option#{Personnals packages#}}# et la macro #verbatim{|}{Packages}#
avec le code #LaTeX# nécessaire à l'ajout des packages.}}
}
#end

#idea{HTMLen}HTML en anglais#text#dep{HTMLfr}{#depref}{}#comment{Fr avant car langue par défaut}
En utilisation conjointe avec l'anglais#dep{5}{anglais}{#depref}{}, le format HTML#dep{10}{HTML}{#depref}{}# rend disponible quelques options et macros.
#§
On trouve tout d'abord de quoi préciser le titre du document et son auteur, comme ce que l'on trouve en #LaTeX#dep{8}{LaTeXen}{#depref}{# (Vu plus haut)}# :
#verbatimlong{
##option#{Title#}#verbligne
##define#{Title#}#{titre du document#}#verbligne
#verbligne
##option#{Author#}#verbligne
##define#{Author#}#{auteur du document#}
}
#§
De plus, on trouve de quoi définir l'icône et le style de la page HTML :
#verbatimlong{
##option#{Icon#}#verbligne
##define#{IconType#}#{type (extention du fichier)#}#verbligne
##define#{Icon#}#{fichier image#}#verbligne
#verbligne
##option#{Style#}#verbligne
##define#{Style#}#{fichier CSS#}
}
Un exemple de ces éléments sera sans nul doute pour préciser l'utilisation de ces derniers :
#verbatimlong{
##option#{Icon#}#verbligne
##define#{IconType#}#{png#}#verbligne
##define#{Icon#}#{favicon#}#verbligne
#verbligne
##option#{Style#}#verbligne
##define#{Style#}#{style.css#}
}
#end

#idea{macrosoptionsgeneriquesen}Macros et options génériques en anglais#text#dep{macrosoptionsgeneriquesfr}{}{}#comment{Fr avant car langue par défaut}
Comme les formats de sortie#dep{5}{formatsortie}{#depref}{}, les macros et options generiques#dep{10}{macrosoptionsgeneriques}{#depref}{}# sont influencées par
la langue#dep{5}{langues}{#depref}{}# et le jeu de caractères#dep{5}{charset}{#depref}{}# employés.
#end

#idea{ideesgeneriquesen}Idées génériques#text
Parmi les idées automatiques, les idées génériques sont #apposition{et de loin}# les moins importantes et certaines, voire toutes, peuvent être supprimées sans
dénaturer le document généré. A cette fin, il suffit de définir l'option#dep{1}{macrosoptionsgeneriquesen}{#depref}{}# #verbatim{|}{##option#{without generic#}}
# pour supprimer la résolution par défaut des idées génériques, et relancer la compilation avec l'option de ligne de commande #verbatim{|}{-A}. Cette opération
n'empêche en rien la résolution ponctuelle d'une ou plusieurs idées génériques.
#end

#idea{listesen}Listes#text
Dans le texte d'une idée, il peut être utile de placer des listes. Pour permettre l'introduction de ces listes dans le texte, les définitions
standard#dep{1}{definitionsstandard}{#depref}{}# apportent quelques macros#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# :
#verbatimlong{
##itemize#{##items#verbligne
#{item 1#}#verbligne
#{item 2#}#verbligne
...#verbligne
#{item n#}#verbligne
#}#verbligne
#verbligne
##enumerate#{##items#verbligne
#{item 1#}#verbligne
#{item 2#}#verbligne
...#verbligne
#{item n#}#verbligne
#}#verbligne
#verbligne
##description#{##items#verbligne
#{##item#{item 1#}#{texte 1#}#}#verbligne
#{##item#{item 2#}#{texte 2#}#}#verbligne
...#verbligne
#{##item#{item n#}#{texte n#}#}#verbligne
#}
}
où les mots #verbatim{|}{item}# et #verbatim{|}{texte}# suivis d'un numéro peuvent être remplacé par le texte désiré.
#end

#idea{paragrapheen}Changement de paragraphe#text
Pour aérer le texte d'une idée un peu longue, les définitions standard#dep{1}{definitionsstandard}{#depref}{}# proposent la
macro#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# #verbatim{|}{##§}# afin de marquer un changement de paragraphe.
#end

#idea{quotationen}Discours rapporté#text
Il peut être particulièrement appréciable de transmettre du texte qui ne devra pas être formaté. Dans ce cas, les
macros#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# :
#verbatimlong{
##verbatim#{marqueur de fin#}#{texte verbatim#}#verbligne
##verbatimlong#{texte verbatim##verbline#verbligne
sur plusieurs##verbligne#verbligne
lignes.#verbligne
#}
}
réalisent cela pour le rédacteur. Le marqueur de fin, un seul caractère de préférence, sert au formateur de texte pour connaître la portée du texte non formaté,
mais peut ne pas être utilisé selon le format de sortie#dep{5}{formatsortie}{#depref}{}.
#§
De même, une citation peut être insérée grâce à la macro#dep{10}{macrosoptionsgeneriquesen}{}{}# :
#verbatimlong{
##quotation#{citation à rapporter#}
}
#end

#idea{cesureen}Césures#text
Le formateur de texte peut avoir des difficultés à couper certains mots. Dans ce cas, il suffira d'indiquer au formateur où couper les mots en ajoutant la
macro#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# #verbatim{|}{##hyphenation}# au milieu du mot, comme par exemple le mot :
#verbatimlong{
fi##hyphenation cel##hyphenation le
}
#end

#idea{appositionen}Apposition#text
Pour un texte en apposition, les définitions standard#dep{1}{definitionsstandard}{#depref}{}# prévoient la macro#dep{10}{macrosoptionsgeneriquesen}{#depref}{}
# :
#verbatimlong{
##dashed#{texte apposé#}
}
#end

#idea{caracteresspeciauxen}Caractères spéciaux#text
Parfois, certains caractères particuliers nécessitent une écriture particulière dans le code produit pour être correctement affichés. Ceux-ci sont dont présents
sous forme de macros#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# :
#verbatimlong{
##oe#verbligne
##...#verbligne
##space#verbligne
##sharp#verbligne
##opensoftbracket#verbligne
##closesoftbracket
}
#end

#idea{inclusionimagefr}Inclusion d'images#text
L'inclusion d'une image dans un document se fait par l'intermédiaire de la macro#dep{10}{macrosoptionsgeneriquesfr}{#depref}{}# #verbatim{|}{image}# activée par
l'option #verbatim{|}{inclusion image}. Cette macro demande six paramètres :
#listeapuce{#items
{nom du fichier à inclure ;}
{extension du fichier précisant le type du fichier ;}
{titre de l'image ;}
{description de l'image pour le HTML#dep{5}{HTMLfr}{#depref}{}# ;}
{formattage pour le #LaTeX#dep{5}{LaTeXfr}{#depref}{}# ;}
{label de l'image.}
}
#end

#idea{inclusionimageen}Inclusion d'images#text
L'inclusion d'une image dans un document se fait par l'intermédiaire de la macro#dep{10}{macrosoptionsgeneriquesen}{#depref}{}# #verbatim{|}{Image}# activée par
l'option #verbatim{|}{Image inclusion}. Cette macro demande six paramètres :
#listeapuce{#items
{nom du fichier à inclure ;}
{extension du fichier précisant le type du fichier ;}
{titre de l'image ;}
{description de l'image pour le HTML#dep{5}{HTMLen}{#depref}{}# ;}
{formattage pour le #LaTeX#dep{5}{LaTeXen}{#depref}{}# ;}
{label de l'image.}
}
#end
