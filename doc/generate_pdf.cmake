#
# This file is part of PlanFacile
#
# Copyright (C) 2014  Sylvain Plantefève
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licences/>.

function( GENERATE_PDF WORKING_DIRECTORY PDF_FILE LATEX_SOURCE TYPE )
        string( REPLACE "tex" "aux" LATEX_AUX_FILE ${LATEX_SOURCE} )
        string( REPLACE "tex" "toc" LATEX_TOC_FILE ${LATEX_SOURCE} )
        string( REPLACE "tex" "log" LATEX_LOG_FILE ${LATEX_SOURCE} )
        set( LATEX_AUXILIARY_FILES
                ${LATEX_AUX_FILE}
                ${LATEX_TOC_FILE}
                ${LATEX_LOG_FILE} )
        add_custom_command( OUTPUT ${LATEX_AUXILIARY_FILES}
                DEPENDS ${LATEX_SOURCE}
                WORKING_DIRECTORY ${WORKING_DIRECTORY}
                COMMAND ${PDFLATEX_COMPILER}
                ARGS    ${LATEX_SOURCE}
                COMMENT "Generate ${TYPE} indexes" )
        add_custom_command( OUTPUT ${PDF_FILE}
                DEPENDS ${LATEX_SOURCE} ${LATEX_AUXILIARY_FILES}
                WORKING_DIRECTORY ${WORKING_DIRECTORY}
                COMMAND ${PDFLATEX_COMPILER}
                ARGS    ${LATEX_SOURCE}
                COMMENT "Generate ${TYPE} documentation" )
endfunction()
