#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#standard

#option{fr}
#option{utf8}

#option{titre}
#define{titre}{Introduction à PlanFacile}

#options{Type de fichier}
#case{LaTeX}
	#define{PlanFacile}{#{\sc @PROJECT_NAME@#}}
	#define{LaTeX}{\LaTeX}
	#define{<}{<}
	#define{>}{>}
#case{HTML}
	#define{PlanFacile}{<strong>@PROJECT_NAME@</strong>}
	#define{LaTeX}{LaTeX}
	#define{<}{&lt;}
	#define{>}{&gt;}
#other
	#define{PlanFacile}{@PROJECT_NAME@}
	#define{LaTeX}{LaTeX}
	#define{<}{<}
	#define{>}{>}
#end

#idea{intro}Introduction à #PlanFacile#text
#end

#idea{base}Concepts et commandes de base#text#dep{10}{prerecquis}{}{}
Cette section de cette introduction#dep{1}{intro}{#depref}{}# a pour but
de vous familiariser avec la représentation d'un document utilisée par
#PlanFacile , ainsi que la syntaxe associée.
#§
A la fin de cette section, vous devriez être en mesure d'écrire un
fichier pouvant contenir un document complet. Il ne manquera alors que
très peu de choses à ajouter au code source pour obtenir un document en XHTML
ou en #LaTeX .
#end

#idea{idee}Idée de #PlanFacile#text
Le concept de base#dep{1}{base}{#depref}{}# le plus fondammental est sans conteste l'idée,
véritable composant indispensable à un document.
#§
Une idée de #PlanFacile# est une portion de texte atomique pouvant être
désignée de manière univoque par un titre simple.
#end

#idea{ideeelements}Eléments d'une idée#text
Une idée#dep{1}{idee}{#depref}{}, de manière purement formelle, est un regroupement
d'éléments très simples.
#end

#idea{syntaxeidee}Syntaxe d'une idée#text
La syntaxe d'une idée#dep{1}{idee}{#depref}{}# contient simplement les trois éléments#dep{ideeelements}
{#depref}{}# mentionnées plus haut :
#verbatimlong{
##idea#{Référence#} Titre ##text
Le texte...
##end}
#§
Parmi ces trois éléments, seuls les deux derniers sont donc du texte à entrer au kilomètre. Pour la référence,
il est plutôt recommandé de n'utiliser qu'un seul mot, même s'il doit être long et comportant des majuscules
pour mettre en relief une composition de plusieurs mots de la langue française.
#end

#idea{titre}Titre#text
Déjà, cet élément d'idée#dep{1}{ideeelements}{#depref}{}# est une portion
de texte complètement inerte pour #PlanFacile . Cependant, ce titre se révèle utile au lecteur, car il peut
lui servir de point de repère dans le document, tant au point de vue de la mise en page que de la sémantique.
#end

#idea{texte}Texte#text
Le texte de l'idée est l'élément#dep{1}{ideeelements}{#depref}{}# correspondant à la portion de document
expliquant en détail le concept mis en avant par l'idée. Pour obtenir un document cohérent, il faut avant
tout s'assurer que pour chaque idée de celui-ci, le titre#dep{titre}{#depref}{}# et le texte soient eux-mêmes
cohérents.
#end

#idea{reference}Référence#text
Une référence est un mot#dep{1}{ideeelements}{#depref}{}# permettant d'identifier une idée de manière unique.
Pour être plus précis, cette référence permet à :
#description{#items
{#item{PlanFacile}{de désigner l'idée pour ses traitements internes ;}}
{#item{au formateur de texte}{#apposition{tels que #LaTeX#espace ou un navigateur web}# pour placer correctement,
à priori, les références croisées pouvant être introduites par #PlanFacile# dans le document.}}
}
#§
Une dernière précision : une référence est unique au sein d'un code source, ce qui a une conséquence plutôt simple
à deviner : lorsqu'une idée de référence donnée existe et que l'on définit une nouvelle fois une idée possédant
un telle référence, la seconde définition remplace la première.
#end

#idea{dependance}Dépendances de #PlanFacile#text
Un concept de base#dep{1}{base}{#depref}{}# plutôt essentiel de #PlanFacile# permet de relier les différentes
idées#dep{idee}{#depref}{}# entre elles : il s'agit des dépendances.
Pour pouvoir comprendre l'interêt des dépendances, il faut simplement constater que les idées formant
un document sont reliées entre elles par leur sens, car le contenu de certaines d'entre elles permettent d'expliquer
le contenu d'autres idées de ce même document.
#§
A ce stade, une description précise s'impose. Supposons que l'on a les deux idées « maison en briques » et « brique ».
Dans ce cas, on comprend bien que le contenu de la première idée dépend de la seconde pour pouvoir être comprise. En
effet, si l'on place les deux idées dans l'ordre indiqué ci-dessus, un lecteur ne sachant pas encore ce qu'est vraiment
une brique pourrait être dubitatif quant à l'explication relative à la maison#...#espace Ici, indiquer à #PlanFacile# que
l'idée « maison en briques » dépend de l'idée « brique » lui permettra d'inverser les deux idées, dans la mesure où cela
ne nuit pas à l'organisation globale du document.
#§
Une petite remarque sur le vocabulaire associé aux dépendances dans cette section : par abus de langage, les mots « origine » et
« destination » ont été interchangés par rapport à la véritable documentation. Cela a été réalisé pour simplifier la compréhension
de celui qui découvre le logiciel, mais cet abus sera corrigé au moment opportun en mettant en avant l'approche que suggère la véritable
notation des dépendances.#comment{FIXME Il faudra penser à ajouter cette idée, en parlant des flots de lecture...}
#end

#idea{dependanceelements}Eléments d'une dépendance#text
Une dépendance#dep{1}{dependance}{#depref}{}# est composée de cinq éléments distincts, dont un est implicite, grâce à la
position#dep{positiondependance}{#depref}{}# de la dépendance dans le code. Décrivons alors les quatres autres#...
#end

#idea{referencedestination}Référence de destination#text
Cet élément#dep{1}{dependanceelements}{#depref}{}# est la référence#dep{reference}{#depref}{}# de l'idée dont dépend l'idée
courante.
#end

#idea{positiondependance}Où placer une dépendance ?#text
Une question peut-être inattendue, mais qui admet une réponse à la fois importante et simple : une dépendance#dep{1}
{dependance}{#depref}{}# se place directement dans le texte d'une idée, à savoir entre les commandes #verbatim{|}{##text}# et
#verbatim{|}{##end}# de l'idée origine de la dépendance.
#§
Ceci explique que l'origine de la dépendance puisse alors être implicite, puisqu'elle est déterminée par sa position particulière
dans le code source.
#end

#idea{irreductibledependance}Texte irréductible#text
Cet élément#dep{1}{dependanceelements}{#depref}{}# est le texte par lequel #PlanFacile# doit remplacer la commande de dépendance
dans le texte de l'idée#dep{positiondependance}{#depref}{}. Ce remplacement de texte n'est effectué que dans le cas où l'idée
destination#dep{referencedestination}{#depref}{}# n'a pu être placée avant l'idée courante.
#end

#idea{texteirreductibledependance}Remplacement par du texte#text
Le remplacement de la commande de dépendance#dep{1}{irreductibledependance}{#depref}{}# peut donc consister en un simple texte,
invitant le lecteur à soit admettre le contenu de l'idée de destination de la dépendance, soit de lui indiquer que l'idée se
trouve en aval du document.
#end

#idea{refirreductibledependance}Remplacement par une référence#text
Le remplacement de la commande de dépendance#dep{1}{irreductibledependance}{#depref}{}# par un simple texte invitant à voir plus
loin dans le texte#dep{texteirreductibledependance}{#depref}{}# est rarement satisfaisant. Donc, en adjonction du texte#dep
{texteirreductibledependance}{#depref}{}, il est possible d'ajouter dans ce cas précis une commande qui se substituera en une
référence informant le lecteur de manière précise quel est le paragraphe qui doit être lu afin de faciliter la compréhension.
#end

#idea{reductibledependance}Texte réductible#text
Cet élément#dep{1}{dependanceelements}{#depref}{}# est le texte par lequel #PlanFacile# doit remplacer la commande de dépendance
dans le texte de l'idée#dep{positiondependance}{#depref}{}. Ce remplacement de texte n'est effectué que dans le cas où l'idée
destination#dep{referencedestination}{#depref}{}# a pu être placée avant l'idée courante. Il s'agit là du cas qui est en fait
souhaité, car il permet au lecteur de poursuivre la lecture sans devoir aller chercher l'information plus en aval du texte.
#§
Le remplacement effectué ici est purement textuel, le placement d'une commande indiquant une référence#dep{refirreductibledependance}
{#depref}{}# étant ici interdit. Cela sert à assurer de manière fiable l'amélioration de lecture linéaire du document.
#end

#idea{pertinence}Pertinence d'une dépendance#text
Cet élément#dep{0xCAFE}{dependanceelements}{#depref}{}# est une valeur numérique entière et positive qui nuance la validité des
dépendances. Plus cette valeur est forte, plus la pertinence de cette dépendance est grande, ce qui indique à #PlanFacile# qu'il
doit #apposition{s'il le peut bien sûr}# tenter d'associer ces deux idées en priorité.
#§
Par ailleurs, cet élément pourra être omis. #PlanFacile# supposera alors que la pertinence est nulle, ce qui est la plus faible de
toutes#...
#end

#idea{syntaxedependance}Syntaxe d'une dépendance#text
La syntaxe d'une dépendance#dep{1}{dependance}{#depref}{}# comprend donc les quatres éléments précités#dep{dependanceelements}{#depref}{}# :
#verbatimlong{
##dep#{pertinence#}#{référence destination#}#{texte irréductible#}#{texte réductible#}
}
ou, si on omet la pertinence :
#verbatimlong{
##dep#{référence destination#}#{texte irréductible#}#{texte réductible#}
}
#§
Dans le texte irréductible, le remplacement de texte par la référence vers l'idée de destination se fait au moyen de la commande #verbatim{|}{##depref}.
#end

#idea{exemplebase}Un exemple, enfin !#text
Pour appuyer ce qui vient d'être développé dans cette partie basique#dep{1}{base}{#depref}{}, un exemple simple mettant en avant
les deux grands concepts de #PlanFacile# #apposition{à savoir les idées#dep{idee}{#depref}{}# et les dépendances#dep{dependance}{#depref}{}}# vous est ici
donné :
#verbatimlong{
##idea#{terre#}La Terre##text#verbligne
La Terre est une planète tellurique appartenant au système solaire##dep#{1#}#{systemesolaire#}#{##depref#}#{#}### et tourne autour du Soleil##dep#{soleil#}#verbligne
#{##depref#}#{#}### en 365 jours.#verbligne
##end#verbligne
#verbligne
##idea#{lune#}La Lune##text#verbligne
La Lune est un satellite tellurique de la Terre##dep#{1#}#{terre#}#{##depref#}#{#}, et de ce fait, appartient au système solaire##dep#{systemesolaire#}#verbligne
#{##depref#}#{#}.#verbligne
##end#verbligne
#verbligne
##idea#{soleil#}Le soleil##text#verbligne
Le Soleil est l'étoile centrale qui illumine l'ensemble du système solaire##dep#{1#}#{systemesolaire#}#{##depref#}#{#}.#verbligne
##end#verbligne
#verbligne
##idea#{systemesolaire#}Le système solaire##text#verbligne
Le système solaire est l'ensemble des astres dans lequel nous vivons.#verbligne
##end#verbligne
}
#§
Sur cet exemple, vous pouvez retrouver les syntaxes des commandes permettant de représenter les deux concepts traités ici, mais cette fois réunies pour donner
une véritable portion de code source de #PlanFacile . Cela vous donne l'allure générale des commandes, ainsi que leurs positions respectives. Evidemment, ces idées
ont été écrites dans le désordre, charge au compilateur de retrouver qui est un chapitre, une section, etc#...#espace ainsi que l'ordre des idées ici données.
#end

#idea{sourcecomplet}Des idées au code source complet#text
En plus des idées#dep{idee}{#depref}{}# et des dépendances#dep{dependance}{#depref}{}, la base#dep{1}{base}{#depref}{}# d'un code source contient quelques
commandes permettant au compilateur de produire un document complet.
#end

#idea{definitionsstandard}Définitions standard#text
D'une part, il faut ajouter au code source#dep{1}{sourcecomplet}{#depref}{}# une commande permettant au compilateur de charger tout un ensemble de définitions
permettant la génération de documents en XHTML et en #LaTeX .
#end

#idea{definitionsstandarddescription}Que sont les définitions standard ?#text
Ces définitions standard#dep{1}{definitionsstandard}{#depref}{}# contiennent tout le nécessaire pour écrire un document pouvant être généré au format #LaTeX# ou
XHTML #apposition{entre autres}# et ce dans plusieurs langues et jeux de caractères.#comment{FIXME Là, j'anticipe sur la version finale pour les formats et les
langues, car cela est déjà vrai pour les charsets.}
#§
L'inspection du contenu de ces définitions n'est pas recommandé à celui qui est en train d'acquérir les bases de l'utilisation de #PlanFacile , mais vous devriez
être capable de comprendre ce même contenu une fois ce didacticiel terminé.#comment{FIXME Pas sûr, en fait, ça va dépendre de là où je m'arrête...}
#end

#comment{#idea{definitionsstandardinstallation}Installation des définitions standard#text
Les définitions standard#dep{1}{definitionsstandard}{#depref}{}# ne sont pas installées automatiquement par l'installeur actuel.#comment{FIXME Cela sera corrigé
dans la version finale du projet}# Pour les installer, tappez #verbatim{|}{make standard}# dans le répertoire du projet. Cela ajoute le répertoire
#verbatim{|}{~/.planfacile}# sur votre système, si celui-ci n'était pas présent.
#end}

#idea{definitionsstandardinstallation}Installation des définitions standard#text
Les définitions standard#dep{1}{definitionsstandard}{#depref}{}# sont normalement installées en même temps que le compilateur. Pour vérifier qu'elles sont
correctement installées, tappez la ligne de commande : #verbatim{*}{echo "##standard" | planfacile -s}. Si l'erreur concerne la commande #verbatim{|}{##standard},
c'est que les définitions standard ne sont pas correctement accessibles. Revoyez alors l'installation du logiciel.
#end

#idea{definitionsstandardsyntaxe}Utilisation des définitions standard#text#dep{definitionsstandarddescription}{}{}
L'utilisation des définitions standard#dep{1}{definitionsstandard}{#depref}{}# est très simple. Il suffit d'ajouter la commande #verbatim{|}{##standard}# au
début de votre fichier source. #PlanFacile# se charge alors de récupérer les définitions standard et de les charger.
#end

#idea{modificationcomportement}Comportement du compilateur#text
En plus des définitions standard#dep{definitionsstandard}{#depref}{# vues ci-dessus}, il faut modifier le comportement du
compilateur#dep{1}{sourcecomplet}{#depref}{}, pour pouvoir générer un document acceptable.
#end

#idea{modificationcomportementmoyens}Modificateurs#text
Les modificateurs du comportement#dep{modificationcomportement}{#depref}{}# de #PlanFacile# sont basés sur deux mécanismes du compilateur.
#end

#idea{modificationcomportementmoyensoptions}Options#text
Le principal modificateur#dep{modificationcomportementmoyens}{#depref}{}# consiste en un jeu d'options de compilation permettant d'indiquer à #PlanFacile# un
choix de l'utilisateur. Ce choix peut être aussi bien une langue, qu'un format de sortie, ou une précision à apporter au compilateur.
#§
Une option est indiquée par la commande #verbatim{|}{##option#{nom de l'option#}}. La définition d'une option est rétro-active, ce qui signifie qu'elle peut être
définie n'importe où dans votre code source (à deux exceptions près non traitées ici.).
#end

#idea{modificationcomportementmoyensmacro}Macros#text
Un autre modificateur#dep{1}{modificationcomportementmoyens}{#depref}{}# est en fait une utilisation particulière d'un outil assez générique du compilateur.
Plus que d'indiquer comment fonctionne le système des macros#dep{personnalisationmacros}{#depref}{}, nous allons mettre ici l'accent sur son utilisation conjointe
aux options#dep{modificationcomportementmoyensoptions}{#depref}{}.
#§
Pour certaines options, il faut fournir une valeur de type texte pour que le document puisse être généré. Dans ce cas, on utilise une syntaxe qui peut sembler
ici un peu surprenante : #verbatim{|}{##define#{nom du champ à définir#}#{valeur du champ#}}. Si le champ nécessite plusieurs valeurs, il suffit d'indiquer les
autres valeurs à la suite de la première : #verbatim{|}{##define#{nom du champ#}#{valeur du champ 1#}#{valeur du champ 2#}# ... #{valeur du champ n#}}
#end

#idea{exemplesourcecomplet}Reprendrez-vous un peu d'exemple ?#text
Reprenons l'exemple du dessus#dep{exemplebase}{#depref}{}# en y ajoutant les commandes nécessaires pour pouvoir se compiler#dep{1}{sourcecomplet}{#depref}{}. Au
fichier précédemment écrit #apposition{celui contenant les idées} commençons par ajouter la commande :
#verbatimlong{
##standard
}#dep{definitionsstandard}{#depref}{}
#§
Ensuite, ajoutons les options#dep{modificationcomportementmoyensoptions}{#depref}{}# :
#verbatimlong{
##option#{fr#}#verbligne
##option#{iso#}
}
qui indiquent au compilateur la langue et le jeu de caractères utilisés pour l'écriture de ce fichier source.
Ensuite, on peut ajouter un titre au document, en utilisant conjointement les options#dep{modificationcomportementmoyensoptions}{#depref}{}# et les
macros#dep{modificationcomportementmoyensmacro}{#depref}{}# :
#verbatimlong{
##option#{titre#}#verbligne
##define#{titre#}#{Le système solaire#}
}
Notez bien la présence des deux lignes : la première indique la présence d'un titre, et la seconde en donne la valeur.
#§
Si on souhaite pouvoir générer ce document en #LaTeX , il faut y ajouter au moins ces deux options#dep{modificationcomportementmoyensoptions}{#depref}{}# :
#verbatimlong{
##option#{rapport#}#verbligne
##option#{11pt#}
}
qui permettent de spécifier de manière plus fine le type de document #LaTeX# et la taille de la police de caractères que l'on souhaite. On pourra accessoirement
ajouter une page de garde et une table des matières :
#verbatimlong{
##option#{table des matières#}#verbligne
##option#{page de garde#}
}
#§
De même, si l'on souhaite générer ce document au format XHTML, on pourra ajouter ces options#dep{modificationcomportementmoyensoptions}{#depref}{}# :
#verbatimlong{
##option#{icone#}#verbligne
##define#{typeicone#}#{png#}#verbligne
##define#{icone#}#{favicon#}#verbligne
#verbligne
##option#{style#}#verbligne
##define#{style#}#{style.css#}
}
Si vous ajoutez ces deux options, pensez à fournir les deux fichiers #verbatim{|}{favicon.png}# et #verbatim{|}{style.css}# pour obtenir l'effet souhaité.
#§
Enfin, on va surtout éviter d'ajouter les options #verbatim{|}{HTML}# ou #verbatim{|}{LaTeX}# au code source ! Au contraire, on va le laisser tel quel, car on
va pouvoir choisir le format de fichier de sortie au moment de la compilation#...# Pour compiler le document, en admettant que l'on ait sauvegardé le code
source dans le fichier #verbatim{|}{planetes.plf}, on utilise ainsi une des deux lignes de commande :
#verbatimlong{
planfacile -O LaTeX -o planetes.tex  planetes.plf#verbligne
planfacile -O HTML  -o planetes.html planetes.plf
}
#§
Et là, c'est le drame#...#espace car #PlanFacile# ajoute une idée tout seul. Pour le moment, on va chercher à supprimer cette idée, en procédant en deux temps :
#enumeration{#items
{ajouter la commande #verbatim{|}{##option#{sans générique#}}# à notre code source ;}
{recompiler en ajoutant l'option #verbatim{|}{-A}# à l'une des lignes de commande précédente :
#verbatimlong{
planfacile -A -O LaTeX -o planetes.tex  planetes.plf#verbligne
planfacile -A -O HTML  -o planetes.html planetes.plf
}}
}
Lors de cette compilation, #PlanFacile# indique deux avertissements #apposition{le premier peut être supprimé en ajoutant l'option #verbatim{|}{-u}}# sans
importance ici, et produit un code sans cette idée supplémentaire.
#§
Enfin ! Ca y est, vous avez #apposition{sans doute}# pu compiler votre premier code source pour #PlanFacile . Un document plus vaste n'est en fait qu'une
extension de ce que vous venez de faire. Pour être même plus précis, le plus gros du travail vient d'être fait, le reste n'étant qu'un apprentissage basé sur la
syntaxe du langage et de ses possibilités.
#§
Notez que ces options sont des exemples extraits des définitions standard, mais que celles-ci peuvent changer. Le mieux, en cas de problème, est de consulter la
documentation correspondante#comment{TODO Ah tiens... faudra l'écrire un jour, celle-là...}.
#end

#idea{personnalisationmacros}Système des macros#text
Le premier pas à franchir pour la personnalisation#dep{1}{personnalisation}{#depref}{}# d'un code source est l'utilisation des macros de #PlanFacile .
Ce système, complètement intégré au compilateur, permet d'étendre le jeu de commandes de ce dernier en donnant la possibilité à l'utilisateur d'en définir de
nouvelles.
#end

#idea{personnalisation}Personnalisation de #PlanFacile#text
La suite de cette introduction#dep{1}{intro}{#depref}{}# à #PlanFacile# va nous mener un peu plus loin. En effet, un code source composé de la commande
#verbatim{|}{##standard}#dep{definitionsstandard}{#depref}{}# suivie de déclaration d'options et des idées est un code ne comprenant que la
base#dep{base}{#depref}{}# d'un code source, même si celui-ci est complet#dep{sourcecomplet}{#depref}{}.
#§
Tout le travail à accomplir maintenant vise à ajouter des commandes permettant :
#enumeration{#items
{de généraliser le code, pour permettre à un même code d'être généré dans divers formats de sortie ;}
{de remplacer l'appel à la commande #verbatim{|}{##standard}#dep{definitionsstandard}{#depref}{}, en définissant soi-même le format de sortie.}
}
#end

#idea{personnalisationmacrosutilisation}Utilisation#text
L'utilisation des macros#dep{1}{personnalisationmacros}{#depref}{}# se fait en deux temps :
#enumeration{#items
{la définition d'une macro ;}
{l'appel d'une macro.}
}
#§
Ces deux parties sont bien distinctes, et suivent des règles propres à chacune.
#end

#idea{personnalisationmacrosutilisationdefinition}Définition#text
La première chose à faire pour utiliser#dep{1}{personnalisationmacrosutilisation}{#depref}{}# une macro est la définir. Comme tout se réalise sous forme de
commandes au niveau du langage de #PlanFacile , il existe donc une commande de définition de macro. Sa syntaxe est relativement simple :
#verbatimlong{
##define#{nom de la macro#}#{contenu de la macro#}
}
Le nom de la macro peut être n'importe quelle succession de caractères hormis l'espace, la tabulation, le retour chariot, ainsi que le dièse et les accolades.
#§
Ensuite, vient le contenu de la macro, qui est une portion valide de code de #PlanFacile . Déjà, il est important de se souvenir que ce contenu n'est pas un
texte quelconque, mais bien une portion de code valide. Si une commande #verbatim{|}{##idea}# doit se trouver dans une macro, elle se doit d'y être jusqu'au
#verbatim{|}{##end}# qui lui correspond !
#§
De plus, il faut ajouter que le contenu de la macro est paramétrable, grâce à des commandes précises placées dans ce même contenu. Ces commandes, formées d'un
dièse suivi d'un nombre entier positif, sont positionnées aux emplacements devant être paramétrables. #verbatim{|}{##n}# correspondra alors au n-ième paramètre
donné lors de l'appel de la macro, et la plus grande valeur donnée déterminera le nombre de paramètres de la macro.
#§
Enfin, la dernière chose à comprendre concerne la portée d'une macro. En effet, lorsqu'une macro est définie, cela ne signifie pas qu'elle est utilisable
depuis n'importe quel endroit du code source. Pour faire simple, on peut dire qu'une macro est connue dans l'environnement dans lequel elle est définie, à
savoir entre les symboles délimitant la portion de texte ou de code dans laquelle se trouve la définition.
#end

#idea{personnalisationmacrosutilisationappel}Appel#text
A partir du moment où une macro#dep{1}{personnalisationmacrosutilisation}{#depref}{}# est définie#dep{personnalisationmacrosutilisationdefinition}{#depref}{},
elle peut être appellée. La syntaxe d'appel d'une macro ressemble fort à celui d'une commande de #PlanFacile# :
#verbatimlong{
##nomdelamacro#{premier paramètre#} ... #{dernier paramètre#}
}
Le contenu de la macro sera alors substitué à l'appel, les commandes de paramètres elles-mêmes remplacées par leur valeur.
#§
Enfin, il faut savoir que le nombre de paramètres donnés lors de l'appel doit correspondre au nombre de paramètres spécifiés dans la définition de la macro,
sous peine d'erreur à la compilation. Dans ce cas, #PlanFacile# vous indique normalement le nombre de paramètres transmis et celui qui était attendu. De même,
une macro ne peut être appellée à l'intérieur d'elle-même, que ce soit depuis le corps de la définition de la macro ou depuis un paramètre.
#end

#idea{personnalisationmacrostypes}Types de macros#text
Les macros#dep{1}{personnalisationmacros}{#depref}{}# sont divisées en deux groupes, qui se différencient par
l'utilisation#dep{personnalisationmacrosutilisation}{#depref}{}# que l'on peut faire de celles-ci.
#end

#idea{personnalisationmacrostypesnormale}Macros normales#text
Ce type de macro#dep{1}{personnalisationmacrostypes}{#depref}{}# est très simple, et correspond au remplacement simple des
macros#dep{personnalisationmacrosutilisationdefinition}{#depref}{# tel que l'on a déjà pu le voir}. On reconnaîtra une telle macro à l'absence de la commande de
remplacement de paramètre spéciale #verbatim{|}{##0}# dans la définition de la macro.
#end

#idea{personnalisationmacrostypesboucle}Macros boucles#text
Ce type de macro#dep{1}{personnalisationmacrostypes}{#depref}{}# est légèrement plus complexe que le#dep{personnalisationmacrostypesnormale}{# suivant}{
# précédent}.  Il part du principe que dans un texte, on peut parfois trouver une succession de portions de texte toutes formatées de manière identique. Dans ce
cas, le système des macros#dep{personnalisationmacros}{#depref}{}# offre un moyen simplifié d'écrire ces portions de texte répétitives.
#§
Pour cela, il suffit de définir une macro contenant une ou plusieurs fois la commande de remplacement de paramètre spéciale #verbatim{|}{##0}, qui aura valeur
d'indice de boucle. L'appel de la macro se fait alors de manière identique au type#dep{personnalisationmacrostypesnormale}{# suivant}{# précédent}, en plaçant
après les paramètres obligatoires #apposition{ceux qui remplaceront les commandes #verbatim{|}{##n}# avec n strictement positif}# ceux qui remplaceront tour à
tour la commande #verbatim{|}{##0}. L'appel de la macro sera alors équivalent, à peu de choses près, à autant d'appels de cette macro que de paramètres
surnuméraires.
#end

#idea{personnalisationmacrosexemples}Exemples d'utilisation possible#text
Un petit exemple s'impose#...#espace Le système des macros#dep{1}{personnalisationmacros}{#depref}{}, même s'il est un peu complexe à expliquer, reste cependant
très simple à utiliser#dep{personnalisationmacrosutilisation}{#depref}{}# après un peu de pratique.
#§
Commençons par les macros simples#dep{personnalisationmacrostypesnormale}{#depref}{}# :
#verbatimlong{
##define#{macrosimple#}#{macro très simple#}#verbligne
##define#{macro#}#{Ceci est une ##macrosimple### dont "##1" est le premier paramètre.#}#verbligne
##macro#{ceci#}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{macrosimple}{macro très simple}#define{macro}{Ceci est une #macrosimple# dont "#1" est le premier paramètre.}#macro{ceci}}
#verbatimlong{
##define#{melange#}#{##3##1##2#}#verbligne
##melange#{deux#}#{un#}#{trois#}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{melange}{#3#1#2}#melange{deux}{un}{trois}}
#verbatimlong{
##define#{radote#}#{##1##1##2##1#}#verbligne
##radote#{Houla ! #}#{Aie ! #}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{radote}{#1#1#2#1}#radote{Houla ! }{Aie ! }}
#verbatimlong{
##define#{macrosimple#}#{macro très simple#}#verbligne
##define#{macro#}#{Ceci est une ##macrosimple### dont "##1" est le premier paramètre.#}#verbligne
##macro#{ceci##define#{macrosimple#}#{macro drôle#}#}#verbligne
##macro#{cela#}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{macrosimple}{macro très simple}#define{macro}{Ceci est une #macrosimple# dont "#1" est le premier paramètre.}#macro{ceci#define{macrosimple}{macro drôle}}#macro{cela}}
#§
Ce dernier exemple montre le genre de manipulations qui peuvent être faites sur les macros. On peut tout à fait combiner plusieurs définitions de macros en un
seul appel. L'avantage est de limiter la portée de certaines macros qui ne se voient utilisables qu'au sein d'une autre macro, cette macro limitée pouvant être
redéfinie localement. Enfin, on constatera que la définition placée dans le paramètre de la macro #verbatim{|}{##macro}# masque la définition plus globale de la
macro #verbatim{|}{##macrosimple}. Il faut se méfier, dans ce cas, des redéfinitions de macros qui peuvent donner des résultats étonnants :
#verbatimlong{
##define#{macro#}#{##1##sousmacro##2##sousmacro#}#verbligne
##macro#{un##define#{sousmacro#}#{1#}#}#{deux##define#{sousmacro#}#{2#}#}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{macro}{#1#sousmacro#2#sousmacro}#macro{un#define{sousmacro}{1}}{deux#define{sousmacro}{2}}}
#§
Pour vérifier si une macro n'est pas redéfinie alors que l'on obtient un résultat curieux, il suffit de relancer #PlanFacile# avec l'option de ligne de commande
#verbatim{|}{-R}, qui dans ce cas affichera un message équivalent à #verbatim{|}{Attention : Redéfinition de la macro sousmacro.}
#§
Poursuivons avec les macros de type boucle#dep{personnalisationmacrostypesboucle}{#depref}{}# :
#verbatimlong{
##define#{boucle#}#{##1##0##2#}#verbligne
##boucle#{#<#}#{#>#}#{zéro#}#{un#}#{deux#}#{trois#}#verbligne
##boucle#{#}#{, #}#{quatre#}#{cinq#}#{six#}
}
Ce code se remplace donc par le texte : #verbatim{|}{#define{boucle}{#1#0#2}#boucle{#<}{#>}{zéro}{un}{deux}{trois}#boucle{}{, }{quatre}{cinq}{six}}# 
#§
Ici encore, il faudra se méfier des redéfinitions de macro, qui peuvent avoir des résultats surprenants :
#verbatimlong{
##define#{boucle#}#{##1##0##2#}#verbligne
##boucle#{##avant#}#{##après#}#{1##define#{avant#}#{(#}#}#{##define#{après#}#{##avant#}2#}#{3##define#{avant#}#{|#}#}
}
Ce code se remplace donc par le texte :
#verbatim{§}{#define{boucle}{#1#0#2}#boucle{#avant}{#après}{1#define{avant}{(}}{#define{après}{#avant}2}{3#define{avant}{|}}}
#§
En dernier exemple, un jeu avec les macros associées à deux autres commandes qui ensemble produisent un document se résumant à #verbatim{|}{Hello world !}# :
#verbatimlong{
##define#{d#}#{r#}##define#{r#}#{H##2##1##H##define#{r#}#{##1##1#}#}##H#{##h#{##r#{##r#{##e#}##define#{H#}#{##l#}#}#{e#}#}#{##define#{d#}#verbligne
#{### #}!#}#{##h#{##l#}#{##o#}#{##w#{w#}#}##e##define#{h#}#{##3##define#{o#}#{r#}##1##2#}##w#{d#}#{##d#}##define#{w#}#{##0#}#}##define#{h#}
#{##1##d##3##2#verbligne
#}##define#{e#}#{l#}#}##start#{0#}##define#{H#}#{##foot#{##1##define#{l#}#{o#}#}#}
}
#end

#idea{personnalisationmacrospossibilites}Utilisations pratiques#text
Ce paragraphe sur les macros#dep{1}{personnalisationmacros}{#depref}{}, plus que les exemples#dep{personnalisationmacrosexemples}{#depref}{}# donnant l'étendue
des possibilités des macros, se veut proche de l'utilisation dans un véritable document.
#§
Ces macros peuvent avoir plusieurs utilisations :
#enumeration{#items
{jouer avec #PlanFacile , comme le suggère le dernier exemple#dep{personnalisationmacrosexemples}{#depref}{}# ;}
{permettre à un même code source d'être réutilisable dans plusieurs formats : #verbatim{§}{##verbatim#{|#}#{texte en verbatim#}}# pourra être transformé selon
la définition de la macro en #verbatim{§}{\verb|texte en verbatim|}# pour le format #LaTeX , ou en #verbatim{|}{#< code#> texte en verbatim#< /code#>}# pour le
format HTML ;}
{simplifier l'écriture de certaines commandes dans des situations particulières : #verbatim{|}{##define#{DEP#}#{##dep#{##2#}#{##1#}#{##depref#}#{#}#}}# ;}
{spécifier certains paramètres, tel que le titre du document, lorsque l'utilisation de ce paramètre doit se faire au niveau des définitions standard.}
}
#end

#idea{personnalisationoptions}Système d'options#text
Une manière simple de personnaliser#dep{1}{personnalisation}{#depref}{}# un code source est d'utiliser les options de compilation de #PlanFacile .
Ce système permet au rédacteur du document de créer des alternatives de code, et d'en sélectionner certaines soit directement dans le code source, soit au
moment de la compilation.
#end

#idea{personnalisationoptionsoptions}Commande de sélection de code#text
Une partie du système d'options#dep{1}{personnalisationoptions}{#depref}{}# est la commande de sélection de code. Elle va ainsi comprendre en son sein des
portions entières de code qui seront le cas écheant conservées ou éliminées.
#end

#idea{personnalisationoptionsoptionssyntaxe}Syntaxe#text
La syntaxe de la commande de sélection de code#dep{1}{personnalisationoptionsoptions}{#depref}{}# est relativement costaud, mais reste tout à fait
compréhensible :
#verbatimlong{
##options#{label#}#verbligne
##case#{option 1#}# code 1#verbligne
##case#{option 2#}# code 2#verbligne
...#verbligne
##case#{option n#}# code n#verbligne
##other code par défaut#verbligne
##end
}
#§
Le label n'est pas utilisé par le compilateur, mais est utile au rédacteur du code pour préciser la nature du choix qu'il désire apporter au code source.
Entre le label et la première commande #verbatim{|}{##case}, il est impossible de placer un commentaire. Les
options#dep{personnalisationoptionsoption}{#depref}{}# représentent le moyen de choisir les portions de code à conserver.
La clause par défaut introduite par la commande #verbatim{|}{##other}# peut être omise. Dans ce cas, rien n'est exécuté par défaut.
#end

#idea{personnalisationoptionsoptionscomportement}Comportement#text
Les commandes de sélection de code#dep{1}{personnalisationoptionsoptions}{#depref}{}, même si elles
ressemblent#dep{personnalisationoptionsoptionssyntaxe}{#depref}{}# à s'y méprendre aux commandes de sélection de
langage de programmation, se demarquent de ces dernières sur un point particulier.
#§
En effet, dans une commande de sélection de code d'un langage de programmation, exactement une seule clause est executée, ce qui n'est pas toujours le cas avec
#PlanFacile . Détaillons :
#listeapuce{#items
{la première clause dont l'option est définie est exécutée ;}
{si aucune clause n'a été sélectionnée, la clause par défaut est exécutée ;}
{si une clause définit une option dont la clause précède la clause en cours, la clause qui précède sera également exécutée, mais après la clause en cours. Ceci
est dû au caractère global de la définition d'une option#dep{personnalisationoptionsoptioncomportement}{#depref}{}.}
}
#end

#idea{personnalisationoptionsoption}Définition d'options#text
Une partie du système d'options#dep{1}{personnalisationoptions}{#depref}{}# consiste à définir des options, pour indiquer les choix que le rédacteur du document
veut prendre.
#end

#idea{personnalisationoptionsoptionsyntaxe}Syntaxe#text
La syntaxe de la commande de définition d'option#dep{1}{personnalisationoptionsoption}{#depref}{}# est très simple :
#verbatimlong{
##option#{nom d'option#}
}
dont le nom est une portion de texte, généralement un seul mot clef.
#§
Une autre manière de définir une option est d'utiliser l'option de ligne de commande #verbatim{|}{-O #< nom d'option#>}. Ce mécanisme permet d'écrire un code
source dont l'aspect change en fonction d'un choix pris au moment de la compilation.
#end

#idea{personnalisationoptionsoptioncomportement}Comportement#text
Le comportement de la commande de définition d'option#dep{1}{personnalisationoptionsoption}{#depref}{}# est très simple#...#espace La définition peut être
placée n'importe où, à l'exception des macros #apposition{définition et paramètres lors des appels}# ce qui provoquera une erreur ; et des commentaires où elles
sont ignorées.
#§
Une fois qu'une option est définie#dep{personnalisationoptionsoptionsyntaxe}{#depref}{}, son existence est connue depuis le début jusqu'à la fin du code source.
#end

#idea{personnalisationoptionsexemple}Exemple#text
Un exemple d'utilisation assez complet du système d'option#dep{1}{personnalisationoptions}{#depref}{}# va sans nul doute dissiper les derniers doutes concernant
l'utilisation de ce dernier :
#verbatimlong{
##options#{Un#}#verbligne
##case#{A#}aaa#verbligne
##option#{D#}#verbligne
##case#{B#}bbb#verbligne
##case#{C#}ccc#verbligne
##option#{B#}#verbligne
##end#verbligne
##options#{Deux#}#verbligne
##case#{B#}bbb#verbligne
##option#{A#}#verbligne
##case#{D#}ddd#verbligne
##case#{E#}eee#verbligne
##other#verbligne
##option#{C#}xxx#verbligne
##end
}
Ce code va se transformer en :
#options{Un}
#case{A}aaa
#option{D}
#case{B}bbb
#case{C}ccc
#option{B}
#end
#options{Deux}
#case{B}bbb
#option{A}
#case{D}ddd
#case{E}eee
#other
#option{C}xxx
#end
. Le résultat peut sembler surprenant, mais il s'explique facilement grâce aux comportements respectifs des commandes
#verbatim{|}{##option}#dep{personnalisationoptionsoptioncomportement}{#depref}{}# et
#verbatim{|}{##options}#dep{personnalisationoptionsoptionscomportement}{#depref}{}. En fait, les deux commandes de sélection de code vont être remplacées l'une
après l'autre, les clauses de chacune étant remplacées dans l'ordre inverse de leur apparition dans le code source. Ensuite, il reste à expliquer dans quel
ordre sont définies les options :
#enumeration{#items
{aucune option n'est définie en dehors des commandes #verbatim{|}{##options}, donc la clause par défaut de la seconde commande est exécutée ;}
{de ce fait, l'option C est définie, ce qui entraine l'exécution de la troisième clause de la première commande #verbatim{|}{##options} ;}
{du coup, l'option B est définie, exécutant la seconde clause de la première commande #verbatim{|}{##options}# et la première clause de la seconde commande ;}
{l'exécution de la première clause de la seconde commande va donc définir l'option A, qui exécute aussi la première clause de la première commande ;}
{la première clause de la première commande contient la définition de l'option D, qui n'entraîne pas l'exécution de la seconde clause de la seconde commande,
puisque la première clause a déjà été activée.}
}
#end

#idea{personnalisationinclusion}Inclusion de fichier#text
Pour de gros documents, il peut être appreciable de placer les idées dans différents fichiers. De même, le découpage d'un code source en différents fichiers
peut également servir à modifier le contenu d'un code source, au même titre que les options#dep{personnalisationoptions}{#depref}{}, afin de
personnaliser#dep{1}{personnalisation}{#depref}{}# les documents.
#end

#idea{personnalisationinclusioninclude}Inclusion simple#text
La commande d'inclusion#dep{1}{personnalisationinclusion}{#depref}{}# la plus simple est la commande #verbatim{|}{##include#{nom de fichier#}}# où le fichier
doit être indiqué soit par chemin absolu, soit par chemin relatif au fichier courant. Enfin, si le premier caractère du nom du fichier est #verbatim{|}{~}, il
sera remplacé par le contenu de la variable d'environnement #verbatim{|}{$HOME}.
#end

#idea{personnalisationinclusionstandard}Inclusion standard#text
Plus complexe que l'inclusion simple#dep{personnalisationinclusioninclude}{#depref}{}, cette commande d'inclusion#dep{1}{personnalisationinclusion}{#depref}{}#
est un peu spéciale. Présentée #dep{definitionsstandard}{plus loin}{bien avant}, la commande #verbatim{|}{##standard}# permet une inclusion de fichier
spécifique à #PlanFacile . Normalement chargée de récupérer des définitions standard, son utilisation peut être détournée pour modifier à la volée le
comportement du compilateur.
#§
Pour être précis, la commande #verbatim{|}{##standard}# inclut un fichier :
#enumeration{#items
{dont le nom est contenu de la variable d'environnement #verbatim{|}{PLANFACILE}# ;}
{le fichier #verbatim{|}{$XDG_CONFIG_HOME/planfacile/standard.plf}# (si la variable d'environnement #verbatim{|}{XDG_CONFIG_HOME}# n'est pas positionnée, la valeur #verbatim{|}{~/.config}# est utilisée à sa place) ;}
{un fichier installé sur le système, que le compilateur sait retrouver #apposition{la position de ce fichier est déterminé au moment de la compilation de
#PlanFacile}, et contenant les véritables définitions dites standard.}
}
De ce fait, il y a deux possibilités pour détourner l'inclusion du fichier système par celui qui lance la compilation :
#listeapuce{#items
{de manière prolongée, en utilisant le répertoire #verbatim{|}{~/.planfacile}# ;}
{de manière ponctuelle, avec la variable #verbatim{|}{PLANFACILE}.}
}
#end

#idea{personnalisationstyles}Styles#text
La manière la plus flagrante de personnaliser#dep{1}{personnalisation}{#depref}{}# un document produit par #PlanFacile# est de spécifier comment le compilateur
doit produire le document sur sa sortie. Plus que les macros#dep{personnalisationmacros}{#depref}{}# qui permettent de modifier le texte généré localement, les
commandes présentées ici vont modifier les documents produits dans leur globalité.
#§
A dire vrai, les formats #LaTeX# et XHTML, nommés comme des formats disponibles pour le document de sortie ne sont en fait que deux cas particuliers de ce qu'il
est possible de générer avec #PlanFacile . En effet, le compilateur est à lui seul absolument incapable de produire ces deux formats, car il récupère le format
de sortie depuis son code source.
#end

#idea{personnalisationstylesmessage}Messages#text
Un style#dep{1}{personnalisationstyles}{#depref}{}# assez particulier permet à #PlanFacile# d'écrire dans le document un message. Le but alors est généralement
d'indiquer au compilateur comment formater un commentaire dans le document produit. Pour cette raison, dans certains cas, ce format est utilisé pour transmettre
certains commentaires présents dans le code source dans le document produit.
#§
La syntaxe de la commande de formatage de message est donc :
#verbatimlong{
##message#{format de message#}
}
Le format est constitué de texte plus la commande #verbatim{|}{##mesg}# pour indiquer où le message doit réellement être placé.
#end

#idea{personnalisationstylesdocument}Document#text
Un document, composé d'idées, comporte souvent un entête et un pied. Les deux commandes qui suivent permettent de spécifier ces dernier, en précisant leur
format#dep{1}{personnalisationstyles}{#depref}{}# :
#verbatimlong{
##head#{entête#}#verbligne
##foot#{pied#}
}
où l'entête et le pied sont seulement le texte à placer respectivement avant et après les idées du document.
#end

#idea{personnalisationstylesracine}Niveau racine#text
Cette commande de style#dep{1}{personnalisationstyles}{#depref}{}# est très particulière. Elle va indiquer le
niveau#dep{personnalisationstylessection}{#depref}{} que devra avoir l'idée la plus importante du document. Cela permet en particulier de produire une partie
seulement d'un document plus vaste.
#§
La syntaxe est extrêmement simple :
#verbatimlong{
##start#{niveau de départ#}
}
où le niveau est soit une valeur numérique, soit le nom associé à un niveau.
#end

#idea{personnalisationstylessection}Style de section#text
Le style#dep{1}{personnalisationstyles}{#depref}{}# le plus complet est sans nul doute le style de section. Le mieux, pour expliquer comment il fonctionne, est
de partir de la syntaxe de la commande :
#verbatimlong{
##section#{niveau#}#{nom niveau#}#verbligne
#{format pré-sous-idées#}#{format post-sous-idées#}#verbligne
#{format pré-section#}#{format post-section#}
}
#listeapuce{#items
{le niveau, facultatif, est un nombre positif indiquant quel est le niveau hiérarchique affecté par la commande. Si ne niveau n'est pas indiqué, le format
s'applique à tous les niveaux dont un format n'aura pas été spécifié explicitement ;}
{le nom du niveau est un mot permettant d'indiquer le rang d'un niveau hiérarchique. Pour exemple, le mot "section" peut être affecté au niveau 2 ;}
{le format pré-sous-idée est le texte, comprenant les commandes #verbatim{|}{##ref}, #verbatim{|}{##title}# et #verbatim{|}{##txt}# pour indiquer respectivement
la référence, le titre et le texte de l'idée à formater. Mais ce texte sera placé avant les idées de niveau supérieur qui appartiennent à cette idée ;}
{le format post-sous-idée est identique au paramètre précédent, sauf qu'il sera placé après les idées de niveau supérieur appartenant à l'idée en cours ;}
{le format de pré-section est le texte à placer avant toute section de niveau en cours ;}
{le format de post-section est le texte à placer après toute section de niveau en cours.}
}
#end

#idea{personnalisationstylesreference}Style de référence#text
Lorsqu'une dépendance est irréductible, le compilateur peut avoir à formater#dep{1}{personnalisationstyles}{#depref}{}# une référence vers une autre idée :
#verbatimlong{
##reference#{niveau#}#{format de référence#}
}
#listeapuce{#items
{le niveau, facultatif, indique à quel niveau d'idée de destination de la référence s'applique ce format. Le niveau peut être le nom d'un
niveau#dep{personnalisationstylessection}{#depref}{}# ou un nombre entier positif ;}
{le format, en lui-même, constitué de texte comprenant les commandes #verbatim{|}{##ref}, #verbatim{|}{##title}# et #verbatim{|}{##sec}# pour indiquer les
positions respectives de la référence de destination, le titre de l'idée de destination, ainsi que le nom du niveau de l'idée de destination.}
}
#end

#idea{personnalisationstylesexemple}Un exemple#text
Un exemple de fichier de styles#dep{1}{personnalisationstyles}{#depref}{}# va permettre de bien fixer les idées par rapport aux commandes de formatage.
Commençons par les sections#dep{personnalisationstylessection}{#depref}{}# :
#verbatimlong{
##section#{0#}#{document#}#verbligne
#{#}#{#}#{#}#{#}#verbligne
#verbligne
##section#{2#}#{section#}#verbligne
#{#< h2#> #< a id="##ref "#> #< /a#>##title #< /h2#>#verbligne
##txt#verbligne
#}#{#< !-- Fin de section ##ref -->#verbligne
#}#{#}#{#}
#verbligne
##section#{idée#}#verbligne
#{#< li#>#verbligne
#< p#>#< a id="##ref "#> #< /a#>#< strong#>##title #< /strong#>#verbligne
##txt#verbligne
#}#{#< /p#>#verbligne
#< /li#>#verbligne
#< !-- Fin de section ##ref -->#verbligne
#}#{#< ul#>#}#{#< /ul#>#}
}
Poursuivons avec les références#dep{personnalisationstylesreference}{#depref}{}# :
#verbatimlong{
##reference#{document#}#{#}#verbligne
#verbligne
##reference#{### (Voir #< a href="######ref "#>##title #< /a#> )#}
}
Ensuite, le niveau racine#dep{personnalisationstylesracine}{#depref}{}# :
#verbatimlong{
##start#{document#}
}
ainsi que les messages#dep{personnalisationstylesmessage}{#depref}{}, entête et pied de document#dep{personnalisationstylesdocument}{#depref}{}# :
#verbatimlong{
##head#{#< html#>#verbligne
#< head#>#verbligne
#< /head#>#verbligne
#< body#>#verbligne
#}#verbligne
#verbligne
##foot#{#< /body#>#verbligne
#< /html#>#verbligne
#}#verbligne
#verbligne
##message#{#< !-- ##mesg### --#>#}
}
Ce format, une fois complété, donne le format utilisé dans les définitions standard pour générer du XHTML.
#end

#idea{tutoconclusion}Conclusion#text
Enfin se termine ce didacticiel#dep{1}{intro}{#depref}{}# sur #PlanFacile . Si vous lisez ces lignes, vous êtes parfaitement capable de comprendre comment
fonctionne n'importe quel code source de #PlanFacile , et même d'écrire vos propres documents, qu'ils soient basiques#dep{base}{#depref}{}# ou
personnalisés#dep{personnalisation}{#depref}{}.
#end

#idea{prerecquis}Outils prérecquis#text
Nous allons commencer par une brève description des outils dont vous allez avoir besoin pour utiliser #PlanFacile#dep{10}{intro}{#depref}{}# :
#description{#items
{#item{un éditeur de texte ASCII}{pour écrire les codes sources de #PlanFacile# ;}}
{#item{un terminal}{pour lancer les compilations ;}}
{#item{d'un outil de visualisation}{pour lire le document produit par #PlanFacile# une fois formaté (si vous compilez en XHTML, n'importe quel navigateur
web conviendra) ;}}
{#item{de make}{pour éventuellement automatiser le processus de compilation.}}
}
#end
