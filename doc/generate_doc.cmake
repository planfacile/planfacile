#
# This file is part of PlanFacile
#
# Copyright (C) 2014  Sylvain Plantefève
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licences/>.

#--------------
# CONFIGURATION
#--------------
if( NOT DOC_PDF )
        set( DOC_FORMAT "html" )
else( NOT DOC_PDF )
        find_package(LATEX)

        if( NOT "${PDFLATEX_COMPILER}" STREQUAL "" )
                include(generate_pdf.cmake)
                set( DOC_FORMAT "pdf" )
        else( NOT "${PDFLATEX_COMPILER}" STREQUAL "" )
                message( ERROR_FATAL "Documentation generation in PDF format is requested, but pdflatex is missing" )
        endif( NOT "${PDFLATEX_COMPILER}" STREQUAL "" )
endif( NOT DOC_PDF )


function( GENERATE_DOCUMENTATION DOC_TYPE DOC_OUTPUT DOC_SOURCE )
        set( PLANFACILE_ENVIRONNEMENT   "PLANFACILE=${CMAKE_SOURCE_DIR}/standard/standard.plf" )
        set( PLANFACILE_COMPILER        "${CMAKE_BINARY_DIR}/src/${SOFTWARE_NAME}" )
        set( PLANFACILE_OPTIONS         "-AdD" )
        if( NOT DOC_PDF )
                set( PLANFACILE_OUTPUT   ${DOC_OUTPUT} )
                set( PLANFACILE_TEMPLATE "${CMAKE_BINARY_DIR}/templates/HTML.plf" )
                set( PLANFACILE_COMMENT  "Generate ${DOC_TYPE} documentation" )
        else( NOT DOC_PDF )
                string( REPLACE "plf" "tex" PLANFACILE_OUTPUT ${DOC_SOURCE} )
                set( PLANFACILE_TEMPLATE "${CMAKE_BINARY_DIR}/templates/LaTeX.plf" )
                set( PLANFACILE_COMMENT  "Generate ${DOC_TYPE} LaTeX file" )
        endif( NOT DOC_PDF )

        add_custom_command( OUTPUT ${PLANFACILE_OUTPUT}
                DEPENDS ${DOC_SOURCE} ${BIN_TARGET}
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                COMMAND ${PLANFACILE_ENVIRONNEMENT} ${PLANFACILE_COMPILER}
                ARGS    ${PLANFACILE_OPTIONS} -o ${PLANFACILE_OUTPUT} ${DOC_SOURCE} ${PLANFACILE_TEMPLATE}
                COMMENT ${PLANFACILE_COMMENT} )

        if( DOC_PDF )
                generate_pdf( "${CMAKE_CURRENT_BINARY_DIR}/${DIR}" ${DOC_OUTPUT} ${PLANFACILE_OUTPUT} ${DOC_TYPE} )
        endif( DOC_PDF )
endfunction()
