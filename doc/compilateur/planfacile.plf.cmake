#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#standard

#option{fr}
#option{utf8}

#option{auteur}
#define{auteur}{Pappy}

#option{date}
#define{date}{Mardi 28 mars 2006}

#define{parent}{100}
	
#options{Type de sortie}
#case{LaTeX}
	#define{PlanFacile}{#{\sc @PROJECT_NAME@#}}
	#option{titre}
	#define{titre}{Documentation de #PlanFacile# @VERSION@}
#case{HTML}
	#define{PlanFacile}{<strong>@PROJECT_NAME@</strong>}
	#option{titre}
	#define{titre}{Documentation de @PROJECT_NAME@ @VERSION@}
#end

#idea{PlanFacile}#PlanFacile#text
#end

#idea{Installation}Installation de #PlanFacile#text#dep{But}{}{}
L'installation de #PlanFacile#dep{#parent}{PlanFacile}{#depref}{}# se fait simplement en décompressant le tarball disponible sur internet.
#§
Il est recommandé de construire le projet dans un répertoire dédié, et non dans l'arborescence des sources. Par exemple :
#§
#verbatim{|}{mkdir bin/ && cd bin/ && cmake ..}#
#§
#enumeration{#items
{#verbatim{|}{make}}
{#verbatim{|}{sudo make install}}
}
#end

#idea{But}But de #PlanFacile#text
Le but premier de #PlanFacile#dep{#parent}{PlanFacile}{#depref}{}# est d'aider toute personne désirant rédiger un document traitant d'un sujet dont :
#listeapuce{#items
{les différentes idées du document sont globalement connues ;}
{les relations entre les différentes idées sont connues. Ces dépendances expriment un ordre partiel de lecture souhaité entre deux idées ;}
{l'ordre global de lecture, ainsi que la hiérarchie entre idées, est en revanche totalement inconnu.}
}
#§
De plus, l'utilisation de #PlanFacile# permet une reflexion de fond sur ce qui doit être décrit dans un document, et permet donc d'affiner et clarifier des
concepts qui pouvaient sembler complexes à exprimer. De la même manière, une reflexion sur les relations entre idées donne souvent la possibilité au rédacteur
de mieux comprendre le contexte dans lequel se développe chaque idée. Ces deux derniers points font de #PlanFacile# un mindmap à part entière.
#§
A partir de cet enchainement, #PlanFacile# est capable de retrouver l'organisation de ces briques de base et, en particulier leur hiérarchie. De plus,
il est capable dans certains cas d'ajouter de lui-même des briques qui semblent manquer dans le document.
#§
Enfin, #PlanFacile# génère au mieux le document, dans le sens où la lecture de ce dernier sera la plus linéaire possible, en évitant les références
vers des parties plus loin dans le document.
#end

#idea{Algorithme}Algorithme de calcul du plan#text#dep{Installation}{}{}
#PlanFacile#dep{#parent}{PlanFacile}{#depref}{}# contient en son c#oe ur un algorithme permettant de calculer le plan du document à produire.
#§
Cet algorithme a un objectif double :
#enumeration{#items
{retrouver la structure globale du document à partir d'informations locales par rapport à chaque idée#dep{But}{#depref}{}# qui lui sont données, pour minimiser
le nombre de références, qui sont des portions de texte indiquant de se reporter à une autre partie du document ;}
{trouver quelles sont les relations hiérarchiques entre les différentes parties pour construire un vrai plan de document.}
}
#end

#missing{1}{EntreeAlgo}Données fournies à l'algorithme#text
Le document, tel qu'il est considéré par l'algorithme de calcul du plan, n'est constitué que de deux choses détaillées ci-dessous ne s'appuyant pas sur le
contenu textuel du document. De ce fait, #PlanFacile# ne se base pas sur le sens des mots, via un dictionnaire, pour organiser les idées du document.
#end

#idea{DependancesAlgo}Dépendances#text#dep{#parent}{Algorithme}{}{}
Une dépendance est un concept permettant de lier des idées#dep{IdeesAlgo}{#depref}{# #apposition{vues ci-dessus}}# entre elles. Cette liaison, loin s'en faut,
n'est pas bidirectionnelle.
#§
Pour illustrer cette notion, on peut donner un cas de figure très précis : dans un document expliquant l'invention de l'outil, si une idée A porte le concept de
clou, et que l'idée B porte le concept de marteau, alors sans nul doute, pour expliquer l'existence des marteaux, il serait bon d'avoir parlé des clous, en tant
que système de fixation#...#espace Dans ce cas, on dit que l'idée B dépend de l'idée A, mais qu'il y a une dépendance de A vers B. Un moyen simple de se
rappeller le sens des dépendances existe : il suffit de se souvenir que l'idée B dépend de l'idée A pour pouvoir être comprise.
#end

#idea{IdeesAlgo}Idées#text
La première entité reconnue par l'algorithme#dep{#parent}{Algorithme}{#depref}{}# est l'idée, au sens général du terme. Une idée est en fait une brique de base
totalement indivisible représentant un concept unique du document. Ce concept est, pour l'algorithme, simplement une référence sous forme de mot-clef désignant
le concept du document.
#§
Seulement, les idées présentes dans un document, même indivisibles, sont interdépendantes. Pour une idée donnée, on peut trouver d'autres
idées#dep{DependancesAlgo}{, dont on décrira les liens juste après, }{, comme nous l'avons vu ci-dessus.}# ayant un contenu pouvant permettre au lecteur de
comprendre instantanément le sens de l'idée en cours de lecture, tout simplement parce que certaines idées correspondent à des concepts liés.
#end

#idea{FonctionnementAlgo}Fonctionnement de l'algorithme#text
L'algorithme#dep{#parent}{Algorithme}{#depref}{}# de #PlanFacile# est chargé de traiter les idées#dep{IdeesAlgo}{}{}# et les
dépendances#dep{DependancesAlgo}{}{}# entre ces idées. L'objectif de l'algorithme est de retrouver l'agencement global du document, tout en cherchant à obtenir
un ordre d'apparition des idées permettant une lecture linéaire du texte.
#end

#idea{GrapheTreillisAlgo}Du graphe de dépendances au treillis de dé#cesure pen#cesure dan#cesure ces#text
Cette partie de l'algorithme va analyser le graphe de dépendances#dep{#parent}{GrapheArbreAlgo}{#depref}{}, à la recherche d'éventuelles boucles. Lorsqu'une
boucle entre idées est détectée, elle est remplacée par un nouveau sommet, désigné comme idée manquante au sein du document, qui prend comme
successeurs les différentes idées de la boucle.
#end

#idea{GrapheArbreAlgo}Transformation en arbre#text
L'ensemble d'idées et de dépendances formant le document forme, au sens informatique du terme, un graphe. Cette structure de données, n'étant évidemment pas
adaptée à la représentation d'un plan de document plutôt arborescent, sera donc transformée en arbre.
#§
Cette première phase de l'algorithme#dep{#parent}{FonctionnementAlgo}{#depref}{}# est chargée de transformer un graphe de dépendances en un arbre. Pour cela,
une structure intermédiaire est nécessaire, structure reprenant certaines caractéristiques du graphe comme la possibilité d'avoir plusieurs sommets comme
précédesseurs d'un sommet donné, et des arbres, comme l'absence de boucles.
#end

#idea{TreillisArbreAlgo}Du treillis de dépendances à l'arbre non trié#text#dep{#parent}{GrapheArbreAlgo}{}{}
Le passage à l'arbre non ordonné est relativement simple. Il suffit, à partir du treillis obtenu à la passe précédente#dep{GrapheTreillisAlgo}{#depref}{}, de
conserver comme liaison hiérarchique la dépendance la plus pertinente. Pour cela, une notion de pertinence est attachée aux dépendances, permettant ce choix. Cette
pertinence sera tout simplement un nombre entier positif, qui de fait indique l'importance relative entre les dépendances d'une même idée.
#end

#idea{ArbreTriAlgo}Tri de l'arbre#text
Cette seconde partie de l'algorithme#dep{#parent}{FonctionnementAlgo}{#depref}{}# consiste simplement, à partir de l'arbre calculé#dep{TreillisArbreAlgo}{#depref}{}
, d'effectuer des échanges entre idées de même niveau hié#cesure rar#cesure chi#cesure que, afin d'éliminer les références dont la destination se situe après la
source dans le document.
Ces références sont qualifiées d'irréductibles, car elles génèrent un renvoi dans le document permettant au lecteur d'avoir les informations nécessaires à la
com#cesure pré#cesure hen#cesure sion du texte. Les autres, réductibles, ne génèrent pas de renvoi puisque l'idée correspondante, permettant la com#cesure
pré#cesure hen#cesure sion, a normalement été lue, rendant un éventuel renvoi inutile.
#end

#idea{RacineGrapheArbreAlgo}Calcul de l'idée racine#text
Avant le calcul de l'arbre#dep{#parent}{GrapheArbreAlgo}{#depref}{}# des idées correspondant au plan, l'algorithme doit avant tout déterminer une
idée, nommée racine, lui donnant une porte d'entrée dans le graphe de dépendances. L'idée choisie est celle qui ne dépend d'aucune autre idée.
#end

#idea{ExemplesUtilisationsPlanFacile}Exemples d'utilisation de #PlanFacile#text#dep{Installation}{}{}
#PlanFacile#dep{#parent}{PlanFacile}{#depref}{}, de par sa capacité de réorganisation globale d'un document à partir de données réparties localement dans ce
dernier, peut être utilisé avec un avantage certain dans diverses situations, telles que :
#listeapuce{#items
{pour un document complexe, où les idées sont très liées sé#cesure man#cesure ti#cesure que#cesure ment ;}
{pour un document écrit par plusieurs personnes. Dans ce cas, #PlanFacile# permet de ne pas perdre l'impression d'unité logique au sein du document final ;}
{dans une utilisation à des fins pédagogiques, il permet à ceux qui ne sont pas familiers avec l'utilisation d'un compilateur, de comprendre les réactions d'un
tel outil. Cette compréhension est plus simple qu'avec un compilateur destiné à un langage de programmation, car dans le cas de #PlanFacile , il est aisé de
lire le résultat du compilateur, et de le comparer au fichier source.}
}
#end

#idea{UtilisationPlanFacile}Utilisation de #PlanFacile#text
L'utilisation de #PlanFacile#dep{#parent}{PlanFacile}{}{}# peut se décrire en trois composantes bien distinctes :
#listeapuce{#items
{la ligne de commande, pour appeller #PlanFacile# ;}
{la syntaxe des fichiers sources ;}
{l'interprétation des messages d'erreur.}
}
#§
Ces trois composantes distinctes sont en fait reliées par le fait que l'utilisation des trois se cristallise au moment de la compilation par #PlanFacile .
#end

#idea{LigneCommandePlanFacile}Ligne de commande#text
Un des aspects de l'utilisation#dep{#parent}{UtilisationPlanFacile}{}{}# de #PlanFacile# est l'appel du compilateur. Cet appel, qu'il soit effectué directement
depuis le shell ou par l'intermédiaire de l'utilitaire de compilation Make, utilise une syntaxe certes standard dans sa forme, mais dont le fond nécessite des
précisions.
#§
Pour des raisons de confort d'écriture du code de #PlanFacile , le compilateur accepte pas moins de trente cinq options, au format POSIX. Dans la même perspective,
le compilateur accepte en entrée un nombre à priori quelconque de noms de fichiers.
#end

#idea{ArgumentsLigneCommandePlanFacile}Arguments#text
Les arguments de la ligne de commande#dep{#parent}{LigneCommandePlanFacile}{#depref}{}# correspondent en fait aux noms des fichiers contenant le source à compiler.
#§
Si plusieurs fichiers sont indiqués, le compilateur concatène le code de ces différents fichiers pour produire le document de sortie. Si le fichier nommé
#verbatim{|}{-}# est indiqué, l'entrée standard est utilisée. Enfin, dans le cas où aucun fichier n'est indiqué, l'entrée standard est utilisée par défaut.
#§
Le mot-clé #verbatim{|}{%template%}# peut être utilisé pour rechercher le fichier dans les templates#dep{Templates}{#depref}{}.
Ainsi, #verbatim{|}{planfacile %template%/LaTeX.plf fichier.plf}# est équivalent à #verbatim{|}{planfacile @TEMPLATE_DIR@/LaTeX.plf fichier.plf}# (sous réserve que le template #verbatim{|}{LaTeX.plf}# n'est pas surchargé dans l'environnement utilisateur).
#end

#idea{OptionsLigneCommandePlanFacile}Options#text
Pour permettre une plus grande liberté aux rédacteurs de fichiers sources, le compilateur accepte une grande variété d'options de ligne de commande#
#dep{#parent}{LigneCommandePlanFacile}{#depref}{}. Ces options s'écrivent #apposition{comme le standard POSIX l'exige}# avec un tiret #verbatim{|}{-}# suivi
d'une lettre. Certaines de ces options demandent un paramètre lorsqu'elles sont indiquées dans la ligne de commande.
#§
Dans leur grande majorité, ces options agissent comme des bascules : placer deux fois une option la replace dans son état initial. Les exceptions seront bien
entendu précisés.
#§
Heureusement, ces trente cinq options peuvent se regrouper en quelques catégories, ce qui permet de les classer un minimum#...
#end

#idea{VariablesLigneCommandePlanFacile}Variables d'environnement#text
Les variables d'environnement, définies lors de la validation de la ligne de commande#dep{#parent}{LigneCommandePlanFacile}{#depref}{}, peuvent influencer le
comportement de #PlanFacile . Pour être précis, seules trois variables d'environnement ont un impact direct sur le compilateur :
#listeapuce{#items
{#verbatim{|}{LANG}# permet de choisir quelle sera la langue et le jeu de caractères utilisés pour l'affichage des messages de #PlanFacile# sur le terminal ;}
{#verbatim{|}{PLANFACILE}# permet de changer temporairement de fichier à inclure au niveau d'un fichier source contenant la commande #verbatim{|}{##standard}
#dep{StandardSyntaxePlanFacile}{}{}.}
{#verbatim{|}{XDG_CONFIG_HOME}# de l'environnement utilisateur est utilisée pour localiser le dossier de configuration des applications de l'utilisateur, et ainsi localiser le fichier de définitions standards définies par l'utilisateur à utiliser dans le même cas d'utilisation de la commande #verbatim{|}{##standard}. Cette variable n'est pas à manipuler, elle n'est mentionnée içi que pour informer de la prise en compte de ce standard freedesktop.org.}
}
#end

#idea{OptionsGeneralesLigneCommandePlanFacile}Options d'ordre général#text
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt destinées à indiquer une action particulière du compilateur :
#description{#items
{#item{-h (help)}{affiche une aide succinte sur la ligne de commande de #PlanFacile# et sort immédiatement ;}}
{#item{-V (version)}{affiche la license d'utilisation de #PlanFacile , ainsi qu'une note sur les développeurs, et sort immédiatement ;}}
{#item{-v (verbose)}{affiche des messages #apposition{mode verbeux}# à propos des actions effectuées par le compilateur. Plus cette option est placée,
plus le compilateur sera généreux en messages ;}}
{#item{-o (output)}{précise un fichier de sortie donnée en argument. Cette option ne peut être donnée qu'une seule fois, et si elle n'est pas précisée,
la sortie standard est automatiquement sélectionnée ;}}
{#item{-a (all)}{bascule toutes les options désactivées par défaut. D'une manière générale, cela rend le compilateur plus flexible sur certaines erreurs,
et plus bavard sur des points particuliers du code source ;}}
{#item{-n (none)}{bascule toutes les options activées par défaut. Cela à tendance à rendre le compilateur plus silencieux sur certains points particuliers
du code source ;}}
{#item{-e (error)}{transforme tous les avertissements en erreur. Cela rend le compilateur plus strict, car aucune erreur, ou remarque n'est tolérée. A
utiliser conjointement à l'option #verbatim{|}{-a}# pour obtenir une sévérité maximale ;}}
{#item{-W (where)}{permet de ne plus afficher la localisation des erreurs dans le source, ce qui a tendance à alléger la mémoire lors de grosses compilations ;}}
{#item{-w (wanted)}{affiche les chemins absolus des fichiers sources dans les messages d'erreur ou d'avertissement du compilateur. Cela est utile lorsque
l'éditeur de texte utilisé pour la rédaction du code source comporte un outil accélérant le cycle édition-compilation-correction.}}
}
#end

#idea{OptionsSourcesLigneCommandePlanFacile}Options sur les fichiers sources#text#dep{OptionsGeneralesLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt destinées à modifier le comportement du compilateur face aux problèmes
concernant les fichiers dans lesquels se trouvent le source à analyser :
#description{#items
{#item{-S (source)}{ignore un fichier source non ouvrable en lecture. Dans ce cas, la compilation se poursuit comme si le fichier n'existait pas ;}}
{#item{-I (include)}{ignore une inclusion de fichier dont le fichier est déjà en cours d'analyse #apposition{inclusion récursive de fichiers}# ;}}
{#item{-s (source)}{rend le compilateur plus strict vis-à-vis des recherches de fichier, pour les définitions standard#dep{StandardSyntaxePlanFacile}{#depref}{}# ou les templates#dep{Templates}{#depref}{}, en le rendant en même temps plus bavard sur ces recherches.
}}
}
#end

#idea{OptionsCompilationLigneCommandePlanFacile}Options sur la compilation#text#dep{OptionsSourcesLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt destinées à modifier la compilation en elle-même :
#description{#items
{#item{-O (option)}{définit une option de compilation, qui est donnée en paramètre de cette option de ligne de commande. La définition d'une option de compilation
par ce moyen là revient à placer une commande #verbatim{|}{##option}#dep{OptionSyntaxePlanFacile}{#depref}{}# en tête du fichier source ;}}
{#item{-u (user)}{désactive l'affichage des avertissements utilisateurs ;}}
{#item{-C (command)}{ignore toute commande qui ne serait pas placée à une position correcte dans le code source. La commande est purement et simplement supprimée.}}
}
#end

#idea{OptionsIdeesLigneCommandePlanFacile}Options sur les idées#text#dep{OptionsCompilationLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt destinées à modifier le comportement du compilateur face à l'interprétation des
idées :
#description{#items
{#item{-i (idea)}{permet d'éviter l'émission d'un avertissement en cas de redéfinition d'une idée. Pour être plus précis, deux idées ayant la même référence
sont considérées comme identiques. Dans ce cas, seule la dernière définie est utilisée ;}}
{#item{-Y (empty)}{ignore les idées dont la référence est vide ;}}
{#item{-m (missing)}{ignore les idées manquantes dont les indices sont incorrects. Ces indices sont normalement des valeurs numériques entières strictement positives ;}}
{#item{-g (generic)}{ignore les idées génériques dont la référence sur l'idée parente est incorrecte. Ces références sont normalement une référence d'idée
#verbatim{|}{##idea.}#dep{IdeaIdeesSyntaxePlanFacile}{}{}}}
{#item{-r (root)}{désactive l'affichage d'un ajout éventuel d'une idée racine. Cela arrive lorsque #PlanFacile# trouve plusieurs idées sans dépendances ;}}
{#item{-A (automatic)}{ignore les idées automatiquement ajoutées par #PlanFacile# qui ne seraient pas résolues par une des commandes #verbatim{|}{##missing}
#dep{MissingIdeesSyntaxePlanFacile}{#depref}{}# et #verbatim{|}{##generic}#dep{GenericIdeesSyntaxePlanFacile}{#depref}{}.}}
}
#end

#idea{OptionsDependancesReferencesLigneCommandePlanFacile}Options sur les dépendances et les références#text#dep{OptionsIdeesLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt destinées à influencer le comportement de #PlanFacile# vis-à-vis des
dépendances et des références :
#description{#items
{#item{-d (destination)}{ignore les dépendances dont la destination est incorrecte. Une dépendance correcte se fait sur une autre idée du document ;}}
{#item{-D (dependency)}{indique les dépendances dont #PlanFacile# ne va pas se servir pour calculer le plan. Cela ne signifie en aucun cas que ces dépendances sont
fausses, mais tout simplement qu'elles ne sont pas utiles pour ce calcul ;}}
{#item{-p (pertinence)}{ignore les dépendances dont la pertinence n'est pas une valeur numérique entière positive ;}}
{#item{-E (extern)}{ignore les références extérieures dont l'indice n'est pas une valeur numérique entière strictement positive.}}
}
#end

#idea{OptionsStylesLigneCommandePlanFacile}Options sur les styles#text#dep{OptionsDependancesReferencesLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# sont plutôt vouées à modifier la manière dont le compilateur doit gérer les styles, ce
qui a une conséquence directe sur la sortie du compilateur :
#description{#items
{#item{-c (comment)}{permet de transmettre les commentaires placés dans le texte dans le document généré ;}}
{#item{-y (style)}{affiche toute redéfinition de style. Seul le dernier est pris en compte ;}}
{#item{-l (level)}{ignore toute définition de style dont le niveau est incorrect. Une valeur numérique entière positive fera toujours l'affaire ;}}
{#item{-G (ignore)}{ignore toutes les idées dont le format de section ne peut être trouvé ;}}
{#item{-N (reference)}{ignore toutes les références dont le format n'a pas été défini ;}}
{#item{-T (section)}{ignore les commandes #verbatim{|}{##sec}#dep{ExpliciteReferenceStyleSyntaxePlanFacile}{#depref}{}# dont le niveau n'est pas défini.}}
}
#end

#idea{OptionsMacrosLigneCommandePlanFacile}Options sur les macros#text#dep{OptionsStylesLigneCommandePlanFacile}{}{}
Ces options#dep{#parent}{OptionsLigneCommandePlanFacile}{#depref}{}# concernent plus précisément les macros de #PlanFacile# :
#description{#items
{#item{-M (macro)}{affiche toute redéfinition de macro. La position dans le source indique la macro qui remplace la précédente ;}}
{#item{-U (unknown)}{ignore tout appel de macro non définie. L'appel est simplement supprimé ;}}
{#item{-P (parameters)}{réajuste le nombre de paramètres de la macro con#cesure for#cesure mé#cesure ment à sa définition : si des paramètres sont ajoutés,
ils sont ajoutés vides, sinon ils sont ignorés ;}}
{#item{-R (recursive)}{ignore tout appel récursif de macro. La récursivité est absolument interdite avec les macros ;}}
{#item{-t (option)}{ignore toute définition d'option de compilation placée au sein d'une macro. Ceci est une limitation nécessaire au bon déroulement de la compilation.}}
}
#end

#idea{ErreursPlanFacile}Erreurs de #PlanFacile#text
#PlanFacile# détecte un nombre assez important d'incorrections possibles dans les fichiers sources, lors de son invocation#dep{#parent}{UtilisationPlanFacile}
{#depref}{}.
#§
Cependant, toutes ces incorrections ne sont pas équivalentes, et le compilateur est capable de graduer la sanction en fonction de l'importance des problèmes rencontrés.
En clair, il y a quatre niveaux distincts de réponse :
#description{#items
{#item{la notification}{est un avertissement que l'on obtient sur demande explicite au compilateur ;}}
{#item{l'avertissement}{est un avertissement que l'on obtient par défaut. Cependant, il est possible de demander au compilateur ne de pas afficher cet avertissement ;}}
{#item{l'erreur rattrapable}{est une erreur dont la gravité est modérée. A l'aide d'une option, le compilateur peut transformer cette erreur en un avertissement et
adopter un comportement visant à réparer au mieux l'erreur commise ;}}
{#item{l'erreur non rattrapable}{est une erreur dont la seule issue est une modification du code source, étant donné qu'il s'agit là d'une faute grave dont
aucune solution n'est connue de #PlanFacile .}}
}
#end

#idea{EnvironnementErreursPlanFacile}Erreurs dûes à l'environnement#text
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# sont reliées au système dans lequel se trouve #PlanFacile# :
#description{#items
{#item{Mémoire insuffisante pour la poursuite de la compilation}{Cette erreur non rattrapable est dûe à un manque de mémoire empêchant la poursuite de la compilation ;}}
{#item{Impossible d'éxécuter PlanFacile dans le répertoire courant}{Cette erreur non rattrapable indique que le répertoire dans lequel revient le compilateur a
changé depuis un passage précédent ;}}
{#item{Option #apposition{-option}# non reconnue. (#... )}{Cet avertissement est obtenu lorsque qu'une option de ligne de commande n'est pas utilisée par le compilateur.}}
}
#end

#idea{FichiersErreursPlanFacile}Erreurs relatives aux fichiers#text#dep{EnvironnementErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# indiquent toutes une opération impossible sur un fichier :
#description{#items
{#item{Impossible d'ouvrir le fichier #apposition{fichier}# en lecture}{Cette erreur rattrapable indique qu'un fichier source ne peut être ouvert pour être analysé.
Si l'erreur est rattrapée, la commande d'inclusion est ignorée ;}}
{#item{Impossible d'ouvrir le fichier #apposition{fichier}# en écriture}{Cette erreur non rattrapable indique que le fichier désigné pour enregistrer le document
produit ne peut être ouvert en écriture. Il faut alors préciser un autre fichier ;}}
{#item{Impossible d'ouvrir le fichier de nom vide en lecture}{Cette erreur rattrapable indique qu'une commande #verbatim{|}{##include}# a été appellée sans nom de
#dep{IncludeSyntaxePlanFacile}{}{}
fichier. Si l'erreur est rattrapée, la commande d'inclusion est ignorée ;}}
{#item{Aucun fichier n'a pu être ouvert pour #dièse standard}{Cette erreur non rattrapable, obtenue avec l'option -s de la ligne de commande
#dep{OptionsSourcesLigneCommandePlanFacile}{#depref}{}, indique que la commande #verbatim{|}{##standard}# n'a pas pu ouvrir de fichier ;}
#dep{StandardSyntaxePlanFacile}{}{}}
{#item{Inclusion récursive du fichier #apposition{fichier}}{Cette erreur rattrapable indique qu'une inclusion de fichier essaye d'ouvrir un fichier déjà en cours
d'analyse par le compilateur. Si l'erreur est rattrapée, la commande d'inclusion est ignorée ;}}
{#item{Fichier de sortie déjà spécifié}{Cette erreur non rattrapable indique la présence d'une option de ligne de commande -o
#dep{OptionsGeneralesLigneCommandePlanFacile}{#depref}{}# surnuméraire.}}
}
#end

#idea{CompilationErreursPlanFacile}Erreurs de compilation#text#dep{FichiersErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# indiquent un problème vraiment attaché au langage de #PlanFacile# :
#description{#items
{#item{Erreur de syntaxe avant le symbole #apposition{symbole}}{Cette erreur non rattrapable indique qu'une commande est syntaxiquement incorrecte. Dans ce cas, la
seule solution est de modifier la commande ;}}
{#item{Commande #apposition{commande}# mal placée}{Cette erreur rattrapable indique d'une commande est placée à un endroit où elle n'est pas admise par le langage.
Si l'erreur est rattrapée, la commande fautive est ignorée.}}
}
#end

#idea{MacrosErreursPlanFacile}Erreurs avec les macros#text#dep{CompilationErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# correspondent toutes à une mauvaise utilisation des macros :
#description{#items
{#item{Redéfinition de la macro #apposition{macro}}{Cette notification sert simplement à indiquer une redéfinition de macro. Cela permet de savoir quelle est la
macro qui est utilisée, en cas de résultat inattendu ;}}
{#item{Appel de la macro #apposition{macro}# non définie}{Cette erreur rattrapable indique qu'aucune définition ne correspond à un appel de macro. Si cette erreur
est rattrapée, l'appel est supprimé ;}}
{#item{Appel de la macro #apposition{macro}# avec #apposition{nbre}# paramètre(s) (#... )}{Cette erreur rattrapable indique qu'une macro n'est pas correctement
appellée. Dans le cas où cette erreur est rattrapée, le compilateur ajuste le nombre des paramètres automatiquement, en ajoutant au besoin des paramètres vides ;}}
{#item{Appel récursif de la macro #apposition{macro}}{Cette erreur rattrapable indique qu'une macro est appellée récursivement, ce qui est strictement interdit.
Si cette erreur est rattrapée, l'appel de la macro est supprimé ;}}
{#item{Option #apposition{option}# définie à l'intérieur de la macro #apposition{macro}}{Cette erreur rattrapable indique qu'une option est définie à l'intérieur
d'une macro, ce qui est également interdit. Dans ce cas, si l'erreur est rattrapée, la définition de l'option est ignorée.}}

}
#end

#idea{IdeesErreursPlanFacile}Erreurs sur les idées#text#dep{MacrosErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# correspondent à tous les petits tracas que l'on peut avoir avec les idées#dep{IdeesAlgo}{#depref}{}
, sauf le drame de la page blanche :
#description{#items
{#item{Redéfinition de l'idée #apposition{idée}}{Cet avertissement indique simplement qu'une idée normale a été redéfinie. Cela arrive lorsque deux idées ont la
même référence ;}}
{#item{Idée de référence vide incorrecte}{Cette erreur rattrapable indique que la référence d'une idée normale est vide. Lorsque cette erreur est rattrappée, l'erreur
est ignorée.}}
}
#§
Par souci de clarté, les idées automatiques seront traitées séparément.
#end

#idea{IdeesGeneriquesErreursPlanFacile}Erreurs sur les idées génériques#text
Ces erreurs#dep{ErreursPlanFacile}{#depref}{}# sont toutes plus spécifiques aux idées génériques#dep{#parent}{IdeesErreursPlanFacile}{#depref}{}# :
#description{#items
{#item{Redéfinition de l'idée générique par défaut}{Cet avertissement in#cesure di#cesure que simplement que la résolution par défaut d'une idée générique a été
redéfinie ;}}
{#item{Redéfinition de l'idée générique de référence parente #apposition{référence}}{Cet avertissement indique simplement que la résolution d'une idée
gé#cesure né#cesure ri#cesure que dont la référence parente a été spécifiée a été redéfinie ;}}
{#item{Idée générique de référence vide incorrecte}{Cette erreur rattrapable indique qu'une idée générique dont la référence est vide a été trouvée dans le source.
Lorsque cette erreur est rattrapée, la résolution de l'idée est ignorée ;}}
{#item{Idée générique de référence parente #apposition{référence}# non définie}{Cet#cesure te erreur rattrapable indique qu'une résolution d'idée générique manque.
Dans le cas où l'erreur est rattrapée, la résolution est ignorée ;}}
{#item{Référence parente d'idée générique incorrecte}{Cette erreur rat#cesure tra#cesure pa#cesure ble est émise lorsqu'une résolution d'idée générique porte sur
une idée parente inexistante.}}
}
#end

#idea{IdeesManquantesErreursPlanFacile}Erreurs sur les idées manquantes#text
Ces erreurs#dep{ErreursPlanFacile}{#depref}{}# sont toutes plus spécifiques aux idées manquantes#dep{#parent}{IdeesErreursPlanFacile}{#depref}{}# :
#description{#items
{#item{Redéfinition de l'idée manquante par défaut}{Cet avertissement in#cesure di#cesure que simplement que la résolution par défaut des idées manquantes a été
redéfinie ;}}
{#item{Redéfinition de l'idée manquante d'indice #apposition{indice}}{Cet avertissement indique simplement que la résolution de l'idée manquante d'indice spécifié
dans le message a été redéfinie ;}}
{#item{Idée manquante de référence vide incorrecte}{Cette erreur rattrapable dénonce une résolution d'idée manquante par une idée dont la référence est vide. Si cette
erreur est rattrappée, la résolution est ignorée ;}}
{#item{Idée manquante d'indice #apposition{indice}# non définie}{Cette erreur rattrapable intervient lorsque #PlanFacile# ne peut résoudre une idée manquante. Si
cette erreur est rattrapée, la résolution de l'idée est ignorée ;}}
{#item{Indice d'idée manquante incorrect}{Cette erreur rattrapable indique qu'une résolution d'idée manquante porte sur un indice invalide. Cette résolution est ignorée
dans le cas où l'erreur est rattrapée.}}
}
#end

#idea{DependancesReferencesErreursPlanFacile}Erreurs sur les dépendances et sur les références#text#dep{IdeesErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# sont reliées aux dépendances#dep{DependancesAlgo}{#depref}{}, ainsi qu'aux références d'une idée vers une autre,
qui sont directement issues des dépendances :
#description{#items
{#item{Dépendance vers l'idée #apposition{idée}# incorrecte}{Cette erreur rattrapable est émise lorsque la destination d'une idée n'est pas celle d'une idée réellement
existante. Si cette erreur est rattrapée, la dépendance est ignorée ;}}
{#item{Dépendance vers l'idée de référence vide incorrecte}{Cette erreur rattrapable dénonce l'utilisation d'une dépendance vers une idée dont la référence est vide.
Lorsque que cette erreur est rattrapée, la dépendance est simplement ignorée ;}}
{#item{Pertinence de la dépendance vers l'idée #apposition{idée}# incorrecte}{Cette erreur rattrapable indique que la pertinence d'une dépendance est incorrecte. La
pertinence est une valeur entière positive, qui peut être représentée en décimal, en octal ou en héxadécimal. La dépendance est ignorée dans le cas où l'erreur est
rattrapée ;}}
{#item{Dépendance vers l'idée #apposition{idée}# non utilisée pour le calcul du plan}{Cette notification sert à vérifier si une dépendance, même correcte, ne sert pas
au calcul du plan ;}}
{#item{Dépendance vers l'idée de référence vide non utilisée pour (#... )}{Cette notification sert à indiquer que la dépendance dont la destination est une référence
vide ne peut pas servir au calcul du plan ;}}
{#item{Indice de référence #apposition{référence}# incorrect}{Cette erreur rattrapable est émise lorsque l'indice d'une référence extérieure est incorrect. Si l'erreur
est rattrapée, la référence extérieure#dep{MissingIdeesSyntaxePlanFacile}{#depref}{}# est ignorée ;}}
{#item{Nom de section de niveau #apposition{niveau}# indéfini}{Cette erreur rattrapable indique qu'un nom de section n'est pas défini. Dans le cas où l'erreur est
rattrapée, la commande devant inclure le nom de section est ignorée.}}
}
#end

#idea{CalculPlanErreursPlanFacile}Erreurs sur le calcul du plan#text#dep{DependancesReferencesErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# sont relatives au calcul du plan#dep{FonctionnementAlgo}{#depref}{}# :
#description{#items
{#item{Impossible de déterminer l'idée racine}{Cette erreur non rattrapable indique une erreur de topologie du graphe de dépendance : le graphe ne contient aucune
idée #apposition{nommée racine}# qui ne dépende de rien, véritable point d'entrée dans ce graphe pour l'algorithme de calcul du plan. Dans ce cas, il suffit d'ajouter
une idée racine à la main, et établir les dépendances concernant cette nouvelle idée ;}}
{#item{Idée #apposition{idée}# non atteignable depuis la racine du document}{Cet#cesure te erreur non rattrapable indique une autre erreur de topologie du graphe de
dépendances, à savoir qu'au moins une idée ne peut pas être atteinte en suivant les dépendances depuis la racine du document. Dans ce cas, la topologie doit être
complétée pour que les idées deviennent toutes atteignables ;}}
{#item{Ajout d'une idée racine au document}{Cet avertissement sert à indiquer que #PlanFacile# a ajouté une idée complètement virtuelle comme racine du document. Cela
arrive lorsque le graphe de dépendances comprend plusieurs racines potentielles.}}
}
#end

#idea{StylesErreursPlanFacile}Erreurs sur les styles#text#dep{CalculPlanErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{ErreursPlanFacile}{#depref}{}# sont toutes liées aux commandes de style de #PlanFacile .
Elles peuvent être classées selon le type de commande sur lesquelles elles portent, et c'est ce classement qui sera ici adopté, dans un but de clarté.
#end

#idea{SectionsStylesErreursPlanFacile}Erreurs sur les sections#text
Ces erreurs#dep{#parent}{StylesErreursPlanFacile}{#depref}{}# concernent tout ce qui est relatif aux formats de section :
#description{#items
{#item{Redéfinition du style pour les sections de niveau #apposition{niveau}}{Cet#cesure te notification indique que l'on modifie un style de section de niveau
explicitement spécifié, et qui a été défini au préalable ;}}
{#item{Redéfinition du style par défaut pour les sections}{Cette notification indique que l'on modifie le style de section pour tous les niveaux non explicitement
définis ;}}
{#item{Niveau incorrect pour la définition du format de section}{Cette er#cesure reur rattrapable indique que le niveau, censé être une valeur entière positive, est
incorrect. Si l'erreur est rattrapée, le format de section est ignoré ;}}
{#item{Format de section non défini pour l'idée #apposition{idée}}{Cette erreur rattrapable signale qu'une idée n'a pas de format de section associé. Si l'erreur
est rattrapée, l'idée est ignorée, comme toutes celles du niveau incriminé.}}
}
#end

#idea{ReferencesStylesErreursPlanFacile}Erreurs sur les références#text#dep{SectionsStylesErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{StylesErreursPlanFacile}{#depref}{}# concernent quant à elles tout ce qui est relatif aux formats de référence :
#description{#items
{#item{Redéfinition du style pour les références de niveau #apposition{niveau}}{Cet#cesure te notification indique qu'un format de référence dont le niveau de la
destination explicitement désigné a été modifié ;}}
{#item{Redéfinition du style par défaut des références}{Cette notification indique que le format par défaut des références a été modifié ;}}
{#item{Niveau incorrect pour la définition du format de référence}{Cette erreur rattrapable survient lorsqu'un format de référence est appliqué à un niveau inexistant.
Lorsque l'erreur est rattrapée, le format est ignoré ;}}
{#item{Format de référence non défini pour la référence #apposition{référence}}{Cette erreur rattrapable signale qu'une référence n'a pas de format associé. Si l'erreur
est rattrapée, la référence est ignorée, comme toutes celles dont la destination est du niveau incriminé.}}
}
#end

#idea{DocumentMessagesStylesErreursPlanFacile}Erreurs sur les messages, les entêtes et pied de document#text#dep{ReferencesStylesErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{StylesErreursPlanFacile}{#depref}{}# sont liés au formatage du document, en dehors des idées :
#description{#items
{#item{Redéfinition du style de message}{Cette notification indique que la ma#cesure niè#cesure re qu'a #PlanFacile# pour formatter les commentaires et les
messages ont été modifiés ;}}
{#item{Redéfinition de l'entête de document}{Cette notification indique que l'en#cesure tê#cesure te du document a été changée ;}}
{#item{Redéfinition du pied de document}{Cette notification indique que le pied du document a été changé.}}
}
#end

#idea{RacineStylesErreursPlanFacile}Erreurs sur le niveau racine#text#dep{DocumentMessagesStylesErreursPlanFacile}{}{}
Ces erreurs#dep{#parent}{StylesErreursPlanFacile}{#depref}{}# sont celles qui concernent le niveau de l'idée racine, qui à lui seul permet de déterminer
comment formatter toutes les idées :
#description{#items
{#item{Redéfinition du niveau racine}{Cette notification indique seulement une modification du niveau racine ;}}
{#item{Niveau incorrect pour la définition du niveau racine}{Cette erreur rattrapable souligne une définition du niveau racine dont le niveau est incorrect.
Dans le cas où cette erreur est rattrapée, la définition du niveau est ignorée ;}}
{#item{Impossible de déterminer le niveau racine}{Cette erreur non rattrapable indique que le niveau racine n'a pas été spécifié, où n'a pas pu être déduit
à partir des autres commandes de formattage.}}
}
#end

#idea{ErreurPersonnelleErreursPlanFacile}Erreur et avertissement utilisateur#text#dep{StylesErreursPlanFacile}{}{}
Cet avertissement et cette erreur#dep{#parent}{ErreursPlanFacile}{#depref}{}# indiquent un problème déterminé par l'auteur d'un source de #PlanFacile . Le
message est alors celui désigné par le rédacteur du code#...
#end

#idea{LangagePlanFacile}Langage reconnu par #PlanFacile#text
Le point le plus crucial de l'utilisation#dep{#parent}{UtilisationPlanFacile}{#depref}{}# de #PlanFacile# est en fait la rédaction d'un texte source pour #PlanFacile .
Ce texte source, à première vue, semble être un simple texte parsemé de commandes indiquant au compilateur comment réaliser ses traitements.
#end

#idea{FluxLangagePlanFacile}Notion de flux#text
Une première notion intrinsèque au langage#dep{#parent}{NotionsLangagePlanFacile}{#depref}{}# de #PlanFacile# est le flux de commandes#dep{CommandesLangagePlanFacile}
{#depref}{}, véritable enchainement linéaire de commandes pouvant elles-mêmes contenir du texte, voire d'autres commandes. De plus, il est important de remarquer que
ces flux ont tous un début et une fin généralement matérialisés par des éléments syntaxiques du langage.
#§
De même, certaines commandes peuvent contenir des flux. Ces flux seront indiqués comme sous-flux du flux dans lequel se trouvent de telles commandes. L'interêt de cette
remarque vient du fait qu'une action sur un flux peut ou non affecter ses sous-flux. Cette remarque peut sembler anodine, mais elle donne deux portées différentes
pour les commandes de #PlanFacile , portée qui est fortement liée à la notion de flux.
#end

#idea{CommandesLangagePlanFacile}Notion de commandes#text
Une des notions intrinsèques du langage#dep{#parent}{NotionsLangagePlanFacile}{#depref}{}# sont les commandes. Les commandes peuvent être classifiées en deux grandes
catégories. Cependant, que les commandes soient simples ou composées, elles se comportent comme des ensembles logiques ayant un sens unique.
#end

#idea{CommandesSimplesLangagePlanFacile}Commandes simples#text
Une commande#dep{#parent}{CommandesLangagePlanFacile}{#depref}{}# simple est formée d'un dièse, suivi d'un nom #apposition{une suite de caractères ne comportant ni
espace, ni tabulation, ni retour chariot, ni accolade, ni dièse, et ne commençant pas par un chiffre}# lui même éventuellement suivi de paramètres entre accolades.
Ces paramètres sont dans leur grande majorité des flux#dep{FluxLangagePlanFacile}{#depref}{}, ce qui permet une grande souplesse en termes d'écriture de code, mais
peuvent dans certains cas précis n'être qu'une simple portion de texte.
#end

#idea{CommandesComposeesLangagePlanFacile}Commandes composées#text
Une commande#dep{#parent}{CommandesLangagePlanFacile}{#depref}{}# composée est formée en fait d'un certain nombre de commandes
simples#dep{CommandesSimplesLangagePlanFacile}{#depref}{}# placées dans un certain ordre. Entre ces commandes simples, le plus souvent, un
flux#dep{FluxLangagePlanFacile}{#depref}{}# peut être inséré, en tant que paramètre de la commande composée.
#end

#idea{NotionsLangagePlanFacile}Notions sous-jacentes au code source#text
Le langage#dep{#parent}{LangagePlanFacile}{#depref}{}# repose sur deux concepts simples qui vont vous permettre de bien comprendre certains aspects du langage,
ainsi que faciliter l'interprétation de certains messages d'erreur du compilateur.
#end

#idea{SyntaxePlanFacile}Syntaxe de #PlanFacile#text
Cette partie va décrire, commande#dep{CommandesLangagePlanFacile}{#depref}{}# par commande, la syntaxe du langage#dep{#parent}{LangagePlanFacile}{#depref}{}, avec
le rôle de chacun des paramètres des commandes, et surtout l'utilité de ces commandes dans un code source.
#end

#idea{TextuelSyntaxePlanFacile}Commandes textuelles#text
Ces commandes#dep{#parent}{SyntaxePlanFacile}{#depref}{}# sont celles qui vont permettre au compilateur de transmettre du texte depuis le fichier source vers le
document généré.
#end

#idea{TexteSyntaxePlanFacile}Commande de texte#text
Cette commande textuelle#dep{#parent}{TextuelSyntaxePlanFacile}{#depref}{}# est en fait unique dans le langage#dep{LangagePlanFacile}{#depref}{}, car elle est
la seule commande qui, contrairement au principe normal d'écriture des commandes#dep{CommandesSimplesLangagePlanFacile}{#depref}{}, ne commence pas par un
dièse. Pour faire simple, tout texte placé dans le code source est une commande de texte.
#§
Pour être plus précis, une commande de texte est une suite de caractères commençant par n'importe quel caractère hormis les espaces, tabulations, retour chariots et
caractères indiquant une commande. La fin de la commande de texte est la commande suivante, ou la fin de la ligne sur laquelle la commande de texte a été commencée.
De ce fait :
#listeapuce{#items
{le texte peut respecter l'indentation du code source, en plaçant des tabulations ou espaces en début de ligne, sans quelles soient prises en compte ;}
{tout espace, tabulation ou retour-chariot placé après une commande incluse dans une portion de texte sera donc ignoré ;}
{les caractères d'espacement ainsi que les caractères #dièse , #accouvrante , #accfermante# ne peuvent pas être directement entrés dans une commande de texte.}
}
#§
Enfin, il faut préciser une chose importante : les commandes de texte ne peuvent pas êtres utilisées autrement que comme paramètre d'une commande de #PlanFacile
. Si une commande de texte se retrouve en dehors d'une commande, la compilation se terminera par une erreur#dep{CompilationErreursPlanFacile}{#depref}{}# du
compilateur.
#end

#idea{EchappementSyntaxePlanFacile}Commandes d'échappement de caractères#text
Cet ensemble de com#cesure man#cesure des textuelles#dep{#parent}{TextuelSyntaxePlanFacile}{#depref}{}# sert à palier au manque de la commande de texte#dep
{TexteSyntaxePlanFacile}{#depref}{}, qui ne permet pas d'inclure certains caractères dans un texte normal. Ces commandes s'écrivent à l'aide d'un dièse suivi du caractère échappé.
#§
D'une part, il y a cinq commandes d'echappement qui se substituent toujours au caractère représenté : #dièse , #accouvrante , #accfermante , l'espace et la
tabulation. D'autre part, l'echappement du retour chariot peut s'utiliser de deux manières différentes. Soit il permet après une commande de texte d'indiquer
que la ligne en cours se poursuit sur la suivante, ce qui revient à écrire en plusieurs lignes de source une seule ligne un peu longue dans le document généré.
Soit il permet d'ajouter un retour chariot qui aurait normalement été ignoré par le compilateur après une commande, ou en début de paramètre.
#end

#idea{GeneralesSyntaxePlanFacile}Commandes générales de #PlanFacile#text
Ces commandes#dep{#parent}{SyntaxePlanFacile}{#depref}{}# sont des commandes qui ne servent pas directement à la génération du document, mais qui permettent de
modifier le comportement du compilateur.
#end

#idea{CommentaireSyntaxePlanFacile}Commande de commentaire#text
Cette commande#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# sert simplement à isoler une portion de code source. La syntaxe de cette commande est tout
simplement #verbatim{|}{##comment#{commentaire#}}.
#§
Ceci dit, il faut être attentif aux commentaires de #PlanFacile# car, contrairement à beaucoup d'autres langages, ils ne sont pas de simples portions de texte
non prises en compte par le compilateur, mais sont de réelles commandes du compilateur. Cela implique que le paramètre contenant le commentaire doit être syntaxique
valide. Toutes les commandes placées dans un commentaire seront éliminées, sauf les commandes de macro#dep{MacrosSyntaxePlanFacile}{#depref}{}
# et la commande de sélection de code#dep{OptionsSyntaxePlanFacile}{#depref}{}, qui continuent de fonctionner. Ce comportement peut sembler étrange au
premier abord, mais se justifie par l'option de ligne de commande #verbatim{|}{-c}#dep{OptionsStylesLigneCommandePlanFacile}{#depref}{}# qui transfère le texte des
commentaires placés dans un flux de texte destiné à être placé dans le document.
#end

#idea{InclusionSyntaxePlanFacile}Inclusions de fichier#text
Ces commandes#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# servent à rediriger temporairement la lecture de #PlanFacile# momentanément vers un autre
fichier.
#end

#idea{IncludeSyntaxePlanFacile}Inclusion simple#text
L'inclusion#dep{#parent}{InclusionSyntaxePlanFacile}{#depref}{}# simple consiste à indiquer quel fichier doit être lu par le compilateur. La syntaxe de cette
commande est assez simple : #verbatim{|}{##include#{fichier#}}. A la lecture de cette commande, le compilateur arrête l'analyse du fichier en cours et lit le
fichier indiqué, pour revenir au fichier de départ après analyse complète du fichier inclus.

Cette commande accepte un paramètre optionnel #verbatim{|}{template}# permettant de spécifier un nom de fichier relatif au répertoire d'installation des templates#dep{Templates}{#depref}{}. Ainsi, la commande #verbatim{|}{##include#{template#}#{HTML.plf#}}# est équivalente à #verbatim{|}{##include#{@TEMPLATE_DIR@/HTML.plf#}}# (sous réserve que le template #verbatim{|}{HTML.plf}# n'est pas surchargé dans l'environnement utilisateur).
#end

#idea{StandardSyntaxePlanFacile}Inclusion standard#text
Cette inclusion#dep{#parent}{InclusionSyntaxePlanFacile}{#depref}{}# est plus complexe que #dep{IncludeSyntaxePlanFacile}{la seconde}{la pre#cesure miè#cesure re}
#comment{Comment jouer avec les dépendances ^_^"}. En effet, étant donné qu'elle s'utilise sans paramètre #apposition{à savoir ainsi #verbatim{|}{##standard}}
il faut garder à l'esprit où est-ce-que le compilateur va chercher le fichier à inclure. Il utilise trois emplacements précis pour cela, qu'il teste dans l'ordre 
jusqu'à arriver à en ouvrir un :
#enumeration{#items
{le contenu de la variable #verbatim{|}{PLANFACILE}# ;}
{le fichier #verbatim{|}{$XDG_CONFIG_HOME/@SOFTWARE_NAME@/standard/standard.plf}# (si la variable d'environnement #verbatim{|}{XDG_CONFIG_HOME}# n'est pas positionnée, la valeur #verbatim{|}{~/.config}# est utilisée à sa place) ;}
{un fichier précisé au moment de l'installation du projet. A priori, ce fichier est #verbatim{|}{@STD_DEFS_DIR@/standard.plf}.}
}
Le premier est surtout utile pour des tests ou des compilations de documents où l'on veut par exemple modifier le style. Le second correspond au fichier des
définitions standard de l'utilisateur, et le dernier correspond au fichier de définitions standard du système, normalement installé avec le compilateur.
#end

#idea{Templates}Fichiers templates#text
Les templates sont des fichiers source de #PlanFacile#dep{#parent}{PlanFacile}{}{}# permettant de définir d'emblée un certains nombre d'options#dep{OptionSyntaxePlanFacile}{#depref}{}# et éventuellement des ressources à inclure dans le document final.
#§
Le compilateur cherche les templates à deux emplacements, successivement et dans l'ordre :
#enumeration{#items
{dans l'environnement utilisateur, c'est-à-dire dans le répertoire #verbatim{|}{$XDG_CONFIG_HOME/@SOFTWARE_NAME@/templates/}# (si la variable d'environnement #verbatim{|}{XDG_CONFIG_HOME}# n'est pas positionnée, la valeur #verbatim{|}{~/.config}# est utilisée à sa place) ;}
{dans l'environnement système, c'est-à-dire un répertoire précisé au moment de l'installation du projet. A priori, ce dossier est #verbatim{|}{@TEMPLATE_DIR@/}.}
}
#end

#missing{2}{SelectionCodeSyntaxePlanFacile}Options du compilateur#text
Les commandes qui vont vous être présentées maintenant donne le moyen au rédacteur d'un document de proposer plusieurs choix possibles pour une portion de code
source, ainsi que d'exprimer le choix retenu pour une compilation donnée.
#end

#idea{OptionsSyntaxePlanFacile}Utilisation des options#text
La commande#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# de sélection de code est une commande composée qui permet, en fonction des options de compilation
déclarées#dep{OptionSyntaxePlanFacile}{#depref{# (Voir #sec# « #title# ».)}}{}, de ne conserver que certaines parties de code. La syntaxe complète de cette commande
est : #verbatimlong{
##options#{label#}#verbligne
##case#{option 1#}# clause 1#verbligne
...#verbligne
##case#{option n#}# clause n#verbligne
##other clause par défaut (facultative)#verbligne
##end}
#§
Le comportement de cette commande est assez particulier, dû au fait que la déclaration d'une option#dep{OptionSyntaxePlanFacile}{}{}# soit globale. En fait, la
première clause, dont l'option est préalablement déclarée, est executéer. Mais si celle-ci déclare une option dont la clause est au-dessus, la clause en amont
sera elle aussi executée à la suite de la première executée. Il faut noter que cette rétroaction s'effectue sur toutes les commandes #verbatim{|}{##options}# du
code source, et donc que cette nouvelle déclaration d'option peut amener à ce que des clauses d'autres commandes de sélection de code soient activées ! Ensuite,
il est toujours bon de noter que la clause par défaut n'est executée que si aucune des options demandées n'est déclarée. Enfin, il reste une remarque à faire à
propos du label. Il est bien évidemment obligatoire, et contient du texte permettant à l'utilisateur de savoir à quoi correspond la sélection de code,
#PlanFacile# ne l'utilisant absolument pas pour le traitement du code source.
#end

#idea{OptionSyntaxePlanFacile}Déclaration des options#text
La commande#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# de déclaration d'option est très simple : #verbatim{|}{##option#{nom de l'option#}}, avec comme
paramètre un texte #apposition{et non un flux}# indiquant quelle option déclarer. Cette déclaration,
une fois qu'elle est prise en compte par le compilateur, est globale à tout le code source ! Cela signifie que son action porte depuis la première ligne du source
jusqu'à sa dernière, et ce où que soit la commande de déclaration en elle-même.
#§
L'utilisation d'une telle commande peut se faire n'importe où, sauf dans les macros#dep{MacrosSyntaxePlanFacile}{#depref}{}, où son utilisation provoque une erreur.
#§
La déclaration d'une option n'a en soit aucune action directe, mais va avoir une influence localisée sur d'autres commandes#dep{OptionsSyntaxePlanFacile}
{#depref{# (Voir #sec# « #title# ».)}}{}# spécialisées dans la sélection de code.
#end

#idea{MacrosSyntaxePlanFacile}Macros#text
Les commandes#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# présentées ici servent au rédacteur d'un fichier source à créer ses propres commandes.
En effet, le principe d'une macro est de pouvoir créer une commande, et de la redévelopper en une portion de code source en réalisant l'appel de la commande créée.
Cependant, ce remplacement est un remplacement de flux#dep{FluxLangagePlanFacile}{#depref}{}# et non un remplacement textuel réalisé avant compilation. Cela implique que
tous les flux concernés par les macros se doivent d'être syntaxiquement corrects.
#end

#idea{DefineSyntaxePlanFacile}Définition de macro#text
La définition d'une macro#dep{#parent}{MacrosSyntaxePlanFacile}{#depref}{}# se fait à l'aide de la commande #verbatim{|}
{##define#{nom de la macro#}#{définition de la macro#}}. Le premier paramètre, un simple texte #apposition{et non un flux}# indique le nom de la macro.
Pour que la macro puisse être appellée, il faut que ce nom ne commence pas par un chiffre, et ne contienne aucun des caractères échappables#
#dep{EchappementSyntaxePlanFacile}{#depref}{}. Ensuite vient la définition de la macro. Cette définition est un flux#dep{FluxLangagePlanFacile}{#depref}{}# tout
à fait normal, aux déclaration de paramètres près. Une telle déclaration est faite par la commande formée d'un #dièse# suivie d'un entier positif. Le premier paramètre
correspond au numéro 1, le suivant au numéro 2, etc#...#espace le plus grand indice donnant le nombre de paramètres acceptés par la macro. Le paramètre 0 a quant à lui
une signification spéciale, plus facile à exprimer en expliquant comment se déroule l'appel des macros.
Enfin, il reste à évoquer la portée d'une définition de macro :
#listeapuce{#items
{une macro est locale au flux#dep{FluxLangagePlanFacile}{#depref}{}# dans laquelle elle est définie ;}
{une macro est masquée dans un sous-flux si une définition de macro portant le même nom est présente ;}
{une macro est définie sur l'ensemble du flux dans lequel elle est définie ;}
{si deux définitions de macros portent le même nom dans un même flux, seule la seconde est utilisée ;}
{une macro définie dans une clause de sélection de code#dep{OptionsSyntaxePlanFacile}{#depref}{}# est vue depuis tout le flux englobant la commande de sélection de
code, mais les définitions de macro de même nom définies à l'extérieur de la commande de sélection de code sont prioritaires.}
}
#end

#idea{AppelMacroSyntaxePlanFacile}Appel de macro#text
Un appel de macro#dep{#parent}{MacrosSyntaxePlanFacile}{#depref}{}, s'écrivant naturellement comme un commande simple de #PlanFacile# #apposition
{#verbatim{|}{##macro#{param 1#}...#{param n#}}}# requiert déjà l'accessibilité à la définition de la macro#dep{DefineSyntaxePlanFacile}{#depref}{}. Ensuite, l'appel
doit à priori comporter autant de paramètres que la définition de la macro l'indique. A partir de là, deux cas se présentent :
#enumeration{#items
{soit la définition de la macro ne comporte pas le paramètre #verbatim{|}{##0}, auquel cas la macro est simplement appellée : l'appel est remplacé par le contenu de
la définition avec les paramètres remplacés par leur valeur ;}
{soit la définition de la macro contient le paramètre #verbatim{|}{##0}, indiquant que la macro est une boucle. Dans ce cas, tous les paramètres au-delà de ce
qu'indique la définition de la macro sert d'indice de boucle. En fait, la macro va être automatiquement appellée autant de fois que nécessaire, en remplaçant tour
à tour la commande #verbatim{|}{##0}# par la valeur des paramètres surnuméraires. En effet, une macro boucle peut très bien utiliser des paramètres
obligatoires qui seront utilisés à chaque itération de la boucle.}
}
#§
Enfin, il faut garder à l'esprit une restriction importante à l'utilisation des macros : un appel de macro ne peut pas se faire de manière
récursive. En revanche, un appel de macro de même nom peut se faire, à condition de masquer la première définition en cours d'appel avec une définition locale à
ce même appel.
#options{Format de sortie}
#case{LaTeX}
	#define{verbatim}{\verb%#2%}#comment{Cette macro est laissée à titre d'exemple...}
#case{HTML}
#end
#end

#idea{MessagesSyntaxePlanFacile}Messages du compilateur#text
Le rédacteur peut vouloir indiquer un avertissement ou une erreur lors de la compilation. Les commandes#dep{#parent}{GeneralesSyntaxePlanFacile}{#depref}{}# qui
suivent existent dans ce but. Leur syntaxe est évidente :
#listeapuce{#items
{#verbatim{|}{##warning#{avertissement#}}# qui donne un avertissement;}
{#verbatim{|}{##error#{erreur#}}# qui termine la compilation sur une erreur.}
}
Le contenu de ces deux paramètres est similaire à celui des commentaires#dep{CommentaireSyntaxePlanFacile}{#depref}{}.
#end

#idea{StyleSyntaxePlanFacile}Styles#text
Ces commandes#dep{#parent}{SyntaxePlanFacile}{#depref}{}# servent toutes à indiquer au compilateur comment gé#cesure né#cesure rer le document.
La plupart du temps, ces commandes se trouvent dans le fichier des définitions standard#dep{StandardSyntaxePlanFacile}{#depref}{}, afin de ne pas alourdir
inutilement le travail du rédacteur de document.
#end

#idea{DocumentStyleSyntaxePlanFacile}Entête et pied de document#text
Ces deux commandes#dep{#parent}{StyleSyntaxePlanFacile}{#depref}{}# servent à indiquer au compilateur ce qu'il doit insérer comme texte avant et après les idées
#dep{IdeesSyntaxePlanFacile}{#depref}{}# organisées dans le plan calculé.
#end

#idea{EnteteDocumentStyleSyntaxePlanFacile}Entête de document#text
Cette commande de style#dep{#parent}{DocumentStyleSyntaxePlanFacile}{#depref}{}, dont la syntaxe est #verbatim{|}{##head#{entête#}}, insère donc le texte#
#dep{TextuelSyntaxePlanFacile}{#depref}{}# indiqué en paramètre en début de document généré.
#end

#idea{PiedDocumentStyleSyntaxePlanFacile}Pied de document#text#dep{EnteteDocumentStyleSyntaxePlanFacile}{}{}
Cette commande de style#dep{#parent}{DocumentStyleSyntaxePlanFacile}{#depref}{}, dont la syntaxe est #verbatim{|}{##foot#{entête#}}, insère donc le texte#
#dep{TextuelSyntaxePlanFacile}{#depref}{}# indiqué en paramètre en fin de document généré.
#end

#idea{SectionStyleSyntaxePlanFacile}Format de section#text
Ces commandes#dep{#parent}{StyleSyntaxePlanFacile}{#depref}{}# sont là pour indiquer comment #PlanFacile# doit écrire les sections. Il y a deux formes de cette
commande, selon la portée de la définition du formattage.
#end

#idea{DefautSectionStyleSyntaxePlanFacile}Format de section par défaut#text
Le format de section#dep{#parent}{SectionStyleSyntaxePlanFacile}{#depref}{}# par défaut s'indique à l'aide de la commande
#verbatimlong{##section#{nom niveau#}#{format avant#}#{format après#}#verbligne
#{présection#}#{postsection#}}# dont les paramètres signifient :
#description{#items
{#item{nom niveau}{ce paramètre #apposition{textuel}# donne le nom du niveau par défaut ;}}
{#item{format avant}{ce paramètre indique quel est le texte à placer avant les sous-sections lorsque le compilateur doit générér un idée dont le niveau n'a pas
de format explicitement défini. Ce paramètre accepte les mêmes commandes que le format des sections explicitement définis#dep{ExpliciteSectionStyleSyntaxePlanFacile}
{#depref}{}# ;}}
{#item{format après}{ce paramètre indique quel est le texte à placer après les sous-sections lorsque le compilateur doit générér un idée dont le niveau n'a pas
de format explicitement défini. Ce paramètre accepte les mêmes commandes que le format des sections explicitement définis#dep{ExpliciteSectionStyleSyntaxePlanFacile}
{#depref}{}# ;}}
{#item{présection}{ce paramètre indique quel est le texte à placer avant toute section ayant le format de section par défaut ;}}
{#item{postsection}{ce paramètre indique quel est le texte à placer après toute section ayant le format de section par défaut.}}
}
Ces paramètres soulèvent une remarque qui est expliquée#dep{ExpliciteSectionStyleSyntaxePlanFacile}{# ci-après}{# un peu plus haut}.
#§
Le format par défaut correspond en fait au format de toutes les idées dont le niveau hiérarchique n'a pas de format explicitement défini#dep
{ExpliciteSectionStyleSyntaxePlanFacile}{#depref}{}.
#end

#idea{ExpliciteSectionStyleSyntaxePlanFacile}Format de section explicitement défini#text
Le format d'une section#dep{#parent}{SectionStyleSyntaxePlanFacile}{#depref}{}# explicitement défini s'indique à l'aide de la commande #verbatimlong
{##section#{niveau#}#{nom niveau#}#{format avant#}#{format après#}#verbligne
#{présection#}#{postsection#}}# dont les paramètres signifient :
#description{#items
{#item{niveau}{ce paramètre est un nombre entier positif indiquant quel est le niveau hiérarchique affecté par la commande ;}}
{#item{nom niveau}{ce paramètre #apposition{textuel}# donne le nom du niveau explicite ;}}
{#item{format avant}{ce paramètre indique quel est le texte à placer avant les sous-sections lorsque le compilateur doit générér une idée dont le niveau hiérarchique
correspond au niveau indiqué au premier paramètre. Ce paramètre accepte des commandes de formattage permettant de positionner les divers éléments d'une idée :
#listeapuce{#items
{#verbatim{|}{##title}# donne la position du titre de l'idée#dep{IdeesSyntaxePlanFacile}{#depref}{},}
{#verbatim{|}{##ref}# donne la position de la référence de l'idée#dep{IdeesSyntaxePlanFacile}{#depref}{},}
{#verbatim{|}{##txt}# donne la position du texte de l'idée#dep{IdeesSyntaxePlanFacile}{#depref}{}# ;}
}
}}
{#item{format après}{ce paramètre indique quel est le texte à placer après les sous-sections lorsque le compilateur doit générér une idée dont le niveau hiérarchique
correspond au niveau indiqué au premier paramètre. Ce paramètre accepte les mêmes commandes de formattage que le paramètre précédent ;}}
{#item{présection}{ce paramètre indique quel est le texte à placer avant toute section ayant le format de section ici défini ;}}
{#item{postsection}{ce paramètre indique quel est le texte à placer après toute section ayant le format de section ici défini.}}
}
#§
Ces paramètres amènent une remarque importante : quel est l'intêret d'autant de paramètres alors que les quatres derniers semblent être redondants ? En réalité, leurs
significations #apposition{et donc leur utilisation}# ne sont pas identiques, et de loin ! Lors du formattage d'une idée, les deux paramètres de format sont utilisés
pour formatter l'idée elle-même, alors que les deux derniers sont liés aux sous-sections de l'idée, et ne sont donc présents que si notre idée a des descendants.
Enfin, le format de présection et de postsection appliqué est celui du niveau des sous-sections !
#end

#idea{ReferenceStyleSyntaxePlanFacile}Format de référence#text
Ces commandes#dep{#parent}{StyleSyntaxePlanFacile}{#depref}{}# sont là pour indiquer comment #PlanFacile# doit écrire les références. Il y a deux formes de cette
commande, selon la portée de la définition du formattage.
#end

#idea{DefautReferenceStyleSyntaxePlanFacile}Format de référence par défaut#text
Le format de référence#dep{#parent}{ReferenceStyleSyntaxePlanFacile}{#depref}{}# par défaut s'indique à l'aide de la commande #verbatimlong{##reference#{format#}}# dont
le paramètre indique quel est le texte à placer lorsque le compilateur doit générér une référence dont le niveau de l'idée de destination n'a pas de format
explicitement défini. Ce paramètre accepte les mêmes commandes que le format des références explicitement définies#dep{ExpliciteReferenceStyleSyntaxePlanFacile}
{#depref}{}.
#§
Le format par défaut correspond en fait au format de toutes les références dont le niveau hiérarchique de l'idée de destination de la références n'a pas de format
explicitement défini#dep{ExpliciteReferenceStyleSyntaxePlanFacile}{#depref}{}.
#end

#idea{ExpliciteReferenceStyleSyntaxePlanFacile}Format de référence explicitement défini#text
Le format d'une référence#dep{#parent}{ReferenceStyleSyntaxePlanFacile}{#depref}{}# explicitement défini s'indique à l'aide de la commande #verbatimlong
{##reference#{niveau#}#{format#}}# dont les paramètres signifient :
#description{#items
{#item{niveau}{ce paramètre est un nombre entier positif, ou un nom de niveau explicitement défini, indiquant quel est le niveau hiérarchique affecté par la commande ;}}
{#item{format}{ce paramètre indique quel est le texte à placer lorsque le compilateur doit générér une référence dont le niveau hiérarchique de la destination correspond
au niveau indiqué au premier paramètre. Ce paramètre accepte des commandes de formattage permettant de positionner les divers éléments d'une référence :
#listeapuce{#items
{#verbatim{|}{##title}# donne la position du titre de l'idée#dep{IdeesSyntaxePlanFacile}{#depref}{},}
{#verbatim{|}{##ref}# donne la position de la référence de l'idée#dep{IdeesSyntaxePlanFacile}{#depref}{},}
{#verbatim{|}{##sec}# donne le nom de la section#dep{SectionStyleSyntaxePlanFacile}{#depref}{}# de la destination. Cependant, cette commande peut accepter un unique
paramètre précisant le niveau dont on désire récuperer le nom.}
}
}}
}
#end

#idea{DepartStyleSyntaxePlanFacile}Niveau hiérarchique de départ#text
Cette commande#dep{#parent}{StyleSyntaxePlanFacile}{#depref}{}# sert à indiquer à #PlanFacile# quel est le niveau hiérarchique de l'idée racine#dep
{RacineGrapheArbreAlgo}{#depref}{}, permettant ainsi de savoir quels formats de section#dep{SectionStyleSyntaxePlanFacile}{#depref}{}# utiliser pour produire
le document. La syntaxe de cette commande est simple : #verbatim{|}{##start#{niveau#}}# où le niveau est soit un nombre entier positif, ou le nom d'un niveau
explicitement défini#dep{ExpliciteSectionStyleSyntaxePlanFacile}{#depref}{}.
#§
Si cette commande n'est pas indiquée dans le code, #PlanFacile# prendra comme niveau racine le niveau le plus faible numériquement dont le format aura été
explicitement défini.
#end

#idea{MessageStyleSyntaxePlanFacile}Format de message#text
Cette commande#dep{#parent}{StyleSyntaxePlanFacile}{#depref}{}# sert à deux choses bien distinctes, qui nécessitent un formattage commun :
#enumeration{#items
{afficher un message de statistiques sur le plan calculé en fin de document généré ;}
{transmettre les commentaires#dep{CommentaireSyntaxePlanFacile}{#depref}{}# du fichier source vers le document généré.}
}
#§
Pour cela, il suffit d'indiquer comment se formatte un commentaire dans le document à l'aide de la commande #verbatim{|}{##message#{message#}}# dans laquelle le
paramètre indique le format d'un commentaire. Dans ce paramètre, le contenu du commentaire est indiqué par la commande #verbatim{|}{##mesg}.
#end

#idea{IdeesSyntaxePlanFacile}Idées#text#dep{GeneralesSyntaxePlanFacile}{}{}
Ces commandes#dep{#parent}{SyntaxePlanFacile}{#depref}{}# sont sans nul doute les plus essentielles à un code de #PlanFacile . En effet, ce sont ces trois commandes
qui servent à bâtir l'ensemble du document généré, notamment grâce à la possibilité d'enregistrer le texte#dep{TextuelSyntaxePlanFacile}{#depref}{}.
#end

#idea{IdeaIdeesSyntaxePlanFacile}Idées normales#text
Cette commande d'idée#dep{#parent}{IdeesSyntaxePlanFacile}{#depref}{}# sert à indiquer un concept du document. Sa syntaxe est relativement simple : #verbatimlong{
##idea#{référence#} titre ##text#verbligne
texte avec dépendances#verbligne
##end}
La référence, véritable identifiant de l'idée pour #PlanFacile , est astreint à l'unicité. Ensuite, le second paramètre est le titre de l'idée, tel qu'il devra apparaître
dans les commandes de formattage de section#dep{SectionStyleSyntaxePlanFacile}{#depref}{}# et de référence#dep{ReferenceStyleSyntaxePlanFacile}{#depref}{}.
Enfin, le texte est tout simplement la portion du document qui correspond à l'énoncé du concept de l'idée. Ce texte contient également les dépendances de l'idée, notées
# avec une commande spécifique : #verbatimlong{##dep#{pertinence#}#{référence#}#{irréductible#}#{réductible#}}
Cette commande est en revanche un peu plus complexe à utiliser :
#description{#items
{#item{pertinence}{ce paramètre, un entier positif, indique l'importance de la dé#cesure pen#cesure dan#cesure ce. Ce paramètre peut être omis, et dans ce cas, la
pertinence de la dépendance est prise à 0, soit la pertinence minimale ;}}
{#item{référence}{ce paramètre est la référence vers l'idée dont dépend l'idée en cours ;}}
{#item{irréductible}{ce paramètre est le texte à placer si la dépendance se transforme en une référence irréductible, c'est-à-dire une référence vers une idée située
plus en aval du texte. Dans ce paramètre, il est possible d'ajouter une commande indiquant l'introduction d'une référence : #verbatim{|}{##depref}. Cette référence est
formattée avec le format de référence du niveau de l'idée destination de la référence, à moins que le format soit passé en premier paramètre de la commande
#verbatim{|}{##depref#{format#}}, paramètre supportant les mêmes commandes de formattage que le dernier paramètre de la commande de formattage de référence#dep
{ReferenceStyleSyntaxePlanFacile}{#depref}{}# ;}}
{#item{réductible}{ce paramètre est le texte, ne pouvant contenir ni dépendance ni format de référence, à placer si la destination est située en amont de l'idée en
cours.}}
}
#end

#idea{MissingIdeesSyntaxePlanFacile}Idées manquantes#text
Cette commande d'idée#dep{#parent}{IdeesSyntaxePlanFacile}{#depref}{}, contrairement à l'idée normale#dep{IdeaIdeesSyntaxePlanFacile}{#depref}{}, est une
commande qui sert à indiquer à #PlanFacile# par quel contenu doit être remplacé une idée manquante.
#end

#idea{DefautMissingIdeesSyntaxePlanFacile}Résolution par défaut#text
La résolution des idées manquantes#dep{#parent}{MissingIdeesSyntaxePlanFacile}{#depref}{}# par défaut s'écrit comme une idée normale : #verbatimlong{
##missing#{référence automatique#} titre automatique##text#verbligne
texte avec références extérieures#verbligne
##end}
Les deux seules différences sont la possibilité d'ajouter la commande #verbatim{|}{##index}# dans la référence et le titre, permettant de savoir quelle est l'idée
concernée. Cela est plutôt pratique lorsqu'une nouvelle idée manquante apparaît, car cela permet de la résoudre de manière plus spécifique, avec la commande de
résolution explicite#dep{ExpliciteMissingIdeesSyntaxePlanFacile}{#depref}{}# des idées manquantes. La seconde différence concerne le texte. Il n'est évidemment plus
question de dépendances dans ce paramètre de texte, mais il se peut que des références partent de l'idée manquante. Pour indiquer ces références, il existe deux
commandes :
#listeapuce{#items
{#verbatim{|}{##extref#{indice#}}# permet d'inclure une référence, en utilisant le format de référence du niveau de l'idée de destination ;}
{#verbatim{|}{##extrefs}# permet d'inclure dans l'ordre toutes les références qui partent de l'idée en cours. Cela permet de connaître l'ensemble des références,
spécialement dans la résolution par défaut, afin de savoir rapidement quelles sont les références à remplacer.}
}
Ces deux commandes acceptent un paramètre supplémentaire donnant le format de référence à utiliser à la place de celle définie pour le niveau de l'idée de destination.
#end

#idea{ExpliciteMissingIdeesSyntaxePlanFacile}Résolution explicite#text
La résolution explicite d'une idée manquante#dep{#parent}{MissingIdeesSyntaxePlanFacile}{#depref}{}# suit une syntaxe relativement simple : #verbatimlong{
##missing#{indice#}#{référence automatique#} titre automatique##text#verbligne
texte avec références extérieures#verbligne
##end}
Les paramètres de cette commande sont :
#description{#items
{#item{indice}{ce paramètre, un entier strictement positif, indique quelle idée manquante est résolue par cette commande ;}}
{#item{référence}{ce paramètre indique la référence de l'idée manquante résolue, pouvant inclure la commande #verbatim{|}{##index}# pour indiquer quelle est l'indice
de l'idée résolue ;}}
{#item{titre}{ce paramètre indique le titre de l'idée manquante résolue. La commande #verbatim{|}{##index}# est également permise dans ce paramètre ; }}
{#item{texte}{ce dernier paramètre est simplement le texte de l'idée manquante résolue. Ce texte ne peut contenir de dépendances, mais des références vers d'autres
idées, nommées références extérieures :
#listeapuce{#items
{#verbatim{|}{##extref#{indice#}}# permet d'inclure une référence, en utilisant le format de référence du niveau de l'idée de destination ;}
{#verbatim{|}{##extrefs}# permet d'inclure dans l'ordre toutes les références qui partent de l'idée en cours. Cela permet de connaître l'ensemble des références,
spécialement dans la résolution par défaut, afin de savoir rapidement quelles sont les références à remplacer.}
}
Ces deux commandes acceptent un paramètre supplémentaire donnant le format de référence à utiliser à la place de celle définie pour le niveau de l'idée de destination.
}}
}
#end

#idea{GenericIdeesSyntaxePlanFacile}Idées génériques#text
Cette commande d'idée#dep{#parent}{IdeesSyntaxePlanFacile}{#depref}{}, contrairement à l'idée normale#dep{IdeaIdeesSyntaxePlanFacile}{#depref}{}, est une
commande qui sert à indiquer à #PlanFacile# par quel contenu doit être remplacé une idée générique.
#end

#idea{DefautGenericIdeesSyntaxePlanFacile}Résolution par défaut#text
La résolution des idées génériques#dep{#parent}{GenericIdeesSyntaxePlanFacile}{#depref}{}# par défaut s'écrit comme une idée normale : #verbatimlong{
##generic#{référence automatique#} titre automatique##text#verbligne
texte#verbligne
##end}
La seule différence avec l'idée normale est la possibilité d'ajouter la commande #verbatim{|}{##index}# dans la référence et le titre, permettant de savoir quelle
est l'idée concernée. Cela est plutôt pratique lorsqu'une nouvelle idée générique apparaît, car cela permet de la résoudre de manière plus spécifique, avec la
commande de résolution explicite#dep{ExpliciteGenericIdeesSyntaxePlanFacile}{#depref}{}# des idées génériques. La seconde différence concerne le texte, qui ne peut
contenir ni dépendances#dep{IdeaIdeesSyntaxePlanFacile}{#depref}{}, ni références extérieures#dep{MissingIdeesSyntaxePlanFacile}{#depref}{}.
#end

#idea{ExpliciteGenericIdeesSyntaxePlanFacile}Résolution explicite#text
La résolution explicite d'une idée générique#dep{#parent}{GenericIdeesSyntaxePlanFacile}{#depref}{}# suit une syntaxe relativement simple : #verbatimlong{
##generic#{indice#}#{référence automatique#} titre automatique##text#verbligne
texte#verbligne
##end}
Les paramètres de cette commande sont :
#description{#items
{#item{indice}{ce paramètre, qui n'est en fait que la référence de l'idée parente de l'idée générique, indique quelle idée générique est résolue par cette commande ;}}
{#item{référence}{ce paramètre indique la référence de l'idée générique résolue, pouvant inclure la commande #verbatim{|}{##index}# pour indiquer quelle est la
référence parente de l'idée résolue ;}}
{#item{titre}{ce paramètre indique le titre de l'idée générique résolue. La commande #verbatim{|}{##index}# est également permise dans ce paramètre ; }}
{#item{texte}{ce dernier paramètre est simplement le texte de l'idée générique résolue. Ce texte ne peut contenir ni dépendances#dep{IdeaIdeesSyntaxePlanFacile}
{#depref}{}, ni références extérieures#dep{MissingIdeesSyntaxePlanFacile}{#depref}{}.}}
}
#end

