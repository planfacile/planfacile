"
" PlanFacile (Easy plan, in french) is a small tool to help people to
" write a document on a particular subject.
" Copyright (C) 2005  Julien BRUGUIER
"
" This program is free software; you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation; version 2 of the License.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, see <http://www.gnu.org/licences/>.
"

syntax match cmdoutput "#output"
syntax match cmdfunction "#function"
syntax match cmdlang "#lang"
syntax match cmdlang "#default_lang"
syntax match cmdcharset "#charset"
syntax match cmdcharset "#default_charset"
syntax match cmdmesg "#mesg"
syntax match cmdmesg "#default_mesg"
syntax match cmdinclude "#include" contained
syntax match cmdincludecomplete "#include.\+$" contains=cmdinclude
syntax match cmdend "#end"
syntax match cmdparametre "#[0-9]\+"
syntax match cmdparam "[{}]" contained
syntax match cmdparamcomplete "{[^}]*}" contains=cmdparam
syntax match cmdechap "# "
syntax match cmdechap "#\t"
syntax match cmdechap "##"
syntax match cmdechap "#{"
syntax match cmdechap "#}"
syntax match cmdechap "#\n"
syntax match cmdpass "#pass"
syntax match cmdcomment "\/.*\n"

highlight cmdoutput ctermfg=darkred
highlight cmdfunction ctermfg=darkgreen
highlight cmdlang ctermfg=yellow
highlight cmdcharset ctermfg=green
highlight cmdmesg ctermfg=red
highlight cmdinclude ctermfg=blue
highlight cmdincludecomplete ctermfg=darkblue
highlight cmdend ctermfg=darkmagenta
highlight cmdparametre ctermfg=darkcyan
highlight cmdparam ctermfg=darkyellow
highlight cmdparamcomplete ctermfg=darkgray
highlight cmdechap ctermfg=white
highlight cmdpass ctermfg=black ctermbg=darkyellow
highlight cmdcomment ctermfg=darkblue
