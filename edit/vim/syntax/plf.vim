"
" PlanFacile (Easy plan, in french) is a small tool to help people to
" write a document on a particular subject.
" Copyright (C) 2005  Julien BRUGUIER
"
" This program is free software; you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation; version 2 of the License.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, see <http://www.gnu.org/licences/>.
"

syntax match planfacileparametre	"#[0-9]*"
syntax match planfacilemacro		"#[^ \n\t0-9{}#]\([^ \n\t{}#]*$\|[^ \n\t{}#]*\)"
syntax match planfacileechap		"#[#{} \n\t]"
syntax match planfacileparamdelim	"[{}]"
syntax keyword planfaciledebug 		FIXME contained
syntax keyword planfaciledebug 		TODO contained
syntax region planfacilecommentaire 	start="\(#comment[ \t\n]*\|#\)\@<!{" skip="#}" end="}" contained contains=planfacilecommentaire,planfacilecomment,planfaciledebug
syntax region planfacilecomment 	start="#comment[ \t\n]*{" skip="#}" end="}" contains=planfacilecommentaire,planfacilecomment,planfaciledebug
syntax match planfacilestandard		"#standard\ze\([ \t\n#{}]\|$\)"
syntax match planfacileinclude		"#include\ze\([ \t\n#{}]\|$\)"
syntax match planfacileoption		"#option\ze\([ \t\n#{}]\|$\)"
syntax match planfacileoptions		"#\(options\|case\|other\|end\)\ze\([ \t\n#{}]\|$\)"
"oblige a mettre #text aussi en 'darkred' << pas un pb
syntax match planfaciledefine		"#define\ze\([ \t\n#{}]\|$\)"
syntax match planfacilewarning		"#warning\ze\([ \t\n#{}]\|$\)"
syntax match planfacileerror		"#error\ze\([ \t\n#{}]\|$\)"
syntax match planfacilehead		"#head\ze\([ \t\n#{}]\|$\)"
syntax match planfacilefoot		"#foot\ze\([ \t\n#{}]\|$\)"
syntax match planfacilemessage		"#message\ze\([ \t\n#{}]\|$\)"
syntax match planfacilemesg		"#mesg\ze\([ \t\n#{}]\|$\)"
syntax match planfaciletitle		"#title\ze\([ \t\n#{}]\|$\)"
syntax match planfacileref		"#ref\ze\([ \t\n#{}]\|$\)"
syntax match planfacilesec		"#sec\ze\([ \t\n#{}]\|$\)"
syntax match planfaciletxt		"#txt\ze\([ \t\n#{}]\|$\)"
syntax match planfacilesection		"#section\ze\([ \t\n#{}]\|$\)"
syntax match planfacilereference	"#reference\ze\([ \t\n#{}]\|$\)"
syntax match planfacilestart		"#start\ze\([ \t\n#{}]\|$\)"
syntax match planfaciletext		"#text\ze\([ \t\n#{}]\|$\)"
syntax match planfacileidea		"#idea\ze\([ \t\n#{}]\|$\)"
syntax match planfacilemissing		"#missing\ze\([ \t\n#{}]\|$\)"
syntax match planfacilegeneric		"#generic\ze\([ \t\n#{}]\|$\)"
syntax match planfaciledep		"#dep\ze\([ \t\n#{}]\|$\)"
syntax match planfaciledepref		"#depref\ze\([ \t\n#{}]\|$\)"
syntax match planfacileextref		"#extref\ze\([ \t\n#{}]\|$\)"
syntax match planfacileextrefs		"#extrefs\ze\([ \t\n#{}]\|$\)"
syntax match planfacileindex		"#index\ze\([ \t\n#{}]\|$\)"

if &background == "dark"
	highlight	planfaciledebug		ctermfg=black		guifg=black	ctermbg=yellow
	highlight	planfacileparametre	ctermfg=cyan		guifg=cyan
	highlight	planfacilemacro		ctermfg=cyan		guifg=cyan
	highlight	planfacileechap		ctermfg=white		guifg=white
	highlight	planfacileparamdelim	ctermfg=darkyellow	guifg=darkyellow
	highlight	planfacilecomment	ctermfg=darkblue	guifg=darkblue
	highlight	planfacilecommentaire	ctermfg=darkblue	guifg=darkblue
	highlight	planfacilestandard	ctermfg=darkgreen	guifg=darkgreen
	highlight	planfacileinclude	ctermfg=darkgreen	guifg=darkgreen
	highlight	planfacileoption	ctermfg=darkcyan	guifg=darkcyan
	highlight	planfacileoptions	ctermfg=darkred		guifg=darkred
	highlight	planfaciledefine	ctermfg=cyan		guifg=cyan
	highlight	planfacilewarning	ctermfg=red		guifg=red
	highlight	planfacileerror		ctermfg=red		guifg=red
	highlight	planfacilehead		ctermfg=magenta		guifg=magenta
	highlight	planfacilefoot		ctermfg=magenta		guifg=magenta
	highlight	planfacilemessage	ctermfg=magenta		guifg=magenta
	highlight	planfacilemesg		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfaciletitle		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacileref		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacilesec		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfaciletxt		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacilesection	ctermfg=magenta		guifg=magenta
	highlight	planfacilereference	ctermfg=magenta		guifg=magenta
	highlight	planfacilestart		ctermfg=magenta		guifg=magenta
	highlight	planfaciletext		ctermfg=darkred		guifg=darkred
	highlight	planfacileidea		ctermfg=yellow		guifg=yellow
	highlight	planfacilemissing	ctermfg=yellow		guifg=yellow
	highlight	planfacilegeneric	ctermfg=yellow		guifg=yellow
	highlight	planfaciledep		ctermfg=green		guifg=green
	highlight	planfaciledepref	ctermfg=blue		guifg=blue
	highlight	planfacileextref	ctermfg=blue		guifg=blue
	highlight	planfacileextrefs	ctermfg=blue		guifg=blue
	highlight	planfacileindex		ctermfg=darkyellow	guifg=darkyellow
else
	highlight	planfaciledebug		ctermfg=black		guifg=black	ctermbg=yellow
	highlight	planfacileparametre	ctermfg=cyan		guifg=cyan
	highlight	planfacilemacro		ctermfg=cyan		guifg=cyan
	highlight	planfacileechap		ctermfg=white		guifg=white
	highlight	planfacileparamdelim	ctermfg=darkyellow	guifg=darkyellow
	highlight	planfacilecomment	ctermfg=blue		guifg=blue
	highlight	planfacilecommentaire	ctermfg=blue		guifg=blue
	highlight	planfacilestandard	ctermfg=darkgreen	guifg=darkgreen
	highlight	planfacileinclude	ctermfg=darkgreen	guifg=darkgreen
	highlight	planfacileoption	ctermfg=darkcyan	guifg=darkcyan
	highlight	planfacileoptions	ctermfg=darkred		guifg=darkred
	highlight	planfaciledefine	ctermfg=cyan		guifg=cyan
	highlight	planfacilewarning	ctermfg=red		guifg=red
	highlight	planfacileerror		ctermfg=red		guifg=red
	highlight	planfacilehead		ctermfg=magenta		guifg=magenta
	highlight	planfacilefoot		ctermfg=magenta		guifg=magenta
	highlight	planfacilemessage	ctermfg=magenta		guifg=magenta
	highlight	planfacilemesg		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfaciletitle		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacileref		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacilesec		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfaciletxt		ctermfg=darkmagenta	guifg=darkmagenta
	highlight	planfacilesection	ctermfg=magenta		guifg=magenta
	highlight	planfacilereference	ctermfg=magenta		guifg=magenta
	highlight	planfacilestart		ctermfg=magenta		guifg=magenta
	highlight	planfaciletext		ctermfg=darkred		guifg=darkred
	highlight	planfacileidea		ctermfg=darkblue	guifg=darkblue
	highlight	planfacilemissing	ctermfg=darkblue	guifg=darkblue
	highlight	planfacilegeneric	ctermfg=darkblue	guifg=darkblue
	highlight	planfaciledep		ctermfg=darkgreen	guifg=darkgreen
	highlight	planfaciledepref	ctermfg=darkcyan	guifg=darkcyan
	highlight	planfacileextref	ctermfg=darkcyan	guifg=darkcyan
	highlight	planfacileextrefs	ctermfg=darkcyan	guifg=darkcyan
	highlight	planfacileindex		ctermfg=yellow		guifg=yellow
endif
