"
" PlanFacile (Easy plan, in french) is a small tool to help people to
" write a document on a particular subject.
" Copyright (C) 2005  Julien BRUGUIER
"
" This program is free software; you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation; version 2 of the License.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, see <http://www.gnu.org/licences/>.
"

"D�finitions utilis�es en global dans le code de Planfacile

syntax keyword Constant VRAI
syntax keyword Constant FAUX

syntax keyword Constant RESULTAT_OK
syntax keyword Constant RESULTAT_ERREUR
syntax keyword Constant RESULTAT_ERREUR_DEPASSEMENT
syntax keyword Constant RESULTAT_ERREUR_DOMAINE
syntax keyword Constant RESULTAT_ERREUR_MEMOIRE
syntax keyword Constant RESULTAT_ERREUR_TAILLE
syntax keyword Constant RESULTAT_ERREUR_NONTROUVE
syntax keyword Constant RESULTAT_ERREUR_PASORPHELINES
syntax keyword Constant RESULTAT_ERREUR_TROPORPHELINES
syntax keyword Constant RESULTAT_ERREUR_PASDIDEES

syntax keyword Constant ENTREESTANDARD
syntax keyword Constant SORTIESTANDARD
syntax keyword Constant SORTIEERREUR

syntax keyword Constant IDEE_VIDE
syntax keyword Constant IDEE_PRESENTE
syntax keyword Constant IDEE_MANQUANTE
syntax keyword Constant IDEE_GENERIQUE
syntax keyword Constant IDEE_RACINE

syntax keyword Constant REFERENCE_INDEFINIE
syntax keyword Constant REFERENCE_REDUCTIBLE
syntax keyword Constant REFERENCE_IRREDUCTIBLE

syntax keyword Constant INDICE_IDEE_INVALIDE
syntax keyword Constant MACRO_NON_TROUVEE
syntax keyword Constant OPTION_NON_TROUVEE
syntax keyword Constant PREMIERE_LIGNE_FICHIER
syntax keyword Constant NIVEAU_PAR_DEFAUT

syntax keyword Constant PARCOURS_MANUEL
syntax keyword Constant PARCOURS_AUTOMATIQUE_AVANT
syntax keyword Constant PARCOURS_AUTOMATIQUE_APRES

syntax keyword Constant CONTEXTE_LOCAL
syntax keyword Constant CONTEXTE_CONTEXTE

syntax keyword Macro SECURISE
syntax keyword Macro ASSERTION

syntax keyword Macro CONTENEUR
syntax keyword Macro STOCKAGE
syntax keyword Macro COREFERENCE
syntax keyword Macro TRAVAIL
syntax keyword Macro REFERENCE
syntax keyword Macro CONTENEUR_SCALAIRE
syntax keyword Macro STOCKAGE_SCALAIRE
syntax keyword Macro COREFERENCE_SCALAIRE
syntax keyword Macro TRAVAIL_SCALAIRE
syntax keyword Macro REFERENCE_SCALAIRE
syntax keyword Macro TABLEAU
syntax keyword Macro TABLEAU_
syntax keyword Macro FONCTION
syntax keyword Macro REFFONCTION
syntax keyword Comment T_S
syntax keyword Comment S_T
syntax keyword Comment C_S
syntax keyword Comment S_C
syntax keyword Comment R_T
syntax keyword Comment T_R
syntax keyword Comment CHAMP
syntax keyword Comment CHAMP_STOCKAGE
syntax keyword Comment CHAMP_TRAVAIL
syntax keyword Comment ELEMENT
syntax keyword Comment ELEMENT_TRAVAIL
syntax keyword Comment SIZEOF
syntax keyword Comment SIZEOF_STOCKAGE
syntax keyword Comment T_S_
syntax keyword Comment S_T_
syntax keyword Comment C_S_
syntax keyword Comment S_C_
syntax keyword Comment R_T_
syntax keyword Comment T_R_
syntax keyword Comment R_S_
syntax keyword Comment S_R_
syntax keyword Comment CHAMP_
syntax keyword Comment CHAMP_STOCKAGE_
syntax keyword Comment CHAMP_TRAVAIL_
syntax keyword Comment ELEMENT_
syntax keyword Comment ELEMENT_TRAVAIL_
syntax keyword Comment SIZEOF_
syntax keyword Comment SIZEOF_STOCKAGE_
syntax keyword Comment R_F
syntax keyword Comment F_R
syntax keyword Comment CAST_S
syntax keyword Comment CAST_T
syntax keyword Comment CAST_R
syntax keyword Comment CAST_C
syntax keyword Comment CAST_S_
syntax keyword Comment CAST_T_
syntax keyword Comment CAST_R_
syntax keyword Comment CAST_C_

syntax keyword Macro TAILLE
syntax keyword Macro TAILLE_

syntax keyword cType Entier
syntax keyword cType Reel
syntax keyword cType Caractere
syntax keyword cType Chaine
syntax keyword cType Pointeur
syntax keyword cType Texte
syntax keyword cType Indice
syntax keyword cType Taille
syntax keyword cType NomFichier
syntax keyword cType DescripteurFichier
syntax keyword cType Label
syntax keyword cType Booleen

syntax keyword cType Resultat


syntax keyword Macro ALLOCATION_N
syntax keyword Macro ALLOCATION
syntax keyword Macro ALLOCATION_N_
syntax keyword Macro ALLOCATION_

syntax keyword Macro REALLOCATION
syntax keyword Macro REALLOCATION_
syntax keyword Macro REALLOCATION_CAST
syntax keyword Macro REALLOCATION_CAST_

syntax keyword Macro NOUVEAUX
syntax keyword Macro NOUVEAU
syntax keyword Macro NOUVEAUX_
syntax keyword Macro NOUVEAU_

syntax keyword Macro STRDUP
syntax keyword Macro DUP_CAST
syntax keyword Macro DUP

syntax keyword Macro STRCPY
syntax keyword Macro STRCAT
syntax keyword Macro STRSTR
syntax keyword Macro STRCMP
syntax keyword Macro STRLEN

syntax keyword Macro SNPRINTF

syntax keyword Macro FOPEN
syntax keyword Macro STAT
syntax keyword Macro GETENV


"D�finitions utilis�es de mani�re sp�cifique � Planfacile.

syntax keyword cType IdNoeud
syntax keyword cType NomNoeud
syntax keyword cType IdIdee
syntax keyword cType Pertinence
syntax keyword cType NiveauHierarchique
syntax keyword cType References
syntax keyword cType Statistiques
syntax keyword cType NomOption
syntax keyword cType NomMacro
syntax keyword cType TypeIdee
syntax keyword cType TypeReference
syntax keyword cType CompteurBoucles
syntax keyword cType Contenu
syntax keyword cType Graphe
syntax keyword cType Idee
syntax keyword cType Liens
syntax keyword cType ListeBoucles
syntax keyword cType ListeDependence
syntax keyword cType ListeIdees
syntax keyword cType ListePerePertinent
syntax keyword cType Marqueurs
syntax keyword cType Parcours
syntax keyword cType Reference
syntax keyword cType Relations
syntax keyword cType Section
syntax keyword cType Plan

syntax keyword cType FileNomFichier

syntax keyword cType Commande
syntax keyword cType TypeCommande
syntax keyword cType CommandeCommentaire
syntax keyword cType CommandeDefine
syntax keyword cType CommandeDep
syntax keyword cType CommandeDepRef
syntax keyword cType CommandeEchappement
syntax keyword cType CommandeError
syntax keyword cType CommandeExtRef
syntax keyword cType CommandeExtRefs
syntax keyword cType CommandeFoot
syntax keyword cType CommandeGeneric
syntax keyword cType CommandeHead
syntax keyword cType CommandeIdea
syntax keyword cType CommandeInclude
syntax keyword cType CommandeIndex
syntax keyword cType CommandeMacro
syntax keyword cType CommandeMacroParametres
syntax keyword cType CommandeMesg
syntax keyword cType CommandeMessage
syntax keyword cType CommandeMissing
syntax keyword cType CommandeOption
syntax keyword cType CommandeOptions
syntax keyword cType CommandeOptionsClause
syntax keyword cType CommandeOptionsClauses
syntax keyword cType CommandeParametre
syntax keyword cType CommandeRef
syntax keyword cType CommandeReference
syntax keyword cType CommandeSec
syntax keyword cType CommandeSection
syntax keyword cType CommandeStandard
syntax keyword cType CommandeStart
syntax keyword cType CommandeTexte
syntax keyword cType CommandeTitle
syntax keyword cType CommandeTxt
syntax keyword cType CommandeWarning
syntax keyword cType Flux
syntax keyword cType TypeFlux
syntax keyword cType ActionCommande
syntax keyword cType TypeParcours
syntax keyword cType ActionFlux
syntax keyword cType PileContexte
syntax keyword cType ProcessusFlux
syntax keyword cType Processus
syntax keyword cType TypeContexte
syntax keyword cType LocalisationFichier
syntax keyword cType DescriptionFichier
syntax keyword cType PositionFichier
syntax keyword cType Option
syntax keyword cType NomOption
syntax keyword cType Macro
syntax keyword cType NomMacro
syntax keyword cType TypeMacroAjout
syntax keyword cType TypeMacroProbleme
syntax keyword cType NomFichier
syntax keyword cType General
syntax keyword cType Message
syntax keyword cType Langue
syntax keyword cType Charset
syntax keyword cType MessageParametres
syntax keyword cType Probleme
syntax keyword cType Verbeux
syntax keyword cType Environnement
syntax keyword cType Options
syntax keyword cType PileChaine
syntax keyword cType PileEntier
syntax keyword cType PileNomFichier
syntax keyword cType PileNomMacro
syntax keyword cType PileDefinitionMacro
syntax keyword cType PileAppelMacro
syntax keyword cType PileLocalisationFichier
syntax keyword cType PileFlux
syntax keyword cType Retouches
syntax keyword cType ReductionOptions
syntax keyword cType ReductionMacros
syntax keyword cType Verification
syntax keyword cType Styles
syntax keyword cType EnregistrementStyles
syntax keyword cType Idees
syntax keyword cType EnregistrementIdees
syntax keyword cType DenominationIdee
syntax keyword cType ArbreIdees
syntax keyword cType ReferencesIdee
syntax keyword cType ListeReferences
syntax keyword cType PerformancesPlan
syntax keyword cType CalculPlan
syntax keyword cType Sortie



syntax keyword Macro LOCALISATION_PROBLEME_PARSE

syntax keyword Macro LOCALISATION_PROBLEME
syntax keyword Macro LOCALISATION_NULLE_PROBLEME
syntax keyword Macro LOCALISATION_COMMANDE 

syntax keyword Macro AJOUT_LOCALISATION
syntax keyword Macro LIRE_LOCALISATION
syntax keyword Macro LOCALISATION_ECHANGE
syntax keyword Macro DETRUIRE_DERNIERE_LOCALISATION

syntax keyword Macro AJOUT_TEXTE
syntax keyword Macro LIRE_TEXTE

syntax keyword Macro AJOUT_PARAMETRE
syntax keyword Macro LIRE_PARAMETRE

syntax keyword Macro AJOUT_MACRO
syntax keyword Macro LIRE_MACRO

syntax keyword Macro MACRO_ECHANGE
syntax keyword Macro MACRO_DEFINIR_NIVEAU
syntax keyword Macro MACRO_APPELS_SUCCESSIFS

syntax keyword Macro DEF_ERREUR_SYNTAXE

syntax keyword Macro MODIFICATION_POSITION

syntax keyword Macro LECTURE_OPTION_BOOLEEN
syntax keyword Macro LECTURE_OPTION_ENTIER

syntax keyword Macro CHANGEMENT_OPTION_BOOLEEN
syntax keyword Macro CHANGEMENT_OPTION_ENTIER


"D�finitions utilis�es uniquement dans l'analyseur syntaxique de Planfacile.

syntax keyword Macro DEFINIR_COMMANDE
syntax keyword Macro DEFINIR_COMMANDE_ECHAPPEMENT

syntax keyword Macro EMPILE_FLUX
syntax keyword Macro DEPILE_FLUX
syntax keyword Macro TEST_PILE_FLUX

syntax match cType "_Entier"
syntax match cType "_Reel"
syntax match cType "_Caractere"
syntax match cType "_Chaine"
syntax match cType "_Pointeur"
syntax match cType "_Texte"
syntax match cType "_Indice"
syntax match cType "_Taille"
syntax match cType "_NomFichier"
syntax match cType "_DescripteurFichier"
syntax match cType "_Label"
syntax match cType "_Booleen"

syntax match cType "_Resultat"

syntax match cType "_IdNoeud"
syntax match cType "_NomNoeud"
syntax match cType "_IdIdee"
syntax match cType "_Pertinence"
syntax match cType "_NiveauHierarchique"
syntax match cType "_References"
syntax match cType "_Statistiques"
syntax match cType "_NomOption"
syntax match cType "_NomMacro"
syntax match cType "_TypeIdee"

syntax match cType "_FileNomFichier"

syntax match cType "_Commande"
syntax match cType "_TypeCommande"
syntax match cType "_CommandeCommentaire"
syntax match cType "_CommandeDefine"
syntax match cType "_CommandeDep"
syntax match cType "_CommandeDepRef"
syntax match cType "_CommandeEchappement"
syntax match cType "_CommandeError"
syntax match cType "_CommandeExtRef"
syntax match cType "_CommandeExtRefs"
syntax match cType "_CommandeFoot"
syntax match cType "_CommandeGeneric"
syntax match cType "_CommandeHead"
syntax match cType "_CommandeIdea"
syntax match cType "_CommandeInclude"
syntax match cType "_CommandeIndex"
syntax match cType "_CommandeMacro"
syntax match cType "_CommandeMacroParametres"
syntax match cType "_CommandeMesg"
syntax match cType "_CommandeMessage"
syntax match cType "_CommandeMissing"
syntax match cType "_CommandeOption"
syntax match cType "_CommandeOptions"
syntax match cType "_CommandeOptionsClause"
syntax match cType "_CommandeOptionsClauses"
syntax match cType "_CommandeParametre"
syntax match cType "_CommandeRef"
syntax match cType "_CommandeReference"
syntax match cType "_CommandeSec"
syntax match cType "_CommandeSection"
syntax match cType "_CommandeStandard"
syntax match cType "_CommandeStart"
syntax match cType "_CommandeTexte"
syntax match cType "_CommandeTitle"
syntax match cType "_CommandeTxt"
syntax match cType "_CommandeWarning"
syntax match cType "_Flux"
syntax match cType "_TypeFlux"
syntax match cType "_ActionCommande"
syntax match cType "_TypeParcours"
syntax match cType "_ActionFlux"
syntax match cType "_PileContexte"
syntax match cType "_ProcessusFlux"
syntax match cType "_TypeContexte"
syntax match cType "_Processus"
syntax match cType "_LocalisationFichier"
syntax match cType "_PositionFichier"
syntax match cType "_Option"
syntax match cType "_Macro"
syntax match cType "_NomMacro"
syntax match cType "_NomOption"
syntax match cType "_NomFichier"
syntax match cType "_General"
syntax match cType "_Message"
syntax match cType "_Langue"
syntax match cType "_Charset"
syntax match cType "_MessageParametres"
syntax match cType "_Probleme"
syntax match cType "_Verbeux"
syntax match cType "_Environnement"
syntax match cType "_Options"
syntax match cType "_PileChaine"
syntax match cType "_PileEntier"
syntax match cType "_PileNomFichier"
syntax match cType "_PileNomMacro"
syntax match cType "_PileDefinitionMacro"
syntax match cType "_PileAppelMacro"
syntax match cType "_PileLocalisationFichier"
syntax match cType "_Retouches"
syntax match cType "_ReductionOptions"
syntax match cType "_ReductionMacros"
syntax match cType "_Verification"
syntax match cType "_Styles"
syntax match cType "_EnregistrementStyles"
syntax match cType "_Idees"
syntax match cType "_EnregistrementIdees"
syntax match cType "_DenominationIdee"
syntax match cType "_ArbreIdees"
syntax match cType "_ReferencesIdee"
syntax match cType "_ListeReferences"
syntax match cType "_PerformancesPlan"
syntax match cType "_CalculPlan"
syntax match cType "_Sortie"

"Constantes utilis�es dans Planfacile

syntax keyword Constant COMMANDE_VIDE
syntax keyword Constant COMMANDE_COMMENTAIRE
syntax keyword Constant COMMANDE_ECHAPPEMENT
syntax keyword Constant COMMANDE_TEXTE
syntax keyword Constant COMMANDE_WARNING
syntax keyword Constant COMMANDE_ERROR
syntax keyword Constant COMMANDE_INCLUDE
syntax keyword Constant COMMANDE_STANDARD
syntax keyword Constant COMMANDE_OPTION
syntax keyword Constant COMMANDE_OPTIONS
syntax keyword Constant COMMANDE_DEFINE
syntax keyword Constant COMMANDE_PARAMETRE
syntax keyword Constant COMMANDE_MACRO
syntax keyword Constant COMMANDE_MESSAGE
syntax keyword Constant COMMANDE_MESG
syntax keyword Constant COMMANDE_HEAD
syntax keyword Constant COMMANDE_FOOT
syntax keyword Constant COMMANDE_SECTION
syntax keyword Constant COMMANDE_REFERENCE
syntax keyword Constant COMMANDE_TITLE
syntax keyword Constant COMMANDE_REF
syntax keyword Constant COMMANDE_SEC
syntax keyword Constant COMMANDE_TXT
syntax keyword Constant COMMANDE_START
syntax keyword Constant COMMANDE_IDEA
syntax keyword Constant COMMANDE_MISSING
syntax keyword Constant COMMANDE_GENERIC
syntax keyword Constant COMMANDE_DEP
syntax keyword Constant COMMANDE_DEPREF
syntax keyword Constant COMMANDE_EXTREFS
syntax keyword Constant COMMANDE_EXTREF
syntax keyword Constant COMMANDE_INDEX
syntax keyword Constant FLUX_VIDE
syntax keyword Constant FLUX_PRINCIPAL
syntax keyword Constant FLUX_COMMENTAIRE
syntax keyword Constant FLUX_COMPILATEUR
syntax keyword Constant FLUX_OPTIONS
syntax keyword Constant FLUX_MACRO_DEFINITION
syntax keyword Constant FLUX_MACRO_PARAMETRE
syntax keyword Constant FLUX_MESSAGE
syntax keyword Constant FLUX_DOCUMENT
syntax keyword Constant FLUX_NIVEAU
syntax keyword Constant FLUX_PERTINENCE
syntax keyword Constant FLUX_SECTION_NOM
syntax keyword Constant FLUX_SECTION_FORMAT
syntax keyword Constant FLUX_SECTION_SECTION
syntax keyword Constant FLUX_REFERENCE_FORMAT
syntax keyword Constant FLUX_REFERENCE
syntax keyword Constant FLUX_REFERENCE_AUTOMATIQUE
syntax keyword Constant FLUX_TITRE
syntax keyword Constant FLUX_TITRE_AUTOMATIQUE
syntax keyword Constant FLUX_TEXTE
syntax keyword Constant FLUX_TEXTE_REDUCTIBLE
syntax keyword Constant FLUX_TEXTE_IRREDUCTIBLE
syntax keyword Constant FLUX_TEXTE_MANQUANTE
syntax keyword Constant FLUX_TEXTE_GENERIQUE
syntax keyword Constant FLUX_INDICE
syntax keyword Constant PROBLEME_MEMOIRE_INSUFFISANTE
syntax keyword Constant PROBLEME_OPTION_LIGNE_COMMANDE
syntax keyword Constant PROBLEME_RECURSIVITE_FICHIER
syntax keyword Constant PROBLEME_RECURSIVITE_FICHIER_STANDARD
syntax keyword Constant PROBLEME_UTILISATION_SORTIE
syntax keyword Constant PROBLEME_OUVERTURE_SORTIE
syntax keyword Constant PROBLEME_OUVERTURE_REPERTOIRE_COURANT
syntax keyword Constant PROBLEME_OUVERTURE_CHEMIN_INVALIDE
syntax keyword Constant PROBLEME_OUVERTURE_CHEMIN_INVALIDE_STANDARD
syntax keyword Constant PROBLEME_OUVERTURE_CHEMIN_INVALIDE_TEMPLATE
syntax keyword Constant PROBLEME_OUVERTURE_INEXISTANT
syntax keyword Constant PROBLEME_OUVERTURE_INEXISTANT_STANDARD
syntax keyword Constant PROBLEME_OUVERTURE_INEXISTANT_TEMPLATE
syntax keyword Constant PROBLEME_OUVERTURE_TYPE_INCORRECT
syntax keyword Constant PROBLEME_OUVERTURE_TYPE_INCORRECT_STANDARD
syntax keyword Constant PROBLEME_OUVERTURE_TYPE_INCORRECT_TEMPLATE
syntax keyword Constant PROBLEME_OUVERTURE_ENTREE
syntax keyword Constant PROBLEME_OUVERTURE_ENTREE_VIDE
syntax keyword Constant PROBLEME_OUVERTURE_ENTREE_STANDARD
syntax keyword Constant PROBLEME_OUVERTURE_ENTREE_TEMPLATE
syntax keyword Constant PROBLEME_OUVERTURE_STANDARD
syntax keyword Constant PROBLEME_OUVERTURE_TEMPLATE
syntax keyword Constant PROBLEME_OUVERTURE_STANDARD_SUIVANTE
syntax keyword Constant PROBLEME_OUVERTURE_TEMPLATE_SUIVANTE
syntax keyword Constant PROBLEME_AVERTISSEMENT_UTILISATEUR
syntax keyword Constant PROBLEME_ERREUR_UTILISATEUR
syntax keyword Constant PROBLEME_COMMANDE_INCORRECTE
syntax keyword Constant PROBLEME_COMMANDE_TEXTE_INCORRECTE
syntax keyword Constant PROBLEME_COMMANDE_ECHAPPEMENT_INCORRECTE
syntax keyword Constant PROBLEME_COMMANDE_MACRO_INCORRECTE
syntax keyword Constant PROBLEME_COMMANDE_PARAMETRE_INCORRECTE
syntax keyword Constant PROBLEME_REDEFINITION_MACRO
syntax keyword Constant PROBLEME_RECURSIVITE_MACRO
syntax keyword Constant PROBLEME_MACRO_INCONNUE
syntax keyword Constant PROBLEME_PARAMETRES_MACRO
syntax keyword Constant PROBLEME_OPTION_DANS_MACRO
syntax keyword Constant PROBLEME_REDEFINITION_SECTION
syntax keyword Constant PROBLEME_REDEFINITION_SECTION_DEFAUT
syntax keyword Constant PROBLEME_REDEFINITION_REFERENCE
syntax keyword Constant PROBLEME_REDEFINITION_REFERENCE_DEFAUT
syntax keyword Constant PROBLEME_REDEFINITION_MESSAGE
syntax keyword Constant PROBLEME_REDEFINITION_ENTETE
syntax keyword Constant PROBLEME_REDEFINITION_PIED
syntax keyword Constant PROBLEME_REDEFINITION_RACINE
syntax keyword Constant PROBLEME_SECTION_NIVEAU_INCORRECT
syntax keyword Constant PROBLEME_REFERENCE_NIVEAU_INCORRECT
syntax keyword Constant PROBLEME_RACINE_NIVEAU_INCORRECT
syntax keyword Constant PROBLEME_RACINE_NIVEAU_NON_DEFINI
syntax keyword Constant PROBLEME_REDEFINITION_IDEE
syntax keyword Constant PROBLEME_IDEE_REFERENCE_VIDE
syntax keyword Constant PROBLEME_IDEE_MANQUANTE_REFERENCE_VIDE
syntax keyword Constant PROBLEME_IDEE_GENERIQUE_REFERENCE_VIDE
syntax keyword Constant PROBLEME_PERTINENCE_INCORRECTE
syntax keyword Constant PROBLEME_INDICE_IDEE_MANQUANTE_INCORRECT
syntax keyword Constant PROBLEME_REDEFINITION_IDEE_MANQUANTE_DEFAUT
syntax keyword Constant PROBLEME_REDEFINITION_IDEE_MANQUANTE
syntax keyword Constant PROBLEME_INDICE_IDEE_GENERIQUE_INCORRECT
syntax keyword Constant PROBLEME_REDEFINITION_IDEE_GENERIQUE_DEFAUT
syntax keyword Constant PROBLEME_REDEFINITION_IDEE_GENERIQUE
syntax keyword Constant PROBLEME_DEPENDANCE_INUTILE
syntax keyword Constant PROBLEME_DEPENDANCE_VIDE_INUTILE
syntax keyword Constant PROBLEME_SANS_IDEE_ORPHELINE
syntax keyword Constant PROBLEME_AJOUT_RACINE
syntax keyword Constant PROBLEME_IDEE_NON_ATTEIGNABLE
syntax keyword Constant PROBLEME_GENERATION_REFERENCE_MANQUANTE
syntax keyword Constant PROBLEME_GENERATION_REFERENCE_GENERIQUE
syntax keyword Constant PROBLEME_SECTION_STYLE_MANQUANT
syntax keyword Constant PROBLEME_REFERENCE_STYLE_MANQUANT
syntax keyword Constant PROBLEME_IDEE_MANQUANTE_MANQUANTE
syntax keyword Constant PROBLEME_IDEE_GENERIQUE_MANQUANTE
syntax keyword Constant PROBLEME_DEPENDANCE_INCORRECTE
syntax keyword Constant PROBLEME_DEPENDANCE_VIDE
syntax keyword Constant PROBLEME_INDICE_REFERENCE_INCORRECT
syntax keyword Constant PROBLEME_NOM_NIVEAU_INCORRECT
syntax keyword Constant PROBLEME_ERREUR_SYNTAXE
syntax match Constant "LANGUE_[A-Z_]\+"
syntax match Constant "CHARSET_[A-Z_]\+"
syntax match Constant "MESSAGE_[A-Z_]\+"
syntax keyword Constant MACROAJOUT_INTERDIT
syntax keyword Constant MACROAJOUT_REMPLACE
syntax keyword Constant MACROAJOUT_CONSERVE
syntax keyword Constant MACROPROBLEME_VERBEUX
syntax keyword Constant MACROPROBLEME_SILENCIEUX
syntax keyword Constant VERBEUX_ANALYSE
syntax keyword Constant VERBEUX_ANALYSE_SOURCE
syntax keyword Constant VERBEUX_ANALYSE_FINSOURCE
syntax keyword Constant VERBEUX_ANALYSE_INCLUSION
syntax keyword Constant VERBEUX_ANALYSE_FININCLUSION
syntax keyword Constant VERBEUX_RETOUCHES
syntax keyword Constant VERBEUX_RETOUCHES_PARAMETRE
syntax keyword Constant VERBEUX_RETOUCHES_MACRO_NORMALE
syntax keyword Constant VERBEUX_RETOUCHES_MACRO_BOUCLE
syntax keyword Constant VERBEUX_RETOUCHES_DEFRAGMENTATION
syntax keyword Constant VERBEUX_RETOUCHES_INCLUDE
syntax keyword Constant VERBEUX_RETOUCHES_STANDARD
syntax keyword Constant VERBEUX_OPTIONS
syntax keyword Constant VERBEUX_OPTIONS_RECHERCHE
syntax keyword Constant VERBEUX_OPTIONS_RECHERCHE_PREMIERE
syntax keyword Constant VERBEUX_OPTIONS_RECHERCHE_NOUVELLE
syntax keyword Constant VERBEUX_OPTIONS_RECHERCHE_FIN
syntax keyword Constant VERBEUX_OPTIONS_ENREGISTREMENT
syntax keyword Constant VERBEUX_OPTIONS_COMMENTAIRE
syntax keyword Constant VERBEUX_OPTIONS_MACRO
syntax keyword Constant VERBEUX_OPTIONS_REDUCTION
syntax keyword Constant VERBEUX_OPTIONS_REDUCTION_OPTIONS
syntax keyword Constant VERBEUX_MACROS
syntax keyword Constant VERBEUX_MACROS_NORMALE
syntax keyword Constant VERBEUX_MACROS_BOUCLE
syntax keyword Constant VERBEUX_MACROS_PARAMETRE
syntax keyword Constant VERBEUX_MACROS_ITERATION
syntax keyword Constant VERBEUX_VERIFICATION
syntax keyword Constant VERBEUX_VERIFICATION_DEFRAGMENTATION
syntax keyword Constant VERBEUX_VERIFICATION_ERREUR
syntax keyword Constant VERBEUX_VERIFICATION_AVERTISSEMENT
syntax keyword Constant VERBEUX_STYLES
syntax keyword Constant VERBEUX_STYLES_SECTION
syntax keyword Constant VERBEUX_STYLES_SECTION_DEFAUT
syntax keyword Constant VERBEUX_STYLES_SECTION_EXPLICITE
syntax keyword Constant VERBEUX_STYLES_AUTRES
syntax keyword Constant VERBEUX_STYLES_REFERENCE_DEFAUT
syntax keyword Constant VERBEUX_STYLES_REFERENCE_EXPLICITE
syntax keyword Constant VERBEUX_STYLES_MESSAGE
syntax keyword Constant VERBEUX_STYLES_ENTETE
syntax keyword Constant VERBEUX_STYLES_PIED
syntax keyword Constant VERBEUX_STYLES_RACINE
syntax keyword Constant VERBEUX_IDEES
syntax keyword Constant VERBEUX_IDEES_PRESENTES
syntax keyword Constant VERBEUX_IDEES_IDEE
syntax keyword Constant VERBEUX_IDEES_DEPENDANCE
syntax keyword Constant VERBEUX_IDEES_AUTOMATIQUES
syntax keyword Constant VERBEUX_IDEES_MANQUANTE_DEFAUT
syntax keyword Constant VERBEUX_IDEES_MANQUANTE_EXPLICITE
syntax keyword Constant VERBEUX_IDEES_GENERIQUE_DEFAUT
syntax keyword Constant VERBEUX_IDEES_GENERIQUE_EXPLICITE
syntax keyword Constant VERBEUX_PLAN
syntax keyword Constant VERBEUX_PLAN_GRAPHE
syntax keyword Constant VERBEUX_PLAN_GRAPHE_VIDE
syntax keyword Constant VERBEUX_PLAN_GRAPHE_IDEES
syntax keyword Constant VERBEUX_PLAN_GRAPHE_DEPENDANCES
syntax keyword Constant VERBEUX_PLAN_RACINE
syntax keyword Constant VERBEUX_PLAN_ATTEIGNABLES
syntax keyword Constant VERBEUX_PLAN_CALCUL
syntax keyword Constant VERBEUX_PLAN_TRANSFORMATION
syntax keyword Constant VERBEUX_PLAN_TRANSFORMATION_IDEE
syntax keyword Constant VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE
syntax keyword Constant VERBEUX_PLAN_TRANSFORMATION_REFERENCE
syntax keyword Constant VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE
syntax keyword Constant VERBEUX_SORTIE
syntax keyword Constant VERBEUX_SORTIE_GENERATION
syntax keyword Constant VERBEUX_SORTIE_GENERATION_ENTETE
syntax keyword Constant VERBEUX_SORTIE_GENERATION_IDEE
syntax keyword Constant VERBEUX_SORTIE_GENERATION_IDEEVIDE
syntax keyword Constant VERBEUX_SORTIE_GENERATION_REFIRR
syntax keyword Constant VERBEUX_SORTIE_GENERATION_REFIRRVIDE
syntax keyword Constant VERBEUX_SORTIE_GENERATION_DEPREF
syntax keyword Constant VERBEUX_SORTIE_GENERATION_REFRED
syntax keyword Constant VERBEUX_SORTIE_GENERATION_REFREDVIDE
syntax keyword Constant VERBEUX_SORTIE_GENERATION_EXTREF
syntax keyword Constant VERBEUX_SORTIE_GENERATION_EXTREFS
syntax keyword Constant VERBEUX_SORTIE_GENERATION_PIED
syntax keyword Constant VERBEUX_SORTIE_PRODUCTION
syntax keyword Constant VERBEUX_SORTIE_PRODUCTION_DOCUMENT
syntax keyword Constant VERBEUX_SORTIE_PRODUCTION_STATS
