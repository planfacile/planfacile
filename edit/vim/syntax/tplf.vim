"
" PlanFacile (Easy plan, in french) is a small tool to help people to
" write a document on a particular subject.
" Copyright (C) 2005  Julien BRUGUIER
"
" This program is free software; you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation; version 2 of the License.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, see <http://www.gnu.org/licences/>.
"

syntax match Statement 	"Verbeux 1 :"
syntax match Constant 	"Verbeux 2 :"
syntax match Identifier	"Verbeux 3 :"
syntax match PreProc 	"Verbeux 4 :"
syntax match Comment 	"Verbeux 5 :"
syntax match Error	"Attention :"
syntax match Error	"Erreur    :"

syntax match Statement 	"Verbose 1 :"
syntax match Constant 	"Verbose 2 :"
syntax match Identifier	"Verbose 3 :"
syntax match PreProc 	"Verbose 4 :"
syntax match Comment 	"Verbose 5 :"
syntax match Error	"Warning   :"
syntax match Error	"Error     :"
