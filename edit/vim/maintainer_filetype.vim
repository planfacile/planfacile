if has("autocmd")
	augroup filetypedetect
	au BufRead,BufNewFile *.plf setf plf
	au BufRead,BufNewFile *.plf compiler planfacile
	augroup END
endif

if has("autocmd")
	augroup filetypedetect
	au BufRead,BufNewFile *.bis setf bison
	augroup END
endif

if has("autocmd")
	augroup filetypedetect
	au BufRead,BufNewFile *.plflg setf plflg
	augroup END
endif

if has("autocmd")
	augroup filetypedetect
	au BufRead,BufNewFile *.tplf setf tplf
	augroup END
endif
