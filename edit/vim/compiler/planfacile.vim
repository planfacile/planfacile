"
" PlanFacile (Easy plan, in french) is a small tool to help people to
" write a document on a particular subject.
" Copyright (C) 2005  Julien BRUGUIER
"
" This program is free software; you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation; version 2 of the License.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, see <http://www.gnu.org/licences/>.
"

if exists("current_compiler")
  finish
endif
let current_compiler = "planfacile"

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet errorformat=%EErreur%*[\ ]:\ %m\\,,%WAttention%*[\ ]:\ %m\\,,%Z%*[\ ]dans\ le\ fichier\ %f\ %*[^\ ]\ la\ ligne\ %l.,%C%*[\ ]dans\ le\ fichier\ %f\ %*[^\ ]\ la\ ligne\ %l\\,,%Z%*[\ ]inclus\ depuis\ le\ fichier\ %f\ %*[^\ ]\ la\ ligne\ %l.,%C%*[\ ]inclus\ depuis\ le\ fichier\ %f\ %*[^\ ]\ la\ ligne\ %l\\,,%-GVerbeux%m
