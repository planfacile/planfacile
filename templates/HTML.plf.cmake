#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER
 Copyright (C) 2014  Sylvain Plantefève

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#option{HTML}

#options{Langue}
#case{fr}
        #option{icone}
        #define{typeicone}{png}
        #define{icone}{@TEMPLATE_DIR@/HTML/favicon}

        #option{style}
        #define{style}{@TEMPLATE_DIR@/HTML/style.css}
#case{en}
        #option{Icon}
        #define{IconType}{png}
        #define{Icon}{@TEMPLATE_DIR@/HTML/style.css}

        #option{Style}
        #define{Style}{@TEMPLATE_DIR@/HTML/style.css}
#other
	#warning{Langue non specifiee. Utilise le francais par defaut}
	#option{fr}
#end
