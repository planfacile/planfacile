#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER
 Copyright (C) 2014  Sylvain Plantefève

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#option{LaTeX}

#options{Langue}
#case{fr}
        #option{rapport}
        #option{11pt}
        #option{page de garde}
        #option{table des matières}
#case{en}
        #option{Report}
        #option{11pt}
        #option{Front page}
        #option{Table of contents}
#other
	#warning{Langue non specifiee. Utilise le francais par defaut}
	#option{fr}
#end
