/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *  Copyright (C) 2013  Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __AIDEENVIRONNEMENT__
#define __AIDEENVIRONNEMENT__

#include <src/global/global.h>
#include <src/analyseur/donnees/pilenomfichier.h>


Resultat aideenvironnement_lecture_repertoiredonnees(REFERENCE_SCALAIRE(NomFichier) repertoiredonnees);
/* Renvoie dans repertoiredonnees le chemin vers le répertoire
 * des donnees utilisateur de PlanFacile.
 * Alloue directement la chaine, qui devra être libérée
 * avec free().
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

Resultat aideenvironnement_lecture_repertoiretemplatesutilisateur(REFERENCE_SCALAIRE(NomFichier) repertoiretemplatesutilisateur);
/* Renvoie dans repertoiretemplatesutilisateur le chemin vers le répertoire
 * contenant les templates utilisateur.
 * Alloue directement la chaîne, qui devra être libérée avec free().
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

Resultat aideenvironnement_lecture_fichierstandardutilisateur(REFERENCE_SCALAIRE(NomFichier) fichierstandardutilisateur);
/* Renvoie dans fichierstandardutilisateur le chemin vers les définitions
 * standards de l'utilisateur.
 * Alloue directement la chaîne, qui devra être libérée avec free().
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

Resultat aideenvironnement_lecture_repertoirecourant(REFERENCE_SCALAIRE(NomFichier) repertoirecourant);
/* Renvoie le nom du répertoire courant.
 * Alloue directement la chaine, qui devra être libérée
 * avec free().
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

Resultat aideenvironnement_lecture_repertoirefichier(TRAVAIL_SCALAIRE(NomFichier) repertoirefichier, REFERENCE_SCALAIRE(NomFichier) repertoire, REFERENCE_SCALAIRE(NomFichier) fichier);
/* Renvoie la partie répertoire d'un nom de fichier
 * dans une chaine automatiquement allouée. Il faudra
 * libérer cette chaine par free(). De même pour le
 * nom de fichier.
 * Renvoie RESULTAT_ERREUR si fichier est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

Resultat aideenvironnement_ajout_repertoirefichier(TRAVAIL(PileNomFichier) pilerepertoire, TRAVAIL_SCALAIRE(NomFichier) fichier, REFERENCE_SCALAIRE(NomFichier) fichierabsolu, REFERENCE_SCALAIRE(Booleen) chrep);
/* Prépare l'inclusion ou l'ouverture d'un fichier,
 * en se plaçant dans le répertoire du fichier,
 * et en renvoyant le nom du fichier avec son chemin
 * absolu, en maintenant la pile de repertoires
 * donnee en parametre.
 * Le booléen indique si l'on a bien pu changer de
 * répertoire.
 */

Resultat aideenvironnement_retrait_repertoirefichier(TRAVAIL(PileNomFichier) pilerepertoire);
/* Effectue les opérations sur le répertoire courant
 * lors de la fin de la lecture d'un fichier source.
 */

Resultat aideenvironnement_lecture_date(REFERENCE_SCALAIRE(Chaine) date, REFERENCE_SCALAIRE(Chaine) heure);
/* Renvoie la date et l'heure sous forme de chaines formattées de
 * caractères, qu'il faudra libérer avec free().
 * Retourne RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
 */

#endif
