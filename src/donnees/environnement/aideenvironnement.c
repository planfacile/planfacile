/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *  Copyright (C) 2013  Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#include "aideenvironnement.h"
#include <time.h>

#define REPTAILLEINIT                   128
#define REPMULTTAILLE                   2

#define CARACTEREREPERTOIRE             '/'
#define CARACTEREECHAPPEMENT            '\\'
#define TEMPS_INCONNU                   "<?>"

#define REPERTOIREUTILISATEUR           "~/"
#define VARENVREPERTOIREUTILISATEUR     "HOME"

/* Pour la recherche des données utilisateur :
 *         * Le répertoire des données utilisateur est donné par la
 *           variable d'environnement indiquée par VARENVRACINEDONNEES ;
 *         * Si la variable d'environnement n'est pas définie ou vide,
 *           le chemin utilisé est alors indiqué par VALEURDEFAUTRACINEDONNEES ;
 */
#define VARENVRACINEDONNEES             "XDG_DATA_HOME"
#define VALEURDEFAUTRACINEDONNEES       "~/.local/share"
// http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html#variables

#define CHEMIN_UTILISATEUR_STANDARD     "/standard/standard.plf"
#define CHEMIN_UTILISATEUR_TEMPLATES    "/templates/"


Resultat aideenvironnement_lecture_repertoiredonnees(REFERENCE_SCALAIRE(NomFichier) repertoiredonnees)
{
        /* Renvoie le nom du répertoire des donnees utilisateur de PlanFacile.
         * Alloue directement la chaine, qui devra être libérée avec free().
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        TRAVAIL_SCALAIRE(NomFichier) repertoireracinedonnees;
        T_R_(repertoiredonnees)=T_S_(NULL);
        if((S_T_(repertoireracinedonnees)=CAST_S_(NomFichier, GETENV(T_S_(VARENVRACINEDONNEES))))==NULL
           || STRLEN(repertoireracinedonnees)==T_S_(0))
        {
                // Variable d'environnement non définie ou vide : utilisation de la valeur par défaut.
                repertoireracinedonnees=T_S_(VALEURDEFAUTRACINEDONNEES);
        }
        S_T_(T_R_(repertoiredonnees))=CAST_S_(NomFichier, ALLOCATION_N_(Caractere, STRLEN(repertoireracinedonnees)+STRLEN(T_S_(SOFTWARE_NAME))+T_S_(2)));
        if(T_R_(repertoiredonnees)==T_S_(NULL))
        {
                free(S_T_(repertoireracinedonnees));
                return RESULTAT_ERREUR_MEMOIRE;
        }
        STRCPY(T_R_(repertoiredonnees), repertoireracinedonnees);
        STRCAT(T_R_(repertoiredonnees), T_S_("/"));
        STRCAT(T_R_(repertoiredonnees), T_S_(SOFTWARE_NAME));
        return RESULTAT_OK;
}

Resultat aideenvironnement_lecture_repertoiretemplatesutilisateur(REFERENCE_SCALAIRE(NomFichier) repertoiretemplatesutilisateur)
{
        /* Renvoie dans repertoiretemplatesutilisateur le chemin vers le répertoire
         * contenant les templates utilisateur.
         * Alloue directement la chaîne, qui devra être libérée avec free().
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        TRAVAIL_SCALAIRE(NomFichier) repertoiredonnees;
        T_R_(repertoiretemplatesutilisateur)=T_S_(NULL);
        SECURISE(aideenvironnement_lecture_repertoiredonnees(R_T_(repertoiredonnees)));
        S_T_(T_R_(repertoiretemplatesutilisateur))=CAST_S_(NomFichier, ALLOCATION_N_(Caractere, STRLEN(repertoiredonnees)+STRLEN(T_S_(CHEMIN_UTILISATEUR_TEMPLATES))+T_S_(1)));
        if(T_R_(repertoiretemplatesutilisateur)==T_S_(NULL))
        {
                free(S_T_(repertoiredonnees));
                return RESULTAT_ERREUR_MEMOIRE;
        }
        STRCPY(T_R_(repertoiretemplatesutilisateur), repertoiredonnees);
        STRCAT(T_R_(repertoiretemplatesutilisateur), T_S_(CHEMIN_UTILISATEUR_TEMPLATES));
        free(S_T_(repertoiredonnees));
        return RESULTAT_OK;
}

Resultat aideenvironnement_lecture_fichierstandardutilisateur(REFERENCE_SCALAIRE(NomFichier) fichierstandardutilisateur)
{
        /* Renvoie dans fichierstandardutilisateur le chemin vers les définitions
         * standards de l'utilisateur.
         * Alloue directement la chaîne, qui devra être libérée avec free().
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        TRAVAIL_SCALAIRE(NomFichier) repertoiredonnees;
        T_R_(fichierstandardutilisateur)=T_S_(NULL);
        SECURISE(aideenvironnement_lecture_repertoiredonnees(R_T_(repertoiredonnees)));
        S_T_(T_R_(fichierstandardutilisateur))=CAST_S_(NomFichier, ALLOCATION_N_(Caractere, STRLEN(repertoiredonnees)+STRLEN(T_S_(CHEMIN_UTILISATEUR_STANDARD))+T_S_(1)));
        if(T_R_(fichierstandardutilisateur)==T_S_(NULL))
        {
                free(S_T_(repertoiredonnees));
                return RESULTAT_ERREUR_MEMOIRE;
        }
        STRCPY(T_R_(fichierstandardutilisateur), repertoiredonnees);
        STRCAT(T_R_(fichierstandardutilisateur), T_S_(CHEMIN_UTILISATEUR_STANDARD));
        free(S_T_(repertoiredonnees));
        return RESULTAT_OK;
}

Resultat aideenvironnement_lecture_repertoirecourant(REFERENCE_SCALAIRE(NomFichier) repertoirecourant)
{
        /* Renvoie le nom du répertoire courant.
         * Alloue directement la chaine, qui devra être libérée
         * avec free().
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        STOCKAGE_SCALAIRE(NomFichier) repertoire;
        TRAVAIL_SCALAIRE(Taille) taille;
        T_R_(repertoirecourant)=T_S_(NULL);
        repertoire=CAST_S_(NomFichier, ALLOCATION_N_(Caractere, T_S_(REPTAILLEINIT)));
        ASSERTION(repertoire!=NULL, RESULTAT_ERREUR_MEMOIRE);

        taille=T_S_(REPTAILLEINIT);
        for(;;)
        {
                STOCKAGE_SCALAIRE(NomFichier) repertoiretemp;
                if(getcwd((char*)(T_S_(repertoire)),(size_t)(T_S_(taille)))==T_S_(NULL))
                {
                        taille*=T_S_(REPMULTTAILLE);
                        repertoiretemp=CAST_S_(NomFichier, REALLOCATION_(repertoire, Caractere, taille));
                        if(repertoiretemp==NULL)
                        {
                                free(repertoire);
                                return RESULTAT_ERREUR_MEMOIRE;
                        }
                        repertoire=repertoiretemp;
                }
                else
                {
                        if((((char*)(T_S_(repertoire)))[1])!=T_S_('\0'))
                        {
                                repertoiretemp=CAST_S_(NomFichier, REALLOCATION_(repertoire, Caractere, STRLEN(T_S_(repertoire))+T_S_(2)));
                                if(repertoiretemp==NULL)
                                {
                                        free(repertoire);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                repertoire=repertoiretemp;
                                STRCAT(T_S_(repertoire), T_S_("/"));
                        }
                        T_R_(repertoirecourant)=T_S_(repertoire);
                        break;
                }
        }
        return RESULTAT_OK;
}

Resultat aideenvironnement_lecture_repertoirefichier(TRAVAIL_SCALAIRE(NomFichier) repertoirefichier, REFERENCE_SCALAIRE(NomFichier) repertoire, REFERENCE_SCALAIRE(NomFichier) fichier)
{
        /* Renvoie la partie répertoire d'un nom de fichier
         * dans une chaine automatiquement allouée. Il faudra
         * libérer cette chaine par free(). De même pour le
         * nom de fichier.
         * Renvoie RESULTAT_ERREUR si fichier est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        enum
        {
                ETAT_NORMAL,
                ETAT_ECHAP
        } etat;
        REFERENCE_SCALAIRE(Caractere) position;
        REFERENCE_SCALAIRE(Caractere) parcours;
        STOCKAGE_SCALAIRE(Booleen) cheminvide;
        etat=ETAT_NORMAL;
        position=repertoirefichier;
        cheminvide=VRAI;
        for(parcours=((REFERENCE_SCALAIRE(Caractere))(repertoirefichier)) ; (T_R_(parcours))!=T_S_('\0') ; parcours++)
        {
                switch(T_S_(etat))
                {
                        case T_S_(ETAT_NORMAL):
                                if(T_R_(parcours)==T_S_(CARACTEREREPERTOIRE))
                                {
                                        position=parcours;
                                        cheminvide=FAUX;
                                }
                                etat=ETAT_NORMAL;
                                if(T_R_(parcours)==T_S_(CARACTEREECHAPPEMENT))
                                        etat=ETAT_ECHAP;
                                break;
                        case T_S_(ETAT_ECHAP):
                                etat=ETAT_NORMAL;
                                break;
                        default:
                                return RESULTAT_ERREUR;
                                break;
                }
        }
        if(T_S_(cheminvide)==T_S_(VRAI))
        {
                ASSERTION((T_R_(repertoire)=T_S_(DUP_CAST(NomFichier, T_S_("./"))))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                ASSERTION((T_R_(fichier)=T_S_(DUP_CAST(NomFichier, T_S_(repertoirefichier))))!=NULL, RESULTAT_ERREUR_MEMOIRE);

                return RESULTAT_OK;
        }
        else
        {
                REFERENCE_SCALAIRE(Caractere) ecriture;
                S_T_(T_R_(repertoire))=T_S_(CAST_S_(NomFichier, ALLOCATION_N_(Caractere, (TRAVAIL_SCALAIRE(Entier))(position-((REFERENCE_SCALAIRE(Caractere))(repertoirefichier)))+T_S_(2))));
                ASSERTION(S_T_(T_R_(repertoire))!=NULL, RESULTAT_ERREUR_MEMOIRE);

                ecriture=(REFERENCE_SCALAIRE(Caractere))(T_R_(repertoire));
                for(parcours=((REFERENCE_SCALAIRE(Caractere))(repertoirefichier)) ; parcours<=position ; parcours++)
                        T_R_(ecriture++)=T_R_(parcours);
                T_R_(ecriture)=T_S_('\0');
                T_R_(fichier)=T_S_(DUP_CAST(NomFichier, (TRAVAIL_SCALAIRE(Chaine))(position+1)));
                ASSERTION(T_R_(fichier)!=T_S_(NULL), RESULTAT_ERREUR_MEMOIRE);

                return RESULTAT_OK;
        }
}

Resultat aideenvironnement_ajout_repertoirefichier(TRAVAIL(PileNomFichier) pilerepertoire, TRAVAIL_SCALAIRE(NomFichier) fichier, REFERENCE_SCALAIRE(NomFichier) fichierabsolu, REFERENCE_SCALAIRE(Booleen) chrep)
{
        /* Prépare l'inclusion ou l'ouverture d'un fichier,
         * en se plaçant dans le répertoire du fichier,
         * et en renvoyant le nom du fichier avec son chemin
         * absolu, en maintenant la pile de repertoires
         * donnee en parametre.
         * Le booléen indique si l'on a bien pu changer de
         * répertoire.
         */
        TRAVAIL_SCALAIRE(NomFichier) chemin;
        TRAVAIL_SCALAIRE(NomFichier) repertoire;
        TRAVAIL_SCALAIRE(NomFichier) nomfichier;
        SECURISE(aideenvironnement_lecture_repertoirecourant(R_T_(chemin)));
        SECURISE(pilenomfichier_ajout(pilerepertoire,chemin));
        free(S_T_(chemin));
        SECURISE(aideenvironnement_lecture_repertoirefichier(fichier,R_T_(repertoire),R_T_(nomfichier)));
        if(STRSTR(repertoire, T_S_(REPERTOIREUTILISATEUR))==repertoire)
        {
                STOCKAGE_SCALAIRE(NomFichier) home;
                if((home=CAST_S_(NomFichier, GETENV(T_S_(VARENVREPERTOIREUTILISATEUR))))==NULL)
                {
                        free(S_T_(repertoire));
                        free(S_T_(nomfichier));
                        {
                                STOCKAGE_SCALAIRE(NomFichier) repertoire;
                                SECURISE(pilenomfichier_retrait(pilerepertoire,C_S_(repertoire)));
                                free(repertoire);
                        }
                        T_R_(fichierabsolu)=T_S_(NULL);
                        T_R_(chrep)=T_S_(FAUX);
                        return RESULTAT_OK;
                }
                S_T_(chemin)=CAST_S_(NomFichier, ALLOCATION_N_(Caractere, STRLEN(repertoire)+STRLEN(T_S_(home))+T_S_(1)));
                ASSERTION(S_T_(chemin)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                STRCPY(chemin, T_S_(home));
                STRCAT(chemin, repertoire+1);
                free(S_T_(repertoire));
                S_T_(repertoire)=S_T_(chemin);
        }
        if(chdir(repertoire)!=0)
        {
                free(S_T_(repertoire));
                free(S_T_(nomfichier));
                {
                        STOCKAGE_SCALAIRE(NomFichier) repertoire;
                        SECURISE(pilenomfichier_retrait(pilerepertoire,C_S_(repertoire)));
                        free(repertoire);
                }
                T_R_(fichierabsolu)=T_S_(NULL);
                T_R_(chrep)=T_S_(FAUX);
                return RESULTAT_OK;
        }
        free(S_T_(repertoire));
        SECURISE(aideenvironnement_lecture_repertoirecourant(R_T_(chemin)));
        T_R_(fichierabsolu)=T_S_(CAST_S_(NomFichier, ALLOCATION_N_(Caractere, STRLEN(chemin)+STRLEN(nomfichier)+T_S_(1))));
        ASSERTION(T_R_(fichierabsolu)!=T_S_(NULL), RESULTAT_ERREUR_MEMOIRE);

        STRCPY(T_R_(fichierabsolu), chemin);
        STRCAT(T_R_(fichierabsolu), nomfichier);
        free(S_T_(chemin));
        free(S_T_(nomfichier));
        T_R_(chrep)=T_S_(VRAI);
        return RESULTAT_OK;
}

Resultat aideenvironnement_retrait_repertoirefichier(TRAVAIL(PileNomFichier) pilerepertoire)
{
        /* Effectue les opérations sur le répertoire courant
         * lors de la fin de la lecture d'un fichier source.
         */
        STOCKAGE_SCALAIRE(NomFichier) repertoire;
        SECURISE(pilenomfichier_retrait(pilerepertoire,C_S_(repertoire)));
        ASSERTION(chdir(T_S_(repertoire))==T_S_(0), RESULTAT_ERREUR);

        free(repertoire);
        return RESULTAT_OK;
}

Resultat aideenvironnement_lecture_date(REFERENCE_SCALAIRE(Chaine) date, REFERENCE_SCALAIRE(Chaine) heure)
{
        /* Renvoie la date sous forme d'une chaine formattée de
         * caractères, qu'il faudra libérer avec free().
         * Retourne RESULTAT_ERREUR_MEMOIRE si une allocation échoue.
         */
        TRAVAIL_SCALAIRE(time_t) temps;
        REFERENCE_SCALAIRE(struct tm) tempslocal;
        STOCKAGE_SCALAIRE(Chaine) chainetemps;
        time(R_T_(temps));
        tempslocal=localtime(R_T_(temps));
        chainetemps=CAST_S_(Chaine, ALLOCATION_N_(Caractere, 128));
        ASSERTION(chainetemps!=NULL, RESULTAT_ERREUR_MEMOIRE);

        if(strftime(T_S_(chainetemps), T_S_(128), T_S_("%x"), tempslocal)>T_S_(0))
                T_R_(date)=T_S_(DUP_CAST(Chaine, chainetemps));
        else
                T_R_(date)=T_S_(DUP_CAST(Chaine, T_S_(TEMPS_INCONNU)));
        ASSERTION(date!=NULL, RESULTAT_ERREUR_MEMOIRE);

        if(strftime(T_S_(chainetemps), T_S_(128), T_S_("%X"), tempslocal)>T_S_(0))
                T_R_(heure)=T_S_(DUP_CAST(Chaine, chainetemps));
        else
                T_R_(heure)=T_S_(DUP_CAST(Chaine, T_S_(TEMPS_INCONNU)));
        ASSERTION(heure!=NULL, RESULTAT_ERREUR_MEMOIRE);

        free(chainetemps);
        return RESULTAT_OK;
}
