/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __OPTIONS__
#define __OPTIONS__

#include <src/global/global.h>

typedef struct options CONTENEUR(Options);

struct options
{
        //Options non repr�sent�es :
        //-a : active tout
        //-n : d�sactive tout
        //-o : nom du fichier de sortie.
        //-O : options d�finies � l'appel.
        STOCKAGE_SCALAIRE(Booleen) aide;
        //Option -h :
        //D�sactiv�e par d�faut.
        //Affiche la signification des options.
        STOCKAGE_SCALAIRE(Booleen) licence;
        //Option -V :
        //D�sactiv�e par d�faut.
        //Affiche les informations de licence.
        STOCKAGE_SCALAIRE(Entier) verbeux;
        //Option -v :
        //D�sactiv�e par d�faut (=0).
        //Affiche des messages concernant le fonctionnement
        //de PlanFacile.
        STOCKAGE_SCALAIRE(Booleen) erreur;
        //Option -e :
        //D�sactiv�e par d�faut.
        //Transforme tous les warnings en erreur.
        STOCKAGE_SCALAIRE(Booleen) copielocalisation;
        //Option -W :
        //Activ�e par d�faut.
        //D�sactive l'enregistrement des localisations
        //de commandes et leur affichage.
        STOCKAGE_SCALAIRE(Booleen) localisationabsolue;
        //Option -w :
        //D�sactiv�e par d�faut.
        //Enregistre les chemins absolus dans les
        //localisation fichier.
        STOCKAGE_SCALAIRE(Booleen) source;
        //Option -S :
        //D�sactiv�e par d�faut.
        //Ignore les fichiers sources non ouvrables.
        STOCKAGE_SCALAIRE(Booleen) inclusion;
        //Option -I :
        //D�sactiv�e par d�faut.
        //Ignore les inclusions r�cursives de fichiers.
        STOCKAGE_SCALAIRE(Booleen) recherche;
        //Option -s :
        //D�sactiv�e par d�faut.
        //Rend planfacile plus s�v�re concernant les recherches
        //de fichiers sources � ouvrir (#standard et templates).
        STOCKAGE_SCALAIRE(Booleen) utilisateur;
        //Option -u :
        //Activ�e par d�faut.
        //Affiche les warnings
        //d�finis par l'utilisateur.
        STOCKAGE_SCALAIRE(Booleen) commandes;
        //Option -C :
        //D�sactiv�e par d�faut.
        //Lorsqu'une commande est plac�e dans
        //un flux o� elle ne devrait pas �tre,
        //planfacile �met un warning et ignore
        //la commande.
        STOCKAGE_SCALAIRE(Booleen) redefinitionidees;
        //Option -i :
        //Activ�e par d�faut.
        //Avertit des red�finitions d'id�es
        //par un warning.
        STOCKAGE_SCALAIRE(Booleen) ideereferencevide;
        //Option -Y :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les id�es dont la
        //r�f�rence est vide sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) indiceincorrect;
        //Option -m :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les id�es
        //manquantes comportant un indice incorrecte
        //sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) referenceincorrecte;
        //Option -g :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les id�es
        //g�n�riques comportant une r�f�rence incorrecte
        //sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) ajoutracine;
        //Option -r :
        //Activ�e par d�faut.
        //Lorsqu'elle est pr�sente, l'ajout �ventuel
        //d'une id�e racine est signal�e.
        STOCKAGE_SCALAIRE(Booleen) ideeautomatiquemanquante;
        //Option -A :
        //D�sactiv�e par d�faut.
        //Lorqu'elle est pr�sente, les id�es automatiques
        //ajout�es par le calcul du plan, mais non d�finies
        //sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) dependancesincorrectes;
        //Option -d :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les d�pendances
        //dont la destination est incorrecte sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) dependancesinutiles;
        //Option -D :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les d�pendances
        //qui ne servent pas pour la construction
        //du graphe sont indiqu�es.
        STOCKAGE_SCALAIRE(Booleen) pertinenceincorrecte;
        //Option -p :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les id�es
        //comportant une pertinence incorrecte
        //sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) indicereferenceincorrect;
        //Option -E :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les commandes
        //#extref comprennant un indice incorrect
        //sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) commentaires;
        //Option -c :
        //D�sactiv�e par d�faut.
        //Lorqu'elle est pr�sente, les commentaires plac�s
        //dans des flux de texte sont plac�s dans le
        //document final, au format des messages.
        STOCKAGE_SCALAIRE(Booleen) redefinitionstyles;
        //Option -y :
        //D�sactiv�e par d�faut.
        //Avertit des red�finitions de styles
        //par un warning.
        STOCKAGE_SCALAIRE(Booleen) niveauincorrect;
        //Option -l :
        //D�sactiv�e par d�faut;
        //Lorsqu'elle est pr�sente, les
        //commandes de style comportant
        //un niveau incorrect sont
        //ignor�es.
        STOCKAGE_SCALAIRE(Booleen) sectionstylemanquant;
        //Option -G :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les id�es dont
        //le niveau n'a pas de style pour le formattage
        //est ignor�e.
        STOCKAGE_SCALAIRE(Booleen) referencestylemanquant;
        //Option -N :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les r�f�rences dont
        //le niveau de destination n'a pas de style pour
        //le formattage est ignor�e.
        STOCKAGE_SCALAIRE(Booleen) nomsectionincorrect;
        //Option -T :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les commandes #sec
        //comprennant un niveau incorrect sont ignor�es.
        STOCKAGE_SCALAIRE(Booleen) redefinitionmacro;
        //Option -M :
        //D�sactiv�e par d�faut.
        //Avertit des red�finitions de macros
        //par un warning.
        STOCKAGE_SCALAIRE(Booleen) macroinconnue;
        //Option -U :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les appels de
        //macros non d�finies seront ignor�es.
        STOCKAGE_SCALAIRE(Booleen) parametres;
        //Option -P :
        //D�sactiv�e par d�faut.
        //Arrange le nombre de param�tres de macro
        //� la d�finition de la macro.
        STOCKAGE_SCALAIRE(Booleen) recursivite;
        //Option -R :
        //D�sactiv�e par d�faut.
        //Ignore les appels r�cursifs de macros
        //et g�n�re un warning.
        STOCKAGE_SCALAIRE(Booleen) optionmacro;
        //Option -t :
        //D�sactiv�e par d�faut.
        //Lorsqu'elle est pr�sente, les options
        //pr�sentes dans les d�finitions et les
        //appels de macro seront ignor�s.
};
/* Cette structure rassemble les options de la ligne
 * de commande d'appel � planfacile.
 * Ces options �videmment influent sur le comportement
 * de planfacile.
 */

Resultat options_initialisation(TRAVAIL(Options) options);
/* Cr�e une structure d'options, avec les valeurs d'options
 * par d�faut.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation m�moire.
 */

Resultat options_changement_licence                                        (TRAVAIL(Options) options);
Resultat options_changement_aide                                        (TRAVAIL(Options) options);
Resultat options_changement_verbeux                                        (TRAVAIL(Options) options);
Resultat options_changement_inclusion                                (TRAVAIL(Options) options);
Resultat options_changement_source                                        (TRAVAIL(Options) options);
Resultat options_changement_recherche                                (TRAVAIL(Options) options);
Resultat options_changement_macroinconnue                        (TRAVAIL(Options) options);
Resultat options_changement_optionmacro                                (TRAVAIL(Options) options);
Resultat options_changement_recursivite                                (TRAVAIL(Options) options);
Resultat options_changement_redefinitionmacro                (TRAVAIL(Options) options);
Resultat options_changement_parametres                                (TRAVAIL(Options) options);
Resultat options_changement_erreur                                        (TRAVAIL(Options) options);
Resultat options_changement_utilisateur                                (TRAVAIL(Options) options);
Resultat options_changement_redefinitionstyles                (TRAVAIL(Options) options);
Resultat options_changement_niveauincorrect                        (TRAVAIL(Options) options);
Resultat options_changement_redefinitionidees                (TRAVAIL(Options) options);
Resultat options_changement_ideereferencevide                (TRAVAIL(Options) options);
Resultat options_changement_referenceincorrecte                (TRAVAIL(Options) options);
Resultat options_changement_pertinenceincorrecte        (TRAVAIL(Options) options);
Resultat options_changement_indiceincorrect                        (TRAVAIL(Options) options);
Resultat options_changement_commandes                                (TRAVAIL(Options) options);
Resultat options_changement_dependancesinutiles                (TRAVAIL(Options) options);
Resultat options_changement_ajoutracine                                (TRAVAIL(Options) options);
Resultat options_changement_copielocalisation                (TRAVAIL(Options) options);
Resultat options_changement_localisationabsolue                (TRAVAIL(Options) options);
Resultat options_changement_sectionstylemanquant        (TRAVAIL(Options) options);
Resultat options_changement_referencestylemanquant        (TRAVAIL(Options) options);
Resultat options_changement_ideeautomatiquemanquante(TRAVAIL(Options) options);
Resultat options_changement_commentaires                        (TRAVAIL(Options) options);
Resultat options_changement_dependancesincorrectes        (TRAVAIL(Options) options);
Resultat options_changement_indicereferenceincorrect(TRAVAIL(Options) options);
Resultat options_changement_nomsectionincorrect                (TRAVAIL(Options) options);

/* Ces fonctions changent l'option concern�e.
 * Renvoie RESULTAT_ERREUR si options est NULL.
 */

Resultat options_lecture_licence                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) licence                                );
Resultat options_lecture_aide                                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) aide                                );
Resultat options_lecture_verbeux                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Entier)  verbeux                                );
Resultat options_lecture_inclusion                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) inclusion                        );
Resultat options_lecture_source                                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) source                                );
Resultat options_lecture_recherche                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) recherche                        );
Resultat options_lecture_macroinconnue                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) macroinconnue                );
Resultat options_lecture_optionmacro                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) optionmacro                        );
Resultat options_lecture_recursivite                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) recursivite                        );
Resultat options_lecture_redefinitionmacro                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) redefinitionmacro        );
Resultat options_lecture_parametres                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) parametres                        );
Resultat options_lecture_erreur                                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) erreur                                );
Resultat options_lecture_utilisateur                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) utilisateur                        );
Resultat options_lecture_redefinitionstyles                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) redefinitionstyles        );
Resultat options_lecture_niveauincorrect                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) niveauincorrect                );
Resultat options_lecture_redefinitionidees                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) redefinitionidees        );
Resultat options_lecture_ideereferencevide                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) ideereferencevide        );
Resultat options_lecture_referenceincorrecte                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) referenceincorrecte        );
Resultat options_lecture_pertinenceincorrecte                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) pertinenceincorrecte        );
Resultat options_lecture_indiceincorrect                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) indiceincorrect                );
Resultat options_lecture_commandes                                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) commandes                        );
Resultat options_lecture_dependancesinutiles                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) dependancesinutiles        );
Resultat options_lecture_ajoutracine                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) ajoutracine                        );
Resultat options_lecture_copielocalisation                        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) copielocalisation        );
Resultat options_lecture_localisationabsolue                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) localisationabsolue        );
Resultat options_lecture_sectionstylemanquant                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) sectionstylemanquant);
Resultat options_lecture_referencestylemanquant                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) referencestylemanquant);
Resultat options_lecture_ideeautomatiquemanquante        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) ideeautomatiquemanquante);
Resultat options_lecture_commentaires                                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) commentaires                );
Resultat options_lecture_dependancesincorrectes                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) dependancesincorrectes);
Resultat options_lecture_indicereferenceincorrect        (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) indicereferenceincorrect);
Resultat options_lecture_nomsectionincorrect                (TRAVAIL(Options) options, REFERENCE_SCALAIRE(Booleen) nomsectionincorrect        );

/* Ces fonctions renvoient l'�tat de l'option concern�e.
 * Renvoie RESULTAT_ERREUR si options est NULL.
 */

Resultat options_copie(TRAVAIL(Options) options, TRAVAIL(Options) copie);
/* Copie une structure d'options.
 * Renvoie RESULTAT_ERREUR si options est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation m�moire �choue.
 * Si la copie est non nulle, elle est d�truite avant la copie.
 */

Resultat options_destruction(TRAVAIL(Options) options);
/* D�truit une structure d'options.
 */

#endif
