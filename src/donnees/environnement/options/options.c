/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "options.h"

Resultat options_initialisation(TRAVAIL(Options) options)
{
        /* Cr�e une structure d'options, avec les valeurs d'options
         * par d�faut.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation m�moire.
         */
        ASSERTION((S_T(options)=NOUVEAU(Options))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(options, licence)                 =FAUX;
        CHAMP(options, aide)                    =FAUX;
        CHAMP(options, verbeux)                 =0;
        CHAMP(options, inclusion)               =FAUX;
        CHAMP(options, source)                  =FAUX;
        CHAMP(options, recherche)               =FAUX;
        CHAMP(options, macroinconnue)           =FAUX;
        CHAMP(options, optionmacro)             =FAUX;
        CHAMP(options, recursivite)             =FAUX;
        CHAMP(options, redefinitionmacro)       =FAUX;
        CHAMP(options, parametres)              =FAUX;
        CHAMP(options, erreur)                  =FAUX;
        CHAMP(options, utilisateur)             =VRAI;
        CHAMP(options, redefinitionstyles)      =FAUX;
        CHAMP(options, niveauincorrect)         =FAUX;
        CHAMP(options, redefinitionidees)       =VRAI;
        CHAMP(options, ideereferencevide)       =FAUX;
        CHAMP(options, referenceincorrecte)     =FAUX;
        CHAMP(options, pertinenceincorrecte)    =FAUX;
        CHAMP(options, indiceincorrect)         =FAUX;
        CHAMP(options, commandes)               =FAUX;
        CHAMP(options, dependancesinutiles)     =FAUX;
        CHAMP(options, ajoutracine)             =VRAI;
        CHAMP(options, copielocalisation)       =VRAI;
        CHAMP(options, localisationabsolue)     =FAUX;
        CHAMP(options, sectionstylemanquant)    =FAUX;
        CHAMP(options, referencestylemanquant)  =FAUX;
        CHAMP(options, ideeautomatiquemanquante)=FAUX;
        CHAMP(options, commentaires)            =FAUX;
        CHAMP(options, dependancesincorrectes)  =FAUX;
        CHAMP(options, indicereferenceincorrect)=FAUX;
        CHAMP(options, nomsectionincorrect)     =FAUX;
        return RESULTAT_OK;
}


#define _OPERATION_OPTION_(OPERATION, _methode_, _champ_)        \
        Resultat _methode_(TRAVAIL(Options) options)             \
        {                                                        \
                ASSERTION(S_T(options)!=NULL, RESULTAT_ERREUR);  \
                OPERATION(T_S_(CHAMP(options, _champ_)));        \
                return RESULTAT_OK;                              \
        }

#define OPERATION_BASCULE(_champ_)                        (_champ_)^=T_S_(VRAI)
#define CHANGEMENT_OPTION_BOOLEEN(_methode_, _champ_)     _OPERATION_OPTION_(OPERATION_BASCULE, _methode_, _champ_)

#define OPERATION_INCREMENT(_champ_)                      (_champ_)++
#define CHANGEMENT_OPTION_ENTIER(_methode_, _champ_)      _OPERATION_OPTION_(OPERATION_INCREMENT, _methode_, _champ_)

CHANGEMENT_OPTION_BOOLEEN(options_changement_licence, licence)
CHANGEMENT_OPTION_BOOLEEN(options_changement_aide, aide)
CHANGEMENT_OPTION_ENTIER(options_changement_verbeux, verbeux)
CHANGEMENT_OPTION_BOOLEEN(options_changement_inclusion, inclusion)
CHANGEMENT_OPTION_BOOLEEN(options_changement_source, source)
CHANGEMENT_OPTION_BOOLEEN(options_changement_recherche, recherche)
CHANGEMENT_OPTION_BOOLEEN(options_changement_macroinconnue, macroinconnue)
CHANGEMENT_OPTION_BOOLEEN(options_changement_optionmacro, optionmacro)
CHANGEMENT_OPTION_BOOLEEN(options_changement_recursivite, recursivite)
CHANGEMENT_OPTION_BOOLEEN(options_changement_redefinitionmacro, redefinitionmacro)
CHANGEMENT_OPTION_BOOLEEN(options_changement_parametres, parametres)
CHANGEMENT_OPTION_BOOLEEN(options_changement_erreur, erreur)
CHANGEMENT_OPTION_BOOLEEN(options_changement_utilisateur, utilisateur)
CHANGEMENT_OPTION_BOOLEEN(options_changement_redefinitionstyles, redefinitionstyles)
CHANGEMENT_OPTION_BOOLEEN(options_changement_niveauincorrect, niveauincorrect)
CHANGEMENT_OPTION_BOOLEEN(options_changement_redefinitionidees, redefinitionidees)
CHANGEMENT_OPTION_BOOLEEN(options_changement_ideereferencevide, ideereferencevide)
CHANGEMENT_OPTION_BOOLEEN(options_changement_referenceincorrecte, referenceincorrecte)
CHANGEMENT_OPTION_BOOLEEN(options_changement_pertinenceincorrecte, pertinenceincorrecte)
CHANGEMENT_OPTION_BOOLEEN(options_changement_indiceincorrect, indiceincorrect)
CHANGEMENT_OPTION_BOOLEEN(options_changement_commandes, commandes)
CHANGEMENT_OPTION_BOOLEEN(options_changement_dependancesinutiles, dependancesinutiles)
CHANGEMENT_OPTION_BOOLEEN(options_changement_ajoutracine, ajoutracine)
CHANGEMENT_OPTION_BOOLEEN(options_changement_copielocalisation, copielocalisation)
CHANGEMENT_OPTION_BOOLEEN(options_changement_localisationabsolue, localisationabsolue)
CHANGEMENT_OPTION_BOOLEEN(options_changement_sectionstylemanquant, sectionstylemanquant)
CHANGEMENT_OPTION_BOOLEEN(options_changement_referencestylemanquant, referencestylemanquant)
CHANGEMENT_OPTION_BOOLEEN(options_changement_ideeautomatiquemanquante, ideeautomatiquemanquante)
CHANGEMENT_OPTION_BOOLEEN(options_changement_commentaires, commentaires)
CHANGEMENT_OPTION_BOOLEEN(options_changement_dependancesincorrectes, dependancesincorrectes)
CHANGEMENT_OPTION_BOOLEEN(options_changement_indicereferenceincorrect, indicereferenceincorrect)
CHANGEMENT_OPTION_BOOLEEN(options_changement_nomsectionincorrect, nomsectionincorrect)
/* Ces fonctions changent l'option concern�e.
 * Renvoie RESULTAT_ERREUR si options est NULL.
 */


#define _LECTURE_OPTION_(_type_, _methode_, _champ_) \
                Resultat _methode_(TRAVAIL(Options) options, REFERENCE_SCALAIRE(_type_) _champ_) \
                {                                                                                \
                        ASSERTION(S_T(options)!=NULL, RESULTAT_ERREUR);                          \
                        T_R_(_champ_)=T_S_(CHAMP(options, _champ_));                             \
                        return RESULTAT_OK;                                                      \
                }
#define LECTURE_OPTION_BOOLEEN(_methode_, _champ_)       _LECTURE_OPTION_(Booleen, _methode_, _champ_)
#define LECTURE_OPTION_ENTIER(_methode_, _champ_)        _LECTURE_OPTION_(Entier, _methode_, _champ_)

LECTURE_OPTION_BOOLEEN(options_lecture_licence, licence)
LECTURE_OPTION_BOOLEEN(options_lecture_aide, aide)
LECTURE_OPTION_ENTIER(options_lecture_verbeux, verbeux)
LECTURE_OPTION_BOOLEEN(options_lecture_inclusion, inclusion)
LECTURE_OPTION_BOOLEEN(options_lecture_source, source)
LECTURE_OPTION_BOOLEEN(options_lecture_recherche, recherche)
LECTURE_OPTION_BOOLEEN(options_lecture_macroinconnue, macroinconnue)
LECTURE_OPTION_BOOLEEN(options_lecture_optionmacro, optionmacro)
LECTURE_OPTION_BOOLEEN(options_lecture_recursivite, recursivite)
LECTURE_OPTION_BOOLEEN(options_lecture_redefinitionmacro, redefinitionmacro)
LECTURE_OPTION_BOOLEEN(options_lecture_parametres, parametres)
LECTURE_OPTION_BOOLEEN(options_lecture_erreur, erreur)
LECTURE_OPTION_BOOLEEN(options_lecture_utilisateur, utilisateur)
LECTURE_OPTION_BOOLEEN(options_lecture_redefinitionstyles, redefinitionstyles)
LECTURE_OPTION_BOOLEEN(options_lecture_niveauincorrect, niveauincorrect)
LECTURE_OPTION_BOOLEEN(options_lecture_redefinitionidees, redefinitionidees)
LECTURE_OPTION_BOOLEEN(options_lecture_ideereferencevide, ideereferencevide)
LECTURE_OPTION_BOOLEEN(options_lecture_referenceincorrecte, referenceincorrecte)
LECTURE_OPTION_BOOLEEN(options_lecture_pertinenceincorrecte, pertinenceincorrecte)
LECTURE_OPTION_BOOLEEN(options_lecture_indiceincorrect, indiceincorrect)
LECTURE_OPTION_BOOLEEN(options_lecture_commandes, commandes)
LECTURE_OPTION_BOOLEEN(options_lecture_dependancesinutiles, dependancesinutiles)
LECTURE_OPTION_BOOLEEN(options_lecture_ajoutracine, ajoutracine)
LECTURE_OPTION_BOOLEEN(options_lecture_copielocalisation, copielocalisation)
LECTURE_OPTION_BOOLEEN(options_lecture_localisationabsolue, localisationabsolue)
LECTURE_OPTION_BOOLEEN(options_lecture_sectionstylemanquant, sectionstylemanquant)
LECTURE_OPTION_BOOLEEN(options_lecture_referencestylemanquant, referencestylemanquant)
LECTURE_OPTION_BOOLEEN(options_lecture_ideeautomatiquemanquante, ideeautomatiquemanquante)
LECTURE_OPTION_BOOLEEN(options_lecture_commentaires, commentaires)
LECTURE_OPTION_BOOLEEN(options_lecture_dependancesincorrectes, dependancesincorrectes)
LECTURE_OPTION_BOOLEEN(options_lecture_indicereferenceincorrect, indicereferenceincorrect)
LECTURE_OPTION_BOOLEEN(options_lecture_nomsectionincorrect, nomsectionincorrect)
/* Ces fonctions renvoient l'�tat de l'option concern�e.
 * Renvoie RESULTAT_ERREUR si options est NULL.
 */

Resultat options_copie(TRAVAIL(Options) options, TRAVAIL(Options) copie)
{
        /* Copie une structure d'options.
         * Renvoie RESULTAT_ERREUR si options est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation m�moire �choue.
         * Si la copie est non nulle, elle est d�truite avant la copie.
         */
        ASSERTION(S_T(options)!=NULL, RESULTAT_ERREUR);

        SECURISE(options_destruction(copie));
        SECURISE(options_initialisation(copie));
        CHAMP(copie, licence)                 =CHAMP(options, licence)                 ;
        CHAMP(copie, aide)                    =CHAMP(options, aide)                    ;
        CHAMP(copie, verbeux)                 =CHAMP(options, verbeux)                 ;
        CHAMP(copie, inclusion)               =CHAMP(options, inclusion)               ;
        CHAMP(copie, source)                  =CHAMP(options, source)                  ;
        CHAMP(copie, recherche)               =CHAMP(options, recherche)               ;
        CHAMP(copie, macroinconnue)           =CHAMP(options, macroinconnue)           ;
        CHAMP(copie, optionmacro)             =CHAMP(options, optionmacro)             ;
        CHAMP(copie, recursivite)             =CHAMP(options, recursivite)             ;
        CHAMP(copie, redefinitionmacro)       =CHAMP(options, redefinitionmacro)       ;
        CHAMP(copie, parametres)              =CHAMP(options, parametres)              ;
        CHAMP(copie, erreur)                  =CHAMP(options, erreur)                  ;
        CHAMP(copie, utilisateur)             =CHAMP(options, utilisateur)             ;
        CHAMP(copie, redefinitionstyles)      =CHAMP(options, redefinitionstyles)      ;
        CHAMP(copie, niveauincorrect)         =CHAMP(options, niveauincorrect)         ;
        CHAMP(copie, redefinitionidees)       =CHAMP(options, redefinitionidees)       ;
        CHAMP(copie, ideereferencevide)       =CHAMP(options, ideereferencevide)       ;
        CHAMP(copie, referenceincorrecte)     =CHAMP(options, referenceincorrecte)     ;
        CHAMP(copie, pertinenceincorrecte)    =CHAMP(options, pertinenceincorrecte)    ;
        CHAMP(copie, indiceincorrect)         =CHAMP(options, indiceincorrect)         ;
        CHAMP(copie, commandes)               =CHAMP(options, commandes)               ;
        CHAMP(copie, dependancesinutiles)     =CHAMP(options, dependancesinutiles)     ;
        CHAMP(copie, ajoutracine)             =CHAMP(options, ajoutracine)             ;
        CHAMP(copie, copielocalisation)       =CHAMP(options, copielocalisation)       ;
        CHAMP(copie, localisationabsolue)     =CHAMP(options, localisationabsolue)     ;
        CHAMP(copie, sectionstylemanquant)    =CHAMP(options, sectionstylemanquant)    ;
        CHAMP(copie, referencestylemanquant)  =CHAMP(options, referencestylemanquant)  ;
        CHAMP(copie, ideeautomatiquemanquante)=CHAMP(options, ideeautomatiquemanquante);
        CHAMP(copie, commentaires)            =CHAMP(options, commentaires)            ;
        CHAMP(copie, dependancesincorrectes)  =CHAMP(options, dependancesincorrectes)  ;
        CHAMP(copie, indicereferenceincorrect)=CHAMP(options, indicereferenceincorrect);
        CHAMP(copie, nomsectionincorrect)     =CHAMP(options, nomsectionincorrect)     ;
        return RESULTAT_OK;
}

Resultat options_destruction(TRAVAIL(Options) options)
{
        /* D�truit une structure d'options.
         */
        if(S_T(options)==NULL)
                return RESULTAT_OK;
        free(S_T(options));
        S_T(options)=NULL;
        return RESULTAT_OK;
}

