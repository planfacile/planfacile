/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ENVIRONNEMENT__
#define __ENVIRONNEMENT__

#include <src/global/global.h>

typedef struct environnement CONTENEUR(Environnement);

#include <src/donnees/environnement/aideenvironnement.h>
#include <src/donnees/environnement/options/options.h>
#include <src/analyseur/donnees/pilenomfichier.h>

#include "messages.h"


struct environnement
{
        STOCKAGE(Options) options;
        //Contient la description des options
        //pass�es en ligne de commande.
        STOCKAGE_SCALAIRE(NomFichier) fichiersortie;
        //Nom du fichier de sortie utilis� pour rendre le
        //r�sultat.
        STOCKAGE_SCALAIRE(DescripteurFichier) sortie;
        //Descripteur de fichier utilis� pour rendre le
        //r�sultat.
        STOCKAGE_SCALAIRE(DescripteurFichier) erreur;
        //Descripteur de fichier utilis� pour fournir les
        //messages d'erreur.
        STOCKAGE(PileNomFichier) pilerepertoirecourant;
        //Pile des r�pertoires courants dans lesquels va se retrouver
        //planfacile lors des inclusions de fichiers.
        STOCKAGE_SCALAIRE(Langue) langue;
        //Ceci est la langue utilis�e par planfacile pour
        //d�livrer ses messages d'erreur, ainsi que placer
        //les statistiques d'am�lioration du plan.
        STOCKAGE_SCALAIRE(Charset) charset;
        //Charset utilis� pour �crire les messages d'erreur
        //et les statistiques d'am�lioration du plan.
};
/* Cette structure est destin�e � recevoir tous les param�tres concernant
 * l'exterieur de planfacile et pouvant modifier son comportement.
 * Cela comprend �videmment les diff�rentes options pass�es au compilateur,
 * mais aussi tout ce qui concerne la localisation et les flux de sortie.
 */

Resultat environnement_initialisation(TRAVAIL(Environnement) environnement);
/* Initialise un environnement.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'echec d'une allocation.
 */

Resultat environnement_definition_options(TRAVAIL(Environnement) environnement, TRAVAIL(Options) options);
/* D�finit une liste d'options pour un environnement.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_fichiersortie(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) fichier);
/* D�finit un nom de fichier de sortie pour un environnement.
 * Si le nom est NULL, la sortie standard sera utilis�e.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_sortie(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) sortie);
/* D�finit un descripteur de fichier de sortie pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_erreur(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) erreur);
/* D�finit un descripteur de fichier d'erreur pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_pilerepertoirecourant(TRAVAIL(Environnement) environnement, TRAVAIL(PileNomFichier) pilerepertoirecourant);
/* D�finit une pile de r�pertoires courants pour un environnement.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_langue(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Langue) langue);
/* D�finit une langue pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_definition_charset(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Charset) charset);
/* D�finit un jeu de caract�res pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_options(TRAVAIL(Environnement) environnement, REFERENCE(Options) options);
/* Lit une liste d'options pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_fichiersortie(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(NomFichier) fichier);
/* Lit un nom de fichier de sortie pour un environnement.
 * Si la valeur renvoy�e est NULL, la sortie standard doit
 * �tre utilis�e.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_sortie(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(DescripteurFichier) sortie);
/* Lit un descripteur de fichier de sortie pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_erreur(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(DescripteurFichier) erreur);
/* Lit un descripteur de fichier d'erreur pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_pilerepertoirecourant(TRAVAIL(Environnement) environnement, REFERENCE(PileNomFichier) pilerepertoirecourant);
/* Lit une pile de noms de fichier pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_langue(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Langue) langue);
/* Lit une langue pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_lecture_charset(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Charset) charset);
/* Lit un jeu de caract�res pour un environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

Resultat environnement_ajout_repertoirefichier(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) fichier, REFERENCE_SCALAIRE(NomFichier) fichierabsolu, REFERENCE_SCALAIRE(Booleen) chrep);
/* Pr�pare l'inclusion ou l'ouverture d'un fichier,
 * en se pla�ant dans le r�pertoire du fichier,
 * et en renvoyant le nom du fichier avec son chemin
 * absolu.
 * Le bool�en indique si l'on a bien pu changer de
 * r�pertoire.
 */

Resultat environnement_retrait_repertoirefichier(TRAVAIL(Environnement) environnement);
/* Effectue les op�rations sur le r�pertoire courant
 * lors de la fin de la lecture d'un fichier source.
 */

Resultat environnement_initialisation_pilenomfichier(TRAVAIL(Environnement) environnement);
/* Cr�e une pile de nom de fichiers.
 */

Resultat environnement_ajout_pilenomfichier(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) nomfichier);
/* Ajoute un nom de fichier au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat environnement_retrait_pilenomfichier(TRAVAIL(Environnement) environnement, COREFERENCE_SCALAIRE(NomFichier) nomfichier);
/* Retire le sommet de la pile, et renvoye le nom de fichier.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Il faudra lib�rer le nom de fichier renvoy� � la main.
 */

Resultat environnement_vide_pilenomfichier(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat environnement_destruction_pilenomfichier(TRAVAIL(Environnement) environnement);
/* D�truit une pile de nom de fichiers.
 */

Resultat environnement_copie(TRAVAIL(Environnement) environnement, TRAVAIL(Environnement) copie);
/* Cr�e une copie de l'environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */ 

Resultat environnement_destruction(TRAVAIL(Environnement) environnement);
/* D�truit un environnement.
 */

#endif
