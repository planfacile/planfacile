/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "environnement.h"


Resultat environnement_initialisation(TRAVAIL(Environnement) environnement)
{
        /* Initialise un environnement.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'echec d'une allocation.
         */
        ASSERTION((S_T(environnement)=NOUVEAU(Environnement))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(options_initialisation(CHAMP_TRAVAIL(environnement, options)));
        SECURISE(pilenomfichier_initialisation(CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        CHAMP(environnement, fichiersortie)=NULL;
        CHAMP(environnement, sortie)=NULL;
        CHAMP(environnement, erreur)=NULL;
        CHAMP(environnement, langue)=LANGUE_INCONNUE;
        CHAMP(environnement, charset)=CHARSET_INCONNU;
        return RESULTAT_OK;
}

Resultat environnement_definition_options(TRAVAIL(Environnement) environnement, TRAVAIL(Options) options)
{
        /* D�finit une liste d'options pour un environnement.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(options_copie(options,CHAMP_TRAVAIL(environnement, options)));
        return RESULTAT_OK;
}

Resultat environnement_definition_fichiersortie(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) fichier)
{
        /* D�finit un nom de fichier de sortie pour un environnement.
         * Si le nom est NULL, la sortie standard sera utilis�e.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(environnement, fichiersortie)!=NULL)
                free(CHAMP(environnement, fichiersortie));
        if(S_T_(fichier)==NULL)
                CHAMP(environnement, fichiersortie)=NULL;
        else
        {
                ASSERTION((CHAMP(environnement, fichiersortie)=DUP_CAST(NomFichier, fichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        return RESULTAT_OK;
}

Resultat environnement_definition_sortie(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) sortie)
{
        /* D�finit un descripteur de fichier de sortie pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        CHAMP(environnement, sortie)=S_T_(sortie);
        return RESULTAT_OK;
}

Resultat environnement_definition_erreur(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) erreur)
{
        /* D�finit un descripteur de fichier d'erreur pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        CHAMP(environnement, erreur)=S_T_(erreur);
        return RESULTAT_OK;
}

Resultat environnement_definition_pilerepertoirecourant(TRAVAIL(Environnement) environnement, TRAVAIL(PileNomFichier) pilerepertoirecourant)
{
        /* D�finit une pile de r�pertoires courants pour un environnement.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_copie(pilerepertoirecourant,CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        return RESULTAT_OK;
}

Resultat environnement_definition_langue(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Langue) langue)
{
        /* D�finit une langue pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        CHAMP(environnement, langue)=S_T_(langue);
        return RESULTAT_OK;
}

Resultat environnement_definition_charset(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Charset) charset)
{
        /* D�finit un jeu de caract�res pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        CHAMP(environnement, charset)=S_T_(charset);
        return RESULTAT_OK;
}

Resultat environnement_lecture_options(TRAVAIL(Environnement) environnement, REFERENCE(Options) options)
{
        /* Lit une liste d'options pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R(options)=CHAMP_TRAVAIL(environnement, options);
        return RESULTAT_OK;
}

Resultat environnement_lecture_fichiersortie(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(NomFichier) fichier)
{
        /* Lit un nom de fichier de sortie pour un environnement.
         * Si la valeur renvoy�e est NULL, la sortie standard doit
         * �tre utilis�e.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R_(fichier)=T_S_(CHAMP(environnement, fichiersortie));
        return RESULTAT_OK;
}

Resultat environnement_lecture_sortie(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(DescripteurFichier) sortie)
{
        /* Lit un descripteur de fichier de sortie pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R_(sortie)=T_S_(CHAMP(environnement, sortie));
        return RESULTAT_OK;
}

Resultat environnement_lecture_erreur(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(DescripteurFichier) erreur)
{
        /* Lit un descripteur de fichier d'erreur pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R_(erreur)=T_S_(CHAMP(environnement, erreur));
        return RESULTAT_OK;
}

Resultat environnement_lecture_pilerepertoirecourant(TRAVAIL(Environnement) environnement, REFERENCE(PileNomFichier) pilerepertoirecourant)
{
        /* Lit une pile de noms de fichier pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R(pilerepertoirecourant)=CHAMP_TRAVAIL(environnement, pilerepertoirecourant);
        return RESULTAT_OK;
}

Resultat environnement_lecture_langue(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Langue) langue)
{
        /* Lit une langue pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R_(langue)=T_S_(CHAMP(environnement, langue));
        return RESULTAT_OK;
}

Resultat environnement_lecture_charset(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Charset) charset)
{
        /* Lit un jeu de caract�res pour un environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        T_R_(charset)=T_S_(CHAMP(environnement, charset));
        return RESULTAT_OK;
}

Resultat environnement_ajout_repertoirefichier(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) fichier, REFERENCE_SCALAIRE(NomFichier) fichierabsolu, REFERENCE_SCALAIRE(Booleen) chrep)
{
        /* Pr�pare l'inclusion ou l'ouverture d'un fichier,
         * en se pla�ant dans le r�pertoire du fichier,
         * et en renvoyant le nom du fichier avec son chemin
         * absolu.
         * Le bool�en indique si l'on a bien pu changer de
         * r�pertoire.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        return aideenvironnement_ajout_repertoirefichier(CHAMP_TRAVAIL(environnement, pilerepertoirecourant), fichier, fichierabsolu, chrep);
}

Resultat environnement_retrait_repertoirefichier(TRAVAIL(Environnement) environnement)
{
        /* Effectue les op�rations sur le r�pertoire courant
         * lors de la fin de la lecture d'un fichier source.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(aideenvironnement_retrait_repertoirefichier(CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        return RESULTAT_OK;
}

Resultat environnement_initialisation_pilenomfichier(TRAVAIL(Environnement) environnement)
{
        /* Cr�e une pile de nom de fichiers.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_initialisation(CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        return RESULTAT_OK;
}

Resultat environnement_ajout_pilenomfichier(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(NomFichier) nomfichier)
{
        /* Ajoute un nom de fichier au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_ajout(CHAMP_TRAVAIL(environnement, pilerepertoirecourant),nomfichier));
        return RESULTAT_OK;
}

Resultat environnement_retrait_pilenomfichier(TRAVAIL(Environnement) environnement, COREFERENCE_SCALAIRE(NomFichier) nomfichier)
{
        /* Retire le sommet de la pile, et renvoye le nom de fichier.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Il faudra lib�rer le nom de fichier renvoy� � la main.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_retrait(CHAMP_TRAVAIL(environnement, pilerepertoirecourant),nomfichier));
        return RESULTAT_OK;
}

Resultat environnement_vide_pilenomfichier(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_vide(CHAMP_TRAVAIL(environnement, pilerepertoirecourant),vide));
        return RESULTAT_OK;
}

Resultat environnement_destruction_pilenomfichier(TRAVAIL(Environnement) environnement)
{
        /* D�truit une pile de nom de fichiers.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilenomfichier_destruction(CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        return RESULTAT_OK;
}

Resultat environnement_copie(TRAVAIL(Environnement) environnement, TRAVAIL(Environnement) copie)
{
        /* Cr�e une copie de l'environnement.
         * Renvoie RESULTAT_ERREUR si environnement est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_destruction(copie));
        SECURISE(environnement_initialisation(copie));
        SECURISE(environnement_definition_options(copie,CHAMP_TRAVAIL(environnement, options)));
        SECURISE(environnement_definition_fichiersortie(copie,T_S_(CHAMP(environnement, fichiersortie))));
        SECURISE(environnement_definition_sortie(copie,T_S_(CHAMP(environnement, sortie))));
        SECURISE(environnement_definition_erreur(copie,T_S_(CHAMP(environnement, erreur))));
        SECURISE(environnement_definition_pilerepertoirecourant(copie,CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        SECURISE(environnement_definition_langue(copie,T_S_(CHAMP(environnement, langue))));
        SECURISE(environnement_definition_charset(copie,T_S_(CHAMP(environnement, charset))));
        return RESULTAT_OK;
}

Resultat environnement_destruction(TRAVAIL(Environnement) environnement)
{
        /* D�truit un environnement.
         */
        if(S_T(environnement)==NULL)
                return RESULTAT_OK;
        if(CHAMP(environnement, fichiersortie)!=NULL)
                free(CHAMP(environnement, fichiersortie));
        SECURISE(options_destruction(CHAMP_TRAVAIL(environnement, options)));
        SECURISE(pilenomfichier_destruction(CHAMP_TRAVAIL(environnement, pilerepertoirecourant)));
        free(S_T(environnement));
        S_T(environnement)=NULL;
        return RESULTAT_OK;
}

