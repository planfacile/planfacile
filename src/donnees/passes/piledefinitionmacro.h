/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILEDEFINITIONMACRO__
#define __PILEDEFINITIONMACRO__

#include <src/global/global.h>

typedef struct piledefinitionmacro CONTENEUR(PileDefinitionMacro);

#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/flux/macro.h>
#include <src/donnees/flux/flux.h>

struct piledefinitionmacro
{
        STOCKAGE(Macro) macro;
        //Donne la liste des macros d�finies � un niveau
        //donn�.
        STOCKAGE(PileDefinitionMacro) suivant;
        //Donne la structure du parent dans la pile;
};
/* Cette structure est utilis�e dans la r�duction des
 * macros pour indiquer quelles sont les macros d�finies
 * � un point donn� du flux de commandes.
 */

Resultat piledefinitionmacro_initialisation(TRAVAIL(PileDefinitionMacro) piledefinitionmacro);
/* Initialise la structure de pile.
 */

Resultat piledefinitionmacro_ajout_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL(Macro) macro);
/* Ajoute une liste de macros � la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
 */

Resultat piledefinitionmacro_retrait_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro);
/* Retire une liste de macros de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat piledefinitionmacro_evaluation_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL_SCALAIRE(NomMacro) nommacro, COREFERENCE_SCALAIRE(Booleen) trouve, REFERENCE(Flux) definition, REFERENCE_SCALAIRE(Taille) parametres, REFERENCE_SCALAIRE(Booleen) boucle, COREFERENCE_SCALAIRE(Booleen) appel);
/* Recherche une d�finition de macro par son nom, et marque cette macro comme appell�e.
 * Retourne FAUX dans trouve si la macro n'existe pas. Dans ce cas, les autres param�tres
 * sont inchang�s.
 * Sinon, la d�finition, le nombre de param�tres, la pr�sence du param�tre #0 sont indiqu�s,
 * si les pointeurs sont non NULL.
 * Pour le bol�en d'appel renvoy�, il contient la valeur avant le marquage.
 */

Resultat piledefinitionmacro_liberation_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL_SCALAIRE(NomMacro) nommacro);
/* Recherche une macro, et place son drapeu d'ex�cution � FAUX.
 * Renvoie RESULTAT_ERREUR si le drapeau �tait d�j� � FAUX, ou
 * si la macro n'existe pas.
 */

Resultat piledefinitionmacro_copie(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL(PileDefinitionMacro) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat piledefinitionmacro_destruction(TRAVAIL(PileDefinitionMacro) piledefinitionmacro);
/* D�truit la pile.
 */

#endif
