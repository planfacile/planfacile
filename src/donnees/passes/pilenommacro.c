/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pilenommacro.h"

static Resultat pilenommacro_copieinterne(TRAVAIL(PileNomMacro) original, TRAVAIL(PileNomMacro) copie)
{
        if(S_T(original)==NULL)
                return RESULTAT_OK;
        SECURISE(pilenommacro_copieinterne(CHAMP_TRAVAIL(original, suivant),copie));
        SECURISE(pilenommacro_ajout(copie,T_S_(CHAMP(original, nommacro)),T_S_(CHAMP(original, niveauimbrication))));
        return RESULTAT_OK;
}

Resultat pilenommacro_initialisation(TRAVAIL(PileNomMacro) pilenommacro)
{
        /* Cr�e une pile de nom de macros.
         */
        S_T(pilenommacro)=NULL;
        return RESULTAT_OK;
}

Resultat pilenommacro_ajout(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL_SCALAIRE(NomMacro) nommacro, TRAVAIL_SCALAIRE(Entier) niveauimbrication)
{
        /* Ajoute une nom de macro au sommet de la pile. 
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileNomMacro) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileNomMacro))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pilenommacro);
        S_T(pilenommacro)=nouveau;  
        if((CHAMP_STOCKAGE(nouveau, nommacro)=DUP_CAST(NomMacro, nommacro))==NULL)
        {
                free(nouveau);
                return RESULTAT_ERREUR_MEMOIRE;
        }
        CHAMP_STOCKAGE(nouveau, niveauimbrication)=S_T_(niveauimbrication);
        return RESULTAT_OK;
}

Resultat pilenommacro_retrait(TRAVAIL(PileNomMacro) pilenommacro, COREFERENCE_SCALAIRE(NomMacro) nommacro)
{
        /* Retire le sommet de la pile, et renvoye le nom de macro.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Il faudra lib�rer le nom de macro renvoy�e � la main.
         */
        STOCKAGE(PileNomMacro) ancien;
        ASSERTION(S_T(pilenommacro)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pilenommacro);
        S_T(pilenommacro)=CHAMP_STOCKAGE(ancien, suivant);
        S_C_(nommacro)=CHAMP_STOCKAGE(ancien, nommacro);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pilenommacro_definition_niveauimbrication(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL_SCALAIRE(Entier) niveauimbrication)
{       
        /* D�finit un niveau d'imbrication d'appel de macro
         * Renvoie RESULTAT_ERREUR si pilenommacro vaut NULL
         */
        ASSERTION(S_T(pilenommacro)!=NULL, RESULTAT_ERREUR);

        CHAMP(pilenommacro, niveauimbrication)=S_T_(niveauimbrication);
        return RESULTAT_OK;
}       

Resultat pilenommacro_echange(TRAVAIL(PileNomMacro) pilenommacro)
{
        /* Echange les deux premiers el�ments de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Ne fait rien si la pile contient moins de 2 maillons.
         */
        STOCKAGE(PileNomMacro) temporaire;
        ASSERTION(S_T(pilenommacro)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(pilenommacro, suivant)==NULL)
                return RESULTAT_OK;
        temporaire=S_T(pilenommacro);
        S_T(pilenommacro)=CHAMP_STOCKAGE(temporaire, suivant);
        CHAMP_STOCKAGE(temporaire, suivant)=CHAMP(pilenommacro, suivant);
        CHAMP(pilenommacro, suivant)=temporaire;
        return RESULTAT_OK;
}       

Resultat pilenommacro_appels_successifs(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(Booleen) appelssuccessifs)
{
        /* Renvoie VRAI dans appelssuccessifs si les 2 premiers
         * �l�ments correspondent � des appels de macro successifs.
         * (�l�ments de meme niveau d'imbrications)
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Renvoie FAUX si la pile contient moins de 2 �l�ments.
         */
        ASSERTION(S_T(pilenommacro)!=NULL, RESULTAT_ERREUR);

        T_R_(appelssuccessifs)=T_S_(FAUX);
        if(CHAMP(pilenommacro, suivant)==NULL)
                return RESULTAT_OK;
        if(T_S_(CHAMP(pilenommacro, niveauimbrication))==T_S_(CHAMP_STOCKAGE(CHAMP(pilenommacro, suivant), niveauimbrication)))
                T_R_(appelssuccessifs)=T_S_(VRAI);
        return RESULTAT_OK;
}

Resultat pilenommacro_lecture_nom(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(NomMacro) nommacro)
{
        /* Lit le sommet et renvoye le nom de macro.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        ASSERTION(S_T(pilenommacro)!=NULL, RESULTAT_ERREUR);

        T_R_(nommacro)=T_S_(CHAMP(pilenommacro, nommacro));
        return RESULTAT_OK;
}

Resultat pilenommacro_vide(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pilenommacro)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pilenommacro_copie(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL(PileNomMacro) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(pilenommacro_destruction(copie));
        SECURISE(pilenommacro_initialisation(copie));
        SECURISE(pilenommacro_copieinterne(pilenommacro,copie));
        return RESULTAT_OK;
}

Resultat pilenommacro_destruction(TRAVAIL(PileNomMacro) pilenommacro)
{
        /* D�truit une pile de nom de macros.
         */
        if(S_T(pilenommacro)==NULL)
                return RESULTAT_OK;
        SECURISE(pilenommacro_destruction(CHAMP_TRAVAIL(pilenommacro, suivant)));
        free(CHAMP(pilenommacro, nommacro));
        free(S_T(pilenommacro));
        S_T(pilenommacro)=NULL;
        return RESULTAT_OK;
}

