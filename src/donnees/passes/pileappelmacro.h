/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILEAPPELMACRO__
#define __PILEAPPELMACRO__

#include <src/global/global.h>

typedef struct pileappelmacro CONTENEUR(PileAppelMacro);

#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commandemacro.h>

struct pileappelmacro
{
        STOCKAGE(CommandeMacro) commandemacro;
        //Commande d'appel de macro,
        //avec ses param�tres. Ce sont eux
        //qui sont ici recherch�s.
        STOCKAGE(PileAppelMacro) suivant;
        //Maillon suivant. Cela correspond
        //en fait � l'appel de macro parent.
};
/* Cette pile est utils�e lors de la r�duction des
 * options, pour pouvoir remplacer les commandes de
 * param�tres plac�es dans les d�finitions de macro
 * par leur valeur r�elle.
 */

Resultat pileappelmacro_initialisation(TRAVAIL(PileAppelMacro) pileappelmacro);
/* Initialise une pile d'appels de macro.
 */

Resultat pileappelmacro_ajout_appelmacro(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL(CommandeMacro) commandemacro);
/* Ajoute un appel de macro � la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pileappelmacro_retrait_appelmacro(TRAVAIL(PileAppelMacro) pileappelmacro);
/* Retire un appel de macro de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pileappelmacro_definition_parametre(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) parametre);
/* Change un param�tre du premier appel de macro.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat pileappelmacro_lecture_parametre(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(Flux) parametre);
/* Lit un param�tre du premier appel de macro.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat pileappelmacro_copie(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL(PileAppelMacro) copie);
/* R�alise une copie de la pile.
 * La pile destination est d�truite avant la copie.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pileappelmacro_destruction(TRAVAIL(PileAppelMacro) pileappelmacro);
/* D�truit une pile d'appel de macro.
 */

#endif
