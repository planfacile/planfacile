/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "piledefinitionmacro.h"

static Resultat piledefinitionmacro_copie_interne(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL(PileDefinitionMacro) copie)
{
        /* R�alise la copie des maillons de la pile.
         */
        if(S_T(piledefinitionmacro)==NULL)
        {
                S_T(copie)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(piledefinitionmacro_copie_interne(CHAMP_TRAVAIL(piledefinitionmacro, suivant),copie));
        SECURISE(piledefinitionmacro_ajout_macro(copie,CHAMP_TRAVAIL(piledefinitionmacro, macro)));
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_initialisation(TRAVAIL(PileDefinitionMacro) piledefinitionmacro)
{
        /* Initialise la structure de pile.
         */
        S_T(piledefinitionmacro)=NULL;
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_ajout_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL(Macro) macro)
{
        /* Ajoute une liste de macros � la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         */
        STOCKAGE(PileDefinitionMacro) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileDefinitionMacro))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, macro)=NULL;
        SECURISE(macro_copie(macro,T_S(CHAMP_STOCKAGE(nouveau, macro))));
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(piledefinitionmacro);
        S_T(piledefinitionmacro)=nouveau;
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_retrait_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro)
{
        /* Retire une liste de macros de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(PileDefinitionMacro) supprime;
        ASSERTION(S_T(piledefinitionmacro)!=NULL, RESULTAT_ERREUR);

        supprime=S_T(piledefinitionmacro);
        S_T(piledefinitionmacro)=CHAMP_STOCKAGE(supprime, suivant);
        SECURISE(macro_destruction(T_S(CHAMP_STOCKAGE(supprime, macro))));
        free(supprime);
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_evaluation_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL_SCALAIRE(NomMacro) nommacro, COREFERENCE_SCALAIRE(Booleen) trouve, REFERENCE(Flux) definition, REFERENCE_SCALAIRE(Taille) parametres, REFERENCE_SCALAIRE(Booleen) boucle, COREFERENCE_SCALAIRE(Booleen) appel)
{
        /* Recherche une d�finition de macro par son nom, et marque cette macro comme appell�e.
         * Retourne FAUX dans trouve si la macro n'existe pas. Dans ce cas, les autres param�tres
         * sont inchang�s.
         * Sinon, la d�finition, le nombre de param�tres, la pr�sence du param�tre #0 sont indiqu�s,
         * si les pointeurs sont non NULL.
         * Pour le bol�en d'appel renvoy�, il contient la valeur avant le marquage.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        if(S_T(piledefinitionmacro)==NULL)
        {
                S_C_(trouve)=FAUX;
                return RESULTAT_OK;
        }
        SECURISE(macro_recherche_macro(CHAMP_TRAVAIL(piledefinitionmacro, macro),nommacro,R_T_(indice)));
        if(indice==T_S_(MACRO_NON_TROUVEE))
        {
                SECURISE(piledefinitionmacro_evaluation_macro(CHAMP_TRAVAIL(piledefinitionmacro, suivant),nommacro,trouve,definition,parametres,boucle,appel));
        }
        else
        {
                TRAVAIL(CommandeDefine) commandedefine;
                SECURISE(macro_lecture_macro(CHAMP_TRAVAIL(piledefinitionmacro, macro),indice,R_T(commandedefine)));
                if(definition!=NULL)
                {
                        SECURISE(commandedefine_lecture_definition(commandedefine,definition));
                }
                SECURISE(commandedefine_lecture_parametres(commandedefine,parametres,boucle));
                if(appel!=NULL)
                {
                        SECURISE(commandedefine_lecture_appel(commandedefine,appel));
                }
                SECURISE(commandedefine_definition_appel(commandedefine,T_S_(VRAI)));
                S_C_(trouve)=VRAI;
        }
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_liberation_macro(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL_SCALAIRE(NomMacro) nommacro)
{
        /* Recherche une macro, et place son drapeu d'ex�cution � FAUX.
         * Renvoie RESULTAT_ERREUR si le drapeau �tait d�j� � FAUX, ou
         * si la macro n'existe pas.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        ASSERTION(S_T(piledefinitionmacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(macro_recherche_macro(CHAMP_TRAVAIL(piledefinitionmacro, macro),nommacro,R_T_(indice)));
        if(indice==T_S_(MACRO_NON_TROUVEE))
        {
                SECURISE(piledefinitionmacro_liberation_macro(CHAMP_TRAVAIL(piledefinitionmacro, suivant),nommacro));
        }
        else
        {
                TRAVAIL(CommandeDefine) commandedefine;
                TRAVAIL_SCALAIRE(Booleen) appel;
                SECURISE(macro_lecture_macro(CHAMP_TRAVAIL(piledefinitionmacro, macro),indice,R_T(commandedefine)));
                SECURISE(commandedefine_lecture_appel(commandedefine,R_T_(appel)));
                ASSERTION(appel==T_S_(VRAI), RESULTAT_ERREUR);

                SECURISE(commandedefine_definition_appel(commandedefine,T_S_(FAUX)));
        }
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_copie(TRAVAIL(PileDefinitionMacro) piledefinitionmacro, TRAVAIL(PileDefinitionMacro) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(piledefinitionmacro_destruction(copie));
        SECURISE(piledefinitionmacro_copie_interne(piledefinitionmacro,copie));
        return RESULTAT_OK;
}

Resultat piledefinitionmacro_destruction(TRAVAIL(PileDefinitionMacro) piledefinitionmacro)
{
        /* D�truit la pile.
         */
        if(S_T(piledefinitionmacro)==NULL)
                return RESULTAT_OK;
        SECURISE(piledefinitionmacro_destruction(CHAMP_TRAVAIL(piledefinitionmacro, suivant)));
        SECURISE(macro_destruction(CHAMP_TRAVAIL(piledefinitionmacro, macro)));
        free(S_T(piledefinitionmacro));
        S_T(piledefinitionmacro)=NULL;
        return RESULTAT_OK;
}

