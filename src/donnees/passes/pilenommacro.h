/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILENOMMACRO__
#define __PILENOMMACRO__

#include <src/global/global.h>

typedef struct pilenommacro CONTENEUR(PileNomMacro);

struct pilenommacro
{
        STOCKAGE_SCALAIRE(NomMacro) nommacro;
        //NomMacro � conserver au chaud.
        STOCKAGE_SCALAIRE(Entier) niveauimbrication;
        //Sert � indiquer le degr� d'imbrication dans un autre
        //appel de macro.
        STOCKAGE(PileNomMacro) suivant;
        //Pointeur sur le suivant.
};
/* Pile de nom de macros utilis�es lors de l'analyse de l'entr�e de
 * planfacile, et grrrrmbl � bison et flex !
 */

Resultat pilenommacro_initialisation(TRAVAIL(PileNomMacro) pilenommacro);
/* Cr�e une pile de nom de macros.
 */

Resultat pilenommacro_ajout(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL_SCALAIRE(NomMacro) nommacro, TRAVAIL_SCALAIRE(Entier) niveauimbrication);
/* Ajoute un nom de macro au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pilenommacro_retrait(TRAVAIL(PileNomMacro) pilenommacro, COREFERENCE_SCALAIRE(NomMacro) nommacro);
/* Retire le sommet de la pile, et renvoye le nom de macro.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Il faudra lib�rer le nom de macro renvoy� � la main.
 */

Resultat pilenommacro_definition_niveauimbrication(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL_SCALAIRE(Entier) niveauimbrication);
/* D�finit un niveau d'imbrication d'appel de macro
 * Renvoie RESULTAT_ERREUR si pilenommacro vaut NULL
 */

Resultat pilenommacro_echange(TRAVAIL(PileNomMacro) pilenommacro);
/* Echange les 2 premiers �l�ments de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Ne fait rien si la pile contient moins de 2 �l�ments.
 */

Resultat pilenommacro_appels_successifs(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(Booleen) appels_successifs);
/* Renvoie VRAI dans appels_successifs si les 2 premiers noms
 * de macro correspondent � des appels successifs.
 * Renvoie FAUX si la pile contient moins de 2 �l�ments.
 */

Resultat pilenommacro_lecture_nom(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(NomMacro) nommacro);
/* Lit le sommet et renvoye le nom de macro.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pilenommacro_vide(TRAVAIL(PileNomMacro) pilenommacro, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pilenommacro_copie(TRAVAIL(PileNomMacro) pilenommacro, TRAVAIL(PileNomMacro) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat pilenommacro_destruction(TRAVAIL(PileNomMacro) pilenommacro);
/* D�truit une pile de nom de macros.
 */

#endif
