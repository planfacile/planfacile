/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pileappelmacro.h"

static Resultat pileappelmacro_copie_interne(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL(PileAppelMacro) copie)
{
        /* R�alise une copie des maillons.
         */
        if(S_T(pileappelmacro)==NULL)
        {
                S_T(copie)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(pileappelmacro_copie_interne(CHAMP_TRAVAIL(pileappelmacro, suivant),copie));
        SECURISE(pileappelmacro_ajout_appelmacro(copie,CHAMP_TRAVAIL(pileappelmacro, commandemacro)));
        return RESULTAT_OK;
}

Resultat pileappelmacro_initialisation(TRAVAIL(PileAppelMacro) pileappelmacro)
{
        /* Initialise une pile d'appels de macro.
         */
        S_T(pileappelmacro)=NULL;
        return RESULTAT_OK;
}

Resultat pileappelmacro_ajout_appelmacro(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL(CommandeMacro) commandemacro)
{
        /* Ajoute un appel de macro � la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileAppelMacro) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileAppelMacro))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, commandemacro)=NULL;
        SECURISE(commandemacro_copie(commandemacro,T_S(CHAMP_STOCKAGE(nouveau, commandemacro))));
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pileappelmacro);
        S_T(pileappelmacro)=nouveau;
        return RESULTAT_OK;
}

Resultat pileappelmacro_retrait_appelmacro(TRAVAIL(PileAppelMacro) pileappelmacro)
{
        /* Retire un appel de macro de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(PileAppelMacro) supprime;
        ASSERTION(S_T(pileappelmacro)!=NULL, RESULTAT_ERREUR);

        supprime=S_T(pileappelmacro);
        S_T(pileappelmacro)=CHAMP_STOCKAGE(supprime, suivant);
        SECURISE(commandemacro_destruction(T_S(CHAMP_STOCKAGE(supprime, commandemacro))));
        free(supprime);
        return RESULTAT_OK;
}

Resultat pileappelmacro_definition_parametre(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) parametre)
{
        /* Change un param�tre du premier appel de macro.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(pileappelmacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacro_definition_parametre(CHAMP_TRAVAIL(pileappelmacro, commandemacro),indice,parametre));
        return RESULTAT_OK;
}

Resultat pileappelmacro_lecture_parametre(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(Flux) parametre)
{
        /* Lit un param�tre du premier appel de macro.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(pileappelmacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacro_lecture_parametre(CHAMP_TRAVAIL(pileappelmacro, commandemacro),indice,parametre));
        return RESULTAT_OK;
}

Resultat pileappelmacro_copie(TRAVAIL(PileAppelMacro) pileappelmacro, TRAVAIL(PileAppelMacro) copie)
{
        /* R�alise une copie de la pile.
         * La pile destination est d�truite avant la copie.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        SECURISE(pileappelmacro_destruction(copie));
        SECURISE(pileappelmacro_copie_interne(pileappelmacro,copie));
        return RESULTAT_OK;
}

Resultat pileappelmacro_destruction(TRAVAIL(PileAppelMacro) pileappelmacro)
{
        /* D�truit une pile d'appel de macro.
         */
        if(S_T(pileappelmacro)==NULL)
                return RESULTAT_OK;
        SECURISE(pileappelmacro_destruction(CHAMP_TRAVAIL(pileappelmacro, suivant)));
        SECURISE(commandemacro_destruction(CHAMP_TRAVAIL(pileappelmacro, commandemacro)));
        free(S_T(pileappelmacro));
        S_T(pileappelmacro)=NULL;
        return RESULTAT_OK;
}

