/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ARBREIDEES__
#define __ARBREIDEES__

#include <src/global/global.h>

typedef struct arbreidees CONTENEUR(ArbreIdees);

#include <src/donnees/sortie/denominationidee.h>
#include <src/donnees/sortie/referencesidee.h>

typedef struct arbreideessoussections CONTENEUR_SCALAIRE(ArbreIdeesSousSections);

struct arbreideessoussections
{
        STOCKAGE_SCALAIRE(Taille) nombre;
        //Nombre de sous sections.
        TABLEAU(STOCKAGE(ArbreIdees)) soussection;
        //Sous sections de l'id�e en cours.
};
/* Structure destin�e � recevoir les sous
 * sections d'une id�e.
 */

struct arbreidees
{
        STOCKAGE(DenominationIdee) denomination;
        //D�nomination de l'id�e.
        STOCKAGE_SCALAIRE(NiveauHierarchique) niveau;
        //Niveau hi�rarchique r�el de
        //l'id�e.
        STOCKAGE(ReferencesIdee) references;
        //R�f�rences �manant de cette id�e.
        STOCKAGE_SCALAIRE(ArbreIdeesSousSections) soussections;
        //Sous sections de l'id�e en cous.
};
/* Structure destin�e � la description
 * des diverses id�es du plan.
 */

Resultat arbreidees_initialisation(TRAVAIL(ArbreIdees) arbreidees);
/* Initialise un arbre d'id�es vide.
 */

Resultat arbreidees_creation_idee(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(DenominationIdee) denomination, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau);
/* Cr�e un arbre comportant une seule id�e.
 * Si l'arbre �tait non vide, il est d�truit au pr�alable.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_definition_references(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(ReferencesIdee) references);
/* Place une liste de r�f�rences dans l'id�e racine de l'arbre
 * d'id�e.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat arbreidees_definition_nombresoussections(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Taille) nombre);
/* Indique le nombre de sous sections que va avoir une id�e.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat arbreidees_definition_soussection(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(ArbreIdees) soussection);
/* D�finit une sous section. Exceptionnellement, cette d�finition
 * se fera sans copie ! Il ne faudra donc pas d�truire l'arbre de
 * la sous-section !
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat arbreidees_lecture_denomination(TRAVAIL(ArbreIdees) arbreidees, REFERENCE(DenominationIdee) denomination);
/* Lit la d�nomination de l'id�e racine.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_lecture_niveau(TRAVAIL(ArbreIdees) arbreidees, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Lit le niveau de l'id�e racine.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_lecture_references(TRAVAIL(ArbreIdees) arbreidees, REFERENCE(ReferencesIdee) references);
/* Lit la liste des r�f�rences de l'id�e racine.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_lecture_nombresoussections(TRAVAIL(ArbreIdees) arbreidees, REFERENCE_SCALAIRE(Taille) nombre);
/* Lit le nombre de sous sections de l'id�e racine.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_lecture_soussection(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(ArbreIdees) soussection);
/* Lit une sous section de l'id�e racine.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat arbreidees_recherche_reference(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, TRAVAIL(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveauhierarchique, TRAVAIL_SCALAIRE(Chaine) referencepivot, TRAVAIL_SCALAIRE(TypeIdee) typepivot, REFERENCE_SCALAIRE(TypeReference) typereference);
/* Recherche une id�e par sa r�f�rence exacte.
 * Le bool�en correct indique si la r�f�rence
 * a �t� trouv�e, et dans ce cas, renvoie le
 * type de l'id�e trouv�e dans type.
 * Attention : si plusieurs occurences de la
 * r�f�rence sont trouv�es, le type renvoy�
 * correspond dans l'ordre de priorit� � :
 * IDEE_PRESENTE, IDEE_MANQUANTE, IDEE_GENERIQUE,
 * IDEE_RACINE.
 * Si la r�f�rence est trouv�e apr�s la r�f�rence
 * pivot, le type de r�f�rence sera fix�e �
 * REFERENCE_IRREDUCTIBLE, et � REFERENCE_REDUCTIBLE
 * dans le cas contraire.
 * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
 */

Resultat arbreidees_copie(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(ArbreIdees) copie);
/* R�alise une copie de l'arbre d'id�es.
 * Si la copie �tait non vide, elle est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat arbreidees_destruction(TRAVAIL(ArbreIdees) arbreidees);
/* D�truit un arbre d'id�es.
 */

#endif
