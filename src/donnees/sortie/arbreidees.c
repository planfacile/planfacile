/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "arbreidees.h"

Resultat arbreidees_initialisation(TRAVAIL(ArbreIdees) arbreidees)
{
        /* Initialise un arbre d'id�es vide.
         */
        S_T(arbreidees)=NULL;
        return RESULTAT_OK;
}

Resultat arbreidees_creation_idee(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(DenominationIdee) denomination, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Cr�e un arbre comportant une seule id�e.
         * Si l'arbre �tait non vide, il est d�truit au pr�alable.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        ASSERTION(arbreidees!=T_S_(NULL), RESULTAT_ERREUR);

        SECURISE(arbreidees_destruction(arbreidees));
        ASSERTION((S_T(arbreidees)=NOUVEAU(ArbreIdees))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(arbreidees, denomination)=NULL;
        CHAMP(arbreidees, references)=NULL;
        CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)=0;
        CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection)=NULL;
        SECURISE(denominationidee_copie(denomination,CHAMP_TRAVAIL(arbreidees, denomination)));
        CHAMP(arbreidees, niveau)=niveau;
        return RESULTAT_OK;
}

Resultat arbreidees_definition_references(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(ReferencesIdee) references)
{
        /* Place une liste de r�f�rences dans l'id�e racine de l'arbre
         * d'id�e.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        SECURISE(referencesidee_copie(references,CHAMP_TRAVAIL(arbreidees, references)));
        return RESULTAT_OK;
}

Resultat arbreidees_definition_nombresoussections(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Taille) nombre)
{
        /* Indique le nombre de sous sections que va avoir une id�e.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection)=NOUVEAUX(ArbreIdees, nombre);
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<nombre ; T_S_(indice)++)
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice))=NULL;
        CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)=S_T_(nombre);
        return RESULTAT_OK;
}

Resultat arbreidees_definition_soussection(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(ArbreIdees) soussection)
{
        /* D�finit une sous section. Exceptionnellement, cette d�finition
         * se fera sans copie ! Il ne faudra donc pas d�truire l'arbre de
         * la sous-section !
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)), RESULTAT_ERREUR_DEPASSEMENT);

        SECURISE(arbreidees_destruction(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice))));
        ELEMENT(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice))=S_T(soussection);
        return RESULTAT_OK;
}

Resultat arbreidees_lecture_denomination(TRAVAIL(ArbreIdees) arbreidees, REFERENCE(DenominationIdee) denomination)
{
        /* Lit la d�nomination de l'id�e racine.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        T_R(denomination)=CHAMP_TRAVAIL(arbreidees, denomination);
        return RESULTAT_OK;
}

Resultat arbreidees_lecture_niveau(TRAVAIL(ArbreIdees) arbreidees, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Lit le niveau de l'id�e racine.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        T_R_(niveau)=T_S_(CHAMP(arbreidees, niveau));
        return RESULTAT_OK;
}

Resultat arbreidees_lecture_references(TRAVAIL(ArbreIdees) arbreidees, REFERENCE(ReferencesIdee) references)
{
        /* Lit la liste des r�f�rences de l'id�e racine.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        T_R(references)=CHAMP_TRAVAIL(arbreidees, references);
        return RESULTAT_OK;
}

Resultat arbreidees_lecture_nombresoussections(TRAVAIL(ArbreIdees) arbreidees, REFERENCE_SCALAIRE(Taille) nombre)
{
        /* Lit le nombre de sous sections de l'id�e racine.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        T_R_(nombre)=T_S_(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre));
        return RESULTAT_OK;
}

Resultat arbreidees_lecture_soussection(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(ArbreIdees) soussection)
{
        /* Lit une sous section de l'id�e racine.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)), RESULTAT_ERREUR_DEPASSEMENT);

        T_R(soussection)=ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), indice);
        return RESULTAT_OK;
}

static Resultat arbreidees_rechercheinterne_reference(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, TRAVAIL(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveauhierarchique, TRAVAIL_SCALAIRE(Chaine) referencepivot, TRAVAIL_SCALAIRE(TypeIdee) typepivot, REFERENCE_SCALAIRE(TypeReference) typereference, COREFERENCE_SCALAIRE(TypeReference) typereferenceinstantane)
{
        /* R�alise la recherche effective d'une id�e par sa r�f�rence exacte.
         * Renvoie les m�mes erreurs que la fonction ci-dessous.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(TypeIdee) typeidee;
        TRAVAIL_SCALAIRE(IdIdee) ididee;
        TRAVAIL_SCALAIRE(Indice) indiceidee;
        TRAVAIL_SCALAIRE(Chaine) referenceparenteidee;
        TRAVAIL_SCALAIRE(Chaine) referenceidee;
        TRAVAIL_SCALAIRE(TypeIdee) typeideecourant;
        TRAVAIL_SCALAIRE(IdIdee) idideecourant;
        TRAVAIL_SCALAIRE(Indice) indicemanquantecourant;
        TRAVAIL_SCALAIRE(Chaine) referenceparenteideecourant;
        TRAVAIL_SCALAIRE(Chaine) referenceideecourant;
        TRAVAIL_SCALAIRE(Booleen) videcourant;
        ASSERTION(S_T(arbreidees)!=NULL, RESULTAT_ERREUR);

        SECURISE(denominationidee_lecture(CHAMP_TRAVAIL(arbreidees, denomination),R_T_(typeidee),R_T_(ididee),R_T_(indiceidee),R_T_(referenceparenteidee),R_T_(referenceidee)));
        ASSERTION(typeidee!=T_S_(IDEE_VIDE), RESULTAT_ERREUR_DOMAINE);

        if((typeidee!=T_S_(IDEE_RACINE))&&(STRCMP(reference, referenceidee)==T_S_(0)))
        {
                SECURISE(denominationidee_vide(denomination,R_T_(videcourant)));
                if(videcourant==T_S_(VRAI))
                {
                        SECURISE(denominationidee_definition_ideeracine(denomination));
                        SECURISE(denominationidee_copie(CHAMP_TRAVAIL(arbreidees, denomination),denomination));
                        T_R_(niveauhierarchique)=T_S_(CHAMP(arbreidees, niveau));
                }
                else
                {
                        SECURISE(denominationidee_lecture(denomination,R_T_(typeideecourant),R_T_(idideecourant),R_T_(indicemanquantecourant),R_T_(referenceparenteideecourant),R_T_(referenceideecourant)));
                        switch(typeideecourant)
                        {
                                case T_S_(IDEE_PRESENTE):
                                        if(typeidee==T_S_(IDEE_PRESENTE))
                                        {
                                                SECURISE(denominationidee_copie(CHAMP_TRAVAIL(arbreidees, denomination),denomination));
                                                T_R_(niveauhierarchique)=T_S_(CHAMP(arbreidees, niveau));
                                        }
                                        break;
                                case T_S_(IDEE_MANQUANTE):
                                        if((typeidee==T_S_(IDEE_PRESENTE))||(typeidee==T_S_(IDEE_MANQUANTE)))
                                        {
                                                SECURISE(denominationidee_copie(CHAMP_TRAVAIL(arbreidees, denomination),denomination));
                                                T_R_(niveauhierarchique)=T_S_(CHAMP(arbreidees, niveau));
                                        }
                                        break;
                                case T_S_(IDEE_GENERIQUE):
                                        if((typeidee==T_S_(IDEE_PRESENTE))||(typeidee==T_S_(IDEE_MANQUANTE))||(typeidee==T_S_(IDEE_GENERIQUE)))
                                        {
                                                SECURISE(denominationidee_copie(CHAMP_TRAVAIL(arbreidees, denomination),denomination));
                                                T_R_(niveauhierarchique)=T_S_(CHAMP(arbreidees, niveau));
                                        }
                                        break;
                                case T_S_(IDEE_RACINE):
                                        SECURISE(denominationidee_copie(CHAMP_TRAVAIL(arbreidees, denomination),denomination));
                                        T_R_(niveauhierarchique)=T_S_(CHAMP(arbreidees, niveau));
                                        break;
                                case T_S_(IDEE_VIDE):
                                default:
                                        return RESULTAT_ERREUR_DOMAINE;
                                        break;
                        }
                }
                S_T_(T_R_(typereference))=S_C_(typereferenceinstantane);
                S_C_(correct)=VRAI;
        }
        if((typeidee!=T_S_(IDEE_RACINE))&&(typeidee==typepivot)&&(referencepivot!=NULL)&&(STRCMP(referencepivot, referenceidee)==T_S_(0)))
        {
                S_C_(typereferenceinstantane)=REFERENCE_IRREDUCTIBLE;
        }
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)) ; T_S_(indice)++)
        {
                SECURISE(arbreidees_rechercheinterne_reference(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice)),reference,correct,denomination,niveauhierarchique,referencepivot,typepivot,typereference,typereferenceinstantane));
        }
        return RESULTAT_OK;
}

Resultat arbreidees_recherche_reference(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, TRAVAIL(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveauhierarchique, TRAVAIL_SCALAIRE(Chaine) referencepivot, TRAVAIL_SCALAIRE(TypeIdee) typepivot, REFERENCE_SCALAIRE(TypeReference) typereference)
{
        /* Recherche une id�e par sa r�f�rence exacte.
         * Le bool�en correct indique si la r�f�rence
         * a �t� trouv�e, et dans ce cas, renvoie le
         * type de l'id�e trouv�e dans type.
         * Attention : si plusieurs occurences de la
         * r�f�rence sont trouv�es, le type renvoy�
         * correspond dans l'ordre de priorit� � :
         * IDEE_PRESENTE, IDEE_MANQUANTE, IDEE_GENERIQUE,
         * IDEE_RACINE.
         * Si la r�f�rence est trouv�e apr�s la r�f�rence
         * pivot, le type de r�f�rence sera fix�e �
         * REFERENCE_IRREDUCTIBLE, et � REFERENCE_REDUCTIBLE
         * dans le cas contraire.
         * Renvoie RESULTAT_ERREUR si arbreidees est NULL.
         */
        STOCKAGE_SCALAIRE(TypeReference) typereferenceinstantane;
        S_C_(correct)=FAUX;
        T_R_(typereference)=T_S_(REFERENCE_REDUCTIBLE);
        typereferenceinstantane=REFERENCE_REDUCTIBLE;
        if(S_T(arbreidees)==NULL)
                return RESULTAT_OK;
        SECURISE(denominationidee_destruction(denomination));
        SECURISE(denominationidee_initialisation(denomination));
        SECURISE(arbreidees_rechercheinterne_reference(arbreidees,reference,correct,denomination,niveauhierarchique,referencepivot,typepivot,typereference,C_S_(typereferenceinstantane)));
        return RESULTAT_OK;
}

Resultat arbreidees_copie(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(ArbreIdees) copie)
{
        /* R�alise une copie de l'arbre d'id�es.
         * Si la copie �tait non vide, elle est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        SECURISE(arbreidees_destruction(copie));
        SECURISE(arbreidees_initialisation(copie));
        if(S_T(arbreidees)==NULL)
                return RESULTAT_OK;
        SECURISE(arbreidees_creation_idee(copie,CHAMP_TRAVAIL(arbreidees, denomination),T_S_(CHAMP(arbreidees, niveau))));
        SECURISE(arbreidees_definition_references(copie,CHAMP_TRAVAIL(arbreidees, references)));
        SECURISE(arbreidees_definition_nombresoussections(copie,CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)));
        for(indice=0 ; T_S_(indice)<CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre) ; T_S_(indice)++)
        {
                SECURISE(arbreidees_copie(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice)),
                                        ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(copie, soussections), soussection), T_S_(indice))));
        }
        return RESULTAT_OK;
}

Resultat arbreidees_destruction(TRAVAIL(ArbreIdees) arbreidees)
{
        /* D�truit un arbre d'id�es.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(arbreidees)==NULL)
                return RESULTAT_OK;
        SECURISE(denominationidee_destruction(CHAMP_TRAVAIL(arbreidees, denomination)));
        SECURISE(referencesidee_destruction(CHAMP_TRAVAIL(arbreidees, references)));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), nombre)) ; T_S_(indice)++)
        {
                SECURISE(arbreidees_destruction(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection), T_S_(indice))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(arbreidees, soussections), soussection));
        free(S_T(arbreidees));
        S_T(arbreidees)=NULL;
        return RESULTAT_OK;
}

