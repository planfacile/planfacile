/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "denominationidee.h"

Resultat denominationidee_initialisation(TRAVAIL(DenominationIdee) denominationidee)
{
        /* Cr�e une d�nomination d'id�e vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION((S_T(denominationidee)=NOUVEAU(DenominationIdee))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(denominationidee, type)=IDEE_VIDE;
        CHAMP(denominationidee, reference)=NULL;
        return RESULTAT_OK;
}

Resultat denominationidee_definition_ideepresente(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(IdIdee) ididee, TRAVAIL(Flux) reference)
{
        /* D�finit une d�nomination d'id�e pr�sente.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(denominationidee, type)==IDEE_VIDE, RESULTAT_ERREUR_DOMAINE);

        CHAMP(denominationidee, type)=IDEE_PRESENTE;
        CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), idideepresente)=S_T_(ididee);
        SECURISE(flux_texte(reference,R_S_(CHAMP(denominationidee, reference))));
        return RESULTAT_OK;
}

Resultat denominationidee_definition_ideemanquante(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) reference)
{
        /* D�finit une d�nomination d'id�e manquante.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(denominationidee, type)==IDEE_VIDE, RESULTAT_ERREUR_DOMAINE);

        CHAMP(denominationidee, type)=IDEE_MANQUANTE;
        CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), indicemanquante)=S_T_(indice);
        SECURISE(flux_reference_manquante(reference,indice,R_S_(CHAMP(denominationidee, reference))));
        return RESULTAT_OK;
}

Resultat denominationidee_definition_ideegenerique(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(Chaine) referenceparente, TRAVAIL(Flux) reference)
{
        /* D�finit une d�nomination d'id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);
        if(CHAMP(denominationidee, type)!=IDEE_VIDE)
                return RESULTAT_ERREUR_DOMAINE;
        CHAMP(denominationidee, type)=IDEE_GENERIQUE;
        CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), referenceparentegenerique)=DUP_CAST(Chaine, referenceparente);
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), referenceparentegenerique)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(flux_reference_generique(reference,referenceparente,R_S_(CHAMP(denominationidee, reference))));
        return RESULTAT_OK;
}

Resultat denominationidee_definition_ideeracine(TRAVAIL(DenominationIdee) denominationidee)
{
        /* D�finit une d�nomination d'id�e racine.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(denominationidee, type)==IDEE_VIDE, RESULTAT_ERREUR_DOMAINE);

        CHAMP(denominationidee, type)=IDEE_RACINE;
        CHAMP(denominationidee, reference)=NULL;
        return RESULTAT_OK;
}

Resultat denominationidee_vide(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI si la d�nomination d'id�e est vide, ou non.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);

        T_R_(vide)=(T_S_(CHAMP(denominationidee, type))==T_S_(IDEE_VIDE));
        return RESULTAT_OK;
}

Resultat denominationidee_lecture(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(TypeIdee) type, REFERENCE_SCALAIRE(IdIdee) ididee, REFERENCE_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(Chaine) referenceparente, REFERENCE_SCALAIRE(Chaine) reference)
{
        /* Lit une d�nomination d'id�e. Le type est renvoy�, et les param�tres
         * suivant donne la d�nomination correspondant au type renvoy�.
         * Dans le cas o� la chaine est renvoy�e, il n'y a pas besoin
         * de la lib�rer.
         * Enfin, la fonction renvoie la r�f�rence exacte de l'id�e
         * ainsi d�nom�e. Cette chaine n'a pas � �tre lib�r�e.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);

        T_R_(type)=T_S_(CHAMP(denominationidee, type));
        T_R_(reference)=T_S_(CHAMP(denominationidee, reference));
        switch(T_S_(CHAMP(denominationidee, type)))
        {
                case T_S_(IDEE_PRESENTE):
                        T_R_(ididee)=T_S_(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), idideepresente));
                        break;
                case T_S_(IDEE_MANQUANTE):
                        T_R_(indice)=T_S_(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), indicemanquante));
                        break;
                case T_S_(IDEE_GENERIQUE):
                        T_R_(referenceparente)=T_S_(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), referenceparentegenerique));
                        break;
                case T_S_(IDEE_RACINE):
                        break;
                case T_S_(IDEE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

Resultat denominationidee_lecture_reference(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(Chaine) reference)
{
        /* Lit la r�f�rence d'une d�nomination d'id�e. Enfin, la fonction
         * renvoie la r�f�rence exacte de l'id�e ainsi d�nom�e.
         * Cette chaine n'a pas � �tre lib�r�e.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);

        T_R_(reference)=T_S_(CHAMP(denominationidee, reference));
        return RESULTAT_OK;
}

Resultat denominationidee_copie(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL(DenominationIdee) copie)
{
        /* R�alise une copie de la d�nomination.
         * Si la copie �tait non vide, elle est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(denominationidee)!=NULL, RESULTAT_ERREUR);

        SECURISE(denominationidee_destruction(copie));
        SECURISE(denominationidee_initialisation(copie));
        CHAMP(copie, type)=CHAMP(denominationidee, type);
        switch(T_S_(CHAMP(denominationidee, type)))
        {
                case T_S_(IDEE_PRESENTE):
                        CHAMP_STOCKAGE_(CHAMP(copie, denomination), idideepresente)=CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), idideepresente);
                        break;
                case T_S_(IDEE_MANQUANTE):
                        CHAMP_STOCKAGE_(CHAMP(copie, denomination), indicemanquante)=CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), indicemanquante);
                        break;
                case T_S_(IDEE_GENERIQUE):
                        {
                        CHAMP_STOCKAGE_(CHAMP(copie, denomination), referenceparentegenerique)=DUP_CAST(Chaine, T_S_(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), referenceparentegenerique)));
                        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, denomination), referenceparentegenerique)!=NULL, RESULTAT_ERREUR_MEMOIRE);
                        }
                        break;
                case T_S_(IDEE_RACINE):
                        break;
                case T_S_(IDEE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        if(CHAMP(denominationidee, reference)==NULL)
        {
                CHAMP(copie, reference)=NULL;
        }
        else
        {
                CHAMP(copie, reference)=DUP_CAST(Chaine, T_S_(CHAMP(denominationidee, reference)));
                ASSERTION(CHAMP(copie, reference)!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        return RESULTAT_OK;
}

Resultat denominationidee_destruction(TRAVAIL(DenominationIdee) denominationidee)
{
        /* D�truit une d�nomination d'id�e.
         */
        if(S_T(denominationidee)==NULL)
                return RESULTAT_OK;
        switch(CHAMP(denominationidee, type))
        {
                case T_S_(IDEE_PRESENTE):
                case T_S_(IDEE_MANQUANTE):
                        break;
                case T_S_(IDEE_GENERIQUE):
                        free(CHAMP_STOCKAGE_(CHAMP(denominationidee, denomination), referenceparentegenerique));
                        break;
                case T_S_(IDEE_RACINE):
                        break;
                case T_S_(IDEE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        if(CHAMP(denominationidee, reference)!=NULL)
                free(CHAMP(denominationidee, reference));
        free(S_T(denominationidee));
        S_T(denominationidee)=NULL;
        return RESULTAT_OK;
}

