/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __REFERENCESIDEE__
#define __REFERENCESIDEE__

#include <src/global/global.h>

typedef struct referencesidee CONTENEUR(ReferencesIdee);

#include <src/donnees/sortie/denominationidee.h>
#include <src/donnees/idees/idees.h>

typedef struct referenceidee CONTENEUR_SCALAIRE(ReferenceIdee);

struct referenceidee
{
        STOCKAGE_SCALAIRE(TypeReference) type;
        //Type de la r�f�rence.
        STOCKAGE_SCALAIRE(NiveauHierarchique) niveau;
        //Niveau de l'id�e destination.
        STOCKAGE(DenominationIdee) denomination;
        //D�nomination de la destination.
};
/* Cette structure sert � pr�ciser une r�f�rence.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

struct referencesidee
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Indique la place prise en m�moire.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Indique la taille utilis�e du tableau.
        TABLEAU_(STOCKAGE_SCALAIRE(ReferenceIdee)) reference;
        //Les r�f�rences sont plac�es dans ce tableau.
};
/* Structure servant � enregister toutes les r�f�rences
 * partant d'une id�e.
 */

Resultat referencesidee_initialisation(TRAVAIL(ReferencesIdee) referencesidee);
/* Cr�e une structure d'enregistement de r�f�rences.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat referencesidee_ajout(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL_SCALAIRE(TypeReference) type, TRAVAIL(DenominationIdee) denomination, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau);
/* Ajoute une r�f�rence dans le vecteur de r�f�rences.
 * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat referencesidee_taille(TRAVAIL(ReferencesIdee) referencesidee, REFERENCE_SCALAIRE(Taille) taille);
/* Renvoie le nombre de r�f�rences contenues dans la structure.
 * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
 */

Resultat referencesidee_lecture(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(TypeReference) type, REFERENCE(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Lit une r�f�rence dans le vecteur de r�f�rences.
 * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat referencesidee_recherche(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(TypeReference) type, REFERENCE(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Recherche une r�f�rence dans le vecteur de r�f�rences.
 * Le bool�en indique si la recherche a aboutie.
 * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat referencesidee_copie(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL(ReferencesIdee) copie);
/* R�alise une copie des r�f�rences d'id�e.
 * Si la copie �tait non vide, elle est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat referencesidee_destruction(TRAVAIL(ReferencesIdee) referencesidee);
/* D�truit les r�f�rences d'id�e.
 */

#endif
