/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "referencesidee.h"

#define TAILLEINIT        5
#define MULTTAILLE        2

Resultat referencesidee_initialisation(TRAVAIL(ReferencesIdee) referencesidee)
{
        /* Cr�e une structure d'enregistement de r�f�rences.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION((S_T(referencesidee)=NOUVEAU(ReferencesIdee))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        ASSERTION((CHAMP(referencesidee, reference)=NOUVEAUX_(ReferenceIdee, T_S_(TAILLEINIT)))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(referencesidee, memoire)=TAILLEINIT;
        CHAMP(referencesidee, taille)=0;
        return RESULTAT_OK;
}

Resultat referencesidee_ajout(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL_SCALAIRE(TypeReference) type, TRAVAIL(DenominationIdee) denomination, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Ajoute une r�f�rence dans le vecteur de r�f�rences.
         * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(referencesidee)!=NULL, RESULTAT_ERREUR);

        if(T_S_(CHAMP(referencesidee, taille))>=T_S_(CHAMP(referencesidee, memoire)))
        {
                TABLEAU_(STOCKAGE_SCALAIRE(ReferenceIdee)) nouveau;
                nouveau=REALLOCATION_CAST_(CHAMP(referencesidee, reference), ReferenceIdee, T_S_(CHAMP(referencesidee, memoire))*T_S_(MULTTAILLE));
                ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                CHAMP(referencesidee, reference)=nouveau;
                CHAMP(referencesidee, memoire)*=T_S_(MULTTAILLE);
        }
        CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(CHAMP(referencesidee, taille))), type)=type;
        CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(CHAMP(referencesidee, taille))), niveau)=niveau;
        CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(CHAMP(referencesidee, taille))), denomination)=NULL;
        SECURISE(denominationidee_copie(denomination,T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(CHAMP(referencesidee, taille))++), denomination))));
        return RESULTAT_OK;
}

Resultat referencesidee_taille(TRAVAIL(ReferencesIdee) referencesidee, REFERENCE_SCALAIRE(Taille) taille)
{
        /* Renvoie le nombre de r�f�rences contenues dans la structure.
         * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
         */
        ASSERTION(S_T(referencesidee)!=NULL, RESULTAT_ERREUR);

        T_R_(taille)=T_S_(CHAMP(referencesidee, taille));
        return RESULTAT_OK;
}

Resultat referencesidee_lecture(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(TypeReference) type, REFERENCE(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Lit une r�f�rence dans le vecteur de r�f�rences.
         * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(referencesidee)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(referencesidee, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        T_R_(type)=T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), type));
        T_R_(niveau)=T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), niveau));
        T_R(denomination)=T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), denomination));
        return RESULTAT_OK;
}

Resultat referencesidee_recherche(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(TypeReference) type, REFERENCE(DenominationIdee) denomination, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Recherche une r�f�rence dans le vecteur de r�f�rences.
         * Le bool�en indique si la recherche a aboutie.
         * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(TypeIdee) typeidee;
        TRAVAIL_SCALAIRE(IdIdee) idideepresente;
        TRAVAIL_SCALAIRE(Indice) indicemanquante;
        TRAVAIL_SCALAIRE(Chaine) referenceparentegenerique;
        TRAVAIL_SCALAIRE(Chaine) referencereelle;
        ASSERTION(S_T(referencesidee)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(referencesidee, taille)) ; T_S_(indice)++)
        {
                SECURISE(denominationidee_lecture(T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(indice)), denomination)),R_T_(typeidee),R_T_(idideepresente),R_T_(indicemanquante),R_T_(referenceparentegenerique),R_T_(referencereelle)));
                if(STRCMP(reference, referencereelle)==T_S_(0))
                {
                        S_C_(correct)=VRAI;
                        T_R_(type)=T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), type));
                        T_R_(niveau)=T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), niveau));
                        T_R(denomination)=T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), indice), denomination));
                        return RESULTAT_OK;
                }
        }
        S_C_(correct)=FAUX;
        return RESULTAT_OK;
}

Resultat referencesidee_copie(TRAVAIL(ReferencesIdee) referencesidee, TRAVAIL(ReferencesIdee) copie)
{
        /* R�alise une copie des r�f�rences d'id�e.
         * Si la copie �tait non vide, elle est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR si referencesidee est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(referencesidee)!=NULL, RESULTAT_ERREUR);

        SECURISE(referencesidee_destruction(copie));
        SECURISE(referencesidee_initialisation(copie));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(referencesidee, taille)) ; T_S_(indice)++)
        {
                SECURISE(referencesidee_ajout(copie,T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(indice)), type)),T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(indice)), denomination)),T_S_(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(indice)), niveau))));
        }
        return RESULTAT_OK;
}

Resultat referencesidee_destruction(TRAVAIL(ReferencesIdee) referencesidee)
{
        /* D�truit les r�f�rences d'id�e.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(referencesidee)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<CHAMP(referencesidee, taille) ; T_S_(indice)++)
        {
                SECURISE(denominationidee_destruction(T_S(CHAMP_STOCKAGE_(ELEMENT(CHAMP(referencesidee, reference), T_S_(indice)), denomination))));
        }
        free(CHAMP(referencesidee, reference));
        free(S_T(referencesidee));
        S_T(referencesidee)=NULL;
        return RESULTAT_OK;
}

