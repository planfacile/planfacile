/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "performancesplan.h"

#define PERFORMANCE_VIDE        -1

Resultat performancesplan_initialisation(TRAVAIL(PerformancesPlan) performancesplan)
{
        /* Cr�e une structure d'enregistrement des informations
         * de performances du plan calcul�.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation m�moire.
         */
        ASSERTION((S_T(performancesplan)=NOUVEAU(PerformancesPlan))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(performancesplan, nombreideestotal)             =PERFORMANCE_VIDE;
        CHAMP(performancesplan, nombreideesmanquantes)        =PERFORMANCE_VIDE;
        CHAMP(performancesplan, nombreideesgeneralites)       =PERFORMANCE_VIDE;
        CHAMP(performancesplan, nombrereferencestotal)        =PERFORMANCE_VIDE;
        CHAMP(performancesplan, nombrereferencesirreductibles)=PERFORMANCE_VIDE;
        return RESULTAT_OK;
}

Resultat performancesplan_renseignement(TRAVAIL(PerformancesPlan) performancesplan, TRAVAIL_SCALAIRE(Statistiques) statisques)
{
        /* Renseigne les divers champs de performances du plan.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        CHAMP(performancesplan, nombreideestotal)             =CHAMP_(statisques, nombreideestotal);
        CHAMP(performancesplan, nombreideesmanquantes)        =CHAMP_(statisques, nombreideesmanquantes);
        CHAMP(performancesplan, nombreideesgeneralites)       =CHAMP_(statisques, nombreideesgeneralites);
        CHAMP(performancesplan, nombrereferencestotal)        =CHAMP_(statisques, nombrereferencestotal);
        CHAMP(performancesplan, nombrereferencesirreductibles)=CHAMP_(statisques, nombrereferencesirreductibles);
        return RESULTAT_OK;
}

Resultat performancesplan_lecture_nombreideestotal(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideestotal)
{
        /* Renvoie le nombre d'id�es total du plan calcul�.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        T_R_(nombreideestotal)=T_S_(CHAMP(performancesplan, nombreideestotal));
        return RESULTAT_OK;
}

Resultat performancesplan_lecture_nombreideesmanquantes(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideesmanquantes)
{
        /* Renvoie le nombre d'id�es manquantes du plan calcul�.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        T_R_(nombreideesmanquantes)=T_S_(CHAMP(performancesplan, nombreideesmanquantes));
        return RESULTAT_OK;
}

Resultat performancesplan_lecture_nombreideesgeneralites(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideesgeneralites)
{
        /* Renvoie le nombre d'id�es g�n�riques du plan calcul�.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        T_R_(nombreideesgeneralites)=T_S_(CHAMP(performancesplan, nombreideesgeneralites));
        return RESULTAT_OK;
}

Resultat performancesplan_lecture_nombrereferencestotal(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombrereferencestotal)
{
        /* Renvoie le nombre de r�f�rences total du plan calcul�.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        T_R_(nombrereferencestotal)=T_S_(CHAMP(performancesplan, nombrereferencestotal));
        return RESULTAT_OK;
}

Resultat performancesplan_lecture_nombrereferencesirreductibles(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombrereferencesirreductibles)
{
        /* Renvoie le nombre de r�f�rences irr�ductibles du plan calcul�.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        T_R_(nombrereferencesirreductibles)=T_S_(CHAMP(performancesplan, nombrereferencesirreductibles));
        return RESULTAT_OK;
}

Resultat performancesplan_copie(TRAVAIL(PerformancesPlan) performancesplan, TRAVAIL(PerformancesPlan) copie)
{
        /* R�alise une copie des performances du plan calcul� .
         * Si la copie �tait non vide, elle est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        ASSERTION(S_T(performancesplan)!=NULL, RESULTAT_ERREUR);

        SECURISE(performancesplan_destruction(copie));
        SECURISE(performancesplan_initialisation(copie));
        CHAMP(copie, nombreideestotal)             =CHAMP(performancesplan, nombreideestotal);
        CHAMP(copie, nombreideesmanquantes)        =CHAMP(performancesplan, nombreideesmanquantes);
        CHAMP(copie, nombreideesgeneralites)       =CHAMP(performancesplan, nombreideesgeneralites);
        CHAMP(copie, nombrereferencestotal)        =CHAMP(performancesplan, nombrereferencestotal);
        CHAMP(copie, nombrereferencesirreductibles)=CHAMP(performancesplan, nombrereferencesirreductibles);
        return RESULTAT_OK;
}

Resultat performancesplan_destruction(TRAVAIL(PerformancesPlan) performancesplan)
{
        /* D�truit des performances du plan calcul�.
         */
        if(S_T(performancesplan)==NULL)
                return RESULTAT_OK;
        free(S_T(performancesplan));
        S_T(performancesplan)=NULL;
        return RESULTAT_OK;
}

