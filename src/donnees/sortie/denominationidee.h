/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __DENOMINATIONIDEE__
#define __DENOMINATIONIDEE__

#include <src/global/global.h>

typedef struct denominationidee CONTENEUR(DenominationIdee);

#include <src/donnees/flux/flux.h>

struct denominationidee
{
        STOCKAGE_SCALAIRE(TypeIdee) type;
        //Type de l'id�e d�nomm�e.
        union
        {
                STOCKAGE_SCALAIRE(IdIdee) idideepresente;
                STOCKAGE_SCALAIRE(Indice) indicemanquante;
                STOCKAGE_SCALAIRE(Chaine) referenceparentegenerique;
        } denomination;
        //Caract�ristique de la d�nomination
        //de l'id�e.
        STOCKAGE_SCALAIRE(Chaine) reference;
        //R�f�rence exacte de l'id�e.
};
/* Structure servant � indiquer une id�e de
 * mani�re pr�cise, et ce quelque soit son
 * type.
 */

Resultat denominationidee_initialisation(TRAVAIL(DenominationIdee) denominationidee);
/* Cr�e une d�nomination d'id�e vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_definition_ideepresente(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(IdIdee) ididee, TRAVAIL(Flux) reference);
/* D�finit une d�nomination d'id�e pr�sente.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_definition_ideemanquante(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) reference);
/* D�finit une d�nomination d'id�e manquante.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_definition_ideegenerique(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL_SCALAIRE(Chaine) referenceparente, TRAVAIL(Flux) reference);
/* D�finit une d�nomination d'id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_definition_ideeracine(TRAVAIL(DenominationIdee) denominationidee);
/* D�finit une d�nomination d'id�e racine.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination n'�tait pas vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_vide(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI si la d�nomination d'id�e est vide, ou non.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 */

Resultat denominationidee_lecture(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(TypeIdee) type, REFERENCE_SCALAIRE(IdIdee) ididee, REFERENCE_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(Chaine) referenceparente, REFERENCE_SCALAIRE(Chaine) reference);
/* Lit une d�nomination d'id�e. Le type est renvoy�, et les param�tres
 * suivant donne la d�nomination correspondant au type renvoy�.
 * Dans le cas o� la chaine est renvoy�e, il n'y a pas besoin
 * de la lib�rer.
 * Enfin, la fonction renvoie la r�f�rence exacte de l'id�e
 * ainsi d�nom�e. Cette chaine n'a pas � �tre lib�r�e.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_lecture_reference(TRAVAIL(DenominationIdee) denominationidee, REFERENCE_SCALAIRE(Chaine) reference);
/* Lit la r�f�rence d'une d�nomination d'id�e. Enfin, la fonction
 * renvoie la r�f�rence exacte de l'id�e ainsi d�nom�e.
 * Cette chaine n'a pas � �tre lib�r�e.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_copie(TRAVAIL(DenominationIdee) denominationidee, TRAVAIL(DenominationIdee) copie);
/* R�alise une copie de la d�nomination.
 * Si la copie �tait non vide, elle est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR si denominationidee est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si la d�nomination �tait vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat denominationidee_destruction(TRAVAIL(DenominationIdee) denominationidee);
/* D�truit une d�nomination d'id�e.
 */

#endif
