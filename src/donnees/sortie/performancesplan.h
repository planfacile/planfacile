/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PERFORMANCESPLAN__
#define __PERFORMANCESPLAN__

#include <src/global/global.h>

typedef struct performancesplan CONTENEUR(PerformancesPlan);

struct performancesplan
{
        STOCKAGE_SCALAIRE(Indice) nombreideestotal;
        //Nombres d'id�es effectivement pr�sentes
        //dans le document produit.
        STOCKAGE_SCALAIRE(Indice) nombreideesmanquantes;
        //Nombre d'id�es manquantes ajout�es par
        //l'algorithme de calcul du plan.
        STOCKAGE_SCALAIRE(Indice) nombreideesgeneralites;
        //Nombre d'id�es g�n�riques ajout�es par
        //l'algorithme de calcul du plan.
        STOCKAGE_SCALAIRE(Indice) nombrereferencestotal;
        //Nombre de r�f�rences pr�sentes dans le
        //document final.
        STOCKAGE_SCALAIRE(Indice) nombrereferencesirreductibles;
        //Donne le nombre d'id�es irr�ductibles
        //restantes apr�s le calcul du plan.
};
/* Structure servant � recevoir des performances
 * sur le plan calcul�.
 */

Resultat performancesplan_initialisation(TRAVAIL(PerformancesPlan) performancesplan);
/* Cr�e une structure d'enregistrement des informations
 * de performances du plan calcul�.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation m�moire.
 */

Resultat performancesplan_renseignement(TRAVAIL(PerformancesPlan) performancesplan, TRAVAIL_SCALAIRE(Statistiques) statisques);
/* Renseigne les divers champs de performances du plan.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_lecture_nombreideestotal(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideestotal);
/* Renvoie le nombre d'id�es total du plan calcul�.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_lecture_nombreideesmanquantes(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideesmanquantes);
/* Renvoie le nombre d'id�es manquantes du plan calcul�.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_lecture_nombreideesgeneralites(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombreideesgeneralites);
/* Renvoie le nombre d'id�es g�n�riques du plan calcul�.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_lecture_nombrereferencestotal(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombrereferencestotal);
/* Renvoie le nombre de r�f�rences total du plan calcul�.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_lecture_nombrereferencesirreductibles(TRAVAIL(PerformancesPlan) performancesplan, REFERENCE_SCALAIRE(Indice) nombrereferencesirreductibles);
/* Renvoie le nombre de r�f�rences irr�ductibles du plan calcul�.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 */

Resultat performancesplan_copie(TRAVAIL(PerformancesPlan) performancesplan, TRAVAIL(PerformancesPlan) copie);
/* R�alise une copie des performances du plan calcul� .
 * Si la copie �tait non vide, elle est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR si performancesplan est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
 * m�moire.
 */

Resultat performancesplan_destruction(TRAVAIL(PerformancesPlan) performancesplan);
/* D�truit des performances du plan calcul�.
 */

#endif
