/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "actionflux.h"

static Resultat actionflux_pilecontexte_initialisation(TRAVAIL(PileContexte) pilecontexte)
{
        /* Initialise une pile de contextes.
         */
        S_T(pilecontexte)=NULL;
        return RESULTAT_OK;
}

static Resultat actionflux_pilecontexte_ajout_contexte(TRAVAIL(PileContexte) pilecontexte, TRAVAIL_SCALAIRE(TypeFlux) contexte, TRAVAIL_SCALAIRE(TypeContexte) type)
{
        /* Ajoute un contexte � une pile de contextes.
         */
        STOCKAGE(PileContexte) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileContexte))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, contexte)=S_T_(contexte);
        CHAMP_STOCKAGE(nouveau, type)=S_T_(type);
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pilecontexte);
        if(S_T(pilecontexte)==NULL)
        {
                CHAMP_STOCKAGE(nouveau, prochain)=NULL;
        }
        else
        {
                if(T_S_(CHAMP(pilecontexte, type))==T_S_(CONTEXTE_CONTEXTE))
                        CHAMP_STOCKAGE(nouveau, prochain)=S_T(pilecontexte);
                else
                        CHAMP_STOCKAGE(nouveau, prochain)=CHAMP(pilecontexte, prochain);
        }
        S_T(pilecontexte)=nouveau;
        return RESULTAT_OK;
}

static Resultat actionflux_pilecontexte_retrait_contexte(TRAVAIL(PileContexte) pilecontexte)
{
        /* Retire un contexte de la pile.
         */
        STOCKAGE(PileContexte) supprime;
        ASSERTION(S_T(pilecontexte)!=NULL, RESULTAT_ERREUR);

        supprime=S_T(pilecontexte);
        S_T(pilecontexte)=CHAMP_STOCKAGE(supprime, suivant);
        free(supprime);
        return RESULTAT_OK;
}

static Resultat actionflux_pilecontexte_destruction(TRAVAIL(PileContexte) pilecontexte)
{
        /* D�truit une pile de contextes.
         */
        if(S_T(pilecontexte)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_pilecontexte_destruction(CHAMP_TRAVAIL(pilecontexte, suivant)));
        free(S_T(pilecontexte));
        return RESULTAT_OK;
}

static Resultat actionflux_pilecontexte_copieinterne(TRAVAIL(PileContexte) pilecontexte, TRAVAIL(PileContexte) copie)
{
        /* Copie les maillons de la pile de contextes.
         */
        if(S_T(pilecontexte)==NULL)
        {
                S_T(copie)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(actionflux_pilecontexte_copieinterne(CHAMP_TRAVAIL(pilecontexte, suivant),copie));
        SECURISE(actionflux_pilecontexte_ajout_contexte(copie,T_S_(CHAMP(pilecontexte, contexte)),T_S_(CHAMP(pilecontexte, type))));
        return RESULTAT_OK;
}

static Resultat actionflux_pilecontexte_copie(TRAVAIL(PileContexte) pilecontexte, TRAVAIL(PileContexte) copie)
{
        /* Copie une pile de contextes.
         */
        SECURISE(actionflux_pilecontexte_destruction(copie));
        SECURISE(actionflux_pilecontexte_initialisation(copie));
        SECURISE(actionflux_pilecontexte_copieinterne(pilecontexte,copie));
        return RESULTAT_OK;
}

Resultat actionflux_initialisation(TRAVAIL(ActionFlux) action)
{
        /* Cr�e et initialise une structure d'action sur flux.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(action)=NOUVEAU(ActionFlux))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_pilecontexte_initialisation(CHAMP_TRAVAIL(action, contexte)));
        CHAMP(action, principal)=NULL;
        CHAMP(action, commentaire)=NULL;
        CHAMP(action, compilateur)=NULL;
        CHAMP(action, options)=NULL;
        CHAMP(action, macro_definition)=NULL;
        CHAMP(action, macro_parametre)=NULL;
        CHAMP(action, message)=NULL;
        CHAMP(action, document)=NULL;
        CHAMP(action, niveau)=NULL;
        CHAMP(action, pertinence)=NULL;
        CHAMP(action, section_nom)=NULL;
        CHAMP(action, section_format)=NULL;
        CHAMP(action, section_section)=NULL;
        CHAMP(action, reference_format)=NULL;
        CHAMP(action, reference)=NULL;
        CHAMP(action, reference_automatique)=NULL;
        CHAMP(action, titre)=NULL;
        CHAMP(action, titre_automatique)=NULL;
        CHAMP(action, texte)=NULL;
        CHAMP(action, texte_reductible)=NULL;
        CHAMP(action, texte_irreductible)=NULL;
        CHAMP(action, texte_manquante)=NULL;
        CHAMP(action, texte_generique)=NULL;
        CHAMP(action, indice)=NULL;
        return RESULTAT_OK;
}

Resultat actionflux_ajout_contexte(TRAVAIL(ActionFlux) action, TRAVAIL_SCALAIRE(TypeFlux) contexte, TRAVAIL_SCALAIRE(TypeContexte) type)
{
        /* Ajoute un contexte � la pile de contextes.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(actionflux_pilecontexte_ajout_contexte(CHAMP_TRAVAIL(action, contexte),contexte,type));
        return RESULTAT_OK;
}

Resultat actionflux_retrait_contexte(TRAVAIL(ActionFlux) action)
{
        /* Retire un contexte de la pile de contextes.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(actionflux_pilecontexte_retrait_contexte(CHAMP_TRAVAIL(action, contexte)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_principal                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) principal                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(principal,CHAMP_TRAVAIL(action, principal)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_commentaire                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) commentaire                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(commentaire,CHAMP_TRAVAIL(action, commentaire)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_compilateur                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) compilateur                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(compilateur,CHAMP_TRAVAIL(action, compilateur)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_options                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) options                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(options,CHAMP_TRAVAIL(action, options)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_macro_definition                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) macro_definition                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(macro_definition,CHAMP_TRAVAIL(action, macro_definition)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_macro_parametre                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) macro_parametre                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(macro_parametre,CHAMP_TRAVAIL(action, macro_parametre)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_message                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) message                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(message,CHAMP_TRAVAIL(action, message)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_document                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) document                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(document,CHAMP_TRAVAIL(action, document)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_niveau                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) niveau                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(niveau,CHAMP_TRAVAIL(action, niveau)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_pertinence                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) pertinence                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(pertinence,CHAMP_TRAVAIL(action, pertinence)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_section_nom                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_nom                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(section_nom,CHAMP_TRAVAIL(action, section_nom)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_section_format                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_format                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(section_format,CHAMP_TRAVAIL(action, section_format)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_section_section                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_section                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(section_section,CHAMP_TRAVAIL(action, section_section)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_reference_format                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference_format                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(reference_format,CHAMP_TRAVAIL(action, reference_format)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_reference                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(reference,CHAMP_TRAVAIL(action, reference)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_reference_automatique        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference_automatique        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(reference_automatique,CHAMP_TRAVAIL(action, reference_automatique)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_titre                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) titre                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(titre,CHAMP_TRAVAIL(action, titre)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_titre_automatique        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) titre_automatique                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(titre_automatique,CHAMP_TRAVAIL(action, titre_automatique)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_texte                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(texte,CHAMP_TRAVAIL(action, texte)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_texte_reductible                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_reductible                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(texte_reductible,CHAMP_TRAVAIL(action, texte_reductible)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_texte_irreductible        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_irreductible                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(texte_irreductible,CHAMP_TRAVAIL(action, texte_irreductible)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_texte_manquante                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_manquante                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(texte_manquante,CHAMP_TRAVAIL(action, texte_manquante)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_texte_generique                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_generique                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(texte_generique,CHAMP_TRAVAIL(action, texte_generique)));
        return RESULTAT_OK;
}

Resultat actionflux_definition_indice                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) indice                        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_copie(indice,CHAMP_TRAVAIL(action, indice)));
        return RESULTAT_OK;
}

/* Ces fonctions servent � d�finir les diff�rents comportement � adopter selon le type de flux
 * rencontr� lors d'un parcours.
 * Ces fonctions renvoient RESULTAT_ERREUR si le pointeur sur la structure est NULL.
 */

Resultat actionflux_lecture_principal                    (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) principal                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(principal)=CHAMP_TRAVAIL(action, principal);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_commentaire                  (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) commentaire              )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(commentaire)=CHAMP_TRAVAIL(action, commentaire);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_compilateur                  (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) compilateur              )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(compilateur)=CHAMP_TRAVAIL(action, compilateur);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_options                      (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) options                  )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(options)=CHAMP_TRAVAIL(action, options);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_macro_definition             (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) macro_definition         )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(macro_definition)=CHAMP_TRAVAIL(action, macro_definition);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_macro_parametre              (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) macro_parametre          )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(macro_parametre)=CHAMP_TRAVAIL(action, macro_parametre);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_message                      (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) message                  )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(message)=CHAMP_TRAVAIL(action, message);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_document                     (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) document                 )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(document)=CHAMP_TRAVAIL(action, document);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_niveau                       (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) niveau                   )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(niveau)=CHAMP_TRAVAIL(action, niveau);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_pertinence                   (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) pertinence               )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(pertinence)=CHAMP_TRAVAIL(action, pertinence);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_section_nom                  (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_nom              )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(section_nom)=CHAMP_TRAVAIL(action, section_nom);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_section_format               (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_format           )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(section_format)=CHAMP_TRAVAIL(action, section_format);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_section_section              (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_section          )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(section_section)=CHAMP_TRAVAIL(action, section_section);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_reference_format             (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference_format         )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(reference_format)=CHAMP_TRAVAIL(action, reference_format);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_reference                    (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference                )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(reference)=CHAMP_TRAVAIL(action, reference);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_reference_automatique        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference_automatique    )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(reference_automatique)=CHAMP_TRAVAIL(action, reference_automatique);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_titre                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) titre                    )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(titre)=CHAMP_TRAVAIL(action, titre);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_titre_automatique            (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) titre_automatique        )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(titre_automatique)=CHAMP_TRAVAIL(action, titre_automatique);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_texte                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte                    )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(texte)=CHAMP_TRAVAIL(action, texte);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_texte_reductible             (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_reductible         )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(texte_reductible)=CHAMP_TRAVAIL(action, texte_reductible);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_texte_irreductible           (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_irreductible       )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(texte_irreductible)=CHAMP_TRAVAIL(action, texte_irreductible);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_texte_manquante              (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_manquante          )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(texte_manquante)=CHAMP_TRAVAIL(action, texte_manquante);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_texte_generique              (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_generique          )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(texte_generique)=CHAMP_TRAVAIL(action, texte_generique);
        return RESULTAT_OK;
}

Resultat actionflux_lecture_indice                       (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) indice                   )
{
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        T_R(indice)=CHAMP_TRAVAIL(action, indice);
        return RESULTAT_OK;
}

/* Ces fonctions servent � lire une valeur dans la structure d'action de flux.
 * Ces fonctions renvoient RESULTAT_ERREUR si le pointeur sur la structure est NULL.
 */

Resultat actionflux_lecture_processusflux(TRAVAIL(ActionFlux) action, TRAVAIL_SCALAIRE(TypeFlux) type, REFERENCE(ProcessusFlux) processusflux)
{
        /* Lit le processus flux associ� au type de flux indiqu�.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le type est incorrect.
         */
        if(S_T(action)==NULL)
                return RESULTAT_OK;
        switch(type)
        {
                case T_S_(FLUX_PRINCIPAL):
                        SECURISE(actionflux_lecture_principal(action,processusflux));
                        break;
                case T_S_(FLUX_COMMENTAIRE):
                        SECURISE(actionflux_lecture_commentaire(action,processusflux));
                        break;
                case T_S_(FLUX_COMPILATEUR):
                        SECURISE(actionflux_lecture_compilateur(action,processusflux));
                        break;
                case T_S_(FLUX_OPTIONS):
                        SECURISE(actionflux_lecture_options(action,processusflux));
                        break;
                case T_S_(FLUX_MACRO_DEFINITION):
                        SECURISE(actionflux_lecture_macro_definition(action,processusflux));
                        break;
                case T_S_(FLUX_MACRO_PARAMETRE):
                        SECURISE(actionflux_lecture_macro_parametre(action,processusflux));
                        break;
                case T_S_(FLUX_MESSAGE):
                        SECURISE(actionflux_lecture_message(action,processusflux));
                        break;
                case T_S_(FLUX_DOCUMENT):
                        SECURISE(actionflux_lecture_document(action,processusflux));
                        break;
                case T_S_(FLUX_NIVEAU):
                        SECURISE(actionflux_lecture_niveau(action,processusflux));
                        break;
                case T_S_(FLUX_PERTINENCE):
                        SECURISE(actionflux_lecture_pertinence(action,processusflux));
                        break;
                case T_S_(FLUX_SECTION_NOM):
                        SECURISE(actionflux_lecture_section_nom(action,processusflux));
                        break;
                case T_S_(FLUX_SECTION_FORMAT):
                        SECURISE(actionflux_lecture_section_format(action,processusflux));
                        break;
                case T_S_(FLUX_SECTION_SECTION):
                        SECURISE(actionflux_lecture_section_section(action,processusflux));
                        break;
                case T_S_(FLUX_REFERENCE_FORMAT):
                        SECURISE(actionflux_lecture_reference_format(action,processusflux));
                        break;
                case T_S_(FLUX_REFERENCE):
                        SECURISE(actionflux_lecture_reference(action,processusflux));
                        break;
                case T_S_(FLUX_REFERENCE_AUTOMATIQUE):
                        SECURISE(actionflux_lecture_reference_automatique(action,processusflux));
                        break;
                case T_S_(FLUX_TITRE):
                        SECURISE(actionflux_lecture_titre(action,processusflux));
                        break;
                case T_S_(FLUX_TITRE_AUTOMATIQUE):
                        SECURISE(actionflux_lecture_titre_automatique(action,processusflux));
                        break;
                case T_S_(FLUX_TEXTE):
                        SECURISE(actionflux_lecture_texte(action,processusflux));
                        break;
                case T_S_(FLUX_TEXTE_REDUCTIBLE):
                        SECURISE(actionflux_lecture_texte_reductible(action,processusflux));
                        break;
                case T_S_(FLUX_TEXTE_IRREDUCTIBLE):
                        SECURISE(actionflux_lecture_texte_irreductible(action,processusflux));
                        break;
                case T_S_(FLUX_TEXTE_MANQUANTE):
                        SECURISE(actionflux_lecture_texte_manquante(action,processusflux));
                        break;
                case T_S_(FLUX_TEXTE_GENERIQUE):
                        SECURISE(actionflux_lecture_texte_generique(action,processusflux));
                        break;
                case T_S_(FLUX_INDICE):
                        SECURISE(actionflux_lecture_indice(action,processusflux));
                        break;
                case T_S_(FLUX_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

Resultat actionflux_execution_preaction(TRAVAIL(ActionFlux) action, TRAVAIL(General) general, TRAVAIL(Flux) flux)
{
        /* Execute les pr�actions des types de flux des contextes
         * en cours sur le flux indiqu�.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         */
        STOCKAGE(PileContexte) contextecourant;
        STOCKAGE_SCALAIRE(Booleen) arretcontextes;
        TRAVAIL(ProcessusFlux) processusflux;
        TRAVAIL_SCALAIRE(Processus) preaction; 
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        arretcontextes=FAUX;
        for(contextecourant=CHAMP(action, contexte) ; (T_S_(arretcontextes)==T_S_(FAUX))&&(contextecourant!=NULL) ; contextecourant=CHAMP_STOCKAGE(contextecourant, prochain))
        {
                SECURISE(actionflux_lecture_processusflux(action,T_S_(CHAMP_STOCKAGE(contextecourant, contexte)),R_T(processusflux)));
                SECURISE(processusflux_lecture_preaction(processusflux,R_T_(preaction)));
                if(preaction!=T_S_(NULL))
                {
                        SECURISE(preaction(flux,general,C_S_(arretcontextes)));
                }
        }
        return RESULTAT_OK;
}

Resultat actionflux_execution_action(TRAVAIL(ActionFlux) action, TRAVAIL(Commande) commande, TRAVAIL(General) general, COREFERENCE(Flux) flux)
{
        /* Ex�cute les actions sur la commande en tenant compte des
         * contextes en cours.
         * Le r�sultat du traitement de la commande est pass� en param�tre (flux).
         * Le type de parcours de la commande est �galement pris en compte ici.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
         */
        STOCKAGE(PileContexte) contextecourant;
        STOCKAGE_SCALAIRE(Booleen) arretcontextes;
        TRAVAIL(ProcessusFlux) processusflux;
        TRAVAIL(ActionCommande) actioncommande;
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        arretcontextes=FAUX;
        S_C(flux)=NULL;
        for(contextecourant=CHAMP(action, contexte) ; (T_S_(arretcontextes)==T_S_(FAUX))&&(S_C(flux)==NULL)&&(contextecourant!=NULL) ; contextecourant=CHAMP_STOCKAGE(contextecourant, prochain))
        {
                SECURISE(actionflux_lecture_processusflux(action,T_S_(CHAMP_STOCKAGE(contextecourant, contexte)),R_T(processusflux)));
                SECURISE(processusflux_lecture_actioncommande(processusflux,R_T(actioncommande)));
                SECURISE(actioncommande_execution_commande(actioncommande,commande,action,general,flux,C_S_(arretcontextes)));
        }
        return RESULTAT_OK;
}

Resultat actionflux_execution_postaction(TRAVAIL(ActionFlux) action, TRAVAIL(General) general, TRAVAIL(Flux) flux)
{
        /* Execute les postactions des types de flux des contextes
         * en cours sur le flux indiqu�.
         * Renvoie RESULTAT_ERREUR si action est NULL.
         */
        STOCKAGE(PileContexte) contextecourant;
        STOCKAGE_SCALAIRE(Booleen) arretcontextes;
        TRAVAIL(ProcessusFlux) processusflux;
        TRAVAIL_SCALAIRE(Processus) postaction;
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        arretcontextes=FAUX;
        for(contextecourant=CHAMP(action, contexte) ; (T_S_(arretcontextes)==T_S_(FAUX))&&(contextecourant!=NULL) ; contextecourant=CHAMP_STOCKAGE(contextecourant, prochain))
        {
                SECURISE(actionflux_lecture_processusflux(action,T_S_(CHAMP_STOCKAGE(contextecourant, contexte)),R_T(processusflux)));
                SECURISE(processusflux_lecture_postaction(processusflux,R_T_(postaction)));
                if(postaction!=T_S_(NULL))
                {
                        SECURISE(postaction(flux,general,C_S_(arretcontextes)));
                }
        }
        return RESULTAT_OK;
}

Resultat actionflux_copie(TRAVAIL(ActionFlux) action, TRAVAIL(ActionFlux) copie)
{
        /* Copie une structure d'action de flux. Si une allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�.
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(action)!=NULL, RESULTAT_ERREUR);

        SECURISE(actionflux_destruction(copie));
        SECURISE(actionflux_initialisation(copie));
        SECURISE(actionflux_pilecontexte_copie(CHAMP_TRAVAIL(action, contexte),CHAMP_TRAVAIL(copie, contexte)));
        SECURISE(actionflux_definition_principal(copie,CHAMP_TRAVAIL(action, principal)));
        SECURISE(actionflux_definition_commentaire(copie,CHAMP_TRAVAIL(action, commentaire)));
        SECURISE(actionflux_definition_compilateur(copie,CHAMP_TRAVAIL(action, compilateur)));
        SECURISE(actionflux_definition_options(copie,CHAMP_TRAVAIL(action, options)));
        SECURISE(actionflux_definition_macro_definition(copie,CHAMP_TRAVAIL(action, macro_definition)));
        SECURISE(actionflux_definition_macro_parametre(copie,CHAMP_TRAVAIL(action, macro_parametre)));
        SECURISE(actionflux_definition_message(copie,CHAMP_TRAVAIL(action, message)));
        SECURISE(actionflux_definition_document(copie,CHAMP_TRAVAIL(action, document)));
        SECURISE(actionflux_definition_niveau(copie,CHAMP_TRAVAIL(action, niveau)));
        SECURISE(actionflux_definition_pertinence(copie,CHAMP_TRAVAIL(action, pertinence)));
        SECURISE(actionflux_definition_section_nom(copie,CHAMP_TRAVAIL(action, section_nom)));
        SECURISE(actionflux_definition_section_format(copie,CHAMP_TRAVAIL(action, section_format)));
        SECURISE(actionflux_definition_section_section(copie,CHAMP_TRAVAIL(action, section_section)));
        SECURISE(actionflux_definition_reference_format(copie,CHAMP_TRAVAIL(action, reference_format)));
        SECURISE(actionflux_definition_reference(copie,CHAMP_TRAVAIL(action, reference)));
        SECURISE(actionflux_definition_reference_automatique(copie,CHAMP_TRAVAIL(action, reference_automatique)));
        SECURISE(actionflux_definition_titre(copie,CHAMP_TRAVAIL(action, titre)));
        SECURISE(actionflux_definition_titre_automatique(copie,CHAMP_TRAVAIL(action, titre_automatique)));
        SECURISE(actionflux_definition_texte(copie,CHAMP_TRAVAIL(action, texte)));
        SECURISE(actionflux_definition_texte_reductible(copie,CHAMP_TRAVAIL(action, texte_reductible)));
        SECURISE(actionflux_definition_texte_irreductible(copie,CHAMP_TRAVAIL(action, texte_irreductible)));
        SECURISE(actionflux_definition_texte_manquante(copie,CHAMP_TRAVAIL(action, texte_manquante)));
        SECURISE(actionflux_definition_texte_generique(copie,CHAMP_TRAVAIL(action, texte_generique)));
        SECURISE(actionflux_definition_indice(copie,CHAMP_TRAVAIL(action, indice)));
        return RESULTAT_OK;
}

Resultat actionflux_destruction(TRAVAIL(ActionFlux) action)
{
        /* Destruction d'une structure d'action sur flux.
         */
        if(S_T(action)==NULL)
                return RESULTAT_OK;

        SECURISE(actionflux_pilecontexte_destruction(CHAMP_TRAVAIL(action, contexte)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, principal)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, commentaire)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, compilateur)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, options)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, macro_definition)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, macro_parametre)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, message)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, document)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, niveau)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, pertinence)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, section_nom)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, section_format)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, section_section)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, reference_format)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, reference)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, reference_automatique)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, titre)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, titre_automatique)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, texte)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, texte_reductible)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, texte_irreductible)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, texte_manquante)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, texte_generique)));
        SECURISE(processusflux_destruction(CHAMP_TRAVAIL(action, indice)));
        free(S_T(action));
        S_T(action)=NULL;
        return RESULTAT_OK;
}

