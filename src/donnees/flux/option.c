/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "option.h"

static Resultat option_ajout_elements(TRAVAIL(Option) option)
{
        /* Fait en sorte qu'en sortie de fonction, au moins une place libre
         * et r�serv�e en m�moire soit disponible.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �ventuelle �choue.
         */
        TABLEAU(STOCKAGE(CommandeOption)) nouveau;
        if(T_S_(CHAMP(option, taille))<T_S_(CHAMP(option, memoire)))
                return RESULTAT_OK;
        nouveau=REALLOCATION_CAST(CHAMP(option, option), CommandeOption, T_S_(CHAMP(option, memoire))*T_S_(MULTTAILLE));
        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(option, memoire) *= T_S_(MULTTAILLE);
        CHAMP(option, option)=nouveau;
        return RESULTAT_OK;
}

static Resultat option_rechercheinterne_option(TRAVAIL(Option) option, TRAVAIL_SCALAIRE(NomOption) nomoption, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Renvoie l'indice de la place occup�e par une
         * option, m�me si elle est absente.
         */
        TRAVAIL_SCALAIRE(NomOption) nom;
        TRAVAIL_SCALAIRE(Indice) inferieur;
        TRAVAIL_SCALAIRE(Indice) median;
        TRAVAIL_SCALAIRE(Indice) superieur;
        inferieur=T_S_(0);
        superieur=T_S_(CHAMP(option, taille))-T_S_(1);
        while(inferieur<=superieur)
        {
                median=(inferieur+superieur)/T_S_(2);
                SECURISE(commandeoption_lecture_option(ELEMENT_TRAVAIL(CHAMP(option, option), median),R_T_(nom)));
                if(STRCMP(nom, nomoption)<T_S_(0))
                        inferieur=median+T_S_(1);
                else
                        superieur=median-T_S_(1);
        }
        T_R_(indice)=inferieur;
        return RESULTAT_OK;
}

Resultat option_initialisation(TRAVAIL(Option) option)
{
        /* Cr�e une structure de d�claration d'option vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION((S_T(option)=NOUVEAU(Option))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        if((CHAMP(option, option)=NOUVEAUX(CommandeOption, T_S_(TAILLEINIT)))==NULL)
        {
                free(S_T(option));
                S_T(option)=NULL;
                return RESULTAT_ERREUR_MEMOIRE;
        }
        CHAMP(option, memoire)=TAILLEINIT;
        CHAMP(option, taille)=0;
        return RESULTAT_OK;
}

Resultat option_ajout_option(TRAVAIL(Option) option, TRAVAIL(CommandeOption) commandeoption)
{
        /* Ajoute une option � la structure de d�claration d'option.
         * Les options sont ajout�es dans l'ordre lexicographique.
         * Si une option du m�me nom existe d�j�, elle est remplac�e.
         */
        TRAVAIL_SCALAIRE(Indice) insertion;
        TRAVAIL_SCALAIRE(NomOption) nom;
        TRAVAIL_SCALAIRE(NomOption) nominsertion;
        STOCKAGE_SCALAIRE(Booleen) testnom;
        ASSERTION(S_T(option)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoption_lecture_option(commandeoption,R_T_(nom)));
        SECURISE(option_rechercheinterne_option(option,nom,R_T_(insertion)));
        if(insertion<T_S_(CHAMP(option, taille)))
        {
                SECURISE(commandeoption_lecture_option(ELEMENT_TRAVAIL(CHAMP(option, option), insertion),R_T_(nominsertion)));
                testnom=(STRCMP(nom, nominsertion)!=T_S_(0))?VRAI:FAUX;
        }
        else
                testnom=VRAI;
        if(T_S_(testnom)==T_S_(VRAI))
        {
                SECURISE(option_ajout_elements(option));
                STOCKAGE_SCALAIRE(Indice) indice;
                for(indice=CHAMP(option, taille) ; T_S_(indice)>insertion ; T_S_(indice)--)
                        ELEMENT(CHAMP(option, option), T_S_(indice))=ELEMENT(CHAMP(option, option), T_S_(indice)-1);
                T_S_(CHAMP(option, taille))++;
                ELEMENT(CHAMP(option, option), insertion)=NULL;
        }
        SECURISE(commandeoption_copie(commandeoption,ELEMENT_TRAVAIL(CHAMP(option, option), insertion)));
        return RESULTAT_OK;
}

Resultat option_lecture_taille(TRAVAIL(Option) option, REFERENCE_SCALAIRE(Taille) taille)
{
        /* Renvoie la taille du tableau d'options.
         * Renvoie RESULTAT_ERREUR si option est NULL.
         */
        ASSERTION(S_T(option)!=NULL, RESULTAT_ERREUR);

        T_R_(taille)=T_S_(CHAMP(option, taille));
        return RESULTAT_OK;
}

Resultat option_lecture_option(TRAVAIL(Option) option, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(CommandeOption) commandeoption)
{
        /* Retourne la commande d'option enregistr�e � l'indice indiqu�.
         * Renvoie RESULTAT_ERREUR si option est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(option)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(option, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        T_R(commandeoption)=ELEMENT_TRAVAIL(CHAMP(option, option), indice);
        return RESULTAT_OK;
}

Resultat option_recherche_option(TRAVAIL(Option) option, TRAVAIL_SCALAIRE(NomOption) nomoption, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Renvoie l'indice de la option pass�e en param�tre. Si elle n'est pas pr�sente,
         * la fonction renvoie MACRO_NON_TROUVEE � la place de l'indice.
         */
        TRAVAIL_SCALAIRE(NomOption) nom;
        SECURISE(option_rechercheinterne_option(option,nomoption,indice));
        if(T_R_(indice)>=T_S_(CHAMP(option, taille)))
                T_R_(indice)=T_S_(OPTION_NON_TROUVEE);
        else
        {
                SECURISE(commandeoption_lecture_option(ELEMENT_TRAVAIL(CHAMP(option, option), T_R_(indice)),R_T_(nom)));
                if(STRCMP(nom, nomoption)!=T_S_(0))
                T_R_(indice)=T_S_(OPTION_NON_TROUVEE);
        }
        return RESULTAT_OK;
}

Resultat option_fusion(TRAVAIL(Option) destination, TRAVAIL(Option) ajout)
{
        /* Fusionne deux listes de d�claration d'option, en ajoutant
         * les options d�clar�es dans ajout � celles de la destination.
         * Les options restent dans l'ordre lexicographique.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(ajout, taille)) ; T_S_(indice)++)
                SECURISE(option_ajout_option(destination,ELEMENT_TRAVAIL(CHAMP(ajout, option), T_S_(indice))));
        return RESULTAT_OK;
}

Resultat option_copie(TRAVAIL(Option) option, TRAVAIL(Option) copie)
{
        /* Cr�e une copie de la option donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        SECURISE(option_destruction(copie));
        SECURISE(option_initialisation(copie));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(option, taille)) ; T_S_(indice)++)
                SECURISE(option_ajout_option(copie,ELEMENT_TRAVAIL(CHAMP(option, option), T_S_(indice))));
        return RESULTAT_OK;
}

Resultat option_destruction(TRAVAIL(Option) option)
{
        /* D�truit une liste de d�claration d'options.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(option)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(option, taille)) ; T_S_(indice)++)
        {
                SECURISE(commandeoption_destruction(ELEMENT_TRAVAIL(CHAMP(option, option), T_S_(indice))));
        }
        free(CHAMP(option, option));
        free(S_T(option));
        S_T(option)=NULL;
        return RESULTAT_OK;
}

