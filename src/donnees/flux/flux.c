/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "flux.h"

#define TAILLENOMBRE        128+1

static Resultat flux_initialisation_maillonflux(TRAVAIL(MaillonFlux) maillon)
{
        /* Cr�e et initialise un maillon du flux.
         */
        ASSERTION((S_T(maillon)=NOUVEAU(MaillonFlux))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(maillon, precedent)=NULL;
        CHAMP(maillon, suivant)=NULL;
        CHAMP(maillon, commande)=NULL;
        return RESULTAT_OK;
}

static Resultat flux_destruction_maillonflux(TRAVAIL(MaillonFlux) maillon)
{
        /* D�truit un maillon de flux.
         */
        if(S_T(maillon)==NULL)
                return RESULTAT_OK;
        SECURISE(commande_destruction(CHAMP_TRAVAIL(maillon, commande)));
        free(S_T(maillon));
        S_T(maillon)=NULL;
        return RESULTAT_OK;
}

static Resultat flux_destructiontotale_maillonflux(TRAVAIL(MaillonFlux) supprime, TRAVAIL(MaillonFlux) premier, TRAVAIL(MaillonFlux) dernier)
{
        /* Supprime totalement un maillon d'un flux, en reformant
         * correctement les chainages avec les voisins.
         */
        ASSERTION(S_T(supprime)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(supprime, precedent)==NULL)
                S_T(premier)=CHAMP(supprime, suivant);
        else
                CHAMP_STOCKAGE(CHAMP(supprime, precedent), suivant)=CHAMP(supprime, suivant);
        if(CHAMP(supprime, suivant)==NULL)
                S_T(dernier)=CHAMP(supprime, precedent);
        else
                CHAMP_STOCKAGE(CHAMP(supprime, suivant), precedent)=CHAMP(supprime, precedent);
        SECURISE(commande_destruction(CHAMP_TRAVAIL(supprime, commande)));
        SECURISE(flux_destruction_maillonflux(supprime));
        return RESULTAT_OK;
}

static Resultat flux_copie_maillonflux(TRAVAIL(MaillonFlux) original, TRAVAIL(MaillonFlux) copie)
{
        /* Cr�e une copie d'un maillon de flux.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        SECURISE(flux_destruction_maillonflux(copie));
        SECURISE(flux_initialisation_maillonflux(copie));
        SECURISE(commande_copie(CHAMP_TRAVAIL(original, commande),CHAMP_TRAVAIL(copie, commande)));
        return RESULTAT_OK;
}

static Resultat flux_ajout_liste(TRAVAIL(MaillonFlux) dernier, TRAVAIL(Commande) commande)
{
        /* Ajoute une commande � la fin d'une liste de commandes.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue,
         * ou RESULTAT_ERREUR_DEPASSEMENT si le maillon point� par
         * dernier n'est pas le dernier maillon de la liste.
         */
        STOCKAGE(MaillonFlux) nouveau;
        ASSERTION((S_T(dernier)==NULL)||(CHAMP(dernier, suivant)==NULL), RESULTAT_ERREUR_DEPASSEMENT);

        SECURISE(flux_initialisation_maillonflux(T_S(nouveau)));
        SECURISE(commande_copie(commande,T_S(CHAMP_STOCKAGE(nouveau, commande))));
        if(S_T(dernier)!=NULL)
                CHAMP(dernier, suivant)=nouveau;
        CHAMP_STOCKAGE(nouveau, precedent)=S_T(dernier);
        S_T(dernier)=nouveau;
        return RESULTAT_OK;
}

static Resultat flux_destruction_listeflux(TRAVAIL(MaillonFlux) premier)
{
        /* D�truit un maillon de flux et tous les maillons
         * qui le suivent.
         */
        if(S_T(premier)==NULL)
                return RESULTAT_OK;
        SECURISE(flux_destruction_listeflux(CHAMP_TRAVAIL(premier, suivant)));
        SECURISE(commande_destruction(CHAMP_TRAVAIL(premier, commande)));
        SECURISE(flux_destruction_maillonflux(premier));
        return RESULTAT_OK;
}

static Resultat flux_copieinterne_listeflux(TRAVAIL(MaillonFlux) original_premier, TRAVAIL(MaillonFlux) copie_premier, TRAVAIL(MaillonFlux) copie_dernier)
{
        /* Proc�de � une copie effective d'une liste de commandes.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si jamais une allocation �choue.
         */
        STOCKAGE(MaillonFlux) nouveau;
        if(S_T(original_premier)==NULL)
        {
                S_T(copie_premier)=NULL;
                S_T(copie_dernier)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(flux_copieinterne_listeflux(CHAMP_TRAVAIL(original_premier, suivant),copie_premier,copie_dernier));
        nouveau=NULL;
        SECURISE(flux_copie_maillonflux(original_premier,T_S(nouveau)));
        CHAMP_STOCKAGE(nouveau, precedent)=NULL;
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(copie_premier);
        if(S_T(copie_premier)!=NULL)
                CHAMP(copie_premier, precedent)=nouveau;
        S_T(copie_premier)=nouveau;
        if(CHAMP(original_premier, suivant)==NULL)
                S_T(copie_dernier)=nouveau;
        return RESULTAT_OK;
}

static Resultat flux_copie_listeflux(TRAVAIL(MaillonFlux) original_premier, TRAVAIL(MaillonFlux) copie_premier, TRAVAIL(MaillonFlux) copie_dernier)
{
        /* Cr�e une copie d'une liste de maillon de flux.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        SECURISE(flux_destruction_listeflux(copie_premier));
        SECURISE(flux_copieinterne_listeflux(original_premier,copie_premier,copie_dernier));
        return RESULTAT_OK;
}

static Resultat flux_inclusion_flux(TRAVAIL(Flux) flux, TRAVAIL(MaillonFlux) maillon, TRAVAIL(Flux) flux_inclusion, TRAVAIL(General) general)
{
        /* Inclus un flux dans un autre, et ce avant le maillon indiqu�.
         * Si le maillon est NULL, le flux est ajout� en fin de chaine.
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         * Rien ne se passe si flux_inclusion est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Attention ! Si maillon est non NULL, il doit absolument
         * d�signer un v�ritable maillon du flux recevant le flux inclus,
         * sous peine de se retrouver avec un comportement ind�fini.
         */
        STOCKAGE(MaillonFlux) premiercopie;
        STOCKAGE(MaillonFlux) derniercopie;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        if(S_T(flux_inclusion)==NULL)
                return RESULTAT_OK;
        SECURISE(option_fusion(CHAMP_TRAVAIL(flux, option),CHAMP_TRAVAIL(flux_inclusion, option)));
        SECURISE(macro_fusion(CHAMP_TRAVAIL(flux, macro),CHAMP_TRAVAIL(flux_inclusion, macro),general));
        premiercopie=NULL;
        derniercopie=NULL;
        SECURISE(flux_copie_listeflux(CHAMP_TRAVAIL(flux_inclusion, premier),T_S(premiercopie),T_S(derniercopie)));
        //cas o� le fichier inclus n'a pas de maillons
        if((premiercopie==NULL)&&(derniercopie==NULL))
                return RESULTAT_OK;
        if(S_T(maillon)==NULL)
        {//cas de l'ajout en fin de listeflux
                if(CHAMP(flux, dernier)==NULL)
                {//le flux est vide
                        CHAMP(flux, premier)=premiercopie;
                        CHAMP(flux, dernier)=derniercopie;
                }
                else
                {//le flux est non vide
                        CHAMP_STOCKAGE(CHAMP(flux, dernier), suivant)=premiercopie;
                        CHAMP_STOCKAGE(premiercopie, precedent)=CHAMP(flux, dernier);
                        CHAMP(flux, dernier)=derniercopie;
                }
        }
        else
        {//cas de l'ajout devant un maillon valide
                if(CHAMP(flux, premier)==S_T(maillon))
                {//cas de l'ajout avant le premier
                        CHAMP_STOCKAGE(CHAMP(flux, premier), precedent)=derniercopie;
                        CHAMP_STOCKAGE(derniercopie, suivant)=CHAMP(flux, premier);
                        CHAMP(flux, premier)=premiercopie;
                }
                else
                {//cas de l'ajout en plein milieu de la listeflux
                        CHAMP_STOCKAGE(CHAMP(maillon, precedent), suivant)=premiercopie;
                        CHAMP_STOCKAGE(premiercopie, precedent)=CHAMP(maillon, precedent);
                        CHAMP(maillon, precedent)=derniercopie;
                        CHAMP_STOCKAGE(derniercopie, suivant)=S_T(maillon);
                }
        }
        return RESULTAT_OK;
}

Resultat flux_initialisation(TRAVAIL(Flux) flux)
{
        /* Cr�e un flux vide. Si l'allocation �choue, renvoie RESULTAT_ERREUR_MEMOIRE.
         */
        ASSERTION((S_T(flux)=NOUVEAU(Flux))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(flux, type)=FLUX_VIDE;
        SECURISE(option_initialisation(CHAMP_TRAVAIL(flux, option)));
        SECURISE(macro_initialisation(CHAMP_TRAVAIL(flux, macro)));
        CHAMP(flux, premier)=NULL;
        CHAMP(flux, dernier)=NULL;
        return RESULTAT_OK;
}

Resultat flux_definition_type(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(TypeFlux) type)
{
        /* Donne un type � un flux de type vide.
         * Si le flux n'est pas de type vide,
         * RESULTAT_ERREUR_DOMAINE est renvoy�.
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(flux, type))==T_S_(FLUX_VIDE), RESULTAT_ERREUR_DOMAINE);

        CHAMP(flux, type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat flux_ajout_commande(TRAVAIL(Flux) flux, TRAVAIL(Commande) commande, TRAVAIL(General) general)
{
        /* Ajoute une commande � un flux. Si l'allocation �choue, renvoie RESULTAT_ERREUR_MEMOIRE.
         * Renvoie aussi RESULTAT_ERREUR si le type de la commande est incorrect.
         */
        TRAVAIL_SCALAIRE(TypeCommande) type;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(commande_lecture_type(commande,R_T_(type)));
        switch(type)
        {
                TRAVAIL(CommandeOption) option;
                TRAVAIL(CommandeDefine) macro;
                case T_S_(COMMANDE_VIDE):
                        return RESULTAT_ERREUR;
                        break;
                case T_S_(COMMANDE_OPTION):
                        SECURISE(commande_lecture_option(commande,R_T(option)));
                        SECURISE(option_ajout_option(CHAMP_TRAVAIL(flux, option),option));
                        break;
                case T_S_(COMMANDE_DEFINE):
                        SECURISE(commande_lecture_define(commande,R_T(macro)));
                        SECURISE(macro_ajout_macro(CHAMP_TRAVAIL(flux, macro),macro,general,T_S_(MACROAJOUT_REMPLACE),T_S_(MACROPROBLEME_VERBEUX)));
                        //Au besoin, on peut faire passer les deux derniers arguments en param�tres de flux_ajout_commande...
                        break;
                default:
                        SECURISE(flux_ajout_liste(CHAMP_TRAVAIL(flux, dernier),commande));
                        if(CHAMP(flux, premier)==NULL)
                                CHAMP(flux, premier)=CHAMP(flux, dernier);
                        break;
        }
        return RESULTAT_OK;
}

Resultat flux_lecture_type(TRAVAIL(Flux) flux, REFERENCE_SCALAIRE(TypeFlux) type)
{
        /* Renvoie le type d'un flux.
         * Renvoie RESULTAT_ERREUR si le flux est NULL.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        T_R_(type)=T_S_(CHAMP(flux, type));
        return RESULTAT_OK;
}

Resultat flux_lecture_option(TRAVAIL(Flux) flux, REFERENCE(Option) option)
{
        /* Renvoie la structure d'option d'un flux.
         * Renvoie RESULTAT_ERREUR si le flux est NULL.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        T_R(option)=CHAMP_TRAVAIL(flux, option);
        return RESULTAT_OK;
}

Resultat flux_lecture_macro(TRAVAIL(Flux) flux, REFERENCE(Macro) macro)
{
        /* Renvoie la structure de macro d'un flux.
         * Renvoie RESULTAT_ERREUR si le flux est NULL.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        T_R(macro)=CHAMP_TRAVAIL(flux, macro);
        return RESULTAT_OK;
}

Resultat flux_concatenation(TRAVAIL(Flux) flux, TRAVAIL(Flux) ajout, TRAVAIL(General) general)
{
        /* Ajoute le flux ajout au flux pass� en premier param�tre.
         * La concat�nation se fait directement sur le flux
         * destination.
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Il ne se passe rien si le flux d'ajout est NULL.
         */
        STOCKAGE(MaillonFlux) maillon;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        maillon=NULL;
        SECURISE(flux_inclusion_flux(flux,T_S(maillon),ajout,general));
        return RESULTAT_OK;
}

Resultat flux_defragmentation(TRAVAIL(Flux) flux)
{
        /* D�fragmente le texte d'un flux, et
         * remplace les caract�res d'�chappement
         * par leur valeur.
         * Cette d�fragmentation n'est pas poursuivie
         * sur les flux inclus dans les commandes !
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR_DOMAINE si une commande est vide.
         */
        STOCKAGE(MaillonFlux) mailloncourant;
        STOCKAGE_SCALAIRE(Booleen) danstexte;
        TRAVAIL_SCALAIRE(TypeCommande) typecommande;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        danstexte=FAUX;
        for(mailloncourant=CHAMP(flux, premier) ; mailloncourant!=NULL ; )
        {
                SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(mailloncourant, commande)),R_T_(typecommande)));
                switch(typecommande)
                {
                        case T_S_(COMMANDE_ECHAPPEMENT):
                                {
                                        STOCKAGE(MaillonFlux) maillonechappement;
                                        STOCKAGE_SCALAIRE(Booleen) danstexteechappement;
                                        danstexteechappement=danstexte;
                                        for(maillonechappement=mailloncourant ; maillonechappement!=NULL ; maillonechappement=maillonechappement->suivant)
                                        {
                                                TRAVAIL(CommandeEchappement) commandeechappement;
                                                STOCKAGE(Commande) commande;
                                                TRAVAIL_SCALAIRE(TypeCommande) typecommandeechappement;
                                                STOCKAGE(CommandeTexte) commandetexte;
                                                STOCKAGE(MaillonFlux) nouveaumaillonflux;
                                                TRAVAIL(LocalisationFichier) localisationfichier;
                                                TRAVAIL_SCALAIRE(Chaine) texteequivalent;
                                                SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(maillonechappement, commande)),R_T_(typecommandeechappement)));
                                                if(typecommandeechappement!=COMMANDE_ECHAPPEMENT)
                                                        break;
                                                SECURISE(commande_lecture_echappement(T_S(CHAMP_STOCKAGE(maillonechappement, commande)),R_T(commandeechappement)));
                                                SECURISE(commandeechappement_lecture_localisationfichier(commandeechappement,R_T(localisationfichier)));
                                                SECURISE(commandeechappement_remplacement_texte(commandeechappement,T_S_(danstexteechappement),R_T_(texteequivalent)));
                                                SECURISE(commandetexte_initialisation(T_S(commandetexte)));
                                                SECURISE(commandetexte_definition_localisationfichier(T_S(commandetexte),localisationfichier));
                                                SECURISE(commandetexte_definition_texte(T_S(commandetexte),texteequivalent));
                                                free(S_T_(texteequivalent));
                                                SECURISE(commande_initialisation(T_S(commande)));
                                                SECURISE(commande_definition_texte(T_S(commande),T_S(commandetexte)));
                                                SECURISE(commandetexte_destruction(T_S(commandetexte)));
                                                SECURISE(flux_initialisation_maillonflux(T_S(nouveaumaillonflux)));
                                                CHAMP_STOCKAGE(nouveaumaillonflux, commande)=commande;
                                                if(CHAMP_STOCKAGE(maillonechappement, precedent)==NULL)
                                                {
                                                        CHAMP_STOCKAGE(nouveaumaillonflux, precedent)=NULL;
                                                        CHAMP(flux, premier)=nouveaumaillonflux;
                                                }
                                                else
                                                {
                                                        CHAMP_STOCKAGE(nouveaumaillonflux, precedent)=CHAMP_STOCKAGE(maillonechappement, precedent);
                                                        CHAMP_STOCKAGE(CHAMP_STOCKAGE(maillonechappement, precedent), suivant)=nouveaumaillonflux;
                                                }
                                                if(CHAMP_STOCKAGE(maillonechappement, suivant)==NULL)
                                                {
                                                        CHAMP_STOCKAGE(nouveaumaillonflux, suivant)=NULL;
                                                        CHAMP(flux, dernier)=nouveaumaillonflux;
                                                }
                                                else
                                                {
                                                        CHAMP_STOCKAGE(nouveaumaillonflux, suivant)=CHAMP_STOCKAGE(maillonechappement, suivant);
                                                        CHAMP_STOCKAGE(CHAMP_STOCKAGE(maillonechappement, suivant), precedent)=nouveaumaillonflux;
                                                }
                                                if(maillonechappement==mailloncourant)
                                                {
                                                        mailloncourant=nouveaumaillonflux;
                                                }
                                                SECURISE(flux_destruction_maillonflux(T_S(maillonechappement)));
                                                maillonechappement=nouveaumaillonflux;
                                                danstexteechappement=FAUX;
                                                //Plac� ici car les caract�res d'echappement suivants ne sont jamais dans du texte.
                                        }
                                        if((CHAMP_STOCKAGE(mailloncourant, precedent)!=NULL)&&(T_S_(danstexte)==T_S_(VRAI)))
                                                mailloncourant=CHAMP_STOCKAGE(mailloncourant, precedent);
                                }
                                break;
                        case T_S_(COMMANDE_TEXTE):
                                {
                                        TRAVAIL_SCALAIRE(TypeCommande) typecommandesuivante;
                                        TRAVAIL(CommandeTexte) commandetextecourant;
                                        TRAVAIL(CommandeTexte) commandetextesuivant;
                                        TRAVAIL_SCALAIRE(Chaine) textecourant;
                                        TRAVAIL_SCALAIRE(Chaine) textesuivant;
                                        STOCKAGE_SCALAIRE(Chaine) textenouveau;
                                        STOCKAGE(MaillonFlux) maillondetruit;
                                        danstexte=VRAI;
                                        if(CHAMP_STOCKAGE(mailloncourant, suivant)==NULL)
                                        {
                                                mailloncourant=CHAMP_STOCKAGE(mailloncourant, suivant);
                                                break;
                                        }
                                        SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(CHAMP_STOCKAGE(mailloncourant, suivant), commande)), R_T_(typecommandesuivante)));
                                        switch(typecommandesuivante)
                                        {
                                                case T_S_(COMMANDE_TEXTE):
                                                        SECURISE(commande_lecture_texte(T_S(CHAMP_STOCKAGE(mailloncourant, commande)), R_T(commandetextecourant)));
                                                        SECURISE(commandetexte_lecture_texte(commandetextecourant,R_T(textecourant)));
                                                        SECURISE(commande_lecture_texte(T_S(CHAMP_STOCKAGE(CHAMP_STOCKAGE(mailloncourant, suivant), commande)), R_T(commandetextesuivant)));
                                                        SECURISE(commandetexte_lecture_texte(commandetextesuivant,R_T(textesuivant)));
                                                        if((textenouveau=CAST_S_(Chaine, ALLOCATION_N_(Caractere, STRLEN(textecourant)+STRLEN(textesuivant)+T_S_(1))))==NULL)
                                                        {
                                                                mailloncourant=CHAMP_STOCKAGE(mailloncourant, suivant);
                                                                break;
                                                        }
                                                        STRCPY(T_S_(textenouveau), textecourant);
                                                        STRCAT(T_S_(textenouveau), textesuivant);
                                                        SECURISE(commandetexte_definition_texte(commandetextecourant,T_S_(textenouveau)));
                                                        free(textenouveau);
                                                        maillondetruit=CHAMP_STOCKAGE(mailloncourant, suivant);
                                                        SECURISE(flux_destructiontotale_maillonflux(T_S(maillondetruit),CHAMP_TRAVAIL(flux, premier),CHAMP_TRAVAIL(flux, dernier)));
                                                        break;
                                                default:
                                                        mailloncourant=CHAMP_STOCKAGE(mailloncourant, suivant);
                                                        break;
                                        }
                                }
                                break;
                        case T_S_(COMMANDE_VIDE):
                                return RESULTAT_ERREUR;
                                break;
                        default:
                                danstexte=FAUX;
                                mailloncourant=CHAMP_STOCKAGE(mailloncourant, suivant);
                                break;
                }
        }
        return RESULTAT_OK;
}

Resultat flux_texte(TRAVAIL(Flux) flux, REFERENCE_SCALAIRE(Chaine) texte)
{
        /* Renvoie le texte le plus court pr�sent en
         * d�but du flux.
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(MaillonFlux) courant;
        STOCKAGE_SCALAIRE(Chaine) texteequivalent;
        TRAVAIL_SCALAIRE(Entier) taillecourante;
        REFERENCE_SCALAIRE(Caractere) parcours;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        texteequivalent=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(1)));
        ASSERTION(texteequivalent!=NULL, RESULTAT_ERREUR_MEMOIRE);

        parcours=CAST_R_(Caractere, texteequivalent);
        T_R_(parcours)=T_S_('\0');
        taillecourante=T_S_(0);
        for(courant=CHAMP(flux, premier) ; courant!=NULL ; courant=CHAMP_STOCKAGE(courant, suivant))
        {
                STOCKAGE_SCALAIRE(Chaine) texteequivalenttemporaire;
                STOCKAGE_SCALAIRE(Chaine) nouveau;
                TRAVAIL_SCALAIRE(TypeCommande) type;
                SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(courant, commande)),R_T_(type)));
                switch(type)
                {
                        case T_S_(COMMANDE_ECHAPPEMENT):
                        {
                                TRAVAIL(CommandeEchappement) commandeechappement;
                                TRAVAIL_SCALAIRE(Caractere) echappement;
                                SECURISE(commande_lecture_echappement(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandeechappement)));
                                SECURISE(commandeechappement_lecture_caractere(commandeechappement,R_T_(echappement)));
                                nouveau=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(2)));
                                if(nouveau==NULL)
                                {
                                        free(texteequivalent);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                parcours=CAST_R_(Caractere, nouveau);
                                T_R_(parcours++)=echappement;
                                T_R_(parcours)=T_S_('\0');
                                break;
                        }
                        case T_S_(COMMANDE_TEXTE):
                        {
                                TRAVAIL(CommandeTexte) commandetexte;
                                TRAVAIL_SCALAIRE(Chaine) nouveautexte;
                                SECURISE(commande_lecture_texte(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandetexte)));
                                SECURISE(commandetexte_lecture_texte(commandetexte,R_T_(nouveautexte)));
                                nouveau=DUP_CAST(Chaine, nouveautexte);
                                if(nouveau==NULL)
                                {
                                        free(texteequivalent);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                break;
                        }
                        default:
                                return RESULTAT_ERREUR_DOMAINE;
                }
                taillecourante+=STRLEN(T_S_(nouveau));
                texteequivalenttemporaire=CAST_S_(Chaine, REALLOCATION_(texteequivalent, Caractere, taillecourante+T_S_(1)));
                if(texteequivalenttemporaire==NULL)
                {
                        free(texteequivalent);
                        return RESULTAT_ERREUR_MEMOIRE;
                }
                texteequivalent=texteequivalenttemporaire;
                STRCAT(T_S_(texteequivalent), T_S_(nouveau));
                free(nouveau);
        }
        T_R_(texte)=T_S_(texteequivalent);
        return RESULTAT_OK;
}

Resultat flux_nombre(TRAVAIL(Flux) flux, COREFERENCE_SCALAIRE(Booleen) nombre, REFERENCE_SCALAIRE(Entier) valeur)
{
        /* Indique si le d�but d'un flux correspond � un
         * nombre, et renvoie sa valeur le cas �ch�ant.
         * La valeur est renvoy�e uniquement si le pointeur
         * correspondant est non NULL.
         * Renvoie RESULTAT_ERREUR si flux est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        TRAVAIL_SCALAIRE(Chaine) texte;
        STOCKAGE_SCALAIRE(Chaine) finnombre;
        STOCKAGE_SCALAIRE(Entier) entier;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_texte(flux,R_T_(texte)));
        S_T_(errno)=0;
        entier=(STOCKAGE_SCALAIRE(Entier))(strtol((char*)(texte),C_S_(finnombre),T_S_(0)));
        if((errno==T_S_(ERANGE))||(errno==T_S_(EINVAL))||(T_S_(S_C_((COREFERENCE_SCALAIRE(Caractere))(finnombre)))!=T_S_('\0')))
        {
                free(texte);
                S_C_(nombre)=FAUX;
                return RESULTAT_OK;
        }
        if(finnombre==(STOCKAGE_SCALAIRE(Chaine))(texte))
        {
                free(texte);
                S_C_(nombre)=FAUX;
                return RESULTAT_OK;
        }
        free(texte);
        S_C_(nombre)=VRAI;
        if(valeur!=NULL)
                T_R_(valeur)=T_S_(entier);
        return RESULTAT_OK;
}

Resultat flux_reference_manquante(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(Chaine) reference)
{
        /* Construit la chaine de r�f�rence relative � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si le flux est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le flux n'est pas de type
         * FLUX_REFERENCE_AUTOMATIQUE.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(MaillonFlux) courant;
        STOCKAGE_SCALAIRE(Chaine) referenceequivalente;
        TRAVAIL_SCALAIRE(Entier) taillecourante;
        REFERENCE_SCALAIRE(Caractere) parcours;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(flux, type))==T_S_(FLUX_REFERENCE_AUTOMATIQUE), RESULTAT_ERREUR_DOMAINE);

        referenceequivalente=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(1)));
        ASSERTION(referenceequivalente!=NULL, RESULTAT_ERREUR_MEMOIRE);

        parcours=CAST_R_(Caractere, referenceequivalente);
        T_R_(parcours)=T_S_('\0');
        taillecourante=T_S_(0);
        for(courant=CHAMP(flux, premier) ; courant!=NULL ; courant=CHAMP_STOCKAGE(courant, suivant))
        {
                STOCKAGE_SCALAIRE(Chaine) referenceequivalentetemporaire;
                STOCKAGE_SCALAIRE(Chaine) nouveau;
                TRAVAIL_SCALAIRE(TypeCommande) type;
                SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(courant, commande)),R_T_(type)));
                switch(type)
                {
                        case T_S_(COMMANDE_ECHAPPEMENT):
                        {
                                TRAVAIL(CommandeEchappement) commandeechappement;
                                TRAVAIL_SCALAIRE(Caractere) echappement;
                                SECURISE(commande_lecture_echappement(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandeechappement)));
                                SECURISE(commandeechappement_lecture_caractere(commandeechappement,R_T_(echappement)));
                                nouveau=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(2)));
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                parcours=CAST_R_(Caractere, nouveau);
                                T_R_(parcours++)=echappement;
                                T_R_(parcours)=T_S_('\0');
                                break;
                        }
                        case T_S_(COMMANDE_TEXTE):
                        {
                                TRAVAIL(CommandeTexte) commandetexte;
                                TRAVAIL_SCALAIRE(Chaine) texte;
                                SECURISE(commande_lecture_texte(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandetexte)));
                                SECURISE(commandetexte_lecture_texte(commandetexte,R_T_(texte)));
                                nouveau=DUP_CAST(Chaine, texte);
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                break;
                        }
                        case T_S_(COMMANDE_INDEX):
                                nouveau=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(TAILLENOMBRE)));
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                SNPRINTF(T_S_(nouveau), T_S_(TAILLENOMBRE), T_S_("%d"), indice);
                                break;
                        default:
                                return RESULTAT_ERREUR_DOMAINE;
                }
                taillecourante+=STRLEN(T_S_(nouveau));
                referenceequivalentetemporaire=CAST_S_(Chaine, REALLOCATION_(referenceequivalente, Caractere, taillecourante+T_S_(1)));
                if(referenceequivalentetemporaire==NULL)
                {
                        free(referenceequivalente);
                        return RESULTAT_ERREUR_MEMOIRE;
                }
                referenceequivalente=referenceequivalentetemporaire;
                STRCAT(T_S_(referenceequivalente), T_S_(nouveau));
                free(nouveau);
        }
        T_R_(reference)=T_S_(referenceequivalente);
        return RESULTAT_OK;
}

Resultat flux_reference_generique(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(Chaine) referenceparent, REFERENCE_SCALAIRE(Chaine) reference)
{
        /* Construit la chaine de r�f�rence relative � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si le flux est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le flux n'est pas de type
         * FLUX_REFERENCE_AUTOMATIQUE.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(MaillonFlux) courant;
        STOCKAGE_SCALAIRE(Chaine) referenceequivalente;
        REFERENCE_SCALAIRE(Caractere) parcours;
        TRAVAIL_SCALAIRE(Entier) taillecourante;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(flux, type))==T_S_(FLUX_REFERENCE_AUTOMATIQUE), RESULTAT_ERREUR_DOMAINE);

        referenceequivalente=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(1)));
        ASSERTION(referenceequivalente!=NULL, RESULTAT_ERREUR_MEMOIRE);

        parcours=CAST_R_(Caractere, referenceequivalente);
        T_R_(parcours)=T_S_('\0');
        taillecourante=T_S_(0);
        for(courant=CHAMP(flux, premier) ; courant!=NULL ; courant=CHAMP_STOCKAGE(courant, suivant))
        {
                STOCKAGE_SCALAIRE(Chaine) referenceequivalentetemporaire;
                TRAVAIL_SCALAIRE(TypeCommande) type;
                STOCKAGE_SCALAIRE(Chaine) nouveau;
                SECURISE(commande_lecture_type(T_S(CHAMP_STOCKAGE(courant, commande)),R_T_(type)));
                switch(type)
                {
                        case T_S_(COMMANDE_ECHAPPEMENT):
                        {
                                TRAVAIL(CommandeEchappement) commandeechappement;
                                TRAVAIL_SCALAIRE(Caractere) echappement;
                                SECURISE(commande_lecture_echappement(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandeechappement)));
                                SECURISE(commandeechappement_lecture_caractere(commandeechappement,R_T_(echappement)));
                                nouveau=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(2)));
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                parcours=CAST_R_(Caractere, nouveau);
                                T_R_(parcours++)=echappement;
                                T_R_(parcours)=T_S_('\0');
                                break;
                        }
                        case T_S_(COMMANDE_TEXTE):
                        {
                                TRAVAIL(CommandeTexte) commandetexte;
                                TRAVAIL_SCALAIRE(Chaine) texte;
                                SECURISE(commande_lecture_texte(T_S(CHAMP_STOCKAGE(courant, commande)),R_T(commandetexte)));
                                SECURISE(commandetexte_lecture_texte(commandetexte,R_T_(texte)));
                                nouveau=DUP_CAST(Chaine, texte);
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                break;
                        }
                        case T_S_(COMMANDE_INDEX):
                                nouveau=DUP_CAST(Chaine, referenceparent);
                                if(nouveau==NULL)
                                {
                                        free(referenceequivalente);
                                        return RESULTAT_ERREUR_MEMOIRE;
                                }
                                break;
                        default:
                                return RESULTAT_ERREUR_DOMAINE;
                }
                taillecourante+=STRLEN(T_S_(nouveau));
                referenceequivalentetemporaire=CAST_S_(Chaine, REALLOCATION_(referenceequivalente, Caractere, taillecourante+T_S_(1)));
                if(referenceequivalentetemporaire==NULL)
                {
                        free(referenceequivalente);
                        return RESULTAT_ERREUR_MEMOIRE;
                }
                referenceequivalente=referenceequivalentetemporaire;
                STRCAT(T_S_(referenceequivalente), T_S_(nouveau));
                free(nouveau);
        }
        T_R_(reference)=T_S_(referenceequivalente);
        return RESULTAT_OK;
}

Resultat flux_parcours(TRAVAIL(Flux) flux, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours de flux. Ce parcours se fait it�rativement sur un flux donn�
         * et r�cursivement sur tous les flux contenus en interne.
         * Les actions � r�aliser sont indiqu�es par la structure indiqu�e.
         */
        TRAVAIL(ProcessusFlux) processusflux;
        TRAVAIL_SCALAIRE(TypeContexte) contexte;
        STOCKAGE(Flux) flux_inclusion;
        STOCKAGE(MaillonFlux) maillon;
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Taille) taille;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        if(S_T(action)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_lecture_processusflux(action,T_S_(CHAMP(flux, type)),R_T(processusflux)));
        SECURISE(processusflux_lecture_contexte(processusflux,R_T_(contexte)));
        SECURISE(actionflux_ajout_contexte(action,T_S_(CHAMP(flux, type)),contexte));
        SECURISE(actionflux_execution_preaction(action,general,flux));

        //Parcours des options
        SECURISE(option_lecture_taille(CHAMP_TRAVAIL(flux, option),R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                TRAVAIL(CommandeOption) commandeoption;
                STOCKAGE(CommandeOption) commandeoptionretour;
                STOCKAGE(Commande) commande;
                SECURISE(option_lecture_option(CHAMP_TRAVAIL(flux, option),T_S_(indice),R_T(commandeoption)));
                SECURISE(commande_encapsulation_commandeoption(T_S(commande),commandeoption));
                flux_inclusion=NULL;
                SECURISE(actionflux_execution_action(action,T_S(commande),general,C_S(flux_inclusion)));
                SECURISE(commande_desencapsulation_commandeoption(T_S(commande),C_S(commandeoptionretour)));
                maillon=NULL;
                SECURISE(flux_inclusion_flux(flux,T_S(maillon),T_S(flux_inclusion),general));
                SECURISE(flux_destruction(T_S(flux_inclusion)));
        }
        //Parcours des macros
        SECURISE(macro_lecture_taille(CHAMP_TRAVAIL(flux, macro),R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                TRAVAIL(CommandeDefine) commandedefine;
                STOCKAGE(CommandeDefine) commandedefineretour;
                STOCKAGE(Commande) commande;
                SECURISE(macro_lecture_macro(CHAMP_TRAVAIL(flux, macro),T_S_(indice),R_T(commandedefine)));
                SECURISE(commande_encapsulation_commandedefine(T_S(commande),commandedefine));
                flux_inclusion=NULL;
                SECURISE(actionflux_execution_action(action,T_S(commande),general,C_S(flux_inclusion)));
                SECURISE(commande_desencapsulation_commandedefine(T_S(commande),C_S(commandedefineretour)));
                maillon=NULL;
                SECURISE(flux_inclusion_flux(flux,T_S(maillon),T_S(flux_inclusion),general));
                SECURISE(flux_destruction(T_S(flux_inclusion)));
        }
        //Parcours des maillons
        for(maillon=CHAMP(flux, premier) ; maillon!=NULL ; )
        {
                flux_inclusion=NULL;
                SECURISE(actionflux_execution_action(action,T_S(CHAMP_STOCKAGE(maillon, commande)),general,C_S(flux_inclusion)));
                if(flux_inclusion==NULL)
                {//cas o� l'on ne touche pas au flux
                        maillon=CHAMP_STOCKAGE(maillon, suivant);
                }
                else
                {//cas o� l'on remplace la commande avec le flux r�sultant
                        STOCKAGE(MaillonFlux) supprime;
                        supprime=maillon;
                        maillon=CHAMP_STOCKAGE(maillon, suivant);
                        SECURISE(flux_destructiontotale_maillonflux(T_S(supprime),CHAMP_TRAVAIL(flux, premier),CHAMP_TRAVAIL(flux, dernier)));
                        SECURISE(flux_inclusion_flux(flux,T_S(maillon),T_S(flux_inclusion),general));
                        SECURISE(flux_destruction(T_S(flux_inclusion)));
                }
        }
        SECURISE(actionflux_execution_postaction(action,general,flux));
        SECURISE(actionflux_retrait_contexte(action));
        return RESULTAT_OK;
}

Resultat flux_copie(TRAVAIL(Flux) original, TRAVAIL(Flux) copie)
{
        /* Copie un flux. Si une allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�.
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(original)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_destruction(copie));
        SECURISE(flux_initialisation(copie));
        SECURISE(flux_definition_type(copie,T_S_(CHAMP(original, type))));
        SECURISE(option_copie(CHAMP_TRAVAIL(original, option),CHAMP_TRAVAIL(copie, option)));
        SECURISE(macro_copie(CHAMP_TRAVAIL(original, macro),CHAMP_TRAVAIL(copie, macro)));
        SECURISE(flux_copie_listeflux(CHAMP_TRAVAIL(original, premier),CHAMP_TRAVAIL(copie, premier),CHAMP_TRAVAIL(copie, dernier)));
        return RESULTAT_OK;
}

Resultat flux_destruction(TRAVAIL(Flux) flux)
{
        /* D�truit un flux et tout ce qui s'y trouve dedans.
         */
        if(S_T(flux)==NULL)
                return RESULTAT_OK;
        SECURISE(option_destruction(CHAMP_TRAVAIL(flux, option)));
        SECURISE(macro_destruction(CHAMP_TRAVAIL(flux, macro)));
        SECURISE(flux_destruction_listeflux(CHAMP_TRAVAIL(flux, premier)));
        free(S_T(flux));
        S_T(flux)=NULL;
        return RESULTAT_OK;
}

