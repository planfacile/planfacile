/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __MACRO__
#define __MACRO__

#include <src/global/global.h>

typedef struct macro CONTENEUR(Macro);

typedef enum
{
        MACROAJOUT_INTERDIT,
        //Cas o� les macros ne doivent
        //normalement pas �tre red�finies.
        MACROAJOUT_REMPLACE,
        //Cas o� une macro remplace la
        //macro de m�me nom lors d'un ajout.
        MACROAJOUT_CONSERVE
        //Cas o� une macro est conserv�e
        //� sa valeur d'origine lors
        //d'un ajout de macro de m�me
        //nom.
} CONTENEUR_SCALAIRE(TypeMacroAjout);
/* Pr�cise la fa�on dont doit se comporter
 * l'ajout des macros lorsque l'on doit
 * ajouter une macro d�j� d�finie.
 */

typedef enum
{
        MACROPROBLEME_VERBEUX,
        //Cas o� le remplacement d'une macro
        //doit se faire avec �mission d'un
        //message sur la sortie d'erreur.
        MACROPROBLEME_SILENCIEUX
        //Cas o� le remplacement d'une macro
        //doit se faire sans �mission d'un
        //message sur la sortie d'erreur.
} CONTENEUR_SCALAIRE(TypeMacroProbleme);
/* Pr�cise comment doit se comporter
 * l'ajout d'une macro vis-�-vis des
 * messages d'erreur.
 */

#include <src/donnees/general/general.h>
#include <src/problemes/probleme/probleme.h>
#include <src/donnees/commandes/localisationfichier.h>

#include <src/donnees/commandes/commandedefine.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

#define MACRO_NON_TROUVEE        -1

struct macro
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille r�serv�e en m�moire.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�ellement utilis�e.
        TABLEAU(STOCKAGE(CommandeDefine)) macro;
        //Macros d�clar�es dans le flux,
        //rang�es par ordre lexicographique du nom.
        STOCKAGE_SCALAIRE(TypeMacroAjout) ajout;
        //Pr�cise comment doit se comporter l'ajout
        //de cet ensemble de macros lors d'une fusion.
        STOCKAGE_SCALAIRE(TypeMacroProbleme) probleme;
        //Pr�cise comment doit se comporter l'ajout
        //de cet ensemble de macros lors d'une fusion.
};

Resultat macro_initialisation(TRAVAIL(Macro) macro);
/* Cr�e une structure de d�claration de macro vide.
 * Le comportement par d�faut est fix� � MACROAJOUT_REMPLACE
 * et MACROPROBLEME_VERBEUX.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 */

Resultat macro_definition_comportement(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(TypeMacroAjout) ajout, TRAVAIL_SCALAIRE(TypeMacroProbleme) probleme);
/* D�finit un comportement de fusion d'ensemble de macros.
 * Renvoie RESULTAT_ERREUR si macro est NULL.
 */

Resultat macro_ajout_macro(TRAVAIL(Macro) macro, TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(General) general, TRAVAIL_SCALAIRE(TypeMacroAjout) ajout, TRAVAIL_SCALAIRE(TypeMacroProbleme) probleme);
/* Ajoute une macro � la structure de d�claration de macro.
 * Les macros sont ajout�es dans l'ordre lexicographique.
 * Renvoie RESULTAT_ERREUR si macro est NULL.
 */

Resultat macro_lecture_taille(TRAVAIL(Macro) macro, REFERENCE_SCALAIRE(Taille) taille);
/* Lit la taille de l'ensemble de macros.
 * Renvoie RESULTAT_ERREUR si macro est NULL.
 */

Resultat macro_lecture_macro(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(CommandeDefine) commandedefine);
/* Renvoie une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si macro est NULL et RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat macro_recherche_macro(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(NomMacro) nommacro, REFERENCE_SCALAIRE(Indice) indice);
/* Renvoie l'indice de la macro pass�e en param�tre. Si elle n'est pas pr�sente,
 * la fonction renvoie MACRO_NON_TROUVEE � la place de l'indice.
 */

Resultat macro_fusion(TRAVAIL(Macro) destination, TRAVAIL(Macro) ajout, TRAVAIL(General) general);
/* Fusionne deux listes de d�claration de macro.
 * Les macros restent dans l'ordre lexicographique.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 */

Resultat macro_copie(TRAVAIL(Macro) macro, TRAVAIL(Macro) copie);
/* Cr�e une copie de la macro donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat macro_destruction(TRAVAIL(Macro) macro);
/* D�truit une liste de d�claration de macros.
 */

#endif
