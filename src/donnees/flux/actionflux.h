/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ACTIONFLUX__
#define __ACTIONFLUX__

#include <src/global/global.h>

typedef struct actionflux CONTENEUR(ActionFlux);

#include <src/donnees/general/general.h>

#include <src/donnees/flux/processusflux.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commande.h>

typedef struct pilecontexte CONTENEUR(PileContexte);

struct pilecontexte
{
        STOCKAGE_SCALAIRE(TypeFlux) contexte;
        //Contexte potentiel.
        STOCKAGE_SCALAIRE(TypeContexte) type;
        //Indique s'il s'agit bien d'un
        //contexte, o� d'un flux simple.
        STOCKAGE(PileContexte) suivant;
        //Indique le flux parent dans le parcours.
        STOCKAGE(PileContexte) prochain;
        //Indique le contexte englobant.
};
/* La pile de contextes est utilis�e lors d'un parcours
 * pour d�terminer quelles sont les actions � �x�cuter
 * sur une commande et dans quel ordre.
 * Cette structure sera le plus possible g�r�e en interne.
 */

struct actionflux
{
        STOCKAGE(PileContexte) contexte;
        STOCKAGE(ProcessusFlux) principal                ;
        STOCKAGE(ProcessusFlux) commentaire                ;
        STOCKAGE(ProcessusFlux) compilateur                ;
        STOCKAGE(ProcessusFlux) options                        ;
        STOCKAGE(ProcessusFlux) macro_definition        ;
        STOCKAGE(ProcessusFlux) macro_parametre                ;
        STOCKAGE(ProcessusFlux) message                        ;
        STOCKAGE(ProcessusFlux) document                ;
        STOCKAGE(ProcessusFlux) niveau                        ;
        STOCKAGE(ProcessusFlux) pertinence                ;
        STOCKAGE(ProcessusFlux) section_nom                ;
        STOCKAGE(ProcessusFlux) section_format                ;
        STOCKAGE(ProcessusFlux) section_section                ;
        STOCKAGE(ProcessusFlux) reference_format        ;
        STOCKAGE(ProcessusFlux) reference                ;
        STOCKAGE(ProcessusFlux) reference_automatique        ;
        STOCKAGE(ProcessusFlux) titre                        ;
        STOCKAGE(ProcessusFlux) titre_automatique        ;
        STOCKAGE(ProcessusFlux) texte                        ;
        STOCKAGE(ProcessusFlux) texte_reductible        ;
        STOCKAGE(ProcessusFlux) texte_irreductible        ;
        STOCKAGE(ProcessusFlux) texte_manquante                ;
        STOCKAGE(ProcessusFlux) texte_generique                ;
        STOCKAGE(ProcessusFlux) indice                        ;
};
/* Repr�sentation des diff�rentes actions qui peuvent �tre men�es lors d'un parcours
 * de flux. Les actions envisag�es sont plac�es par type de flux, puis pour chaque type,
 * selon la commande qui sera rencontr�e lors du parcours.
 */

Resultat actionflux_initialisation(TRAVAIL(ActionFlux) action);
/* Cr�e et initialise une structure d'action sur flux.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat actionflux_ajout_contexte(TRAVAIL(ActionFlux) action, TRAVAIL_SCALAIRE(TypeFlux) contexte, TRAVAIL_SCALAIRE(TypeContexte) type);
/* Ajoute un contexte � la pile de contextes.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat actionflux_retrait_contexte(TRAVAIL(ActionFlux) action);
/* Retire un contexte de la pile de contextes.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat actionflux_definition_principal                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) principal                        );
Resultat actionflux_definition_commentaire                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) commentaire                );
Resultat actionflux_definition_compilateur                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) compilateur                );
Resultat actionflux_definition_options                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) options                        );
Resultat actionflux_definition_macro_definition                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) macro_definition                );
Resultat actionflux_definition_macro_parametre                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) macro_parametre                );
Resultat actionflux_definition_message                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) message                        );
Resultat actionflux_definition_document                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) document                        );
Resultat actionflux_definition_niveau                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) niveau                        );
Resultat actionflux_definition_pertinence                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) pertinence                        );
Resultat actionflux_definition_section_nom                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_nom                );
Resultat actionflux_definition_section_format                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_format                );
Resultat actionflux_definition_section_section                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) section_section                );
Resultat actionflux_definition_reference_format                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference_format                );
Resultat actionflux_definition_reference                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference                        );
Resultat actionflux_definition_reference_automatique        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) reference_automatique        );
Resultat actionflux_definition_titre                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) titre                        );
Resultat actionflux_definition_titre_automatique        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) titre_automatique                );
Resultat actionflux_definition_texte                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte                        );
Resultat actionflux_definition_texte_reductible                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_reductible                );
Resultat actionflux_definition_texte_irreductible        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_irreductible                );
Resultat actionflux_definition_texte_manquante                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_manquante                );
Resultat actionflux_definition_texte_generique                (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) texte_generique                );
Resultat actionflux_definition_indice                        (TRAVAIL(ActionFlux) action, TRAVAIL(ProcessusFlux) indice                        );
/* Ces fonctions servent � d�finir les diff�rents comportement � adopter selon le type de flux
 * rencontr� lors d'un parcours.
 * Ces fonctions renvoient RESULTAT_ERREUR si le pointeur sur la structure est NULL.
 */

Resultat actionflux_lecture_principal                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) principal                );
Resultat actionflux_lecture_commentaire                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) commentaire                );
Resultat actionflux_lecture_compilateur                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) compilateur                );
Resultat actionflux_lecture_options                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) options                        );
Resultat actionflux_lecture_macro_definition                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) macro_definition                );
Resultat actionflux_lecture_macro_parametre                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) macro_parametre                );
Resultat actionflux_lecture_message                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) message                        );
Resultat actionflux_lecture_document                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) document                        );
Resultat actionflux_lecture_niveau                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) niveau                        );
Resultat actionflux_lecture_pertinence                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) pertinence                );
Resultat actionflux_lecture_section_nom                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_nom                );
Resultat actionflux_lecture_section_format                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_format                );
Resultat actionflux_lecture_section_section                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) section_section                );
Resultat actionflux_lecture_reference_format                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference_format                );
Resultat actionflux_lecture_reference                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference                );
Resultat actionflux_lecture_reference_automatique        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) reference_automatique        );
Resultat actionflux_lecture_titre                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) titre                        );
Resultat actionflux_lecture_titre_automatique                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) titre_automatique        );
Resultat actionflux_lecture_texte                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte                        );
Resultat actionflux_lecture_texte_reductible                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_reductible                );
Resultat actionflux_lecture_texte_irreductible                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_irreductible        );
Resultat actionflux_lecture_texte_manquante                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_manquante                );
Resultat actionflux_lecture_texte_generique                (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) texte_generique                );
Resultat actionflux_lecture_indice                        (TRAVAIL(ActionFlux) action, REFERENCE(ProcessusFlux) indice                        );
/* Ces fonctions servent � lire une valeur dans la structure d'action de flux.
 * Ces fonctions renvoient RESULTAT_ERREUR si le pointeur sur la structure est NULL.
 */

Resultat actionflux_lecture_processusflux(TRAVAIL(ActionFlux) action, TRAVAIL_SCALAIRE(TypeFlux) type, REFERENCE(ProcessusFlux) processusflux);
/* Lit le processus flux associ� au type de flux indiqu�.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le type est incorrect.
 */

Resultat actionflux_execution_preaction(TRAVAIL(ActionFlux) action, TRAVAIL(General) general, TRAVAIL(Flux) flux);
/* Execute les pr�actions des types de flux des contextes
 * en cours sur le flux indiqu�.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 */

Resultat actionflux_execution_action(TRAVAIL(ActionFlux) action, TRAVAIL(Commande) commande, TRAVAIL(General) general, COREFERENCE(Flux) flux);
/* Ex�cute les actions sur la commande en tenant compte des
 * contextes en cours.
 * Le r�sultat du traitement de la commande est pass� en param�tre (flux).
 * Le type de parcours de la commande est �galement pris en compte ici.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
 */

Resultat actionflux_execution_postaction(TRAVAIL(ActionFlux) action, TRAVAIL(General) general, TRAVAIL(Flux) flux);
/* Execute les postactions des types de flux des contextes
 * en cours sur le flux indiqu�.
 * Renvoie RESULTAT_ERREUR si action est NULL.
 */

Resultat actionflux_copie(TRAVAIL(ActionFlux) action, TRAVAIL(ActionFlux) copie);
/* Copie une structure d'action de flux. Si une allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�.
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat actionflux_destruction(TRAVAIL(ActionFlux) action);
/* Destruction d'une structure d'action sur flux.
 */

#endif
