/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PROCESSUSFLUX__
#define __PROCESSUSFLUX__

#include <src/global/global.h>

typedef struct processusflux CONTENEUR(ProcessusFlux);

typedef enum
{
        CONTEXTE_LOCAL,
        //Parcours de flux simple,
        //invisible pour les flux inclus.
        CONTEXTE_CONTEXTE
        //Parcours de flux contextuel,
        //visible pour les flux inclus.
} CONTENEUR_SCALAIRE(TypeContexte);
/* Description du type de parcours d'un
 * flux complet. Les contextes permettent
 * d'agir sur une partie enti�re d'un flux
 * et des flux inclus.
 */

#include <src/donnees/general/general.h>

#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/actioncommande.h>

typedef Resultat (FONCTION(Processus))(TRAVAIL(Flux), TRAVAIL(General), COREFERENCE_SCALAIRE(Booleen));
/* D�fini le type utilis� pour les pr�actions
 * et les postactions.
 * Ces actions sont r�alis�es sur le flux en question,
 * et le bool�en sert � indiquer si le parcours des
 * contextes doit �tre arr�t�, en le pla�ant � VRAI.
 */

struct processusflux
{
        STOCKAGE_SCALAIRE(Processus) preaction;
        //D�finit l'action � r�aliser avant toute action sur les commandes.
        STOCKAGE(ActionCommande) action;
        //D�finit l'action � r�aliser sur les commandes.
        STOCKAGE_SCALAIRE(Processus) postaction;
        //D�finit l'action � r�aliser apr�s toute action sur les commandes.
        STOCKAGE_SCALAIRE(TypeContexte) contexte;
        //D�finit le type du flux vis-�-vis des parcours.
};
/* Repr�sente l'ensemble des actions disponibles sur un flux.
 */

Resultat processusflux_initialisation(TRAVAIL(ProcessusFlux) processusflux);
/* Cr�e et initialise une structure d'action sur un flux particulier sans actions associ�es
 * aux diff�rents types de commande, ni aux actions de pr� et post parcours des commandes.
 * Renvoie une erreur RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Le type de contexte est plac� � CONTEXTE_LOCAL par d�faut.
 */

Resultat processusflux_definition_preaction(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(Processus) preaction);
/* D�finit l'action effectu�e avant le parcours des commandes.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_definition_actioncommande(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL(ActionCommande) actioncommande);
/* D�finit l'action effectu�e pour chaque commande incluse dans le flux.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_definition_postaction(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(Processus) postaction);
/* D�finit l'action effectu�e apr�s le parcours des commandes.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_definition_contexte(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(TypeContexte) contexte);
/* Indique le type de contexte employ� pour ce flux.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_lecture_preaction(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(Processus) preaction);
/* Lit l'action effectu�e avant le parcours des commandes.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_lecture_actioncommande(TRAVAIL(ProcessusFlux) processusflux, REFERENCE(ActionCommande) actioncommande);
/* Lit l'action effectu�e pour chaque commande incluse dans le flux.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_lecture_postaction(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(Processus) postaction);
/* Lit l'action effectu�e apr�s le parcours des commandes.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_lecture_contexte(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(TypeContexte) contexte);
/* Indique le type de contexte employ� pour ce flux.
 * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
 */

Resultat processusflux_copie(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL(ProcessusFlux) copie);
/* Cr�e une copie du processusflux donn� en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat processusflux_destruction(TRAVAIL(ProcessusFlux) processusflux);
/* D�truit une structure d'action sur les flux.
 */

#endif
