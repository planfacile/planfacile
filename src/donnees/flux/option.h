/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __OPTION__
#define __OPTION__

#include <src/global/global.h>

typedef struct option CONTENEUR(Option);

#include <src/donnees/commandes/commandeoption.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

#define OPTION_NON_TROUVEE        -1

struct option
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille r�serv�e en m�moire.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�ellement utilis�e.
        TABLEAU(STOCKAGE(CommandeOption)) option;
        //Options d�clar�es dans le flux,
        //rang�es par ordre lexicographique du nom.
};

Resultat option_initialisation(TRAVAIL(Option) option);
/* Cr�e une structure de d�claration d'option vide.
 */

Resultat option_ajout_option(TRAVAIL(Option) option, TRAVAIL(CommandeOption) commandeoption);
/* Ajoute une option � la structure de d�claration d'option.
 * Les options sont ajout�es dans l'ordre lexicographique.
 */

Resultat option_lecture_taille(TRAVAIL(Option) option, REFERENCE_SCALAIRE(Taille) taille);
/* Renvoie la taille du tableau d'options.
 * Renvoie RESULTAT_ERREUR si option est NULL.
 */

Resultat option_lecture_option(TRAVAIL(Option) option, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(CommandeOption) commandeoption);
/* Retourne la commande d'option enregistr�e � l'indice indiqu�.
 * Renvoie RESULTAT_ERREUR si option est NULL.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat option_recherche_option(TRAVAIL(Option) option, TRAVAIL_SCALAIRE(NomOption) nomoption, REFERENCE_SCALAIRE(Indice) indice);
/* Renvoie l'indice de l'option pass�e en param�tre. Si elle n'est pas pr�sente,
 * la fonction renvoie OPTION_NON_TROUVEE � la place de l'indice.
 */

Resultat option_fusion(TRAVAIL(Option) destination, TRAVAIL(Option) ajout);
/* Fusionne deux listes de d�claration d'option, en ajoutant
 * les options d�clar�es dans ajout � celles de la destination.
 * Les options restent dans l'ordre lexicographique.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat option_copie(TRAVAIL(Option) option, TRAVAIL(Option) copie);
/* Cr�e une copie de l'option donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat option_destruction(TRAVAIL(Option) option);
/* D�truit une liste de d�claration d'options.
 */

#endif
