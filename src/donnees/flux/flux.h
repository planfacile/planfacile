/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __FLUX__
#define __FLUX__

#include <src/global/global.h>

typedef struct flux CONTENEUR(Flux);

typedef enum {
        FLUX_VIDE,
        //Flux dont le type est invalide.
        FLUX_PRINCIPAL,
        //Ceci repr�sente le flux racine, contenant toutes les commandes de style.
        FLUX_COMMENTAIRE,
        //Ceci est le contenu d'un commentaire.
        //Voir commande #comment.
        FLUX_COMPILATEUR,
        //Ceci est le contenu d'une commande de message vers l'utilisateur.
        //Voir commandes #warning et #error.
        FLUX_OPTIONS,
        //Ceci repr�sente un flux inclus dans une clause de la commande de s�l�ction
        //de code.
        //Voir l'ensemble de commandes #options, #case, #other et #end.
        FLUX_MACRO_DEFINITION,
        //Ceci est le contenu d'une d�finition de macro.
        //Voir commande #define.
        FLUX_MACRO_PARAMETRE,
        //Ceci est le contenu d'un param�tre utilis� lors d'un appel de macro.
        //Voir les commandes d'appel de macro.
        FLUX_MESSAGE,
        //Ceci est le contenu d'un message.
        //Voir commande #message.
        FLUX_DOCUMENT,
        //Ceci est le contenu d'un ent�te ou d'un pied de document.
        //Voir commandes #head et #foot.
        FLUX_NIVEAU,
        //Ceci repr�sente un niveau de section.
        //Voir commandes #start, #section, #reference, #sec.
        FLUX_PERTINENCE,
        //Ceci repr�sente une pertinence,
        //Voir commande #dep.
        FLUX_SECTION_NOM,
        //Ceci repr�sente un nom de section.
        //Voir commande #section.
        FLUX_SECTION_FORMAT,
        //Ceci repr�sente un format de section.
        //Voir commande #section.
        FLUX_SECTION_SECTION,
        //Ceci repr�sente les textes de pr� et post section.
        //Voir commande #section.
        FLUX_REFERENCE_FORMAT,
        //Ceci repr�sente un format de r�f�rence.
        //Voir commandes #reference, #depref, #extrefs et #extref.
        FLUX_REFERENCE,
        //Ceci repr�sente un nom de r�f�rence pour une id�e.
        //Voir commandes #idea et #dep.
        FLUX_REFERENCE_AUTOMATIQUE,
        //Ceci repr�sente une r�f�rence d'id�e automatique.
        //Voir commandes #missing et #generic.
        FLUX_TITRE,
        //Ceci repr�sente un flux de d�finition de titre d'id�e.
        //Voir commande #idea.
        FLUX_TITRE_AUTOMATIQUE,
        //Ceci repr�sente un flux de d�finition de titre d'id�e automatique.
        //Voir commande #missing et #generic.
        FLUX_TEXTE,
        //Ceci repr�sente un flux de texte d'id�e.
        //Voir commande #idea.
        FLUX_TEXTE_REDUCTIBLE,
        //Ceci repr�sente un flux de texte de d�pendance r�ductible.
        //Voir commande #dep.
        FLUX_TEXTE_IRREDUCTIBLE,
        //Ceci repr�sente un flux de texte de d�pendance irr�ductible.
        //Voir commande #dep.
        FLUX_TEXTE_MANQUANTE,
        //Ceci repr�sente un flux de texte d'id�e manquante.
        //Voir commande #missing.
        FLUX_TEXTE_GENERIQUE,
        //Ceci repr�sente un flux de texte d'id�e g�n�rique.
        //Voir commande #generic.
        FLUX_INDICE
        //Ceci repr�sente un index d'id�e automatique.
        //Voir commandes #missing, #generic et #extref.
} CONTENEUR_SCALAIRE(TypeFlux);
/* Repr�sentation des divers contextes de flux.
 * Ils permettent d'assurer la v�rification de la validit� syntaxique des diff�rentes
 * commandes selon leur position dans le flux global.
 */

#include <src/donnees/general/general.h>
#include <src/donnees/flux/actionflux.h>

#include <src/donnees/flux/option.h>
#include <src/donnees/flux/macro.h>
#include <src/donnees/commandes/commande.h>

/* Le but de cette section est de repr�senter un flux de commande du compilateur.
 * Il s'agit en particulier de pr�ciser clairement le contexte dans lequel s'inscrit
 * les diff�rentes commandes pouvant �tre rencontr�es lors du traitement.
 */

typedef struct maillonflux CONTENEUR(MaillonFlux);

struct maillonflux
{
        STOCKAGE(MaillonFlux) precedent;
        //Pointeur sur la structure pr�c�dente du flux.
        //Pour le premier �l�ment de la liste, il vaut NULL.
        STOCKAGE(Commande) commande;
        //Une commande. Ce type repr�sente en fait n'importe quelle commande.
        //Si cette valeur vaut NULL, le maillon sera supprim� de la liste doublement
        //chain�e au parcours suivant. Cependant, ce cas ne devrait a priori pas se
        //produire.
        STOCKAGE(MaillonFlux) suivant;
        //Pointeur sur la structure suivante.
        //Pour le dernier �l�ment de la liste, il vaut NULL.
};
/* Repr�sentation d'un maillon de la liste doublement chain�e servant � contenir
 * les diff�rentes commandes d'un flux.
 * L'utilisation de cette structure sera transparente.
 * Elle n'est donn�e qu'� titre informatif.
 */

struct flux
{
        STOCKAGE_SCALAIRE(TypeFlux) type;
        //Type du flux.
        STOCKAGE(Option) option;
        //Options d�finies dans le flux.
        STOCKAGE(Macro) macro;
        //Macros d�finies dans le flux : elles seront locales � ce flux.
        STOCKAGE(MaillonFlux) premier;
        //Pointe vers la premi�re commande du flux.
        //Vaut NULL si le flux est vide.
        STOCKAGE(MaillonFlux) dernier;
        //Pointe vers la derni�re commande du flux.
        //Vaut NULL si le flux est vide.
};
/* Repr�sentation d'un flux au sens le plus g�n�ral. Il s'agit d'une succession de
 * commandes dans un contexte particulier permettant l'utilisation de commandes
 * particuli�res au sein d'un flux.
 */

Resultat flux_initialisation(TRAVAIL(Flux) flux);
/* Cr�e un flux vide. Si l'allocation �choue, renvoie RESULTAT_ERREUR_MEMOIRE.
 */

Resultat flux_definition_type(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(TypeFlux) type);
/* Donne un type � un flux de type vide.
 * Si le flux n'est pas de type vide,
 * RESULTAT_ERREUR_DOMAINE est renvoy�.
 * Renvoie RESULTAT_ERREUR si flux est NULL.
 */

Resultat flux_ajout_commande(TRAVAIL(Flux) flux, TRAVAIL(Commande) commande, TRAVAIL(General) general);
/* Ajoute une commande � un flux. Si l'allocation �choue, renvoie RESULTAT_ERREUR_MEMOIRE.
 * Renvoie aussi RESULTAT_ERREUR si le type de la commande est incorrect.
 */

Resultat flux_lecture_type(TRAVAIL(Flux) flux, REFERENCE_SCALAIRE(TypeFlux) type);
/* Renvoie le type d'un flux.
 * Renvoie RESULTAT_ERREUR si le flux est NULL.
 */

Resultat flux_lecture_option(TRAVAIL(Flux) flux, REFERENCE(Option) option);
/* Renvoie la structure d'option d'un flux.
 * Renvoie RESULTAT_ERREUR si le flux est NULL.
 */

Resultat flux_lecture_macro(TRAVAIL(Flux) flux, REFERENCE(Macro) macro);
/* Renvoie la structure de macro d'un flux.
 * Renvoie RESULTAT_ERREUR si le flux est NULL.
 */

Resultat flux_concatenation(TRAVAIL(Flux) flux, TRAVAIL(Flux) ajout, TRAVAIL(General) general);
/* Ajoute le flux ajout au flux pass� en premier param�tre.
 * La concat�nation se fait directement sur le flux
 * destination.
 * Renvoie RESULTAT_ERREUR si flux est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Il ne se passe rien si le flux d'ajout est NULL.
 */

Resultat flux_defragmentation(TRAVAIL(Flux) flux);
/* D�fragmente le texte d'un flux, et
 * remplace les caract�res d'�chappement
 * par leur valeur.
 * Cette d�fragmentation n'est pas poursuivie
 * sur les flux inclus dans les commandes !
 * Renvoie RESULTAT_ERREUR si flux est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR_DOMAINE si une commande est vide.
 */

Resultat flux_texte(TRAVAIL(Flux) flux, REFERENCE_SCALAIRE(Chaine) texte);
/* Renvoie le texte le plus court pr�sent en
 * d�but du flux.
 * Renvoie RESULTAT_ERREUR si flux est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat flux_nombre(TRAVAIL(Flux) flux, COREFERENCE_SCALAIRE(Booleen) nombre, REFERENCE_SCALAIRE(Entier) valeur);
/* Indique si le d�but d'un flux correspond � un
 * nombre, et renvoie sa valeur le cas �ch�ant.
 * La valeur est renvoy�e uniquement si le pointeur
 * correspondant est non NULL.
 * Renvoie RESULTAT_ERREUR si flux est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat flux_reference_manquante(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(Chaine) reference);
/* Construit la chaine de r�f�rence relative � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si le flux est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le flux n'est pas de type
 * FLUX_REFERENCE_AUTOMATIQUE.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat flux_reference_generique(TRAVAIL(Flux) flux, TRAVAIL_SCALAIRE(Chaine) referenceparent, REFERENCE_SCALAIRE(Chaine) reference);
/* Construit la chaine de r�f�rence relative � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si le flux est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le flux n'est pas de type
 * FLUX_REFERENCE_AUTOMATIQUE.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

/* Pour une structure de flux, il n'y aura pas de possibilit� de r�cup�ration
 * de maillons, car cette structure est jug�e trop complexe pour
 * permettre une lecture en direct dessus. Cette structure sera donc manipul�e
 * via la fonction flux_parcours.
 */

Resultat flux_parcours(TRAVAIL(Flux) flux, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours de flux. Ce parcours se fait it�rativement sur un flux donn�
 * et r�cursivement sur tous les flux contenus en interne.
 * Les actions � r�aliser sont indiqu�es par la structure indiqu�e.
 */

Resultat flux_copie(TRAVAIL(Flux) original, TRAVAIL(Flux) copie);
/* Copie un flux. Si une allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�.
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat flux_destruction(TRAVAIL(Flux) flux);
/* D�truit un flux et tout ce qui s'y trouve dedans.
 */

#endif
