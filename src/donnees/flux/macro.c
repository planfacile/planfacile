/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "macro.h"

static Resultat macro_ajout_elements(TRAVAIL(Macro) macro)
{
        /* Fait en sorte qu'en sortie de fonction, au moins une place libre
         * et r�serv�e en m�moire soit disponible.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �ventuelle �choue.
         */
        TABLEAU(STOCKAGE(CommandeDefine)) nouveau;
        if(T_S_(CHAMP(macro, taille))<T_S_(CHAMP(macro, memoire)))
                return RESULTAT_OK;
        nouveau=REALLOCATION_CAST(CHAMP(macro, macro), CommandeDefine, T_S_(CHAMP(macro, memoire))*T_S_(MULTTAILLE));
        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(macro, memoire) *= T_S_(MULTTAILLE);
        CHAMP(macro, macro)=nouveau;
        return RESULTAT_OK;
}

static Resultat macro_rechercheinterne_macro(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(NomMacro) nommacro, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Renvoie l'indice de la place occup�e par une
         * macro, m�me si elle est absente.
         */
        TRAVAIL_SCALAIRE(NomMacro) nom;
        TRAVAIL_SCALAIRE(Indice) inferieur;
        TRAVAIL_SCALAIRE(Indice) median;
        TRAVAIL_SCALAIRE(Indice) superieur;
        inferieur=T_S_(0);
        superieur=T_S_(CHAMP(macro, taille))-T_S_(1);
        while(inferieur<=superieur)
        {
                median=(inferieur+superieur)/T_S_(2);
                SECURISE(commandedefine_lecture_nom(ELEMENT_TRAVAIL(CHAMP(macro, macro), median),R_T_(nom)));
                if(STRCMP(nom, nommacro)<T_S_(0))
                        inferieur=median+T_S_(1);
                else
                        superieur=median-T_S_(1);
        }
        T_R_(indice)=inferieur;
        return RESULTAT_OK;
}

Resultat macro_initialisation(TRAVAIL(Macro) macro)
{
        /* Cr�e une structure de d�claration de macro vide.
         * Le comportement par d�faut est fix� � MACROAJOUT_REMPLACE
         * et MACROPROBLEME_VERBEUX.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION((S_T(macro)=NOUVEAU(Macro))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        if((CHAMP(macro, macro)=NOUVEAUX(CommandeDefine, T_S_(TAILLEINIT)))==NULL)
        {
                free(S_T(macro));
                S_T(macro)=NULL;
                return RESULTAT_ERREUR_MEMOIRE;
        }
        CHAMP(macro, memoire)=TAILLEINIT;
        CHAMP(macro, taille)=0;
        CHAMP(macro, ajout)=MACROAJOUT_REMPLACE;
        CHAMP(macro, probleme)=MACROPROBLEME_VERBEUX;
        return RESULTAT_OK;
}

Resultat macro_definition_comportement(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(TypeMacroAjout) ajout, TRAVAIL_SCALAIRE(TypeMacroProbleme) probleme)
{
        /* D�finit un comportement de fusion d'ensemble de macros.
         * Renvoie RESULTAT_ERREUR si macro est NULL.
         */
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);

        CHAMP(macro, ajout)=ajout;
        CHAMP(macro, probleme)=probleme;
        return RESULTAT_OK;
}

Resultat macro_ajout_macro(TRAVAIL(Macro) macro, TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(General) general, TRAVAIL_SCALAIRE(TypeMacroAjout) ajout, TRAVAIL_SCALAIRE(TypeMacroProbleme) probleme)
{
        /* Ajoute une macro � la structure de d�claration de macro.
         * Les macros sont ajout�es dans l'ordre lexicographique.
         * Si une macro du m�me nom existe d�j�, elle est remplac�e.
         */
        TRAVAIL_SCALAIRE(Indice) insertion;
        TRAVAIL_SCALAIRE(NomMacro) nom ;
        TRAVAIL_SCALAIRE(NomMacro) nominsertion;
        STOCKAGE_SCALAIRE(Booleen) testnom;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveaumacro;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauajout;
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedefine_lecture_nom(commandedefine,R_T_(nom)));
        SECURISE(macro_rechercheinterne_macro(macro,nom,R_T_(insertion)));
        if(insertion<T_S_(CHAMP(macro, taille)))
        {
                SECURISE(commandedefine_lecture_nom(ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion),R_T_(nominsertion)));
                testnom=(STRCMP(nom, nominsertion)!=T_S_(0))?VRAI:FAUX;
        }
        else
                testnom=VRAI;
        if(testnom==T_S_(VRAI))
        {
                SECURISE(macro_ajout_elements(macro));
                STOCKAGE_SCALAIRE(Indice) indice;
                for(indice=CHAMP(macro, taille) ; T_S_(indice)>insertion ; T_S_(indice)--)
                        ELEMENT(CHAMP(macro, macro), T_S_(indice))=ELEMENT(CHAMP(macro, macro), T_S_(indice)-1);
                T_S_(CHAMP(macro, taille))++;
                ELEMENT(CHAMP(macro, macro), insertion)=NULL;
                SECURISE(commandedefine_copie(commandedefine,ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion)));
        }
        else
        {
                //Red�finition de macro, en tenant compte du mode d'ajout, et, le cas �ch�ant, des niveaux hi�rarchiques des
                //d�finitions.
                SECURISE(commandedefine_lecture_niveau(ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion),R_T_(niveaumacro)));
                SECURISE(commandedefine_lecture_niveau(commandedefine,R_T_(niveauajout)));
                if((ajout==T_S_(MACROAJOUT_CONSERVE))&&(niveauajout<=niveaumacro))
                {
                        S_T_(ajout)=MACROAJOUT_REMPLACE;
                }
                switch(ajout)
                {
                        case T_S_(MACROAJOUT_REMPLACE):
                                switch(probleme)
                                {
                                        case T_S_(MACROPROBLEME_SILENCIEUX):
                                                break;
                                        case T_S_(MACROPROBLEME_VERBEUX):
                                                {
                                                        STOCKAGE(LocalisationFichier) localisation;
                                                        TRAVAIL(LocalisationFichier) localisationcommande;
                                                        TRAVAIL_SCALAIRE(NomMacro) nommacro;
                                                        SECURISE(commandedefine_lecture_nom(commandedefine, R_T_(nommacro)));
                                                        SECURISE(commandedefine_lecture_localisationfichier(commandedefine, R_T(localisationcommande)));
                                                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                                                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_REDEFINITION_MACRO), T_S(localisation),nommacro));
                                                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                                                }
                                                break;
                                        default:
                                                return RESULTAT_ERREUR_DOMAINE;
                                }
                                SECURISE(commandedefine_copie(commandedefine,ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion)));
                                break;
                        case T_S_(MACROAJOUT_CONSERVE):
                                switch(probleme)
                                {
                                        case T_S_(MACROPROBLEME_SILENCIEUX):
                                                break;
                                        case T_S_(MACROPROBLEME_VERBEUX):
                                                {
                                                        STOCKAGE(LocalisationFichier) localisation;
                                                        TRAVAIL(LocalisationFichier) localisationcommande;
                                                        TRAVAIL_SCALAIRE(NomMacro) nommacro;
                                                        SECURISE(commandedefine_lecture_nom(ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion), R_T_(nommacro)));
                                                        SECURISE(commandedefine_lecture_localisationfichier(ELEMENT_TRAVAIL(CHAMP(macro, macro), insertion), R_T(localisationcommande)));
                                                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                                        SECURISE(localisationfichier_copie(localisationcommande, T_S(localisation)));
                                                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_REDEFINITION_MACRO), T_S(localisation), nommacro));
                                                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                                                }
                                                break;
                                        default:
                                                return RESULTAT_ERREUR_DOMAINE;
                                }
                                break;
                        case T_S_(MACROAJOUT_INTERDIT):
                        default:
                                return RESULTAT_ERREUR_DOMAINE;
                }
        }
        return RESULTAT_OK;
}

Resultat macro_lecture_taille(TRAVAIL(Macro) macro, REFERENCE_SCALAIRE(Taille) taille)
{
        /* Lit la taille de l'ensemble de macros.
         * Renvoie RESULTAT_ERREUR si macro est NULL.
         */
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);

        T_R_(taille)=T_S_(CHAMP(macro, taille));
        return RESULTAT_OK;
}

Resultat macro_lecture_macro(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(CommandeDefine) commandedefine)
{
        /* Renvoie une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si macro est NULL et RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(macro, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        T_R(commandedefine)=ELEMENT_TRAVAIL(CHAMP(macro, macro), indice);
        return RESULTAT_OK;
}

Resultat macro_recherche_macro(TRAVAIL(Macro) macro, TRAVAIL_SCALAIRE(NomMacro) nommacro, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Renvoie l'indice de la macro pass�e en param�tre. Si elle n'est pas pr�sente,
         * la fonction renvoie MACRO_NON_TROUVEE � la place de l'indice.
         */
        TRAVAIL_SCALAIRE(NomMacro) nom;
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);

        SECURISE(macro_rechercheinterne_macro(macro,nommacro,indice));
        if(T_R_(indice)>=T_S_(CHAMP(macro, taille)))
                T_R_(indice)=T_S_(MACRO_NON_TROUVEE);
        else
        {
                SECURISE(commandedefine_lecture_nom(ELEMENT_TRAVAIL(CHAMP(macro, macro), T_R_(indice)),R_T_(nom)));
                if(STRCMP(nom, nommacro)!=T_S_(0))
                        T_R_(indice)=T_S_(MACRO_NON_TROUVEE);
        }
        return RESULTAT_OK;
}

Resultat macro_fusion(TRAVAIL(Macro) destination, TRAVAIL(Macro) ajout, TRAVAIL(General) general)
{
        /* Fusionne deux listes de d�claration de macro.
         * Les macros restent dans l'ordre lexicographique.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(destination)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T(ajout)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(ajout, taille)) ; T_S_(indice)++)
        {
                SECURISE(macro_ajout_macro(destination,ELEMENT_TRAVAIL(CHAMP(ajout, macro), T_S_(indice)),general,T_S_(CHAMP(ajout, ajout)),T_S_(CHAMP(ajout, probleme))));
        }
        return RESULTAT_OK;
}

Resultat macro_copie(TRAVAIL(Macro) macro, TRAVAIL(Macro) copie)
{
        /* Cr�e une copie de la macro donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(macro)!=NULL, RESULTAT_ERREUR);

        SECURISE(macro_destruction(copie));
        SECURISE(macro_initialisation(copie));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(macro, taille)) ; T_S_(indice)++)
        {
                SECURISE(macro_ajout_macro(copie,ELEMENT_TRAVAIL(CHAMP(macro, macro), T_S_(indice)),T_S_(NULL),T_S_(MACROAJOUT_INTERDIT),T_S_(MACROPROBLEME_SILENCIEUX)));
        }
        return RESULTAT_OK;
}

Resultat macro_destruction(TRAVAIL(Macro) macro)
{
        /* D�truit une liste de d�claration de macros.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(macro)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(macro, taille)) ; T_S_(indice)++)
        {
                SECURISE(commandedefine_destruction(ELEMENT_TRAVAIL(CHAMP(macro, macro), T_S_(indice))));
        }
        free(CHAMP(macro, macro));
        free(S_T(macro));
        S_T(macro)=NULL;
        return RESULTAT_OK;
}

