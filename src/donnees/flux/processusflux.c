/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "processusflux.h"

Resultat processusflux_initialisation(TRAVAIL(ProcessusFlux) processusflux)
{
        /* Cr�e et initialise une structure d'action sur un flux particulier sans actions associ�es
         * aux diff�rents types de commande, ni aux actions de pr� et post parcours des commandes.
         * Renvoie une erreur RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(processusflux)=NOUVEAU(ProcessusFlux))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(processusflux, preaction)=NULL;
        CHAMP(processusflux, action)=NULL;
        CHAMP(processusflux, postaction)=NULL;
        CHAMP(processusflux, contexte)=CONTEXTE_LOCAL;
        return RESULTAT_OK;
}

Resultat processusflux_definition_preaction(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(Processus) preaction)
{
        /* D�finit l'action effectu�e avant le parcours des commandes.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        CHAMP(processusflux, preaction)=S_T_(preaction);
        return RESULTAT_OK;
}

Resultat processusflux_definition_actioncommande(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL(ActionCommande) actioncommande)
{
        /* D�finit l'action effectu�e pour chaque commande incluse dans le flux.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        SECURISE(actioncommande_copie(actioncommande,CHAMP_TRAVAIL(processusflux, action)));
        return RESULTAT_OK;
}

Resultat processusflux_definition_postaction(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(Processus) postaction)
{
        /* D�finit l'action effectu�e apr�s le parcours des commandes.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        CHAMP(processusflux, postaction)=S_T_(postaction);
        return RESULTAT_OK;
}

Resultat processusflux_definition_contexte(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL_SCALAIRE(TypeContexte) contexte)
{
        /* Indique le type de contexte employ� pour ce flux.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        CHAMP(processusflux, contexte)=S_T_(contexte);
        return RESULTAT_OK;
}

Resultat processusflux_lecture_preaction(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(Processus) preaction)
{
        /* Lit l'action effectu�e avant le parcours des commandes.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        T_R_(preaction)=T_S_(CHAMP(processusflux, preaction));
        return RESULTAT_OK;
}

Resultat processusflux_lecture_actioncommande(TRAVAIL(ProcessusFlux) processusflux, REFERENCE(ActionCommande) actioncommande)
{
        /* Lit l'action effectu�e pour chaque commande incluse dans le flux.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        T_R(actioncommande)=CHAMP_TRAVAIL(processusflux, action);
        return RESULTAT_OK;
}

Resultat processusflux_lecture_postaction(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(Processus) postaction)
{
        /* Lit l'action effectu�e apr�s le parcours des commandes.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        T_R_(postaction)=T_S_(CHAMP(processusflux, postaction));
        return RESULTAT_OK;
}

Resultat processusflux_lecture_contexte(TRAVAIL(ProcessusFlux) processusflux, REFERENCE_SCALAIRE(TypeContexte) contexte)
{
        /* Indique le type de contexte employ� pour ce flux.
         * Renvoie RESULTAT_ERREUR si le processusflux est NULL.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        T_R_(contexte)=T_S_(CHAMP(processusflux, contexte));
        return RESULTAT_OK;
}

Resultat processusflux_copie(TRAVAIL(ProcessusFlux) processusflux, TRAVAIL(ProcessusFlux) copie)
{
        /* Cr�e une copie du processusflux donn� en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(processusflux)!=NULL, RESULTAT_ERREUR);

        SECURISE(processusflux_destruction(copie));
        SECURISE(processusflux_initialisation(copie));
        SECURISE(processusflux_definition_preaction(copie,T_S_(CHAMP(processusflux, preaction))));
        SECURISE(processusflux_definition_actioncommande(copie,CHAMP_TRAVAIL(processusflux, action)));
        SECURISE(processusflux_definition_postaction(copie,T_S_(CHAMP(processusflux, postaction))));
        SECURISE(processusflux_definition_contexte(copie,T_S_(CHAMP(processusflux, contexte))));
        return RESULTAT_OK;
}

Resultat processusflux_destruction(TRAVAIL(ProcessusFlux) processusflux)
{
        /* D�truit une structure d'action sur les flux.
        */
        if(S_T(processusflux)==NULL)
                return RESULTAT_OK;
        CHAMP(processusflux, preaction)=NULL;
        SECURISE(actioncommande_destruction(CHAMP_TRAVAIL(processusflux, action)));
        CHAMP(processusflux, postaction)=NULL;
        free(S_T(processusflux));
        S_T(processusflux)=NULL;
        return RESULTAT_OK;
}

