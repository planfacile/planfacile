/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandegeneric.h"

Resultat commandegeneric_initialisation(TRAVAIL(CommandeGeneric) commandegeneric)
{
        /* Cr�e une commande d'id�e g�n�rique vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandegeneric)=NOUVEAU(CommandeGeneric))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandegeneric, localisation)=NULL;
        CHAMP(commandegeneric, indice)=NULL;
        CHAMP(commandegeneric, reference)=NULL;
        CHAMP(commandegeneric, titre)=NULL;
        CHAMP(commandegeneric, texte)=NULL;
        return RESULTAT_OK;
}

Resultat commandegeneric_definition_localisationfichier(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande generic.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandegeneric, localisation)));
        return RESULTAT_OK;
}

Resultat commandegeneric_definition_indice(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) indice)
{
        /* Assigne un indice � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        if(S_T(indice)==NULL)
                CHAMP(commandegeneric, indice)=NULL;
        else
        {
                SECURISE(flux_copie(indice,CHAMP_TRAVAIL(commandegeneric, indice)));
        }
        return RESULTAT_OK;
}

Resultat commandegeneric_definition_reference(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) reference)
{
        /* Assigne un nom de r�f�rence � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(reference,CHAMP_TRAVAIL(commandegeneric, reference)));
        return RESULTAT_OK;
}

Resultat commandegeneric_definition_titre(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) titre)
{
        /* Assigne un titre � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(titre,CHAMP_TRAVAIL(commandegeneric, titre)));
        return RESULTAT_OK;
}

Resultat commandegeneric_definition_texte(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) texte)
{
        /* Assigne un texte � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(texte,CHAMP_TRAVAIL(commandegeneric, texte)));
        return RESULTAT_OK;
}

Resultat commandegeneric_lecture_localisationfichier(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande generic.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandegeneric, localisation);
        return RESULTAT_OK;
}

Resultat commandegeneric_lecture_indice(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) indice)
{
        /* Lit un indice � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        T_R(indice)=CHAMP_TRAVAIL(commandegeneric, indice);
        return RESULTAT_OK;
}

Resultat commandegeneric_lecture_reference(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) reference)
{
        /* Lit un nom de r�f�rence � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        T_R(reference)=CHAMP_TRAVAIL(commandegeneric, reference);
        return RESULTAT_OK;
}

Resultat commandegeneric_lecture_titre(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) titre)
{
        /* Lit un titre � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        T_R(titre)=CHAMP_TRAVAIL(commandegeneric, titre);
        return RESULTAT_OK;
}

Resultat commandegeneric_lecture_texte(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) texte)
{
        /* Lit un texte � une id�e g�n�rique.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        T_R(texte)=CHAMP_TRAVAIL(commandegeneric, texte);
        return RESULTAT_OK;
}

Resultat commandegeneric_parcours(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandegeneric, indice)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandegeneric, indice),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandegeneric, reference),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandegeneric, titre),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandegeneric, texte),action,general));
        return RESULTAT_OK;
}

Resultat commandegeneric_copie(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(CommandeGeneric) copie)
{
        /* Cr�e une copie de la commande generic.
         * Renvoie RESULTAT_ERREUR si commandegeneric est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandegeneric_destruction(copie));
        SECURISE(commandegeneric_initialisation(copie));
        SECURISE(commandegeneric_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandegeneric, localisation)));
        SECURISE(commandegeneric_definition_indice(copie,CHAMP_TRAVAIL(commandegeneric, indice)));
        SECURISE(commandegeneric_definition_reference(copie,CHAMP_TRAVAIL(commandegeneric, reference)));
        SECURISE(commandegeneric_definition_titre(copie,CHAMP_TRAVAIL(commandegeneric, titre)));
        SECURISE(commandegeneric_definition_texte(copie,CHAMP_TRAVAIL(commandegeneric, texte)));
        return RESULTAT_OK;
}

Resultat commandegeneric_destruction(TRAVAIL(CommandeGeneric) commandegeneric)
{
        /* D�truit une id�e g�n�rique.
         */ 
        if(S_T(commandegeneric)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandegeneric, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandegeneric, indice)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandegeneric, reference)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandegeneric, titre)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandegeneric, texte)));
        free(S_T(commandegeneric));
        S_T(commandegeneric)=NULL;
        return RESULTAT_OK;
}

