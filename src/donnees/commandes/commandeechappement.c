/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeechappement.h"

Resultat commandeechappement_initialisation(TRAVAIL(CommandeEchappement) commandeechappement)
{
        /* Cr�e une commande d'�chappement de caract�re vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeechappement)=NOUVEAU(CommandeEchappement))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeechappement, localisation)=NULL;
        CHAMP(commandeechappement, caractere)='\0';
        return RESULTAT_OK;
}

Resultat commandeechappement_definition_localisationfichier(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande d'echappement de caract�re.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeechappement, localisation)));
        return RESULTAT_OK;
}

Resultat commandeechappement_definition_caractere(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL_SCALAIRE(Caractere) caractere)
{
        /* Assigne un caract�re d'�chappement.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandeechappement, caractere)=S_T_(caractere);
        return RESULTAT_OK;
}

Resultat commandeechappement_lecture_localisationfichier(TRAVAIL(CommandeEchappement) commandeechappement, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande d'echappement de caract�re.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeechappement, localisation);
        return RESULTAT_OK;
}

Resultat commandeechappement_lecture_caractere(TRAVAIL(CommandeEchappement) commandeechappement, REFERENCE_SCALAIRE(Caractere) caractere)
{
        /* Lit un caract�re d'�chappement.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        T_R_(caractere)=CHAMP(commandeechappement, caractere);
        return RESULTAT_OK;
}

Resultat commandeechappement_remplacement_texte(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL_SCALAIRE(Booleen) danstexte, REFERENCE_SCALAIRE(Chaine) texte)
{
        /* Renvoie le texte �quivalent d'une commande d'echappement.
         * Le bool�en est utilis� pour savoir si la commande d'echappement
         * de caract�res est situ�e ou non au milieu d'un texte.
         * La chaine renvoy�e devra �tre d�sallou�e � la main.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL,
         * et RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        S_R_(texte)=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(2)));
        ASSERTION(S_R_(texte)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        if((CHAMP(commandeechappement, caractere)=='\n')&&(danstexte==T_S_(VRAI)))
        {
                ELEMENT_(T_R_(texte), 0)='\0';
        }
        else
        {
                ELEMENT_(T_R_(texte), 0)=CHAMP(commandeechappement, caractere);
                ELEMENT_(T_R_(texte), 1)='\0';
        }
        return RESULTAT_OK;
}

Resultat commandeechappement_parcours(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}
Resultat commandeechappement_copie(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(CommandeEchappement) copie)
{
        /* Cr�e une copie de la commande d'�chappement de caract�re.
         * Renvoie RESULTAT_ERREUR si commandeechappement est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeechappement_destruction(copie));
        SECURISE(commandeechappement_initialisation(copie));
        SECURISE(commandeechappement_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeechappement, localisation)));
        SECURISE(commandeechappement_definition_caractere(copie,T_S_(CHAMP(commandeechappement, caractere))));
        return RESULTAT_OK;
}

Resultat commandeechappement_destruction(TRAVAIL(CommandeEchappement) commandeechappement)
{
        /* D�truit une commande d'�chappement de caract�re.
         */
        if(S_T(commandeechappement)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeechappement, localisation)));
        free(S_T(commandeechappement));
        S_T(commandeechappement)=NULL;
        return RESULTAT_OK;
}
