/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeextref.h"

Resultat commandeextrefs_initialisation(TRAVAIL(CommandeExtRefs) commandeextrefs)
{
        /* Cr�e une commande extrefs vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeextrefs)=NOUVEAU(CommandeExtRefs))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeextrefs, localisation)=NULL;
        CHAMP(commandeextrefs, format)=NULL;
        return RESULTAT_OK;
}

Resultat commandeextrefs_definition_localisationfichier(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande extrefs dans le code source.
         * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeextrefs, localisation)));
        return RESULTAT_OK;
}

Resultat commandeextrefs_definition_format(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(Flux) format)
{
        /* Assigne un format � la commande extrefs.
         * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        if(S_T(format)==NULL)
                CHAMP(commandeextrefs, format)=NULL;
        else
        {
                SECURISE(flux_copie(format,CHAMP_TRAVAIL(commandeextrefs, format)));
        }
        return RESULTAT_OK;
}

Resultat commandeextrefs_lecture_localisationfichier(TRAVAIL(CommandeExtRefs) commandeextrefs, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande extrefs dans le code source.
         * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeextrefs, localisation);
        return RESULTAT_OK;
}

Resultat commandeextrefs_lecture_format(TRAVAIL(CommandeExtRefs) commandeextrefs, REFERENCE(Flux) format)
{
        /* Lit un format � la commande extrefs.
         * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        T_R(format)=CHAMP_TRAVAIL(commandeextrefs, format);
        return RESULTAT_OK;
}

Resultat commandeextrefs_parcours(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandeextrefs, format)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeextrefs, format),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandeextrefs_copie(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(CommandeExtRefs) copie)
{
        /* Cr�e une copie de la commande extrefs.
         * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeextrefs_destruction(copie));
        SECURISE(commandeextrefs_initialisation(copie));
        SECURISE(commandeextrefs_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeextrefs, localisation)));
        if(CHAMP(commandeextrefs, format)!=NULL)
                SECURISE(commandeextrefs_definition_format(copie,CHAMP_TRAVAIL(commandeextrefs, format)));
        return RESULTAT_OK;
}

Resultat commandeextrefs_destruction(TRAVAIL(CommandeExtRefs) commandeextrefs)
{
        /* D�truit une commande extrefs.
         */
        if(S_T(commandeextrefs)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeextrefs, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeextrefs, format)));
        free(S_T(commandeextrefs));
        S_T(commandeextrefs)=NULL;
        return RESULTAT_OK;
}
