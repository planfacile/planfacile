/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDETITLE__
#define __COMMANDETITLE__

#include <src/global/global.h>

typedef struct commandetitle CONTENEUR(CommandeTitle);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandetitle
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande de remplacement par titre
        //dans le source.
};
/* Commande d�sign�e pour indiquer o� remplacer par un titre
 * dans un format de r�f�rence.
 */

Resultat commandetitle_initialisation(TRAVAIL(CommandeTitle) commandetitle);
/* Cr�e une commande title vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandetitle_definition_localisationfichier(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande title.
 * Renvoie RESULTAT_ERREUR si commandetitle est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandetitle_lecture_localisationfichier(TRAVAIL(CommandeTitle) commandetitle, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande title.
 * Renvoie RESULTAT_ERREUR si commandetitle est NULL.
 */

Resultat commandetitle_parcours(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandetitle_copie(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(CommandeTitle) copie);
/* Cr�e une copie de la commande title.
 * Renvoie RESULTAT_ERREUR si commandetitle est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandetitle_destruction(TRAVAIL(CommandeTitle) commandetitle);
/* D�truit une commande title.
 */

#endif
