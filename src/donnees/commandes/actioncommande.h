/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ACTIONCOMMANDE__
#define __ACTIONCOMMANDE__

#include <src/global/global.h>

typedef struct actioncommande CONTENEUR(ActionCommande);

typedef enum
{
        PARCOURS_MANUEL,
        //Pr�cise que le parcours se fait
        //manuellement.
        PARCOURS_AUTOMATIQUE_AVANT,
        //Pr�cise que le parcours se fait
        //avant de consid�rer la commande.
        PARCOURS_AUTOMATIQUE_APRES
        //Pr�cise que le parcours se fait
        //apr�s avoir consid�r� la commande,
        //si elle n'a pas �t� remplac�e par
        //un flux.
} CONTENEUR_SCALAIRE(TypeParcours);
/* Sert � indiquer comment effectuer le
 * parcours des sous flux d'une commande,
 * par rapport � l'�x�cution d'une action
 * de commande.
 */

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>

#include <src/donnees/commandes/commande.h>
#include <src/donnees/commandes/commandecommentaire.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandedep.h>
#include <src/donnees/commandes/commandedepref.h>
#include <src/donnees/commandes/commandeechappement.h>
#include <src/donnees/commandes/commandeerror.h>
#include <src/donnees/commandes/commandeextref.h>
#include <src/donnees/commandes/commandeextrefs.h>
#include <src/donnees/commandes/commandefoot.h>
#include <src/donnees/commandes/commandegeneric.h>
#include <src/donnees/commandes/commandehead.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/commandeinclude.h>
#include <src/donnees/commandes/commandeindex.h>
#include <src/donnees/commandes/commandemacro.h>
#include <src/donnees/commandes/commandemesg.h>
#include <src/donnees/commandes/commandemessage.h>
#include <src/donnees/commandes/commandemissing.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/donnees/commandes/commandeoptions.h>
#include <src/donnees/commandes/commandeparametre.h>
#include <src/donnees/commandes/commandereference.h>
#include <src/donnees/commandes/commanderef.h>
#include <src/donnees/commandes/commandesec.h>
#include <src/donnees/commandes/commandesection.h>
#include <src/donnees/commandes/commandestandard.h>
#include <src/donnees/commandes/commandestart.h>
#include <src/donnees/commandes/commandetexte.h>
#include <src/donnees/commandes/commandetitle.h>
#include <src/donnees/commandes/commandetxt.h>
#include <src/donnees/commandes/commandewarning.h>

struct actioncommande
{
        struct
        {
                Resultat (FONCTION(commentaire))(TRAVAIL(CommandeCommentaire) commentaire, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } commentaire;
        struct
        {
                Resultat (FONCTION(define))(TRAVAIL(CommandeDefine) define, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } define;
        struct
        {
                Resultat (FONCTION(dep))(TRAVAIL(CommandeDep) dep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } dep;
        struct
        {
                Resultat (FONCTION(depref))(TRAVAIL(CommandeDepRef) depref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } depref;
        struct
        {
                Resultat (FONCTION(echappement))(TRAVAIL(CommandeEchappement) echappement, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } echappement;
        struct
        {
                Resultat (FONCTION(error))(TRAVAIL(CommandeError) error, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } error;
        struct
        {
                Resultat (FONCTION(extref))(TRAVAIL(CommandeExtRef) extref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } extref;
        struct
        {
                Resultat (FONCTION(extrefs))(TRAVAIL(CommandeExtRefs) extrefs, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } extrefs;
        struct
        {
                Resultat (FONCTION(foot))(TRAVAIL(CommandeFoot) foot, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } foot;
        struct
        {
                Resultat (FONCTION(generic))(TRAVAIL(CommandeGeneric) generic, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } generic;
        struct
        {
                Resultat (FONCTION(head))(TRAVAIL(CommandeHead) head, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } head;
        struct
        {
                Resultat (FONCTION(idea))(TRAVAIL(CommandeIdea) idea, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } idea;
        struct
        {
                Resultat (FONCTION(include))(TRAVAIL(CommandeInclude) include, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } include;
        struct
        {
                Resultat (FONCTION(index))(TRAVAIL(CommandeIndex) index, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } index;
        struct
        {
                Resultat (FONCTION(macro))(TRAVAIL(CommandeMacro) macro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } macro;
        struct
        {
                Resultat (FONCTION(mesg))(TRAVAIL(CommandeMesg) mesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } mesg;
        struct
        {
                Resultat (FONCTION(message))(TRAVAIL(CommandeMessage) message, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } message;
        struct
        {
                Resultat (FONCTION(missing))(TRAVAIL(CommandeMissing) missing, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } missing;
        struct
        {
                Resultat (FONCTION(option))(TRAVAIL(CommandeOption) option, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } option;
        struct
        {
                Resultat (FONCTION(options))(TRAVAIL(CommandeOptions) options, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } options;
        struct
        {
                Resultat (FONCTION(parametre))(TRAVAIL(CommandeParametre) parametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } parametre;
        struct
        {
                Resultat (FONCTION(reference))(TRAVAIL(CommandeReference) reference, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } reference;
        struct
        {
                Resultat (FONCTION(ref))(TRAVAIL(CommandeRef) ref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } ref;
        struct
        {
                Resultat (FONCTION(sec))(TRAVAIL(CommandeSec) sec, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } sec;
        struct
        {
                Resultat (FONCTION(section))(TRAVAIL(CommandeSection) section, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } section;
        struct
        {
                Resultat (FONCTION(standard))(TRAVAIL(CommandeStandard) standard, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } standard;
        struct
        {
                Resultat (FONCTION(start))(TRAVAIL(CommandeStart) start, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } start;
        struct
        {
                Resultat (FONCTION(texte))(TRAVAIL(CommandeTexte) texte, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } texte;
        struct
        {
                Resultat (FONCTION(title))(TRAVAIL(CommandeTitle) title, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } title;
        struct
        {
                Resultat (FONCTION(txt))(TRAVAIL(CommandeTxt) txt, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } txt;
        struct
        {
                Resultat (FONCTION(warning))(TRAVAIL(CommandeWarning) warning, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
                STOCKAGE_SCALAIRE(TypeParcours) type;
        } warning;
};
/* Repr�sente une structure permettant de r�aliser une action pr�cise sur une commande
 * selon le type de la commande rencontr�e.
 * Ces fonctions peuvent renvoyer un flux �gal � NULL si elles travaillent directement
 * sur le contenu de la commande. Cependant, si le flux renvoy� est une partie de la
 * commande d'entr�e, veillez � bien faire une copie de ce flux, car la commande sera
 * lib�r�e par une fonction appellante. Pour supprimer une commande d'un flux, il suffira
 * de renvoyer un flux vide, � savoir ne contenant aucune commande...
 * Le bool�en sert � indiquer un arr�t du parcours des contextes, en �tant positionn� �
 * VRAI.
 * Si la fonction vaut NULL, aucune action n'est effectu�e.
 */

Resultat actioncommande_initialisation(TRAVAIL(ActionCommande) actioncommande);
/* Cr�e et initialise une structure d'action sur une commande sans actions associ�es
 * aux diff�rents types de commande.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 * Les PARCOURS_AUTOMATIQUE_AVANT sont s�lectionn�s par d�faut.
 */

Resultat actioncommande_definition_commentaire        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(commentaire))(TRAVAIL(CommandeCommentaire), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_define        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(define))(TRAVAIL(CommandeDefine), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_dep                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(dep))(TRAVAIL(CommandeDep), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_depref        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(depref))(TRAVAIL(CommandeDepRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_echappement        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(echappement))(TRAVAIL(CommandeEchappement), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_error        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(error))(TRAVAIL(CommandeError), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_extref        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(extref))(TRAVAIL(CommandeExtRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_extrefs        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(extrefs))(TRAVAIL(CommandeExtRefs), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_foot                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(foot))(TRAVAIL(CommandeFoot), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_generic        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(generic))(TRAVAIL(CommandeGeneric), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_head                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(head))(TRAVAIL(CommandeHead), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_idea                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(idea))(TRAVAIL(CommandeIdea), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_include        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(include))(TRAVAIL(CommandeInclude), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_index        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(index))(TRAVAIL(CommandeIndex), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_macro        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(macro))(TRAVAIL(CommandeMacro), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_mesg                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(mesg))(TRAVAIL(CommandeMesg), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_message        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(message))(TRAVAIL(CommandeMessage), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_missing        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(missing))(TRAVAIL(CommandeMissing), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_option        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(option))(TRAVAIL(CommandeOption), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_options        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(options))(TRAVAIL(CommandeOptions), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_parametre        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(parametre))(TRAVAIL(CommandeParametre), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_reference        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(reference))(TRAVAIL(CommandeReference), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_ref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(ref))(TRAVAIL(CommandeRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_sec                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(sec))(TRAVAIL(CommandeSec), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_section        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(section))(TRAVAIL(CommandeSection), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_standard        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(standard))(TRAVAIL(CommandeStandard), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_start        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(start))(TRAVAIL(CommandeStart), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_texte        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(texte))(TRAVAIL(CommandeTexte), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_title        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(title))(TRAVAIL(CommandeTitle), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_txt                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(txt))(TRAVAIL(CommandeTxt), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
Resultat actioncommande_definition_warning        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(warning))(TRAVAIL(CommandeWarning), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type);
/* Toutes ces fonctions permettent de pr�ciser quelle est l'action associ�e � un type de commande particulier.
 * Renvoie RESULTAT_ERREUR si actioncommande est NULL.
 */

Resultat actioncommande_lecture_commentaire        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(commentaire))(TRAVAIL(CommandeCommentaire), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_define                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(define))(TRAVAIL(CommandeDefine), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_dep                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(dep))(TRAVAIL(CommandeDep), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_depref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(depref))(TRAVAIL(CommandeDepRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_echappement        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(echappement))(TRAVAIL(CommandeEchappement), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_error                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(error))(TRAVAIL(CommandeError), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_extref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(extref))(TRAVAIL(CommandeExtRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_extrefs                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(extrefs))(TRAVAIL(CommandeExtRefs), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_foot                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(foot))(TRAVAIL(CommandeFoot), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_generic                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(generic))(TRAVAIL(CommandeGeneric), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_head                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(head))(TRAVAIL(CommandeHead), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_idea                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(idea))(TRAVAIL(CommandeIdea), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_include                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(include))(TRAVAIL(CommandeInclude), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_index                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(index))(TRAVAIL(CommandeIndex), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_macro                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(macro))(TRAVAIL(CommandeMacro), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_mesg                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(mesg))(TRAVAIL(CommandeMesg), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_message                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(message))(TRAVAIL(CommandeMessage), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_missing                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(missing))(TRAVAIL(CommandeMissing), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_option                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(option))(TRAVAIL(CommandeOption), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_options                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(options))(TRAVAIL(CommandeOptions), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_parametre        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(parametre))(TRAVAIL(CommandeParametre), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_reference        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(reference))(TRAVAIL(CommandeReference), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_ref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(ref))(TRAVAIL(CommandeRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_sec                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(sec))(TRAVAIL(CommandeSec), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_section                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(section))(TRAVAIL(CommandeSection), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_standard        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(standard))(TRAVAIL(CommandeStandard), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_start                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(start))(TRAVAIL(CommandeStart), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_texte                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(texte))(TRAVAIL(CommandeTexte), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_title                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(title))(TRAVAIL(CommandeTitle), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_txt                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(txt))(TRAVAIL(CommandeTxt), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
Resultat actioncommande_lecture_warning                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(warning))(TRAVAIL(CommandeWarning), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type);
/* Lit un �l�ment dans la structure actioncommande. 
 * Renvoie RESULTAT_ERREUR si actioncommande est NULL.
 * Seuls les champs non NULL sont renseign�s.
 */

Resultat actioncommande_execution_commande(TRAVAIL(ActionCommande) actioncommande, TRAVAIL(Commande) commande, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte);
/* R�alise une action sur la commande pass�e en param�tre.
 * Les r�sultats sont plac�s dans le flux et le bool�en.
 * Renvoie RESULTAT_ERREUR si actioncommande ou commande
 * sont NULL.
 */

Resultat actioncommande_copie(TRAVAIL(ActionCommande) actioncommande, TRAVAIL(ActionCommande) copie);
/* Cr�e une copie de l'actioncommande donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat actioncommande_destruction(TRAVAIL(ActionCommande) actioncommande);
/* D�truit une structure d'action de commande.
 */
#endif
