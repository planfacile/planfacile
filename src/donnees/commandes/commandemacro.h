/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEMACRO__
#define __COMMANDEMACRO__

#include <src/global/global.h>

typedef struct commandemacro CONTENEUR(CommandeMacro);

typedef struct commandemacroparametres CONTENEUR(CommandeMacroParametres);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

struct commandemacroparametres
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille r�serv�e en m�moire.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�ellement utilis�e.
        //Cette valeur donne le nombre de param�tres
        //pass�s � la macro lors de l'appel.
        TABLEAU(STOCKAGE(Flux)) parametre;
        //Param�tres de la macro.
        //Ces flux seront de type FLUX_MACRO_PARAMETRE.
};
/* Cette structure sert � contenir un nombre arbitraire
 * de param�tres de la macro.
 */

Resultat commandemacroparametres_initialisation(TRAVAIL(CommandeMacroParametres) commandemacroparametres);
/* Cr�e une structure destin�e � recevoir les diff�rents
 * param�tres d'un appel de macro.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation.
 */

Resultat commandemacroparametres_ajout_parametre(TRAVAIL(CommandeMacroParametres) commandemacroparametres, TRAVAIL(Flux) parametre);
/* Ajoute un parametre � la liste des param�tres.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �ventuelle �choue.
 * Renvoie RESULTAT_ERREUR si la liste commandemacroparametres est NULL.
 */

Resultat commandemacroparametres_destruction(TRAVAIL(CommandeMacroParametres) commandemacroparametres);
/* D�truit le contenu d'une liste de param�tres.
 */

struct commandemacro
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'appel de macro dans le source.
        STOCKAGE_SCALAIRE(NomMacro) nom;
        //Nom de la macro appell�e.
        STOCKAGE(CommandeMacroParametres) parametres;
        //Param�tres de la macro.
};
/* Cette commande est destin�e � recevoir les diff�rents �l�ments permettant
 * de r�aliser un appel de macro. Contrairement � la commande #define, cette commande
 * sera explicitement pr�sente dans le flux de commandes.
 */

Resultat commandemacro_initialisation(TRAVAIL(CommandeMacro) commandemacro);
/* Cr�e une commande d'appel de macro vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_definition_localisationfichier(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande d'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_definition_nom(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(NomMacro) nom);
/* Assigne un nom � l'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_ajout_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(Flux) parametre);
/* Ajoute un param�tre � l'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_definition_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) parametre);
/* Assigne un param�tre � un appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat commandemacro_definition_parametres(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(CommandeMacroParametres) commandemacroparametres);
/* Assigne une liste de param�tres � un appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_lecture_localisationfichier(TRAVAIL(CommandeMacro) commandemacro, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande d'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 */

Resultat commandemacro_lecture_nom(TRAVAIL(CommandeMacro) commandemacro, REFERENCE_SCALAIRE(NomMacro) nom);
/* Lit un nom � l'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 */

Resultat commandemacro_lecture_tailleparametre(TRAVAIL(CommandeMacro) commandemacro, REFERENCE_SCALAIRE(Taille) tailleparametre);
/* Lit le nombre de param�tres de l'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 */

Resultat commandemacro_lecture_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(Flux) parametre);
/* Lit un param�tre � l'appel de macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
 */

Resultat commandemacro_decalage_parametres(TRAVAIL(CommandeMacro) commandemacro);
/* D�cale les param�tres d'un indice. Le param�tre 0 ajout�
 * sera NULL.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemacro_parcours(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandemacro_copie(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(CommandeMacro) copie);
/* Cr�e une copie de la commande macro.
 * Renvoie RESULTAT_ERREUR si commandemacro est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandemacro_destruction(TRAVAIL(CommandeMacro) commandemacro);
/* D�truit une commande d'appel de macro.
 */

#endif
