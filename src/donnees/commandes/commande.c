/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commande.h"

Resultat commande_initialisation(TRAVAIL(Commande) commande)
{
/* Cr�e une commande vide. (De type COMMANDE_VIDE.)
 * Retourne RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 */
        ASSERTION((S_T(commande)=NOUVEAU(Commande))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commande, type)=COMMANDE_VIDE;
        CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire)=NULL;
        return RESULTAT_OK;
}

Resultat commande_definition_commentaire        (TRAVAIL(Commande) commande, TRAVAIL(CommandeCommentaire) commentaire)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_COMMENTAIRE;
        SECURISE(commandecommentaire_copie(commentaire,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire))));
        return RESULTAT_OK;
}

Resultat commande_definition_define             (TRAVAIL(Commande) commande, TRAVAIL(CommandeDefine) define)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_DEFINE;
        SECURISE(commandedefine_copie(define,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), define))));
        return RESULTAT_OK;
}

Resultat commande_definition_dep                (TRAVAIL(Commande) commande, TRAVAIL(CommandeDep) dep)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_DEP;
        SECURISE(commandedep_copie(dep,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), dep))));
        return RESULTAT_OK;
}

Resultat commande_definition_depref             (TRAVAIL(Commande) commande, TRAVAIL(CommandeDepRef) depref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_DEPREF;
        SECURISE(commandedepref_copie(depref,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), depref))));
        return RESULTAT_OK;
}

Resultat commande_definition_echappement        (TRAVAIL(Commande) commande, TRAVAIL(CommandeEchappement) echappement)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_ECHAPPEMENT;
        SECURISE(commandeechappement_copie(echappement,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), echappement))));
        return RESULTAT_OK;
}

Resultat commande_definition_error              (TRAVAIL(Commande) commande, TRAVAIL(CommandeError) error)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_ERROR;
        SECURISE(commandeerror_copie(error,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), error))));
        return RESULTAT_OK;
}

Resultat commande_definition_extref             (TRAVAIL(Commande) commande, TRAVAIL(CommandeExtRef) extref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_EXTREF;
        SECURISE(commandeextref_copie(extref,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extref))));
        return RESULTAT_OK;
}

Resultat commande_definition_extrefs            (TRAVAIL(Commande) commande, TRAVAIL(CommandeExtRefs) extrefs)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_EXTREFS;
        SECURISE(commandeextrefs_copie(extrefs,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extrefs))));
        return RESULTAT_OK;
}

Resultat commande_definition_foot               (TRAVAIL(Commande) commande, TRAVAIL(CommandeFoot) foot)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_FOOT;
        SECURISE(commandefoot_copie(foot,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), foot))));
        return RESULTAT_OK;
}

Resultat commande_definition_generic            (TRAVAIL(Commande) commande, TRAVAIL(CommandeGeneric) generic)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_GENERIC;
        SECURISE(commandegeneric_copie(generic,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), generic))));
        return RESULTAT_OK;
}

Resultat commande_definition_head               (TRAVAIL(Commande) commande, TRAVAIL(CommandeHead) head)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_HEAD;
        SECURISE(commandehead_copie(head,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), head))));
        return RESULTAT_OK;
}

Resultat commande_definition_idea               (TRAVAIL(Commande) commande, TRAVAIL(CommandeIdea) idea)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_IDEA;
        SECURISE(commandeidea_copie(idea,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), idea))));
        return RESULTAT_OK;
}

Resultat commande_definition_include            (TRAVAIL(Commande) commande, TRAVAIL(CommandeInclude) include)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_INCLUDE;
        SECURISE(commandeinclude_copie(include,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), include))));
        return RESULTAT_OK;
}

Resultat commande_definition_index              (TRAVAIL(Commande) commande, TRAVAIL(CommandeIndex) index)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_INDEX;
        SECURISE(commandeindex_copie(index,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), index))));
        return RESULTAT_OK;
}

Resultat commande_definition_macro              (TRAVAIL(Commande) commande, TRAVAIL(CommandeMacro) macro)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_MACRO;
        SECURISE(commandemacro_copie(macro,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), macro))));
        return RESULTAT_OK;
}

Resultat commande_definition_mesg               (TRAVAIL(Commande) commande, TRAVAIL(CommandeMesg) mesg)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_MESG;
        SECURISE(commandemesg_copie(mesg,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), mesg))));
        return RESULTAT_OK;
}

Resultat commande_definition_message            (TRAVAIL(Commande) commande, TRAVAIL(CommandeMessage) message)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_MESSAGE;
        SECURISE(commandemessage_copie(message,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), message))));
        return RESULTAT_OK;
}

Resultat commande_definition_missing            (TRAVAIL(Commande) commande, TRAVAIL(CommandeMissing) missing)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_MISSING;
        SECURISE(commandemissing_copie(missing,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), missing))));
        return RESULTAT_OK;
}

Resultat commande_definition_option             (TRAVAIL(Commande) commande, TRAVAIL(CommandeOption) option)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_OPTION;
        SECURISE(commandeoption_copie(option,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), option))));
        return RESULTAT_OK;
}

Resultat commande_definition_options            (TRAVAIL(Commande) commande, TRAVAIL(CommandeOptions) options)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_OPTIONS;
        SECURISE(commandeoptions_copie(options,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), options))));
        return RESULTAT_OK;
}

Resultat commande_definition_parametre          (TRAVAIL(Commande) commande, TRAVAIL(CommandeParametre) parametre)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_PARAMETRE;
        SECURISE(commandeparametre_copie(parametre,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), parametre))));
        return RESULTAT_OK;
}

Resultat commande_definition_reference          (TRAVAIL(Commande) commande, TRAVAIL(CommandeReference) reference)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_REFERENCE;
        SECURISE(commandereference_copie(reference,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), reference))));
        return RESULTAT_OK;
}

Resultat commande_definition_ref                (TRAVAIL(Commande) commande, TRAVAIL(CommandeRef) ref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_REF;
        SECURISE(commanderef_copie(ref,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), ref))));
        return RESULTAT_OK;
}

Resultat commande_definition_sec                (TRAVAIL(Commande) commande, TRAVAIL(CommandeSec) sec)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_SEC;
        SECURISE(commandesec_copie(sec,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), sec))));
        return RESULTAT_OK;
}

Resultat commande_definition_section            (TRAVAIL(Commande) commande, TRAVAIL(CommandeSection) section)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_SECTION;
        SECURISE(commandesection_copie(section,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), section))));
        return RESULTAT_OK;
}

Resultat commande_definition_standard           (TRAVAIL(Commande) commande, TRAVAIL(CommandeStandard) standard)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_STANDARD;
        SECURISE(commandestandard_copie(standard,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), standard))));
        return RESULTAT_OK;
}

Resultat commande_definition_start              (TRAVAIL(Commande) commande, TRAVAIL(CommandeStart) start)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_START;
        SECURISE(commandestart_copie(start,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), start))));
        return RESULTAT_OK;
}

Resultat commande_definition_texte              (TRAVAIL(Commande) commande, TRAVAIL(CommandeTexte) texte)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_TEXTE;
        SECURISE(commandetexte_copie(texte,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), texte))));
        return RESULTAT_OK;
}

Resultat commande_definition_title              (TRAVAIL(Commande) commande, TRAVAIL(CommandeTitle) title)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_TITLE;
        SECURISE(commandetitle_copie(title,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), title))));
        return RESULTAT_OK;
}

Resultat commande_definition_txt                (TRAVAIL(Commande) commande, TRAVAIL(CommandeTxt) txt)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_TXT;
        SECURISE(commandetxt_copie(txt,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), txt))));
        return RESULTAT_OK;
}

Resultat commande_definition_warning            (TRAVAIL(Commande) commande, TRAVAIL(CommandeWarning) warning)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        CHAMP(commande, type)=COMMANDE_WARNING;
        SECURISE(commandewarning_copie(warning,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), warning))));
        return RESULTAT_OK;
}

/* Ces fonctions servent � encapsuler une commande pr�cise dans une structure de commande g�n�rique.
 * Si l'allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�e, et si commande est NULL, RESULTAT_ERREUR est renvoy�e.
 */

Resultat commande_lecture_type(TRAVAIL(Commande) commande, REFERENCE_SCALAIRE(TypeCommande) type)
{
        /* Lit le type d'une commande.
         * Renvoie RESULTAT_ERREUR si commande est NULL.
         */
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        T_R_(type)=T_S_(CHAMP(commande, type));
        return RESULTAT_OK;
}

Resultat commande_lecture_commentaire   (TRAVAIL(Commande) commande, REFERENCE(CommandeCommentaire) commentaire)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_COMMENTAIRE), RESULTAT_ERREUR_DOMAINE);

        T_R(commentaire)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire));
        return RESULTAT_OK;
}

Resultat commande_lecture_define        (TRAVAIL(Commande) commande, REFERENCE(CommandeDefine) define)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_DEFINE), RESULTAT_ERREUR_DOMAINE);

        T_R(define)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), define));
        return RESULTAT_OK;
}

Resultat commande_lecture_dep           (TRAVAIL(Commande) commande, REFERENCE(CommandeDep) dep)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_DEP), RESULTAT_ERREUR_DOMAINE);

        T_R(dep)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), dep));
        return RESULTAT_OK;
}

Resultat commande_lecture_depref        (TRAVAIL(Commande) commande, REFERENCE(CommandeDepRef) depref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_DEPREF), RESULTAT_ERREUR_DOMAINE);

        T_R(depref)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), depref));
        return RESULTAT_OK;
}

Resultat commande_lecture_echappement   (TRAVAIL(Commande) commande, REFERENCE(CommandeEchappement) echappement)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_ECHAPPEMENT), RESULTAT_ERREUR_DOMAINE);

        T_R(echappement)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), echappement));
        return RESULTAT_OK;
}

Resultat commande_lecture_error         (TRAVAIL(Commande) commande, REFERENCE(CommandeError) error)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_ERROR), RESULTAT_ERREUR_DOMAINE);

        T_R(error)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), error));
        return RESULTAT_OK;
}

Resultat commande_lecture_extref        (TRAVAIL(Commande) commande, REFERENCE(CommandeExtRef) extref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_EXTREF), RESULTAT_ERREUR_DOMAINE);

        T_R(extref)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extref));
        return RESULTAT_OK;
}

Resultat commande_lecture_extrefs       (TRAVAIL(Commande) commande, REFERENCE(CommandeExtRefs) extrefs)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_EXTREFS), RESULTAT_ERREUR_DOMAINE);

        T_R(extrefs)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extrefs));
        return RESULTAT_OK;
}

Resultat commande_lecture_foot          (TRAVAIL(Commande) commande, REFERENCE(CommandeFoot) foot)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_FOOT), RESULTAT_ERREUR_DOMAINE);

        T_R(foot)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), foot));
        return RESULTAT_OK;
}

Resultat commande_lecture_generic       (TRAVAIL(Commande) commande, REFERENCE(CommandeGeneric) generic)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_GENERIC), RESULTAT_ERREUR_DOMAINE);

        T_R(generic)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), generic));
        return RESULTAT_OK;
}

Resultat commande_lecture_head          (TRAVAIL(Commande) commande, REFERENCE(CommandeHead) head)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_HEAD), RESULTAT_ERREUR_DOMAINE);

        T_R(head)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), head));
        return RESULTAT_OK;
}

Resultat commande_lecture_idea          (TRAVAIL(Commande) commande, REFERENCE(CommandeIdea) idea)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_IDEA), RESULTAT_ERREUR_DOMAINE);

        T_R(idea)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), idea));
        return RESULTAT_OK;
}

Resultat commande_lecture_include       (TRAVAIL(Commande) commande, REFERENCE(CommandeInclude) include)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_INCLUDE), RESULTAT_ERREUR_DOMAINE);

        T_R(include)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), include));
        return RESULTAT_OK;
}

Resultat commande_lecture_index         (TRAVAIL(Commande) commande, REFERENCE(CommandeIndex) index)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_INDEX), RESULTAT_ERREUR_DOMAINE);

        T_R(index)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), index));
        return RESULTAT_OK;
}

Resultat commande_lecture_macro         (TRAVAIL(Commande) commande, REFERENCE(CommandeMacro) macro)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_MACRO), RESULTAT_ERREUR_DOMAINE);

        T_R(macro)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), macro));
        return RESULTAT_OK;
}

Resultat commande_lecture_mesg          (TRAVAIL(Commande) commande, REFERENCE(CommandeMesg) mesg)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_MESG), RESULTAT_ERREUR_DOMAINE);

        T_R(mesg)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), mesg));
        return RESULTAT_OK;
}

Resultat commande_lecture_message       (TRAVAIL(Commande) commande, REFERENCE(CommandeMessage) message)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_MESSAGE), RESULTAT_ERREUR_DOMAINE);

        T_R(message)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), message));
        return RESULTAT_OK;
}

Resultat commande_lecture_missing       (TRAVAIL(Commande) commande, REFERENCE(CommandeMissing) missing)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_MISSING), RESULTAT_ERREUR_DOMAINE);

        T_R(missing)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), missing));
        return RESULTAT_OK;
}

Resultat commande_lecture_option        (TRAVAIL(Commande) commande, REFERENCE(CommandeOption) option)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_OPTION), RESULTAT_ERREUR_DOMAINE);

        T_R(option)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), option));
        return RESULTAT_OK;
}

Resultat commande_lecture_options       (TRAVAIL(Commande) commande, REFERENCE(CommandeOptions) options)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_OPTIONS), RESULTAT_ERREUR_DOMAINE);

        T_R(options)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), options));
        return RESULTAT_OK;
}

Resultat commande_lecture_parametre     (TRAVAIL(Commande) commande, REFERENCE(CommandeParametre) parametre)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_PARAMETRE), RESULTAT_ERREUR_DOMAINE);

        T_R(parametre)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), parametre));
        return RESULTAT_OK;
}

Resultat commande_lecture_reference     (TRAVAIL(Commande) commande, REFERENCE(CommandeReference) reference)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_REFERENCE), RESULTAT_ERREUR_DOMAINE);

        T_R(reference)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), reference));
        return RESULTAT_OK;
}

Resultat commande_lecture_ref           (TRAVAIL(Commande) commande, REFERENCE(CommandeRef) ref)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_REF), RESULTAT_ERREUR_DOMAINE);

        T_R(ref)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), ref));
        return RESULTAT_OK;
}

Resultat commande_lecture_sec           (TRAVAIL(Commande) commande, REFERENCE(CommandeSec) sec)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_SEC), RESULTAT_ERREUR_DOMAINE);

        T_R(sec)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), sec));
        return RESULTAT_OK;
}

Resultat commande_lecture_section       (TRAVAIL(Commande) commande, REFERENCE(CommandeSection) section)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_SECTION), RESULTAT_ERREUR_DOMAINE);

        T_R(section)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), section));
        return RESULTAT_OK;
}

Resultat commande_lecture_standard      (TRAVAIL(Commande) commande, REFERENCE(CommandeStandard) standard)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_STANDARD), RESULTAT_ERREUR_DOMAINE);

        T_R(standard)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), standard));
        return RESULTAT_OK;
}

Resultat commande_lecture_start         (TRAVAIL(Commande) commande, REFERENCE(CommandeStart) start)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_START), RESULTAT_ERREUR_DOMAINE);

        T_R(start)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), start));
        return RESULTAT_OK;
}

Resultat commande_lecture_texte         (TRAVAIL(Commande) commande, REFERENCE(CommandeTexte) texte)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_TEXTE), RESULTAT_ERREUR_DOMAINE);

        T_R(texte)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), texte));
        return RESULTAT_OK;
}

Resultat commande_lecture_title         (TRAVAIL(Commande) commande, REFERENCE(CommandeTitle) title)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_TITLE), RESULTAT_ERREUR_DOMAINE);

        T_R(title)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), title));
        return RESULTAT_OK;
}

Resultat commande_lecture_txt           (TRAVAIL(Commande) commande, REFERENCE(CommandeTxt) txt)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_TXT), RESULTAT_ERREUR_DOMAINE);

        T_R(txt)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), txt));
        return RESULTAT_OK;
}

Resultat commande_lecture_warning       (TRAVAIL(Commande) commande, REFERENCE(CommandeWarning) warning)
{
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP(commande, type))==T_S_(COMMANDE_WARNING), RESULTAT_ERREUR_DOMAINE);

        T_R(warning)=T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), warning));
        return RESULTAT_OK;
}

/* Ces fonctions permettent de r�cup�rer une commande pr�cise depuis une commande g�n�rique.
 * Renvoie RESULTAT_ERREUR si commande est NULL, et RESULTAT_ERREUR_DOMAINE si le types est incorrect.
 */

Resultat commande_encapsulation_commandeoption(TRAVAIL(Commande) commande, TRAVAIL(CommandeOption) commandeoption)
{
        /* Encapsule une commande #option dans une commande g�n�rique.
         * La commande #option n'est pas recopi�e.
         * Renvoie RESULTAT_ERREUR si commandeoption ou commande est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION(commandeoption!=T_S_(NULL), RESULTAT_ERREUR);

        SECURISE(commande_initialisation(commande));
        CHAMP(commande, type)=COMMANDE_OPTION;
        CHAMP_STOCKAGE_(CHAMP(commande, commande), option)=S_T(commandeoption);
        return RESULTAT_OK;
}

Resultat commande_desencapsulation_commandeoption(TRAVAIL(Commande) commande, COREFERENCE(CommandeOption) commandeoption)
{
        /* D�sencapsule une commande #option depuis une commande g�n�rique.
         * La commande #option n'est pas recopi�e.
         * Renvoie RESULTAT_ERREUR si commandeoption ou commande est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(commandeoption!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(commande, type)==T_S_(COMMANDE_OPTION), RESULTAT_ERREUR_DOMAINE);

        S_C(commandeoption)=CHAMP_STOCKAGE_(CHAMP(commande, commande), option);
        free(S_T(commande));
        return RESULTAT_OK;
}

Resultat commande_encapsulation_commandedefine(TRAVAIL(Commande) commande, TRAVAIL(CommandeDefine) commandedefine)
{
        /* Encapsule une commande #define dans une commande g�n�rique.
         * La commande #define n'est pas recopi�e.
         * Renvoie RESULTAT_ERREUR si commandedefine ou commande est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION(commandedefine!=T_S_(NULL), RESULTAT_ERREUR);

        SECURISE(commande_initialisation(commande));
        CHAMP(commande, type)=COMMANDE_DEFINE;
        CHAMP_STOCKAGE_(CHAMP(commande, commande), define)=S_T(commandedefine);
        return RESULTAT_OK;
}

Resultat commande_desencapsulation_commandedefine(TRAVAIL(Commande) commande, COREFERENCE(CommandeDefine) commandedefine)
{
        /* D�sencapsule une commande #define depuis une commande g�n�rique.
         * La commande #define n'est pas recopi�e.
         * Renvoie RESULTAT_ERREUR si commandedefine ou commande est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         */
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(commandedefine!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(commande, type)==T_S_(COMMANDE_DEFINE), RESULTAT_ERREUR_DOMAINE);

        S_C(commandedefine)=CHAMP_STOCKAGE_(CHAMP(commande, commande), define);
        free(S_T(commande));
        return RESULTAT_OK;
}

Resultat commande_parcours(TRAVAIL(Commande) commande, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL
         * et RESULTAT_ERREUR_DOMAINE si la commande est de type
         * incorrect.
         */
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        if(S_T(action)==NULL)
                return RESULTAT_OK;
        switch(T_S_(CHAMP(commande, type)))
        {
                case T_S_(COMMANDE_COMMENTAIRE):
                        SECURISE(commandecommentaire_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire)),action,general));
                        break;
                case T_S_(COMMANDE_DEFINE):
                        SECURISE(commandedefine_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), define)),action,general));
                        break;
                case T_S_(COMMANDE_DEP):
                        SECURISE(commandedep_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), dep)),action,general));
                        break;
                case T_S_(COMMANDE_DEPREF):
                        SECURISE(commandedepref_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), depref)),action,general));
                        break;
                case T_S_(COMMANDE_ECHAPPEMENT):
                        SECURISE(commandeechappement_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), echappement)),action,general));
                        break;
                case T_S_(COMMANDE_ERROR):
                        SECURISE(commandeerror_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), error)),action,general));
                        break;
                case T_S_(COMMANDE_EXTREF):
                        SECURISE(commandeextref_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extref)),action,general));
                        break;
                case T_S_(COMMANDE_EXTREFS):
                        SECURISE(commandeextrefs_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extrefs)),action,general));
                        break;
                case T_S_(COMMANDE_FOOT):
                        SECURISE(commandefoot_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), foot)),action,general));
                        break;
                case T_S_(COMMANDE_GENERIC):
                        SECURISE(commandegeneric_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), generic)),action,general));
                        break;
                case T_S_(COMMANDE_HEAD):
                        SECURISE(commandehead_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), head)),action,general));
                        break;
                case T_S_(COMMANDE_IDEA):
                        SECURISE(commandeidea_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), idea)),action,general));
                        break;
                case T_S_(COMMANDE_INCLUDE):
                        SECURISE(commandeinclude_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), include)),action,general));
                        break;
                case T_S_(COMMANDE_INDEX):
                        SECURISE(commandeindex_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), index)),action,general));
                        break;
                case T_S_(COMMANDE_MACRO):
                        SECURISE(commandemacro_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), macro)),action,general));
                        break;
                case T_S_(COMMANDE_MESG):
                        SECURISE(commandemesg_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), mesg)),action,general));
                        break;
                case T_S_(COMMANDE_MESSAGE):
                        SECURISE(commandemessage_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), message)),action,general));
                        break;
                case T_S_(COMMANDE_MISSING):
                        SECURISE(commandemissing_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), missing)),action,general));
                        break;
                case T_S_(COMMANDE_OPTION):
                        SECURISE(commandeoption_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), option)),action,general));
                        break;
                case T_S_(COMMANDE_OPTIONS):
                        SECURISE(commandeoptions_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), options)),action,general));
                        break;
                case T_S_(COMMANDE_PARAMETRE):
                        SECURISE(commandeparametre_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), parametre)),action,general));
                        break;
                case T_S_(COMMANDE_REFERENCE):
                        SECURISE(commandereference_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), reference)),action,general));
                        break;
                case T_S_(COMMANDE_REF):
                        SECURISE(commanderef_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), ref)),action,general));
                        break;
                case T_S_(COMMANDE_SEC):
                        SECURISE(commandesec_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), sec)),action,general));
                        break;
                case T_S_(COMMANDE_SECTION):
                        SECURISE(commandesection_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), section)),action,general));
                        break;
                case T_S_(COMMANDE_STANDARD):
                        SECURISE(commandestandard_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), standard)),action,general));
                        break;
                case T_S_(COMMANDE_START):
                        SECURISE(commandestart_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), start)),action,general));
                        break;
                case T_S_(COMMANDE_TEXTE):
                        SECURISE(commandetexte_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), texte)),action,general));
                        break;
                case T_S_(COMMANDE_TITLE):
                        SECURISE(commandetitle_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), title)),action,general));
                        break;
                case T_S_(COMMANDE_TXT):
                        SECURISE(commandetxt_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), txt)),action,general));
                        break;
                case T_S_(COMMANDE_WARNING):
                        SECURISE(commandewarning_parcours(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), warning)),action,general));
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
        }
        return RESULTAT_OK;
}

Resultat commande_copie(TRAVAIL(Commande) commande, TRAVAIL(Commande) copie)
{
        /* Cr�e une copie de la commande donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * et RESULTAT_ERREUR_DOMAINE si le type est incorrect.
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        SECURISE(commande_destruction(copie));
        SECURISE(commande_initialisation(copie));
        switch(T_S_(CHAMP(commande, type)))
        {
                case T_S_(COMMANDE_COMMENTAIRE):
                        SECURISE(commande_definition_commentaire(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire))));
                        break;
                case T_S_(COMMANDE_DEFINE):
                        SECURISE(commande_definition_define(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), define))));
                        break;
                case T_S_(COMMANDE_DEP):
                        SECURISE(commande_definition_dep(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), dep))));
                        break;
                case T_S_(COMMANDE_DEPREF):
                        SECURISE(commande_definition_depref(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), depref))));
                        break;
                case T_S_(COMMANDE_ECHAPPEMENT):
                        SECURISE(commande_definition_echappement(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), echappement))));
                        break;
                case T_S_(COMMANDE_ERROR):
                        SECURISE(commande_definition_error(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), error))));
                        break;
                case T_S_(COMMANDE_EXTREF):
                        SECURISE(commande_definition_extref(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extref))));
                        break;
                case T_S_(COMMANDE_EXTREFS):
                        SECURISE(commande_definition_extrefs(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extrefs))));
                        break;
                case T_S_(COMMANDE_FOOT):
                        SECURISE(commande_definition_foot(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), foot))));
                        break;
                case T_S_(COMMANDE_GENERIC):
                        SECURISE(commande_definition_generic(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), generic))));
                        break;
                case T_S_(COMMANDE_HEAD):
                        SECURISE(commande_definition_head(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), head))));
                        break;
                case T_S_(COMMANDE_IDEA):
                        SECURISE(commande_definition_idea(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), idea))));
                        break;
                case T_S_(COMMANDE_INCLUDE):
                        SECURISE(commande_definition_include(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), include))));
                        break;
                case T_S_(COMMANDE_INDEX):
                        SECURISE(commande_definition_index(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), index))));
                        break;
                case T_S_(COMMANDE_MACRO):
                        SECURISE(commande_definition_macro(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), macro))));
                        break;
                case T_S_(COMMANDE_MESG):
                        SECURISE(commande_definition_mesg(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), mesg))));
                        break;
                case T_S_(COMMANDE_MESSAGE):
                        SECURISE(commande_definition_message(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), message))));
                        break;
                case T_S_(COMMANDE_MISSING):
                        SECURISE(commande_definition_missing(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), missing))));
                        break;
                case T_S_(COMMANDE_OPTION):
                        SECURISE(commande_definition_option(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), option))));
                        break;
                case T_S_(COMMANDE_OPTIONS):
                        SECURISE(commande_definition_options(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), options))));
                        break;
                case T_S_(COMMANDE_PARAMETRE):
                        SECURISE(commande_definition_parametre(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), parametre))));
                        break;
                case T_S_(COMMANDE_REFERENCE):
                        SECURISE(commande_definition_reference(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), reference))));
                        break;
                case T_S_(COMMANDE_REF):
                        SECURISE(commande_definition_ref(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), ref))));
                        break;
                case T_S_(COMMANDE_SEC):
                        SECURISE(commande_definition_sec(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), sec))));
                        break;
                case T_S_(COMMANDE_SECTION):
                        SECURISE(commande_definition_section(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), section))));
                        break;
                case T_S_(COMMANDE_STANDARD):
                        SECURISE(commande_definition_standard(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), standard))));
                        break;
                case T_S_(COMMANDE_START):
                        SECURISE(commande_definition_start(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), start))));
                        break;
                case T_S_(COMMANDE_TEXTE):
                        SECURISE(commande_definition_texte(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), texte))));
                        break;
                case T_S_(COMMANDE_TITLE):
                        SECURISE(commande_definition_title(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), title))));
                        break;
                case T_S_(COMMANDE_TXT):
                        SECURISE(commande_definition_txt(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), txt))));
                        break;
                case T_S_(COMMANDE_WARNING):
                        SECURISE(commande_definition_warning(copie,T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), warning))));
                        break;
                default:
                        SECURISE(commande_destruction(copie));
                        return RESULTAT_ERREUR_DOMAINE;
        }
        return RESULTAT_OK;
}

Resultat commande_destruction(TRAVAIL(Commande) commande)
{
        /* Cette fonction sert � d�truire une commande, et ce quelle qu'elle soit.
         * Renvoit l'erreur RESULTAT_ERREUR_DOMAINE si le type est incorrect.
         */
        if(S_T(commande)==NULL)
                return RESULTAT_OK;
        switch(T_S_(CHAMP(commande, type)))
        {
                case T_S_(COMMANDE_COMMENTAIRE):
                        SECURISE(commandecommentaire_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), commentaire))));
                        break;
                case T_S_(COMMANDE_DEFINE):
                        SECURISE(commandedefine_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), define))));
                        break;
                case T_S_(COMMANDE_DEP):
                        SECURISE(commandedep_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), dep))));
                        break;
                case T_S_(COMMANDE_DEPREF):
                        SECURISE(commandedepref_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), depref))));
                        break;
                case T_S_(COMMANDE_ECHAPPEMENT):
                        SECURISE(commandeechappement_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), echappement))));
                        break;
                case T_S_(COMMANDE_ERROR):
                        SECURISE(commandeerror_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), error))));
                        break;
                case T_S_(COMMANDE_EXTREF):
                        SECURISE(commandeextref_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extref))));
                        break;
                case T_S_(COMMANDE_EXTREFS):
                        SECURISE(commandeextrefs_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), extrefs))));
                        break;
                case T_S_(COMMANDE_FOOT):
                        SECURISE(commandefoot_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), foot))));
                        break;
                case T_S_(COMMANDE_GENERIC):
                        SECURISE(commandegeneric_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), generic))));
                        break;
                case T_S_(COMMANDE_HEAD):
                        SECURISE(commandehead_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), head))));
                        break;
                case T_S_(COMMANDE_IDEA):
                        SECURISE(commandeidea_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), idea))));
                        break;
                case T_S_(COMMANDE_INCLUDE):
                        SECURISE(commandeinclude_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), include))));
                        break;
                case T_S_(COMMANDE_INDEX):
                        SECURISE(commandeindex_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), index))));
                        break;
                case T_S_(COMMANDE_MACRO):
                        SECURISE(commandemacro_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), macro))));
                        break;
                case T_S_(COMMANDE_MESG):
                        SECURISE(commandemesg_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), mesg))));
                        break;
                case T_S_(COMMANDE_MESSAGE):
                        SECURISE(commandemessage_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), message))));
                        break;
                case T_S_(COMMANDE_MISSING):
                        SECURISE(commandemissing_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), missing))));
                        break;
                case T_S_(COMMANDE_OPTION):
                        SECURISE(commandeoption_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), option))));
                        break;
                case T_S_(COMMANDE_OPTIONS):
                        SECURISE(commandeoptions_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), options))));
                        break;
                case T_S_(COMMANDE_PARAMETRE):
                        SECURISE(commandeparametre_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), parametre))));
                        break;
                case T_S_(COMMANDE_REFERENCE):
                        SECURISE(commandereference_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), reference))));
                        break;
                case T_S_(COMMANDE_REF):
                        SECURISE(commanderef_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), ref))));
                        break;
                case T_S_(COMMANDE_SEC):
                        SECURISE(commandesec_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), sec))));
                        break;
                case T_S_(COMMANDE_SECTION):
                        SECURISE(commandesection_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), section))));
                        break;
                case T_S_(COMMANDE_STANDARD):
                        SECURISE(commandestandard_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), standard))));
                        break;
                case T_S_(COMMANDE_START):
                        SECURISE(commandestart_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), start))));
                        break;
                case T_S_(COMMANDE_TEXTE):
                        SECURISE(commandetexte_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), texte))));
                        break;
                case T_S_(COMMANDE_TITLE):
                        SECURISE(commandetitle_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), title))));
                        break;
                case T_S_(COMMANDE_TXT):
                        SECURISE(commandetxt_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), txt))));
                        break;
                case T_S_(COMMANDE_WARNING):
                        SECURISE(commandewarning_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(commande, commande), warning))));
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
        }
        free(S_T(commande));
        S_T(commande)=NULL;
        return RESULTAT_OK;
}

