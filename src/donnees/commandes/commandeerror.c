/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeerror.h"

Resultat commandeerror_initialisation(TRAVAIL(CommandeError) commandeerror)
{
        /* Cr�e une commande d'erreur vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeerror)=NOUVEAU(CommandeError))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeerror, localisation)=NULL;
        CHAMP(commandeerror, erreur)=NULL;
        return RESULTAT_OK;
}

Resultat commandeerror_definition_localisationfichier(TRAVAIL(CommandeError) commandeerror, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande error dans le source.
         * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeerror, localisation)));
        return RESULTAT_OK;
}

Resultat commandeerror_definition_erreur(TRAVAIL(CommandeError) commandeerror, TRAVAIL(Flux) erreur)
{
        /* Assigne un flux de texte d'erreur.
         * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(erreur,CHAMP_TRAVAIL(commandeerror, erreur)));
        return RESULTAT_OK;
}

Resultat commandeerror_lecture_localisationfichier(TRAVAIL(CommandeError) commandeerror, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande error dans le source.
         * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeerror, localisation);
        return RESULTAT_OK;
}

Resultat commandeerror_lecture_erreur(TRAVAIL(CommandeError) commandeerror, REFERENCE(Flux) erreur)
{
        /* Lit un flux de texte d'erreur.
         * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        T_R(erreur)=CHAMP_TRAVAIL(commandeerror, erreur);
        return RESULTAT_OK;
}

Resultat commandeerror_parcours(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeerror, erreur),action,general));
        return RESULTAT_OK;
}
Resultat commandeerror_copie(TRAVAIL(CommandeError) commandeerror, TRAVAIL(CommandeError) copie)
{
        /* Cr�e une copie de la commande d'erreur.
         * Renvoie RESULTAT_ERREUR si commandeerror est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeerror_destruction(copie));
        SECURISE(commandeerror_initialisation(copie));
        SECURISE(commandeerror_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeerror, localisation)));
        SECURISE(commandeerror_definition_erreur(copie,CHAMP_TRAVAIL(commandeerror, erreur)));
        return RESULTAT_OK;
}

Resultat commandeerror_destruction(TRAVAIL(CommandeError) commandeerror)
{
        /* D�truit une commande d'erreur.
         */
        if(S_T(commandeerror)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeerror, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeerror, erreur)));
        free(S_T(commandeerror));
        S_T(commandeerror)=NULL;
        return RESULTAT_OK;
}

