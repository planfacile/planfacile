/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEOPTION__
#define __COMMANDEOPTION__

#include <src/global/global.h>

typedef struct commandeoption CONTENEUR(CommandeOption);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeoption
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la d�claration d'option dans le source.
        STOCKAGE_SCALAIRE(NomOption) option;
        //Nom de l'option d�clar�e.
};
/* Cette commande est destin�e � recevoir les d�clarations d'options.
 * Elle ne sera cependant pas directement incluse dans le flux, mais
 * sera plac�e � part, dans un tableau redimensionnable.
 */

Resultat commandeoption_initialisation(TRAVAIL(CommandeOption) commandeoption);
/* Cr�e une commande d'option vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoption_definition_localisationfichier(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande option.
 * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoption_definition_option(TRAVAIL(CommandeOption) commandeoption, TRAVAIL_SCALAIRE(NomOption) option);
/* Assigne un nom d'option � une commande d'option.
 * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoption_lecture_localisationfichier(TRAVAIL(CommandeOption) commandeoption, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande option.
 * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
 */

Resultat commandeoption_lecture_option(TRAVAIL(CommandeOption) commandeoption, REFERENCE_SCALAIRE(NomOption) option);
/* Lit un nom d'option � une commande d'option.
 * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
 */

Resultat commandeoption_parcours(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeoption_copie(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(CommandeOption) copie);
/* Cr�e une copie de la commande option.
 * Renvoie RESULTAT_ERREUR si commandeoption est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeoption_destruction(TRAVAIL(CommandeOption) commandeoption);
/* D�truit une commande d'option.
 */

#endif
