/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEREF__
#define __COMMANDEREF__

#include <src/global/global.h>

typedef struct commanderef CONTENEUR(CommandeRef);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commanderef
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande de remplacement par r�f�rence
        //dans le source.
};
/* Commande d�sign�e pour indiquer o� remplacer par une r�f�rence
 * dans un format de r�f�rence.
 */

Resultat commanderef_initialisation(TRAVAIL(CommandeRef) commanderef);
/* Cr�e une commande ref vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commanderef_definition_localisationfichier(TRAVAIL(CommandeRef) commanderef, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande ref.
 * Renvoie RESULTAT_ERREUR si commanderef est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commanderef_lecture_localisationfichier(TRAVAIL(CommandeRef) commanderef, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande ref.
 * Renvoie RESULTAT_ERREUR si commanderef est NULL.
 */

Resultat commanderef_parcours(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commanderef_copie(TRAVAIL(CommandeRef) commanderef, TRAVAIL(CommandeRef) copie);
/* Cr�e une copie de la commande ref.
 * Renvoie RESULTAT_ERREUR si commanderef est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commanderef_destruction(TRAVAIL(CommandeRef) commanderef);
/* D�truit une commande ref.
 */

#endif
