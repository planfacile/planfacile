/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandemessage.h"

Resultat commandemessage_initialisation(TRAVAIL(CommandeMessage) commandemessage)
{
        /* Cr�e une commande de message vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(((S_T(commandemessage))=NOUVEAU(CommandeMessage))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemessage, localisation)=NULL;
        CHAMP(commandemessage, message)=NULL;
        return RESULTAT_OK;
}

Resultat commandemessage_definition_localisationfichier(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position de une commande message dans le source.
         * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandemessage, localisation)));
        return RESULTAT_OK;
}

Resultat commandemessage_definition_message(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(Flux) message)
{
        /* Assigne un flux de texte de message.
         * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(message,CHAMP_TRAVAIL(commandemessage, message)));
        return RESULTAT_OK;
}

Resultat commandemessage_lecture_localisationfichier(TRAVAIL(CommandeMessage) commandemessage, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position de une commande message dans le source.
         * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandemessage, localisation);
        return RESULTAT_OK;
}

Resultat commandemessage_lecture_message(TRAVAIL(CommandeMessage) commandemessage, REFERENCE(Flux) message)
{
        /* Lit un flux de texte de message.
         * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        T_R(message)=CHAMP_TRAVAIL(commandemessage, message);
        return RESULTAT_OK;
}

Resultat commandemessage_parcours(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandemessage, message),action,general));
        return RESULTAT_OK;
}

Resultat commandemessage_copie(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(CommandeMessage) copie)
{
        /* Cr�e une copie de la commande de message.
         * Renvoie RESULTAT_ERREUR si commandemessage est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemessage_destruction(copie));
        SECURISE(commandemessage_initialisation(copie));
        SECURISE(commandemessage_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandemessage, localisation)));
        SECURISE(commandemessage_definition_message(copie,CHAMP_TRAVAIL(commandemessage, message)));
        return RESULTAT_OK;
}

Resultat commandemessage_destruction(TRAVAIL(CommandeMessage) commandemessage)
{
        /* D�truit une commande de message.
         */
        if(S_T(commandemessage)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandemessage, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandemessage, message)));
        free(S_T(commandemessage));
        S_T(commandemessage)=NULL;
        return RESULTAT_OK;
}

