/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeoption.h"

Resultat commandeoption_initialisation(TRAVAIL(CommandeOption) commandeoption)
{
        /* Cr�e une commande d'option vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandeoption)=NOUVEAU(CommandeOption))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeoption, localisation)=NULL;
        CHAMP(commandeoption, option)=NULL;
        return RESULTAT_OK;
}

Resultat commandeoption_definition_localisationfichier(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande option.
         * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeoption, localisation)));
        return RESULTAT_OK;
}

Resultat commandeoption_definition_option(TRAVAIL(CommandeOption) commandeoption, TRAVAIL_SCALAIRE(NomOption) option)
{
        /* Assigne un nom de fichier � une commande d'option.
         * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandeoption, option)!=NULL)
        {
                free(CHAMP(commandeoption, option));
                CHAMP(commandeoption, option)=NULL;
        }
        ASSERTION((CHAMP(commandeoption, option)=DUP_CAST(NomOption, option))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        return RESULTAT_OK;
}

Resultat commandeoption_lecture_localisationfichier(TRAVAIL(CommandeOption) commandeoption, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande option.
         * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
         */ 
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeoption, localisation);
        return RESULTAT_OK;
}

Resultat commandeoption_lecture_option(TRAVAIL(CommandeOption) commandeoption, REFERENCE_SCALAIRE(NomOption) option)
{
        /* Lit un nom de fichier � une commande d'option.
         * Renvoie RESULTAT_ERREUR si commandeoption est NULL.
         */ 
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        T_R_(option)=T_S_(CHAMP(commandeoption, option));
        return RESULTAT_OK;
}

Resultat commandeoption_parcours(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandeoption_copie(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(CommandeOption) copie)
{
        /* Cr�e une copie de la commande option.
         * Renvoie RESULTAT_ERREUR si commandeoption est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoption_destruction(copie));
        SECURISE(commandeoption_initialisation(copie));
        SECURISE(commandeoption_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeoption, localisation)));
        SECURISE(commandeoption_definition_option(copie,T_S_(CHAMP(commandeoption, option))));
        return RESULTAT_OK;
}

Resultat commandeoption_destruction(TRAVAIL(CommandeOption) commandeoption)
{
        /* D�truit une commande d'option.
         */ 
        if(S_T(commandeoption)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeoption, localisation)));
        if(CHAMP(commandeoption, option)!=NULL)
        {
                free(CHAMP(commandeoption, option));
                CHAMP(commandeoption, option)=NULL;
        }
        free(S_T(commandeoption));
        S_T(commandeoption)=NULL;
        return RESULTAT_OK;
}

