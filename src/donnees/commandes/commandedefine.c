/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandedefine.h"

Resultat commandedefine_initialisation(TRAVAIL(CommandeDefine) commandedefine)
{
        /* Cr�e une commande define vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandedefine)=NOUVEAU(CommandeDefine))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandedefine, localisation)=NULL;
        CHAMP(commandedefine, nom)=NULL;
        CHAMP(commandedefine, definition)=NULL;
        CHAMP(commandedefine, parametres)=0;
        CHAMP(commandedefine, boucle)=FAUX;
        CHAMP(commandedefine, appel)=FAUX;
        CHAMP(commandedefine, niveau)=0;
        return RESULTAT_OK;
}

Resultat commandedefine_definition_localisationfichier(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position de la commande de d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandedefine, localisation)));
        return RESULTAT_OK;
}

Resultat commandedefine_definition_nom(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(NomMacro) nom)
{
        /* Assigne un nom � la d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandedefine, nom)!=NULL)
        {
                free(CHAMP(commandedefine, nom));
                CHAMP(commandedefine, nom)=NULL;
        }
        ASSERTION((CHAMP(commandedefine, nom)=DUP_CAST(NomMacro, nom))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat commandedefine_definition_definition(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(Flux) definition)
{
        /* Assigne une d�finition � la macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(definition,CHAMP_TRAVAIL(commandedefine, definition)));
        return RESULTAT_OK;
}

Resultat commandedefine_definition_parametres(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(Taille) parametres, TRAVAIL_SCALAIRE(Booleen) boucle)
{
        /* Assigne les caract�ristiques de param�tres � une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandedefine, parametres)=S_T_(parametres);
        CHAMP(commandedefine, boucle)=S_T_(boucle);
        return RESULTAT_OK;
}

Resultat commandedefine_definition_appel(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(Booleen) appel)
{
        /* Assigne les caract�ristiques d'appel � une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandedefine, appel)=S_T_(appel);
        return RESULTAT_OK;
}

Resultat commandedefine_definition_niveau(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Assigne le niveau d'une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandedefine, niveau)=S_T_(niveau);
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_localisationfichier(TRAVAIL(CommandeDefine) commandedefine, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position de la commande de d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandedefine, localisation);
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_nom(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(NomMacro) nom)
{
        /* Assigne un nom � la d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        T_R_(nom)=T_S_(CHAMP(commandedefine, nom));
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_definition(TRAVAIL(CommandeDefine) commandedefine, REFERENCE(Flux) definition)
{
        /* Assigne une d�finition � la macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        T_R(definition)=CHAMP_TRAVAIL(commandedefine, definition);
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_parametres(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(Taille) parametres, REFERENCE_SCALAIRE(Booleen) boucle)
{
        /* Assigne les caract�ristiques de param�tres � une d�finition de macro.
         * Dans cette commande, placer un des argument de r�sultat � NULL revient
         * � ne pas demander l'information correspondante.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        if(parametres!=NULL)
                T_R_(parametres)=T_S_(CHAMP(commandedefine, parametres));
        if(boucle!=NULL)
                T_R_(boucle)=T_S_(CHAMP(commandedefine, boucle));
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_appel(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(Booleen) appel)
{
        /* Lit les caract�ristiques d'appel � une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        T_R_(appel)=T_S_(CHAMP(commandedefine, appel));
        return RESULTAT_OK;
}

Resultat commandedefine_lecture_niveau(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Lit le niveau d'une d�finition de macro.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        T_R_(niveau)=T_S_(CHAMP(commandedefine, niveau));
        return RESULTAT_OK;
}

Resultat commandedefine_parcours(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedefine, definition),action,general));
        return RESULTAT_OK;
}

Resultat commandedefine_copie(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(CommandeDefine) copie)
{
        /* Cr�e une copie de la commande define.
         * Renvoie RESULTAT_ERREUR si commandedefine est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si uen allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedefine_destruction(copie));
        SECURISE(commandedefine_initialisation(copie));
        SECURISE(commandedefine_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandedefine, localisation)));
        SECURISE(commandedefine_definition_nom(copie,T_S_(CHAMP(commandedefine, nom))));
        SECURISE(commandedefine_definition_definition(copie,CHAMP_TRAVAIL(commandedefine, definition)));
        SECURISE(commandedefine_definition_parametres(copie,T_S_(CHAMP(commandedefine, parametres)),T_S_(CHAMP(commandedefine, boucle))));
        SECURISE(commandedefine_definition_appel(copie,T_S_(CHAMP(commandedefine, appel))));
        SECURISE(commandedefine_definition_niveau(copie,T_S_(CHAMP(commandedefine, niveau))));
        return RESULTAT_OK;
}

Resultat commandedefine_destruction(TRAVAIL(CommandeDefine) commandedefine)
{
        /* D�truit une commande de d�finition de macro.
         */
        if(S_T(commandedefine)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandedefine, localisation)));
        if(CHAMP(commandedefine, nom)!=NULL)
                free(CHAMP(commandedefine, nom));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedefine, definition)));
        free(S_T(commandedefine));
        S_T(commandedefine)=NULL;
        return RESULTAT_OK;
}

