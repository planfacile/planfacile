/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDELOCALISATION__
#define __COMMANDELOCALISATION__

#include <src/global/global.h>

typedef struct descriptionfichier CONTENEUR(DescriptionFichier);
typedef struct localisationfichier CONTENEUR(LocalisationFichier);

typedef Entier CONTENEUR_SCALAIRE(PositionFichier);
//Position dans un fichier.

struct descriptionfichier
{
        STOCKAGE_SCALAIRE(NomFichier) nom;
        //Nom du fichier dans lequel la localisation est donn�e.
        STOCKAGE_SCALAIRE(NomFichier) nomabsolu;
        //Nom du fichier dans lequel la localisation est donn�e.
        //Ce nom est absolu. Ce param�tre peut �tre �gal � NULL.
        STOCKAGE_SCALAIRE(DescripteurFichier) descripteur;
        //Descripteur de fichier, utilis� par l'analyseur lexical.
        STOCKAGE(LocalisationFichier) inclusion;
        //Si inclusion vaut NULL, c'est que le fichier n'a pas �t� inclu.
        //Sinon, inclusion pointe sur une structure de localisation correspondant
        //� la localisation depuis laquelle le fichier courant a �t� inclus.
        STOCKAGE_SCALAIRE(unsigned long) references;
        //Nombre d'objets LocalisationFichier utilisant la description.
};
        
struct localisationfichier
{
        STOCKAGE(DescriptionFichier) fichier;
        //Description de la localisation de fichier.
        STOCKAGE_SCALAIRE(PositionFichier) position;
        //Position dans le fichier de la localisation.
};
/* Localisation dans un fichier. Cette localisation sert au compilateur pour d�terminer
 * � quel endroit dans le source une erreur s'est produite, m�me longtemps apr�s la
 * fin de la phase d'analyse.
 */

Resultat localisationfichier_initialisation(TRAVAIL(LocalisationFichier) localisationfichier);
/* Cr�e une localisation de fichier vide.
 */

Resultat localisationfichier_ajout(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(NomFichier) nom, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, TRAVAIL_SCALAIRE(DescripteurFichier) descripteur, TRAVAIL_SCALAIRE(PositionFichier) position);
/* Ajoute une localisation de fichier. La position est celle dans le fichier inclus.
 * Utilis� lors de l'inclusion d'un fichier.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 */

Resultat localisationfichier_modification(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(PositionFichier) position);
/* Modifie la position dans le fichier courant.
 * Retourne RESULTAT_ERREUR si localisationfichier est NULL.
 * Retourne RESULTAT_ERREUR_DOMAINE si position est n�gatif ou nul.
 */

Resultat localisationfichier_retrait(TRAVAIL(LocalisationFichier) localisationfichier);
/* Retire une localisation de fichier.
 * Ne referme pas le fichier d�sign� par le descripteur.
 * Utilis� lors d'un retour de fichier inclus.
 */

Resultat localisationfichier_lecture_nom(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(NomFichier) nom);
/* Lit le nom du fichier.
 * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
 */

Resultat localisationfichier_lecture_nomabsolu(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(NomFichier) nomabsolu);
/* Lit le nom absolu du fichier.
 * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
 */

Resultat localisationfichier_lecture_descripteur(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(DescripteurFichier) descripteur);
/* Lit le descripteur du dernier fichier inclus.
 * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
 */

Resultat localisationfichier_lecture_position(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(PositionFichier) position);
/* Lit la position dans le dernier fichier inclus.
 * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
 */

Resultat localisationfichier_lecture_inclusion(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE(LocalisationFichier) inclusion);
/* Renvoie la position du fichier parent dans l'ordre d'inclusion.
 * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
 */

Resultat localisationfichier_vide(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(Booleen) vide);
/* Indique si la localisationfichier est vide ou non.
 */

Resultat localisationfichier_test_fichier(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, REFERENCE_SCALAIRE(Booleen) present);
/* Indique si un nom de fichier est pr�sent dans la localisation
 * de fichier, afin de savoir si on ne r�alise pas une inclusion
 * r�cursive. Le r�sultat est renvoy� dans le bool�en pass� en
 * param�tre.
 */

Resultat localisationfichier_copie(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL(LocalisationFichier) copie);
/* Effectue une copie d'une localisation de fichier.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat localisationfichier_destruction(TRAVAIL(LocalisationFichier) localisationfichier);
/* D�truit une localisation de fichier.
 */

#endif
