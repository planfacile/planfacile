/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandereference.h"

Resultat commandereference_initialisation(TRAVAIL(CommandeReference) commandereference)
{
        /* Cr�e une commande d'une r�f�rence vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandereference)=NOUVEAU(CommandeReference))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandereference, localisation)=NULL;
        CHAMP(commandereference, niveau)=NULL;
        CHAMP(commandereference, format)=NULL;
        return RESULTAT_OK;
}

Resultat commandereference_definition_localisationfichier(TRAVAIL(CommandeReference) commandereference, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande r�f�rence.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandereference, localisation)));
        return RESULTAT_OK;
}

Resultat commandereference_definition_niveau(TRAVAIL(CommandeReference) commandereference, TRAVAIL(Flux) niveau)
{
        /* Assigne un niveau � une r�f�rence.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        if(S_T(niveau)==NULL)
                CHAMP(commandereference, niveau)=NULL;
        else
        {
                SECURISE(flux_copie(niveau,CHAMP_TRAVAIL(commandereference, niveau)));
        }
        return RESULTAT_OK;
}

Resultat commandereference_definition_format(TRAVAIL(CommandeReference) commandereference, TRAVAIL(Flux) format)
{
        /* Assigne un format � une r�f�rence.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(format,CHAMP_TRAVAIL(commandereference, format)));
        return RESULTAT_OK;
}

Resultat commandereference_lecture_localisationfichier(TRAVAIL(CommandeReference) commandereference, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande r�f�rence.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandereference, localisation);
        return RESULTAT_OK;
}

Resultat commandereference_lecture_niveau(TRAVAIL(CommandeReference) commandereference, REFERENCE(Flux) niveau)
{
        /* Lit un niveau � une r�f�rence.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        T_R(niveau)=CHAMP_TRAVAIL(commandereference, niveau);
        return RESULTAT_OK;
}

Resultat commandereference_lecture_format(TRAVAIL(CommandeReference) commandereference, REFERENCE(Flux) format)
{
        /* Lit un format � une r�f�rence.
         * Renvoie RESULTAT_ERREUR si commander�f�rence est NULL.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        T_R(format)=CHAMP_TRAVAIL(commandereference, format);
        return RESULTAT_OK;
}

Resultat commandereference_parcours(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandereference, niveau)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandereference, niveau),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandereference, format),action,general));
        return RESULTAT_OK;
}

Resultat commandereference_copie(TRAVAIL(CommandeReference) commandereference, TRAVAIL(CommandeReference) copie)
{
        /* Cr�e une copie de la commande reference.
         * Renvoie RESULTAT_ERREUR si commandereference est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandereference_destruction(copie));
        SECURISE(commandereference_initialisation(copie));
        SECURISE(commandereference_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandereference, localisation)));
        SECURISE(commandereference_definition_niveau(copie,CHAMP_TRAVAIL(commandereference, niveau)));
        SECURISE(commandereference_definition_format(copie,CHAMP_TRAVAIL(commandereference, format)));
        return RESULTAT_OK;
}

Resultat commandereference_destruction(TRAVAIL(CommandeReference) commandereference)
{
        /* D�truit une r�f�rence.
         */ 
        if(S_T(commandereference)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandereference, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandereference, niveau)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandereference, format)));
        free(S_T(commandereference));
        S_T(commandereference)=NULL;
        return RESULTAT_OK;
}

