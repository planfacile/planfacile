/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDESTART__
#define __COMMANDESTART__

#include <src/global/global.h>

typedef struct commandestart CONTENEUR(CommandeStart);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandestart
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande de d�part dans le source.
        STOCKAGE(Flux) niveau;
        //Flux repr�sentant le niveau de d�part.
        //Si le flux est NULL, et ben, ca va chier !
        //Si le flux ne repr�sente pas un nombre, on recherchera le niveau
        //qui correspond au nom de section que repr�sente ce flux.
        //(requiert une passe de plus que g�n�rer une erreur...)
        //S'il n'est pas trouv�, le niveau choisi sera le niveau
        //le plus petit (le plus �lev� dans la hierarchie) d�fini explicitement
        //avec la commande #section.
        //Ce flux est de type FLUX_NIVEAU.
};
/* Commande d�sign�e pour sp�cifier le niveau de d�part du plan.
 * Si cette commande est absente, cela �quivaudra � un appel #start o� le niveau n'est
 * pas trouv�.
 */

Resultat commandestart_initialisation(TRAVAIL(CommandeStart) commandestart);
/* Cr�e une commande start vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandestart_definition_localisationfichier(TRAVAIL(CommandeStart) commandestart, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande start.
 * Renvoie RESULTAT_ERREUR si commandestart est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandestart_definition_niveau(TRAVAIL(CommandeStart) commandestart, TRAVAIL(Flux) niveau);
/* Assigne un niveau � la commande start.
 * Renvoie RESULTAT_ERREUR si commandestart est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandestart_lecture_localisationfichier(TRAVAIL(CommandeStart) commandestart, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande start.
 * Renvoie RESULTAT_ERREUR si commandestart est NULL.
 */

Resultat commandestart_lecture_niveau(TRAVAIL(CommandeStart) commandestart, REFERENCE(Flux) niveau);
/* Lit un niveau � la commande start.
 * Renvoie RESULTAT_ERREUR si commandestart est NULL.
 */

Resultat commandestart_parcours(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandestart_copie(TRAVAIL(CommandeStart) commandestart, TRAVAIL(CommandeStart) copie);
/* Cr�e une copie de la commande start.
 * Renvoie RESULTAT_ERREUR si commandestart est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandestart_destruction(TRAVAIL(CommandeStart) commandestart);
/* D�truit une commande start.
 */

#endif
