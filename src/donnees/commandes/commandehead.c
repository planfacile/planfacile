/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandehead.h"

Resultat commandehead_initialisation(TRAVAIL(CommandeHead) commandehead)
{
        /* Cr�e une commande de entete de document vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandehead)=NOUVEAU(CommandeHead))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandehead, localisation)=NULL;
        CHAMP(commandehead, entete)=NULL;
        return RESULTAT_OK;
}

Resultat commandehead_definition_localisationfichier(TRAVAIL(CommandeHead) commandehead, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande head.
         * Renvoie RESULTAT_ERREUR si commandehead est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandehead, localisation)));
        return RESULTAT_OK;
}

Resultat commandehead_definition_entete(TRAVAIL(CommandeHead) commandehead, TRAVAIL(Flux) entete)
{
        /* Assigne un entete de document � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandehead est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(entete,CHAMP_TRAVAIL(commandehead, entete)));
        return RESULTAT_OK;
}

Resultat commandehead_lecture_localisationfichier(TRAVAIL(CommandeHead) commandehead, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande head.
         * Renvoie RESULTAT_ERREUR si commandehead est NULL.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandehead, localisation);
        return RESULTAT_OK;
}

Resultat commandehead_lecture_entete(TRAVAIL(CommandeHead) commandehead, REFERENCE(Flux) entete)
{
        /* Lit un entete de document � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandehead est NULL.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        T_R(entete)=CHAMP_TRAVAIL(commandehead, entete);
        return RESULTAT_OK;
}

Resultat commandehead_parcours(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandehead, entete),action,general));
        return RESULTAT_OK;
}

Resultat commandehead_copie(TRAVAIL(CommandeHead) commandehead, TRAVAIL(CommandeHead) copie)
{
        /* Cr�e une copie de la commande head.
         * Renvoie RESULTAT_ERREUR si commandehead est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandehead_destruction(copie));
        SECURISE(commandehead_initialisation(copie));
        SECURISE(commandehead_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandehead, localisation)));
        SECURISE(commandehead_definition_entete(copie,CHAMP_TRAVAIL(commandehead, entete)));
        return RESULTAT_OK;
}

Resultat commandehead_destruction(TRAVAIL(CommandeHead) commandehead)
{
        /* D�truit une commande d'entete de document.
         */
        if(S_T(commandehead)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandehead, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandehead, entete)));
        free(S_T(commandehead));
        S_T(commandehead)=NULL;
        return RESULTAT_OK;
}

