/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandemesg.h"

Resultat commandemesg_initialisation(TRAVAIL(CommandeMesg) commandemesg)
{
        /* Cr�e une commande mesg vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandemesg)=NOUVEAU(CommandeMesg))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemesg, localisation)=NULL;
        return RESULTAT_OK;
}

Resultat commandemesg_definition_localisationfichier(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande mesg dans le source.
         * Renvoie RESULTAT_ERREUR si commandemesg est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandemesg, localisation)));
        return RESULTAT_OK;
}

Resultat commandemesg_lecture_localisationfichier(TRAVAIL(CommandeMesg) commandemesg, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande mesg dans le source.
         * Renvoie RESULTAT_ERREUR si commandemesg est NULL.
         */
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandemesg, localisation);
        return RESULTAT_OK;
}

Resultat commandemesg_parcours(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandemesg_copie(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(CommandeMesg) copie)
{
        /* Cr�e une copie de la commande mesg.
         * Renvoie RESULTAT_ERREUR si commandemesg est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemesg_destruction(copie));
        SECURISE(commandemesg_initialisation(copie));
        SECURISE(commandemesg_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandemesg, localisation)));
        return RESULTAT_OK;
}

Resultat commandemesg_destruction(TRAVAIL(CommandeMesg) commandemesg)
{
        /* D�truit une commande mesg.
         */
        if(S_T(commandemesg)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandemesg, localisation)));
        free(S_T(commandemesg));
        S_T(commandemesg)=NULL;
        return RESULTAT_OK;
}

