/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEDEPREF__
#define __COMMANDEDEPREF__

#include <src/global/global.h>

typedef struct commandedepref CONTENEUR(CommandeDepRef);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandedepref
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la r�f�rence dans le source.
        STOCKAGE(Flux) format;
        //Format facultatif de la r�f�rence.
        //S'il est absent (NULL), celui pr�cis� dans la commande
        //#reference est pris en compte.
        //Ce flux est de type FLUX_REFERENCE_FORMAT.
};
/* Commande d�sign�e pour indiquer la pr�sence d'une r�f�rence � une autre id�e
 * dans une d�pendance.
 */

Resultat commandedepref_initialisation(TRAVAIL(CommandeDepRef) commandedepref);
/* Initialise une commande depref vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedepref_definition_localisationfichier(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande depref
 * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedepref_definition_format(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(Flux) format);
/* Assigne un format � une commande depref.
 * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedepref_lecture_localisationfichier(TRAVAIL(CommandeDepRef) commandedepref, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande depref
 * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
 */

Resultat commandedepref_lecture_format(TRAVAIL(CommandeDepRef) commandedepref, REFERENCE(Flux) format);
/* Assigne un format � une commande depref.
 * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
 */

Resultat commandedepref_parcours(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandedepref_copie(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(CommandeDepRef) copie);
/* Cr�e une copie de la commande depref.
 * Renvoie RESULTAT_ERREUR si commandedepref est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandedepref_destruction(TRAVAIL(CommandeDepRef) commandedepref);
/* D�truit une commande depref.
 */

#endif
