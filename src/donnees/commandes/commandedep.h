/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEDEP__
#define __COMMANDEDEP__

#include <src/global/global.h>

typedef struct commandedep CONTENEUR(CommandeDep);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandedep
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la d�pendance dans le source.
        STOCKAGE(Flux) pertinence;
        //Flux donnant la pertinence de la d�pendance.
        //Cette valeur doit �tre positive.
        //Si la valeur donn�e est invalide ou absente,
        //z�ro est pris par d�faut.
        //Ce flux est de type FLUX_PERTINENCE.
        STOCKAGE(Flux) destination;
        //Flux pr�cisant la destination.
        //Si la valeur est incorrecte, produire une erreur.
        //Ce flux est de type FLUX_REFERENCE.
        STOCKAGE(Flux) irreductible;
        //Flux pr�cisant le texte � placer en cas
        //d'irr�ductibilit� de la d�pendance.
        //Ce flux est de type FLUX_TEXTE_IRREDUCTIBLE.
        STOCKAGE(Flux) reductible;
        //Flux pr�cisant le texte � placer en cas
        //de r�ductibilit� de la d�pendance.
        //Ce flux est de type FLUX_TEXTE_REDUCTIBLE.
};
/* Commande d�sign�e pour indiquer une d�pendance entre id�es.
 */

Resultat commandedep_initialisation(TRAVAIL(CommandeDep) commandedep);
/* Cr�e une commande de d�pendance vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_definition_localisationfichier(TRAVAIL(CommandeDep) commandedep, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande dep dans le source.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_definition_pertinence(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) pertinence);
/* Assigne une pertinence � une commande de d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_definition_destination(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) destination);
/* Assigne une destination � la commande de d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_definition_irreductible(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) irreductible);
/* Assigne le texte � placer en cas d'irr�ductibilit� de la d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_definition_reductible(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) reductible);
/* Assigne le texte � placer en cas de r�ductibilit� de la d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedep_lecture_localisationfichier(TRAVAIL(CommandeDep) commandedep, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande dep dans le source.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 */

Resultat commandedep_lecture_pertinence(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) pertinence);
/* Assigne une pertinence � une commande de d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 */

Resultat commandedep_lecture_destination(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) destination);
/* Assigne une destination � la commande de d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 */

Resultat commandedep_lecture_irreductible(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) irreductible);
/* Assigne le texte � placer en cas d'irr�ductibilit� de la d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 */

Resultat commandedep_lecture_reductible(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) reductible);
/* Assigne le texte � placer en cas de r�ductibilit� de la d�pendance.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL.
 */

Resultat commandedep_parcours(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandedep_copie(TRAVAIL(CommandeDep) commandedep, TRAVAIL(CommandeDep) copie);
/* Cr�e une copie de la commande dep.
 * Renvoie RESULTAT_ERREUR si commandedep est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandedep_destruction(TRAVAIL(CommandeDep) commandedep);
/* D�truit une commande de d�pendance.
 */

#endif
