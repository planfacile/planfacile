/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeparametre.h"

#define INDICENONVALIDE        -1

Resultat commandeparametre_initialisation(TRAVAIL(CommandeParametre) commandeparametre)
{
        /* Cr�e une commande de param�tre vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(((S_T(commandeparametre))=NOUVEAU(CommandeParametre))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeparametre, localisation)=NULL;
        CHAMP(commandeparametre, indice)=INDICENONVALIDE;
        return RESULTAT_OK;
}

Resultat commandeparametre_definition_localisationfichier(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande de param�tre.
         * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeparametre, localisation)));
        return RESULTAT_OK;
}

Resultat commandeparametre_definition_indice(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL_SCALAIRE(Indice) indice)
{
        /* Assigne un indice de param�tre.
         * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandeparametre, indice)=S_T_(indice);
        return RESULTAT_OK;
}

Resultat commandeparametre_lecture_localisationfichier(TRAVAIL(CommandeParametre) commandeparametre, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande param�tre.
         * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeparametre, localisation);
        return RESULTAT_OK;
}

Resultat commandeparametre_lecture_indice(TRAVAIL(CommandeParametre) commandeparametre, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Lit un indice de param�tre.
         * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        T_R_(indice)=T_S_(CHAMP(commandeparametre, indice));
        return RESULTAT_OK;
}

Resultat commandeparametre_parcours(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandeparametre_copie(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(CommandeParametre) copie)
{
        /* Cr�e une copie de la commande de param�tre.
         * Renvoie RESULTAT_ERREUR si commandeparametre est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeparametre_destruction(copie));
        SECURISE(commandeparametre_initialisation(copie));
        SECURISE(commandeparametre_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeparametre, localisation)));
        SECURISE(commandeparametre_definition_indice(copie,T_S_(CHAMP(commandeparametre, indice))));
        return RESULTAT_OK;
}

Resultat commandeparametre_destruction(TRAVAIL(CommandeParametre) commandeparametre)
{
        /* D�truit une commande de param�tre.
         */
        if(S_T(commandeparametre)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeparametre, localisation)));
        free(S_T(commandeparametre));
        S_T(commandeparametre)=NULL;
        return RESULTAT_OK;
}
