/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandetxt.h"

Resultat commandetxt_initialisation(TRAVAIL(CommandeTxt) commandetxt)
{
        /* Cr�e une commande txt vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandetxt)=NOUVEAU(CommandeTxt))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandetxt, localisation)=NULL;
        return RESULTAT_OK;
}

Resultat commandetxt_definition_localisationfichier(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande txt dans le source.
         * Renvoie RESULTAT_ERREUR si commandetxt est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandetxt, localisation)));
        return RESULTAT_OK;
}

Resultat commandetxt_lecture_localisationfichier(TRAVAIL(CommandeTxt) commandetxt, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande txt dans le source.
         * Renvoie RESULTAT_ERREUR si commandetxt est NULL.
         */
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandetxt, localisation);
        return RESULTAT_OK;
}

Resultat commandetxt_parcours(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandetxt_copie(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(CommandeTxt) copie)
{
        /* Cr�e une copie de la commande txt.
         * Renvoie RESULTAT_ERREUR si commandetxt est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetxt_destruction(copie));
        SECURISE(commandetxt_initialisation(copie));
        SECURISE(commandetxt_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandetxt, localisation)));
        return RESULTAT_OK;
}

Resultat commandetxt_destruction(TRAVAIL(CommandeTxt) commandetxt)
{
        /* D�truit une commande txt.
         */
        if(S_T(commandetxt)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandetxt, localisation)));
        free(S_T(commandetxt));
        S_T(commandetxt)=NULL;
        return RESULTAT_OK;
}

