/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEECHAPPEMENT__
#define __COMMANDEECHAPPEMENT__

#include <src/global/global.h>

typedef struct commandeechappement CONTENEUR(CommandeEchappement);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeechappement
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du caract�re d'echappement dans le source.
        STOCKAGE_SCALAIRE(Caractere) caractere;
        //Contient le caract�re d'�chappement.
        //Attention au cas du retour-chariot,
        //o� la commande est �limin�e.
};
/* Commande destin�e � recevoir les caract�res �chapp�s dans un flux de texte.
 */

Resultat commandeechappement_initialisation(TRAVAIL(CommandeEchappement) commandeechappement);
/* Cr�e une commande d'�chappement de caract�re vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeechappement_definition_localisationfichier(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande d'echappement de caract�re.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeechappement_definition_caractere(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL_SCALAIRE(Caractere) caractere);
/* Assigne un caract�re d'�chappement.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeechappement_lecture_localisationfichier(TRAVAIL(CommandeEchappement) commandeechappement, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande d'echappement de caract�re.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
 */

Resultat commandeechappement_lecture_caractere(TRAVAIL(CommandeEchappement) commandeechappement, REFERENCE_SCALAIRE(Caractere) caractere);
/* Lit un caract�re d'�chappement.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL.
 */

Resultat commandeechappement_remplacement_texte(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL_SCALAIRE(Booleen) danstexte, REFERENCE_SCALAIRE(Chaine) texte);
/* Renvoie le texte �quivalent d'une commande d'echappement.
 * Le bool�en est utilis� pour savoir si la commande d'echappement
 * de caract�res est situ�e ou non au milieu d'un texte.
 * La chaine renvoy�e devra �tre d�sallou�e � la main.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL,
 * et RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeechappement_parcours(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeechappement_copie(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(CommandeEchappement) copie);
/* Cr�e une copie de la commande d'�chappement de caract�re.
 * Renvoie RESULTAT_ERREUR si commandeechappement est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeechappement_destruction(TRAVAIL(CommandeEchappement) commandeechappement);
/* D�truit une commande d'�chappement de caract�re.
 */

#endif
