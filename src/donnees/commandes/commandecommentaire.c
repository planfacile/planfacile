/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandecommentaire.h"

Resultat commandecommentaire_initialisation(TRAVAIL(CommandeCommentaire) commandecommentaire)
{
        /* Cr�e une commande de commentaire vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandecommentaire)=NOUVEAU(CommandeCommentaire))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandecommentaire, localisation)=NULL;
        CHAMP(commandecommentaire, commentaire)=NULL;
        return RESULTAT_OK;
}

Resultat commandecommentaire_definition_localisationfichier(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande de commentaire.
         * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandecommentaire, localisation)));
        return RESULTAT_OK;
}

Resultat commandecommentaire_definition_commentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(Flux) commentaire)
{
        /* Assigne un commentaire dans une commande d�sign�e.
         * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(commentaire,CHAMP_TRAVAIL(commandecommentaire, commentaire)));
        return RESULTAT_OK;
}

Resultat commandecommentaire_lecture_localisationfichier(TRAVAIL(CommandeCommentaire) commandecommentaire, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande de commentaire.
         * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandecommentaire, localisation);
        return RESULTAT_OK;
}

Resultat commandecommentaire_lecture_commentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, REFERENCE(Flux) commentaire)
{
        /* Lit un commentaire.
         * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        T_R(commentaire)=CHAMP_TRAVAIL(commandecommentaire, commentaire);
        return RESULTAT_OK;
}

Resultat commandecommentaire_parcours(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandecommentaire, commentaire),action,general));
        return RESULTAT_OK;
}

Resultat commandecommentaire_copie(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(CommandeCommentaire) copie)
{
        /* Cr�e une copie de la commande de commentaire.
         * Si la copie n'est pas vide, elle est d�truite avant.
         * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL, et RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandecommentaire_destruction(copie));
        SECURISE(commandecommentaire_initialisation(copie));
        SECURISE(commandecommentaire_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandecommentaire, localisation)));
        SECURISE(commandecommentaire_definition_commentaire(copie,CHAMP_TRAVAIL(commandecommentaire, commentaire)));
        return RESULTAT_OK;
}

Resultat commandecommentaire_destruction(TRAVAIL(CommandeCommentaire) commandecommentaire)
{
        /* D�truit une commande de commentaire.
         */
        if(S_T(commandecommentaire)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandecommentaire, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandecommentaire, commentaire)));
        free(S_T(commandecommentaire));
        S_T(commandecommentaire)=NULL;
        return RESULTAT_OK;
        
}

