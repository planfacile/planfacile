/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandedep.h"

Resultat commandedep_initialisation(TRAVAIL(CommandeDep) commandedep)
{
        /* Cr�e une commande de d�pendance vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandedep)=NOUVEAU(CommandeDep))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandedep, localisation)=NULL;
        CHAMP(commandedep, pertinence)=NULL;
        CHAMP(commandedep, destination)=NULL;
        CHAMP(commandedep, irreductible)=NULL;
        CHAMP(commandedep, reductible)=NULL;
        return RESULTAT_OK;
}

Resultat commandedep_definition_localisationfichier(TRAVAIL(CommandeDep) commandedep, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande dep dans le source.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandedep, localisation)));
        return RESULTAT_OK;
}

Resultat commandedep_definition_pertinence(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) pertinence)
{
        /* Assigne une pertinence � une commande de d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        if(S_T(pertinence)==NULL)
                CHAMP(commandedep, pertinence)=NULL;
        else
        {
                SECURISE(flux_copie(pertinence,CHAMP_TRAVAIL(commandedep, pertinence)));
        }
        return RESULTAT_OK;
}

Resultat commandedep_definition_destination(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) destination)
{
        /* Assigne une destination � la commande de d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(destination,CHAMP_TRAVAIL(commandedep, destination)));
        return RESULTAT_OK;
}

Resultat commandedep_definition_irreductible(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) irreductible)
{
        /* Assigne le texte � placer en cas d'irr�ductibilit� de la d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(irreductible,CHAMP_TRAVAIL(commandedep, irreductible)));
        return RESULTAT_OK;
}

Resultat commandedep_definition_reductible(TRAVAIL(CommandeDep) commandedep, TRAVAIL(Flux) reductible)
{
        /* Assigne le texte � placer en cas de r�ductibilit� de la d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(reductible,CHAMP_TRAVAIL(commandedep, reductible)));
        return RESULTAT_OK;
}

Resultat commandedep_lecture_localisationfichier(TRAVAIL(CommandeDep) commandedep, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande dep dans le source.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandedep, localisation);
        return RESULTAT_OK;
}

Resultat commandedep_lecture_pertinence(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) pertinence)
{
        /* Assigne une pertinence � une commande de d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        T_R(pertinence)=CHAMP_TRAVAIL(commandedep, pertinence);
        return RESULTAT_OK;
}

Resultat commandedep_lecture_destination(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) destination)
{
        /* Assigne une destination � la commande de d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        T_R(destination)=CHAMP_TRAVAIL(commandedep, destination);
        return RESULTAT_OK;
}

Resultat commandedep_lecture_irreductible(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) irreductible)
{
        /* Assigne le texte � placer en cas d'irr�ductibilit� de la d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        T_R(irreductible)=CHAMP_TRAVAIL(commandedep, irreductible);
        return RESULTAT_OK;
}

Resultat commandedep_lecture_reductible(TRAVAIL(CommandeDep) commandedep, REFERENCE(Flux) reductible)
{
        /* Assigne le texte � placer en cas de r�ductibilit� de la d�pendance.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        T_R(reductible)=CHAMP_TRAVAIL(commandedep, reductible);
        return RESULTAT_OK;
}

Resultat commandedep_parcours(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandedep, pertinence)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedep, pertinence),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedep, destination),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedep, irreductible),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedep, reductible),action,general));
        return RESULTAT_OK;
}

Resultat commandedep_copie(TRAVAIL(CommandeDep) commandedep, TRAVAIL(CommandeDep) copie)
{
        /* Cr�e une copie de la commande dep.
         * Renvoie RESULTAT_ERREUR si commandedep est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedep_destruction(copie));
        SECURISE(commandedep_initialisation(copie));
        SECURISE(commandedep_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandedep, localisation)));
        SECURISE(commandedep_definition_pertinence(copie,CHAMP_TRAVAIL(commandedep, pertinence)));
        SECURISE(commandedep_definition_destination(copie,CHAMP_TRAVAIL(commandedep, destination)));
        SECURISE(commandedep_definition_irreductible(copie,CHAMP_TRAVAIL(commandedep, irreductible)));
        SECURISE(commandedep_definition_reductible(copie,CHAMP_TRAVAIL(commandedep, reductible)));
        return RESULTAT_OK;
}

Resultat commandedep_destruction(TRAVAIL(CommandeDep) commandedep)
{
        /* D�truit une commande de d�pendance.
         */
        if(S_T(commandedep)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandedep, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedep, pertinence)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedep, destination)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedep, irreductible)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedep, reductible)));
        free(S_T(commandedep));
        S_T(commandedep)=NULL;
        return RESULTAT_OK;
}
