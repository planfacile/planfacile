/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEEXTREF__
#define __COMMANDEEXTREF__

#include <src/global/global.h>

typedef struct commandeextref CONTENEUR(CommandeExtRef);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeextref
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la r�f�rence dans le source.
        STOCKAGE(Flux) indice;
        //Flux indiquant quelle r�f�rence dans la liste des
        //des r�f�rences externes on doit choisir pour placer
        //la r�f�rence.
        //S'il est non indiqu�, on g�n�re une erreur.
        //Ce flux est de type FLUX_INDICE.
        STOCKAGE(Flux) format;
        //Format facultatif de la r�f�rence.
        //S'il est absent (NULL), celui pr�cis� dans la commande
        //#reference est pris en compte.
        //Ce flux est de type FLUX_REFERENCE_FORMAT.
};
/* Commande d�sign�e pour indiquer la pr�sence d'une r�f�rence � une autre id�e
 * depuis une id�e manquante.
 */

Resultat commandeextref_initialisation(TRAVAIL(CommandeExtRef) commandeextref);
/* Cr�e une commande extref vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeextref_definition_localisationfichier(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande extref dans le code source.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeextref_definition_indice(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(Flux) indice);
/* Assigne un indice � la commande extref.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeextref_definition_format(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(Flux) format);
/* Assigne un format � la commande extref.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeextref_lecture_localisationfichier(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande extref dans le code source.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 */

Resultat commandeextref_lecture_indice(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(Flux) indice);
/* Lit un indice � la commande extref.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 */

Resultat commandeextref_lecture_format(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(Flux) format);
/* Lit un format � la commande extref.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
 */

Resultat commandeextref_parcours(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeextref_copie(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(CommandeExtRef) copie);
/* Cr�e une copie de la commande extref.
 * Renvoie RESULTAT_ERREUR si commandeextref est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeextref_destruction(TRAVAIL(CommandeExtRef) commandeextref);
/* D�truit une commande extref.
 */

#endif
