/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEMESSAGE__
#define __COMMANDEMESSAGE__

#include <src/global/global.h>

typedef struct commandemessage CONTENEUR(CommandeMessage);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandemessage
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du format de message dans le source.
        STOCKAGE(Flux) message;
        //Format du message.
        //Ce flux est de type FLUX_MESSAGE.
};
/* Commande d�sign�e pour recevoir le format d'un message du compilateur
 * dans le code produit.
 */

Resultat commandemessage_initialisation(TRAVAIL(CommandeMessage) commandemessage);
/* Cr�e une commande de message vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemessage_definition_localisationfichier(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande message.
 * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemessage_definition_message(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(Flux) message);
/* Assigne un message � une commande de message.
 * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemessage_lecture_localisationfichier(TRAVAIL(CommandeMessage) commandemessage, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande message.
 * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
 */

Resultat commandemessage_lecture_message(TRAVAIL(CommandeMessage) commandemessage, REFERENCE(Flux) message);
/* Lit un message � une commande de message.
 * Renvoie RESULTAT_ERREUR si commandemessage est NULL.
 */

Resultat commandemessage_parcours(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandemessage_copie(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(CommandeMessage) copie);
/* Cr�e une copie de la commande message.
 * Renvoie RESULTAT_ERREUR si commandemessage est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandemessage_destruction(TRAVAIL(CommandeMessage) commandemessage);
/* D�truit une commande de message.
 */

#endif
