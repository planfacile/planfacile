/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEHEAD__
#define __COMMANDEHEAD__

#include <src/global/global.h>

typedef struct commandehead CONTENEUR(CommandeHead);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandehead
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande d'ent�te dans le source.
        STOCKAGE(Flux) entete;
        //Texte de l'ent�te du document.
        //Ce flux est de type FLUX_DOCUMENT.
};
/* Commande d�sign�e pour recevoir l'ent�te du document � g�n�rer.
 */

Resultat commandehead_initialisation(TRAVAIL(CommandeHead) commandehead);
/* Cr�e une commande d'ent�te de document vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandehead_definition_localisationfichier(TRAVAIL(CommandeHead) commandehead, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande head.
 * Renvoie RESULTAT_ERREUR si commandehead est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandehead_definition_entete(TRAVAIL(CommandeHead) commandehead, TRAVAIL(Flux) entete);
/* Assigne une ent�te � une commande head.
 * Renvoie RESULTAT_ERREUR si commandehead est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandehead_lecture_localisationfichier(TRAVAIL(CommandeHead) commandehead, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande head.
 * Renvoie RESULTAT_ERREUR si commandehead est NULL.
 */

Resultat commandehead_lecture_entete(TRAVAIL(CommandeHead) commandehead, REFERENCE(Flux) entete);
/* Lit une ent�te � une commande head.
 * Renvoie RESULTAT_ERREUR si commandehead est NULL.
 */

Resultat commandehead_parcours(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandehead_copie(TRAVAIL(CommandeHead) commandehead, TRAVAIL(CommandeHead) copie);
/* Cr�e une copie de la commande head.
 * Renvoie RESULTAT_ERREUR si commandehead est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandehead_destruction(TRAVAIL(CommandeHead) commandehead);
/* D�truit une commande d'ent�te de document.
 */

#endif
