/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandedepref.h"

Resultat commandedepref_initialisation(TRAVAIL(CommandeDepRef) commandedepref)
{
        /* Initialise une commande depref vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandedepref)=NOUVEAU(CommandeDepRef))!=NULL, RESULTAT_ERREUR);

        CHAMP(commandedepref, localisation)=NULL;
        CHAMP(commandedepref, format)=NULL;
        return RESULTAT_OK;
}

Resultat commandedepref_definition_localisationfichier(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande depref
         * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandedepref, localisation)));
        return RESULTAT_OK;
}

Resultat commandedepref_definition_format(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(Flux) format)
{
        /* Assigne un format � une commande depref.
         * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        if(S_T(format)==NULL)
                CHAMP(commandedepref, format)=NULL;
        else
        {
                SECURISE(flux_copie(format,CHAMP_TRAVAIL(commandedepref, format)));
        }
        return RESULTAT_OK;
}

Resultat commandedepref_lecture_localisationfichier(TRAVAIL(CommandeDepRef) commandedepref, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande depref
         * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandedepref, localisation);
        return RESULTAT_OK;
}

Resultat commandedepref_lecture_format(TRAVAIL(CommandeDepRef) commandedepref, REFERENCE(Flux) format)
{
        /* Assigne un format � une commande depref.
         * Renvoie RESULTAT_ERREUR si commandedepref est NULL.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        T_R(format)=CHAMP_TRAVAIL(commandedepref, format);
        return RESULTAT_OK;
}

Resultat commandedepref_parcours(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{        
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandedepref, format)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandedepref, format),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandedepref_copie(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(CommandeDepRef) copie)
{
        /* Cr�e une copie de la commande depref.
         * Renvoie RESULTAT_ERREUR si commandedepref est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedepref_destruction(copie));
        SECURISE(commandedepref_initialisation(copie));
        SECURISE(commandedepref_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandedepref, localisation)));
        SECURISE(commandedepref_definition_format(copie,CHAMP_TRAVAIL(commandedepref, format)));
        return RESULTAT_OK;
}

Resultat commandedepref_destruction(TRAVAIL(CommandeDepRef) commandedepref)
{
        /* D�truit une commande depref.
         */
        if(S_T(commandedepref)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandedepref, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandedepref, format)));
        free(S_T(commandedepref));
        S_T(commandedepref)=NULL;
        return RESULTAT_OK;
}
