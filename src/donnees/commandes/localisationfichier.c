/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "localisationfichier.h"


Resultat localisationfichier_initialisation(TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Cr�e une localisation de fichier vide.
         */
        S_T(localisationfichier)=NULL;
        return RESULTAT_OK;
}

static Resultat descriptionfichier_destruction(TRAVAIL(DescriptionFichier) descriptionfichier)
{
        /* D�truit une description de fichier.
         */
        if(S_T(descriptionfichier)==NULL)
                return RESULTAT_OK;
        if(--CHAMP(descriptionfichier, references)==0)
        {
                TRAVAIL_SCALAIRE(Booleen) suppressionnomabsolu;
                suppressionnomabsolu=((CHAMP(descriptionfichier, nom)!=CHAMP(descriptionfichier, nomabsolu))&&(CHAMP(descriptionfichier, nomabsolu)!=NULL));
                free(CHAMP(descriptionfichier, nom));
                if(suppressionnomabsolu==T_S_(VRAI))
                        free(CHAMP(descriptionfichier, nomabsolu));
                SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(descriptionfichier, inclusion)));
                free(S_T(descriptionfichier));
                S_T(descriptionfichier)=NULL;
        }
        return RESULTAT_OK;
}

Resultat localisationfichier_destruction(TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* D�truit une localisation de fichier.
         */
        if(S_T(localisationfichier)==NULL)
                return RESULTAT_OK;
        SECURISE(descriptionfichier_destruction(CHAMP_TRAVAIL(localisationfichier, fichier)));
        free(S_T(localisationfichier));
        S_T(localisationfichier)=NULL;
        return RESULTAT_OK;
}

Resultat localisationfichier_lecture_nom(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(NomFichier) nom)
{
        /* Lit le nom du fichier.
         * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        T_R_(nom)=T_S_(CHAMP(CHAMP_TRAVAIL(localisationfichier, fichier), nom));
        return RESULTAT_OK;
}

Resultat localisationfichier_lecture_nomabsolu(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(NomFichier) nomabsolu)
{
        /* Lit le nom absolu du fichier.
         * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        T_R_(nomabsolu)=T_S_(CHAMP(CHAMP_TRAVAIL(localisationfichier, fichier), nomabsolu));
        return RESULTAT_OK;
}

Resultat localisationfichier_lecture_descripteur(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(DescripteurFichier) descripteur)
{
        /* Lit le descripteur du dernier fichier inclus.
         * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        T_R_(descripteur)=T_S_(CHAMP(CHAMP_TRAVAIL(localisationfichier, fichier), descripteur));
        return RESULTAT_OK;
}

Resultat localisationfichier_lecture_position(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(PositionFichier) position)
{
        /* Lit la position dans le dernier fichier inclus.
         * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        T_R_(position)=T_S_(CHAMP(localisationfichier, position));
        return RESULTAT_OK;
}

Resultat localisationfichier_lecture_inclusion(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE(LocalisationFichier) inclusion)
{
        /* Renvoie la position du fichier parent dans l'ordre d'inclusion.
         * Renvoie RESULTAT_ERREUR si localisationfichier est NULL.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        T_R(inclusion)=CHAMP_TRAVAIL(CHAMP_TRAVAIL(localisationfichier, fichier), inclusion);
        return RESULTAT_OK;
}

Resultat localisationfichier_modification(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(PositionFichier) position)
{
        /* Modifie la position dans le fichier courant.
         * Retourne RESULTAT_ERREUR si localisationfichier est NULL.
         * Retourne RESULTAT_ERREUR_DOMAINE si position est n�gatif ou nul.
         */
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);
        ASSERTION(position>T_S_(0), RESULTAT_ERREUR_DOMAINE);

        CHAMP(localisationfichier, position)=S_T_(position);
        return RESULTAT_OK;
}

static Resultat descriptionfichier_ajout(TRAVAIL(DescriptionFichier) descriptionfichier, TRAVAIL_SCALAIRE(NomFichier) nom, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, TRAVAIL_SCALAIRE(DescripteurFichier) descripteur, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Ajoute une description de fichier.
         * Utilis� lors de l'inclusion d'un fichier.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        STOCKAGE(DescriptionFichier) fichierinclus;
        ASSERTION((fichierinclus=NOUVEAU(DescriptionFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(fichierinclus, descripteur)=S_T_(descripteur);
        if((CHAMP_STOCKAGE(fichierinclus, nom)=DUP_CAST(NomFichier, nom))==NULL)
        {
                free(fichierinclus);
                return RESULTAT_ERREUR_MEMOIRE;
        }
        if(nomabsolu==T_S_(NULL))
                CHAMP_STOCKAGE(fichierinclus, nomabsolu)=NULL;
        else if(STRCMP(nom, nomabsolu)==T_S_(0))
                CHAMP_STOCKAGE(fichierinclus, nomabsolu)=CHAMP_STOCKAGE(fichierinclus, nom);
        else if((CHAMP_STOCKAGE(fichierinclus, nomabsolu)=DUP_CAST(NomFichier, nomabsolu))==NULL)
        {
                free(CHAMP_STOCKAGE(fichierinclus, nom));
                free(fichierinclus);
                return RESULTAT_ERREUR_MEMOIRE;
        }
        SECURISE(localisationfichier_initialisation(T_S(CHAMP_STOCKAGE(fichierinclus, inclusion))));
        SECURISE(localisationfichier_copie(localisationfichier, T_S(CHAMP_STOCKAGE(fichierinclus, inclusion))));
        CHAMP_STOCKAGE(fichierinclus, references)=1;
        S_T(descriptionfichier)=fichierinclus;
        return RESULTAT_OK;
}

Resultat localisationfichier_ajout(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(NomFichier) nom, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, TRAVAIL_SCALAIRE(DescripteurFichier) descripteur, TRAVAIL_SCALAIRE(PositionFichier) position)
{
        /* Ajoute une localisation de fichier.
         * Utilis� lors de l'inclusion d'un fichier.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        STOCKAGE(LocalisationFichier) fichierinclus;
        ASSERTION((fichierinclus=NOUVEAU(LocalisationFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(descriptionfichier_ajout(T_S(CHAMP_STOCKAGE(fichierinclus, fichier)), nom, nomabsolu, descripteur, localisationfichier));
        SECURISE(localisationfichier_destruction(localisationfichier));
        CHAMP_STOCKAGE(fichierinclus, position)=S_T_(position);
        S_T(localisationfichier)=fichierinclus;
        return RESULTAT_OK;
}

Resultat localisationfichier_retrait(TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Retire une localisation de fichier.
         * Ne referme pas le fichier d�sign� par le descripteur.
         * Utilis� lors d'un retour de fichier inclu.
         */
        STOCKAGE(LocalisationFichier) localisationobsolete;
        TRAVAIL(LocalisationFichier) reste;
        ASSERTION(S_T(localisationfichier)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_lecture_inclusion(localisationfichier, R_T(reste)));
        localisationobsolete=S_T(localisationfichier);
        SECURISE(localisationfichier_initialisation(localisationfichier));
        SECURISE(localisationfichier_copie(reste, localisationfichier));
        SECURISE(localisationfichier_destruction(T_S(localisationobsolete)));
        return RESULTAT_OK;
}

Resultat localisationfichier_vide(TRAVAIL(LocalisationFichier) localisationfichier, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Indique si la localisationfichier est vide ou non.
         */
        if(S_T(localisationfichier)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

static Resultat descriptionfichier_test_fichier(TRAVAIL(DescriptionFichier) descriptionfichier, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, REFERENCE_SCALAIRE(Booleen) present)
{
        /* Indique si un nom de fichier est pr�sent dans la description
         * de fichier, afin de savoir si on ne r�alise pas une inclusion
         * r�cursive. Le r�sultat est renvoy� dans le bool�en pass� en
         * param�tre.
         */
        T_R_(present)=T_S_(FAUX);
        if(S_T(descriptionfichier)==NULL)
                return RESULTAT_OK;
        if((nomabsolu==T_S_(NULL))||(CHAMP(descriptionfichier, nomabsolu)==NULL))
                return RESULTAT_OK;
        if(STRCMP(nomabsolu, T_S_(CHAMP(descriptionfichier, nomabsolu)))==T_S_(0))
        {
                T_R_(present)=T_S_(VRAI);
                return RESULTAT_OK;
        }
        SECURISE(localisationfichier_test_fichier(CHAMP_TRAVAIL(descriptionfichier, inclusion), nomabsolu, present));
        return RESULTAT_OK;
}

Resultat localisationfichier_test_fichier(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL_SCALAIRE(NomFichier) nomabsolu, REFERENCE_SCALAIRE(Booleen) present)
{
        /* Indique si un nom de fichier est pr�sent dans la localisation
         * de fichier, afin de savoir si on ne r�alise pas une inclusion
         * r�cursive. Le r�sultat est renvoy� dans le bool�en pass� en
         * param�tre.
         */
        T_R_(present)=T_S_(FAUX);
        if(S_T(localisationfichier)==NULL)
                return RESULTAT_OK;
        SECURISE(descriptionfichier_test_fichier(CHAMP_TRAVAIL(localisationfichier, fichier), nomabsolu, present));
        return RESULTAT_OK;
}

Resultat localisationfichier_copie(TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL(LocalisationFichier) copie)
{
        /* Effectue une copie d'une localisation de fichier.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(LocalisationFichier) nouveau;
        if(S_T(localisationfichier)==NULL)
                return RESULTAT_OK;
        ASSERTION((nouveau=NOUVEAU(LocalisationFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(localisationfichier_destruction(copie));
        SECURISE(localisationfichier_initialisation(copie));
        CHAMP_STOCKAGE(nouveau, fichier)=CHAMP(localisationfichier, fichier);
        CHAMP_STOCKAGE(CHAMP_STOCKAGE(nouveau, fichier), references)++;
        CHAMP_STOCKAGE(nouveau, position)=CHAMP(localisationfichier, position);
        S_T(copie)=nouveau;
        return RESULTAT_OK;
}
