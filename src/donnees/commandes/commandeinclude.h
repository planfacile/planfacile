/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEINCLUDE__
#define __COMMANDEINCLUDE__

#include <src/global/global.h>

typedef struct commandeinclude CONTENEUR(CommandeInclude);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeinclude
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'inclusion dans le source.
        STOCKAGE_SCALAIRE(NomFichier) inclusion;
        //Nom du fichier d'inclusion.
};
/* Commande destin�e � recevoir le nom d'un fichier � inclure.
 * Cette commande est a priori inusit�e.
 */

Resultat commandeinclude_initialisation(TRAVAIL(CommandeInclude) commandeinclude);
/* Cr�e une commande d'inclusion vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeinclude_definition_localisationfichier(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande include.
 * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeinclude_definition_inclusion(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL_SCALAIRE(NomFichier) inclusion);
/* Assigne un nom de fichier � une commande d'inclusion.
 * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeinclude_lecture_localisationfichier(TRAVAIL(CommandeInclude) commandeinclude, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande include.
 * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
 */

Resultat commandeinclude_lecture_inclusion(TRAVAIL(CommandeInclude) commandeinclude, REFERENCE_SCALAIRE(NomFichier) inclusion);
/* Lit un nom de fichier � une commande d'inclusion.
 * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
 */

Resultat commandeinclude_parcours(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeinclude_copie(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(CommandeInclude) copie);
/* Cr�e une copie de la commande include.
 * Renvoie RESULTAT_ERREUR si commandeinclude est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeinclude_destruction(TRAVAIL(CommandeInclude) commandeinclude);
/* D�truit une commande d'inclusion.
 */

#endif
