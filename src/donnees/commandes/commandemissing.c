/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandemissing.h"

Resultat commandemissing_initialisation(TRAVAIL(CommandeMissing) commandemissing)
{
        /* Cr�e une commande d'id�e manquante vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandemissing)=NOUVEAU(CommandeMissing))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemissing, localisation)=NULL;
        CHAMP(commandemissing, indice)=NULL;
        CHAMP(commandemissing, reference)=NULL;
        CHAMP(commandemissing, titre)=NULL;
        CHAMP(commandemissing, texte)=NULL;
        return RESULTAT_OK;
}

Resultat commandemissing_definition_localisationfichier(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande missing.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandemissing, localisation)));
        return RESULTAT_OK;
}

Resultat commandemissing_definition_indice(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) indice)
{
        /* Assigne un indice � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        if(S_T(indice)==NULL)
                CHAMP(commandemissing, indice)=NULL;
        else
        {
                SECURISE(flux_copie(indice,CHAMP_TRAVAIL(commandemissing, indice)));
        }
        return RESULTAT_OK;
}

Resultat commandemissing_definition_reference(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) reference)
{
        /* Assigne un nom de r�f�rence � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(reference,CHAMP_TRAVAIL(commandemissing, reference)));
        return RESULTAT_OK;
}

Resultat commandemissing_definition_titre(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) titre)
{
        /* Assigne un titre � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(titre,CHAMP_TRAVAIL(commandemissing, titre)));
        return RESULTAT_OK;
}

Resultat commandemissing_definition_texte(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) texte)
{
        /* Assigne un texte � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(texte,CHAMP_TRAVAIL(commandemissing, texte)));
        return RESULTAT_OK;
}

Resultat commandemissing_lecture_localisationfichier(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande missing.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandemissing, localisation);
        return RESULTAT_OK;
}

Resultat commandemissing_lecture_indice(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) indice)
{
        /* Lit un indice � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        T_R(indice)=CHAMP_TRAVAIL(commandemissing, indice);
        return RESULTAT_OK;
}

Resultat commandemissing_lecture_reference(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) reference)
{
        /* Lit un nom de r�f�rence � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        T_R(reference)=CHAMP_TRAVAIL(commandemissing, reference);
        return RESULTAT_OK;
}

Resultat commandemissing_lecture_titre(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) titre)
{
        /* Lit un titre � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        T_R(titre)=CHAMP_TRAVAIL(commandemissing, titre);
        return RESULTAT_OK;
}

Resultat commandemissing_lecture_texte(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) texte)
{
        /* Lit un texte � une id�e manquante.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        T_R(texte)=CHAMP_TRAVAIL(commandemissing, texte);
        return RESULTAT_OK;
}

Resultat commandemissing_parcours(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandemissing, indice)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandemissing, indice),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandemissing, reference),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandemissing, titre),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandemissing, texte),action,general));
        return RESULTAT_OK;
}

Resultat commandemissing_copie(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(CommandeMissing) copie)
{
        /* Cr�e une copie de la commande missing.
         * Renvoie RESULTAT_ERREUR si commandemissing est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemissing_destruction(copie));
        SECURISE(commandemissing_initialisation(copie));
        SECURISE(commandemissing_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandemissing, localisation)));
        SECURISE(commandemissing_definition_indice(copie,CHAMP_TRAVAIL(commandemissing, indice)));
        SECURISE(commandemissing_definition_reference(copie,CHAMP_TRAVAIL(commandemissing, reference)));
        SECURISE(commandemissing_definition_titre(copie,CHAMP_TRAVAIL(commandemissing, titre)));
        SECURISE(commandemissing_definition_texte(copie,CHAMP_TRAVAIL(commandemissing, texte)));
        return RESULTAT_OK;
}

Resultat commandemissing_destruction(TRAVAIL(CommandeMissing) commandemissing)
{
        /* D�truit une id�e manquante.
         */ 
        if(S_T(commandemissing)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandemissing, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandemissing, indice)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandemissing, reference)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandemissing, titre)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandemissing, texte)));
        free(S_T(commandemissing));
        S_T(commandemissing)=NULL;
        return RESULTAT_OK;
}

