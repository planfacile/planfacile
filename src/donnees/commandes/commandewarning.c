/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandewarning.h"

Resultat commandewarning_initialisation(TRAVAIL(CommandeWarning) commandewarning)
{
        /* Cr�e une commande d'avertissement vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandewarning)=NOUVEAU(CommandeWarning))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandewarning, localisation)=NULL;
        CHAMP(commandewarning, avertissement)=NULL;
        return RESULTAT_OK;
}

Resultat commandewarning_definition_localisationfichier(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande warning dans le source.
         * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandewarning, localisation)));
        return RESULTAT_OK;
}

Resultat commandewarning_definition_avertissement(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(Flux) avertissement)
{
        /* Assigne un flux de texte d'avertissement.
         * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(avertissement,CHAMP_TRAVAIL(commandewarning, avertissement)));
        return RESULTAT_OK;
}

Resultat commandewarning_lecture_localisationfichier(TRAVAIL(CommandeWarning) commandewarning, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande warning dans le source.
         * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandewarning, localisation);
        return RESULTAT_OK;
}

Resultat commandewarning_lecture_avertissement(TRAVAIL(CommandeWarning) commandewarning, REFERENCE(Flux) avertissement)
{
        /* Lit un flux de texte d'avertissement.
         * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        T_R(avertissement)=CHAMP_TRAVAIL(commandewarning, avertissement);
        return RESULTAT_OK;
}

Resultat commandewarning_parcours(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandewarning, avertissement),action,general));
        return RESULTAT_OK;
}

Resultat commandewarning_copie(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(CommandeWarning) copie)
{
        /* Cr�e une copie de la commande d'avertissement.
         * Renvoie RESULTAT_ERREUR si commandewarning est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandewarning_destruction(copie));
        SECURISE(commandewarning_initialisation(copie));
        SECURISE(commandewarning_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandewarning, localisation)));
        SECURISE(commandewarning_definition_avertissement(copie,CHAMP_TRAVAIL(commandewarning, avertissement)));
        return RESULTAT_OK;
}

Resultat commandewarning_destruction(TRAVAIL(CommandeWarning) commandewarning)
{
        /* D�truit une commande d'avertissement.
         */
        if(S_T(commandewarning)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandewarning, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandewarning, avertissement)));
        free(S_T(commandewarning));
        S_T(commandewarning)=NULL;
        return RESULTAT_OK;
}

