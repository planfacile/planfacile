/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandetexte.h"

Resultat commandetexte_initialisation(TRAVAIL(CommandeTexte) commandetexte)
{
        /* Cr�e une commande de texte vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandetexte)=NOUVEAU(CommandeTexte))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandetexte, localisation)=NULL;
        CHAMP(commandetexte, texte)=NULL;
        return RESULTAT_OK;
}

Resultat commandetexte_definition_localisationfichier(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande texte.
         * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandetexte, localisation)));
        return RESULTAT_OK;
}

Resultat commandetexte_definition_texte(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL_SCALAIRE(Chaine) texte)
{
        /* Assigne un nom de fichier � une commande de texte.
         * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandetexte, texte)!=NULL)
        {
                free(CHAMP(commandetexte, texte));
                CHAMP(commandetexte, texte)=NULL;
        }
        ASSERTION((CHAMP(commandetexte, texte)=DUP_CAST(Chaine, texte))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat commandetexte_lecture_localisationfichier(TRAVAIL(CommandeTexte) commandetexte, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande texte.
         * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
         */ 
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandetexte, localisation);
        return RESULTAT_OK;
}

Resultat commandetexte_lecture_texte(TRAVAIL(CommandeTexte) commandetexte, REFERENCE_SCALAIRE(Chaine) texte)
{
        /* Lit un nom de fichier � une commande de texte.
         * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
         */ 
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        T_R_(texte)=T_S_(CHAMP(commandetexte, texte));
        return RESULTAT_OK;
}

Resultat commandetexte_parcours(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandetexte_copie(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(CommandeTexte) copie)
{
        /* Cr�e une copie de la commande texte.
         * Renvoie RESULTAT_ERREUR si commandetexte est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetexte_destruction(copie));
        SECURISE(commandetexte_initialisation(copie));
        SECURISE(commandetexte_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandetexte, localisation)));
        SECURISE(commandetexte_definition_texte(copie,T_S_(CHAMP(commandetexte, texte))));
        return RESULTAT_OK;
}

Resultat commandetexte_destruction(TRAVAIL(CommandeTexte) commandetexte)
{
        /* D�truit une commande texte.
         */ 
        if(S_T(commandetexte)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandetexte, localisation)));
        if(CHAMP(commandetexte, texte)!=NULL)
        {
                free(CHAMP(commandetexte, texte));
                CHAMP(commandetexte, texte)=NULL;
        }
        free(S_T(commandetexte));
        S_T(commandetexte)=NULL;
        return RESULTAT_OK;
}

