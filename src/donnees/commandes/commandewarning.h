/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEWARNING__
#define __COMMANDEWARNING__

#include <src/global/global.h>

typedef struct commandewarning CONTENEUR(CommandeWarning);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandewarning
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'avertissement dans le source.
        STOCKAGE(Flux) avertissement;
        //Flux de texte d'avertissement.
        //Ce flux est de type FLUX_COMPILATEUR.
};
/* Commande destin�e � recevoir les messages d'avertissement �crits par l'utilisateur.
 */

Resultat commandewarning_initialisation(TRAVAIL(CommandeWarning) commandewarning);
/* Cr�e une commande d'avertissement vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandewarning_definition_localisationfichier(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande warning.
 * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandewarning_definition_avertissement(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(Flux) avertissement);
/* Assigne un avertissement � la commande d'avertissement.
 * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandewarning_lecture_localisationfichier(TRAVAIL(CommandeWarning) commandewarning, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande warning.
 * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
 */

Resultat commandewarning_lecture_avertissement(TRAVAIL(CommandeWarning) commandewarning, REFERENCE(Flux) avertissement);
/* Lit un avertissement � la commande d'avertissement.
 * Renvoie RESULTAT_ERREUR si commandewarning est NULL.
 */

Resultat commandewarning_parcours(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandewarning_copie(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(CommandeWarning) copie);
/* Cr�e une copie de la commande warning.
 * Renvoie RESULTAT_ERREUR si commandewarning est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandewarning_destruction(TRAVAIL(CommandeWarning) commandewarning);
/* D�truit une commande d'avertissement.
 */

#endif
