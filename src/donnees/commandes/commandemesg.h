/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEMESG__
#define __COMMANDEMESG__

#include <src/global/global.h>

typedef struct commandemesg CONTENEUR(CommandeMesg);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandemesg
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande de remplacement dans le source.
};
/* Commande d�sign�e pour indiquer o� remplacer les messages du compilateur
 * dans un format de message.
 */

Resultat commandemesg_initialisation(TRAVAIL(CommandeMesg) commandemesg);
/* Cr�e une commande mesg vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemesg_definition_localisationfichier(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande mesg.
 * Renvoie RESULTAT_ERREUR si commandemesg est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemesg_lecture_localisationfichier(TRAVAIL(CommandeMesg) commandemesg, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande mesg.
 * Renvoie RESULTAT_ERREUR si commandemesg est NULL.
 */

Resultat commandemesg_parcours(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandemesg_copie(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(CommandeMesg) copie);
/* Cr�e une copie de la commande mesg.
 * Renvoie RESULTAT_ERREUR si commandemesg est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandemesg_destruction(TRAVAIL(CommandeMesg) commandemesg);
/* D�truit une commande mesg.
 */

#endif
