/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEMISSING__
#define __COMMANDEMISSING__

#include <src/global/global.h>

typedef struct commandemissing CONTENEUR(CommandeMissing);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandemissing
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'id�e manquante dans le source.
        STOCKAGE(Flux) indice;
        //Indice de l'id�e manquante.
        //Si le flux est manquant, cela correspondra �
        //la r�solution d'une id�e manquante par d�faut.
        //Ce flux est de type FLUX_INDICE.
        STOCKAGE(Flux) reference;
        //Nom de r�f�rence de l'id�e manquante.
        //Ce flux est de type FLUX_REFERENCE_AUTOMATIQUE.
        STOCKAGE(Flux) titre;
        //Titre de l'id�e manquante.
        //Ce flux est de type FLUX_TITRE_AUTOMATIQUE.
        STOCKAGE(Flux) texte;
        //Texte associ� � l'id�e manquante. (tout �a pour �a !)
        //Ce flux est de type FLUX_TEXTE_MANQUANTE.
};
/* Commande d�sign�e pour enregistrer une id�e manquante du document.
 */

Resultat commandemissing_initialisation(TRAVAIL(CommandeMissing) commandemissing);
/* Cr�e une commande d'id�e manquante vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_definition_localisationfichier(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande missing.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_definition_indice(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) indice);
/* Assigne un indice � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_definition_reference(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) reference);
/* Assigne un nom de r�f�rence � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_definition_titre(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) titre);
/* Assigne un titre � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_definition_texte(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(Flux) texte);
/* Assigne un texte � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandemissing_lecture_localisationfichier(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande missing.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 */

Resultat commandemissing_lecture_indice(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) indice);
/* Lit un indice � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 */

Resultat commandemissing_lecture_reference(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) reference);
/* Lit un nom de r�f�rence � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 */

Resultat commandemissing_lecture_titre(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) titre);
/* Lit un titre � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 */

Resultat commandemissing_lecture_texte(TRAVAIL(CommandeMissing) commandemissing, REFERENCE(Flux) texte);
/* Lit un texte � une id�e manquante.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL.
 */

Resultat commandemissing_parcours(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandemissing_copie(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(CommandeMissing) copie);
/* Cr�e une copie de la commande missing.
 * Renvoie RESULTAT_ERREUR si commandemissing est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandemissing_destruction(TRAVAIL(CommandeMissing) commandemissing);
/* D�truit une id�e manquante.
 */

#endif
