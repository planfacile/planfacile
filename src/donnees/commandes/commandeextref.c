/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeextref.h"

Resultat commandeextref_initialisation(TRAVAIL(CommandeExtRef) commandeextref)
{
        /* Cr�e une commande extref vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeextref)=NOUVEAU(CommandeExtRef))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeextref, localisation)=NULL;
        CHAMP(commandeextref, indice)=NULL;
        CHAMP(commandeextref, format)=NULL;
        return RESULTAT_OK;
}

Resultat commandeextref_definition_localisationfichier(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande extref dans le code source.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeextref, localisation)));
        return RESULTAT_OK;
}

Resultat commandeextref_definition_indice(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(Flux) indice)
{
        /* Assigne un indice � la commande extref.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(indice,CHAMP_TRAVAIL(commandeextref, indice)));
        return RESULTAT_OK;
}

Resultat commandeextref_definition_format(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(Flux) format)
{
        /* Assigne un format � la commande extref.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        if(S_T(format)==NULL)
                CHAMP(commandeextref, format)=NULL;
        else
        {
                SECURISE(flux_copie(format,CHAMP_TRAVAIL(commandeextref, format)));
        }
        return RESULTAT_OK;
}

Resultat commandeextref_lecture_localisationfichier(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande extref dans le code source.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeextref, localisation);
        return RESULTAT_OK;
}

Resultat commandeextref_lecture_indice(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(Flux) indice)
{
        /* Lit un indice � la commande extref.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        T_R(indice)=CHAMP_TRAVAIL(commandeextref, indice);
        return RESULTAT_OK;
}

Resultat commandeextref_lecture_format(TRAVAIL(CommandeExtRef) commandeextref, REFERENCE(Flux) format)
{
        /* Lit un format � la commande extref.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        T_R(format)=CHAMP_TRAVAIL(commandeextref, format);
        return RESULTAT_OK;
}

Resultat commandeextref_parcours(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeextref, indice),action,general));
        if(CHAMP(commandeextref, format)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeextref, format),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandeextref_copie(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(CommandeExtRef) copie)
{
        /* Cr�e une copie de la commande extref.
         * Renvoie RESULTAT_ERREUR si commandeextref est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeextref_destruction(copie));
        SECURISE(commandeextref_initialisation(copie));
        SECURISE(commandeextref_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeextref, localisation)));
        SECURISE(commandeextref_definition_indice(copie,CHAMP_TRAVAIL(commandeextref, indice)));
        if(CHAMP(commandeextref, format)!=NULL)
        {
                SECURISE(commandeextref_definition_format(copie,CHAMP_TRAVAIL(commandeextref, format)));
        }
        return RESULTAT_OK;
}

Resultat commandeextref_destruction(TRAVAIL(CommandeExtRef) commandeextref)
{
        /* D�truit une commande extref.
         */
        if(S_T(commandeextref)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeextref, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeextref, indice)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeextref, format)));
        free(S_T(commandeextref));
        S_T(commandeextref)=NULL;
        return RESULTAT_OK;
}
