/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEREFERENCE__
#define __COMMANDEREFERENCE__

#include <src/global/global.h>

typedef struct commandereference CONTENEUR(CommandeReference);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandereference
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du format de r�f�rence dans le source.
        STOCKAGE(Flux) niveau;
        //Flux indiquant le niveau o� le format s'applique.
        //Si ce flux vaut NULL, ce format sera le format
        //g�n�rique.
        //Ce flux est de type FLUX_NIVEAU.
        STOCKAGE(Flux) format;
        //Format de r�f�rence.
        //Ce flux est de type FLUX_REFERENCE_FORMAT.
};
/* Commande d�sign�e pour indiquer comment formatter les r�f�rences
 * g�n�r�es dans le document.
 */

Resultat commandereference_initialisation(TRAVAIL(CommandeReference) commandereference);
/* Cr�e une commande de format de r�f�rence vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandereference_definition_localisationfichier(TRAVAIL(CommandeReference) commandereference, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande reference.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandereference_definition_niveau(TRAVAIL(CommandeReference) commandereference, TRAVAIL(Flux) niveau);
/* Assigne un niveau d'action du format de r�f�rence.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandereference_definition_format(TRAVAIL(CommandeReference) commandereference, TRAVAIL(Flux) format);
/* Assigne un format de r�f�rence.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandereference_lecture_localisationfichier(TRAVAIL(CommandeReference) commandereference, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande reference.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 */

Resultat commandereference_lecture_niveau(TRAVAIL(CommandeReference) commandereference, REFERENCE(Flux) niveau);
/* Lit un niveau d'action du format de r�f�rence.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 */

Resultat commandereference_lecture_format(TRAVAIL(CommandeReference) commandereference, REFERENCE(Flux) format);
/* Lit un format de r�f�rence.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL.
 */

Resultat commandereference_parcours(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandereference_copie(TRAVAIL(CommandeReference) commandereference, TRAVAIL(CommandeReference) copie);
/* Cr�e une copie de la commande reference.
 * Renvoie RESULTAT_ERREUR si commandereference est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandereference_destruction(TRAVAIL(CommandeReference) commandereference);
/* D�truit une commande de format de r�f�rence.
 */

#endif
