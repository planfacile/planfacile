/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEERROR__
#define __COMMANDEERROR__

#include <src/global/global.h>

typedef struct commandeerror CONTENEUR(CommandeError);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeerror
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'erreur dans le source.
        STOCKAGE(Flux) erreur;
        //Flux de texte d'erreur.
        //Ce flux est de type FLUX_COMPILATEUR.
};
/* Commande destin�e � recevoir les messages d'erreur �crits par l'utilisateur.
 */

Resultat commandeerror_initialisation(TRAVAIL(CommandeError) commandeerror);
/* Cr�e une commande d'erreur vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeerror_definition_localisationfichier(TRAVAIL(CommandeError) commandeerror, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande error dans le source.
 * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeerror_definition_erreur(TRAVAIL(CommandeError) commandeerror, TRAVAIL(Flux) erreur);
/* Assigne un flux de texte d'erreur.
 * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeerror_lecture_localisationfichier(TRAVAIL(CommandeError) commandeerror, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande error dans le source.
 * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
 */

Resultat commandeerror_lecture_erreur(TRAVAIL(CommandeError) commandeerror, REFERENCE(Flux) erreur);
/* Lit un flux de texte d'erreur.
 * Renvoie RESULTAT_ERREUR si commandeerror est NULL.
 */

Resultat commandeerror_parcours(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeerror_copie(TRAVAIL(CommandeError) commandeerror, TRAVAIL(CommandeError) copie);
/* Cr�e une copie de la commande d'erreur.
 * Renvoie RESULTAT_ERREUR si commandeerror est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeerror_destruction(TRAVAIL(CommandeError) commandeerror);
/* D�truit une commande d'erreur.
 */

#endif
