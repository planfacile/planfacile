/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEDEFINE__
#define __COMMANDEDEFINE__

#include <src/global/global.h>

typedef struct commandedefine CONTENEUR(CommandeDefine);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandedefine
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la d�finiton de macro dans le source.
        STOCKAGE_SCALAIRE(NomMacro) nom;
        //Nom de la macro.
        STOCKAGE(Flux) definition;
        //Flux de commandes associ� � la macro.
        //Ce flux sera de type FLUX_MACRO_DEFINITION.
        STOCKAGE_SCALAIRE(Taille) parametres;
        //Nombre de param�tres qui devront �tre pass�s � la
        //macro lors de son appel.
        STOCKAGE_SCALAIRE(Booleen) boucle;
        //Ce bool�en sera mis � vrai si le param�tre #0 se trouve dans la
        //d�finition de la macro. Cela signifie que le comportement de la
        //macro sera affect�, pour introduire une boucle.
        STOCKAGE_SCALAIRE(Booleen) appel;
        //Ce bool�en servira dans la r�solution des macros, pour savoir si
        //on a affaire � un appel r�cursif.
        STOCKAGE_SCALAIRE(NiveauHierarchique) niveau;
        //Ce niveau indique l'importance de la d�finition de macro en relation
        //avec les imbrications de commandes #options. Plus le niveau est faible,
        //plus la macro est importante.
};
/* Cette commande est destin�e � recevoir la d�finition d'une macro.
 * Il est � noter que, � l'instar de la commande #option, cette commande
 * ne sera jamais directement incluse dans le flux, mais � part.
 */

Resultat commandedefine_initialisation(TRAVAIL(CommandeDefine) commandedefine);
/* Cr�e une commande define vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_localisationfichier(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position de la commande de d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_nom(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(NomMacro) nom);
/* Assigne un nom � la d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_definition(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(Flux) definition);
/* Assigne une d�finition � la macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_parametres(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(Taille) parametres, TRAVAIL_SCALAIRE(Booleen) boucle);
/* Assigne les caract�ristiques de param�tres � une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_appel(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(Booleen) appel);
/* Assigne les caract�ristiques d'appel � une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_definition_niveau(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau);
/* Assigne le niveau d'une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_lecture_localisationfichier(TRAVAIL(CommandeDefine) commandedefine, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position de la commande de d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL.
 */

Resultat commandedefine_lecture_nom(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(NomMacro) nom);
/* Assigne un nom � la d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 */

Resultat commandedefine_lecture_definition(TRAVAIL(CommandeDefine) commandedefine, REFERENCE(Flux) definition);
/* Assigne une d�finition � la macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 */

Resultat commandedefine_lecture_parametres(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(Taille) parametres, REFERENCE_SCALAIRE(Booleen) boucle);
/* Assigne les caract�ristiques de param�tres � une d�finition de macro.
 * Dans cette commande, placer un des argument de r�sultat � NULL revient
 * � ne pas demander l'information correspondante.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 */

Resultat commandedefine_lecture_appel(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(Booleen) appel);
/* Lit les caract�ristiques d'appel � une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_lecture_niveau(TRAVAIL(CommandeDefine) commandedefine, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Lit le niveau d'une d�finition de macro.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL;
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandedefine_parcours(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandedefine_copie(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(CommandeDefine) copie);
/* Cr�e une copie de la commande define.
 * Renvoie RESULTAT_ERREUR si commandedefine est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si uen allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandedefine_destruction(TRAVAIL(CommandeDefine) commandedefine);
/* D�truit une commande de d�finition de macro.
 */

#endif
