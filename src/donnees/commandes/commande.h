/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDE__
#define __COMMANDE__

#include <src/global/global.h>

typedef struct commande CONTENEUR(Commande);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>

#include <src/donnees/commandes/actioncommande.h>

#include <src/donnees/commandes/commandecommentaire.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandedep.h>
#include <src/donnees/commandes/commandedepref.h>
#include <src/donnees/commandes/commandeechappement.h>
#include <src/donnees/commandes/commandeerror.h>
#include <src/donnees/commandes/commandeextref.h>
#include <src/donnees/commandes/commandeextrefs.h>
#include <src/donnees/commandes/commandefoot.h>
#include <src/donnees/commandes/commandegeneric.h>
#include <src/donnees/commandes/commandehead.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/commandeinclude.h>
#include <src/donnees/commandes/commandeindex.h>
#include <src/donnees/commandes/commandemacro.h>
#include <src/donnees/commandes/commandemesg.h>
#include <src/donnees/commandes/commandemessage.h>
#include <src/donnees/commandes/commandemissing.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/donnees/commandes/commandeoptions.h>
#include <src/donnees/commandes/commandeparametre.h>
#include <src/donnees/commandes/commandereference.h>
#include <src/donnees/commandes/commanderef.h>
#include <src/donnees/commandes/commandesec.h>
#include <src/donnees/commandes/commandesection.h>
#include <src/donnees/commandes/commandestandard.h>
#include <src/donnees/commandes/commandestart.h>
#include <src/donnees/commandes/commandetexte.h>
#include <src/donnees/commandes/commandetitle.h>
#include <src/donnees/commandes/commandetxt.h>
#include <src/donnees/commandes/commandewarning.h>

typedef enum {
        COMMANDE_VIDE,
        //Commande vide, sans type. (Inusit�)
        COMMANDE_COMMENTAIRE,
        //Repr�sente un commentaire.
        //Commande #comment.
        COMMANDE_ECHAPPEMENT,
        //Repr�sente un caract�re d'�chappement.
        //Commandes ##, #{, #}, #<spc> #<tab> et #<rc>.
        COMMANDE_TEXTE,
        //Repr�sente une portion de texte.
        //Commande sans nom.
        COMMANDE_WARNING,
        //Repr�sente une commande d'�mission d'avertissement.
        //Commande #warning.
        COMMANDE_ERROR,
        //Repr�sente une commande d'�mission d'erreur.
        //Commande #error.
        COMMANDE_INCLUDE,
        //Repr�sente une commande d'inclusion de fichier.
        //Commande #include.
        COMMANDE_STANDARD,
        //Repr�sente une commande d'inclusion de fichier standard.
        //Commande #standard.
        COMMANDE_OPTION,
        //Repr�sente une d�finition d'option.
        //Commande #option.
        COMMANDE_OPTIONS,
        //Repr�sente une commande de s�lection de flux en fonction
        //d'une option.
        //Commandes #options / #case / #other / #end.
        COMMANDE_DEFINE,
        //Repr�sente une d�finition de macro.
        //Commande #define.
        COMMANDE_PARAMETRE,
        //Repr�sente une commande de remplacement par un param�tre de macro.
        //Commande #<nombre>
        COMMANDE_MACRO,
        //Repr�sente une commande d'appel de macro.
        //Commande dont le nom change pour chaque cas.
        COMMANDE_MESSAGE,
        //Repr�sente la commande de formattage d'un texte en commentaire dans
        //le fichier d'arriv�e.
        //Commande #message.
        COMMANDE_MESG,
        //Repr�sente la commande de remplacement de message dans la commande
        //de message.
        //Commande #mesg.
        COMMANDE_HEAD,
        //Repr�sente la commande incluant du texte en amont du plan.
        //Commande #head.
        COMMANDE_FOOT,
        //Repr�sente la commande incluant du texte en aval du plan.
        //Commande #foot.
        COMMANDE_SECTION,
        //Repr�sente la commande de formattage d'une section.
        //Commande #section.
        COMMANDE_REFERENCE,
        //Repr�sente la commande de formattage d'une r�f�rence.
        //Commande #reference.
        COMMANDE_TITLE,
        //Repr�sente la commande de remplacement par un titre de section,
        //dans une commande de style.
        //Commande #title.
        COMMANDE_REF,
        //Repr�sente la commande de remplacement par une r�f�rence,
        //dans une commande de style.
        //Commande #ref.
        COMMANDE_SEC,
        //Repr�sente la commande de remplacement par un nom de section,
        //dans une commande de style.
        //Commande #sec.
        COMMANDE_TXT,
        //Repr�sente la commande de remplacement par le texte d'une section,
        //dans une commande de style.
        //Commande #txt.
        COMMANDE_START,
        //Repr�sente la commande indiquant le niveau de d�part du plan.
        //Commande #start.
        COMMANDE_IDEA,
        //Repr�sente la commande introduisant une id�e.
        //Commandes #idea / #text / #end.
        COMMANDE_MISSING,
        //Repr�sente la commande introduisant une id�e manquante.
        //Commandes #missing / #text / #end.
        COMMANDE_GENERIC,
        //Repr�sente la commande introduisant une id�e g�n�rique.
        //Commandes #generic / #text / #end.
        COMMANDE_DEP,
        //Repr�sente la commande de d�pendance dans un texte.
        //Commande #dep.
        COMMANDE_DEPREF,
        //Repr�sente la commande de remplacement par une r�f�rence,
        //dans une d�pendance.
        //Commande #depref.
        COMMANDE_EXTREFS,
        //Repr�sente la commande de remplacement par toutes les
        //r�f�rences d'une id�e manquante.
        //Commande #extrefs.
        COMMANDE_EXTREF,
        //Repr�sente la commande de remplacement par une r�f�rence,
        //dans une id�e manquante.
        //Commande #extref.
        COMMANDE_INDEX
        //Repr�sente la commande de remplacement par l'index de l'id�e
        //automatique dans une d�pendance.
} CONTENEUR_SCALAIRE(TypeCommande);

struct commande
{
        STOCKAGE_SCALAIRE(TypeCommande) type;
        //Indique quelle est la commande incluse dans cette structure.
        union
        {
                STOCKAGE(CommandeCommentaire)        commentaire        ;
                STOCKAGE(CommandeDefine)        define                ;
                STOCKAGE(CommandeDep)                dep                ;
                STOCKAGE(CommandeDepRef)        depref                ;
                STOCKAGE(CommandeEchappement)        echappement        ;
                STOCKAGE(CommandeError)                error                ;
                STOCKAGE(CommandeExtRef)        extref                ;
                STOCKAGE(CommandeExtRefs)        extrefs                ;
                STOCKAGE(CommandeFoot)                foot                ;
                STOCKAGE(CommandeGeneric)        generic                ;
                STOCKAGE(CommandeHead)                head                ;
                STOCKAGE(CommandeIdea)                idea                ;
                STOCKAGE(CommandeInclude)        include                ;
                STOCKAGE(CommandeIndex)                index                ;
                STOCKAGE(CommandeMacro)                macro                ;
                STOCKAGE(CommandeMesg)                mesg                ;
                STOCKAGE(CommandeMessage)        message                ;
                STOCKAGE(CommandeMissing)        missing                ;
                STOCKAGE(CommandeOption)        option                ;
                STOCKAGE(CommandeOptions)        options                ;
                STOCKAGE(CommandeParametre)        parametre        ;
                STOCKAGE(CommandeReference)        reference        ;
                STOCKAGE(CommandeRef)                ref                ;
                STOCKAGE(CommandeSec)                sec                ;
                STOCKAGE(CommandeSection)        section                ;
                STOCKAGE(CommandeStandard)        standard        ;
                STOCKAGE(CommandeStart)                start                ;
                STOCKAGE(CommandeTexte)                texte                ;
                STOCKAGE(CommandeTitle)                title                ;
                STOCKAGE(CommandeTxt)                txt                ;
                STOCKAGE(CommandeWarning)        warning                ;
        } commande;//Equivalent � un STOCKAGE_SCALAIRE
        //La commande en elle-m�me.
};
/* Repr�sente une commande incluse dans un flux.
 */

Resultat commande_initialisation(TRAVAIL(Commande) commande);
/* Cr�e une commande vide. (De type COMMANDE_VIDE.)
 * Retourne RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 */

Resultat commande_definition_commentaire        (TRAVAIL(Commande) commande, TRAVAIL(CommandeCommentaire)        commentaire        );
Resultat commande_definition_define                (TRAVAIL(Commande) commande, TRAVAIL(CommandeDefine)                define                );
Resultat commande_definition_dep                (TRAVAIL(Commande) commande, TRAVAIL(CommandeDep)                dep                );
Resultat commande_definition_depref                (TRAVAIL(Commande) commande, TRAVAIL(CommandeDepRef)                depref                );
Resultat commande_definition_echappement        (TRAVAIL(Commande) commande, TRAVAIL(CommandeEchappement)        echappement        );
Resultat commande_definition_error                (TRAVAIL(Commande) commande, TRAVAIL(CommandeError)                error                );
Resultat commande_definition_extref                (TRAVAIL(Commande) commande, TRAVAIL(CommandeExtRef)                extref                );
Resultat commande_definition_extrefs                (TRAVAIL(Commande) commande, TRAVAIL(CommandeExtRefs)                extrefs                );
Resultat commande_definition_foot                (TRAVAIL(Commande) commande, TRAVAIL(CommandeFoot)                foot                );
Resultat commande_definition_generic                (TRAVAIL(Commande) commande, TRAVAIL(CommandeGeneric)                generic                );
Resultat commande_definition_head                (TRAVAIL(Commande) commande, TRAVAIL(CommandeHead)                head                );
Resultat commande_definition_idea                (TRAVAIL(Commande) commande, TRAVAIL(CommandeIdea)                idea                );
Resultat commande_definition_include                (TRAVAIL(Commande) commande, TRAVAIL(CommandeInclude)                include                );
Resultat commande_definition_index                (TRAVAIL(Commande) commande, TRAVAIL(CommandeIndex)                index                );
Resultat commande_definition_macro                (TRAVAIL(Commande) commande, TRAVAIL(CommandeMacro)                macro                );
Resultat commande_definition_mesg                (TRAVAIL(Commande) commande, TRAVAIL(CommandeMesg)                mesg                );
Resultat commande_definition_message                (TRAVAIL(Commande) commande, TRAVAIL(CommandeMessage)                message                );
Resultat commande_definition_missing                (TRAVAIL(Commande) commande, TRAVAIL(CommandeMissing)                missing                );
Resultat commande_definition_option                (TRAVAIL(Commande) commande, TRAVAIL(CommandeOption)                option                );
Resultat commande_definition_options                (TRAVAIL(Commande) commande, TRAVAIL(CommandeOptions)                options                );
Resultat commande_definition_parametre                (TRAVAIL(Commande) commande, TRAVAIL(CommandeParametre)        parametre        );
Resultat commande_definition_reference                (TRAVAIL(Commande) commande, TRAVAIL(CommandeReference)        reference        );
Resultat commande_definition_ref                (TRAVAIL(Commande) commande, TRAVAIL(CommandeRef)                ref                );
Resultat commande_definition_sec                (TRAVAIL(Commande) commande, TRAVAIL(CommandeSec)                sec                );
Resultat commande_definition_section                (TRAVAIL(Commande) commande, TRAVAIL(CommandeSection)                section                );
Resultat commande_definition_standard                (TRAVAIL(Commande) commande, TRAVAIL(CommandeStandard)                standard        );
Resultat commande_definition_start                (TRAVAIL(Commande) commande, TRAVAIL(CommandeStart)                start                );
Resultat commande_definition_texte                (TRAVAIL(Commande) commande, TRAVAIL(CommandeTexte)                texte                );
Resultat commande_definition_title                (TRAVAIL(Commande) commande, TRAVAIL(CommandeTitle)                title                );
Resultat commande_definition_txt                (TRAVAIL(Commande) commande, TRAVAIL(CommandeTxt)                txt                );
Resultat commande_definition_warning                (TRAVAIL(Commande) commande, TRAVAIL(CommandeWarning)                warning                );
/* Ces fonctions servent � encapsuler une commande pr�cise dans une structure de commande g�n�rique.
 * Si l'allocation �choue, RESULTAT_ERREUR_MEMOIRE est renvoy�e, et si commande est NULL, RESULTAT_ERREUR est renvoy�e.
 */

Resultat commande_lecture_type(TRAVAIL(Commande) commande, REFERENCE_SCALAIRE(TypeCommande) type);
/* Lit le type d'une commande.
 * Renvoie RESULTAT_ERREUR si commande est NULL.
 */

Resultat commande_lecture_commentaire        (TRAVAIL(Commande) commande, REFERENCE(CommandeCommentaire)        commentaire        );
Resultat commande_lecture_define        (TRAVAIL(Commande) commande, REFERENCE(CommandeDefine)                define                );
Resultat commande_lecture_dep                (TRAVAIL(Commande) commande, REFERENCE(CommandeDep)                dep                );
Resultat commande_lecture_depref        (TRAVAIL(Commande) commande, REFERENCE(CommandeDepRef)                depref                );
Resultat commande_lecture_echappement        (TRAVAIL(Commande) commande, REFERENCE(CommandeEchappement)        echappement        );
Resultat commande_lecture_error                (TRAVAIL(Commande) commande, REFERENCE(CommandeError)                error                );
Resultat commande_lecture_extref        (TRAVAIL(Commande) commande, REFERENCE(CommandeExtRef)                extref                );
Resultat commande_lecture_extrefs        (TRAVAIL(Commande) commande, REFERENCE(CommandeExtRefs)        extrefs                );
Resultat commande_lecture_foot                (TRAVAIL(Commande) commande, REFERENCE(CommandeFoot)                foot                );
Resultat commande_lecture_generic        (TRAVAIL(Commande) commande, REFERENCE(CommandeGeneric)        generic                );
Resultat commande_lecture_head                (TRAVAIL(Commande) commande, REFERENCE(CommandeHead)                head                );
Resultat commande_lecture_idea                (TRAVAIL(Commande) commande, REFERENCE(CommandeIdea)                idea                );
Resultat commande_lecture_include        (TRAVAIL(Commande) commande, REFERENCE(CommandeInclude)        include                );
Resultat commande_lecture_index                (TRAVAIL(Commande) commande, REFERENCE(CommandeIndex)                index                );
Resultat commande_lecture_macro                (TRAVAIL(Commande) commande, REFERENCE(CommandeMacro)                macro                );
Resultat commande_lecture_mesg                (TRAVAIL(Commande) commande, REFERENCE(CommandeMesg)                mesg                );
Resultat commande_lecture_message        (TRAVAIL(Commande) commande, REFERENCE(CommandeMessage)        message                );
Resultat commande_lecture_missing        (TRAVAIL(Commande) commande, REFERENCE(CommandeMissing)        missing                );
Resultat commande_lecture_option        (TRAVAIL(Commande) commande, REFERENCE(CommandeOption)                option                );
Resultat commande_lecture_options        (TRAVAIL(Commande) commande, REFERENCE(CommandeOptions)        options                );
Resultat commande_lecture_parametre        (TRAVAIL(Commande) commande, REFERENCE(CommandeParametre)        parametre        );
Resultat commande_lecture_reference        (TRAVAIL(Commande) commande, REFERENCE(CommandeReference)        reference        );
Resultat commande_lecture_ref                (TRAVAIL(Commande) commande, REFERENCE(CommandeRef)                ref                );
Resultat commande_lecture_sec                (TRAVAIL(Commande) commande, REFERENCE(CommandeSec)                sec                );
Resultat commande_lecture_section        (TRAVAIL(Commande) commande, REFERENCE(CommandeSection)        section                );
Resultat commande_lecture_standard        (TRAVAIL(Commande) commande, REFERENCE(CommandeStandard)        standard        );
Resultat commande_lecture_start                (TRAVAIL(Commande) commande, REFERENCE(CommandeStart)                start                );
Resultat commande_lecture_texte                (TRAVAIL(Commande) commande, REFERENCE(CommandeTexte)                texte                );
Resultat commande_lecture_title                (TRAVAIL(Commande) commande, REFERENCE(CommandeTitle)                title                );
Resultat commande_lecture_txt                (TRAVAIL(Commande) commande, REFERENCE(CommandeTxt)                txt                );
Resultat commande_lecture_warning        (TRAVAIL(Commande) commande, REFERENCE(CommandeWarning)        warning                );

/* Ces fonctions permettent de r�cup�rer une commande pr�cise depuis une commande g�n�rique.
 * Renvoie RESULTAT_ERREUR si commande est NULL, et RESULTAT_ERREUR_DOMAINE si le type est incorrect.
 */

Resultat commande_encapsulation_commandeoption(TRAVAIL(Commande) commande, TRAVAIL(CommandeOption) commandeoption);
/* Encapsule une commande #option dans une commande g�n�rique.
 * La commande #option n'est pas recopi�e.
 * Renvoie RESULTAT_ERREUR si commandeoption ou commande est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 */

Resultat commande_desencapsulation_commandeoption(TRAVAIL(Commande) commande, COREFERENCE(CommandeOption) commandeoption);
/* D�sencapsule une commande #option depuis une commande g�n�rique.
 * La commande #option n'est pas recopi�e.
 * Renvoie RESULTAT_ERREUR si commandeoption ou commande est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 */

Resultat commande_encapsulation_commandedefine(TRAVAIL(Commande) commande, TRAVAIL(CommandeDefine) commandedefine);
/* Encapsule une commande #define dans une commande g�n�rique.
 * La commande #define n'est pas recopi�e.
 * Renvoie RESULTAT_ERREUR si commandedefine ou commande est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 */

Resultat commande_desencapsulation_commandedefine(TRAVAIL(Commande) commande, COREFERENCE(CommandeDefine) commandedefine);
/* D�sencapsule une commande #define depuis une commande g�n�rique.
 * La commande #define n'est pas recopi�e.
 * Renvoie RESULTAT_ERREUR si commandedefine ou commande est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si le type de la commande est incorrect.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 */

Resultat commande_parcours(TRAVAIL(Commande) commande, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL
 * et RESULTAT_ERREUR_DOMAINE si la commande est de type
 * incorrect.
 * Cette commande est � priori obsol�te. Elle est conserv�e
 * au cas o�. Cependant sa pr�sence ici n'est plus assur�e.
 */

Resultat commande_copie(TRAVAIL(Commande) commande, TRAVAIL(Commande) copie);
/* Cr�e une copie de la commande donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * et RESULTAT_ERREUR_DOMAINE si le type est incorrect.
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat commande_destruction(TRAVAIL(Commande) commande);
/* Cette fonction sert � d�truire une commande, et ce quelle qu'elle soit.
 * Renvoie l'erreur RESULTAT_ERREUR_DOMAINE si le type est incorrect.
 */

#endif
