/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeindex.h"

Resultat commandeindex_initialisation(TRAVAIL(CommandeIndex) commandeindex)
{
        /* Cr�e une commande d'index vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeindex)=NOUVEAU(CommandeIndex))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeindex, localisation)=NULL;
        return RESULTAT_OK;
}

Resultat commandeindex_definition_localisationfichier(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande index dans le source.
         * Renvoie RESULTAT_ERREUR si commandeindex est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeindex, localisation)));
        return RESULTAT_OK;
}

Resultat commandeindex_lecture_localisationfichier(TRAVAIL(CommandeIndex) commandeindex, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande index dans le source.
         * Renvoie RESULTAT_ERREUR si commandeindex est NULL.
         */
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeindex, localisation);
        return RESULTAT_OK;
}

Resultat commandeindex_parcours(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandeindex_copie(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(CommandeIndex) copie)
{
        /* Cr�e une copie de la commande d'index.
         * Renvoie RESULTAT_ERREUR si commandeindex est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeindex_destruction(copie));
        SECURISE(commandeindex_initialisation(copie));
        SECURISE(commandeindex_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeindex, localisation)));
        return RESULTAT_OK;
}

Resultat commandeindex_destruction(TRAVAIL(CommandeIndex) commandeindex)
{
        /* D�truit une commande d'index.
         */
        if(S_T(commandeindex)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeindex, localisation)));
        free(S_T(commandeindex));
        S_T(commandeindex)=NULL;
        return RESULTAT_OK;
}

