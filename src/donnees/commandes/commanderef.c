/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commanderef.h"

Resultat commanderef_initialisation(TRAVAIL(CommandeRef) commanderef)
{
        /* Cr�e une commande ref vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commanderef)=NOUVEAU(CommandeRef))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commanderef, localisation)=NULL;
        return RESULTAT_OK;
}

Resultat commanderef_definition_localisationfichier(TRAVAIL(CommandeRef) commanderef, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande ref dans le source.
         * Renvoie RESULTAT_ERREUR si commanderef est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commanderef, localisation)));
        return RESULTAT_OK;
}

Resultat commanderef_lecture_localisationfichier(TRAVAIL(CommandeRef) commanderef, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande ref dans le source.
         * Renvoie RESULTAT_ERREUR si commanderef est NULL.
         */
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commanderef, localisation);
        return RESULTAT_OK;
}

Resultat commanderef_parcours(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commanderef_copie(TRAVAIL(CommandeRef) commanderef, TRAVAIL(CommandeRef) copie)
{
        /* Cr�e une copie de la commande ref.
         * Renvoie RESULTAT_ERREUR si commanderef est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        SECURISE(commanderef_destruction(copie));
        SECURISE(commanderef_initialisation(copie));
        SECURISE(commanderef_definition_localisationfichier(copie,CHAMP_TRAVAIL(commanderef, localisation)));
        return RESULTAT_OK;
}

Resultat commanderef_destruction(TRAVAIL(CommandeRef) commanderef)
{
        /* D�truit une commande ref.
         */
        if(S_T(commanderef)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commanderef, localisation)));
        free(S_T(commanderef));
        S_T(commanderef)=NULL;
        return RESULTAT_OK;
}

