/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandestart.h"

Resultat commandestart_initialisation(TRAVAIL(CommandeStart) commandestart)
{
        /* Cr�e une commande d'indication de d�part de plan.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandestart)=NOUVEAU(CommandeStart))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandestart, localisation)=NULL;
        CHAMP(commandestart, niveau)=NULL;
        return RESULTAT_OK;
}

Resultat commandestart_definition_localisationfichier(TRAVAIL(CommandeStart) commandestart, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande start.
         * Renvoie RESULTAT_ERREUR si commandestart est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandestart, localisation)));
        return RESULTAT_OK;
}

Resultat commandestart_definition_niveau(TRAVAIL(CommandeStart) commandestart, TRAVAIL(Flux) niveau)
{
        /* Assigne un niveau � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandestart est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(niveau,CHAMP_TRAVAIL(commandestart, niveau)));
        return RESULTAT_OK;
}

Resultat commandestart_lecture_localisationfichier(TRAVAIL(CommandeStart) commandestart, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande start.
         * Renvoie RESULTAT_ERREUR si commandestart est NULL.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandestart, localisation);
        return RESULTAT_OK;
}

Resultat commandestart_lecture_niveau(TRAVAIL(CommandeStart) commandestart, REFERENCE(Flux) niveau)
{
        /* Lit un niveau � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandestart est NULL.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        T_R(niveau)=CHAMP_TRAVAIL(commandestart, niveau);
        return RESULTAT_OK;
}

Resultat commandestart_parcours(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandestart, niveau)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandestart, niveau),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandestart_copie(TRAVAIL(CommandeStart) commandestart, TRAVAIL(CommandeStart) copie)
{
        /* Cr�e une copie de la commande start.
         * Renvoie RESULTAT_ERREUR si commandestart est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandestart_destruction(copie));
        SECURISE(commandestart_initialisation(copie));
        SECURISE(commandestart_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandestart, localisation)));
        if(CHAMP(commandestart, niveau)!=NULL)
                SECURISE(commandestart_definition_niveau(copie,CHAMP_TRAVAIL(commandestart, niveau)));
        return RESULTAT_OK;
}

Resultat commandestart_destruction(TRAVAIL(CommandeStart) commandestart)
{
        /* D�truit une commande start.
         */
        if(S_T(commandestart)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandestart, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandestart, niveau)));
        free(S_T(commandestart));
        S_T(commandestart)=NULL;
        return RESULTAT_OK;
}

