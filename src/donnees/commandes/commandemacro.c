/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandemacro.h"

Resultat commandemacroparametres_initialisation(TRAVAIL(CommandeMacroParametres) commandemacroparametres)
{
        /* Cr�e une structure destin�e � recevoir les diff�rents
         * param�tres d'un appel de macro.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation.
         */
        S_T(commandemacroparametres)=NOUVEAU(CommandeMacroParametres);
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemacroparametres, parametre)=NOUVEAUX(Flux, T_S_(TAILLEINIT));
        ASSERTION(CHAMP(commandemacroparametres, parametre)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemacroparametres, memoire)=TAILLEINIT;
        CHAMP(commandemacroparametres, taille)=0;
        return RESULTAT_OK;
}

static Resultat commandemacroparametres_ajout_parametres(TRAVAIL(CommandeMacroParametres) commandemacroparametres)
{
        /* Fais en sorte de laisser au moins une place de libre � la fin de la liste.
         * Renvoie RESULTAT_ERREUR si commandemacroparametres est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        TABLEAU(STOCKAGE(Flux)) nouveau;
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR);

        if(T_S_(CHAMP(commandemacroparametres, taille))<T_S_(CHAMP(commandemacroparametres, memoire)))
                return RESULTAT_OK;
        nouveau=REALLOCATION_CAST(CHAMP(commandemacroparametres, parametre), Flux, T_S_(CHAMP(commandemacroparametres, memoire))*T_S_(MULTTAILLE));
        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemacroparametres, parametre)=nouveau;
        CHAMP(commandemacroparametres, memoire)*=T_S_(MULTTAILLE);
        return RESULTAT_OK;
}

Resultat commandemacroparametres_ajout_parametre(TRAVAIL(CommandeMacroParametres) commandemacroparametres, TRAVAIL(Flux) parametre)
{
        /* Ajoute un parametre � la liste des param�tres.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �ventuelle �choue.
         * Renvoie RESULTAT_ERREUR si la liste commandemacroparametres est NULL.
         */
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_ajout_parametres(commandemacroparametres));
        ELEMENT(CHAMP(commandemacroparametres, parametre), T_S_(CHAMP(commandemacroparametres, taille)))=NULL;
        SECURISE(flux_copie(parametre,ELEMENT_TRAVAIL(CHAMP(commandemacroparametres, parametre), T_S_(CHAMP(commandemacroparametres, taille)))));
        T_S_(CHAMP(commandemacroparametres, taille))++;
        return RESULTAT_OK;
}

static Resultat commandemacroparametres_lecture_taille(TRAVAIL(CommandeMacroParametres) commandemacroparametres, REFERENCE_SCALAIRE(Taille) taille)
{
        /* Renvoie la taille de la liste de param�tres.
         * Renvoie RESULTAT_ERREUR si le pointeur est NULL.
         */
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR);

        T_R_(taille)=T_S_(CHAMP(commandemacroparametres, taille));
        return RESULTAT_OK;
}

static Resultat commandemacroparametres_lecture_parametre(TRAVAIL(CommandeMacroParametres) commandemacroparametres, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(Flux) parametre)
{
        /* Renvoie un param�tre.
         * Renvoie RESULTAT_ERREUR si commandemacroparametres est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(commandemacroparametres, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        T_R(parametre)=ELEMENT_TRAVAIL(CHAMP(commandemacroparametres, parametre), indice);
        return RESULTAT_OK;
}

static Resultat commandemacroparametres_copie(TRAVAIL(CommandeMacroParametres) commandemacroparametres, TRAVAIL(CommandeMacroParametres) copie)
{
        /* Effectue une copie de la liste des param�tres.
         * Renvoie RESULTAT_ERREUR si commandemacroparametres est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandemacroparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_destruction(copie));
        SECURISE(commandemacroparametres_initialisation(copie));
        CHAMP(copie, memoire)=CHAMP(commandemacroparametres, memoire);
        CHAMP(copie, taille)=CHAMP(commandemacroparametres, taille);
        free(CHAMP(copie, parametre));
        CHAMP(copie, parametre)=NOUVEAUX(Flux, T_S_(CHAMP(commandemacroparametres, memoire)));
        ASSERTION(CHAMP(copie, parametre)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(commandemacroparametres, taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP(copie, parametre), T_S_(indice))=NULL;
                SECURISE(flux_copie(ELEMENT_TRAVAIL(CHAMP(commandemacroparametres, parametre), T_S_(indice)),ELEMENT_TRAVAIL(CHAMP(copie, parametre), T_S_(indice))));
        }
        return RESULTAT_OK;
}

Resultat commandemacroparametres_destruction(TRAVAIL(CommandeMacroParametres) commandemacroparametres)
{
        /* D�truit le contenu d'une liste de param�tres.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(commandemacroparametres)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(commandemacroparametres, taille)) ; T_S_(indice)++)
        {
                SECURISE(flux_destruction(ELEMENT_TRAVAIL(CHAMP(commandemacroparametres, parametre), T_S_(indice))));
        }
        free(CHAMP(commandemacroparametres, parametre));
        CHAMP(commandemacroparametres, parametre)=NULL;
        CHAMP(commandemacroparametres, memoire)=0;
        CHAMP(commandemacroparametres, taille)=0;
        free(S_T(commandemacroparametres));
        S_T(commandemacroparametres)=NULL;
        return RESULTAT_OK;
}

Resultat commandemacro_initialisation(TRAVAIL(CommandeMacro) commandemacro)
{
        /* Cr�e une commande d'appel de macro vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandemacro)=NOUVEAU(CommandeMacro))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandemacro, localisation)=NULL;
        CHAMP(commandemacro, nom)=NULL;
        SECURISE(commandemacroparametres_initialisation(CHAMP_TRAVAIL(commandemacro, parametres)));
        return RESULTAT_OK;
}

Resultat commandemacro_definition_localisationfichier(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande d'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandemacro, localisation)));
        return RESULTAT_OK;
}

Resultat commandemacro_definition_nom(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(NomMacro) nom)
{
        /* Assigne un nom � l'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);
        ASSERTION((CHAMP(commandemacro, nom)=DUP_CAST(NomMacro, nom))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat commandemacro_ajout_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(Flux) parametre)
{
        /* Ajoute un param�tre � l'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_ajout_parametre(CHAMP_TRAVAIL(commandemacro, parametres),parametre));
        return RESULTAT_OK;
}

Resultat commandemacro_definition_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL(Flux) parametre)
{
        /* Assigne un param�tre � un appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), taille)), RESULTAT_ERREUR_DEPASSEMENT);

        SECURISE(flux_copie(parametre,ELEMENT_TRAVAIL(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), parametre), indice)));
        return RESULTAT_OK;
}

Resultat commandemacro_definition_parametres(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(CommandeMacroParametres) commandemacroparametres) 
{
        /* Assigne une liste de param�tres � un appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_copie(commandemacroparametres,CHAMP_TRAVAIL(commandemacro, parametres)));
        return RESULTAT_OK;
}

Resultat commandemacro_lecture_localisationfichier(TRAVAIL(CommandeMacro) commandemacro, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande d'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         */
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandemacro, localisation);
        return RESULTAT_OK;
}

Resultat commandemacro_lecture_nom(TRAVAIL(CommandeMacro) commandemacro, REFERENCE_SCALAIRE(NomMacro) nom)
{
        /* Lit un nom � l'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         */ 
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        T_R_(nom)=T_S_(CHAMP(commandemacro, nom));
        return RESULTAT_OK;
}

Resultat commandemacro_lecture_tailleparametre(TRAVAIL(CommandeMacro) commandemacro, REFERENCE_SCALAIRE(Taille) tailleparametre)
{
        /* Lit le nombre de param�tres de l'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         */ 
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_lecture_taille(CHAMP_TRAVAIL(commandemacro, parametres),tailleparametre));
        return RESULTAT_OK;
}

Resultat commandemacro_lecture_parametre(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE(Flux) parametre)
{
        /* Lit un param�tre � l'appel de macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_lecture_parametre(CHAMP_TRAVAIL(commandemacro, parametres),indice,parametre));
        return RESULTAT_OK;
}

Resultat commandemacro_decalage_parametres(TRAVAIL(CommandeMacro) commandemacro)
{
        /* D�cale les param�tres d'un indice. Le param�tre 0 ajout�
         * sera NULL.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacroparametres_ajout_parametres(CHAMP_TRAVAIL(commandemacro, parametres)));
        for(indice=CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), taille) ; T_S_(indice)>T_S_(0) ; T_S_(indice)--)
                ELEMENT(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), parametre), T_S_(indice))=
                        ELEMENT(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), parametre), T_S_(indice)-T_S_(1));
        ELEMENT(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), parametre), T_S_(0))=NULL;
        T_S_(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), taille))++;
        return RESULTAT_OK;
}

Resultat commandemacro_parcours(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), taille)) ; T_S_(indice)++)
        {
                SECURISE(flux_parcours(ELEMENT_TRAVAIL(CHAMP_STOCKAGE(CHAMP(commandemacro, parametres), parametre), T_S_(indice)),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandemacro_copie(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(CommandeMacro) copie)
{
        /* Cr�e une copie de la commande macro.
         * Renvoie RESULTAT_ERREUR si commandemacro est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacro_destruction(copie));
        SECURISE(commandemacro_initialisation(copie));
        SECURISE(commandemacro_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandemacro, localisation)));
        SECURISE(commandemacro_definition_nom(copie,T_S_(CHAMP(commandemacro, nom))));
        SECURISE(commandemacroparametres_copie(CHAMP_TRAVAIL(commandemacro, parametres),CHAMP_TRAVAIL(copie, parametres)));
        return RESULTAT_OK;
}

Resultat commandemacro_destruction(TRAVAIL(CommandeMacro) commandemacro)
{
        /* D�truit une commande d'appel de macro.
         */ 
        if(S_T(commandemacro)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandemacro, localisation)));
        if(CHAMP(commandemacro, nom)!=NULL)
        {
                free(CHAMP(commandemacro, nom));
                CHAMP(commandemacro, nom)=NULL;
        }
        SECURISE(commandemacroparametres_destruction(CHAMP_TRAVAIL(commandemacro, parametres)));
        free(S_T(commandemacro));
        S_T(commandemacro)=NULL;
        return RESULTAT_OK;
}

