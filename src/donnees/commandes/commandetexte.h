/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDETEXTE__
#define __COMMANDETEXTE__

#include <src/global/global.h>

typedef struct commandetexte CONTENEUR(CommandeTexte);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandetexte
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du texte dans le source.
        STOCKAGE_SCALAIRE(Chaine) texte;
        //Contient le texte, en un seul bloc.
};
/* Commande d�stin�e � recevoir un bloc de texte sans commandes.
 */

Resultat commandetexte_initialisation(TRAVAIL(CommandeTexte) commandetexte);
/* Cr�e une commande de texte vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandetexte_definition_localisationfichier(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande texte.
 * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandetexte_definition_texte(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL_SCALAIRE(Chaine) texte);
/* Assigne un texte � la commande de texte.
 * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandetexte_lecture_localisationfichier(TRAVAIL(CommandeTexte) commandetexte, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande texte.
 * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
 */

Resultat commandetexte_lecture_texte(TRAVAIL(CommandeTexte) commandetexte, REFERENCE_SCALAIRE(Chaine) texte);
/* Lit un texte � la commande de texte.
 * Renvoie RESULTAT_ERREUR si commandetexte est NULL.
 */

Resultat commandetexte_parcours(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandetexte_copie(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(CommandeTexte) copie);
/* Cr�e une copie de la commande texte.
 * Renvoie RESULTAT_ERREUR si commandetexte est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandetexte_destruction(TRAVAIL(CommandeTexte) commandetexte);
/* D�truit une commande de texte.
 */

#endif
