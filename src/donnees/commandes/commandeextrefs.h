/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEEXTREFS__
#define __COMMANDEEXTREFS__

#include <src/global/global.h>

typedef struct commandeextrefs CONTENEUR(CommandeExtRefs);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeextrefs
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation des r�f�rences dans le source.
        STOCKAGE(Flux) format;
        //Format facultatif des r�f�rences.
        //S'il est absent, celui pr�cis� dans la commande
        //#reference est pris en compte.
        //Ce flux est de type FLUX_REFERENCE_FORMAT.
};
/* Commande d�sign�e pour indiquer la pr�sence de r�f�rences � une autre id�e
 * depuis une id�e manquante.
 */

Resultat commandeextrefs_initialisation(TRAVAIL(CommandeExtRefs) commandeextrefs);
/* Cr�e une commande extrefs vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une aloocation �choue.
 */

Resultat commandeextrefs_definition_localisationfichier(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande extrefs.
 * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une aloocation �choue.
 */

Resultat commandeextrefs_definition_format(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(Flux) format);
/* Assigne un format � la commande extrefs.
 * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une aloocation �choue.
 */

Resultat commandeextrefs_lecture_localisationfichier(TRAVAIL(CommandeExtRefs) commandeextrefs, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande extrefs.
 * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
 */

Resultat commandeextrefs_lecture_format(TRAVAIL(CommandeExtRefs) commandeextrefs, REFERENCE(Flux) format);
/* Lit un format � la commande extrefs.
 * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL.
 */

Resultat commandeextrefs_parcours(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeextrefs_copie(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(CommandeExtRefs) copie);
/* Cr�e une copie de la commande extrefs.
 * Renvoie RESULTAT_ERREUR si commandeextrefs est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeextrefs_destruction(TRAVAIL(CommandeExtRefs) commandeextrefs);
/* D�truit une commande extrefs.
 */

#endif
