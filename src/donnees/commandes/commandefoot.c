/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandefoot.h"

Resultat commandefoot_initialisation(TRAVAIL(CommandeFoot) commandefoot)
{
        /* Cr�e une commande de pied de document vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandefoot)=NOUVEAU(CommandeFoot))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandefoot, localisation)=NULL;
        CHAMP(commandefoot, pied)=NULL;
        return RESULTAT_OK;
}

Resultat commandefoot_definition_localisationfichier(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande foot.
         * Renvoie RESULTAT_ERREUR si commandefoot est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandefoot, localisation)));
        return RESULTAT_OK;
}

Resultat commandefoot_definition_pied(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(Flux) pied)
{
        /* Assigne un pied de document � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandefoot est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(pied,CHAMP_TRAVAIL(commandefoot, pied)));
        return RESULTAT_OK;
}

Resultat commandefoot_lecture_localisationfichier(TRAVAIL(CommandeFoot) commandefoot, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande foot.
         * Renvoie RESULTAT_ERREUR si commandefoot est NULL.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandefoot, localisation);
        return RESULTAT_OK;
}

Resultat commandefoot_lecture_pied(TRAVAIL(CommandeFoot) commandefoot, REFERENCE(Flux) pied)
{
        /* Lit un pied de document � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandefoot est NULL.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        T_R(pied)=CHAMP_TRAVAIL(commandefoot, pied);
        return RESULTAT_OK;
}

Resultat commandefoot_parcours(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandefoot, pied),action,general));
        return RESULTAT_OK;
}

Resultat commandefoot_copie(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(CommandeFoot) copie)
{
        /* Cr�e une copie de la commande foot.
         * Renvoie RESULTAT_ERREUR si commandefoot est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandefoot_destruction(copie));
        SECURISE(commandefoot_initialisation(copie));
        SECURISE(commandefoot_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandefoot, localisation)));
        SECURISE(commandefoot_definition_pied(copie,CHAMP_TRAVAIL(commandefoot, pied)));
        return RESULTAT_OK;
}

Resultat commandefoot_destruction(TRAVAIL(CommandeFoot) commandefoot)
{
        /* D�truit une commande de pied de document.
         */
        if(S_T(commandefoot)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandefoot, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandefoot, pied)));
        free(S_T(commandefoot));
        S_T(commandefoot)=NULL;
        return RESULTAT_OK;
}

