/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandesection.h"

Resultat commandesection_initialisation(TRAVAIL(CommandeSection) commandesection)
{
        /* Cr�e une commande de format de section vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandesection)=NOUVEAU(CommandeSection))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandesection, localisation)=NULL;
        CHAMP(commandesection, niveau)=NULL;
        CHAMP(commandesection, nom)=NULL;
        CHAMP(commandesection, formatavant)=NULL;
        CHAMP(commandesection, formatapres)=NULL;
        CHAMP(commandesection, presection)=NULL;
        CHAMP(commandesection, postsection)=NULL;
        return RESULTAT_OK;
}

Resultat commandesection_definition_localisationfichier(TRAVAIL(CommandeSection) commandesection, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandesection, localisation)));
        return RESULTAT_OK;
}

Resultat commandesection_definition_niveau(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) niveau)
{
        /* Assigne un niveau � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        if(S_T(niveau)==NULL)
        {
                SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, niveau)));
        }
        else
        {
                SECURISE(flux_copie(niveau,CHAMP_TRAVAIL(commandesection, niveau)));
        }
        return RESULTAT_OK;
}

Resultat commandesection_definition_nom(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) nom)
{
        /* Assigne un nom de r�f�rence � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(nom,CHAMP_TRAVAIL(commandesection, nom)));
        return RESULTAT_OK;
}

Resultat commandesection_definition_formatavant(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) formatavant)
{
        /* Assigne un format avant � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(formatavant,CHAMP_TRAVAIL(commandesection, formatavant)));
        return RESULTAT_OK;
}

Resultat commandesection_definition_formatapres(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) formatapres)
{
        /* Assigne un format apr�s � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(formatapres,CHAMP_TRAVAIL(commandesection, formatapres)));
        return RESULTAT_OK;
}

Resultat commandesection_definition_presection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) presection)
{
        /* Assigne un presection � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(presection,CHAMP_TRAVAIL(commandesection, presection)));
        return RESULTAT_OK;
}

Resultat commandesection_definition_postsection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) postsection)
{
        /* Assigne un postsection � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(postsection,CHAMP_TRAVAIL(commandesection, postsection)));
        return RESULTAT_OK;
}

Resultat commandesection_lecture_localisationfichier(TRAVAIL(CommandeSection) commandesection, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandesection, localisation);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_niveau(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) niveau)
{
        /* Lit un niveau � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(niveau)=CHAMP_TRAVAIL(commandesection, niveau);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_nom(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) nom)
{
        /* Lit un nom de r�f�rence � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(nom)=CHAMP_TRAVAIL(commandesection, nom);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_formatavant(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) formatavant)
{
        /* Lit un format avant � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(formatavant)=CHAMP_TRAVAIL(commandesection, formatavant);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_formatapres(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) formatapres)
{
        /* Lit un format apr�s � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(formatapres)=CHAMP_TRAVAIL(commandesection, formatapres);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_presection(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) presection)
{
        /* Lit un presection � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(presection)=CHAMP_TRAVAIL(commandesection, presection);
        return RESULTAT_OK;
}

Resultat commandesection_lecture_postsection(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) postsection)
{
        /* Lit un postsection � une commande de format de section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        T_R(postsection)=CHAMP_TRAVAIL(commandesection, postsection);
        return RESULTAT_OK;
}

Resultat commandesection_parcours(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandesection, niveau)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, niveau),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, nom),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, formatavant),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, formatapres),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, presection),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesection, postsection),action,general));
        return RESULTAT_OK;
}

Resultat commandesection_copie(TRAVAIL(CommandeSection) commandesection, TRAVAIL(CommandeSection) copie)
{
        /* Cr�e une copie de la commande section.
         * Renvoie RESULTAT_ERREUR si commandesection est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandesection_destruction(copie));
        SECURISE(commandesection_initialisation(copie));
        SECURISE(commandesection_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandesection, localisation)));
        SECURISE(commandesection_definition_niveau(copie,CHAMP_TRAVAIL(commandesection, niveau)));
        SECURISE(commandesection_definition_nom(copie,CHAMP_TRAVAIL(commandesection, nom)));
        SECURISE(commandesection_definition_formatavant(copie,CHAMP_TRAVAIL(commandesection, formatavant)));
        SECURISE(commandesection_definition_formatapres(copie,CHAMP_TRAVAIL(commandesection, formatapres)));
        SECURISE(commandesection_definition_presection(copie,CHAMP_TRAVAIL(commandesection, presection)));
        SECURISE(commandesection_definition_postsection(copie,CHAMP_TRAVAIL(commandesection, postsection)));
        return RESULTAT_OK;
}

Resultat commandesection_destruction(TRAVAIL(CommandeSection) commandesection)
{
        /* D�truit une commande de format de section.
         */ 
        if(S_T(commandesection)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandesection, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, niveau)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, nom)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, formatavant)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, formatapres)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, presection)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesection, postsection)));
        free(S_T(commandesection));
        S_T(commandesection)=NULL;
        return RESULTAT_OK;
}

