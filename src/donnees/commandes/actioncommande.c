/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "actioncommande.h"


Resultat actioncommande_initialisation(TRAVAIL(ActionCommande) actioncommande)
{
        /* Cr�e et initialise une structure d'action sur une commande sans actions associ�es
         * aux diff�rents types de commande.
         */
        ASSERTION((S_T(actioncommande)=NOUVEAU(ActionCommande))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), commentaire) =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), type)        =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, define), define)           =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, define), type)             =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), dep)                 =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), type)                =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), depref)           =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), type)             =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), echappement) =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), type)        =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, error), error)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, error), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), extref)           =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), type)             =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), extrefs)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), foot)               =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), type)               =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), generic)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, head), head)               =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, head), type)               =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), idea)               =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), type)               =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, include), include)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, include), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, index), index)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, index), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), macro)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), mesg)               =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), type)               =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, message), message)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, message), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), missing)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, option), option)           =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, option), type)             =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, options), options)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, options), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), parametre)     =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), type)          =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), reference)     =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), type)          =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), ref)                 =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), type)                =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), sec)                 =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), type)                =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, section), section)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, section), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), standard)       =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), type)           =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, start), start)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, start), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), texte)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, title), title)             =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, title), type)              =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), txt)                 =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), type)                =PARCOURS_AUTOMATIQUE_AVANT;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), warning)         =NULL;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), type)            =PARCOURS_AUTOMATIQUE_AVANT;
        return RESULTAT_OK;
}

/* Les fonctions qui suivent ne fonctionnent pas par copie !
 */

Resultat actioncommande_definition_commentaire        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(commentaire))(TRAVAIL(CommandeCommentaire), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), commentaire)=commentaire;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_define        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(define))(TRAVAIL(CommandeDefine), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, define), define)=define;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, define), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_dep                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(dep))(TRAVAIL(CommandeDep), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), dep)=dep;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_depref        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(depref))(TRAVAIL(CommandeDepRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), depref)=depref;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_echappement        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(echappement))(TRAVAIL(CommandeEchappement), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), echappement)=echappement;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_error        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(error))(TRAVAIL(CommandeError), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, error), error)=error;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, error), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_extref        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(extref))(TRAVAIL(CommandeExtRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), extref)=extref;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_extrefs        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(extrefs))(TRAVAIL(CommandeExtRefs), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), extrefs)=extrefs;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_foot                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(foot))(TRAVAIL(CommandeFoot), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), foot)=foot;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_generic        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(generic))(TRAVAIL(CommandeGeneric), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), generic)=generic;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_head                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(head))(TRAVAIL(CommandeHead), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, head), head)=head;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, head), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_idea                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(idea))(TRAVAIL(CommandeIdea), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), idea)=idea;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_include        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(include))(TRAVAIL(CommandeInclude), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, include), include)=include;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, include), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_index        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(index))(TRAVAIL(CommandeIndex), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, index), index)=index;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, index), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_macro        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(macro))(TRAVAIL(CommandeMacro), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), macro)=macro;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_mesg                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(mesg))(TRAVAIL(CommandeMesg), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), mesg)=mesg;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_message        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(message))(TRAVAIL(CommandeMessage), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, message), message)=message;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, message), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_missing        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(missing))(TRAVAIL(CommandeMissing), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), missing)=missing;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_option        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(option))(TRAVAIL(CommandeOption), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, option), option)=option;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, option), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_options        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(options))(TRAVAIL(CommandeOptions), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, options), options)=options;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, options), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_parametre        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(parametre))(TRAVAIL(CommandeParametre), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), parametre)=parametre;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_reference        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(reference))(TRAVAIL(CommandeReference), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), reference)=reference;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_ref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(ref))(TRAVAIL(CommandeRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), ref)=ref;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_sec                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(sec))(TRAVAIL(CommandeSec), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), sec)=sec;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_section        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(section))(TRAVAIL(CommandeSection), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, section), section)=section;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, section), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_standard        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(standard))(TRAVAIL(CommandeStandard), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), standard)=standard;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_start        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(start))(TRAVAIL(CommandeStart), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, start), start)=start;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, start), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_texte        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(texte))(TRAVAIL(CommandeTexte), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), texte)=texte;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_title        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(title))(TRAVAIL(CommandeTitle), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, title), title)=title;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, title), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_txt                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(txt))(TRAVAIL(CommandeTxt), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), txt)=txt;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), type)=S_T_(type);
        return RESULTAT_OK;
}

Resultat actioncommande_definition_warning        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (FONCTION(warning))(TRAVAIL(CommandeWarning), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), TRAVAIL_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), warning)=warning;
        CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), type)=S_T_(type);
        return RESULTAT_OK;
}

/* Toutes ces fonctions permettent de pr�ciser quelle est l'action associ�e � un type de commande particulier.
 * Renvoie RESULTAT_ERREUR si actioncommande est NULL.
 */

Resultat actioncommande_lecture_commentaire        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(commentaire))(TRAVAIL(CommandeCommentaire), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(commentaire!=NULL)
                F_R(commentaire)=CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), commentaire);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_define                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(define))(TRAVAIL(CommandeDefine), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(define!=NULL)
                F_R(define)=CHAMP_STOCKAGE_(CHAMP(actioncommande, define), define);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, define), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_dep                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(dep))(TRAVAIL(CommandeDep), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(dep!=NULL)
                F_R(dep)=CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), dep);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_depref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(depref))(TRAVAIL(CommandeDepRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(depref!=NULL)
                F_R(depref)=CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), depref);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_echappement        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(echappement))(TRAVAIL(CommandeEchappement), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(echappement!=NULL)
                F_R(echappement)=CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), echappement);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_error                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(error))(TRAVAIL(CommandeError), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(error!=NULL)
                F_R(error)=CHAMP_STOCKAGE_(CHAMP(actioncommande, error), error);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, error), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_extref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(extref))(TRAVAIL(CommandeExtRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(extref!=NULL)
                F_R(extref)=CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), extref);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_extrefs                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(extrefs))(TRAVAIL(CommandeExtRefs), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(extrefs!=NULL)
                F_R(extrefs)=CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), extrefs);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_foot                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(foot))(TRAVAIL(CommandeFoot), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(foot!=NULL)
                F_R(foot)=CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), foot);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_generic                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(generic))(TRAVAIL(CommandeGeneric), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(generic!=NULL)
                F_R(generic)=CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), generic);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_head                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(head))(TRAVAIL(CommandeHead), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(head!=NULL)
                F_R(head)=CHAMP_STOCKAGE_(CHAMP(actioncommande, head), head);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, head), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_idea                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(idea))(TRAVAIL(CommandeIdea), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(idea!=NULL)
                F_R(idea)=CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), idea);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_include                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(include))(TRAVAIL(CommandeInclude), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(include!=NULL)
                F_R(include)=CHAMP_STOCKAGE_(CHAMP(actioncommande, include), include);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, include), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_index                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(index))(TRAVAIL(CommandeIndex), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(index!=NULL)
                F_R(index)=CHAMP_STOCKAGE_(CHAMP(actioncommande, index), index);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, index), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_macro                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(macro))(TRAVAIL(CommandeMacro), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(macro!=NULL)
                F_R(macro)=CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), macro);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_mesg                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(mesg))(TRAVAIL(CommandeMesg), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(mesg!=NULL)
                F_R(mesg)=CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), mesg);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_message                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(message))(TRAVAIL(CommandeMessage), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(message!=NULL)
                F_R(message)=CHAMP_STOCKAGE_(CHAMP(actioncommande, message), message);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, message), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_missing                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(missing))(TRAVAIL(CommandeMissing), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(missing!=NULL)
                F_R(missing)=CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), missing);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_option                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(option))(TRAVAIL(CommandeOption), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(option!=NULL)
                F_R(option)=CHAMP_STOCKAGE_(CHAMP(actioncommande, option), option);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, option), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_options                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(options))(TRAVAIL(CommandeOptions), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(options!=NULL)
                F_R(options)=CHAMP_STOCKAGE_(CHAMP(actioncommande, options), options);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, options), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_parametre        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(parametre))(TRAVAIL(CommandeParametre), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(parametre!=NULL)
                F_R(parametre)=CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), parametre);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_reference        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(reference))(TRAVAIL(CommandeReference), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(reference!=NULL)
                F_R(reference)=CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), reference);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_ref                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(ref))(TRAVAIL(CommandeRef), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(ref!=NULL)
                F_R(ref)=CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), ref);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_sec                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(sec))(TRAVAIL(CommandeSec), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(sec!=NULL)
                F_R(sec)=CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), sec);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_section                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(section))(TRAVAIL(CommandeSection), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(section!=NULL)
                F_R(section)=CHAMP_STOCKAGE_(CHAMP(actioncommande, section), section);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, section), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_standard        (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(standard))(TRAVAIL(CommandeStandard), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(standard!=NULL)
                F_R(standard)=CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), standard);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_start                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(start))(TRAVAIL(CommandeStart), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(start!=NULL)
                F_R(start)=CHAMP_STOCKAGE_(CHAMP(actioncommande, start), start);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, start), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_texte                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(texte))(TRAVAIL(CommandeTexte), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(texte!=NULL)
                F_R(texte)=CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), texte);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_title                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(title))(TRAVAIL(CommandeTitle), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(title!=NULL)
                F_R(title)=CHAMP_STOCKAGE_(CHAMP(actioncommande, title), title);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, title), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_txt                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(txt))(TRAVAIL(CommandeTxt), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(txt!=NULL)
                F_R(txt)=CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), txt);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt), type));
        return RESULTAT_OK;
}

Resultat actioncommande_lecture_warning                (TRAVAIL(ActionCommande) actioncommande,
                Resultat (REFFONCTION(warning))(TRAVAIL(CommandeWarning), TRAVAIL(ActionFlux), TRAVAIL(General),
                        COREFERENCE(Flux), COREFERENCE_SCALAIRE(Booleen)), REFERENCE_SCALAIRE(Booleen) type)
{
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        if(warning!=NULL)
                F_R(warning)=CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), warning);
        if(type!=NULL)
                T_R_(type)=T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning), type));
        return RESULTAT_OK;
}

/* Lit un �l�ment dans la structure actioncommande. 
 * Renvoie RESULTAT_ERREUR si actioncommande est NULL.
 * Seuls les champs non NULL sont renseign�s.
 */

Resultat actioncommande_execution_commande(TRAVAIL(ActionCommande) actioncommande, TRAVAIL(Commande) commande, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* R�alise une action sur la commande pass�e en param�tre.
         * Les r�sultats sont plac�s dans le flux et le bool�en.
         * Renvoie RESULTAT_ERREUR si actioncommande ou commande
         * sont NULL.
         */
        TRAVAIL_SCALAIRE(TypeCommande) type;
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T(commande)!=NULL, RESULTAT_ERREUR);

        T_R_(arretcontexte)=T_S_(FAUX);
        SECURISE(commande_lecture_type(commande,R_T_(type)));
        switch(type)
        {
                case T_S_(COMMANDE_COMMENTAIRE):
                        {
                                TRAVAIL(CommandeCommentaire) commandecommentaire;
                                SECURISE(commande_lecture_commentaire(commande,R_T(commandecommentaire)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandecommentaire_parcours(commandecommentaire,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), commentaire)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), commentaire)(commandecommentaire,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandecommentaire_parcours(commandecommentaire,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_DEFINE):
                        {
                                TRAVAIL(CommandeDefine) commandedefine;
                                SECURISE(commande_lecture_define(commande,R_T(commandedefine)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandedefine_parcours(commandedefine,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), define)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), define)(commandedefine,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandedefine_parcours(commandedefine,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_DEP):
                        {
                                TRAVAIL(CommandeDep) commandedep;
                                SECURISE(commande_lecture_dep(commande,R_T(commandedep)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandedep_parcours(commandedep,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), dep)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), dep)(commandedep,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandedep_parcours(commandedep,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_DEPREF):
                        {
                                TRAVAIL(CommandeDepRef) commandedepref;
                                SECURISE(commande_lecture_depref(commande,R_T(commandedepref)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandedepref_parcours(commandedepref,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), depref)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), depref)(commandedepref,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandedepref_parcours(commandedepref,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_ECHAPPEMENT):
                        {
                                TRAVAIL(CommandeEchappement) commandeechappement;
                                SECURISE(commande_lecture_echappement(commande,R_T(commandeechappement)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeechappement_parcours(commandeechappement,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), echappement)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), echappement)(commandeechappement,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeechappement_parcours(commandeechappement,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_ERROR):
                        {
                                TRAVAIL(CommandeError) commandeerror;
                                SECURISE(commande_lecture_error(commande,R_T(commandeerror)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeerror_parcours(commandeerror,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), error)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), error)(commandeerror,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeerror_parcours(commandeerror,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_EXTREF):
                        {
                                TRAVAIL(CommandeExtRef) commandeextref;
                                SECURISE(commande_lecture_extref(commande,R_T(commandeextref)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeextref_parcours(commandeextref,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), extref)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), extref)(commandeextref,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeextref_parcours(commandeextref,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_EXTREFS):
                        {
                                TRAVAIL(CommandeExtRefs) commandeextrefs;
                                SECURISE(commande_lecture_extrefs(commande,R_T(commandeextrefs)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeextrefs_parcours(commandeextrefs,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), extrefs)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), extrefs)(commandeextrefs,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeextrefs_parcours(commandeextrefs,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_FOOT):
                        {
                                TRAVAIL(CommandeFoot) commandefoot;
                                SECURISE(commande_lecture_foot(commande,R_T(commandefoot)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandefoot_parcours(commandefoot,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), foot)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), foot)(commandefoot,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandefoot_parcours(commandefoot,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_GENERIC):
                        {
                                TRAVAIL(CommandeGeneric) commandegeneric;
                                SECURISE(commande_lecture_generic(commande,R_T(commandegeneric)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandegeneric_parcours(commandegeneric,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), generic)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), generic)(commandegeneric,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandegeneric_parcours(commandegeneric,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_HEAD):
                        {
                                TRAVAIL(CommandeHead) commandehead;
                                SECURISE(commande_lecture_head(commande,R_T(commandehead)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandehead_parcours(commandehead,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), head)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), head)(commandehead,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandehead_parcours(commandehead,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_IDEA):
                        {
                                TRAVAIL(CommandeIdea) commandeidea;
                                SECURISE(commande_lecture_idea(commande,R_T(commandeidea)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeidea_parcours(commandeidea,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), idea)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), idea)(commandeidea,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeidea_parcours(commandeidea,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_INCLUDE):
                        {
                                TRAVAIL(CommandeInclude) commandeinclude;
                                SECURISE(commande_lecture_include(commande,R_T(commandeinclude)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeinclude_parcours(commandeinclude,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), include)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), include)(commandeinclude,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeinclude_parcours(commandeinclude,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_INDEX):
                        {
                                TRAVAIL(CommandeIndex) commandeindex;
                                SECURISE(commande_lecture_index(commande,R_T(commandeindex)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeindex_parcours(commandeindex,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), index)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), index)(commandeindex,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeindex_parcours(commandeindex,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_MACRO):
                        {
                                TRAVAIL(CommandeMacro) commandemacro;
                                SECURISE(commande_lecture_macro(commande,R_T(commandemacro)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandemacro_parcours(commandemacro,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), macro)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), macro)(commandemacro,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandemacro_parcours(commandemacro,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_MESG):
                        {
                                TRAVAIL(CommandeMesg) commandemesg;
                                SECURISE(commande_lecture_mesg(commande,R_T(commandemesg)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandemesg_parcours(commandemesg,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), mesg)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), mesg)(commandemesg,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandemesg_parcours(commandemesg,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_MESSAGE):
                        {
                                TRAVAIL(CommandeMessage) commandemessage;
                                SECURISE(commande_lecture_message(commande,R_T(commandemessage)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandemessage_parcours(commandemessage,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), message)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), message)(commandemessage,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandemessage_parcours(commandemessage,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_MISSING):
                        {
                                TRAVAIL(CommandeMissing) commandemissing;
                                SECURISE(commande_lecture_missing(commande,R_T(commandemissing)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandemissing_parcours(commandemissing,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), missing)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), missing)(commandemissing,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandemissing_parcours(commandemissing,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_OPTION):
                        {
                                TRAVAIL(CommandeOption) commandeoption;
                                SECURISE(commande_lecture_option(commande,R_T(commandeoption)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeoption_parcours(commandeoption,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), option)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), option)(commandeoption,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeoption_parcours(commandeoption,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_OPTIONS):
                        {
                                TRAVAIL(CommandeOptions) commandeoptions;
                                SECURISE(commande_lecture_options(commande,R_T(commandeoptions)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeoptions_parcours(commandeoptions,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), options)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), options)(commandeoptions,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeoptions_parcours(commandeoptions,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_PARAMETRE):
                        {
                                TRAVAIL(CommandeParametre) commandeparametre;
                                SECURISE(commande_lecture_parametre(commande,R_T(commandeparametre)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandeparametre_parcours(commandeparametre,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), parametre)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), parametre)(commandeparametre,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandeparametre_parcours(commandeparametre,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_REFERENCE):
                        {
                                TRAVAIL(CommandeReference) commandereference;
                                SECURISE(commande_lecture_reference(commande,R_T(commandereference)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandereference_parcours(commandereference,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), reference)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), reference)(commandereference,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandereference_parcours(commandereference,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_REF):
                        {
                                TRAVAIL(CommandeRef) commanderef;
                                SECURISE(commande_lecture_ref(commande,R_T(commanderef)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commanderef_parcours(commanderef,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), ref)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), ref)(commanderef,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commanderef_parcours(commanderef,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_SEC):
                        {
                                TRAVAIL(CommandeSec) commandesec;
                                SECURISE(commande_lecture_sec(commande,R_T(commandesec)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandesec_parcours(commandesec,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), sec)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), sec)(commandesec,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandesec_parcours(commandesec,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_SECTION):
                        {
                                TRAVAIL(CommandeSection) commandesection;
                                SECURISE(commande_lecture_section(commande,R_T(commandesection)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandesection_parcours(commandesection,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), section)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), section)(commandesection,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandesection_parcours(commandesection,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_STANDARD):
                        {
                                TRAVAIL(CommandeStandard) commandestandard;
                                SECURISE(commande_lecture_standard(commande,R_T(commandestandard)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandestandard_parcours(commandestandard,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), standard)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), standard)(commandestandard,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandestandard_parcours(commandestandard,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_START):
                        {
                                TRAVAIL(CommandeStart) commandestart;
                                SECURISE(commande_lecture_start(commande,R_T(commandestart)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandestart_parcours(commandestart,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), start)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), start)(commandestart,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandestart_parcours(commandestart,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_TEXTE):
                        {
                                TRAVAIL(CommandeTexte) commandetexte;
                                SECURISE(commande_lecture_texte(commande,R_T(commandetexte)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandetexte_parcours(commandetexte,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), texte)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), texte)(commandetexte,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandetexte_parcours(commandetexte,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_TITLE):
                        {
                                TRAVAIL(CommandeTitle) commandetitle;
                                SECURISE(commande_lecture_title(commande,R_T(commandetitle)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandetitle_parcours(commandetitle,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), title)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), title)(commandetitle,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandetitle_parcours(commandetitle,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_TXT):
                        {
                                TRAVAIL(CommandeTxt) commandetxt;
                                SECURISE(commande_lecture_txt(commande,R_T(commandetxt)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandetxt_parcours(commandetxt,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), txt)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), txt)(commandetxt,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandetxt_parcours(commandetxt,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_WARNING):
                        {
                                TRAVAIL(CommandeWarning) commandewarning;
                                SECURISE(commande_lecture_warning(commande,R_T(commandewarning)));
                                if(T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), type))==T_S_(PARCOURS_AUTOMATIQUE_AVANT))
                                {
                                        SECURISE(commandewarning_parcours(commandewarning,actionflux,general));
                                }
                                if(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), warning)==NULL)
                                {
                                        S_C(flux)=NULL;
                                }
                                else
                                {
                                        SECURISE(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), warning)(commandewarning,actionflux,general,flux,arretcontexte));
                                }
                                if((T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), type))==T_S_(PARCOURS_AUTOMATIQUE_APRES))&&(S_C(flux)==NULL))
                                {
                                        SECURISE(commandewarning_parcours(commandewarning,actionflux,general));
                                }
                        }
                        break;
                case T_S_(COMMANDE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

Resultat actioncommande_copie(TRAVAIL(ActionCommande) actioncommande, TRAVAIL(ActionCommande) copie)
{
        /* Cr�e une copie de l'actioncommande donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(actioncommande)!=NULL, RESULTAT_ERREUR);

        SECURISE(actioncommande_destruction(copie));
        SECURISE(actioncommande_initialisation(copie));
        SECURISE(actioncommande_definition_commentaire(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire ), commentaire),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, commentaire ), type))));
        SECURISE(actioncommande_definition_define(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), define),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, define ), type))));
        SECURISE(actioncommande_definition_dep(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), dep),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, dep ), type))));
        SECURISE(actioncommande_definition_depref(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), depref),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, depref ), type))));
        SECURISE(actioncommande_definition_echappement(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), echappement),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, echappement ), type))));
        SECURISE(actioncommande_definition_error(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), error),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, error ), type))));
        SECURISE(actioncommande_definition_extref(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), extref),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extref ), type))));
        SECURISE(actioncommande_definition_extrefs(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), extrefs),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, extrefs ), type))));
        SECURISE(actioncommande_definition_foot(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), foot),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, foot ), type))));
        SECURISE(actioncommande_definition_generic(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), generic),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, generic ), type))));
        SECURISE(actioncommande_definition_head(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), head),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, head ), type))));
        SECURISE(actioncommande_definition_idea(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), idea),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, idea ), type))));
        SECURISE(actioncommande_definition_include(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), include),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, include ), type))));
        SECURISE(actioncommande_definition_index(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), index),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, index ), type))));
        SECURISE(actioncommande_definition_macro(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), macro),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, macro ), type))));
        SECURISE(actioncommande_definition_mesg(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), mesg),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, mesg ), type))));
        SECURISE(actioncommande_definition_message(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), message),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, message ), type))));
        SECURISE(actioncommande_definition_missing(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), missing),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, missing ), type))));
        SECURISE(actioncommande_definition_option(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), option),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, option ), type))));
        SECURISE(actioncommande_definition_options(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), options),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, options ), type))));
        SECURISE(actioncommande_definition_parametre(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), parametre),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, parametre ), type))));
        SECURISE(actioncommande_definition_reference(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), reference),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, reference ), type))));
        SECURISE(actioncommande_definition_ref(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), ref),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, ref ), type))));
        SECURISE(actioncommande_definition_sec(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), sec),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, sec ), type))));
        SECURISE(actioncommande_definition_section(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), section),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, section ), type))));
        SECURISE(actioncommande_definition_standard(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), standard),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, standard ), type))));
        SECURISE(actioncommande_definition_start(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), start),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, start ), type))));
        SECURISE(actioncommande_definition_texte(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), texte),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, texte ), type))));
        SECURISE(actioncommande_definition_title(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), title),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, title ), type))));
        SECURISE(actioncommande_definition_txt(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), txt),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, txt ), type))));
        SECURISE(actioncommande_definition_warning(copie,CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), warning),
                T_S_(CHAMP_STOCKAGE_(CHAMP(actioncommande, warning ), type))));
        return RESULTAT_OK;
}

Resultat actioncommande_destruction(TRAVAIL(ActionCommande) actioncommande)
{
        /* D�truit une structure d'action de commande.
         */
        if(S_T(actioncommande)==NULL)
                return RESULTAT_OK;
        free(S_T(actioncommande));
        S_T(actioncommande)=NULL;
        return RESULTAT_OK;
}
