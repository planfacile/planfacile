/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEGENERIC__
#define __COMMANDEGENERIC__

#include <src/global/global.h>

typedef struct commandegeneric CONTENEUR(CommandeGeneric);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandegeneric
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'id�e g�n�rique dans le source.
        STOCKAGE(Flux) indice;
        //Indice de l'id�e g�n�rique.
        //Si le flux est manquant, cela correspondra �
        //la r�solution d'une id�e g�n�rique par d�faut.
        //Ce flux est de type FLUX_INDICE.
        STOCKAGE(Flux) reference;
        //Nom de r�f�rence de l'id�e g�n�rique.
        //Ce flux est de type FLUX_REFERENCE_AUTOMATIQUE.
        STOCKAGE(Flux) titre;
        //Titre de l'id�e g�n�rique.
        //Ce flux est de type FLUX_TITRE_AUTOMATIQUE.
        STOCKAGE(Flux) texte;
        //Texte associ� � l'id�e g�n�rique. (tout �a pour �a !)
        //Ce flux est de type FLUX_TEXTE_GENERIQUE.
};
/* Commande d�sign�e pour enregistrer une id�e g�n�rique du document.
 */

Resultat commandegeneric_initialisation(TRAVAIL(CommandeGeneric) commandegeneric);
/* Cr�e une commande d'id�e g�n�rique vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_definition_localisationfichier(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande generic.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_definition_indice(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) indice);
/* Assigne un indice � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_definition_reference(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) reference);
/* Assigne un nom de r�f�rence � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_definition_titre(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) titre);
/* Assigne un titre � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_definition_texte(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(Flux) texte);
/* Assigne un texte � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandegeneric_lecture_localisationfichier(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande generic.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 */

Resultat commandegeneric_lecture_indice(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) indice);
/* Lit un indice � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 */

Resultat commandegeneric_lecture_reference(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) reference);
/* Lit un nom de r�f�rence � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 */

Resultat commandegeneric_lecture_titre(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) titre);
/* Lit un titre � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 */

Resultat commandegeneric_lecture_texte(TRAVAIL(CommandeGeneric) commandegeneric, REFERENCE(Flux) texte);
/* Lit un texte � une id�e g�n�rique.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL.
 */

Resultat commandegeneric_parcours(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandegeneric_copie(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(CommandeGeneric) copie);
/* Cr�e une copie de la commande generic.
 * Renvoie RESULTAT_ERREUR si commandegeneric est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandegeneric_destruction(TRAVAIL(CommandeGeneric) commandegeneric);
/* D�truit une id�e g�n�rique.
 */

#endif
