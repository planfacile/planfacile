/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeinclude.h"

Resultat commandeinclude_initialisation(TRAVAIL(CommandeInclude) commandeinclude)
{
        /* Cr�e une commande d'inclusion vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandeinclude)=NOUVEAU(CommandeInclude))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeinclude, localisation)=NULL;
        CHAMP(commandeinclude, inclusion)=NULL;
        return RESULTAT_OK;
}

Resultat commandeinclude_definition_localisationfichier(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande include.
         * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeinclude, localisation)));
        return RESULTAT_OK;
}

Resultat commandeinclude_definition_inclusion(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL_SCALAIRE(NomFichier) inclusion)
{
        /* Assigne un nom de fichier � une commande d'inclusion.
         * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandeinclude, inclusion)!=NULL)
        {
                free(CHAMP(commandeinclude, inclusion));
                CHAMP(commandeinclude, inclusion)=NULL;
        }
        ASSERTION((CHAMP(commandeinclude, inclusion)=DUP_CAST(NomFichier, inclusion))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat commandeinclude_lecture_localisationfichier(TRAVAIL(CommandeInclude) commandeinclude, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande include.
         * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
         */ 
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeinclude, localisation);
        return RESULTAT_OK;
}

Resultat commandeinclude_lecture_inclusion(TRAVAIL(CommandeInclude) commandeinclude, REFERENCE_SCALAIRE(NomFichier) inclusion)
{
        /* Lit un nom de fichier � une commande d'inclusion.
         * Renvoie RESULTAT_ERREUR si commandeinclude est NULL.
         */ 
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        T_R_(inclusion)=T_S_(CHAMP(commandeinclude, inclusion));
        return RESULTAT_OK;
}

Resultat commandeinclude_parcours(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandeinclude_copie(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(CommandeInclude) copie)
{
        /* Cr�e une copie de la commande include.
         * Renvoie RESULTAT_ERREUR si commandeinclude est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeinclude_destruction(copie));
        SECURISE(commandeinclude_initialisation(copie));
        SECURISE(commandeinclude_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeinclude, localisation)));
        SECURISE(commandeinclude_definition_inclusion(copie,T_S_(CHAMP(commandeinclude, inclusion))));
        return RESULTAT_OK;
}

Resultat commandeinclude_destruction(TRAVAIL(CommandeInclude) commandeinclude)
{
        /* D�truit une commande d'inclusion.
         */ 
        if(S_T(commandeinclude)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeinclude, localisation)));
        if(CHAMP(commandeinclude, inclusion)!=NULL)
        {
                free(CHAMP(commandeinclude, inclusion));
                CHAMP(commandeinclude, inclusion)=NULL;
        }
        free(S_T(commandeinclude));
        S_T(commandeinclude)=NULL;
        return RESULTAT_OK;
}

