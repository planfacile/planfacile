/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEINDEX__
#define __COMMANDEINDEX__

#include <src/global/global.h>

typedef struct commandeindex CONTENEUR(CommandeIndex);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeindex
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'index dans le source.
};
/* Commande d�sign�e pour indiquer un remplacement par un index.
 */

Resultat commandeindex_initialisation(TRAVAIL(CommandeIndex) commandeindex);
/* Cr�e une commande d'index vide.
 * Renvoie RESULTAT_ERREUR si commandeindex est NULL.
 */

Resultat commandeindex_definition_localisationfichier(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande index.
 * Renvoie RESULTAT_ERREUR si commandeindex est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeindex_lecture_localisationfichier(TRAVAIL(CommandeIndex) commandeindex, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande index.
 * Renvoie RESULTAT_ERREUR si commandeindex est NULL.
 */

Resultat commandeindex_parcours(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeindex_copie(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(CommandeIndex) copie);
/* Cr�e une copie de la commande index.
 * Renvoie RESULTAT_ERREUR si commandeindex est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeindex_destruction(TRAVAIL(CommandeIndex) commandeindex);
/* D�truit une commande d'index.
 */

#endif
