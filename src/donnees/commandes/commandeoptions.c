/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeoptions.h"

Resultat commandeoptionsclause_initialisation(TRAVAIL(CommandeOptionsClause) commandeoptionsclause)
{
        /* Cr�e une commande commandeoptionsclause vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeoptionsclause)=NOUVEAU(CommandeOptionsClause))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeoptionsclause, option)=NULL;
        CHAMP(commandeoptionsclause, clause)=NULL;
        CHAMP(commandeoptionsclause, execution)=FAUX;
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_definition_option(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL_SCALAIRE(NomOption) option)
{
        /* Assigne un nom d'option � une clause
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandeoptionsclause, option)!=NULL)
        {
                free(CHAMP(commandeoptionsclause, option));
                CHAMP(commandeoptionsclause, option)=NULL;
        }
        ASSERTION((CHAMP(commandeoptionsclause, option)=DUP_CAST(NomOption, option))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_definition_clause(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL(Flux) execution)
{
        /* Assigne un flux d'�x�cution � une clause
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(execution,CHAMP_TRAVAIL(commandeoptionsclause, clause)));
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_definition_execution(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL_SCALAIRE(Booleen) execution)
{
        /* Assigne une valeur au drapeau d'�x�cution d'une clause
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandeoptionsclause, execution)=S_T_(execution);
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_lecture_option(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, REFERENCE_SCALAIRE(NomOption) option)
{
        /* Lit l'option associ�e � une clause.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        T_R_(option)=T_S_(CHAMP(commandeoptionsclause, option));
        return RESULTAT_OK;
}

Resultat commandeoptionsclause_definition(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL(Flux) clause)
{
        /* D�finit une clause.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclause_definition_option(commandeoptionsclause,option));
        SECURISE(commandeoptionsclause_definition_clause(commandeoptionsclause,clause));
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_lecture_clause(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, REFERENCE(Flux) clause)
{
        /* Lit une clause.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        T_R(clause)=CHAMP_TRAVAIL(commandeoptionsclause, clause);
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_lecture_execution(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, REFERENCE_SCALAIRE(Booleen) execution)
{
        /* Lit le drapeau d'�x�cution d'une clause.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        T_R_(execution)=T_S_(CHAMP(commandeoptionsclause, execution));
        return RESULTAT_OK;
}

static Resultat commandeoptionsclause_copie(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL(CommandeOptionsClause) copie)
{
        /* Copie intelligemment une commandeoptionsclause. Voir plus bas.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclause ou copie est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeoptionsclause)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclause_destruction(copie));
        SECURISE(commandeoptionsclause_initialisation(copie));
        SECURISE(commandeoptionsclause_definition_option(copie,T_S_(CHAMP(commandeoptionsclause, option))));
        SECURISE(commandeoptionsclause_definition_clause(copie,CHAMP_TRAVAIL(commandeoptionsclause, clause)));
        SECURISE(commandeoptionsclause_definition_execution(copie,T_S_(CHAMP(commandeoptionsclause, execution))));
        return RESULTAT_OK;
}

Resultat commandeoptionsclause_destruction(TRAVAIL(CommandeOptionsClause) commandeoptionsclause)
{
        /* D�truit le contenu de la commandeoptionsclause.
         */
        if(S_T(commandeoptionsclause)==NULL)
                return RESULTAT_OK;
        if(CHAMP(commandeoptionsclause, option)!=NULL)
        {
                free(CHAMP(commandeoptionsclause, option));
                CHAMP(commandeoptionsclause, option)=NULL;
        }
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeoptionsclause, clause)));
        CHAMP(commandeoptionsclause, execution)=FAUX;
        free(S_T(commandeoptionsclause));
        S_T(commandeoptionsclause)=NULL;
        return RESULTAT_OK;
}

Resultat commandeoptionsclauses_initialisation(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses)
{
        /* Cr�e une liste de clauses vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandeoptionsclauses)=NOUVEAU(CommandeOptionsClauses))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        ASSERTION((CHAMP(commandeoptionsclauses, clause)=NOUVEAUX(CommandeOptionsClause, T_S_(TAILLEINIT)))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeoptionsclauses, memoire)=TAILLEINIT;
        CHAMP(commandeoptionsclauses, taille)=0;
        return RESULTAT_OK;
}

static Resultat commandeoptionsclauses_ajout_clauses(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses)
{
        /* Fait en sorte de laisser un indice libre en fin de tableau.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �ventuelle �choue.
         */
        TABLEAU(STOCKAGE(CommandeOptionsClause)) nouveau;
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);

        if(T_S_(CHAMP(commandeoptionsclauses, taille))<T_S_(CHAMP(commandeoptionsclauses, memoire)))
                return RESULTAT_OK;
        nouveau=REALLOCATION_CAST(CHAMP(commandeoptionsclauses, clause), CommandeOptionsClause, T_S_(CHAMP(commandeoptionsclauses, memoire))*T_S_(MULTTAILLE));
        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeoptionsclauses, clause)=nouveau;
        CHAMP(commandeoptionsclauses, memoire)*=T_S_(MULTTAILLE);
        return RESULTAT_OK;
}

Resultat commandeoptionsclauses_ajout_clause(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL(CommandeOptionsClause) commandeoptionsclause)
{
        /* Ajoute une clause en fin de liste.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclauses_ajout_clauses(commandeoptionsclauses));
        ELEMENT(CHAMP(commandeoptionsclauses, clause), T_S_(CHAMP(commandeoptionsclauses, taille)))=NULL;
        SECURISE(commandeoptionsclause_copie(commandeoptionsclause,ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), T_S_(CHAMP(commandeoptionsclauses, taille)))));
        T_S_(CHAMP(commandeoptionsclauses, taille))++;
        return RESULTAT_OK;
}

static Resultat commandeoptionsclauses_definition_execution(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL_SCALAIRE(Indice) indice, TRAVAIL_SCALAIRE(Booleen) execution)
{
        /* Affecte le drapeau d'�x�cution d'une clause.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         */
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(commandeoptionsclauses, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        SECURISE(commandeoptionsclause_definition_execution(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), indice),execution));
        return RESULTAT_OK;
}

//static Resultat commandeoptionsclauses_lecture_taille(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, REFERENCE_SCALAIRE(Taille) taille)
//{
//        /* Renvoie le nombre de clauses.
//         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
//         */
//        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);
//
//        T_R_(taille)=T_S_(CHAMP(commandeoptionsclauses, taille));
//        return RESULTAT_OK;
//}

static Resultat commandeoptionsclauses_lecture_clause(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL_SCALAIRE(Indice) indice, REFERENCE_SCALAIRE(NomOption) option, REFERENCE(Flux) clause, REFERENCE_SCALAIRE(Booleen) execution)
{
        /* Lit une clause de la liste.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect.
         * La fonction ne renseigne que les champs dont les pointeurs sont non NULL.
         */
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>=T_S_(0), RESULTAT_ERREUR_DEPASSEMENT);
        ASSERTION(indice<T_S_(CHAMP(commandeoptionsclauses, taille)), RESULTAT_ERREUR_DEPASSEMENT);

        if(option!=NULL)
                SECURISE(commandeoptionsclause_lecture_option(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), indice),option));
        if(clause!=NULL)
                SECURISE(commandeoptionsclause_lecture_clause(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), indice),clause));
        if(execution!=NULL)
                SECURISE(commandeoptionsclause_lecture_execution(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), indice),execution));
        return RESULTAT_OK;
}

static Resultat commandeoptionsclauses_recherche_clause(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL_SCALAIRE(NomOption) option, REFERENCE_SCALAIRE(Indice) indice)
{
        /* Recherche la position d'une clause d'apr�s son option associ�e.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie CLAUSENONTROUVEE dans indice si la clause n'est pas trouv�e.
         */
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);

        for(T_R_(indice)=0 ; T_R_(indice)<T_S_(CHAMP(commandeoptionsclauses, taille)) ; T_R_(indice)++)
        {
                TRAVAIL_SCALAIRE(NomOption) nom;
                SECURISE(commandeoptionsclause_lecture_option(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), T_R_(indice)),R_T_(nom)));
                if(STRCMP(nom, option)==T_S_(0))
                        return RESULTAT_OK;
        }
        T_R_(indice)=T_S_(CLAUSENONTROUVEE);
        return RESULTAT_OK;
}

Resultat commandeoptionsclauses_destruction(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses)
{
        /* D�truit une commandeoptionsclauses.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(commandeoptionsclauses)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(commandeoptionsclauses, taille)) ; T_S_(indice)++)
                SECURISE(commandeoptionsclause_destruction(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), T_S_(indice))));
        free(CHAMP(commandeoptionsclauses, clause));
        CHAMP(commandeoptionsclauses, clause)=NULL;
        CHAMP(commandeoptionsclauses, memoire)=0;
        CHAMP(commandeoptionsclauses, taille)=0;
        free(S_T(commandeoptionsclauses));
        S_T(commandeoptionsclauses)=NULL;
        return RESULTAT_OK;
}

static Resultat commandeoptionsclauses_copie(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL(CommandeOptionsClauses) copie)
{
        /* Copie intelligemment une commandeoptionsclauses. Voir plus bas.
         * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandeoptionsclauses)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclauses_destruction(copie));
        SECURISE(commandeoptionsclauses_initialisation(copie));
        CHAMP(copie, memoire)=CHAMP(commandeoptionsclauses, memoire);
        CHAMP(copie, taille)=CHAMP(commandeoptionsclauses, taille);
        free(CHAMP(copie, clause));
        ASSERTION((CHAMP(copie, clause)=NOUVEAUX(CommandeOptionsClause, T_S_(CHAMP(copie, memoire))))!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(commandeoptionsclauses, taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP(copie, clause), T_S_(indice))=NULL;
                SECURISE(commandeoptionsclause_copie(ELEMENT_TRAVAIL(CHAMP(commandeoptionsclauses, clause), T_S_(indice)),ELEMENT_TRAVAIL(CHAMP(copie, clause), T_S_(indice))));
        }
        return RESULTAT_OK;
}

Resultat commandeoptions_initialisation(TRAVAIL(CommandeOptions) commandeoptions)
{
        /* Cr�e une commande d'options vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandeoptions)=NOUVEAU(CommandeOptions))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeoptions, localisation)=NULL;
        CHAMP(commandeoptions, label)=NULL;
        SECURISE(commandeoptionsclauses_initialisation(CHAMP_TRAVAIL(commandeoptions, clauses)));
        CHAMP(commandeoptions, autres)=NULL;
        CHAMP(commandeoptions, execution)=FAUX;
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_localisationfichier(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeoptions, localisation)));
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_label(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(Label) label)
{
        /* Assigne un label � une commande d'options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandeoptions, label)!=NULL)
        {
                free(CHAMP(commandeoptions, label));
                CHAMP(commandeoptions, label)=NULL;
        }
        ASSERTION((CHAMP(commandeoptions, label)=DUP_CAST(Label, label))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat commandeoptions_ajout_clause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL(Flux) clause)
{
        /* Ajoute une clause � une commande d'options
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        STOCKAGE(CommandeOptionsClause) nouvelleclause;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclause_initialisation(T_S(nouvelleclause)));
        SECURISE(commandeoptionsclause_definition(T_S(nouvelleclause),option,clause));
        SECURISE(commandeoptionsclauses_ajout_clause(CHAMP_TRAVAIL(commandeoptions, clauses),T_S(nouvelleclause)));
        SECURISE(commandeoptionsclause_destruction(T_S(nouvelleclause)));
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_clauses(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses)
{
        /* Affecte une liste d'option � une commande d'options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclauses_copie(commandeoptionsclauses,CHAMP_TRAVAIL(commandeoptions, clauses)));
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_executionclause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL_SCALAIRE(Booleen) execution)
{
        /* Assigne une valeur au drapeau d'�x�cution de la clause par d�faut.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Si le nom d'option ne correspond � aucun de la liste,
         * le drapeau de la clause par d�faut sera affect�.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclauses_recherche_clause(CHAMP_TRAVAIL(commandeoptions, clauses),option,R_T_(indice)));
        if(indice==T_S_(CLAUSENONTROUVEE))
                CHAMP(commandeoptions, execution)=S_T_(execution);
        else
                SECURISE(commandeoptionsclauses_definition_execution(CHAMP_TRAVAIL(commandeoptions, clauses),indice,execution));
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_autres(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Flux) autres)
{
        /* Assigne un flux � la clause autres d'une commande d'options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(autres,CHAMP_TRAVAIL(commandeoptions, autres)));
        return RESULTAT_OK;
}

Resultat commandeoptions_definition_executionautres(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(Booleen) execution)
{
        /* Assigne une valeur au drapeau d'�x�cution de la clause par d�faut.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         */
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        CHAMP(commandeoptions, execution)=S_T_(execution);
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_localisationfichier(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeoptions, localisation);
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_label(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE_SCALAIRE(Label) label)
{
        /* Lit un label � une commande d'options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        T_R_(label)=T_S_(CHAMP(commandeoptions, label));
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_clause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, REFERENCE(Flux) clause, REFERENCE_SCALAIRE(Booleen) execution)
{
        /* Lit une clause � une commande d'options.
         * La clause est recherch�e d'apr�s le nom de l'option pass�e en param�tre.
         * Si l'option ne fait pas partie des clauses, la clause par d�faut est renvoy�e.
         * Si clause ou execution vaut NULL, c'est que l'information en question n'est pas souhait�e.
         * Renvoie RESULTAT_ERREUR si commandeoptions ou option est NULL.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptionsclauses_recherche_clause(CHAMP_TRAVAIL(commandeoptions, clauses),option,R_T_(indice)));
        if(indice==T_S_(CLAUSENONTROUVEE))
        {
                if(clause!=NULL)
                        T_R(clause)=CHAMP_TRAVAIL(commandeoptions, autres);
                if(execution!=NULL)
                        T_R_(execution)=T_S_(CHAMP(commandeoptions, execution));
        }
        else
        {
                SECURISE(commandeoptionsclauses_lecture_clause(CHAMP_TRAVAIL(commandeoptions, clauses),indice,NULL,clause,execution));
        }
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_autres(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE(Flux) autres)
{
        /* Lit le flux de la clause autres d'une commande d'options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         */ 
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        T_R(autres)=CHAMP_TRAVAIL(commandeoptions, autres);
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_execution(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE_SCALAIRE(Booleen) execution)
{
        /* Lit une valeur au drapeau d'�x�cution de la clause par d�faut.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
         */
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        T_R_(execution)=T_S_(CHAMP(commandeoptions, execution));
        return RESULTAT_OK;
}

Resultat commandeoptions_lecture_clauseexecutable(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Option) option, REFERENCE_SCALAIRE(NomOption) nomoption, REFERENCE(Flux) clause, REFERENCE_SCALAIRE(Booleen) execution)
{
        /* Lit la clause devant �tre execut�e selon la liste d'option donn�e.
         * Si l'option ne fait pas partie des clauses, la clause par d�faut est renvoy�e,
         * avec NULL comme nom d'option.
         * Si clause ou execution vaut NULL, c'est que l'information en question n'est pas souhait�e.
         * Renvoie RESULTAT_ERREUR si commandeoptions ou option est NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Indice) clausetrouvee;
        TRAVAIL_SCALAIRE(Indice) clausepresente;
        TRAVAIL_SCALAIRE(Taille) taille;
        TRAVAIL_SCALAIRE(NomOption) nomoptionrecherche;
        TRAVAIL(CommandeOption) commandeoption;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T(option)!=NULL, RESULTAT_ERREUR);

        clausetrouvee=CLAUSENONTROUVEE;
        SECURISE(option_lecture_taille(option,R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                SECURISE(option_lecture_option(option,T_S_(indice),R_T(commandeoption)));
                SECURISE(commandeoption_lecture_option(commandeoption,R_T_(nomoptionrecherche)));
                SECURISE(commandeoptionsclauses_recherche_clause(CHAMP_TRAVAIL(commandeoptions, clauses),nomoptionrecherche,R_T_(clausepresente)));
                if(clausepresente!=T_S_(CLAUSENONTROUVEE))
                {
                        if(clausetrouvee==T_S_(CLAUSENONTROUVEE))
                                clausetrouvee=clausepresente;
                        else
                        {
                                if(clausepresente<clausetrouvee)
                                        clausetrouvee=clausepresente;
                        }
                }
        }
        if(clausetrouvee==T_S_(CLAUSENONTROUVEE))
        {
                if(nomoption!=NULL)
                        T_R_(nomoption)=NULL;
                if(clause!=NULL)
                        T_R(clause)=CHAMP_TRAVAIL(commandeoptions, autres);
                if(execution!=NULL)
                        T_R_(execution)=T_S_(CHAMP(commandeoptions, execution));
        }
        else
        {
                SECURISE(commandeoptionsclauses_lecture_clause(CHAMP_TRAVAIL(commandeoptions, clauses),clausetrouvee,nomoption,clause,execution));
        }
        return RESULTAT_OK;
}

Resultat commandeoptions_extraction_fluxexecutable(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Flux) executable, TRAVAIL(General) general)
{
        /* Renvoie le flux �x�cutable devant remplacer la commande
         * d'options apr�s d�termination des clauses �x�cutables.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Renvoie RESULTAT_ERREUR si commandeoptions vaut NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Booleen) execution;
        TRAVAIL(Flux) clause;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(executable));
        SECURISE(flux_definition_type(executable,T_S_(FLUX_OPTIONS)));
        if(T_S_(CHAMP(commandeoptions, execution))==T_S_(VRAI))
        {
                SECURISE(flux_concatenation(executable,CHAMP_TRAVAIL(commandeoptions, autres),general));
        }
        for(indice=S_T_(T_S_(CHAMP_STOCKAGE(CHAMP(commandeoptions, clauses), taille))-T_S_(1)) ; T_S_(indice)>=T_S_(0) ; T_S_(indice)--)
        {
                SECURISE(commandeoptionsclauses_lecture_clause(CHAMP_TRAVAIL(commandeoptions, clauses),T_S_(indice),NULL,R_T_(clause),R_T_(execution)));
                if(execution==T_S_(VRAI))
                {
                        SECURISE(flux_concatenation(executable,clause,general));
                }
        }
        return RESULTAT_OK;
}

Resultat commandeoptions_parcours(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE(CHAMP(commandeoptions, clauses), taille)) ; T_S_(indice)++)
        {
                SECURISE(flux_parcours(T_S(CHAMP_STOCKAGE(ELEMENT(CHAMP_STOCKAGE(CHAMP(commandeoptions, clauses), clause), T_S_(indice)), clause)),action,general));
        }
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeoptions, autres),action,general));
        return RESULTAT_OK;
}

Resultat commandeoptions_copie(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(CommandeOptions) copie)
{
        /* Cr�e une copie de la commande options.
         * Renvoie RESULTAT_ERREUR si commandeoptions est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptions_destruction(copie));
        SECURISE(commandeoptions_initialisation(copie));
        SECURISE(commandeoptions_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeoptions, localisation)));
        SECURISE(commandeoptions_definition_label(copie,T_S_(CHAMP(commandeoptions, label))));
        SECURISE(commandeoptionsclauses_copie(CHAMP_TRAVAIL(commandeoptions, clauses),CHAMP_TRAVAIL(copie, clauses)));
        SECURISE(commandeoptions_definition_autres(copie,CHAMP_TRAVAIL(commandeoptions, autres)));
        SECURISE(commandeoptions_definition_executionautres(copie,T_S_(CHAMP(commandeoptions, execution))));
        return RESULTAT_OK;
}

Resultat commandeoptions_destruction(TRAVAIL(CommandeOptions) commandeoptions)
{
        /* D�truit une commande d'options.
         */
        if(S_T(commandeoptions)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeoptions, localisation)));
        if(CHAMP(commandeoptions, label)!=NULL)
        {
                free(CHAMP(commandeoptions, label));
                CHAMP(commandeoptions, label)=NULL;
        }
        SECURISE(commandeoptionsclauses_destruction(CHAMP_TRAVAIL(commandeoptions, clauses)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeoptions, autres)));
        CHAMP(commandeoptions, execution)=FAUX;
        free(S_T(commandeoptions));
        S_T(commandeoptions)=NULL;
        return RESULTAT_OK;
}

