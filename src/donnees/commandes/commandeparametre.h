/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEPARAMETRE__
#define __COMMANDEPARAMETRE__

#include <src/global/global.h>

typedef struct commandeparametre CONTENEUR(CommandeParametre);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeparametre
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du param�tre dans le source.
        STOCKAGE_SCALAIRE(Indice) indice;
        //Indice du param�tre souhait�.
        //Rappel : 0 �quivaut � l'indice de boucle.
};
/* Cette commande est destin�e � recevoir l'indice du param�tre par lequel il faudra
 * remplacer la commande par le flux de commande du param�tre lors de l'appel de la
 * macro.
 */

Resultat commandeparametre_initialisation(TRAVAIL(CommandeParametre) commandeparametre);
/* Cr�e une commande de param�tre vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeparametre_definition_localisationfichier(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande parametre.
 * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeparametre_definition_indice(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL_SCALAIRE(Indice) indice);
/* Assigne un indice de param�tre � la commande de parametre.
 * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeparametre_lecture_localisationfichier(TRAVAIL(CommandeParametre) commandeparametre, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande parametre.
 * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
 */

Resultat commandeparametre_lecture_indice(TRAVAIL(CommandeParametre) commandeparametre, REFERENCE_SCALAIRE(Indice) indice);
/* Lit un indice de param�tre � la commande de parametre.
 * Renvoie RESULTAT_ERREUR si commandeparametre est NULL.
 */

Resultat commandeparametre_parcours(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeparametre_copie(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(CommandeParametre) copie);
/* Cr�e une copie de la commande parametre.
 * Renvoie RESULTAT_ERREUR si commandeparametre est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeparametre_destruction(TRAVAIL(CommandeParametre) commandeparametre);
/* D�truit une commande de parametre.
 */

#endif
