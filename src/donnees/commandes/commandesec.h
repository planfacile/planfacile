/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDESEC__
#define __COMMANDESEC__

#include <src/global/global.h>

typedef struct commandesec CONTENEUR(CommandeSec);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandesec
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du remplacement par un nom de section
        //dans le source.
        STOCKAGE(Flux) niveau;
        //Niveau cible du nom � remplacer.
        //Si ce flux est �gal � NULL, alors le niveau
        //de la destination de la r�f�rence sera choisi.
        //Dans le cas o� le niveau est indiqu�, si le param�tre est
        //num�rique, la commande sera remplac�e par le nom de la
        //section du niveau demand�. S'il est textuel, le texte en
        //question sera recherch� dans la liste des noms de sections
        //et conserv� seulement si n nom correspond. Dans le cas
        //contraire, la commande sera simplement ignor�e.
        //Ce flux est de type FLUX_NIVEAU.
};
/* Commande d�sign�e pour indiquer o� remplacer par un nom de section
 * dans un format de r�f�rence.
 */

Resultat commandesec_initialisation(TRAVAIL(CommandeSec) commandesec);
/* Cr�e une commande sec vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesec_definition_localisationfichier(TRAVAIL(CommandeSec) commandesec, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande sec.
 * Renvoie RESULTAT_ERREUR si commandesec est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesec_definition_niveau(TRAVAIL(CommandeSec) commandesec, TRAVAIL(Flux) niveau);
/* Assigne un niveau � une commande sec.
 * Renvoie RESULTAT_ERREUR si commandesec est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesec_lecture_localisationfichier(TRAVAIL(CommandeSec) commandesec, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande sec.
 * Renvoie RESULTAT_ERREUR si commandesec est NULL.
 */

Resultat commandesec_lecture_niveau(TRAVAIL(CommandeSec) commandesec, REFERENCE(Flux) niveau);
/* Lit un niveau � une commande sec.
 * Renvoie RESULTAT_ERREUR si commandesec est NULL.
 */

Resultat commandesec_parcours(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandesec_copie(TRAVAIL(CommandeSec) commandesec, TRAVAIL(CommandeSec) copie);
/* Cr�e une copie de la commande sec.
 * Renvoie RESULTAT_ERREUR si commandesec est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandesec_destruction(TRAVAIL(CommandeSec) commandesec);
/* D�truit une commande sec.
 */

#endif
