/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDESECTION__
#define __COMMANDESECTION__

#include <src/global/global.h>

typedef struct commandesection CONTENEUR(CommandeSection);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandesection
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du format de section dans le source.
        STOCKAGE(Flux) niveau;
        //Flux indiquant le niveau o� le format s'applique.
        //Si ce flux vaut NULL, ce format sera le format
        //g�n�rique.
        //Ce flux est de type FLUX_NIVEAU.
        STOCKAGE(Flux) nom;
        //Nom de section.
        //Ce flux est de type FLUX_SECTION_NOM.
        STOCKAGE(Flux) formatavant;
        //Format de section avant sous sections.
        //Ce flux est de type FLUX_SECTION_FORMAT.
        STOCKAGE(Flux) formatapres;
        //Format de section apr�s sous sections.
        //Ce flux est de type FLUX_SECTION_FORMAT.
        STOCKAGE(Flux) presection;
        //Texte pr�c�dent la section.
        //Ce flux est de type FLUX_SECTION_SECTION.
        STOCKAGE(Flux) postsection;
        //Texte suivant la section.
        //Ce flux est de type FLUX_SECTION_SECTION.
};
/* Commande d�sign�e pour indiquer comment formatter les ent�tes de sections
 * dans le document.
 */

Resultat commandesection_initialisation(TRAVAIL(CommandeSection) commandesection);
/* Cr�e une commande de format de section vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_localisationfichier(TRAVAIL(CommandeSection) commandesection, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_niveau(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) niveau);
/* Assigne un niveau � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_nom(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) nom);
/* Assigne un nom � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_formatavant(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) formatavant);
/* Assigne un format avant � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_formatapres(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) formatapres);
/* Assigne un format apr�s � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_presection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) presection);
/* Assigne un texte de pr�section � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_definition_postsection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(Flux) postsection);
/* Assigne un texte de postsection � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandesection_lecture_localisationfichier(TRAVAIL(CommandeSection) commandesection, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_niveau(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) niveau);
/* Lit un niveau � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_nom(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) nom);
/* Lit un nom � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_formatavant(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) formatavant);
/* Lit un format avant � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_formatapres(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) formatapres);
/* Lit un format apr�s � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_presection(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) presection);
/* Lit un texte de pr�section � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_lecture_postsection(TRAVAIL(CommandeSection) commandesection, REFERENCE(Flux) postsection);
/* Lit un texte de postsection � une commande de format de section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL.
 */

Resultat commandesection_parcours(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandesection_copie(TRAVAIL(CommandeSection) commandesection, TRAVAIL(CommandeSection) copie);
/* Cr�e une copie de la commande section.
 * Renvoie RESULTAT_ERREUR si commandesection est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandesection_destruction(TRAVAIL(CommandeSection) commandesection);
/* D�truit une commande de format de section.
 */

#endif
