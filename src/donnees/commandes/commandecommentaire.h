/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDECOMMENTAIRE__
#define __COMMANDECOMMENTAIRE__

#include <src/global/global.h>

typedef struct commandecommentaire CONTENEUR(CommandeCommentaire);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandecommentaire
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation du commentaire dans le source.
        STOCKAGE(Flux) commentaire;
        //Texte du commentaire.
        //Ce flux est de type FLUX_COMMENTAIRE.
};
/* Commande d�sign�e pour recevoir des commentaires dans le code source.
 * A priori, cette commande sera inusit�e.
 */

Resultat commandecommentaire_initialisation(TRAVAIL(CommandeCommentaire) commandecommentaire);
/* Cr�e une commande de commentaire vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandecommentaire_definition_localisationfichier(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande de commentaire.
 * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandecommentaire_definition_commentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(Flux) commentaire);
/* Assigne un commentaire dans une commande d�sign�e.
 * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandecommentaire_lecture_localisationfichier(TRAVAIL(CommandeCommentaire) commandecommentaire, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande de commentaire.
 * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
 */

Resultat commandecommentaire_lecture_commentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, REFERENCE(Flux) commentaire);
/* Lit un commentaire.
 * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL.
 */

Resultat commandecommentaire_parcours(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandecommentaire_copie(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(CommandeCommentaire) copie);
/* Cr�e une copie de la commande de commentaire.
 * Si la copie n'est pas vide, elle est d�truite avant.
 * Renvoie RESULTAT_ERREUR si commandecommentaire est NULL, et RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandecommentaire_destruction(TRAVAIL(CommandeCommentaire) commandecommentaire);
/* D�truit une commande de commentaire.
 */

#endif
