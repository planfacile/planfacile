/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandeidea.h"

Resultat commandeidea_initialisation(TRAVAIL(CommandeIdea) commandeidea)
{
        /* Cr�e une commande d'id�e vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION((S_T(commandeidea)=NOUVEAU(CommandeIdea))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandeidea, localisation)=NULL;
        CHAMP(commandeidea, reference)=NULL;
        CHAMP(commandeidea, titre)=NULL;
        CHAMP(commandeidea, texte)=NULL;
        return RESULTAT_OK;
}

Resultat commandeidea_definition_localisationfichier(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande idea.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandeidea, localisation)));
        return RESULTAT_OK;
}

Resultat commandeidea_definition_reference(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) reference)
{
        /* Assigne un nom de r�f�rence � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(reference,CHAMP_TRAVAIL(commandeidea, reference)));
        return RESULTAT_OK;
}

Resultat commandeidea_definition_titre(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) titre)
{
        /* Assigne un titre � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(titre,CHAMP_TRAVAIL(commandeidea, titre)));
        return RESULTAT_OK;
}

Resultat commandeidea_definition_texte(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) texte)
{
        /* Assigne un texte � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_copie(texte,CHAMP_TRAVAIL(commandeidea, texte)));
        return RESULTAT_OK;
}

Resultat commandeidea_lecture_localisationfichier(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande idea.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandeidea, localisation);
        return RESULTAT_OK;
}

Resultat commandeidea_lecture_reference(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) reference)
{
        /* Lit un nom de r�f�rence � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        T_R(reference)=CHAMP_TRAVAIL(commandeidea, reference);
        return RESULTAT_OK;
}

Resultat commandeidea_lecture_titre(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) titre)
{
        /* Lit un titre � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        T_R(titre)=CHAMP_TRAVAIL(commandeidea, titre);
        return RESULTAT_OK;
}

Resultat commandeidea_lecture_texte(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) texte)
{
        /* Lit un texte � une id�e.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        T_R(texte)=CHAMP_TRAVAIL(commandeidea, texte);
        return RESULTAT_OK;
}

Resultat commandeidea_parcours(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeidea, reference),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeidea, titre),action,general));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(commandeidea, texte),action,general));
        return RESULTAT_OK;
}

Resultat commandeidea_copie(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(CommandeIdea) copie)
{
        /* Cr�e une copie de la commande idea.
         * Renvoie RESULTAT_ERREUR si commandeidea est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */ 
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeidea_destruction(copie));
        SECURISE(commandeidea_initialisation(copie));
        SECURISE(commandeidea_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandeidea, localisation)));
        SECURISE(commandeidea_definition_reference(copie,CHAMP_TRAVAIL(commandeidea, reference)));
        SECURISE(commandeidea_definition_titre(copie,CHAMP_TRAVAIL(commandeidea, titre)));
        SECURISE(commandeidea_definition_texte(copie,CHAMP_TRAVAIL(commandeidea, texte)));
        return RESULTAT_OK;
}

Resultat commandeidea_destruction(TRAVAIL(CommandeIdea) commandeidea)
{
        /* D�truit une id�e.
         */ 
        if(S_T(commandeidea)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandeidea, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeidea, reference)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeidea, titre)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandeidea, texte)));
        free(S_T(commandeidea));
        S_T(commandeidea)=NULL;
        return RESULTAT_OK;
}

