/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDESTANDARD__
#define __COMMANDESTANDARD__

#include <src/global/global.h>

typedef struct commandestandard CONTENEUR(CommandeStandard);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandestandard
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'inclusion standard dans le source.
};
/* Cette commande est destin�e � marquer l'inclusion du fichier standard.
 * Elle sera � priori inusit�e.
 */

Resultat commandestandard_initialisation(TRAVAIL(CommandeStandard) commandestandard);
/* Cr�e une commande standard vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandestandard_definition_localisationfichier(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande standard.
 * Renvoie RESULTAT_ERREUR si commandestandard est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandestandard_lecture_localisationfichier(TRAVAIL(CommandeStandard) commandestandard, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande standard.
 * Renvoie RESULTAT_ERREUR si commandestandard est NULL.
 */

Resultat commandestandard_parcours(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandestandard_copie(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(CommandeStandard) copie);
/* Cr�e une copie de la commande standard.
 * Renvoie RESULTAT_ERREUR si commandestandard est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandestandard_destruction(TRAVAIL(CommandeStandard) commandestandard);
/* D�truit une commande standard.
 */

#endif
