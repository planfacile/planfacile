/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEIDEA__
#define __COMMANDEIDEA__

#include <src/global/global.h>

typedef struct commandeidea CONTENEUR(CommandeIdea);

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/commandes/localisationfichier.h>

struct commandeidea
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de l'id�e dans le source.
        STOCKAGE(Flux) reference;
        //Nom de r�f�rence de l'id�e.
        //Ce flux est de type FLUX_REFERENCE.
        STOCKAGE(Flux) titre;
        //Titre de l'id�e.
        //Ce flux est de type FLUX_TITRE.
        STOCKAGE(Flux) texte;
        //Texte associ� � l'id�e. (tout �a pour �a !)
        //Ce flux est de type FLUX_TEXTE.
};
/* Commande d�sign�e pour enregistrer une id�e du document.
 */

Resultat commandeidea_initialisation(TRAVAIL(CommandeIdea) commandeidea);
/* Cr�e une commande d'id�e vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeidea_definition_localisationfichier(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande idea.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeidea_definition_reference(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) reference);
/* Assigne un nom de r�f�rence � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeidea_definition_titre(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) titre);
/* Assigne un titre � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeidea_definition_texte(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(Flux) texte);
/* Assigne un texte � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeidea_lecture_localisationfichier(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande idea.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 */

Resultat commandeidea_lecture_reference(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) reference);
/* Lit un nom de r�f�rence � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 */

Resultat commandeidea_lecture_titre(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) titre);
/* Lit un titre � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 */

Resultat commandeidea_lecture_texte(TRAVAIL(CommandeIdea) commandeidea, REFERENCE(Flux) texte);
/* Lit un texte � une id�e.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL.
 */

Resultat commandeidea_parcours(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeidea_copie(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(CommandeIdea) copie);
/* Cr�e une copie de la commande idea.
 * Renvoie RESULTAT_ERREUR si commandeidea est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeidea_destruction(TRAVAIL(CommandeIdea) commandeidea);
/* D�truit une id�e.
 */

#endif
