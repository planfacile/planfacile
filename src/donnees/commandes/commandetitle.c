/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandetitle.h"

Resultat commandetitle_initialisation(TRAVAIL(CommandeTitle) commandetitle)
{
        /* Cr�e une commande title vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandetitle)=NOUVEAU(CommandeTitle))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandetitle, localisation)=NULL;
        return RESULTAT_OK;
}

Resultat commandetitle_definition_localisationfichier(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande title dans le source.
         * Renvoie RESULTAT_ERREUR si commandetitle est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandetitle, localisation)));
        return RESULTAT_OK;
}

Resultat commandetitle_lecture_localisationfichier(TRAVAIL(CommandeTitle) commandetitle, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande title dans le source.
         * Renvoie RESULTAT_ERREUR si commandetitle est NULL.
         */
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandetitle, localisation);
        return RESULTAT_OK;
}

Resultat commandetitle_parcours(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        return RESULTAT_OK;
}

Resultat commandetitle_copie(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(CommandeTitle) copie)
{
        /* Cr�e une copie de la commande title.
         * Renvoie RESULTAT_ERREUR si commandetitle est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetitle_destruction(copie));
        SECURISE(commandetitle_initialisation(copie));
        SECURISE(commandetitle_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandetitle, localisation)));
        return RESULTAT_OK;
}

Resultat commandetitle_destruction(TRAVAIL(CommandeTitle) commandetitle)
{
        /* D�truit une commande title.
         */
        if(S_T(commandetitle)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandetitle, localisation)));
        free(S_T(commandetitle));
        S_T(commandetitle)=NULL;
        return RESULTAT_OK;
}

