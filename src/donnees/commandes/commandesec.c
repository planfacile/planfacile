/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "commandesec.h"

Resultat commandesec_initialisation(TRAVAIL(CommandeSec) commandesec)
{
        /* Cr�e une commande de remplacement par nom de section vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(commandesec)=NOUVEAU(CommandeSec))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(commandesec, localisation)=NULL;
        CHAMP(commandesec, niveau)=NULL;
        return RESULTAT_OK;
}

Resultat commandesec_definition_localisationfichier(TRAVAIL(CommandeSec) commandesec, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande sec.
         * Renvoie RESULTAT_ERREUR si commandesec est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        SECURISE(localisationfichier_copie(localisationfichier,CHAMP_TRAVAIL(commandesec, localisation)));
        return RESULTAT_OK;
}

Resultat commandesec_definition_niveau(TRAVAIL(CommandeSec) commandesec, TRAVAIL(Flux) niveau)
{
        /* Assigne un niveau � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandesec est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        if(S_T(niveau)==NULL)
        {
                SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesec, niveau)));
        }
        else
        {
                SECURISE(flux_copie(niveau,CHAMP_TRAVAIL(commandesec, niveau)));
        }
        return RESULTAT_OK;
}

Resultat commandesec_lecture_localisationfichier(TRAVAIL(CommandeSec) commandesec, REFERENCE(LocalisationFichier) localisationfichier)
{
        /* Indique la position d'une commande sec.
         * Renvoie RESULTAT_ERREUR si commandesec est NULL.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        T_R(localisationfichier)=CHAMP_TRAVAIL(commandesec, localisation);
        return RESULTAT_OK;
}

Resultat commandesec_lecture_niveau(TRAVAIL(CommandeSec) commandesec, REFERENCE(Flux) niveau)
{
        /* Lit un niveau � la commande correspondante.
         * Renvoie RESULTAT_ERREUR si commandesec est NULL.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        T_R(niveau)=CHAMP_TRAVAIL(commandesec, niveau);
        return RESULTAT_OK;
}

Resultat commandesec_parcours(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) action, TRAVAIL(General) general)
{
        /* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
         * Renvoie les m�mes erreurs que flux_parcours,
         * ainsi que RESULTAT_ERREUR si commande est NULL,
         * rien ne se passe si action est NULL.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(commandesec, niveau)!=NULL)
        {
                SECURISE(flux_parcours(CHAMP_TRAVAIL(commandesec, niveau),action,general));
        }
        return RESULTAT_OK;
}

Resultat commandesec_copie(TRAVAIL(CommandeSec) commandesec, TRAVAIL(CommandeSec) copie)
{
        /* Cr�e une copie de la commande sec.
         * Renvoie RESULTAT_ERREUR si commandesec est NULL, et
         * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Si la copie est non vide, elle est au pr�alable supprim�e.
         */
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandesec_destruction(copie));
        SECURISE(commandesec_initialisation(copie));
        SECURISE(commandesec_definition_localisationfichier(copie,CHAMP_TRAVAIL(commandesec, localisation)));
        if(CHAMP(commandesec, niveau)!=NULL)
                SECURISE(commandesec_definition_niveau(copie,CHAMP_TRAVAIL(commandesec, niveau)));
        return RESULTAT_OK;
}

Resultat commandesec_destruction(TRAVAIL(CommandeSec) commandesec)
{
        /* D�truit une commande sec.
         */
        if(S_T(commandesec)==NULL)
                return RESULTAT_OK;
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(commandesec, localisation)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(commandesec, niveau)));
        free(S_T(commandesec));
        S_T(commandesec)=NULL;
        return RESULTAT_OK;
}

