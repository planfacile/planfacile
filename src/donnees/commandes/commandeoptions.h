/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMMANDEOPTIONS__
#define __COMMANDEOPTIONS__

#include <src/global/global.h>

typedef struct commandeoptions CONTENEUR(CommandeOptions);

typedef struct commandeoptionsclauses CONTENEUR(CommandeOptionsClauses);

typedef struct commandeoptionsclause CONTENEUR(CommandeOptionsClause);

#include <src/donnees/general/general.h>

#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/option.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/commandes/commandeoption.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

#define CLAUSENONTROUVEE        -1

struct commandeoptionsclause
{
        STOCKAGE_SCALAIRE(NomOption) option;
        //Nom de l'option dont la clause est tir�e.
        STOCKAGE(Flux) clause;
        //Flux de commandes associ� � la clause.
        //Si le flux est vide, un flux vide devra
        //�tre plac� ici.
        //Ce flux sera de type FLUX_OPTIONS.
        STOCKAGE_SCALAIRE(Booleen) execution;
        //Ce bool�en, plac� � FAUX � la cr�ation, sera mis
        //� VRAI par un algorithme pr�cis, pour savoir si la
        //clause doit �tre incluse dans le flux sup�rieur.
};
/* Cette structure d�crit comment est form�e une clause de
 * la commande d'options.
 */

Resultat commandeoptionsclause_initialisation(TRAVAIL(CommandeOptionsClause) commandeoptionsclause);
/* Cr�e une clause vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptionsclause_definition(TRAVAIL(CommandeOptionsClause) commandeoptionsclause, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL(Flux) clause);
/* D�finit une clause.
 * Renvoie RESULTAT_ERREUR si commandeoptionsclause est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptionsclause_destruction(TRAVAIL(CommandeOptionsClause) commandeoptionsclause);
/* D�truit une clause.
 */

struct commandeoptionsclauses
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille r�serv�e en m�moire pour le tableau.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille utilis�e pour le tableau
        TABLEAU(STOCKAGE(CommandeOptionsClause)) clause;
        //Clauses de la commande.
};
/* Cette structure permet de contenir un nombre arbitraire
 * de clauses de la commande d'options.
 */

Resultat commandeoptionsclauses_initialisation(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses);
/* Cr�e une liste de clauses vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptionsclauses_ajout_clause(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses, TRAVAIL(CommandeOptionsClause) commandeoptionsclause);
/* Ajoute une clause en fin de liste.
 * Renvoie RESULTAT_ERREUR si commandeoptionsclauses est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptionsclauses_destruction(TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses);
/* D�truit une commandeoptionsclauses.
 */

struct commandeoptions
{
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande de choix d'option dans le source.
        STOCKAGE_SCALAIRE(Label) label;
        //Label de la commande. Il pourra ainsi �tre utilis� dans des
        //messages d'erreur.
        STOCKAGE(CommandeOptionsClauses) clauses;
        //Flux de commandes associ�s aux clauses #case{...}
        STOCKAGE(Flux) autres;
        //Flux de commandes associ� � la clause #other.
        //Attention, si la clause #other n'est pas donn�e,
        //un flux vide devra �tre ici donn�.
        //Ce flux sera de type FLUX_OPTIONS.
        STOCKAGE_SCALAIRE(Booleen) execution;
        //Ce bool�en, plac� � FAUX � la cr�ation, sera mis
        //� VRAI par un algorithme pr�cis, pour savoir si la
        //clause par d�faut doit �tre incluse dans le flux sup�rieur.
};
/* Cette commande sera destin�e � recevoir les diff�rentes clauses
 * de s�lection de code selon les options d�finies.
 */

Resultat commandeoptions_initialisation(TRAVAIL(CommandeOptions) commandeoptions);
/* Cr�e une commande d'options vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_definition_localisationfichier(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_definition_label(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(Label) label);
/* Assigne un label � une commande d'options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_ajout_clause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL(Flux) clause);
/* Ajoute une clause � une commande d'options
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_definition_clauses(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(CommandeOptionsClauses) commandeoptionsclauses);
/* Affecte une liste d'option � une commande d'options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_definition_executionclause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, TRAVAIL_SCALAIRE(Booleen) execution);
/* Assigne une valeur au drapeau d'�x�cution de la clause par d�faut.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Si le nom d'option ne correspond � aucun de la liste,
 * le drapeau de la clause par d�faut sera affect�.
 */

Resultat commandeoptions_definition_autres(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Flux) autres);
/* Assigne un flux � la clause autres d'une commande d'options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat commandeoptions_definition_executionautres(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(Booleen) execution);
/* Assigne une valeur au drapeau d'�x�cution de la clause par d�faut.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 */

Resultat commandeoptions_lecture_localisationfichier(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE(LocalisationFichier) localisationfichier);
/* Indique la position d'une commande options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 */

Resultat commandeoptions_lecture_label(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE_SCALAIRE(Label) label);
/* Lit un label � une commande d'options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 */

Resultat commandeoptions_lecture_clause(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL_SCALAIRE(NomOption) option, REFERENCE(Flux) clause, REFERENCE_SCALAIRE(Booleen) execution);
/* Lit une clause � une commande d'options.
 * La clause est recherch�e d'apr�s le nom de l'option pass�e en param�tre.
 * Si l'option ne fait pas partie des clauses, la clause par d�faut est renvoy�e.
 * Si clause ou execution vaut NULL, c'est que l'information en question n'est pas souhait�e.
 * Renvoie RESULTAT_ERREUR si commandeoptions ou option est NULL.
 */

Resultat commandeoptions_lecture_autres(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE(Flux) autres);
/* Lit le flux de la clause autres d'une commande d'options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 */

Resultat commandeoptions_lecture_execution(TRAVAIL(CommandeOptions) commandeoptions, REFERENCE_SCALAIRE(Booleen) execution);
/* Lit une valeur au drapeau d'�x�cution de la clause par d�faut.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL.
 */

Resultat commandeoptions_lecture_clauseexecutable(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Option) option, REFERENCE_SCALAIRE(NomOption) nomoption, REFERENCE(Flux) clause, REFERENCE_SCALAIRE(Booleen) execution);
/* Lit la clause devant �tre execut�e selon la liste d'option donn�e.
 * Si l'option ne fait pas partie des clauses, la clause par d�faut est renvoy�e,
 * avec NULL comme nom d'option.
 * Si clause ou execution vaut NULL, c'est que l'information en question n'est pas souhait�e.
 * Renvoie RESULTAT_ERREUR si commandeoptions ou option est NULL.
 */

Resultat commandeoptions_extraction_fluxexecutable(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(Flux) executable, TRAVAIL(General) general);
/* Renvoie le flux �x�cutable devant remplacer la commande
 * d'options apr�s d�termination des clauses �x�cutables.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Renvoie RESULTAT_ERREUR si commandeoptions vaut NULL.
 */

Resultat commandeoptions_parcours(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) action, TRAVAIL(General) general);
/* R�alise un parcours des flux inclus dans la commande sp�cifi�e.
 * Renvoie les m�mes erreurs que flux_parcours,
 * ainsi que RESULTAT_ERREUR si commande est NULL,
 * rien ne se passe si action est NULL.
 */

Resultat commandeoptions_copie(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(CommandeOptions) copie);
/* Cr�e une copie de la commande options.
 * Renvoie RESULTAT_ERREUR si commandeoptions est NULL, et
 * RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Si la copie est non vide, elle est au pr�alable supprim�e.
 */

Resultat commandeoptions_destruction(TRAVAIL(CommandeOptions) commandeoptions);
/* D�truit une commande d'options.
 */

#endif
