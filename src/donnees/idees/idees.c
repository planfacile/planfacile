/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "idees.h"

#define TAILLEINIT        5
#define MULTTAILLE        2

static Resultat idees_dependances_initialisation(TRAVAIL(IdeeReelleDependances) dependances)
{
        /* Initialise une pile de d�pendances.
         */
        S_T(dependances)=NULL;
        return RESULTAT_OK;
}

static Resultat idees_dependances_ajout(TRAVAIL(IdeeReelleDependances) dependances, TRAVAIL_SCALAIRE(Chaine) dependance, TRAVAIL_SCALAIRE(Pertinence) pertinence, TRAVAIL(LocalisationFichier) localisation)
{
        /* Ajoute un �l�ment dans la pile.
         */
        STOCKAGE(IdeeReelleDependances) nouveau;
        ASSERTION((nouveau=NOUVEAU(IdeeReelleDependances))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        ASSERTION((CHAMP_STOCKAGE(nouveau, dependance)=DUP_CAST(Chaine, dependance))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, pertinence)=S_T_(pertinence);
        CHAMP_STOCKAGE(nouveau, localisation)=NULL;
        SECURISE(localisationfichier_copie(localisation,T_S(CHAMP_STOCKAGE(nouveau, localisation))));
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(dependances);
        S_T(dependances)=nouveau;
        return RESULTAT_OK;
}

static Resultat idees_dependances_retrait(TRAVAIL(IdeeReelleDependances) dependances, COREFERENCE_SCALAIRE(Chaine) dependance, COREFERENCE_SCALAIRE(Pertinence) pertinence, COREFERENCE(LocalisationFichier) localisation)
{
        /* Retire une d�pendance. La pile DOIT
         * contenir au moins un �l�ment.
         */
        STOCKAGE(IdeeReelleDependances) supprime;
        ASSERTION(S_T(dependances)!=NULL, RESULTAT_ERREUR);

        supprime=S_T(dependances);
        S_C_(dependance)=CHAMP_STOCKAGE(supprime, dependance);
        S_C_(pertinence)=CHAMP_STOCKAGE(supprime, pertinence);
        S_C(localisation)=CHAMP_STOCKAGE(supprime, localisation);
        S_T(dependances)=CHAMP_STOCKAGE(supprime, suivant);
        free(supprime);
        return RESULTAT_OK;
}

static Resultat idees_dependances_destruction(TRAVAIL(IdeeReelleDependances) dependances)
{
        /* D�truit une pile de d�pendances.
         */
        if(S_T(dependances)==NULL)
                return RESULTAT_OK;
        SECURISE(idees_dependances_destruction(CHAMP_TRAVAIL(dependances, suivant)));
        free(CHAMP(dependances, dependance));
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(dependances, localisation)));
        free(S_T(dependances));
        S_T(dependances)=NULL;
        return RESULTAT_OK;
}

static Resultat idees_dependances_copie_interne(TRAVAIL(IdeeReelleDependances) dependances, TRAVAIL(IdeeReelleDependances) copie)
{
        /* Copie les maillons de la pile de d�pendances.
         */
        if(S_T(dependances)==NULL)
        {
                S_T(copie)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(idees_dependances_copie_interne(CHAMP_TRAVAIL(dependances, suivant),copie));
        SECURISE(idees_dependances_ajout(copie,T_S_(CHAMP(dependances, dependance)),T_S_(CHAMP(dependances, pertinence)),CHAMP_TRAVAIL(dependances, localisation)));
        return RESULTAT_OK;
}

static Resultat idees_dependances_copie(TRAVAIL(IdeeReelleDependances) dependances, TRAVAIL(IdeeReelleDependances) copie)
{
        /* Copie une pile de d�pendances.
         */
        SECURISE(idees_dependances_destruction(copie));
        SECURISE(idees_dependances_initialisation(copie));
        SECURISE(idees_dependances_copie_interne(dependances,copie));
        return RESULTAT_OK;
}

Resultat idees_initialisation(TRAVAIL(Idees) idees)
{
        /* Initialise une structure d'id�es.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION((S_T(idees)=NOUVEAU(Idees))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(idees, idees), idee)=NOUVEAUX_(IdeeReelle, TAILLEINIT);
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(idees, idees), memoire)=TAILLEINIT;
        CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)=0;
        CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee)=NOUVEAUX(CommandeMissing, T_S_(TAILLEINIT));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)=TAILLEINIT;
        for(indice=0 ; T_S_(indice)<T_S_(TAILLEINIT) ; T_S_(indice)++)
                ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), indice)=NULL;
        CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)=NULL;
        CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee)=NOUVEAUX(CommandeGeneric, T_S_(TAILLEINIT));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire)=TAILLEINIT;
        CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)=0;
        CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)=NULL;
        return RESULTAT_OK;
}

Resultat idees_idees_definition_idee(TRAVAIL(Idees) idees, TRAVAIL(CommandeIdea) commandeidea, REFERENCE_SCALAIRE(IdIdee) ididee, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit une id�e.
         * L'ididee de l'id�e est renvoy� dans l'ididee,
         * et le bool�en de remplacement indique si une
         * d�finition pr�alable de l'id�e existait.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        TRAVAIL_SCALAIRE(Chaine) reference;
        TRAVAIL(Flux) referenceidee;
        TRAVAIL_SCALAIRE(Booleen) correct;
        TRAVAIL_SCALAIRE(IdIdee) idideerecherche;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(referenceidee)));
        SECURISE(flux_texte(referenceidee,R_T_(reference)));
        SECURISE(idees_idees_recherche_idee(idees,reference,R_T_(correct),R_T_(idideerecherche)));
        free(S_T_(reference));
        if(correct==VRAI)
        {
                S_C_(remplacement)=VRAI;
                T_R_(ididee)=idideerecherche;
                SECURISE(commandeidea_copie(commandeidea,T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), idideerecherche), commandeidea))));
                SECURISE(idees_dependances_destruction(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), idideerecherche), dependances))));
                SECURISE(idees_dependances_initialisation(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), idideerecherche), dependances))));
        }
        else
        {
                S_C_(remplacement)=FAUX;
                if(T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille))>=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), memoire)))
                {
                        TABLEAU_(STOCKAGE_SCALAIRE(IdeeReelle)) nouveau;
                        nouveau=REALLOCATION_CAST_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), IdeeReelle, T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), memoire))*T_S_(MULTTAILLE));
                        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        CHAMP_STOCKAGE_(CHAMP(idees, idees),idee)=nouveau;
                        CHAMP_STOCKAGE_(CHAMP(idees, idees),memoire)*=T_S_(MULTTAILLE);
                }
                T_R_(ididee)=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille))++;
                CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_R_(ididee)), commandeidea)=NULL;
                SECURISE(commandeidea_copie(commandeidea,T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_R_(ididee)), commandeidea))));
                SECURISE(idees_dependances_initialisation(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_R_(ididee)), dependances))));
        }
        return RESULTAT_OK;
}

Resultat idees_idees_lecture_idee(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, REFERENCE(CommandeIdea) commandeidea)
{
        /* Lit une id�e.
         * Renvoie RESULTAT_ERREUR_DOMAINE si l'ididee est incorrect.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         */
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(ididee>=T_S_(0), RESULTAT_ERREUR_DOMAINE);
        ASSERTION(ididee<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)), RESULTAT_ERREUR_DOMAINE);

        T_R(commandeidea)=T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), ididee), commandeidea));
        return RESULTAT_OK;
}

Resultat idees_idees_nombre(TRAVAIL(Idees) idees, REFERENCE_SCALAIRE(Taille) nombre)
{
        /* Donne le nombre d'id�es pr�sentes dans la structure.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         */
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        T_R_(nombre)=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille));
        return RESULTAT_OK;
}

Resultat idees_idees_recherche_idee(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, REFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(IdIdee) ididee)
{
        /* Renvoie l'ididee d'une id�e en fonction de
         * sa r�f�rence.
         * Si la r�f�rence n'�tait pas celle d'une
         * id�e valide, le bool�en est plac� � FAUX.
         * Dans le cas contraire, il est plac� � VRAI,
         * et l'ididee est renvoy�.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)) ; T_S_(indice)++)
        {
                TRAVAIL_SCALAIRE(Chaine) referenceidee;
                TRAVAIL(Flux) fluxreference;
                SECURISE(commandeidea_lecture_reference(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), commandeidea)),R_T(fluxreference)));
                SECURISE(flux_texte(fluxreference,R_T_(referenceidee)));
                if(STRCMP(reference, referenceidee)==T_S_(0))
                {
                        free(S_T_(referenceidee));
                        T_R_(correct)=T_S_(VRAI);
                        T_R_(ididee)=T_S_(indice);
                        return RESULTAT_OK;
                }
                free(S_T_(referenceidee));
        }
        T_R_(correct)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat idees_idees_ajout_dependance(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, TRAVAIL_SCALAIRE(Chaine) dependance, TRAVAIL_SCALAIRE(Pertinence) pertinence, TRAVAIL(LocalisationFichier) localisation)
{
        /* Ajoute une d�pendance � une id�e.
         * Si l'ididee est incorrect, la
         * fonction renvoie RESULTAT_ERREUR.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(ididee>=T_S_(0), RESULTAT_ERREUR_DOMAINE);
        ASSERTION(ididee<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)), RESULTAT_ERREUR_DOMAINE);

        SECURISE(idees_dependances_ajout(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), ididee), dependances)),dependance,pertinence,localisation));
        return RESULTAT_OK;
}

Resultat idees_idees_retrait_dependance(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Chaine) dependance, COREFERENCE_SCALAIRE(Pertinence) pertinence, COREFERENCE(LocalisationFichier) localisation)
{
        /* Retire une d�pendance � une id�e.
         * Si l'ididee est incorrect, la
         * fonction renvoie RESULTAT_ERREUR_DOMAINE.
         * Le bool�en correct est mis � FAUX s'il
         * n'y avait plus de d�pendances.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(ididee>=T_S_(0), RESULTAT_ERREUR_DOMAINE);
        ASSERTION(ididee<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)), RESULTAT_ERREUR_DOMAINE);

        if(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), ididee), dependances)==NULL)
        {
                S_C_(correct)=FAUX;
        }
        else
        {
                S_C_(correct)=VRAI;
                SECURISE(idees_dependances_retrait(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), ididee), dependances)),dependance,pertinence,localisation));
        }
        return RESULTAT_OK;
}

Resultat idees_manquantes_definition(TRAVAIL(Idees) idees, TRAVAIL(CommandeMissing) commandemissing, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Booleen) defaut, REFERENCE_SCALAIRE(Indice) indice, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit une id�e manquante.
         * La fonction indique si l'indice contenu dans
         * l'id�e �tait correct ou non, si l'id�e par d�faut
         * a �t� modifi�e, et, enfin, si l'id�e d�finie en remplace
         * une autre.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        TRAVAIL(Flux) fluxindice;
        STOCKAGE_SCALAIRE(Booleen) nombre;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemissing_lecture_indice(commandemissing,R_T(fluxindice)));
        if(S_T(fluxindice)==NULL)
        {
                S_C_(defaut)=VRAI;
                if(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                SECURISE(commandemissing_copie(commandemissing,T_S(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut))));
                S_C_(correct)=VRAI;
        }
        else
        {
                S_C_(defaut)=FAUX;
                SECURISE(flux_nombre(fluxindice,C_S_(nombre),indice));
                if((T_S_(nombre)==T_S_(FAUX))||(T_R_(indice)<=T_S_(0)))
                {
                        S_C_(correct)=FAUX;
                        return RESULTAT_OK;
                }
                if(T_R_(indice)>=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)))
                {
                        TABLEAU(STOCKAGE(CommandeMissing)) nouveau;
                        nouveau=REALLOCATION_CAST(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), CommandeMissing, T_R_(indice)+T_S_(1));
                        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee)=nouveau;
                        for( ; T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille))<=T_R_(indice) ; T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille))++)
                                ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)))=NULL;
                }
                if(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_R_(indice))!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                SECURISE(commandemissing_copie(commandemissing,T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_R_(indice)))));
                S_C_(correct)=VRAI;
        }
        return RESULTAT_OK;
}

Resultat idees_manquantes_lecture(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Indice) indice, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(CommandeMissing) commandemissing)
{
        /* Lit une id�e manquante.
         * S'il n'y a pas de d�finition explicite, la
         * valeur par d�faut est utilis�e. Dans ce cas,
         * si celle-ci n'est pas d�finie, le bool�en est
         * �galement mis � FAUX.
         * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est incorrect.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         */
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(indice>T_S_(0), RESULTAT_ERREUR_DOMAINE);

        if(indice>=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)))
        {
                if(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)==NULL)
                        S_C_(correct)=FAUX;
                else
                {
                        S_C_(correct)=VRAI;
                        T_R(commandemissing)=T_S(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut));
                }
        }
        else
        {
                if(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), indice)==NULL)
                {
                        if(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)==NULL)
                                S_C_(correct)=FAUX;
                        else
                        {
                                S_C_(correct)=VRAI;
                                T_R(commandemissing)=T_S(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut));
                        }
                }
                else
                {
                        S_C_(correct)=VRAI;
                        T_R(commandemissing)=T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), indice));
                }
        }
        return RESULTAT_OK;
}

Resultat idees_generiques_definition(TRAVAIL(Idees) idees, TRAVAIL(CommandeGeneric) commandegeneric, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Booleen) defaut, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit une id�e g�n�rique.
         * La fonction indique si la r�f�rence contenue dans
         * l'id�e �tait correcte ou non, si l'id�e par d�faut
         * a �t� modifi�e, et, enfin, si l'id�e d�finie en remplace
         * une autre.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
         * d'une allocation m�moire.
         */
        TRAVAIL(Flux) fluxreference;
        TRAVAIL_SCALAIRE(Chaine) reference;
        TRAVAIL_SCALAIRE(Chaine) referenceindice;
        TRAVAIL_SCALAIRE(Booleen) ideeexistante;
        TRAVAIL_SCALAIRE(Indice) indiceidee;
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandegeneric_lecture_indice(commandegeneric,R_T(fluxreference)));
        if(S_T(fluxreference)==NULL)
        {
                S_C_(defaut)=VRAI;
                if(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                SECURISE(commandegeneric_copie(commandegeneric,T_S(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut))));
                S_C_(correct)=VRAI;
        }
        else
        {
                S_C_(defaut)=FAUX;
                SECURISE(flux_texte(fluxreference,R_T_(reference)));
                SECURISE(idees_idees_recherche_idee(idees,reference,R_T_(ideeexistante),R_T_(indiceidee)));
                if(ideeexistante==T_S_(FAUX))
                {
                        free(S_T_(reference));
                        S_C_(correct)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(correct)=VRAI;
                for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)) ; T_S_(indice)++)
                {
                        SECURISE(commandegeneric_lecture_indice(T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice))),R_T_(fluxreference)));
                        SECURISE(flux_texte(fluxreference,R_T_(referenceindice)));
                        if(STRCMP(reference, referenceindice)==T_S_(0))
                        {
                                free(S_T_(reference));
                                free(S_T_(referenceindice));
                                S_C_(remplacement)=VRAI;
                                SECURISE(commandegeneric_copie(commandegeneric,T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice)))));
                                return RESULTAT_OK;
                        }
                        free(S_T_(referenceindice));
                }
                S_C_(remplacement)=FAUX;
                if(T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille))>=T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire)))
                {
                        TABLEAU(STOCKAGE(CommandeGeneric)) nouveau;
                        nouveau=REALLOCATION_CAST(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), CommandeGeneric, T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire))*T_S_(MULTTAILLE));
                        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee)=nouveau;
                        CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire)*=T_S_(MULTTAILLE);
                }
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)))=NULL;
                SECURISE(commandegeneric_copie(commandegeneric,T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille))++))));
                free(S_T_(reference));
        }
        return RESULTAT_OK;
}

Resultat idees_generiques_lecture(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(CommandeGeneric) commandegeneric)
{
        /* Lit une id�e g�n�rique.
         * S'il n'y a pas de d�finition explicite, la
         * valeur par d�faut est utilis�e. Dans ce cas,
         * si celle-ci n'est pas d�finie, le bool�en est
         * �galement mis � FAUX.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         */
        TRAVAIL(Flux) fluxreference;
        TRAVAIL_SCALAIRE(Chaine) referenceindice;
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)) ; T_S_(indice)++)
        {
                SECURISE(commandegeneric_lecture_indice(T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), indice)),R_T(fluxreference)));
                SECURISE(flux_texte(fluxreference,R_T_(referenceindice)));
                if(STRCMP(reference, referenceindice)==T_S_(0))
                {
                        free(S_T_(referenceindice));
                        S_C_(correct)=VRAI;
                        T_R(commandegeneric)=T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), indice));
                        return RESULTAT_OK;
                }
                free(S_T_(referenceindice));
        }
        if(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)==NULL)
        {
                S_C_(correct)=FAUX;
        }
        else
        {
                S_C_(correct)=VRAI;
                T_R(commandegeneric)=T_S(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut));
        }
        return RESULTAT_OK;
}

Resultat idees_recherche_type(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(TypeIdee) typeidee)
{
        /* Recherche le type d'une id�e en fonction de sa r�f�rence.
         * La priorit� des types est IDEE_PRESENTE, IDEE_MANQUANTE et
         * IDEE_GENERIQUE.
         * Si la r�f�rence n'est pas trouv�e, le bool�en correct est
         * mis a FAUX.
         * Renvoie RESULTAT_ERREUR si idees ou reference est NULL.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T_(reference)!=NULL, RESULTAT_ERREUR);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)) ; T_S_(indice)++)
        {
                TRAVAIL_SCALAIRE(Chaine) referenceidee;
                TRAVAIL(Flux) fluxreference;
                SECURISE(commandeidea_lecture_reference(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), commandeidea)),R_T(fluxreference)));
                SECURISE(flux_texte(fluxreference,R_T_(referenceidee)));
                if(STRCMP(reference, referenceidee)==T_S_(0))
                {
                        free(S_T_(referenceidee));
                        T_R_(correct)=T_S_(VRAI);
                        T_R_(typeidee)=T_S_(IDEE_PRESENTE);
                        return RESULTAT_OK;
                }
                free(S_T_(referenceidee));
        }
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)) ; T_S_(indice)++)
        {
                TRAVAIL_SCALAIRE(Chaine) referenceidee;
                TRAVAIL(Flux) fluxreference;
                SECURISE(commandemissing_lecture_reference(T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(indice))),R_T(fluxreference)));
                SECURISE(flux_texte(fluxreference,R_T_(referenceidee)));
                if(STRCMP(reference, referenceidee)==T_S_(0))
                {
                        free(S_T_(referenceidee));
                        T_R_(correct)=T_S_(VRAI);
                        T_R_(typeidee)=T_S_(IDEE_MANQUANTE);
                        return RESULTAT_OK;
                }
                free(S_T_(referenceidee));
        }
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)) ; T_S_(indice)++)
        {
                TRAVAIL_SCALAIRE(Chaine) referenceidee;
                TRAVAIL(Flux) fluxreference;
                SECURISE(commandegeneric_lecture_reference(T_S(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice))),R_T(fluxreference)));
                SECURISE(flux_texte(fluxreference,R_T_(referenceidee)));
                if(STRCMP(reference, referenceidee)==T_S_(0))
                {
                        free(S_T_(referenceidee));
                        T_R_(correct)=T_S_(VRAI);
                        T_R_(typeidee)=T_S_(IDEE_GENERIQUE);
                        return RESULTAT_OK;
                }
                free(S_T_(referenceidee));
        }
        T_R_(correct)=T_S_(FAUX);
        return RESULTAT_OK;
}


Resultat idees_copie(TRAVAIL(Idees) idees, TRAVAIL(Idees) copie)
{
        /* R�alise une copie d'une structure d'id�es.
         * Si copie est non NULL, la valeur est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR si idees est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(idees)!=NULL, RESULTAT_ERREUR);

        SECURISE(idees_destruction(copie));
        ASSERTION((S_T(copie)=NOUVEAU(Idees))==NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(copie, idees), idee)=NOUVEAUX_(IdeeReelle, T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), memoire)));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(copie, idees), memoire)=CHAMP_STOCKAGE_(CHAMP(idees, idees), memoire);
        CHAMP_STOCKAGE_(CHAMP(copie, idees), taille)=CHAMP_STOCKAGE_(CHAMP(idees, idees), taille);
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(copie, idees), taille)) ; T_S_(indice)++)
        {
                CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee), T_S_(indice)), commandeidea)=NULL;
                if(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee), T_S_(indice)), commandeidea)!=NULL)
                {
                        SECURISE(commandeidea_copie(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), commandeidea)),
                                                T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee), T_S_(indice)), commandeidea))));
                }
                CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee), T_S_(indice)), dependances)=NULL;
                SECURISE(idees_dependances_copie(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), dependances)),
                                        T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(copie, idees), idee), T_S_(indice)), dependances))));
        }
        CHAMP_STOCKAGE_(CHAMP(copie, manquantes), idee)=NOUVEAUX(CommandeMissing, T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, manquantes), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(copie, manquantes), taille)=CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille);
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(copie, manquantes), taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, manquantes), idee), T_S_(indice))=NULL;
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(indice))!=NULL)
                {
                        SECURISE(commandemissing_copie(T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(indice))),
                                                T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, manquantes), idee), T_S_(indice)))));
                }
        }
        CHAMP_STOCKAGE_(CHAMP(copie, manquantes), defaut)=NULL;
        if(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)!=NULL)
        {
                SECURISE(commandemissing_copie(T_S(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)),T_S(CHAMP_STOCKAGE_(CHAMP(copie, manquantes), defaut))));
        }
        CHAMP_STOCKAGE_(CHAMP(copie, generiques), idee)=NOUVEAUX(CommandeGeneric, T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire)));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, generiques), idee)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE_(CHAMP(copie, generiques), memoire)=CHAMP_STOCKAGE_(CHAMP(idees, generiques), memoire);
        CHAMP_STOCKAGE_(CHAMP(copie, generiques), taille)=CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille);
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(copie, generiques), taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, generiques), idee), T_S_(indice))=NULL;
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice))!=NULL)
                {
                        SECURISE(commandegeneric_copie(T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice))),
                                                T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, generiques), idee), T_S_(indice)))));
                }
        }
        CHAMP_STOCKAGE_(CHAMP(copie, generiques), defaut)=NULL;
        if(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)!=NULL)
        {
                SECURISE(commandegeneric_copie(T_S(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)),T_S(CHAMP_STOCKAGE_(CHAMP(copie, generiques), defaut))));
        }
        return RESULTAT_OK;
}

Resultat idees_destruction(TRAVAIL(Idees) idees)
{
        /* D�truit une structure d'id�es.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(idees)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, idees), taille)) ; T_S_(indice)++)
        {
                if(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), commandeidea)!=NULL)
                {
                        SECURISE(commandeidea_destruction(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), commandeidea))));
                }
                SECURISE(idees_dependances_destruction(T_S(CHAMP_STOCKAGE_(ELEMENT_(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee), T_S_(indice)), dependances))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(idees, idees), idee));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), taille)) ; T_S_(indice)++)
        {
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(indice))!=NULL)
                {
                        SECURISE(commandemissing_destruction(T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee), T_S_(indice)))));
                }
        }
        if(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut)!=NULL)
        {
                SECURISE(commandemissing_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), defaut))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(idees, manquantes), idee));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(idees, generiques), taille)) ; T_S_(indice)++)
        {
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice))!=NULL)
                {
                        SECURISE(commandegeneric_destruction(T_S(ELEMENT(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee), T_S_(indice)))));
                }
        }
        if(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut)!=NULL)
        {
                SECURISE(commandegeneric_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(idees, generiques), defaut))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(idees, generiques), idee));
        free(S_T(idees));
        S_T(idees)=NULL;
        return RESULTAT_OK;
}

