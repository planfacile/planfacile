/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __IDEES__
#define __IDEES__

#include <src/global/global.h>

typedef struct idees CONTENEUR(Idees);

#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/commandemissing.h>
#include <src/donnees/commandes/commandegeneric.h>
#include <src/donnees/commandes/localisationfichier.h>

typedef struct ideereelledependances CONTENEUR(IdeeReelleDependances);

struct ideereelledependances
{
        STOCKAGE_SCALAIRE(Chaine) dependance;
        //Nom de l'id�e destination de
        //la d�pendance.
        STOCKAGE_SCALAIRE(Pertinence) pertinence;
        //Pertinence de la d�pendance.
        STOCKAGE(LocalisationFichier) localisation;
        //Localisation de la commande #dep
        //permettant de sp�cifier cette
        //d�pendance.
        STOCKAGE(IdeeReelleDependances) suivant;
        //D�pendance suivante de l'id�e.
};
/* Structure permettant d'enregistrer les
 * d�pendances d'une id�e.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

typedef struct ideereelle CONTENEUR_SCALAIRE(IdeeReelle);

struct ideereelle
{
        STOCKAGE(CommandeIdea) commandeidea;
        //Id�e enregistr�e.
        STOCKAGE(IdeeReelleDependances) dependances;
        //D�pendances pr�sentes dans l'id�e.
        //Ces d�pendances serviront � construire
        //le graphe de d�pendances.
};
/* Structure d'enregistrement d'une id�e.
 * On y ajoute une liste des d�pendances
 * permettant de faciliter le calcul du graphe
 * de d�pendances. Cependant, les d�pendances
 * restent pr�sentes dans le flux de texte de
 * l'id�e, afin de rep�rer leur position.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

typedef struct ideesreelles CONTENEUR_SCALAIRE(IdeesReelles);

struct ideesreelles
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille m�moire occup�e par
        //les id�es du document.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�elle utilis�e par les
        //id�es.
        TABLEAU_(STOCKAGE_SCALAIRE(IdeeReelle)) idee;
        //Id�es du document.
        //Elles sont simplement ajout�es
        //dans l'ordre o� elles sont
        //d�finies.
};
/* Structure servant � enregistrer les id�es du
 * document.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

typedef struct ideesmanquantes CONTENEUR_SCALAIRE(IdeesManquantes);

struct ideesmanquantes
{
        TABLEAU(STOCKAGE(CommandeMissing)) idee;
        //Id�es manquantes r�solues.
        //Les id�es sont rang�es selon
        //l'indice correspondant � leur
        //premier param�tre.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille du tableau pr�c�dent.
        STOCKAGE(CommandeMissing) defaut;
        //Commande d'id�e manquante par
        //d�faut.
};
/* Structure servant � enregistrer les id�es
 * manquantes du document.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

typedef struct ideesgeneriques CONTENEUR_SCALAIRE(IdeesGeneriques);

struct ideesgeneriques
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille m�moire occup�e par
        //les id�es g�n�riques du document.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�elle utilis�e par les
        //id�es g�n�riques.
        TABLEAU(STOCKAGE(CommandeGeneric)) idee;
        //Id�es g�n�riques r�solues.
        //Elles sont simplement ajout�es
        //dans l'ordre o� elles sont
        //d�finies.
        STOCKAGE(CommandeGeneric) defaut;
        //Commande d'id�e g�n�rique par
        //d�faut.
};
/* Structure servant � enregistrer les id�es
 * g�n�riques du document.
 * Cette structure sera utilis�e en interne. Elle
 * n'est donn�e qu'� titre indicatif.
 */

struct idees
{
        STOCKAGE_SCALAIRE(IdeesReelles) idees;
        //Id�es du document.
        STOCKAGE_SCALAIRE(IdeesManquantes) manquantes;
        //Id�es manquantes du document.
        STOCKAGE_SCALAIRE(IdeesGeneriques) generiques;
        //Id�es g�n�riques du document.
};
/* Structure servant � enregistrer toutes les
 * id�es du document.
 */

Resultat idees_initialisation(TRAVAIL(Idees) idees);
/* Initialise une structure d'id�es.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_idees_definition_idee(TRAVAIL(Idees) idees, TRAVAIL(CommandeIdea) commandeidea, REFERENCE_SCALAIRE(IdIdee) ididee, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit une id�e.
 * L'ididee de l'id�e est renvoy� dans l'ididee,
 * et le bool�en de remplacement indique si une
 * d�finition pr�alable de l'id�e existait.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_idees_lecture_idee(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, REFERENCE(CommandeIdea) commandeidea);
/* Lit une id�e.
 * Renvoie RESULTAT_ERREUR_DOMAINE si l'ididee est incorrect.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 */

Resultat idees_idees_nombre(TRAVAIL(Idees) idees, REFERENCE_SCALAIRE(Taille) nombre);
/* Donne le nombre d'id�es pr�sentes dans la structure.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 */

Resultat idees_idees_recherche_idee(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, REFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(IdIdee) ididee);
/* Renvoie l'ididee d'une id�e en fonction de
 * sa r�f�rence.
 * Si la r�f�rence n'�tait pas celle d'une
 * id�e valide, le bool�en est plac� � FAUX.
 * Dans le cas contraire, il est plac� � VRAI,
 * et l'ididee est renvoy�.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 */

Resultat idees_idees_ajout_dependance(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, TRAVAIL_SCALAIRE(Chaine) dependance, TRAVAIL_SCALAIRE(Pertinence) pertinence, TRAVAIL(LocalisationFichier) localisation);
/* Ajoute une d�pendance � une id�e.
 * Si l'ididee est incorrect, la
 * fonction renvoie RESULTAT_ERREUR_DOMAINE.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_idees_retrait_dependance(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(IdIdee) ididee, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Chaine) dependance, COREFERENCE_SCALAIRE(Pertinence) pertinence, COREFERENCE(LocalisationFichier) localisation);
/* Retire une d�pendance � une id�e.
 * Si l'ididee est incorrect, la
 * fonction renvoie RESULTAT_ERREUR_DOMAINE.
 * Le bool�en correct est mis � FAUX s'il
 * n'y avait plus de d�pendances.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_manquantes_definition(TRAVAIL(Idees) idees, TRAVAIL(CommandeMissing) commandemissing, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Booleen) defaut, REFERENCE_SCALAIRE(Indice) indice, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit une id�e manquante.
 * La fonction indique si l'indice contenu dans
 * l'id�e �tait correct ou non, si l'id�e par d�faut
 * a �t� modifi�e, et, enfin, si l'id�e d�finie en remplace
 * une autre.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_manquantes_lecture(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Indice) indice, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(CommandeMissing) commandemissing);
/* Lit une id�e manquante.
 * S'il n'y a pas de d�finition explicite, la
 * valeur par d�faut est utilis�e. Dans ce cas,
 * si celle-ci n'est pas d�finie, le bool�en est
 * �galement mis � FAUX.
 * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est incorrect.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 */

Resultat idees_generiques_definition(TRAVAIL(Idees) idees, TRAVAIL(CommandeGeneric) commandegeneric, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(Booleen) defaut, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit une id�e g�n�rique.
 * La fonction indique si la r�f�rence contenue dans
 * l'id�e �tait correcte ou non, si l'id�e par d�faut
 * a �t� modifi�e, et, enfin, si l'id�e d�finie en remplace
 * une autre.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec
 * d'une allocation m�moire.
 */

Resultat idees_generiques_lecture(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(CommandeGeneric) commandegeneric);
/* Lit une id�e g�n�rique.
 * S'il n'y a pas de d�finition explicite, la
 * valeur par d�faut est utilis�e. Dans ce cas,
 * si celle-ci n'est pas d�finie, le bool�en est
 * �galement mis � FAUX.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 */

Resultat idees_recherche_type(TRAVAIL(Idees) idees, TRAVAIL_SCALAIRE(Chaine) reference, COREFERENCE_SCALAIRE(Booleen) correct, COREFERENCE_SCALAIRE(TypeIdee) typeidee);
/* Recherche le type d'une id�e en fonction de sa r�f�rence.
 * La priorit� des types est IDEE_PRESENTE, IDEE_MANQUANTE et
 * IDEE_GENERIQUE.
 * Si la r�f�rence n'est pas trouv�e, le bool�en correct est
 * mis a FAUX.
 * Renvoie RESULTAT_ERREUR si idees ou reference est NULL.
 */

Resultat idees_copie(TRAVAIL(Idees) idees, TRAVAIL(Idees) copie);
/* R�alise une copie d'une structure d'id�es.
 * Si copie est non NULL, la valeur est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR si idees est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
 */

Resultat idees_destruction(TRAVAIL(Idees) idees);
/* D�truit une structure d'id�es.
 */

#endif
