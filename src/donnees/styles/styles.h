/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __STYLES__
#define __STYLES__

#include <src/global/global.h>

typedef struct styles CONTENEUR(Styles);

#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commandesection.h>
#include <src/donnees/commandes/commandereference.h>

#define NIVEAU_PAR_DEFAUT        -1

typedef struct stylessection CONTENEUR_SCALAIRE(StylesSection);

struct stylessection
{
        TABLEAU(STOCKAGE(CommandeSection)) section;
        //Commandes de section contenant
        //les noms et les formats.
        //L'indice dans le tableau indique
        //le niveau auquel le format de
        //section s'applique.
        //Si un niveau n'a pas de format,
        //la commande de section est plac�e
        //� NULL.
        //Les niveaux n�gatifs sont donc
        //interdits.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Indique la taille du tableau.
        STOCKAGE(CommandeSection) defaut;
        //Cette commande de section correspond
        //aux niveaux non explicitement d�finis.
};
/* Structure interne servant � rassembler
 * les informations concernant les styles
 * de section.
 */

typedef struct stylesreference CONTENEUR_SCALAIRE(StylesReference);

struct stylesreference
{
        TABLEAU(STOCKAGE(CommandeReference)) reference;
        //Commandes de reference contenant
        //les noms et les formats.
        //L'indice dans le tableau indique
        //le niveau auquel le format de
        //reference s'applique.
        //Si un niveau n'a pas de format,
        //la commande de reference est plac�e
        //� NULL.
        //Les niveaux n�gatifs sont donc
        //interdits.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Indique la taille du tableau.
        STOCKAGE(CommandeReference) defaut;
        //Cette commande de reference correspond
        //aux niveaux non explicitement d�finis.
};
/* Structure interne servant � rassembler
 * les informations concernant les styles
 * de r�f�rence.
 */

struct styles
{
        STOCKAGE_SCALAIRE(NiveauHierarchique) racine;
        //Niveau de la racine du document.
        //Cette valeur sera initialis�e
        //� la valeur NIVEAU_PAR_DEFAUT, et
        //sera automatiquement modifi� apr�s
        //la passe de d�finition de styles au
        //plus haut niveau explicitement
        //d�fini, s'il n'a pas �t� modifi�.
        STOCKAGE(Flux) message;
        //Flux repr�sentant un message
        //plac� en commentaire dans le
        //fichier objet.
        //Initialis� � NULL, il
        //sera transform� en un
        //flux vide apr�s la passe
        //de d�finition de style, si
        //une valeur n'a pas �t�
        //explicitement donn�e.
        STOCKAGE_SCALAIRE(StylesSection) sections;
        //Cette structure contient les
        //diff�rents styles applicables
        //aux sections.
        STOCKAGE_SCALAIRE(StylesReference) references;
        //Cette structure contient les
        //diff�rents styles applicables
        //aux references.
        STOCKAGE(Flux) entete;
        //Ent�te du document.
        //Initialis� � NULL, il
        //sera transform� en un
        //flux vide apr�s la passe
        //de d�finition de style, si
        //une valeur n'a pas �t�
        //explicitement donn�e.
        STOCKAGE(Flux) pied;
        //Pied du document.
        //Initialis� � NULL, il
        //sera transform� en un
        //flux vide apr�s la passe
        //de d�finition de style, si
        //une valeur n'a pas �t�
        //explicitement donn�e.
};
/* Structure interne servant � rassembler
 * les informations concernant les styles
 * de section.
 */

Resultat styles_initialisation(TRAVAIL(Styles) styles);
/* Initialise une structure d'enregistrement de style.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
 * allocation m�moire.
 */

Resultat styles_section_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, TRAVAIL(CommandeSection) commandesection, COREFERENCE_SCALAIRE(Booleen) niveaucorrect, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit un format pour un niveau.
 * Si le niveau est �gal � NIVEAU_PAR_DEFAUT, la
 * d�finition affecte le format par d�faut.
 * Le premier bool�en renvoy� indique si le niveau
 * �tait bien correct. Le second indique le fait
 * qu'une d�finition explicite existait pour ce niveau.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
 * allocation m�moire.
 */

Resultat styles_section_lecture(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) format, REFERENCE(CommandeSection) commandesection);
/* Renvoie la commande de section donnant l'ensemble
 * des informations de format pour le niveau indiqu�.
 * Si niveau est �gal � NIVEAU_PAR_DEFAUT, le niveau
 * par d�faut est renvoy�. C'est �galement le cas si
 * le niveau demand� n'est pas explicitement d�fini.
 * Si dans ce cas, m�me le niveau par d�faut n'est
 * pas d�fini, le bool�en format est pass� � FAUX.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
 * incorrect.
 */

Resultat styles_section_recherche_niveau(TRAVAIL(Styles) styles, TRAVAIL(Flux) recherche, TRAVAIL_SCALAIRE(Booleen) numerique, TRAVAIL_SCALAIRE(Booleen) strict, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Renvoie le niveau �quivalent au flux donn�.
 * La fonction indique si le niveau est correct,
 * et donne le niveau le plus appropri�.
 * Si numerique est VRAI, le flux doit absolument
 * �tre �quivalent � un nombre.
 * Si strict est FAUX, la recherche renvoie l'indice
 * par d�faut si la d�finition explicite est absente.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
 * allocation m�moire.
 */

Resultat styles_section_niveau_racine(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(NiveauHierarchique) racine);
/* Renvoie l'indice du niveau racine.
 * Il s'agit du plus haut niveau explicitement
 * d�fini. Le niveau par d�faut est renvoy� dans
 * le cas o� aucun niveau ne serait explicitement
 * d�fini.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_reference_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, TRAVAIL(CommandeReference) commandereference, COREFERENCE_SCALAIRE(Booleen) niveaucorrect, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit un format pour un niveau.
 * Si le niveau est �gal � NIVEAU_PAR_DEFAUT, la
 * d�finition affecte le format par d�faut.
 * Le premier bool�en renvoy� indique si le niveau
 * �tait bien correct. Le second indique le fait
 * qu'une d�finition explicite existait pour ce niveau.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
 * allocation m�moire.
 */

Resultat styles_reference_lecture(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) format, REFERENCE(CommandeReference) commandereference);
/* Renvoie la commande de reference donnant l'ensemble
 * des informations de format pour le niveau indiqu�.
 * Si niveau est �gal � NIVEAU_PAR_DEFAUT, le niveau
 * par d�faut est renvoy�. C'est �galement le cas si
 * le niveau demand� n'est pas explicitement d�fini.
 * Si dans ce cas, m�me le niveau par d�faut n'est
 * pas d�fini, le bool�en format est pass� � FAUX.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
 * incorrect.
 */

Resultat styles_racine_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit le niveau de la racine.
 * Le bool�en sert � indiquer une
 * red�finition du niveau racine.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
 * incorrect.
 */

Resultat styles_racine_lecture(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(NiveauHierarchique) niveau);
/* Lit le niveau de la racine.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_message_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) message, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit un message de document.
 * Le bool�en sert � indiquer une
 * red�finition du message.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_message_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) message);
/* Lit un message de document.
 * Le bool�en sert � indiquer si
 * le message existe bien. Si ce bool�en
 * est mis � FAUX, le flux n'est pas modifi�.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_entete_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) entete, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit un ent�te de document.
 * Le bool�en sert � indiquer une
 * red�finition de l'ent�te.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_entete_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) entete);
/* Lit un ent�te de document.
 * Le bool�en sert � indiquer si
 * l'ent�te existe bien. Si ce bool�en
 * est mis � FAUX, le flux n'est pas modifi�.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_pied_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) pied, COREFERENCE_SCALAIRE(Booleen) remplacement);
/* D�finit un pied de document.
 * Le bool�en sert � indiquer une
 * red�finition du pied.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_pied_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) pied);
/* Lit un pied de document.
 * Le bool�en sert � indiquer si
 * le pied existe bien. Si ce bool�en
 * est mis � FAUX, le flux n'est pas modifi�.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 */

Resultat styles_correction(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(Booleen) racine);
/* Fait les corrections d'usage apr�s le remplissage
 * de la structure par la passe d'enregistrement de
 * styles.
 * Le bool�en indique si le niveau racine est valide
 * � la sortie de la correction.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
 */

Resultat styles_copie(TRAVAIL(Styles) styles, TRAVAIL(Styles) copie);
/* R�alise une copie d'une structure de styles.
 * Si copie est non NULL, la valeur est d�truite
 * avant la copie.
 * Renvoie RESULTAT_ERREUR si styles est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
 */

Resultat styles_destruction(TRAVAIL(Styles) styles);
/* D�truit une structure de styles.
 */

#endif
