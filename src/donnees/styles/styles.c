/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "styles.h"

#define TAILLEINIT        5

Resultat styles_initialisation(TRAVAIL(Styles) styles)
{
        /* Initialise une structure d'enregistrement de style.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
         * allocation m�moire.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION((S_T(styles)=NOUVEAU(Styles))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(styles, racine)=NIVEAU_PAR_DEFAUT;
        CHAMP(styles, message)=NULL;
        CHAMP_STOCKAGE_(CHAMP(styles, sections), section)=NOUVEAUX(CommandeSection, T_S_(TAILLEINIT));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(styles, sections), section)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<TAILLEINIT ; T_S_(indice)++)
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_S_(indice))=NULL;
        CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)=TAILLEINIT;
        CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)=NULL;
        CHAMP_STOCKAGE_(CHAMP(styles, references), reference)=NOUVEAUX(CommandeReference, T_S_(TAILLEINIT));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(styles, references), reference)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<TAILLEINIT ; T_S_(indice)++)
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), T_S_(indice))=NULL;
        CHAMP_STOCKAGE_(CHAMP(styles, references), taille)=TAILLEINIT;
        CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)=NULL;
        CHAMP(styles, entete)=NULL;
        CHAMP(styles, pied)=NULL;
        return RESULTAT_OK;
}

Resultat styles_section_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, TRAVAIL(CommandeSection) commandesection, COREFERENCE_SCALAIRE(Booleen) niveaucorrect, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit un format pour un niveau.
         * Si le niveau est �gal � NIVEAU_PAR_DEFAUT, la
         * d�finition affecte le format par d�faut.
         * Le premier bool�en renvoy� indique si le niveau
         * �tait bien correct. Le second indique le fait
         * qu'une d�finition explicite existait pour ce niveau.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
         * allocation m�moire.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        if(niveau==T_S_(NIVEAU_PAR_DEFAUT))
        {
                if(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                S_C_(niveaucorrect)=VRAI;
                SECURISE(commandesection_copie(commandesection,T_S(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut))));
        }
        else
        {
                if(niveau<T_S_(0))
                {
                        S_C_(niveaucorrect)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(niveaucorrect)=VRAI;
                if(niveau>=T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)))
                {
                        TABLEAU(STOCKAGE(CommandeSection)) nouveau;
                        nouveau=REALLOCATION_CAST(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), CommandeSection, niveau+T_S_(1));
                        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        CHAMP_STOCKAGE_(CHAMP(styles, sections), section)=nouveau;
                        for( ; CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)<=niveau ; CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)++)
                                ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)))=NULL;
                }
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), niveau)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                SECURISE(commandesection_copie(commandesection,ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), niveau)));
        }
        return RESULTAT_OK;
}

Resultat styles_section_lecture(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) format, REFERENCE(CommandeSection) commandesection)
{
        /* Renvoie la commande de section donnant l'ensemble
         * des informations de format pour le niveau indiqu�.
         * Si niveau est �gal � NIVEAU_PAR_DEFAUT, le niveau
         * par d�faut est renvoy�. C'est �galement le cas si
         * le niveau demand� n'est pas explicitement d�fini.
         * Si dans ce cas, m�me le niveau par d�faut n'est
         * pas d�fini, le bool�en format est pass� � FAUX.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
         * incorrect.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        if(niveau==T_S_(NIVEAU_PAR_DEFAUT))
        {
                if(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)==NULL)
                {
                        S_C_(format)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(format)=VRAI;
                T_R(commandesection)=T_S(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut));
        }
        else
        {
                ASSERTION(niveau>=T_S_(0), RESULTAT_ERREUR_DOMAINE);

                if((niveau>=T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)))||(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), niveau)==NULL))
                {
                        if(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)==NULL)
                        {
                                S_C_(format)=FAUX;
                                return RESULTAT_OK;
                        }
                        S_C_(format)=VRAI;
                        T_R(commandesection)=T_S(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut));
                        return RESULTAT_OK;
                }
                S_C_(format)=VRAI;
                T_R(commandesection)=ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), niveau);
        }
        return RESULTAT_OK;
}

Resultat styles_section_recherche_niveau(TRAVAIL(Styles) styles, TRAVAIL(Flux) recherche, TRAVAIL_SCALAIRE(Booleen) numerique, TRAVAIL_SCALAIRE(Booleen) strict, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Renvoie le niveau �quivalent au flux donn�.
         * La fonction indique si le niveau est correct,
         * et donne le niveau le plus appropri�.
         * Si numerique est VRAI, le flux doit absolument
         * �tre �quivalent � un nombre.
         * Si strict est FAUX, la recherche renvoie l'indice
         * par d�faut si la d�finition explicite est absente.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
         * allocation m�moire.
         */
        STOCKAGE_SCALAIRE(Booleen) nombre;
        TRAVAIL_SCALAIRE(Entier) valeur;
        TRAVAIL_SCALAIRE(Chaine) texte;
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_nombre(recherche,C_S_(nombre),R_T_(valeur)));
        if(T_S_(nombre)==VRAI)
        {
                if(valeur<0)
                {
                        S_C_(correct)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(correct)=VRAI;
                if(strict==T_S_(VRAI))
                {
                        T_R_(niveau)=valeur;
                }
                else
                {
                        if((valeur>=T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)))||(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), valeur)==NULL))
                                T_R_(niveau)=T_S_(NIVEAU_PAR_DEFAUT);
                        else
                                T_R_(niveau)=valeur;
                }
        }
        else
        {
                if(numerique==T_S_(VRAI))
                {
                        S_C_(correct)=FAUX;
                        return RESULTAT_OK;
                }
                SECURISE(flux_texte(recherche,R_T_(texte)));
                for(S_T_(valeur)=0 ; valeur<T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)) ; valeur++)
                {
                        TRAVAIL_SCALAIRE(Chaine) nomniveautexte;
                        TRAVAIL(Flux) nomniveau;
                        if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), valeur)==NULL)
                                continue;
                        SECURISE(commandesection_lecture_nom(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), valeur),R_T(nomniveau)));
                        ASSERTION(nomniveau!=NULL, RESULTAT_ERREUR);

                        SECURISE(flux_texte(nomniveau,R_T_(nomniveautexte)));
                        if(STRCMP(texte, nomniveautexte)==T_S_(0))
                        {
                                S_C_(correct)=VRAI;
                                T_R_(niveau)=valeur;
                                free(S_T_(nomniveautexte));
                                free(S_T_(texte));
                                return RESULTAT_OK;
                        }
                        free(S_T_(nomniveautexte));
                }
                if(strict==VRAI)
                {
                        S_C_(correct)=FAUX;
                }
                else
                {
                        if(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)!=NULL)
                        {
                                S_C_(correct)=VRAI;
                                T_R_(niveau)=T_S_(NIVEAU_PAR_DEFAUT);
                        }
                        else
                        {
                                S_C_(correct)=FAUX;
                        }
                }
                free(S_T_(texte));
        }
        return RESULTAT_OK;
}

Resultat styles_section_niveau_racine(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(NiveauHierarchique) racine)
{
        /* Renvoie l'indice du niveau racine.
         * Il s'agit du plus haut niveau explicitement
         * d�fini. Le niveau par d�faut est renvoy� dans
         * le cas o� aucun niveau ne serait explicitement
         * d�fini.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        for(T_R_(racine)=0 ; T_R_(racine)<T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)) ; T_R_(racine)++)
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_R_(racine))!=NULL)
                        return RESULTAT_OK;
        T_R_(racine)=T_S_(NIVEAU_PAR_DEFAUT);
        return RESULTAT_OK;
}

Resultat styles_reference_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, TRAVAIL(CommandeReference) commandereference, COREFERENCE_SCALAIRE(Booleen) niveaucorrect, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit un format pour un niveau.
         * Si le niveau est �gal � NIVEAU_PAR_DEFAUT, la
         * d�finition affecte le format par d�faut.
         * Le premier bool�en renvoy� indique si le niveau
         * �tait bien correct. Le second indique le fait
         * qu'une d�finition explicite existait pour ce niveau.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une
         * allocation m�moire.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        if(niveau==T_S_(NIVEAU_PAR_DEFAUT))
        {
                if(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                S_C_(niveaucorrect)=VRAI;
                SECURISE(commandereference_copie(commandereference,T_S(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut))));
        }
        else
        {
                if(niveau<0)
                {
                        S_C_(niveaucorrect)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(niveaucorrect)=VRAI;
                if(niveau>=T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille)))
                {
                        TABLEAU(STOCKAGE(CommandeReference)) nouveau;
                        nouveau=REALLOCATION_CAST(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), CommandeReference, niveau+T_S_(1));
                        ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);
                        CHAMP_STOCKAGE_(CHAMP(styles, references), reference)=nouveau;
                        for( ; T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille))<=niveau ; T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille))++)
                                ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille)))=NULL;
                }
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), niveau)!=NULL)
                        S_C_(remplacement)=VRAI;
                else
                        S_C_(remplacement)=FAUX;
                SECURISE(commandereference_copie(commandereference,ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), niveau)));
        }
        return RESULTAT_OK;
}

Resultat styles_reference_lecture(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) format, REFERENCE(CommandeReference) commandereference)
{
        /* Renvoie la commande de reference donnant l'ensemble
         * des informations de format pour le niveau indiqu�.
         * Si niveau est �gal � NIVEAU_PAR_DEFAUT, le niveau
         * par d�faut est renvoy�. C'est �galement le cas si
         * le niveau demand� n'est pas explicitement d�fini.
         * Si dans ce cas, m�me le niveau par d�faut n'est
         * pas d�fini, le bool�en format est pass� � FAUX.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
         * incorrect.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        if(niveau==T_S_(NIVEAU_PAR_DEFAUT))
        {
                if(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)==NULL)
                {
                        S_C_(format)=FAUX;
                        return RESULTAT_OK;
                }
                S_C_(format)=VRAI;
                T_R(commandereference)=T_S(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut));
        }
        else
        {
                ASSERTION(niveau>=T_S_(0), RESULTAT_ERREUR_DOMAINE);

                if((niveau>=T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille)))||(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), niveau)==NULL))
                {
                        if(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)==NULL)
                        {
                                S_C_(format)=FAUX;
                                return RESULTAT_OK;
                        }
                        S_C_(format)=VRAI;
                        T_R(commandereference)=T_S(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut));
                        return RESULTAT_OK;
                }
                S_C_(format)=VRAI;
                T_R(commandereference)=ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), niveau);
        }
        return RESULTAT_OK;
}

Resultat styles_racine_definition(TRAVAIL(Styles) styles, TRAVAIL_SCALAIRE(NiveauHierarchique) niveau, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit le niveau de la racine.
         * Le bool�en sert � indiquer une
         * red�finition du niveau racine.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_DOMAINE si l'indice est
         * incorrect.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);
        ASSERTION((niveau>=T_S_(0))||(niveau==T_S_(NIVEAU_PAR_DEFAUT)), RESULTAT_ERREUR_DOMAINE);

        if(T_S_(CHAMP(styles, racine))!=NIVEAU_PAR_DEFAUT)
                S_C_(remplacement)=FAUX;
        else
                S_C_(remplacement)=VRAI;
        CHAMP(styles, racine)=S_T_(niveau);
        return RESULTAT_OK;
}

Resultat styles_racine_lecture(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(NiveauHierarchique) niveau)
{
        /* Lit le niveau de la racine.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        T_R_(niveau)=T_S_(CHAMP(styles, racine));
        return RESULTAT_OK;
}

Resultat styles_message_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) message, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit un message de document.
         * Le bool�en sert � indiquer une
         * red�finition du message.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(remplacement)=(CHAMP(styles, message)!=NULL)?VRAI:FAUX;
        SECURISE(flux_copie(message,CHAMP_TRAVAIL(styles, message)));
        return RESULTAT_OK;
}

Resultat styles_message_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) message)
{
        /* Lit un message de document.
         * Le bool�en sert � indiquer si
         * le message existe bien. Si ce bool�en
         * est mis � FAUX, le flux n'est pas modifi�.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(correct)=(CHAMP(styles, message)!=NULL)?VRAI:FAUX;
        if(T_S_(S_C_(correct))==T_S_(VRAI))
                T_R(message)=CHAMP_TRAVAIL(styles, message);
        return RESULTAT_OK;
}

Resultat styles_entete_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) entete, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit un ent�te de document.
         * Le bool�en sert � indiquer une
         * red�finition de l'ent�te.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(remplacement)=(CHAMP(styles, entete)!=NULL)?VRAI:FAUX;
        SECURISE(flux_copie(entete,CHAMP_TRAVAIL(styles, entete)));
        return RESULTAT_OK;
}

Resultat styles_entete_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) entete)
{
        /* Lit un ent�te de document.
         * Le bool�en sert � indiquer si
         * l'ent�te existe bien. Si ce bool�en
         * est mis � FAUX, le flux n'est pas modifi�.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(correct)=(CHAMP(styles, entete)!=NULL)?VRAI:FAUX;
        if(T_S_(S_C_(correct))==T_S_(VRAI))
                T_R(entete)=CHAMP_TRAVAIL(styles, entete);
        return RESULTAT_OK;
}

Resultat styles_pied_definition(TRAVAIL(Styles) styles, TRAVAIL(Flux) pied, COREFERENCE_SCALAIRE(Booleen) remplacement)
{
        /* D�finit un pied de document.
         * Le bool�en sert � indiquer une
         * red�finition du pied.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(remplacement)=(CHAMP(styles, pied)!=NULL)?VRAI:FAUX;
        SECURISE(flux_copie(pied,CHAMP_TRAVAIL(styles, pied)));
        return RESULTAT_OK;
}

Resultat styles_pied_lecture(TRAVAIL(Styles) styles, COREFERENCE_SCALAIRE(Booleen) correct, REFERENCE(Flux) pied)
{
        /* Lit un pied de document.
         * Le bool�en sert � indiquer si
         * le pied existe bien. Si ce bool�en
         * est mis � FAUX, le flux n'est pas modifi�.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         */
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        S_C_(correct)=(CHAMP(styles, pied)!=NULL)?VRAI:FAUX;
        if(T_S_(S_C_(correct))==T_S_(VRAI))
                T_R(pied)=CHAMP_TRAVAIL(styles, pied);
        return RESULTAT_OK;
}

Resultat styles_correction(TRAVAIL(Styles) styles, REFERENCE_SCALAIRE(Booleen) racine)
{
        /* Fait les corrections d'usage apr�s le remplissage
         * de la structure par la passe d'enregistrement de
         * styles.
         * Le bool�en indique si le niveau racine est valide
         * � la sortie de la correction.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
         */
        STOCKAGE(Flux) flux;
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(styles, message)==NULL)
        {
                SECURISE(flux_initialisation(T_S(flux)));
                SECURISE(flux_definition_type(T_S(flux),T_S_(FLUX_MESSAGE)));
                SECURISE(flux_copie(T_S(flux),CHAMP_TRAVAIL(styles, message)));
                SECURISE(flux_destruction(T_S(flux)));
        }
        if(CHAMP(styles, entete)==NULL)
        {
                SECURISE(flux_initialisation(T_S(flux)));
                SECURISE(flux_definition_type(T_S(flux),T_S_(FLUX_DOCUMENT)));
                SECURISE(flux_copie(T_S(flux),CHAMP_TRAVAIL(styles, entete)));
                SECURISE(flux_destruction(T_S(flux)));
        }
        if(CHAMP(styles, pied)==NULL)
        {
                SECURISE(flux_initialisation(T_S(flux)));
                SECURISE(flux_definition_type(T_S(flux),T_S_(FLUX_DOCUMENT)));
                SECURISE(flux_copie(T_S(flux),CHAMP_TRAVAIL(styles, pied)));
                SECURISE(flux_destruction(T_S(flux)));
        }
        T_R_(racine)=VRAI;
        if(T_S_(CHAMP(styles, racine))==T_S_(NIVEAU_PAR_DEFAUT))
        {
                TRAVAIL_SCALAIRE(NiveauHierarchique) niveauracine;
                SECURISE(styles_section_niveau_racine(styles,R_T_(niveauracine)));
                if(niveauracine==T_S_(NIVEAU_PAR_DEFAUT))
                        T_R_(racine)=T_S_(FAUX);
                else
                        CHAMP(styles, racine)=S_T_(niveauracine);
        }
        return RESULTAT_OK;
}

Resultat styles_copie(TRAVAIL(Styles) styles, TRAVAIL(Styles) copie)
{
        /* R�alise une copie d'une structure de styles.
         * Si copie est non NULL, la valeur est d�truite
         * avant la copie.
         * Renvoie RESULTAT_ERREUR si styles est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �coue.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(styles)!=NULL, RESULTAT_ERREUR);

        SECURISE(styles_destruction(copie));
        SECURISE(styles_initialisation(copie));
        CHAMP(copie, racine)=CHAMP(styles, racine);
        if(CHAMP(styles, message)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(styles, message),CHAMP_TRAVAIL(copie, message)));
        }
        free(CHAMP_STOCKAGE_(CHAMP(copie, sections), section));
        CHAMP_STOCKAGE_(CHAMP(copie, sections), section)=NOUVEAUX(CommandeSection, T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, sections), section)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(styles, sections), taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, sections), section), T_S_(indice))=NULL;
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_S_(indice))!=NULL)
                {
                        SECURISE(commandesection_copie(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_S_(indice)),ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(copie, sections), section), T_S_(indice))));
                }
        }
        CHAMP_STOCKAGE_(CHAMP(copie, sections), taille)=CHAMP_STOCKAGE_(CHAMP(styles, sections), taille);
        CHAMP_STOCKAGE_(CHAMP(copie, sections), defaut)=NULL;
        if(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)!=NULL)
        {
                SECURISE(commandesection_copie(T_S(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut)),T_S(CHAMP_STOCKAGE_(CHAMP(copie, sections), defaut))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(copie, references), reference));
        CHAMP_STOCKAGE_(CHAMP(copie, references), reference)=NOUVEAUX(CommandeReference, T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille)));
        ASSERTION(CHAMP_STOCKAGE_(CHAMP(copie, references), reference)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        for(indice=0 ; T_S_(indice)<T_S_(CHAMP_STOCKAGE_(CHAMP(styles, references), taille)) ; T_S_(indice)++)
        {
                ELEMENT(CHAMP_STOCKAGE_(CHAMP(copie, references), reference), T_S_(indice))=NULL;
                if(ELEMENT(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), T_S_(indice))!=NULL)
                {
                        SECURISE(commandereference_copie(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), T_S_(indice)),ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(copie, references), reference), T_S_(indice))));
                }
        }
        CHAMP_STOCKAGE_(CHAMP(copie, references), taille)=CHAMP_STOCKAGE_(CHAMP(styles, references), taille);
        CHAMP_STOCKAGE_(CHAMP(copie, references), defaut)=NULL;
        if(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)!=NULL)
        {
                SECURISE(commandereference_copie(T_S(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut)),T_S(CHAMP_STOCKAGE_(CHAMP(copie, references), defaut))));
        }
        if(CHAMP(styles, entete)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(styles, entete),CHAMP_TRAVAIL(copie, entete)));
        }
        if(CHAMP(styles, pied)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(styles, pied),CHAMP_TRAVAIL(copie, pied)));
        }
        return RESULTAT_OK;
}

Resultat styles_destruction(TRAVAIL(Styles) styles)
{
        /* D�truit une structure de styles.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(styles)==NULL)
                return RESULTAT_OK;
        SECURISE(flux_destruction(CHAMP_TRAVAIL(styles, message)));
        for(indice=0 ; T_S_(indice)<CHAMP_STOCKAGE_(CHAMP(styles, sections), taille) ; T_S_(indice)++)
        {
                SECURISE(commandesection_destruction(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, sections), section), T_S_(indice))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(styles, sections), section));
        SECURISE(commandesection_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(styles, sections), defaut))));
        for(indice=0 ; T_S_(indice)<CHAMP_STOCKAGE_(CHAMP(styles, references), taille) ; T_S_(indice)++)
        {
                SECURISE(commandereference_destruction(ELEMENT_TRAVAIL(CHAMP_STOCKAGE_(CHAMP(styles, references), reference), T_S_(indice))));
        }
        free(CHAMP_STOCKAGE_(CHAMP(styles, references), reference));
        SECURISE(commandereference_destruction(T_S(CHAMP_STOCKAGE_(CHAMP(styles, references), defaut))));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(styles, entete)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(styles, pied)));
        free(S_T(styles));
        S_T(styles)=NULL;
        return RESULTAT_OK;
}

