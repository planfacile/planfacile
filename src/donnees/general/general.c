/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "general.h"

Resultat general_initialisation(TRAVAIL(General) general)
{
        /* Cr�e une structure de g�n�ralit� vide.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
         */
        ASSERTION((S_T(general)=NOUVEAU(General))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(general, principal)=NULL;
        CHAMP(general, localisation)=NULL;
        CHAMP(general, pilenommacro)=NULL;
        CHAMP(general, piletexte)=NULL;
        CHAMP(general, pileparametre)=NULL;
        CHAMP(general, pilelocalisation)=NULL;
        CHAMP(general, pileflux)=NULL;
        CHAMP(general, retouches)=NULL;
        CHAMP(general, reductionoptions)=NULL;
        CHAMP(general, reductionmacros)=NULL;
        CHAMP(general, verification)=NULL;
        CHAMP(general, enregistrementstyles)=NULL;
        CHAMP(general, enregistrementidees)=NULL;
        CHAMP(general, calculplan)=NULL;
        CHAMP(general, performancesplan)=NULL;
        CHAMP(general, erreursyntaxe)=NULL;
        CHAMP(general, sources)=NULL;
        CHAMP(general, option)=NULL;
        CHAMP(general, environnement)=NULL;
        CHAMP(general, styles)=NULL;
        CHAMP(general, idees)=NULL;
        CHAMP(general, arbreidees)=NULL;
        CHAMP(general, sortie)=NULL;
        return RESULTAT_OK;
}


Resultat general_ligne_commande(TRAVAIL(General) general, int argc, char *argv[])
{
        /* Traite la ligne de commande du compilateur,
         * et place les variables en fonction.
         * Renvoie RESULTAT_ERREUR si general est NULL.
         * Renvoie RESULTAT_ERREUR si CHAMP(general, environnement) est NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE_SCALAIRE(Entier) option;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE_SCALAIRE(Booleen) utilisationsortie;
        STOCKAGE(CommandeOption) commandeoption;
        TRAVAIL(Options) options;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, environnement)!=NULL, RESULTAT_ERREUR);

        utilisationsortie=FAUX;
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(environnement_definition_sortie(CHAMP_TRAVAIL(general, environnement), T_S_(SORTIESTANDARD)));
        SECURISE(environnement_definition_erreur(CHAMP_TRAVAIL(general, environnement), T_S_(SORTIEERREUR)));
        SECURISE(environnement_lecture_options(CHAMP_TRAVAIL(general, environnement), R_T(options)));
        SECURISE(option_destruction(CHAMP_TRAVAIL(general, option)));
        SECURISE(option_initialisation(CHAMP_TRAVAIL(general, option)));
        opterr=0;
        while(T_S_((option=getopt(argc,argv,T_S_("abcdefghijklmno:pqrstuvwxyzABCDEFGHIJKLMNO:PQRSTUVWXYZ0123456789"))))!=T_S_(EOF))
        {
                switch(T_S_((STOCKAGE_SCALAIRE(Caractere))(option)))
                {
                        case 'h':
                                SECURISE(options_changement_aide(options));
                                break;
                        case 'V':
                                SECURISE(options_changement_licence(options));
                                break;
                        case 'v':
                                SECURISE(options_changement_verbeux(options));
                                break;
                        case 'o':
                                if(T_S_(utilisationsortie)==VRAI)
                                {
                                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_UTILISATION_SORTIE), T_S(localisation)));
                                        break;
                                }
                                utilisationsortie=VRAI;
                                SECURISE(environnement_definition_fichiersortie(CHAMP_TRAVAIL(general, environnement), T_S_((STOCKAGE_SCALAIRE(NomFichier))(optarg))));
                                break;
                        case 'a':
                                //Toutes les options activ�es par d�faut :
                                SECURISE(options_changement_source(options));
                                SECURISE(options_changement_inclusion(options));
                                SECURISE(options_changement_commandes(options));
                                SECURISE(options_changement_ideereferencevide(options));
                                SECURISE(options_changement_indiceincorrect(options));
                                SECURISE(options_changement_referenceincorrecte(options));
                                SECURISE(options_changement_ideeautomatiquemanquante(options));
                                SECURISE(options_changement_dependancesincorrectes(options));
                                SECURISE(options_changement_dependancesinutiles(options));
                                SECURISE(options_changement_pertinenceincorrecte(options));
                                SECURISE(options_changement_indicereferenceincorrect(options));
                                SECURISE(options_changement_commentaires(options));
                                SECURISE(options_changement_redefinitionstyles(options));
                                SECURISE(options_changement_niveauincorrect(options));
                                SECURISE(options_changement_sectionstylemanquant(options));
                                SECURISE(options_changement_referencestylemanquant(options));
                                SECURISE(options_changement_nomsectionincorrect(options));
                                SECURISE(options_changement_redefinitionmacro(options));
                                SECURISE(options_changement_macroinconnue(options));
                                SECURISE(options_changement_parametres(options));
                                SECURISE(options_changement_recursivite(options));
                                SECURISE(options_changement_optionmacro(options));
                                break;
                        case 'n':
                                //Toutes les options d�sactiv�es par d�faut :
                                SECURISE(options_changement_utilisateur(options));
                                SECURISE(options_changement_redefinitionidees(options));
                                SECURISE(options_changement_ajoutracine(options));
                                break;
                        case 'e':
                                SECURISE(options_changement_erreur(options));
                                break;
                        case 'W':
                                SECURISE(options_changement_copielocalisation(options));
                                break;
                        case 'w':
                                SECURISE(options_changement_localisationabsolue(options));
                                break;
                        case 'S':
                                SECURISE(options_changement_source(options));
                                break;
                        case 'I':
                                SECURISE(options_changement_inclusion(options));
                                break;
                        case 's':
                                SECURISE(options_changement_recherche(options));
                                break;
                        case 'O':
                                SECURISE(commandeoption_initialisation(T_S(commandeoption)));
                                SECURISE(localisationfichier_ajout(T_S(localisation),T_S_("-O"),T_S_(NULL),T_S_(NULL),T_S_(0)));
                                SECURISE(commandeoption_definition_localisationfichier(T_S(commandeoption),T_S(localisation)));
                                SECURISE(localisationfichier_retrait(T_S(localisation)));
                                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_ENREGISTREMENT),T_S_(optarg)));
                                SECURISE(commandeoption_definition_option(T_S(commandeoption),T_S_((STOCKAGE_SCALAIRE(NomOption))(optarg))));
                                SECURISE(option_ajout_option(CHAMP_TRAVAIL(general, option),T_S(commandeoption)));
                                SECURISE(commandeoption_destruction(T_S(commandeoption)));
                                break;
                        case 'u':
                                SECURISE(options_changement_utilisateur(options));
                                break;
                        case 'C':
                                SECURISE(options_changement_commandes(options));
                                break;
                        case 'i':
                                SECURISE(options_changement_redefinitionidees(options));
                                break;
                        case 'Y': 
                                SECURISE(options_changement_ideereferencevide(options));
                                break;
                        case 'm':
                                SECURISE(options_changement_indiceincorrect(options));
                                break;
                        case 'g':
                                SECURISE(options_changement_referenceincorrecte(options));
                                break;
                        case 'r':
                                SECURISE(options_changement_ajoutracine(options));
                                break;
                        case 'A':
                                SECURISE(options_changement_ideeautomatiquemanquante(options));
                                break;
                        case 'd':
                                SECURISE(options_changement_dependancesincorrectes(options));
                                break;
                        case 'D':
                                SECURISE(options_changement_dependancesinutiles(options));
                                break;
                        case 'p':
                                SECURISE(options_changement_pertinenceincorrecte(options));
                                break;
                        case 'E':
                                SECURISE(options_changement_indicereferenceincorrect(options));
                                break;
                        case 'c':
                                SECURISE(options_changement_commentaires(options));
                                break;
                        case 'y':
                                SECURISE(options_changement_redefinitionstyles(options));
                                break;
                        case 'l':
                                SECURISE(options_changement_niveauincorrect(options));
                                break;
                        case 'G':
                                SECURISE(options_changement_sectionstylemanquant(options));
                                break;
                        case 'N':
                                SECURISE(options_changement_referencestylemanquant(options));
                                break;
                        case 'T':
                                SECURISE(options_changement_nomsectionincorrect(options));
                                break;
                        case 'M':
                                SECURISE(options_changement_redefinitionmacro(options));
                                break;
                        case 'U':
                                SECURISE(options_changement_macroinconnue(options));
                                break;
                        case 'P':
                                SECURISE(options_changement_parametres(options));
                                break;
                        case 'R':
                                SECURISE(options_changement_recursivite(options));
                                break;
                        case 't':
                                SECURISE(options_changement_optionmacro(options));
                                break;
                        case 'b': case 'f': case 'j': case 'k': case 'q':
                        case 'x': case 'z': case 'B': case 'F': case 'H':
                        case 'J': case 'K': case 'L': case 'Q': case 'X':
                        case 'Z': case '0': case '1': case '2': case '3':
                        case '4': case '5': case '6': case '7': case '8':
                        case '9':
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_OPTION_LIGNE_COMMANDE), T_S(localisation), T_S_((STOCKAGE_SCALAIRE(Caractere))(option))));
                                break;                                                                                                     
                        default:
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_OPTION_LIGNE_COMMANDE), T_S(localisation), T_S_((STOCKAGE_SCALAIRE(Caractere))(optopt))));
                                break;
                }
        }
        SECURISE(filenomfichier_destruction(CHAMP_TRAVAIL(general, sources)));
        SECURISE(filenomfichier_initialisation(CHAMP_TRAVAIL(general, sources)));
        for( ; T_S_(optind)<argc ; T_S_(optind)++)
        {
                SECURISE(filenomfichier_ajout_nomfichier(CHAMP_TRAVAIL(general, sources),argv[T_S_(optind)]));
        }
        SECURISE(filenomfichier_vide(CHAMP_TRAVAIL(general, sources),R_S_(utilisationsortie)));
        if(T_S_(utilisationsortie)==VRAI)
        {
                SECURISE(filenomfichier_ajout_nomfichier(CHAMP_TRAVAIL(general, sources),T_S_("-")));
        }
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        return RESULTAT_OK;
}

Resultat general_copie(TRAVAIL(General) general, TRAVAIL(General) copie)
{
        /* Cr�e une copie de la structure g�n�rale donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(general_destruction(copie));
        SECURISE(general_initialisation(copie));
        if(CHAMP(general, principal)==NULL)
                CHAMP(copie, principal)=NULL;
        else
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(general, principal),CHAMP_TRAVAIL(copie, principal)));
        }
        if(CHAMP(general, localisation)==NULL)
                CHAMP(copie, localisation)=NULL;
        else
        {
                SECURISE(localisationfichier_copie(CHAMP_TRAVAIL(general, localisation),CHAMP_TRAVAIL(copie, localisation)));
        }
        if(CHAMP(general, pilenommacro)==NULL)
                CHAMP(copie, pilenommacro)=NULL;
        else
        {
                SECURISE(pilenommacro_copie(CHAMP_TRAVAIL(general, pilenommacro),CHAMP_TRAVAIL(copie, pilenommacro)));
        }
        if(CHAMP(general, piletexte)==NULL)
                CHAMP(copie, piletexte)=NULL;
        else
        {
                SECURISE(pilechaine_copie(CHAMP_TRAVAIL(general, piletexte),CHAMP_TRAVAIL(copie, piletexte)));
        }
        if(CHAMP(general, pileparametre)==NULL)
                CHAMP(copie, pileparametre)=NULL;
        else
        {
                SECURISE(pileentier_copie(CHAMP_TRAVAIL(general, pileparametre),CHAMP_TRAVAIL(copie, pileparametre)));
        }
        if(CHAMP(general, pilelocalisation)==NULL)
                CHAMP(copie, pilelocalisation)=NULL;
        else
        {
                SECURISE(pilelocalisationfichier_copie(CHAMP_TRAVAIL(general, pilelocalisation),CHAMP_TRAVAIL(copie, pilelocalisation)));
        }
        if(CHAMP(general, retouches)==NULL)
                CHAMP(copie, retouches)=NULL;
        else
        {
                SECURISE(retouches_copie(CHAMP_TRAVAIL(general, retouches),CHAMP_TRAVAIL(copie, retouches)));
        }
        if(CHAMP(general, reductionoptions)==NULL)
                CHAMP(copie, reductionoptions)=NULL;
        else
        {
                SECURISE(reductionoptions_copie(CHAMP_TRAVAIL(general, reductionoptions),CHAMP_TRAVAIL(copie, reductionoptions)));
        }
        if(CHAMP(general, reductionmacros)==NULL)
                CHAMP(copie, reductionmacros)=NULL;
        else
        {
                SECURISE(reductionmacros_copie(CHAMP_TRAVAIL(general, reductionmacros),CHAMP_TRAVAIL(copie, reductionmacros)));
        }
        if(CHAMP(general, verification)==NULL)
                CHAMP(copie, verification)=NULL;
        else
        {
                SECURISE(verification_copie(CHAMP_TRAVAIL(general, verification),CHAMP_TRAVAIL(copie, verification)));
        }
        if(CHAMP(general, enregistrementstyles)==NULL)
                CHAMP(copie, enregistrementstyles)=NULL;
        else
        {
                SECURISE(enregistrementstyles_copie(CHAMP_TRAVAIL(general, enregistrementstyles),CHAMP_TRAVAIL(copie, enregistrementstyles)));
        }
        if(CHAMP(general, enregistrementidees)==NULL)
                CHAMP(copie, enregistrementidees)=NULL;
        else
        {
                SECURISE(enregistrementidees_copie(CHAMP_TRAVAIL(general, enregistrementidees),CHAMP_TRAVAIL(copie, enregistrementidees)));
        }
        if(CHAMP(general, calculplan)==NULL)
                CHAMP(copie, calculplan)=NULL;
        else
        {
                SECURISE(calculplan_copie(CHAMP_TRAVAIL(general, calculplan),CHAMP_TRAVAIL(copie, calculplan)));
        }
        if(CHAMP(general, performancesplan)==NULL)
                CHAMP(copie, performancesplan)=NULL;
        else
        {
                SECURISE(performancesplan_copie(CHAMP_TRAVAIL(general, performancesplan),CHAMP_TRAVAIL(copie, performancesplan)));
        }
        if(CHAMP(general, erreursyntaxe)==NULL)
                CHAMP(copie, erreursyntaxe)=NULL;
        else
        {
                CHAMP(copie, erreursyntaxe)=DUP_CAST(Chaine, T_S_(CHAMP(general, erreursyntaxe)));
                ASSERTION(CHAMP(copie, erreursyntaxe)!=NULL, RESULTAT_ERREUR);
        }
        if(CHAMP(general, sources)==NULL)
                CHAMP(copie, sources)=NULL;
        else
        {
                SECURISE(filenomfichier_copie(CHAMP_TRAVAIL(general, sources),CHAMP_TRAVAIL(copie, sources)));
        }
        if(CHAMP(general, option)==NULL)
                CHAMP(copie, option)=NULL;
        else
        {
                SECURISE(option_copie(CHAMP_TRAVAIL(general, option),CHAMP_TRAVAIL(copie, option)));
        }
        if(CHAMP(general, environnement)==NULL)
                CHAMP(copie, environnement)=NULL;
        else
        {
                SECURISE(environnement_copie(CHAMP_TRAVAIL(general, environnement),CHAMP_TRAVAIL(copie, environnement)));
        }
        if(CHAMP(general, styles)==NULL)
                CHAMP(copie, styles)=NULL;
        else
        {
                SECURISE(styles_copie(CHAMP_TRAVAIL(general, styles),CHAMP_TRAVAIL(copie, styles)));
        }
        if(CHAMP(general, idees)==NULL)
                CHAMP(copie, idees)=NULL;
        else
        {
                SECURISE(idees_copie(CHAMP_TRAVAIL(general, idees),CHAMP_TRAVAIL(copie, idees)));
        }
        if(CHAMP(general, arbreidees)==NULL)
                CHAMP(copie, arbreidees)=NULL;
        else
        {
                SECURISE(arbreidees_copie(CHAMP_TRAVAIL(general, arbreidees),CHAMP_TRAVAIL(copie, arbreidees)));
        }
        if(CHAMP(general, sortie)==NULL)
                CHAMP(copie, sortie)=NULL;
        else
        {
                SECURISE(sortie_copie(CHAMP_TRAVAIL(general, sortie),CHAMP_TRAVAIL(copie, sortie)));
        }
        return RESULTAT_OK;
}

Resultat general_destruction(TRAVAIL(General) general)
{
        /* D�truit une structure g�n�rale et tous ses contenus non vides.
         */
        if(S_T(general)==NULL)
                return RESULTAT_OK;
        SECURISE(flux_destruction(CHAMP_TRAVAIL(general, principal)));
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(general, localisation)));
        SECURISE(pilenommacro_destruction(CHAMP_TRAVAIL(general, pilenommacro)));
        SECURISE(pilechaine_destruction(CHAMP_TRAVAIL(general, piletexte)));
        SECURISE(pileentier_destruction(CHAMP_TRAVAIL(general, pileparametre)));
        SECURISE(pilelocalisationfichier_destruction(CHAMP_TRAVAIL(general, pilelocalisation)));
        SECURISE(pileflux_destruction(CHAMP_TRAVAIL(general, pileflux)));
        SECURISE(retouches_destruction(CHAMP_TRAVAIL(general, retouches)));
        SECURISE(reductionoptions_destruction(CHAMP_TRAVAIL(general, reductionoptions)));
        SECURISE(reductionmacros_destruction(CHAMP_TRAVAIL(general, reductionmacros)));
        SECURISE(verification_destruction(CHAMP_TRAVAIL(general, verification)));
        SECURISE(enregistrementstyles_destruction(CHAMP_TRAVAIL(general, enregistrementstyles)));
        SECURISE(enregistrementidees_destruction(CHAMP_TRAVAIL(general, enregistrementidees)));
        SECURISE(calculplan_destruction(CHAMP_TRAVAIL(general, calculplan)));
        SECURISE(performancesplan_destruction(CHAMP_TRAVAIL(general, performancesplan)));
        if(CHAMP(general, erreursyntaxe)!=NULL)
        {
                free(CHAMP(general, erreursyntaxe));
                CHAMP(general, erreursyntaxe)=NULL;
        }
        SECURISE(filenomfichier_destruction(CHAMP_TRAVAIL(general, sources)));
        SECURISE(option_destruction(CHAMP_TRAVAIL(general, option)));
        SECURISE(environnement_destruction(CHAMP_TRAVAIL(general, environnement)));
        SECURISE(styles_destruction(CHAMP_TRAVAIL(general, styles)));
        SECURISE(idees_destruction(CHAMP_TRAVAIL(general, idees)));
        SECURISE(arbreidees_destruction(CHAMP_TRAVAIL(general, arbreidees)));
        SECURISE(sortie_destruction(CHAMP_TRAVAIL(general, sortie)));
        free(S_T(general));
        S_T(general)=NULL;
        return RESULTAT_OK;
}

