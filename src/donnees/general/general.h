/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __GENERAL__
#define __GENERAL__

#include <src/global/global.h>

typedef struct general CONTENEUR(General);

#include <src/analyseur/donnees/pilechaine.h>
#include <src/analyseur/donnees/pileentier.h>
#include <src/analyseur/donnees/pilelocalisationfichier.h>
#include <src/analyseur/donnees/pileflux.h>
#include <src/analyseur/donnees/filenomfichier.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>
#include <src/donnees/environnement/environnement.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/option.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/donnees/passes/pilenommacro.h>
#include <src/donnees/styles/styles.h>
#include <src/donnees/idees/idees.h>
#include <src/donnees/sortie/arbreidees.h>
#include <src/donnees/sortie/performancesplan.h>
#include <src/passes/retouches/retouches.h>
#include <src/passes/options/reductionoptions.h>
#include <src/passes/macros/reductionmacros.h>
#include <src/passes/verification/verification.h>
#include <src/passes/styles/enregistrementstyles.h>
#include <src/passes/idees/enregistrementidees.h>
#include <src/passes/plan/plan.h>
#include <src/passes/sortie/sortie.h>

//Bcp d'includes, l�...

struct general
{
        //Pointeurs sur toutes les structures dynamiques
        //pr�sentes dans le compilateur...

        /* Contexte */
        STOCKAGE(Option) option;
        //Options d�finies en ligne de commande.
        STOCKAGE(Environnement) environnement;
        //Environnement de PlanFacile.
        STOCKAGE(FileNomFichier) sources;
        //File des fichiers sources � traiter.

        /* Analyse */
        STOCKAGE(LocalisationFichier) localisation;
        //Position de l'analyseur syntaxique lors d'un parcours
        //de l'entr�e.
        STOCKAGE(PileNomMacro) pilenommacro;
        //Pile des noms de macros utilis�e lors de l'analyse.
        STOCKAGE(PileChaine) piletexte;
        //Pile des chaine de texte utilis�e lors de l'analyse.
        STOCKAGE(PileEntier) pileparametre;
        //Pile des num�ros de param�tre utilis�e lors de l'analyse.
        STOCKAGE(PileLocalisationFichier) pilelocalisation;
        //Pile de localisation de fichier utilis�e lors de l'analyse.
        STOCKAGE(PileFlux) pileflux;
        //Pile des flux construits durant l'analyse, servant � les
        //d�truire en cas d'erreur durant cette �tape.
        STOCKAGE_SCALAIRE(Chaine) erreursyntaxe;
        //Chaine utilis�e lors des erreurs de syntaxe, pour indiquer
        //le token qui a r�v�l� l'erreur.

        /* Donn�es */
        STOCKAGE(Flux) principal;
        // Flux principal de donn�es
        STOCKAGE(Styles) styles;
        //Styles � appliquer sur la sortie de PlanFacile.
        STOCKAGE(Idees) idees;
        //Id�es du document � produire par PlanFacile.
        STOCKAGE(ArbreIdees) arbreidees;
        //Id�es du document organis�es dans le plan
        //calcul�, sous forme de r�f�rences vers la
        //structure pr�c�dente.

        /* Passes */
        STOCKAGE(Retouches) retouches;
        //Structure de donnees utilis�e pour la gestion de la
        //passe de retouches.
        STOCKAGE(ReductionOptions) reductionoptions;
        //Structure de donnees utilis�e pour la gestion de la
        //passe de reduction des options.
        STOCKAGE(ReductionMacros) reductionmacros;
        //Structure de donnees utilis�e pour la gestion de la
        //passe de reduction des macros.
        STOCKAGE(Verification) verification;
        //Structure de donnees utilis�e pour la gestion de la
        //passe de v�rification de flux.
        STOCKAGE(EnregistrementStyles) enregistrementstyles;
        //Structure de donnees utilis�e pour la gestion de la
        //passe d'enregistrement de styles.
        STOCKAGE(EnregistrementIdees) enregistrementidees;
        //Structure de donnees utilis�e pour la gestion de la
        //passe d'enregistrement d'id�es.
        STOCKAGE(CalculPlan) calculplan;
        //Structure de donnees utilis�e pour la gestion de la
        //passe de calcul du plan.
        STOCKAGE(PerformancesPlan) performancesplan;
        //Structure servant � accueillir les informations
        //renvoy�es par l'algorithme de calcul du plan.

        /* Sortie */
        STOCKAGE(Sortie) sortie;
        //Structure utilis�e pour la g�n�ration de la sortie.
};
/* Structure contenant l'ensemble des donn�es g�r�es dynamiquement,
 * en vue de leur suppression en urgence, si le cas se pr�sente.
 * Les trois fonctions suivantes servent � manipuler la structure
 * en elle-m�me, les champs �tant laiss�s � la disposition de
 * l'utilisateur. Cependant, il sera ammen� � copier les valeurs
 * aux emplacements pr�vus, en utilisant *_copie.
 */

Resultat general_initialisation(TRAVAIL(General) general);
/* Cr�e une structure de g�n�ralit� vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation �choue.
 */

Resultat general_ligne_commande(TRAVAIL(General) general, int argc, char *argv[]);
/* Traite la ligne de commande du compilateur,
 * et place les variables en fonction.
 * Renvoie RESULTAT_ERREUR si general est NULL.
 * Renvoie RESULTAT_ERREUR si general->environnement est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat general_copie(TRAVAIL(General) general, TRAVAIL(General) copie);
/* Cr�e une copie de la structure g�n�rale donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation,
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat general_destruction(TRAVAIL(General) general);
/* D�truit une structure g�n�rale et tous ses contenus non vides.
 */

#endif
