/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include <src/global/global.h>
#include <src/donnees/general/general.h>
#include <src/planfacile/planfacile.h>
#include <signal.h>


STOCKAGE(General) general;
/* Structure g�n�rale de planfacile.
 * Elle doit obligatoirement s'appeller
 * 'general', puisque diverses passes
 * d�clarent cette variable en 'extern'.
 */


void gestionnaire_signal_terminaison(int signum)
{
        general_destruction(T_S(general));
        exit(EXIT_FAILURE);
}

void gestion_signal_terminaison(void)
{
        struct sigaction action;

        action.sa_handler = gestionnaire_signal_terminaison;
        sigemptyset(&action.sa_mask);
        action.sa_flags = 0;

        sigaction(SIGTERM, &action, NULL);
}

void exception(Resultat resultat)
{
        const char *label;
        switch(resultat)
        {
                case RESULTAT_ERREUR:                   label = "Mistake in one algorithm";             break;
                case RESULTAT_ERREUR_DEPASSEMENT:       label = "Wrong index value";                    break;
                case RESULTAT_ERREUR_DOMAINE:           label = "Wrong value in data storage";          break;
                case RESULTAT_ERREUR_MEMOIRE:           label = "Insufficient memory";                  break;
                case RESULTAT_ERREUR_TAILLE:            label = "Unmatched data storage size";          break;
                case RESULTAT_ERREUR_NONTROUVE:         label = "Element not found in data storage";    break;
                default:                                label = "Unexpected error";                     break;
        }

        fprintf(SORTIEERREUR, "\n");
        fprintf(SORTIEERREUR, "*** Exception : %s ***\n", label);
        fprintf(SORTIEERREUR, "Congratulations! You just have found a bug in " PROJECT_NAME "...\n");
        fprintf(SORTIEERREUR, "If this bug is permanent and unknown, send to " PROJECT_EMAIL "\n" );
#ifdef BUG_VERBOSE
        fprintf(SORTIEERREUR, "the debugging informations printed above this message,\n");
#else
        fprintf(SORTIEERREUR, "an as exhaustive as possible description of the symptoms,\n");
#endif
        fprintf(SORTIEERREUR, "and eventually your source code if it might help to reproduce the problem,\n");
        fprintf(SORTIEERREUR, "if its content is not confidential.\n");
        exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
        Resultat resultat;
        gestion_signal_terminaison();
        resultat = planfacile_planfacile(T_S(general), argc, argv);
        if(resultat!=RESULTAT_OK)
                exception(resultat);
        return EXIT_SUCCESS;
}
