/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __GLOBAL__
#define __GLOBAL__

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "config.h"


#define ENTREESTANDARD      stdin
#define SORTIESTANDARD      stdout
#define SORTIEERREUR        stderr


//types scalaires de base
typedef int Entier;
typedef double Reel;
typedef char Caractere;
typedef char* Chaine;
typedef void* Pointeur;

//precision sur certains concepts

typedef Entier Indice;
typedef Entier Taille;
typedef Entier IdNoeud;
typedef char* NomNoeud;
//identifiant lisible d'un noeud
typedef Entier IdIdee;
//indice dans la liste d'idees et identifiant d'une idee
typedef Entier Pertinence;
//sert a quantifier la pertinence d'une liaison dans un graphe
typedef Entier NiveauHierarchique;
//precise le niveau hierarchique d'un noeud d'un graphe
//cela a une consequence sur la sortie du plan

typedef Chaine NomFichier;
//Nom de fichier, avec le chemin.

typedef FILE* DescripteurFichier;
//Descripteur de fichier, utilis� pour les entr�es sorties.

typedef Chaine NomOption;
//Nom d'option.

typedef Chaine NomMacro;
//Nom de macro.

typedef Chaine Label;
//Label utilis� dans la commande de s�lection d'options.

//types particuliers
typedef enum {
        RESULTAT_OK,
        RESULTAT_ERREUR,
        RESULTAT_ERREUR_DEPASSEMENT,
        RESULTAT_ERREUR_DOMAINE,
        RESULTAT_ERREUR_MEMOIRE,
        RESULTAT_ERREUR_TAILLE,
        RESULTAT_ERREUR_NONTROUVE,
} Resultat;

typedef enum {FAUX, VRAI} Booleen;

typedef enum {IDEE_VIDE, IDEE_PRESENTE, IDEE_MANQUANTE, IDEE_GENERIQUE, IDEE_RACINE} TypeIdee;

typedef enum {REFERENCE_INDEFINIE, REFERENCE_REDUCTIBLE, REFERENCE_IRREDUCTIBLE} TypeReference;

typedef struct
{
        Indice referencesreductibles;
        Indice referencesirreductibles;
} References;

typedef struct
{
        Indice nombreideestotal;
        Indice nombreideesmanquantes;
        Indice nombreideesgeneralites;
        Indice nombrereferencestotal;
        Indice nombrereferencesirreductibles;
} Statistiques;



/* Couche d'abstraction des structures de donn�es.
 *
 * Les structures seront divis�es en deux cat�gories :
 * 1) les structures scalaires :
 *         Les structures scalaires sont des structures qui
 *         sont leur propre repr�sentant. On y acc�de donc
 *         directement. Cela implique que la modification 
 *         d'un travail scalaire revient � le transformer
 *         en son propre r�sultat.         
 *         Elles sont constitu�s de :
 *         - les types d�finis dans global.h ;
 *         - les types d�finis comme 'enum' ;
 *         - les champs de structure pr�cis�s comme tels.
 * 2) les autres structures :
 *         Les autres structures sont des structures qui sont
 *         compl�tement opaques, puisqu'elles sont toujours
 *         accessibles via un repr�sentant. Ceci implique qu'une
 *         modification d'un travail non scalaire ne revient pas
 *         � le transformer en son propre r�sultat. Bien s�r, un
 *         travail non scalaire peut �tre un r�sultat de fonction.
 *
 * Ces deux cat�gories ont un point commun : elles ob�issent
 * � 5 niveaux d'utilisation diff�rents :
 * 1) le niveau conteneur :
 *         - repr�sentant du type de donn�e ;
 *         - utilis� pour obtenir la taille du type
 *           dans les malloc/realloc.
 * 2) le niveau stockage :
 *         - repr�sentant une instance du type ;
 *         - champ d'une autre structure de donn�e.
 * 3) le niveau cor�f�rence :
 *         - repr�sente une indirection simple ;
 *         - utilis� comme param�tre de fonction lorsque
 *           celui-ci est un r�sultat de celle-ci, mais
 *           obtenu par effet de bord. Ce niveau accepte
 *           les op�rateurs standard du C =, ++, --,
 *           +<entier>, -<entier> et les comparaisons.
 * 4) le niveau travail :
 *         - repr�sente la structure de donn�e manipulable ;
 *         - utilis� comme param�tre de fonction sur laquelle
 *           elle peut agir.
 * 5) le niveau r�f�rence :
 *         - repr�sente la structure par une pure indirection ;
 *         - utilis� lorsqu'une fonction doit renvoyer une structure
 *           de donn�e sur laquelle on peut travailler. Ce niveau
 *           peut accepter les op�rateurs standard du C =, ++, --,
 *           +<entier>, -<entier> et les comparaisons.
 *
 *
 * Ces cinq niveaux ont des comportements bien pr�cis, selon leur
 * d�claration :
 * 1) variable locale ou globale :
 *         conteneur => la d�finition en tant que conteneur est
 *                 interdite ;
 *        stockage => la variable doit etre initialis�e, utilis�e
 *                puis �ventuellement d�truite ;
 *        cor�f�rence => la d�finition en tant que cor�f�rence est
 *                interdite ;
 *        travail => la variable n'a pas � �tre initialis�e et
 *                d�truite. Elle est obtenue par lecture dans une
 *                structure de donn�e ;
 *        r�f�rence => la d�finition en tant que r�f�rence est
 *                interdite.
 * 2) param�tre de fonction :
 *         conteneur => la d�finition en tant que conteneur est
 *                 interdite ;
 *         stockage => la d�finition en tant que stockage est
 *                 interdite ;
 *        cor�f�rence => la d�finition est permise, et cette structure
 *                n'a pas � �tre explicitement initialis� ou d�truit ;
 *         travail => la d�finition est permise, et cette variable
 *                 n'a pas a �tre initialis�e et d�truite. En revanche,
 *                 toute action sur un param�tre travail est une
 *                 op�ration qui ne doit en aucun cas produire un
 *                 r�sultat de la fonction ;
 *         r�f�rence => la d�finition est permise, mais uniquement dans le
 *                 cas o� le param�tre de r�f�rence est uniquement destin�
 *                 � recevoir le r�sultat de la fonction. Dans ce cas,
 *                 l'affectation est r�alis�e au niveau travail. Toute
 *                 autre affectation sera au niveau stockage.
 * 3) champ de structure :
 *         conteneur => la d�finition en tant que conteneur est
 *                 interdite ;
 *         stockage => la d�finition est permise, m�me accompagn�
 *                 du sp�cificateur de tableau ;
 *        cor�f�rence => la d�finition en tant que cor�f�rence est
 *                interdite ;
 *         travail => la d�finition en tant que travail est
 *                 interdite ;
 *         r�f�rence => la d�finition en tant que r�f�rence est
 *                 interdite.
 * 4) op�rations de type :
 *         stockage => l'utilisation est permise, � la fois lors
 *                 de la d�claration du type, que de l'utilisation
 *                 de l'op�rateur sizeof() ;
 *         stockage => la d�finition en tant que stockage est
 *                 interdite ;
 *        cor�f�rence => la d�finition en tant que cor�f�rence est
 *                interdite ;
 *         travail => la d�finition en tant que travail est
 *                 interdite ;
 *         r�f�rence => la d�finition en tant que r�f�rence est
 *                 interdite.
 * 
 * A cela s'ajoute des possibilit�s de conversions entre certains
 * niveaux, et la possibilit�, au niveau stockage, de r�cup�rer un
 * champ d'une structure.
 * 
 * Afin de bien pouvoir communiquer sur cette couche d'abstraction,
 * il semble n�cessaire de fixer les d�nominations :
 * 1) pour les niveaux d'une structure de donn�es :
 *         TRAVAIL(Type) valeur; //valeur est un travail de Type.
 * 2) pour les valeurs utilis�es :
 *         valeur; //un travail valeur
 *         S_T(valeur); //un stockage du travail valeur
 *
 * Niveaux associ�s aux diff�rentes commandes :
 * T_S :                stockage                           => travail
 * S_T :                travail                            => stockage
 * C_S :                stockage                           => coreference
 * S_C :                coreference                        => stockage
 * R_T :                travail                            => reference
 * T_R :                reference                          => travail
 * CHAMP :              (travail, champ)                   => stockage ou stockage_scalaire ou tableau ou tableau_scalaire
 * CHAMP_STOCKAGE :     (stockage, champ)                  => stockage ou stockage_scalaire ou tableau ou tableau_scalaire
 * CHAMP_TRAVAIL :      (travail, champ)                   => travail  ou tableau
 * TABLEAU :            stockage                           => tableau
 * ELEMENT :            (tableau, travail_indice)          => stockage
 * ELEMENT_TRAVAIL :    (tableau, travail_indice)          => travail
 * SIZEOF :             conteneur                          => travail_scalaire
 * SIZEOF_STOCKAGE :    conteneur                          => stockage_scalaire
 *
 * T_S_ :               stockage_scalaire                  => travail_scalaire
 * S_T_ :               travail_scalaire                   => stockage_scalaire
 * C_S_ :               stockage_scalaire                  => coreference_scalaire
 * S_C_ :               coreference_scalaire               => stockage_scalaire
 * R_T_ :               travail_scalaire                   => reference_scalaire
 * T_R_ :               reference_scalaire                 => travail_scalaire
 * R_S_ :               stockage_scalaire                  => reference_scalaire
 * S_R_ :               reference_scalaire                 => stockage_scalaire
 * CHAMP_ :             (travail_scalaire, champ)          => stockage_scalaire ou stockage ou tableau_scalaire ou tableau
 * CHAMP_STOCKAGE_ :    (stockage_scalaire, champ)         => stockage_scalaire ou stockage ou tableau_scalaire ou tableau
 * CHAMP_TRAVAIL_ :     (travail_scalaire, champ)          => travail           ou tableau
 * TABLEAU_ :           stockage_scalaire                  => tableau_scalaire
 * ELEMENT_ :           (tableau_scalaire, travail_indice) => stockage_scalaire
 * ELEMENT_TRAVAIL_ :   (tableau_scalaire, travail_indice) => travail_scalaire
 * SIZEOF_ :            conteneur_scalaire                 => travail_scalaire
 * SIZEOF_STOCKAGE_ :   conteneur_scalaire                 => stockage_scalaire
 */

/* Structures normales : niveaux
 */
#define   CONTENEUR(type)        type
#define    STOCKAGE(type)        type*
#define COREFERENCE(type)        type**
#define     TRAVAIL(type)        type**
#define   REFERENCE(type)        type***

/* Structures normales : conversions de niveaux
 */
#define T_S(stockage)            (&(stockage))
#define S_T(travail)             (*(travail))
#define C_S(stockage)            (&(stockage))
#define S_C(coreference)         (*(coreference))
#define R_T(travail)             (&(travail))
#define T_R(reference)           (*(reference))

/* Structures normales : extraction de champs
 */
#define           CHAMP(travail,champ)        ((*travail)->champ)
#define CHAMP_STOCKAGE(stockage,champ)        ((stockage)->champ)
#define   CHAMP_TRAVAIL(travail,champ)        (&((*travail)->champ))

/* Structures normales : tableaux
 */
#define TABLEAU(stockage)                              stockage*
#define ELEMENT(tableau,travail_indice)                ((tableau)[travail_indice])
#define ELEMENT_TRAVAIL(tableau,travail_indice)        (&((tableau)[travail_indice]))

/* Structures normales : taille du conteneur
 */
#define          SIZEOF(conteneur)        sizeof(conteneur)
#define SIZEOF_STOCKAGE(conteneur)        sizeof(conteneur)

/* "Casts"
 */
#define CAST_S(type, valeur)           ((STOCKAGE(type))(valeur))
#define CAST_T(type, valeur)           ((TRAVAIL(type))(valeur))
#define CAST_R(type, valeur)           ((REFERENCE(type))(valeur))
#define CAST_C(type, valeur)           ((COREFERENCE(type))(valeur))


/* Structures scalaires : niveaux
 */
#define   CONTENEUR_SCALAIRE(type_scalaire)        type_scalaire
#define    STOCKAGE_SCALAIRE(type_scalaire)        type_scalaire
#define COREFERENCE_SCALAIRE(type_scalaire)        type_scalaire*
#define     TRAVAIL_SCALAIRE(type_scalaire)        type_scalaire
#define   REFERENCE_SCALAIRE(type_scalaire)        type_scalaire*

/* Structures scalaires : conversions de niveaux
 */
#define T_S_(stockage_scalaire)         (stockage_scalaire)
#define S_T_(travail_scalaire)          (travail_scalaire)
#define C_S_(stockage_scalaire)         (&(stockage_scalaire))
#define S_C_(coreference_scalaire)      (*(coreference_scalaire))
#define R_T_(travail_scalaire)          (&(travail_scalaire))
#define T_R_(reference_scalaire)        (*(reference_scalaire))
/* Ces deux derni�res ne devraient normalement pas servir,
 * m�me si elles sont autoris�es. A n'utiliser qu'en dernier
 * recours.
 */
#define R_S_(stockage_scalaire)         (&(stockage_scalaire))
#define S_R_(reference_scalaire)        (*(reference_scalaire))

/* Structures scalaires : extraction de champs
 */
#define           CHAMP_(travail_scalaire,champ)        ((travail_scalaire).champ)
#define CHAMP_STOCKAGE_(stockage_scalaire,champ)        ((stockage_scalaire).champ)
#define   CHAMP_TRAVAIL_(travail_scalaire,champ)        (&((travail_scalaire).champ))

/* Structures scalaires : tableaux scalaires
 */
#define TABLEAU_(stockage_scalaire)                                stockage_scalaire*
#define ELEMENT_(tableau_scalaire,travail_scalaire)                ((tableau_scalaire)[travail_scalaire])
#define ELEMENT_TRAVAIL_(tableau_scalaire,travail_scalaire)        ((tableau_scalaire)[travail_scalaire])

/* Structures scalaires : taille du conteneur scalaire
 */
#define          SIZEOF_(conteneur)        sizeof(conteneur)
#define SIZEOF_STOCKAGE_(conteneur)        sizeof(conteneur)

/* "Casts" scalaires
 */
#define CAST_S_(type_scalaire, valeur)           ((STOCKAGE_SCALAIRE(type_scalaire))(valeur))
#define CAST_T_(type_scalaire, valeur)           ((TRAVAIL_SCALAIRE(type_scalaire))(valeur))
#define CAST_R_(type_scalaire, valeur)           ((REFERENCE_SCALAIRE(type_scalaire))(valeur))
#define CAST_C_(type_scalaire, valeur)           ((COREFERENCE_SCALAIRE(type_scalaire))(valeur))

/* Fonctions :
 */
#define FONCTION(fonction)      (*fonction)
#define REFFONCTION(fonction)   (**fonction)
#define F_R(reffonction)        (*reffonction)
#define R_F(fonction)           (&fonction)



/* Simplifions quelques op�rations...
 */
#define TAILLE(type, n)                                  (SIZEOF(CONTENEUR(type))*(n))
#define TAILLE_(type_scalaire, n)                        (SIZEOF_(CONTENEUR_SCALAIRE(type_scalaire))*(n))

#define ALLOCATION_N(type, n)                            (malloc(TAILLE(type, n)))
#define ALLOCATION(type)                                 ALLOCATION_N(type, 1)
#define ALLOCATION_N_(type_scalaire, n)                  (malloc(TAILLE_(type_scalaire, n)))
#define ALLOCATION_(type_scalaire)                       ALLOCATION_N_(type_scalaire, 1)

#define REALLOCATION(stockage, type, n)                  (realloc((void*)(stockage), TAILLE(type, n)))
#define REALLOCATION_(stockage, type_scalaire, n)        (realloc((void*)(stockage), TAILLE_(type_scalaire, n)))
#define REALLOCATION_CAST(stockage, type, n)             ((TABLEAU(STOCKAGE(type)))(REALLOCATION(stockage, type, n)))
#define REALLOCATION_CAST_(stockage, type_scalaire, n)   ((TABLEAU_(STOCKAGE_SCALAIRE(type_scalaire)))(REALLOCATION_(stockage, type_scalaire, n)))

#define NOUVEAUX(type, n)                                ((TABLEAU(STOCKAGE(type)))(ALLOCATION_N(STOCKAGE(type), n)))
#define NOUVEAU(type)                                    ((STOCKAGE(type))(ALLOCATION(type)))
#define NOUVEAUX_(type_scalaire, n)                      ((TABLEAU_(STOCKAGE_SCALAIRE(type_scalaire)))(ALLOCATION_N_(type_scalaire, n)))
#define NOUVEAU_(type_scalaire)                          ((COREFERENCE_SCALAIRE(type_scalaire))(ALLOCATION_(type_scalaire)))

#define STRDUP(chaine)                                   strdup((char*)(chaine))
#define DUP_CAST(type_scalaire, chaine)                  CAST_S_(type_scalaire, STRDUP(chaine))
#define DUP(chaine)                                      ((TABLEAU_(STOCKAGE_SCALAIRE(Caractere)))(STRDUP(chaine)))

#define STRCPY(destination, source)                      CAST_T_(Chaine, strcpy((char*)(destination),       (const char*)(source)))
#define STRCAT(destination, source)                      CAST_T_(Chaine, strcat((char*)(destination),       (const char*)(source)))
#define STRSTR(bottedefoin, aiguille)                    CAST_T_(Chaine, strstr((const char*)(bottedefoin), (const char*)(aiguille)))
#define STRCMP(chaine1, chaine2)                         CAST_T_(Entier, strcmp((const char*)(chaine1),     (const char*)(chaine2)))
#define STRLEN(chaine)                                   CAST_T_(Entier, strlen((const char*)(chaine)))

#define SNPRINTF(tampon, taille, format, ...)            CAST_T_(Entier, snprintf((char*)(tampon), (size_t)(taille), (const char*)(format), ##__VA_ARGS__))

#define FOPEN(fichier, mode)                             CAST_S_(DescripteurFichier, fopen((const char*)(fichier), (const char*)(mode)))
#define STAT(fichier, info)                              CAST_T_(Entier, stat((const char*)(fichier), (struct stat*)(info)))
#define GETENV(nom)                                      CAST_S_(Chaine, getenv((const char*)(nom)))






#ifdef BUG_VERBOSE

static inline void afficher_emplacement(const char* fonction, const char* nomfichier, int ligne)
{
        fprintf(SORTIEERREUR, "in function %s in file %s at line %d\n", fonction, nomfichier, ligne);
}

static inline void afficher_assertion(const char* expression)
{
        fprintf(SORTIEERREUR, "Expression %s not verified\n", expression);
}

#define SECURISE(appel) \
        do {                                                                                    \
                Resultat _resultat_=(appel);                                                    \
                if(_resultat_!=RESULTAT_OK) {                                                   \
                        afficher_emplacement(T_S_(__func__), T_S_(__FILE__), T_S_(__LINE__));   \
                        return _resultat_;                                                      \
                }                                                                               \
        } while(0)

#define ASSERTION(expression, retour)\
        do {                                                                                    \
                if((expression)==0) {                                                           \
                        afficher_assertion(T_S_(#expression));                                  \
                        afficher_emplacement(T_S_(__func__), T_S_(__FILE__), T_S_(__LINE__));   \
                        return (retour);                                                        \
                }                                                                               \
        } while(0)

#else // BUG_VERBOSE

#define SECURISE(appel) \
        do {                                                                                    \
                Resultat _resultat_=(appel);                                                    \
                if(_resultat_!=RESULTAT_OK)                                                     \
                        return _resultat_;                                                      \
        } while(0)

#define ASSERTION(expression, retour)\
        do {                                                                                    \
                if((expression)==0)                                                             \
                        return (retour);                                                        \
        } while(0)

#endif // BUG_VERBOSE

#endif
