/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PROBLEME__
#define __PROBLEME__

#include <src/global/global.h>

#include "messages.h"

#include <src/messages/messages/sources/messageparametres.h>
#include <src/donnees/environnement/environnement.h>
#include <src/donnees/environnement/options/options.h>
#include <src/donnees/commandes/localisationfichier.h>

typedef enum {
        PROBLEME_MEMOIRE_INSUFFISANTE,
        //Correspond au cas o� une fonction renvoie RESULTAT_ERREUR_MEMOIRE
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_RECURSIVITE_FICHIER,
        //Correspond � une boucle dans les inclusions de fichiers.
        //Ce probl�me requiert l'utilisation de :
        //        - le nom du fichier incrimin�.
        PROBLEME_RECURSIVITE_FICHIER_STANDARD,
        //Correspond � une boucle dans les inclusions de fichiers
        //dans le cas de la commande #standard.
        //Ce probl�me requiert l'utilisation de :
        //        - le nom du fichier incrimin�.
        PROBLEME_OPTION_LIGNE_COMMANDE,
        //Correspond � la d�couverte d'une option non reconnue dans
        //la ligne de commande.
        //Ce probl�me requiert l'utilisation de :
        //        - le caract�re de l'option non reconnue.
        PROBLEME_UTILISATION_SORTIE,
        //Correspond au cas o� l'option -o est utilis�e deux fois
        //dans la ligne de commande.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_OUVERTURE_SORTIE,
        //Correspond au cas o� le fichier pr�cis� en sortie ne peut
        //�tre ouvert en �criture.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�.
        PROBLEME_OUVERTURE_REPERTOIRE_COURANT,
        //Correspond au cas o� le r�pertoire courant de planfacile est
        //incorrect.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_OUVERTURE_CHEMIN_INVALIDE,
        //Correspond au cas o� le chemin vers le source est invalide.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_CHEMIN_INVALIDE_STANDARD,
        //Correspond au cas o� le chemin vers le source est invalide,
        //dans le cas de la commande #standard.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_CHEMIN_INVALIDE_TEMPLATE,
        //Correspond au cas o� le chemin vers le source est invalide,
        //dans le cas d'une recherche de template.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_INEXISTANT,
        //Correspond au cas o� le fichier source est introuvable.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_INEXISTANT_STANDARD,
        //Correspond au cas o� le fichier source est introuvable,
        //dans le cas de la commande #standard.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_INEXISTANT_TEMPLATE,
        //Correspond au cas o� le fichier source est introuvable,
        //dans le cas d'une recherche de template.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_TYPE_INCORRECT,
        //Correspond au cas o� le fichier source est de type incorrect
        // (r�pertoire ou fichier de type block)
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_TYPE_INCORRECT_STANDARD,
        //Correspond au cas o� le fichier source est de type incorrect
        // (r�pertoire ou fichier de type block),
        //dans le cas de la commande #standard.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_TYPE_INCORRECT_TEMPLATE,
        //Correspond au cas o� le fichier source est de type incorrect
        // (r�pertoire ou fichier de type block),
        //dans le cas d'une recherche de template.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�
        PROBLEME_OUVERTURE_ENTREE,
        //Correspond au cas o� un fichier pr�cis� en entr�e ne peut
        //�tre ouvert en lecture.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�.
        PROBLEME_OUVERTURE_ENTREE_VIDE,
        //Correspond au cas o� un fichier pr�cis� en entr�e est vide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_OUVERTURE_ENTREE_STANDARD,
        //Correspond au cas o� un fichier pr�cis� en entr�e de la
        //commande #standard ne peut �tre ouvert en lecture.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�.
        PROBLEME_OUVERTURE_ENTREE_TEMPLATE,
        //Correspond au cas o� un fichier template ne peut �tre ouvert en lecture.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�.
        PROBLEME_OUVERTURE_STANDARD,
        //Correspond au cas o� la commande standard ne peut ouvrir
        //aucun fichier.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_OUVERTURE_TEMPLATE,
        //Correspond au cas o� aucun fichier template n'a pu �tre trouv�.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier incrimin�.
        PROBLEME_OUVERTURE_STANDARD_SUIVANTE,
        //Correspond au cas o� la commande #standard va se rabattre
        //sur un fichier de priorit� inf�rieure.
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier suivant test�.
        PROBLEME_OUVERTURE_TEMPLATE_SUIVANTE,
        //Correspond au cas o� la recherche d'un fchier template va se rabattre
        //sur un fichier de priorit� inf�rieure (templates syst�mes).
        //Ce probl�me requiert l'utilisation de :
        //        - nom du fichier suivant test�.
        PROBLEME_AVERTISSEMENT_UTILISATEUR,
        //Correspond � un avertissement explicitement demand� par
        //l'utilisateur.
        //Ce probl�me requiert l'utilisation de :
        //        - le message de l'utilisateur.
        PROBLEME_ERREUR_UTILISATEUR,
        //Correspond � une erreur explicitement demand�e par
        //l'utilisateur.
        //Ce probl�me requiert l'utilisation de :
        //        - le message de l'utilisateur.
        PROBLEME_COMMANDE_INCORRECTE,
        //Correspond � une commande non autoris�e dans un flux donn�.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle commande est incrimin�e.
        PROBLEME_COMMANDE_TEXTE_INCORRECTE,
        //Correspond � une commande non autoris�e dans un flux donn�.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_COMMANDE_ECHAPPEMENT_INCORRECTE,
        //Correspond � une commande non autoris�e dans un flux donn�.
        //Ce probl�me requiert l'utilisation de :
        //        - un caract�re repr�sentant la commande incrimin�e.
        PROBLEME_COMMANDE_MACRO_INCORRECTE,
        //Correspond � une commande non autoris�e dans un flux donn�.
        //Ce probl�me requiert l'utilisation de :
        //        - un nombre repr�sentant le nom de la macro incrimin�e.
        PROBLEME_COMMANDE_PARAMETRE_INCORRECTE,
        //Correspond � une commande non autoris�e dans un flux donn�.
        //Ce probl�me requiert l'utilisation de :
        //        - un nombre repr�sentant la commande incrimin�e.
        PROBLEME_REDEFINITION_MACRO,
        //Correspond au cas d'une red�finition de macro.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine d�crivant quelle macro est incrimin�e.
        PROBLEME_MACRO_INCONNUE,
        //Correspond � la d�couverte d'un appel de macro dont la macro
        //est inconnue.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine d�crivant quelle macro est incrimin�e.
        PROBLEME_RECURSIVITE_MACRO,
        //Correspond � la d�couverte d'une r�cursivit� entre les macros.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine d�crivant quelle macro est incrimin�e.
        PROBLEME_PARAMETRES_MACRO,
        //Correspond � la d�couverte d'un appel de macro contenant un nombre
        //de param�tres incorrects.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine d�crivant quelle macro est incrimin�e,
        //        - un entier indiquant le nombre de param�tres trouv�s,
        //        - un entier indiquant le nombre de param�tres demand�s.
        PROBLEME_OPTION_DANS_MACRO,
        //Correspond au cas o� une commande de d�finition d'option
        //est d�couverte dans une d�finition ou un param�tre de macro.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant l'option d�finie,
        //        - une chaine pr�cisant le nom de la macro victime.
        PROBLEME_REDEFINITION_SECTION,
        //Correspond au cas o� un niveau de format de section
        //est red�fini.
        //Ce probl�me requiert l'utilisation de :
        //        - un entier indiquant le niveau modifi�.
        PROBLEME_REDEFINITION_SECTION_DEFAUT,
        //Correspond au cas o� le format de section
        //par d�faut est red�fini.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_REFERENCE,
        //Correspond au cas o� un niveau de format de r�f�rence
        //est red�fini.
        //Ce probl�me requiert l'utilisation de :
        //        - un entier indiquant le niveau modifi�.
        PROBLEME_REDEFINITION_REFERENCE_DEFAUT,
        //Correspond au cas o� le format de r�f�rence
        //par d�faut est red�fini.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_MESSAGE,
        //Correspond au cas o� le format de message
        //est red�fini.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_ENTETE,
        //Correspond au cas o� le format d'ent�te de document
        //est red�fini.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_PIED,
        //Correspond au cas o� le format de pied de document
        //est red�fini.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_RACINE,
        //Correspond au cas o� le niveau racine est modifi�.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_SECTION_NIVEAU_INCORRECT,
        //Correspond au cas o� le niveau de la commande de
        //section est invalide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REFERENCE_NIVEAU_INCORRECT,
        //Correspond au cas o� le niveau de la commande de
        //r�f�rence est invalide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_RACINE_NIVEAU_INCORRECT,
        //Correspond au cas o� le niveau de la commande de
        //d�finition du niveau racine est invalide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_RACINE_NIVEAU_NON_DEFINI,
        //Correspond au cas o� le niveau de la commande de
        //d�finition du niveau racine ne peut �tre d�termin�.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_IDEE,
        //Correspond au cas o� une id�e est red�finie.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine repr�sentant la r�f�rence de l'id�e.
        PROBLEME_IDEE_REFERENCE_VIDE,
        //Correspond au cas o� une id�e a une r�f�rence vide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_IDEE_MANQUANTE_REFERENCE_VIDE,
        //Correspond au cas o� une id�e manquante a une r�f�rence vide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_IDEE_GENERIQUE_REFERENCE_VIDE,
        //Correspond au cas o� une id�e g�n�rique a une r�f�rence vide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_PERTINENCE_INCORRECTE,
        //Correspond au cas o� la pertinence d'une d�pendance est
        //incorrecte.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle d�pendance est concern�e.
        PROBLEME_INDICE_IDEE_MANQUANTE_INCORRECT,
        //Correspond au cas o� l'indice d'une id�e manquante
        //est incorrect.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_IDEE_MANQUANTE_DEFAUT,
        //Cas o� l'id�e manquante par d�faut est red�finie.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_IDEE_MANQUANTE,
        //Cas o� une id�e manquante est red�finie.
        //Ce probl�me requiert l'utilisation de :
        //        - un entier donnant l'indice red�fini.
        PROBLEME_INDICE_IDEE_GENERIQUE_INCORRECT,
        //Correspond au cas o� l'indice d'une id�e g�n�rique
        //est incorrect.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_IDEE_GENERIQUE_DEFAUT,
        //Cas o� l'id�e g�n�rique par d�faut est red�finie.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_REDEFINITION_IDEE_GENERIQUE,
        //Cas o� une id�e g�n�rique est red�finie.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle id�e est red�finie.
        PROBLEME_DEPENDANCE_INUTILE,
        //Cas o� une d�pendance ne peut �tre utilis�e pour le calcul
        //du plan.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle d�pendance est concern�e.
        PROBLEME_DEPENDANCE_VIDE_INUTILE,
        //Cas o� une d�pendance vide ne peut �tre utilis�e pour le calcul
        //du plan.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_SANS_IDEE_ORPHELINE,
        //Correspond au cas o� aucune id�e orpheline n'est pr�sente dans
        //le graphe de d�pendances.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_AJOUT_RACINE,
        //Correspond au cas o� une id�e racine est ajout�e au graphe
        //de d�pendances.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_IDEE_NON_ATTEIGNABLE,
        //Correspond au cas o� une id�e n'est pas atteignable depuis
        //la racine du graphe de d�pendance.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle id�e est impliqu�e.
        PROBLEME_SECTION_STYLE_MANQUANT,
        //Correspond au cas o� une section n'a pas de style de formattage.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle id�e est impliqu�e.
        PROBLEME_REFERENCE_STYLE_MANQUANT,
        //Correspond au cas o� une r�f�rence n'a pas de style de formattage.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine indiquant quelle r�f�rence est impliqu�e.
        PROBLEME_IDEE_MANQUANTE_MANQUANTE,
        //Correspond au cas o� une id�e manquante n'est pas d�finie.
        //Ce probl�me requiert l'utilisation de :
        //        - un nombre repr�sentant l'indice de l'id�e demand�e.
        PROBLEME_IDEE_GENERIQUE_MANQUANTE,
        //Correspond au cas o� une id�e g�n�rique n'est pas d�finie.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine repr�sentant l'indice de l'id�e demand�e.
        PROBLEME_DEPENDANCE_INCORRECTE,
        //Correspond au cas o� une d�pendance pointe sur une id�e incorrecte.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine repr�sentant la r�f�rence de destination incrimin�e.
        PROBLEME_DEPENDANCE_VIDE,
        //Correspond au cas o� la destination d'une d�pendance est vide.
        //Ce probl�me ne requiert aucun parametre.
        PROBLEME_INDICE_REFERENCE_INCORRECT,
        //Correspond au cas o� l'indice d'une commande #extref est incorrect.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine repr�sentant l'indice incorrect.
        PROBLEME_NOM_NIVEAU_INCORRECT,
        //Correspond au cas o� l'indice de la commande #sec ne donne pas
        //un nom de niveau explicitement d�fini.
        //Ce probl�me requiert l'utilisation de :
        //        - une chaine repr�sentant l'indice incorrect.
        PROBLEME_ERREUR_SYNTAXE
        //Correspond au cas o� l'on d�couvre une erreur de syntaxe dans
        //un fichier source.
        //Ce probl�me requiert l'utilisation de :
        //        - token incrimin�.
} CONTENEUR_SCALAIRE(Probleme);

#include <src/problemes/probleme/erreur.h>
#include <src/problemes/probleme/avertissement.h>

Resultat probleme_probleme(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Probleme) probleme, TRAVAIL(LocalisationFichier) localisationfichier, ...);
/* Annonce d'un probl�me. Le message est donn� sur la sortie d'erreur.
 * Si la localisation est non vide, la position du probl�me est �galement
 * donn�e.
 * Renvoie RESULTAT_ERREUR si environnement est vide.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Attention ! Les param�tres surnum�raires sont les param�tres
 * du message correspondant au probl�me rencontr�. Le nombre et le type
 * de ces arguments doivent absolument correspondre avec leurs sp�cifications,
 * sous peine de comportement ind�fini.
 */

Resultat probleme_message(TRAVAIL_SCALAIRE(Probleme) probleme, REFERENCE_SCALAIRE(Message) message);
/* Renvoie le message associ� � un probl�me d�termin�.
 * Si le probl�me n'est pas r�pertori�, RESULTAT_ERREUR est renvoy�.
 */

Resultat probleme_localisation(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) sortie, TRAVAIL(LocalisationFichier) localisationfichier);
/* Affiche le contenu d'une localisation de fichier sur la sortie indiqu�e
 * par l'environnement.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

#endif
