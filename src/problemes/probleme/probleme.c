/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "probleme.h"

typedef enum
{
        TYPEPROBLEME_AVERTISSEMENT,
        TYPEPROBLEME_ERREUR,
        TYPEPROBLEME_RIEN
} CONTENEUR_SCALAIRE(TypeProbleme);

static Resultat probleme_localisationinterne(TRAVAIL(Environnement) environment, TRAVAIL_SCALAIRE(DescripteurFichier) sortie, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Affiche la localisation des fichiers parents
         * dans les inclusions.
         */
        STOCKAGE(MessageParametres) messageparametres;
        TRAVAIL(LocalisationFichier) localisationinclusion;
        TRAVAIL_SCALAIRE(NomFichier) nom;
        TRAVAIL_SCALAIRE(PositionFichier) position;
        if(S_T(localisationfichier)==NULL)
        {
                SECURISE(messageparametres_initialisation(T_S(messageparametres)));
                SECURISE(messages_message(environment, sortie, T_S_(MESSAGE_POINT), T_S(messageparametres)));
                SECURISE(messageparametres_destruction(T_S(messageparametres)));
                return RESULTAT_OK;
        }
        SECURISE(messageparametres_initialisation(T_S(messageparametres)));
        SECURISE(messages_message(environment, sortie, T_S_(MESSAGE_VIRGULE), T_S(messageparametres)));
        SECURISE(localisationfichier_lecture_nom(localisationfichier, R_T_(nom)));
        SECURISE(localisationfichier_lecture_position(localisationfichier, R_T_(position)));
        SECURISE(messageparametres_ajout_chaine(T_S(messageparametres), (TRAVAIL_SCALAIRE(Chaine))(nom)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres), (TRAVAIL_SCALAIRE(Entier))(position)));
        SECURISE(messages_message(environment, sortie,T_S_(MESSAGE_INCLUSION), T_S(messageparametres)));
        SECURISE(messageparametres_destruction(T_S(messageparametres)));
        SECURISE(localisationfichier_lecture_inclusion(localisationfichier, R_T(localisationinclusion)));
        SECURISE(probleme_localisationinterne(environment, sortie, localisationinclusion));
        return RESULTAT_OK;
}

static Resultat probleme_typeprobleme(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Probleme) probleme, REFERENCE_SCALAIRE(TypeProbleme) type)
{
        /* Renvoie le type de probl�me associ� � un probl�me,
         * dans un environnement donn�.
         */
        TRAVAIL(Options) options;
        TRAVAIL_SCALAIRE(Booleen) option;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_options(environnement, R_T(options)));
        switch(probleme)
        {
                case T_S_(PROBLEME_MEMOIRE_INSUFFISANTE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_RECURSIVITE_FICHIER):
                        SECURISE(options_lecture_inclusion(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_RECURSIVITE_FICHIER_STANDARD):
                        SECURISE(options_lecture_recherche(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_OPTION_LIGNE_COMMANDE):
                        T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        break;
                case T_S_(PROBLEME_UTILISATION_SORTIE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_OUVERTURE_SORTIE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_OUVERTURE_REPERTOIRE_COURANT):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE):
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT):
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT):
                case T_S_(PROBLEME_OUVERTURE_ENTREE):
                case T_S_(PROBLEME_OUVERTURE_ENTREE_VIDE):
                        SECURISE(options_lecture_source(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_ENTREE_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE_TEMPLATE):
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT_TEMPLATE):
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT_TEMPLATE):
                case T_S_(PROBLEME_OUVERTURE_ENTREE_TEMPLATE):
                        SECURISE(options_lecture_recherche(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_OUVERTURE_STANDARD_SUIVANTE):
                case T_S_(PROBLEME_OUVERTURE_TEMPLATE_SUIVANTE):
                        SECURISE(options_lecture_recherche(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_OUVERTURE_STANDARD):
                        SECURISE(options_lecture_recherche(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        break;
                case T_S_(PROBLEME_OUVERTURE_TEMPLATE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_AVERTISSEMENT_UTILISATEUR):
                        SECURISE(options_lecture_utilisateur(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_ERREUR_UTILISATEUR):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_COMMANDE_INCORRECTE):
                case T_S_(PROBLEME_COMMANDE_TEXTE_INCORRECTE):
                case T_S_(PROBLEME_COMMANDE_ECHAPPEMENT_INCORRECTE):
                case T_S_(PROBLEME_COMMANDE_MACRO_INCORRECTE):
                case T_S_(PROBLEME_COMMANDE_PARAMETRE_INCORRECTE):
                        SECURISE(options_lecture_commandes(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_MACRO_INCONNUE):
                        SECURISE(options_lecture_macroinconnue(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_OPTION_DANS_MACRO):
                        SECURISE(options_lecture_optionmacro(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_RECURSIVITE_MACRO):
                        SECURISE(options_lecture_recursivite(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_REDEFINITION_MACRO):
                        SECURISE(options_lecture_redefinitionmacro(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_PARAMETRES_MACRO):
                        SECURISE(options_lecture_parametres(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_REDEFINITION_SECTION):
                case T_S_(PROBLEME_REDEFINITION_SECTION_DEFAUT):
                case T_S_(PROBLEME_REDEFINITION_REFERENCE):
                case T_S_(PROBLEME_REDEFINITION_REFERENCE_DEFAUT):
                case T_S_(PROBLEME_REDEFINITION_MESSAGE):
                case T_S_(PROBLEME_REDEFINITION_ENTETE):
                case T_S_(PROBLEME_REDEFINITION_PIED):
                case T_S_(PROBLEME_REDEFINITION_RACINE):
                        SECURISE(options_lecture_redefinitionstyles(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_SECTION_NIVEAU_INCORRECT):
                case T_S_(PROBLEME_REFERENCE_NIVEAU_INCORRECT):
                case T_S_(PROBLEME_RACINE_NIVEAU_INCORRECT):
                        SECURISE(options_lecture_niveauincorrect(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_RACINE_NIVEAU_NON_DEFINI):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_IDEE_REFERENCE_VIDE):
                case T_S_(PROBLEME_IDEE_MANQUANTE_REFERENCE_VIDE):
                case T_S_(PROBLEME_IDEE_GENERIQUE_REFERENCE_VIDE):
                        SECURISE(options_lecture_ideereferencevide(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE):
                case T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE):
                case T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE_DEFAUT):
                case T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE):
                case T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE_DEFAUT):
                        SECURISE(options_lecture_redefinitionidees(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_PERTINENCE_INCORRECTE):
                        SECURISE(options_lecture_pertinenceincorrecte(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_INDICE_IDEE_MANQUANTE_INCORRECT):
                        SECURISE(options_lecture_indiceincorrect(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_INDICE_IDEE_GENERIQUE_INCORRECT):
                        SECURISE(options_lecture_referenceincorrecte(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_INUTILE):
                case T_S_(PROBLEME_DEPENDANCE_VIDE_INUTILE):
                        SECURISE(options_lecture_dependancesinutiles(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_SANS_IDEE_ORPHELINE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_AJOUT_RACINE):
                        SECURISE(options_lecture_ajoutracine(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_RIEN);
                        break;
                case T_S_(PROBLEME_IDEE_NON_ATTEIGNABLE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_ERREUR_SYNTAXE):
                        T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_SECTION_STYLE_MANQUANT):
                        SECURISE(options_lecture_sectionstylemanquant(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_REFERENCE_STYLE_MANQUANT):
                        SECURISE(options_lecture_referencestylemanquant(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_IDEE_MANQUANTE_MANQUANTE):
                case T_S_(PROBLEME_IDEE_GENERIQUE_MANQUANTE):
                        SECURISE(options_lecture_ideeautomatiquemanquante(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_INCORRECTE):
                case T_S_(PROBLEME_DEPENDANCE_VIDE):
                        SECURISE(options_lecture_dependancesincorrectes(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_INDICE_REFERENCE_INCORRECT):
                        SECURISE(options_lecture_indicereferenceincorrect(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                case T_S_(PROBLEME_NOM_NIVEAU_INCORRECT):
                        SECURISE(options_lecture_nomsectionincorrect(options, R_T_(option)));
                        if(option==T_S_(VRAI))
                                T_R_(type)=T_S_(TYPEPROBLEME_AVERTISSEMENT);
                        else
                                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
                        break;
                default:
                        return RESULTAT_ERREUR;
                        break;
        }
        SECURISE(options_lecture_erreur(options, R_T_(option)));
        if((option==T_S_(VRAI))&&(T_R_(type)==T_S_(TYPEPROBLEME_AVERTISSEMENT)))
                T_R_(type)=T_S_(TYPEPROBLEME_ERREUR);
        return RESULTAT_OK;
}

Resultat probleme_probleme(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Probleme) probleme, TRAVAIL(LocalisationFichier) localisationfichier, ...)
{
        /* Annonce d'un probl�me. Le message est donn� sur la sortie d'erreur.
         * Si la localisation est non vide, la position du probl�me est �galement
         * donn�e.
         * Renvoie RESULTAT_ERREUR si environnement est null.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Attention ! Les param�tres surnum�raires sont les param�tres
         * du message correspondant au probl�me rencontr�. Le nombre et le type
         * de ces arguments doivent absolument correspondre avec leurs sp�cifications,
         * sous peine de comportement ind�fini.
         */
        TRAVAIL_SCALAIRE(va_list) ap;
        TRAVAIL_SCALAIRE(Chaine) format;
        TRAVAIL_SCALAIRE(Message) messageerreur;
        STOCKAGE(MessageParametres) parametres;
        TRAVAIL_SCALAIRE(TypeProbleme) type;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(probleme_message(probleme, R_T_(messageerreur)));
        SECURISE(messages_parametres(messageerreur, R_T_(format)));
        SECURISE(messageparametres_initialisation(T_S(parametres)));
        va_start(ap, localisationfichier);
        for( ; S_C_((COREFERENCE_SCALAIRE(Caractere))(format))!='\0' ; format++)
                switch(S_C_((COREFERENCE_SCALAIRE(Caractere))(format)))
                {
                        case T_S_('d'):
                        case T_S_('i'):
                        case T_S_('o'):
                        case T_S_('u'):
                        case T_S_('x'):
                        case T_S_('X'):
                                SECURISE(messageparametres_ajout_entier(T_S(parametres), va_arg(ap, CONTENEUR_SCALAIRE(Entier))));
                                break;
                        case T_S_('f'):
                        case T_S_('F'):
                        case T_S_('e'):
                        case T_S_('E'):
                        case T_S_('g'):
                        case T_S_('G'):
                                SECURISE(messageparametres_ajout_reel(T_S(parametres), va_arg(ap, CONTENEUR_SCALAIRE(Reel))));
                                break;
                        case T_S_('c'):
                                SECURISE(messageparametres_ajout_caractere(T_S(parametres), (STOCKAGE_SCALAIRE(Caractere))(va_arg(ap, CONTENEUR_SCALAIRE(Entier)))));
                                break;
                        case T_S_('s'):
                                SECURISE(messageparametres_ajout_chaine(T_S(parametres), va_arg(ap, CONTENEUR_SCALAIRE(Chaine))));
                                break;
                        default:
                                SECURISE(messageparametres_ajout_pointeur(T_S(parametres), va_arg(ap, CONTENEUR_SCALAIRE(Pointeur))));
                                break;
                }
        va_end(ap);
        SECURISE(probleme_typeprobleme(environnement, probleme, R_T_(type)));
        switch(type)
        {
                case T_S_(TYPEPROBLEME_ERREUR):
                        SECURISE(erreur_erreur(environnement, probleme, localisationfichier, T_S(parametres)));
                        break;
                case T_S_(TYPEPROBLEME_AVERTISSEMENT):
                        SECURISE(avertissement_avertissement(environnement, probleme, localisationfichier, T_S(parametres)));
                        break;
                case T_S_(TYPEPROBLEME_RIEN):
                        break;
                default:
                        return RESULTAT_ERREUR;
        }
        SECURISE(messageparametres_destruction(T_S(parametres)));
        return RESULTAT_OK;
}

Resultat probleme_message(TRAVAIL_SCALAIRE(Probleme) probleme, REFERENCE_SCALAIRE(Message) message)
{
        /* Renvoie le message associ� � un probl�me d�termin�.
         * Si le probl�me n'est pas r�pertori�, RESULTAT_ERREUR_DOMAINE est renvoy�.
         */
        switch(probleme)
        {
                case T_S_(PROBLEME_MEMOIRE_INSUFFISANTE):
                        T_R_(message)=T_S_(MESSAGE_MEMOIRE_INSUFFISANTE);
                        break;
                case T_S_(PROBLEME_RECURSIVITE_FICHIER):
                case T_S_(PROBLEME_RECURSIVITE_FICHIER_STANDARD):
                        T_R_(message)=T_S_(MESSAGE_RECURSIVITE_FICHIER);
                        break;
                case T_S_(PROBLEME_OPTION_LIGNE_COMMANDE):
                        T_R_(message)=T_S_(MESSAGE_OPTION_LIGNE_COMMANDE);
                        break;
                case T_S_(PROBLEME_UTILISATION_SORTIE):
                        T_R_(message)=T_S_(MESSAGE_UTILISATION_SORTIE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_SORTIE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_SORTIE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_REPERTOIRE_COURANT):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_REPERTOIRE_COURANT);
                        break;
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE):
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_CHEMIN_INVALIDE_TEMPLATE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_CHEMIN_INVALIDE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT):
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_INEXISTANT_TEMPLATE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_INEXISTANT);
                        break;
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT):
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_TYPE_INCORRECT_TEMPLATE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_TYPE_INCORRECT);
                        break;
                case T_S_(PROBLEME_OUVERTURE_ENTREE):
                case T_S_(PROBLEME_OUVERTURE_ENTREE_STANDARD):
                case T_S_(PROBLEME_OUVERTURE_ENTREE_TEMPLATE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_ENTREE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_ENTREE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_ENTREE_VIDE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_STANDARD_SUIVANTE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_STANDARD_SUIVANTE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_STANDARD):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_STANDARD);
                        break;
                case T_S_(PROBLEME_OUVERTURE_TEMPLATE_SUIVANTE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_TEMPLATE_SUIVANTE);
                        break;
                case T_S_(PROBLEME_OUVERTURE_TEMPLATE):
                        T_R_(message)=T_S_(MESSAGE_OUVERTURE_TEMPLATE);
                        break;
                case T_S_(PROBLEME_AVERTISSEMENT_UTILISATEUR):
                        T_R_(message)=T_S_(MESSAGE_UTILISATEUR);
                        break;
                case T_S_(PROBLEME_ERREUR_UTILISATEUR):
                        T_R_(message)=T_S_(MESSAGE_UTILISATEUR);
                        break;
                case T_S_(PROBLEME_COMMANDE_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_COMMANDE_INCORRECTE);
                        break;
                case T_S_(PROBLEME_COMMANDE_TEXTE_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_COMMANDE_TEXTE_INCORRECTE);
                        break;
                case T_S_(PROBLEME_COMMANDE_ECHAPPEMENT_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_COMMANDE_ECHAPPEMENT_INCORRECTE);
                        break;
                case T_S_(PROBLEME_COMMANDE_MACRO_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_COMMANDE_MACRO_INCORRECTE);
                        break;
                case T_S_(PROBLEME_COMMANDE_PARAMETRE_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_COMMANDE_PARAMETRE_INCORRECTE);
                        break;
                case T_S_(PROBLEME_MACRO_INCONNUE):
                        T_R_(message)=T_S_(MESSAGE_MACRO_INCONNUE);
                        break;
                case T_S_(PROBLEME_RECURSIVITE_MACRO):
                        T_R_(message)=T_S_(MESSAGE_RECURSIVITE_MACRO);
                        break;
                case T_S_(PROBLEME_REDEFINITION_MACRO):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_MACRO);
                        break;
                case T_S_(PROBLEME_PARAMETRES_MACRO):
                        T_R_(message)=T_S_(MESSAGE_PARAMETRES_MACRO);
                        break;
                case T_S_(PROBLEME_OPTION_DANS_MACRO):
                        T_R_(message)=T_S_(MESSAGE_OPTION_DANS_MACRO);
                        break;
                case T_S_(PROBLEME_REDEFINITION_SECTION):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_SECTION);
                        break;
                case T_S_(PROBLEME_REDEFINITION_SECTION_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_SECTION_DEFAUT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_REFERENCE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_REFERENCE);
                        break;
                case T_S_(PROBLEME_REDEFINITION_REFERENCE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_REFERENCE_DEFAUT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_MESSAGE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_MESSAGE);
                        break;
                case T_S_(PROBLEME_REDEFINITION_ENTETE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_ENTETE);
                        break;
                case T_S_(PROBLEME_REDEFINITION_PIED):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_PIED);
                        break;
                case T_S_(PROBLEME_REDEFINITION_RACINE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_RACINE);
                        break;
                case T_S_(PROBLEME_SECTION_NIVEAU_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_SECTION_NIVEAU_INCORRECT);
                        break;
                case T_S_(PROBLEME_REFERENCE_NIVEAU_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_REFERENCE_NIVEAU_INCORRECT);
                        break;
                case T_S_(PROBLEME_RACINE_NIVEAU_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_RACINE_NIVEAU_INCORRECT);
                        break;
                case T_S_(PROBLEME_RACINE_NIVEAU_NON_DEFINI):
                        T_R_(message)=T_S_(MESSAGE_RACINE_NIVEAU_NON_DEFINI);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_IDEE);
                        break;
                case T_S_(PROBLEME_IDEE_REFERENCE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_REFERENCE_VIDE);
                        break;
                case T_S_(PROBLEME_IDEE_MANQUANTE_REFERENCE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_MANQUANTE_REFERENCE_VIDE);
                        break;
                case T_S_(PROBLEME_IDEE_GENERIQUE_REFERENCE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_GENERIQUE_REFERENCE_VIDE);
                        break;
                case T_S_(PROBLEME_PERTINENCE_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_PERTINENCE_INCORRECTE);
                        break;
                case T_S_(PROBLEME_INDICE_IDEE_MANQUANTE_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_INDICE_IDEE_MANQUANTE_INCORRECT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_IDEE_MANQUANTE_DEFAUT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_IDEE_MANQUANTE);
                        break;
                case T_S_(PROBLEME_INDICE_IDEE_GENERIQUE_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_INDICE_IDEE_GENERIQUE_INCORRECT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_IDEE_GENERIQUE_DEFAUT);
                        break;
                case T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE):
                        T_R_(message)=T_S_(MESSAGE_REDEFINITION_IDEE_GENERIQUE);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_INUTILE):
                        T_R_(message)=T_S_(MESSAGE_DEPENDANCE_INUTILE);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_VIDE_INUTILE):
                        T_R_(message)=T_S_(MESSAGE_DEPENDANCE_VIDE_INUTILE);
                        break;
                case T_S_(PROBLEME_SANS_IDEE_ORPHELINE):
                        T_R_(message)=T_S_(MESSAGE_SANS_IDEE_ORPHELINE);
                        break;
                case T_S_(PROBLEME_AJOUT_RACINE):
                        T_R_(message)=T_S_(MESSAGE_AJOUT_RACINE);
                        break;
                case T_S_(PROBLEME_IDEE_NON_ATTEIGNABLE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_NON_ATTEIGNABLE);
                        break;
                case T_S_(PROBLEME_ERREUR_SYNTAXE):
                        T_R_(message)=T_S_(MESSAGE_ERREUR_SYNTAXE);
                        break;
                case T_S_(PROBLEME_SECTION_STYLE_MANQUANT):
                        T_R_(message)=T_S_(MESSAGE_SECTION_STYLE_MANQUANT);
                        break;
                case T_S_(PROBLEME_REFERENCE_STYLE_MANQUANT):
                        T_R_(message)=T_S_(MESSAGE_REFERENCE_STYLE_MANQUANT);
                        break;
                case T_S_(PROBLEME_IDEE_MANQUANTE_MANQUANTE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_MANQUANTE_MANQUANTE);
                        break;
                case T_S_(PROBLEME_IDEE_GENERIQUE_MANQUANTE):
                        T_R_(message)=T_S_(MESSAGE_IDEE_GENERIQUE_MANQUANTE);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_DEPENDANCE_VIDE);
                        break;
                case T_S_(PROBLEME_DEPENDANCE_INCORRECTE):
                        T_R_(message)=T_S_(MESSAGE_DEPENDANCE_INCORRECTE);
                        break;
                case T_S_(PROBLEME_INDICE_REFERENCE_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_INDICE_REFERENCE_INCORRECT);
                        break;
                case T_S_(PROBLEME_NOM_NIVEAU_INCORRECT):
                        T_R_(message)=T_S_(MESSAGE_NOM_NIVEAU_INCORRECT);
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

Resultat probleme_localisation(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(DescripteurFichier) sortie, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Affiche le contenu d'une localisation de fichier sur la sortie indiqu�e,
         * en utilisant l'environment.
         * Renvoie RESULTAT_ERREUR si environment est NULL.
         */
        STOCKAGE(MessageParametres) messageparametres;
        TRAVAIL(LocalisationFichier) localisationinclusion;
        TRAVAIL_SCALAIRE(NomFichier) nom;
        TRAVAIL_SCALAIRE(PositionFichier) position;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        if(S_T(localisationfichier)==NULL)
        {
                SECURISE(messageparametres_initialisation(T_S(messageparametres)));
                SECURISE(messages_message(environnement, sortie, T_S_(MESSAGE_POINT), T_S(messageparametres)));
                SECURISE(messageparametres_destruction(T_S(messageparametres)));
                return RESULTAT_OK;
        }
        SECURISE(messageparametres_initialisation(T_S(messageparametres)));
        SECURISE(messages_message(environnement, sortie,T_S_(MESSAGE_VIRGULE), T_S(messageparametres)));
        SECURISE(localisationfichier_lecture_nom(localisationfichier, R_T_(nom)));
        SECURISE(localisationfichier_lecture_position(localisationfichier, R_T_(position)));
        SECURISE(messageparametres_ajout_chaine(T_S(messageparametres), (TRAVAIL_SCALAIRE(Chaine))(nom)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres), (TRAVAIL_SCALAIRE(Entier))(position)));
        SECURISE(messages_message(environnement, sortie,T_S_(MESSAGE_LOCALISATION), T_S(messageparametres)));
        SECURISE(messageparametres_destruction(T_S(messageparametres)));
        SECURISE(localisationfichier_lecture_inclusion(localisationfichier, R_T(localisationinclusion)));
        SECURISE(probleme_localisationinterne(environnement, sortie, localisationinclusion));
        return RESULTAT_OK;
}

