/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "avertissement.h"

Resultat avertissement_avertissement(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Probleme) probleme, TRAVAIL(LocalisationFichier) localisationfichier, TRAVAIL(MessageParametres) messageparametres)
{
        /* Signale un avertissement sur la sortie consacr�e aux erreurs.
         * Attention ! Les param�tres de message doivent correspondre
         * � ceux attendus, sous peine de r�sultats impr�visibles.
         */
        TRAVAIL_SCALAIRE(DescripteurFichier) erreur;
        TRAVAIL_SCALAIRE(Message) message;
        STOCKAGE(MessageParametres) parametres;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_erreur(environnement, R_T_(erreur)));
        SECURISE(probleme_message(probleme, R_T_(message)));
        SECURISE(messageparametres_initialisation(T_S(parametres)));
        SECURISE(messages_message(environnement, erreur, T_S_(MESSAGE_AVERTISSEMENT), T_S(parametres)));
        SECURISE(messages_message(environnement, erreur, T_S_(MESSAGE_DOUBLE_POINTS), T_S(parametres)));
        SECURISE(messageparametres_destruction(T_S(parametres)));
        SECURISE(messages_message(environnement, erreur, message, messageparametres));
        SECURISE(probleme_localisation(environnement, erreur, localisationfichier));
        return RESULTAT_OK;
}

