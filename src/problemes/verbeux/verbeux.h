/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __VERBEUX__
#define __VERBEUX__

#include <src/global/global.h>

#include "messages.h"

#include <src/messages/messages/sources/messageparametres.h>
#include <src/donnees/environnement/environnement.h>
#include <src/donnees/environnement/options/options.h>

typedef enum
{
        VERBEUX_ANALYSE,
        //Ce message de niveau 1 indique
        //le d�but de l'analyse du source.
        //Ce message ne requiert aucun parametre.
        VERBEUX_ANALYSE_SOURCE,
        //Ce message de niveau 3 indique
        //le d�but de l'analyse d'un fichier source.
        //Ce message requiert l'utilisation de :
        //        - nom du fichier.
        VERBEUX_ANALYSE_FINSOURCE,
        //Ce message de niveau 4 indique
        //la fin de l'analyse d'un fichier source.
        //Ce message ne requiert aucun parametre.
        VERBEUX_ANALYSE_INCLUSION,
        //Ce message de niveau 4 indique
        //le d�but de l'analyse d'un fichier source inclus.
        //Ce message requiert l'utilisation de :
        //        - nom du fichier.
        VERBEUX_ANALYSE_FININCLUSION,
        //Ce message de niveau 4 indique
        //la fin de l'analyse d'un fichier inclus.
        //Ce message ne requiert aucun parametre.
        VERBEUX_RETOUCHES,
        //Ce message de niveau 1 indique
        //le d�but de la passe de retouches.
        //Ce message ne requiert aucun parametre.
        VERBEUX_RETOUCHES_PARAMETRE,
        //Ce message de niveau 4 indique
        //que l'on a trouv� une commande param�tre.
        //Ce message requiert l'utilisation de :
        //        - indice du param�tre.
        VERBEUX_RETOUCHES_MACRO_NORMALE,
        //Ce message de niveau 3 indique
        //que l'on a rencontr� une macro qui n'est
        //pas une boucle.
        //Ce message requiert l'utilisation de :
        //        - chaine indiquant le nom de la macro ;
        //        - nombre de param�tres de la macro.
        VERBEUX_RETOUCHES_MACRO_BOUCLE,
        //Ce message de niveau 3 indique
        //que l'on a rencontr� une macro qui est
        //une boucle.
        //Ce message requiert l'utilisation de :
        //        - chaine indiquant le nom de la macro ;
        //        - nombre de param�tres de la macro.
        VERBEUX_RETOUCHES_DEFRAGMENTATION,
        //Ce message de niveau 5 indique
        //la d�fragmentation d'un flux
        //Ce message requiert l'utilisation de :
        //        - le type (num�rique) du flux.
        VERBEUX_RETOUCHES_INCLUDE,
        //Ce message de niveau 4 indique
        //Ce message ne requiert aucun parametre.
        VERBEUX_RETOUCHES_STANDARD,
        //Ce message de niveau 4 indique
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS,
        //Ce message de niveau 1 indique
        //le d�but de la passe de r�duction d'options.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_RECHERCHE,
        //Ce message de niveau 2 indique
        //le d�but de la passe de recherche des options
        //d�finies.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_RECHERCHE_PREMIERE,
        //Ce message de niveau 3 indique
        //que la premi�re passe de recherche est lanc�e.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_RECHERCHE_NOUVELLE,
        //Ce message de niveau 3 indique
        //qu'une nouvelle passe de recherche est lanc�e.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_RECHERCHE_FIN,
        //Ce message de niveau 3 indique
        //que la recherche des options est termin�e.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_ENREGISTREMENT,
        //Ce message de niveau 4 indique
        //l'enregistrement d'une option.
        //Ce message requiert l'utilisation de :
        //        - nom de l'option.
        VERBEUX_OPTIONS_COMMENTAIRE,
        //Ce message de niveau 4 indique
        //l'abandon d'une option d�finie dans un commentaire.
        //Ce message requiert l'utilisation de :
        //        - nom de l'option.
        VERBEUX_OPTIONS_MACRO,
        //Ce message de niveau 4 indique
        //l'abandon d'une option d�finie dans une macro.
        //Ce message requiert l'utilisation de :
        //        - nom de l'option ;
        //        - nom de la macro.
        VERBEUX_OPTIONS_REDUCTION,
        //Ce message de niveau 2 indique
        //le d�but de la passe de r�duction des commandes
        //de s�lection de code.
        //Ce message ne requiert aucun parametre.
        VERBEUX_OPTIONS_REDUCTION_OPTIONS,
        //Ce message de niveau 4 indique
        //la r�duction d'une commande #options.
        //Ce message requiert l'utilisation de :
        //        - le label de la commande.
        VERBEUX_MACROS,
        //Ce message de niveau 1 indique
        //le d�but de la passe de r�duction de macros.
        //Ce message ne requiert aucun parametre.
        VERBEUX_MACROS_NORMALE,
        //Ce message de niveau 3 indique
        //r�duction d'une macro normale.
        //Ce message requiert l'utilisation de :
        //        - nom de la macro.
        VERBEUX_MACROS_BOUCLE,
        //Ce message de niveau 3 indique
        //r�duction d'une macro boucle.
        //Ce message requiert l'utilisation de :
        //        - nom de la macro.
        VERBEUX_MACROS_PARAMETRE,
        //Ce message de niveau 4 indique
        //un remplacement de param�tre.
        //Ce message requiert l'utilisation de :
        //        - indice du param�tre.
        VERBEUX_MACROS_ITERATION,
        //Ce message de niveau 4 indique
        //une it�ration de boucle de macro.
        //Ce message requiert l'utilisation de :
        //        - nom de la macro ;
        //        - indice de l'it�ration.
        VERBEUX_VERIFICATION,
        //Ce message de niveau 1 indique
        //le d�but de la passe de v�rification s�mantique.
        //Ce message ne requiert aucun parametre.
        VERBEUX_VERIFICATION_DEFRAGMENTATION,
        //Ce message de niveau 5 indique
        //le d�but de la passe de v�rification s�mantique.
        //Ce message requiert l'utilisation de :
        //        - le type (num�rique) du flux.
        VERBEUX_VERIFICATION_ERREUR,
        //Ce message de niveau 3 indique
        //le d�but de la passe de v�rification s�mantique.
        //Ce message ne requiert aucun parametre.
        VERBEUX_VERIFICATION_AVERTISSEMENT,
        //Ce message de niveau 3 indique
        //le d�but de la passe de v�rification s�mantique.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES,
        //Ce message de niveau 1 indique
        //le d�but de la passe d'enregistrement des styles.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_SECTION,
        //Ce message de niveau 2 indique
        //le d�but de la passe d'enregistrement des styles
        //de section.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_SECTION_DEFAUT,
        //Ce message de niveau 3 indique
        //l'enregistrement d'un style de section par d�faut.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_SECTION_EXPLICITE,
        //Ce message de niveau 3 indique
        //l'enregistrement d'un style de section explicite.
        //Ce message requiert l'utilisation de :
        //        - niveau concern� par l'enregistrement.
        VERBEUX_STYLES_AUTRES,
        //Ce message de niveau 2 indique
        //le d�but de la passe d'enregistrement des autres
        //styles.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_REFERENCE_DEFAUT,
        //Ce message de niveau 3 indique
        //l'enregistrement d'un style de r�f�rence par d�faut.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_REFERENCE_EXPLICITE,
        //Ce message de niveau 3 indique
        //l'enregistrement d'un style de r�f�rence explicite.
        //Ce message requiert l'utilisation de :
        //        - niveau concern� par l'enregistrement.
        VERBEUX_STYLES_MESSAGE,
        //Ce message de niveau 3 indique
        //l'enregistrement du style de message.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_ENTETE,
        //Ce message de niveau 3 indique
        //l'enregistrement du style d'ent�te.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_PIED,
        //Ce message de niveau 3 indique
        //l'enregistrement du style de pied.
        //Ce message ne requiert aucun parametre.
        VERBEUX_STYLES_RACINE,
        //Ce message de niveau 3 indique
        //l'enregistrement du niveau de la racine.
        //Ce message requiert l'utilisation de :
        //        - niveau concern� par l'enregistrement.
        VERBEUX_IDEES,
        //Ce message de niveau 1 indique
        //le d�but de la passe d'enregistrement des id�es.
        //Ce message ne requiert aucun parametre.
        VERBEUX_IDEES_PRESENTES,
        //Ce message de niveau 2 indique
        //le d�but de la passe d'enregistrement des id�es
        //pr�sentes.
        //Ce message ne requiert aucun parametre.
        VERBEUX_IDEES_IDEE,
        //Ce message de niveau 3 indique
        //l'enregistrement d'une id�e normale.
        //Ce message requiert l'utilisation de :
        //        - r�f�rence de l'id�e.
        VERBEUX_IDEES_DEPENDANCE,
        //Ce message de niveau 4 indique
        //l'enregistrement d'une d�pendance.
        //Ce message requiert l'utilisation de :
        //        - r�f�rence de l'id�e de destination.
        VERBEUX_IDEES_AUTOMATIQUES,
        //Ce message de niveau 2 indique
        //le d�but de la passe d'enregistrement des id�es
        //automatiques.
        //Ce message ne requiert aucun parametre.
        VERBEUX_IDEES_MANQUANTE_DEFAUT,
        //Ce message de niveau 3 indique
        //l'enregistrement de l'id�e manquante par d�faut.
        //Ce message ne requiert aucun parametre.
        VERBEUX_IDEES_MANQUANTE_EXPLICITE,
        //Ce message de niveau 3 indique
        //l'enregistrement de l'id�e manquante d'indice
        //explicitement donn�.
        //Ce message requiert l'utilisation de :
        //        - l'indice de l'id�e manquante.
        VERBEUX_IDEES_GENERIQUE_DEFAUT,
        //Ce message de niveau 3 indique
        //l'enregistrement de l'id�e g�n�rique par d�faut.
        //Ce message ne requiert aucun parametre.
        VERBEUX_IDEES_GENERIQUE_EXPLICITE,
        //Ce message de niveau 3 indique
        //l'enregistrement de l'id�e g�n�rique de
        //r�f�rence parente explicitement donn�.
        //Ce message requiert l'utilisation de :
        //        - la r�f�rence de l'id�e parente.
        VERBEUX_PLAN,
        //Ce message de niveau 1 indique
        //le d�but de la passe de calcul du plan.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_GRAPHE,
        //Ce message de niveau 2 indique
        //le d�but de la passe de calcul du graphe
        //de d�pendances.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_GRAPHE_VIDE,
        //Ce message de niveau 3 indique
        //le nombre d'id�es utilis�es pour le calcul
        //du plan est nul !
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_GRAPHE_IDEES,
        //Ce message de niveau 3 indique
        //le nombre d'id�es utilis�es pour le calcul
        //du plan.
        //Ce message requiert l'utilisation de :
        //        - le nombre d'id�es utilis�es.
        VERBEUX_PLAN_GRAPHE_DEPENDANCES,
        //Ce message de niveau 3 indique
        //le nombre de d�pendances utilis�es pour le
        //calcul du plan.
        //Ce message requiert l'utilisation de :
        //        - le nombre de d�pendances utilis�es ;
        //        - le nombre de d�pendances inutiles.
        VERBEUX_PLAN_RACINE,
        //Ce message de niveau 2 indique
        //le d�but de la passe de calcul de la racine.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_ATTEIGNABLES,
        //Ce message de niveau 2 indique
        //le d�but de la passe de calcul de l'atteignabilit�.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_CALCUL,
        //Ce message de niveau 2 indique
        //le d�but de la passe de calcul du plan proprement dit.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_TRANSFORMATION,
        //Ce message de niveau 2 indique
        //le d�but de la passe de transformation du plan calcul�.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_TRANSFORMATION_IDEE,
        //Ce message de niveau 3 indique
        //la transformation d'une id�e.
        //Ce message requiert l'utilisation de :
        //        - la r�f�rence de l'id�e transform�e.
        VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE,
        //Ce message de niveau 3 indique
        //la transformation d'une id�e de r�f�rence vide.
        //Ce message ne requiert aucun parametre.
        VERBEUX_PLAN_TRANSFORMATION_REFERENCE,
        //Ce message de niveau 4 indique
        //la compl�tion d'une r�f�rence.
        //Ce message requiert l'utilisation de :
        //        - la r�f�rence de la destination de la r�f�rence.
        VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE,
        //Ce message de niveau 4 indique
        //la compl�tion d'une r�f�rence dont la destination a une
        //r�f�rence vide.
        //Ce message requiert l'utilisation de :
        //        - la r�f�rence de la destination de la r�f�rence.
        VERBEUX_SORTIE,
        //Ce message de niveau 1 indique
        //le d�but de la passe de g�n�ration du document.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION,
        //Ce message de niveau 2 indique
        //le d�but de la passe de g�n�ration du texte du document.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_ENTETE,
        //Ce message de niveau 3 indique
        //la g�n�ration de l'ent�te du document.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_IDEE,
        //Ce message de niveau 3 indique
        //la g�n�ration d'une id�e du document.
        //Ce message requiert l'utilisation de :
        //        - la r�f�rence de l'id�e.
        VERBEUX_SORTIE_GENERATION_IDEEVIDE,
        //Ce message de niveau 3 indique
        //la g�n�ration d'une id�e de r�f�rence vide.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_REFIRR,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une r�f�rence irr�ductible.
        //Ce message requiert l'utilisation de :
        //        - r�f�rence de l'id�e destination.
        VERBEUX_SORTIE_GENERATION_REFIRRVIDE,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une r�f�rence irr�ductible
        //vers une id�e de r�f�rence vide.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_DEPREF,
        //Ce message de niveau 4 indique
        //la g�n�ration de la r�f�rence provenant
        //d'une d�pendance.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_REFRED,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une r�f�rence r�ductible.
        //Ce message requiert l'utilisation de :
        //        - r�f�rence de l'id�e destination.
        VERBEUX_SORTIE_GENERATION_REFREDVIDE,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une r�f�rence irr�ductible
        //vers une id�e de r�f�rence vide.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_EXTREF,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une r�f�rence ext�rieure
        //Ce message requiert l'utilisation de :
        //        - l'indice de la r�f�rence ext�rieure.
        VERBEUX_SORTIE_GENERATION_EXTREFS,
        //Ce message de niveau 4 indique
        //la g�n�ration d'une liste de r�f�rences
        //ext�rieures.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_GENERATION_PIED,
        //Ce message de niveau 3 indique
        //la g�n�ration du pied du document.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_PRODUCTION,
        //Ce message de niveau 2 indique
        //le d�but de la passe de production du document sur la
        //sortie.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_PRODUCTION_DOCUMENT,
        //Ce message de niveau 3 indique
        //la production du document sur la sortie.
        //Ce message ne requiert aucun parametre.
        VERBEUX_SORTIE_PRODUCTION_STATS
        //Ce message de niveau 3 indique
        //la production des statistiques internes du plan.
        //Ce message ne requiert aucun parametre.
} CONTENEUR_SCALAIRE(Verbeux);

Resultat verbeux_verbeux(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Verbeux) verbeux, ...);
/* Affiche un message indiquant l'op�ration effectu�e par le compilateur
 * sur la sortie d'erreur. Ce message servant � indiquer qu'une op�ration
 * interne a �t� effectu�e, aucune localisation n'est fournie.
 * Le niveau d'affichage est automatiquemen g�r�.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * Attention ! Les param�tres surnum�raires sont les param�tres
 * du message correspondant au probl�me rencontr�. Le nombre et le type
 * de ces arguments doivent absolument correspondre avec leurs sp�cifications,
 * sous peine de comportement ind�fini.
 */

#endif
