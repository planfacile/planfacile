/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "verbeux.h"

static Resultat verbeux_niveau(TRAVAIL_SCALAIRE(Verbeux) verbeux, REFERENCE_SCALAIRE(Entier) niveau)
{
        /* Renvoie le niveau d'un message verbeux.
         * Renvoie RESULTAT_ERREUR_DOMAINE si le message est incorrect.
         */
        switch(verbeux)
        {
                case T_S_(VERBEUX_ANALYSE):
                case T_S_(VERBEUX_RETOUCHES):
                case T_S_(VERBEUX_OPTIONS):
                case T_S_(VERBEUX_MACROS):
                case T_S_(VERBEUX_VERIFICATION):
                case T_S_(VERBEUX_STYLES):
                case T_S_(VERBEUX_IDEES):
                case T_S_(VERBEUX_PLAN):
                case T_S_(VERBEUX_SORTIE):
                        T_R_(niveau)=T_S_(1);
                        break;
                case T_S_(VERBEUX_OPTIONS_RECHERCHE):
                case T_S_(VERBEUX_OPTIONS_REDUCTION):
                case T_S_(VERBEUX_STYLES_SECTION):
                case T_S_(VERBEUX_STYLES_AUTRES):
                case T_S_(VERBEUX_IDEES_PRESENTES):
                case T_S_(VERBEUX_IDEES_AUTOMATIQUES):
                case T_S_(VERBEUX_PLAN_GRAPHE):
                case T_S_(VERBEUX_PLAN_RACINE):
                case T_S_(VERBEUX_PLAN_ATTEIGNABLES):
                case T_S_(VERBEUX_PLAN_CALCUL):
                case T_S_(VERBEUX_PLAN_TRANSFORMATION):
                case T_S_(VERBEUX_SORTIE_GENERATION):
                case T_S_(VERBEUX_SORTIE_PRODUCTION):
                        T_R_(niveau)=T_S_(2);
                        break;
                case T_S_(VERBEUX_ANALYSE_SOURCE):
                case T_S_(VERBEUX_RETOUCHES_MACRO_NORMALE):
                case T_S_(VERBEUX_RETOUCHES_MACRO_BOUCLE):
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_PREMIERE):
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_NOUVELLE):
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_FIN):
                case T_S_(VERBEUX_MACROS_NORMALE):
                case T_S_(VERBEUX_MACROS_BOUCLE):
                case T_S_(VERBEUX_VERIFICATION_ERREUR):
                case T_S_(VERBEUX_VERIFICATION_AVERTISSEMENT):
                case T_S_(VERBEUX_STYLES_SECTION_DEFAUT):
                case T_S_(VERBEUX_STYLES_SECTION_EXPLICITE):
                case T_S_(VERBEUX_STYLES_REFERENCE_DEFAUT):
                case T_S_(VERBEUX_STYLES_REFERENCE_EXPLICITE):
                case T_S_(VERBEUX_STYLES_MESSAGE):
                case T_S_(VERBEUX_STYLES_ENTETE):
                case T_S_(VERBEUX_STYLES_PIED):
                case T_S_(VERBEUX_STYLES_RACINE):
                case T_S_(VERBEUX_IDEES_IDEE):
                case T_S_(VERBEUX_IDEES_MANQUANTE_DEFAUT):
                case T_S_(VERBEUX_IDEES_MANQUANTE_EXPLICITE):
                case T_S_(VERBEUX_IDEES_GENERIQUE_DEFAUT):
                case T_S_(VERBEUX_IDEES_GENERIQUE_EXPLICITE):
                case T_S_(VERBEUX_PLAN_GRAPHE_VIDE):
                case T_S_(VERBEUX_PLAN_GRAPHE_IDEES):
                case T_S_(VERBEUX_PLAN_GRAPHE_DEPENDANCES):
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEE):
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE):
                case T_S_(VERBEUX_SORTIE_GENERATION_ENTETE):
                case T_S_(VERBEUX_SORTIE_GENERATION_IDEE):
                case T_S_(VERBEUX_SORTIE_GENERATION_IDEEVIDE):
                case T_S_(VERBEUX_SORTIE_GENERATION_PIED):
                case T_S_(VERBEUX_SORTIE_PRODUCTION_DOCUMENT):
                case T_S_(VERBEUX_SORTIE_PRODUCTION_STATS):
                        T_R_(niveau)=T_S_(3);
                        break;
                case T_S_(VERBEUX_ANALYSE_FINSOURCE):
                case T_S_(VERBEUX_ANALYSE_INCLUSION):
                case T_S_(VERBEUX_ANALYSE_FININCLUSION):
                case T_S_(VERBEUX_RETOUCHES_PARAMETRE):
                case T_S_(VERBEUX_RETOUCHES_INCLUDE):
                case T_S_(VERBEUX_RETOUCHES_STANDARD):
                case T_S_(VERBEUX_OPTIONS_ENREGISTREMENT):
                case T_S_(VERBEUX_OPTIONS_COMMENTAIRE):
                case T_S_(VERBEUX_OPTIONS_MACRO):
                case T_S_(VERBEUX_OPTIONS_REDUCTION_OPTIONS):
                case T_S_(VERBEUX_MACROS_PARAMETRE):
                case T_S_(VERBEUX_MACROS_ITERATION):
                case T_S_(VERBEUX_IDEES_DEPENDANCE):
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCE):
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE):
                case T_S_(VERBEUX_SORTIE_GENERATION_REFIRR):
                case T_S_(VERBEUX_SORTIE_GENERATION_REFIRRVIDE):
                case T_S_(VERBEUX_SORTIE_GENERATION_DEPREF):
                case T_S_(VERBEUX_SORTIE_GENERATION_REFRED):
                case T_S_(VERBEUX_SORTIE_GENERATION_REFREDVIDE):
                case T_S_(VERBEUX_SORTIE_GENERATION_EXTREF):
                case T_S_(VERBEUX_SORTIE_GENERATION_EXTREFS):
                        T_R_(niveau)=T_S_(4);
                        break;
                case T_S_(VERBEUX_RETOUCHES_DEFRAGMENTATION):
                case T_S_(VERBEUX_VERIFICATION_DEFRAGMENTATION):
                        T_R_(niveau)=T_S_(5);
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

static Resultat verbeux_message(TRAVAIL_SCALAIRE(Verbeux) verbeux, REFERENCE_SCALAIRE(Message) message)
{
        /* Renvoie le message associ� � un message d�termin�.
         * Si le probl�me n'est pas r�pertori�, RESULTAT_ERREUR_DOMAINE est renvoy�.
         */
        switch(verbeux)
        {
                case T_S_(VERBEUX_ANALYSE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_ANALYSE);
                        break;
                case T_S_(VERBEUX_ANALYSE_SOURCE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_ANALYSE_SOURCE);
                        break;
                case T_S_(VERBEUX_ANALYSE_FINSOURCE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_ANALYSE_FINSOURCE);
                        break;
                case T_S_(VERBEUX_ANALYSE_INCLUSION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_ANALYSE_INCLUSION);
                        break;
                case T_S_(VERBEUX_ANALYSE_FININCLUSION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_ANALYSE_FININCLUSION);
                        break;
                case T_S_(VERBEUX_RETOUCHES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES);
                        break;
                case T_S_(VERBEUX_RETOUCHES_PARAMETRE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_PARAMETRE);
                        break;
                case T_S_(VERBEUX_RETOUCHES_MACRO_NORMALE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_MACRO_NORMALE);
                        break;
                case T_S_(VERBEUX_RETOUCHES_MACRO_BOUCLE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_MACRO_BOUCLE);
                        break;
                case T_S_(VERBEUX_RETOUCHES_DEFRAGMENTATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_DEFRAGMENTATION);
                        break;
                case T_S_(VERBEUX_RETOUCHES_INCLUDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_INCLUDE);
                        break;
                case T_S_(VERBEUX_RETOUCHES_STANDARD):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_RETOUCHES_STANDARD);
                        break;
                case T_S_(VERBEUX_OPTIONS):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS);
                        break;
                case T_S_(VERBEUX_OPTIONS_RECHERCHE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_RECHERCHE);
                        break;
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_PREMIERE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_RECHERCHE_PREMIERE);
                        break;
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_NOUVELLE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_RECHERCHE_NOUVELLE);
                        break;
                case T_S_(VERBEUX_OPTIONS_RECHERCHE_FIN):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_RECHERCHE_FIN);
                        break;
                case T_S_(VERBEUX_OPTIONS_ENREGISTREMENT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_ENREGISTREMENT);
                        break;
                case T_S_(VERBEUX_OPTIONS_COMMENTAIRE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_COMMENTAIRE);
                        break;
                case T_S_(VERBEUX_OPTIONS_MACRO):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_MACRO);
                        break;
                case T_S_(VERBEUX_OPTIONS_REDUCTION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_REDUCTION);
                        break;
                case T_S_(VERBEUX_OPTIONS_REDUCTION_OPTIONS):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_OPTIONS_OPTIONS);
                        break;
                case T_S_(VERBEUX_MACROS):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_MACROS);
                        break;
                case T_S_(VERBEUX_MACROS_NORMALE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_MACROS_NORMALE);
                        break;
                case T_S_(VERBEUX_MACROS_BOUCLE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_MACROS_BOUCLE);
                        break;
                case T_S_(VERBEUX_MACROS_PARAMETRE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_MACROS_PARAMETRE);
                        break;
                case T_S_(VERBEUX_MACROS_ITERATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_MACROS_ITERATION);
                        break;
                case T_S_(VERBEUX_VERIFICATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_VERIFICATION);
                        break;
                case T_S_(VERBEUX_VERIFICATION_DEFRAGMENTATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_VERIFICATION_DEFRAGMENTATION);
                        break;
                case T_S_(VERBEUX_VERIFICATION_ERREUR):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_VERIFICATION_ERREUR);
                        break;
                case T_S_(VERBEUX_VERIFICATION_AVERTISSEMENT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_VERIFICATION_AVERTISSEMENT);
                        break;
                case T_S_(VERBEUX_STYLES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES);
                        break;
                case T_S_(VERBEUX_STYLES_SECTION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_SECTION);
                        break;
                case T_S_(VERBEUX_STYLES_SECTION_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_SECTION_DEFAUT);
                        break;
                case T_S_(VERBEUX_STYLES_SECTION_EXPLICITE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_SECTION_EXPLICITE);
                        break;
                case T_S_(VERBEUX_STYLES_AUTRES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_AUTRES);
                        break;
                case T_S_(VERBEUX_STYLES_REFERENCE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_REFERENCE_DEFAUT);
                        break;
                case T_S_(VERBEUX_STYLES_REFERENCE_EXPLICITE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_REFERENCE_EXPLICITE);
                        break;
                case T_S_(VERBEUX_STYLES_MESSAGE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_MESSAGE);
                        break;
                case T_S_(VERBEUX_STYLES_ENTETE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_ENTETE);
                        break;
                case T_S_(VERBEUX_STYLES_PIED):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_PIED);
                        break;
                case T_S_(VERBEUX_STYLES_RACINE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_STYLES_RACINE);
                        break;
                case T_S_(VERBEUX_IDEES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES);
                        break;
                case T_S_(VERBEUX_IDEES_PRESENTES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_PRESENTES);
                        break;
                case T_S_(VERBEUX_IDEES_IDEE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_IDEE);
                        break;
                case T_S_(VERBEUX_IDEES_DEPENDANCE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_DEPENDANCE);
                        break;
                case T_S_(VERBEUX_IDEES_AUTOMATIQUES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_AUTOMATIQUES);
                        break;
                case T_S_(VERBEUX_IDEES_MANQUANTE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_MANQUANTE_DEFAUT);
                        break;
                case T_S_(VERBEUX_IDEES_MANQUANTE_EXPLICITE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_MANQUANTE_EXPLICITE);
                        break;
                case T_S_(VERBEUX_IDEES_GENERIQUE_DEFAUT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_GENERIQUE_DEFAUT);
                        break;
                case T_S_(VERBEUX_IDEES_GENERIQUE_EXPLICITE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_IDEES_GENERIQUE_EXPLICITE);
                        break;
                case T_S_(VERBEUX_PLAN):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN);
                        break;
                case T_S_(VERBEUX_PLAN_GRAPHE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_GRAPHE);
                        break;
                case T_S_(VERBEUX_PLAN_GRAPHE_VIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_GRAPHE_VIDE);
                        break;
                case T_S_(VERBEUX_PLAN_GRAPHE_IDEES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_GRAPHE_IDEES);
                        break;
                case T_S_(VERBEUX_PLAN_GRAPHE_DEPENDANCES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_GRAPHE_DEPENDANCES);
                        break;
                case T_S_(VERBEUX_PLAN_RACINE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_RACINE);
                        break;
                case T_S_(VERBEUX_PLAN_ATTEIGNABLES):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_ATTEIGNABLES);
                        break;
                case T_S_(VERBEUX_PLAN_CALCUL):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_CALCUL);
                        break;
                case T_S_(VERBEUX_PLAN_TRANSFORMATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_TRANSFORMATION);
                        break;
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_TRANSFORMATION_IDEE);
                        break;
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE);
                        break;
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_TRANSFORMATION_REFERENCE);
                        break;
                case T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE);
                        break;
                case T_S_(VERBEUX_SORTIE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_ENTETE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_ENTETE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_IDEE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_IDEE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_IDEEVIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_IDEEVIDE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_REFIRR):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_REFIRR);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_REFIRRVIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_REFIRRVIDE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_DEPREF):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_DEPREF);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_REFRED):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_REFRED);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_REFREDVIDE):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_REFREDVIDE);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_EXTREF):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_EXTREF);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_EXTREFS):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_EXTREFS);
                        break;
                case T_S_(VERBEUX_SORTIE_GENERATION_PIED):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_GENERATION_PIED);
                        break;
                case T_S_(VERBEUX_SORTIE_PRODUCTION):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_PRODUCTION);
                        break;
                case T_S_(VERBEUX_SORTIE_PRODUCTION_DOCUMENT):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_PRODUCTION_DOCUMENT);
                        break;
                case T_S_(VERBEUX_SORTIE_PRODUCTION_STATS):
                        T_R_(message)=T_S_(MESSAGE_VERBEUX_SORTIE_PRODUCTION_STATS);
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

Resultat verbeux_verbeux(TRAVAIL(Environnement) environnement, TRAVAIL_SCALAIRE(Verbeux) verbeux, ...)
{
        /* Affiche un message indiquant l'op�ration effectu�e par le compilateur
         * sur la sortie d'erreur. Ce message servant � indiquer qu'une op�ration
         * interne a �t� effectu�e, aucune localisation n'est fournie.
         * Le niveau d'affichage est automatiquemen g�r�.
         * Renvoie RESULTAT_ERREUR si environnement est null.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * Attention ! Les param�tres surnum�raires sont les param�tres
         * du message correspondant au probl�me rencontr�. Le nombre et le type
         * de ces arguments doivent absolument correspondre avec leurs sp�cifications,
         * sous peine de comportement ind�fini.
         */
        TRAVAIL_SCALAIRE(Entier) niveau;
        TRAVAIL_SCALAIRE(Entier) niveaureel;
        TRAVAIL(Options) options;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_options(environnement,R_T(options)));
        SECURISE(options_lecture_verbeux(options,R_T_(niveaureel)));
        SECURISE(verbeux_niveau(verbeux,R_T_(niveau)));
        if(niveaureel>=niveau)
        {
                TRAVAIL_SCALAIRE(DescripteurFichier) erreur;
                STOCKAGE(MessageParametres) parametresentete;
                TRAVAIL_SCALAIRE(Message) message;
                TRAVAIL_SCALAIRE(Chaine) format;
                STOCKAGE(MessageParametres) parametres;
                TRAVAIL_SCALAIRE(va_list) ap;
                SECURISE(environnement_lecture_erreur(environnement,R_T_(erreur)));
                SECURISE(messageparametres_initialisation(T_S(parametresentete)));
                SECURISE(messageparametres_ajout_entier(T_S(parametresentete),niveau));
                SECURISE(messages_message(environnement,erreur,T_S_(MESSAGE_VERBEUX),T_S(parametresentete)));
                SECURISE(messageparametres_destruction(T_S(parametresentete)));
                SECURISE(messageparametres_initialisation(T_S(parametresentete)));
                SECURISE(messages_message(environnement,erreur,T_S_(MESSAGE_DOUBLE_POINTS),T_S(parametresentete)));
                SECURISE(verbeux_message(verbeux,R_T_(message)));
                SECURISE(messages_parametres(message,R_T_(format)));
                SECURISE(messageparametres_initialisation(T_S(parametres)));
                va_start(ap,verbeux);
                for( ; S_C_((COREFERENCE_SCALAIRE(Caractere))(format))!='\0' ; format++)
                        switch(S_C_((COREFERENCE_SCALAIRE(Caractere))(format)))
                        {
                                case T_S_('d'):
                                case T_S_('i'):
                                case T_S_('o'):
                                case T_S_('u'):
                                case T_S_('x'):
                                case T_S_('X'):
                                        SECURISE(messageparametres_ajout_entier(T_S(parametres),va_arg(ap,CONTENEUR_SCALAIRE(Entier))));
                                        break;
                                case T_S_('f'):
                                case T_S_('F'):
                                case T_S_('e'):
                                case T_S_('E'):
                                case T_S_('g'):
                                case T_S_('G'):
                                        SECURISE(messageparametres_ajout_reel(T_S(parametres),va_arg(ap,CONTENEUR_SCALAIRE(Reel))));
                                        break;
                                case T_S_('c'):
                                        SECURISE(messageparametres_ajout_caractere(T_S(parametres),(STOCKAGE_SCALAIRE(Caractere))(va_arg(ap,CONTENEUR_SCALAIRE(Entier)))));
                                        break;
                                case T_S_('s'):
                                        SECURISE(messageparametres_ajout_chaine(T_S(parametres),va_arg(ap,CONTENEUR_SCALAIRE(Chaine))));
                                        break;
                                default:
                                        SECURISE(messageparametres_ajout_pointeur(T_S(parametres),va_arg(ap,CONTENEUR_SCALAIRE(Pointeur))));
                                        break;
                        }
                va_end(ap);
                SECURISE(messages_message(environnement,erreur,message,T_S(parametres)));
                SECURISE(messageparametres_destruction(T_S(parametres)));
                SECURISE(messages_message(environnement,erreur,T_S_(MESSAGE_POINT),T_S(parametresentete)));
                SECURISE(messageparametres_destruction(T_S(parametresentete)));
        }
        return RESULTAT_OK;
}

