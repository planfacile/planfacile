/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "analyse.h"

extern STOCKAGE_SCALAIRE(DescripteurFichier) yyin;
extern TRAVAIL_SCALAIRE(int) yyparse();
extern Resultat analyseur_ouverture_fichier(REFERENCE_SCALAIRE(Booleen) finanalyse);

Resultat analyse_analyseur(TRAVAIL(General) general)
{
        /* Analyse les fichiers sources de planfacile.
         * Renvoie une valeur autre que RESULTAT_OK en
         * cas d'erreur. Si un probl�me est d�t�ct�,
         * la fonction ne revient pas. La fonction
         * prend ses param�tres directement dans la
         * variable general.
         */
        STOCKAGE_SCALAIRE(Booleen) vide;
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement), T_S_(VERBEUX_ANALYSE)));
        SECURISE(environnement_initialisation_pilenomfichier(CHAMP_TRAVAIL(general, environnement)));
        SECURISE(pilenommacro_initialisation(CHAMP_TRAVAIL(general, pilenommacro)));
        SECURISE(pilechaine_initialisation(CHAMP_TRAVAIL(general, piletexte)));
        SECURISE(pileentier_initialisation(CHAMP_TRAVAIL(general, pileparametre)));
        SECURISE(pilelocalisationfichier_initialisation(CHAMP_TRAVAIL(general, pilelocalisation)));
        SECURISE(analyseur_ouverture_fichier(R_S_(vide)));
        if(T_S_(vide)==T_S_(FAUX))
        {
                SECURISE(localisationfichier_lecture_descripteur(CHAMP_TRAVAIL(general, localisation),R_S_(yyin)));
                SECURISE((Resultat)(yyparse()));
        }
        else
        {
                STOCKAGE(Flux) flux;
                SECURISE(flux_initialisation(T_S(flux)));
                SECURISE(flux_definition_type(T_S(flux),T_S_(FLUX_PRINCIPAL)));
                SECURISE(flux_copie(T_S(flux),CHAMP_TRAVAIL(general, principal)));
                SECURISE(flux_destruction(T_S(flux)));
        }
        SECURISE(pilenommacro_destruction(CHAMP_TRAVAIL(general, pilenommacro)));
        SECURISE(pilechaine_destruction(CHAMP_TRAVAIL(general, piletexte)));
        SECURISE(pileentier_destruction(CHAMP_TRAVAIL(general, pileparametre)));
        SECURISE(pilelocalisationfichier_destruction(CHAMP_TRAVAIL(general, pilelocalisation)));
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(general, localisation)));
        SECURISE(filenomfichier_destruction(CHAMP_TRAVAIL(general, sources)));
        SECURISE(environnement_destruction_pilenomfichier(CHAMP_TRAVAIL(general, environnement)));
        return RESULTAT_OK;
}

