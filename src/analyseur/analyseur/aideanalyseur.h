/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __AIDEANALYSEUR__
#define __AIDEANALYSEUR__

#include <src/global/global.h>
#include <src/donnees/general/general.h>

/* Quelques fonctions utiles pour la construction des flux
 */
 
Resultat analyseur_detruiredernierelocalisation(TRAVAIL(PileLocalisationFichier) pilelocalisation);
/* Permet de detruire les localisations des tokens ne correspondant pas à une commande,
 * comme #end, #{ et #}, ou correspondant à des commandes sans localisation, comme
 * #txt, #case et #other.
 * Renvoie RESULTAT_ERREUR si pilelocalisation vaut NULL.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat analyseur_verificationcopielocalisation(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) copielocalisation);
/* Sert à déterminer si les localisations de fichier doivent être affichées.
 * Renvoie RESULTAT_ERREUR si environnement vaut NULL.
 */

Resultat analyseur_verificationcheminabsolu(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) cheminabsolu);
/* Sert à déterminer si le chemin à afficher pour les problèmes lors des inclusions de fichiers
 * est le chemin absolu (depuis la racine du FS) ou le chemin donné par l'utilisateur.
 * Renvoie RESULTAT_ERREUR si environnement vaut NULL.
 */

Resultat analyseur_modificationerreursyntaxe(TRAVAIL(General) general, TRAVAIL_SCALAIRE(Chaine) chainetokenerreur);
/* Fait pointer general->erreursyntaxe sur une copie de la chaine
 * passée en paramètre, pour connaitre la derniere chaine lue si
 * une erreur de syntaxe est détectée.
 * Libère la chaine précédement allouée si besoin est.
 * Renvoie RESULTAT_ERREUR si general vaut NULL.
 * Renvoie RESULTAT_ERREUR si chainetokenerreur vaut NULL.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation echoue.
 */

#endif//__AIDEANALYSEUR__
