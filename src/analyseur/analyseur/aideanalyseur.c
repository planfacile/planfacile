/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#include <src/analyseur/analyseur/aideanalyseur.h>


Resultat analyseur_detruiredernierelocalisation(TRAVAIL(PileLocalisationFichier) pilelocalisation)
{
        /* Permet de detruire les localisations des tokens ne correspondant pas à une commande,
         * comme #end, #{ et #}, ou correspondant à des commandes sans localisation, comme
         * #txt, #case et #other.
         * Renvoie RESULTAT_ERREUR si pilelocalisation vaut NULL.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(LocalisationFichier) localisationobsolete;
        TRAVAIL_SCALAIRE(Booleen) pile_vide;
        ASSERTION(S_T(pilelocalisation)!=NULL, RESULTAT_ERREUR);

        SECURISE(pilelocalisationfichier_vide(pilelocalisation, R_T_(pile_vide)));
        ASSERTION(pile_vide!=T_S_(VRAI), RESULTAT_ERREUR);

        SECURISE(pilelocalisationfichier_retrait(pilelocalisation, C_S(localisationobsolete)));
        SECURISE(localisationfichier_destruction(T_S(localisationobsolete)));
        return RESULTAT_OK;
}

Resultat analyseur_verificationcopielocalisation(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) copielocalisation)
{
        /* Sert à déterminer si les localisations de fichier doivent être affichées.
         * Renvoie RESULTAT_ERREUR si environnement vaut NULL.
         */
        TRAVAIL(Options) options;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_options(environnement, R_T(options)));
        SECURISE(options_lecture_copielocalisation(options, copielocalisation));
        return RESULTAT_OK;
}

Resultat analyseur_verificationcheminabsolu(TRAVAIL(Environnement) environnement, REFERENCE_SCALAIRE(Booleen) cheminabsolu)
{
        /* Sert à déterminer si le chemin à afficher pour les problèmes lors des inclusions de fichiers
         * est le chemin absolu (depuis la racine du FS) ou le chemin donné par l'utilisateur.
         * Renvoie RESULTAT_ERREUR si environnement vaut NULL.
         */
        TRAVAIL(Options) options;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_options(environnement, R_T(options)));
        SECURISE(options_lecture_localisationabsolue(options, cheminabsolu));
        return RESULTAT_OK;
}

Resultat analyseur_modificationerreursyntaxe(TRAVAIL(General) general, TRAVAIL_SCALAIRE(Chaine) chainetokenerreur)
{
        /* Fait pointer general->erreursyntaxe sur une copie de la chaine
         * passée en paramètre, pour connaitre la derniere chaine lue si
         * une erreur de syntaxe est détectée.
         * Libère la chaine précédement allouée si besoin est.
         * Renvoie RESULTAT_ERREUR si general vaut NULL.
         * Renvoie RESULTAT_ERREUR si chainetokenerreur vaut NULL.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si l'allocation echoue.
         */
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T_(chainetokenerreur)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(general, erreursyntaxe)!=NULL)
                free(CHAMP(general, erreursyntaxe));

        ASSERTION((CHAMP(general, erreursyntaxe)=DUP_CAST(Chaine, chainetokenerreur))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        return RESULTAT_OK;
}
