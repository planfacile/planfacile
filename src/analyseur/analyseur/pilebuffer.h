/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILEBUFFER__
#define __PILEBUFFER__
/* Fichier à inclure dans un fichier *.l destiné à Flex.
 */


/* Pile de buffers pour les changements d'entrée
 */
typedef struct pilebuffer CONTENEUR(PileBuffer);

struct pilebuffer
{
        STOCKAGE_SCALAIRE(YY_BUFFER_STATE) buffer;
        STOCKAGE_SCALAIRE(Entier) numeroligne;
        STOCKAGE(PileBuffer) suivant;
};

static STOCKAGE(PileBuffer) pile=NULL;

Resultat buffer_empile()
{
        STOCKAGE(PileBuffer) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileBuffer))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, buffer)=YY_CURRENT_BUFFER;
        CHAMP_STOCKAGE(nouveau, numeroligne)=(STOCKAGE_SCALAIRE(Entier))(yylineno);
        CHAMP_STOCKAGE(nouveau, suivant)=pile;
        pile=nouveau;
        return RESULTAT_OK;
}

Resultat buffer_depile()
{
        STOCKAGE(PileBuffer) ancien;
        ASSERTION(pile!=NULL, RESULTAT_ERREUR);

        ancien=pile;
        yy_delete_buffer(T_S_(YY_CURRENT_BUFFER));
        yylineno=(int)(CHAMP_STOCKAGE(ancien, numeroligne));
        yy_switch_to_buffer(T_S_(CHAMP_STOCKAGE(ancien, buffer)));
        pile=CHAMP_STOCKAGE(ancien, suivant);
        free(ancien);
        return RESULTAT_OK;
}

#endif // __PILEBUFFER__
