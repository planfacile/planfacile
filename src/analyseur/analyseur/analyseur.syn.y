/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *  Copyright (C) 2005-2013 Sylvain Plantef�ve
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

%{

/* Bikous d'includes ici didon!
 */
#include <signal.h>

#include <src/global/global.h>
#include <src/donnees/general/general.h>

#include <src/analyseur/analyseur/aideanalyseur.h>
#include <src/analyseur/analyseur/macrosanalyseur.h>

#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commande.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/donnees/commandes/commandeoptions.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandewarning.h>
#include <src/donnees/commandes/commandeerror.h>
#include <src/donnees/commandes/commandestart.h>
#include <src/donnees/commandes/commandehead.h>
#include <src/donnees/commandes/commandefoot.h>
#include <src/donnees/commandes/commandesection.h>
#include <src/donnees/commandes/commandereference.h>
#include <src/donnees/commandes/commandemessage.h>
#include <src/donnees/commandes/commandetitle.h>
#include <src/donnees/commandes/commanderef.h>
#include <src/donnees/commandes/commandetxt.h>
#include <src/donnees/commandes/commandesec.h>
#include <src/donnees/commandes/commandemesg.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/commandemissing.h>
#include <src/donnees/commandes/commandegeneric.h>
#include <src/donnees/commandes/commandeindex.h>
#include <src/donnees/commandes/commandeextref.h>
#include <src/donnees/commandes/commandeextrefs.h>
#include <src/donnees/commandes/commandedep.h>
#include <src/donnees/commandes/commandedepref.h>
#include <src/donnees/commandes/commandedepref.h>
#include <src/donnees/commandes/commandecommentaire.h>
#include <src/donnees/commandes/commandeechappement.h>
#include <src/donnees/commandes/commandetexte.h>
#include <src/donnees/commandes/commandemacro.h>
#include <src/donnees/commandes/commandeparametre.h>


/* Debuguons un peu...
 */
#define YYDEBUG        0
int yydebug=0;


/* Simplifions nous un peu la vie...
 */
#define FONCTION_LOCALISATION_COMMANDE(localisation, type, objet)        commande##type##_definition_localisationfichier(objet, localisation)
#define LOCALISATION_COMMANDE(type, objet) \
        LOCALISATION_PILE_FONCTION(WRAP_SECURISE, FONCTION_LOCALISATION_COMMANDE, type, objet)

#define DEFINIR_COMMANDE(type, entree)                                              \
        ({                                                                          \
                STOCKAGE(Commande) commande;                                        \
                SECURISE(commande_initialisation(T_S(commande)));                   \
                SECURISE(commande_definition_##type (T_S(commande), T_S(entree)));  \
                SECURISE(commande##type##_destruction(T_S(entree)));                \
                commande;                                                           \
        })
#define DEFINIR_COMMANDE_ECHAPPEMENT(caractere)                                                                 \
        ({                                                                                                      \
                STOCKAGE(CommandeEchappement) commandeechappement;                                              \
                SECURISE(commandeechappement_initialisation(T_S(commandeechappement)));                         \
                SECURISE(commandeechappement_definition_caractere(T_S(commandeechappement), T_S_(caractere)));  \
                LOCALISATION_COMMANDE(echappement, T_S(commandeechappement));                                   \
                commandeechappement;                                                                            \
        })


/* Elements g�n�r�s par flex et bison
 */
extern FILE *yyin;
extern int yylex();

void yyerror();


extern STOCKAGE(General) general;
STOCKAGE_SCALAIRE(Entier) niveauimbricationmacro=0;
/* Sert � rep�rer le niveau d'imbrication des appels
 * de macros, lors de leur enregistrement dans yylex().
 */

%}

%union {
        STOCKAGE_SCALAIRE(Chaine) _Chaine;
        STOCKAGE_SCALAIRE(Entier) _Entier;
        STOCKAGE(struct flux) _Flux;
        STOCKAGE(struct commande) _Commande;
        STOCKAGE(struct commandeoption) _CommandeOption;
        STOCKAGE(struct commandeoptions) _CommandeOptions;
        STOCKAGE(struct commandeoptionsclauses) _CommandeOptionsClauses;
        STOCKAGE(struct commandeoptionsclause) _CommandeOptionsClause;
        STOCKAGE(struct commandedefine) _CommandeDefine;
        STOCKAGE(struct commandewarning) _CommandeWarning;
        STOCKAGE(struct commandeerror) _CommandeError;
        STOCKAGE(struct commandestart) _CommandeStart;
        STOCKAGE(struct commandehead) _CommandeHead;
        STOCKAGE(struct commandefoot) _CommandeFoot;
        STOCKAGE(struct commandesection) _CommandeSection;
        STOCKAGE(struct commandereference) _CommandeReference;
        STOCKAGE(struct commandemessage) _CommandeMessage;
        STOCKAGE(struct commandetitle) _CommandeTitle;
        STOCKAGE(struct commanderef) _CommandeRef;
        STOCKAGE(struct commandetxt) _CommandeTxt;
        STOCKAGE(struct commandesec) _CommandeSec;
        STOCKAGE(struct commandemesg) _CommandeMesg;
        STOCKAGE(struct commandeidea) _CommandeIdea;
        STOCKAGE(struct commandemissing) _CommandeMissing;
        STOCKAGE(struct commandegeneric) _CommandeGeneric;
        STOCKAGE(struct commandeindex) _CommandeIndex;
        STOCKAGE(struct commandeextref) _CommandeExtRef;
        STOCKAGE(struct commandeextrefs) _CommandeExtRefs;
        STOCKAGE(struct commandedep) _CommandeDep;
        STOCKAGE(struct commandedepref) _CommandeDepRef;
        STOCKAGE(struct commandecommentaire) _CommandeCommentaire;
        STOCKAGE(struct commandeechappement) _CommandeEchappement;
        STOCKAGE(struct commandetexte) _CommandeTexte;
        STOCKAGE(struct commandemacro) _CommandeMacro;
        STOCKAGE(struct commandemacroparametres) _CommandeMacroParametres;
        STOCKAGE(struct commandeparametre) _CommandeParametre;
        STOCKAGE(struct commandeinclude) _CommandeInclude;
        STOCKAGE(struct commandestandard) _CommandeStandard;
       }

%token TOKEN_TEXTE

%token TOKEN_STANDARD
%token TOKEN_INCLUDE

%token TOKEN_OPTION
%token TOKEN_OPTIONS
%token TOKEN_CASE
%token TOKEN_OTHER
%token TOKEN_END


%token TOKEN_DEFINE
%token TOKEN_PARAMETRE

%token TOKEN_MACRO


%token TOKEN_WARNING
%token TOKEN_ERROR

%token TOKEN_START
%token TOKEN_HEAD
%token TOKEN_FOOT
%token TOKEN_SECTION
%token TOKEN_REFERENCE
%token TOKEN_MESSAGE

%token TOKEN_TITLE
%token TOKEN_REF
%token TOKEN_TXT
%token TOKEN_SEC
%token TOKEN_MESG

%token TOKEN_IDEA
%token TOKEN_MISSING
%token TOKEN_GENERIC
%token TOKEN_TEXT

%token TOKEN_INDEX
%token TOKEN_EXTREF
%token TOKEN_EXTREFS

%token TOKEN_DEP
%token TOKEN_DEPREF

%token TOKEN_COMMENT

%token TOKEN_PARAM_DEBUT
%token TOKEN_PARAM_FIN

%token TOKEN_ECHAP_COMMANDE
%token TOKEN_ECHAP_PARAM_DEBUT
%token TOKEN_ECHAP_PARAM_FIN
%token TOKEN_ECHAP_RETOURLIGNE
%token TOKEN_ECHAP_ESPACE
%token TOKEN_ECHAP_TAB

/* Declaration des types.
 */

%type<_Flux>                    flux_planfacile

%type<_Flux>                    flux
%type<_Commande>                commande
%type<_CommandeOption>          commande_option
%type<_CommandeOptions>         commande_options
%type<_CommandeOptionsClauses>  liste_options
%type<_CommandeOptionsClause>   option
%type<_CommandeDefine>          commande_define
%type<_CommandeError>           commande_error
%type<_CommandeWarning>         commande_warning
%type<_CommandeStart>           commande_start
%type<_CommandeHead>            commande_head
%type<_CommandeFoot>            commande_foot
%type<_CommandeSection>         commande_section
%type<_CommandeReference>       commande_reference
%type<_CommandeMessage>         commande_message
%type<_CommandeTitle>           commande_title
%type<_CommandeRef>             commande_ref
%type<_CommandeTxt>             commande_txt
%type<_CommandeSec>             commande_sec
%type<_CommandeMesg>            commande_mesg
%type<_CommandeIdea>            commande_idea
%type<_CommandeMissing>         commande_missing
%type<_CommandeGeneric>         commande_generic
%type<_CommandeIndex>           commande_index
%type<_CommandeExtRef>          commande_extref
%type<_CommandeExtRefs>         commande_extrefs
%type<_CommandeDep>             commande_dep
%type<_CommandeDepRef>          commande_depref
%type<_CommandeCommentaire>     commande_comment
%type<_CommandeEchappement>     commande_echappement
%type<_CommandeTexte>           commande_texte
%type<_CommandeMacro>           commande_macro
%type<_CommandeMacroParametres> liste_parametre
%type<_CommandeParametre>       commande_parametre
%type<_CommandeInclude>         commande_include
%type<_CommandeStandard>        commande_standard

%type<_Chaine>                  parametre_texte
%type<_Flux>                    parametre_flux


%start flux_planfacile

%%

flux_planfacile:        flux
        {
                SECURISE(flux_definition_type(T_S($1), T_S_(FLUX_PRINCIPAL)));
                CHAMP_STOCKAGE(general, principal)=$1;
                SECURISE(DEPILE_FLUX());
                SECURISE(TEST_PILE_FLUX());
        }

flux:
        {
                /* Initialisation d'un nouveau flux
                 */
                STOCKAGE(Flux) flux;
                SECURISE(flux_initialisation(T_S(flux)));
                SECURISE(EMPILE_FLUX(T_S(flux)));
                $$=flux;
        }
|               flux commande
        {
                /* Ajout d'un commande dans le flux courant
                 */
                SECURISE(flux_ajout_commande(T_S($1), T_S($2), T_S(general)));
                SECURISE(commande_destruction(T_S($2)));
                $$=$1;
        };


commande:       commande_standard
        {
                /* commande g�r�e par l'analyseur lexical, elle n'est pr�sente ici que pour valider la grammaire
                 */
                $$=DEFINIR_COMMANDE(standard, $1);
        }
|               commande_include
        {
                /* commande g�r�e par l'analyseur lexical, elle n'est pr�sente ici que pour valider la grammaire
                 */
                $$=DEFINIR_COMMANDE(include, $1);
        }
|               commande_option
        {
                $$=DEFINIR_COMMANDE(option, $1);
        }
|               commande_options
        {
                $$=DEFINIR_COMMANDE(options, $1);
        }
|               commande_define
        {
                $$=DEFINIR_COMMANDE(define, $1);
        }
|               commande_warning
        {
                $$=DEFINIR_COMMANDE(warning, $1);
        }
|               commande_error
        {
                $$=DEFINIR_COMMANDE(error, $1);
        }
|               commande_start
        {
                $$=DEFINIR_COMMANDE(start, $1);
        }
|               commande_head
        {
                $$=DEFINIR_COMMANDE(head, $1);
        }
|               commande_foot
        {
                $$=DEFINIR_COMMANDE(foot, $1);
        }
|               commande_section
        {
                $$=DEFINIR_COMMANDE(section, $1);
        }
|               commande_reference
        {
                $$=DEFINIR_COMMANDE(reference, $1);
        }
|               commande_message
        {
                $$=DEFINIR_COMMANDE(message, $1);
        }
|               commande_title
        {
                $$=DEFINIR_COMMANDE(title, $1);
        }
|               commande_ref
        {
                $$=DEFINIR_COMMANDE(ref, $1);
        }
|               commande_txt
        {
                $$=DEFINIR_COMMANDE(txt, $1);
        }
|               commande_sec
        {
                $$=DEFINIR_COMMANDE(sec, $1);
        }
|               commande_mesg
        {
                $$=DEFINIR_COMMANDE(mesg, $1);
        }
|               commande_idea
        {
                $$=DEFINIR_COMMANDE(idea, $1);
        }
|               commande_missing
        {
                $$=DEFINIR_COMMANDE(missing, $1);
        }
|               commande_generic
        {
                $$=DEFINIR_COMMANDE(generic, $1);
        }
|               commande_index
        {
                $$=DEFINIR_COMMANDE(index, $1);
        }
|               commande_extref
        {
                $$=DEFINIR_COMMANDE(extref, $1);
        }
|               commande_extrefs
        {
                $$=DEFINIR_COMMANDE(extrefs, $1);
        }
|               commande_dep
        {
                $$=DEFINIR_COMMANDE(dep, $1);
        }
|               commande_depref
        {
                $$=DEFINIR_COMMANDE(depref, $1);
        }
|               commande_comment
        {
                $$=DEFINIR_COMMANDE(commentaire, $1);
        }
|               commande_echappement
        {
                $$=DEFINIR_COMMANDE(echappement, $1);
        }
|               commande_texte
        {
                $$=DEFINIR_COMMANDE(texte, $1);
        }
|               commande_macro
        {
                $$=DEFINIR_COMMANDE(macro, $1);
        }
|               commande_parametre
        {
                $$=DEFINIR_COMMANDE(parametre, $1);
        };

commande_include:       TOKEN_INCLUDE
        {
                /* L'inclusion est g�r�e par l'analyseur lexical
                 */
                STOCKAGE(CommandeInclude) commandeinclude;
                TRAVAIL_SCALAIRE(NomFichier) nomfichierinclude;
                SECURISE(commandeinclude_initialisation(T_S(commandeinclude)));
                LOCALISATION_COMMANDE(include, T_S(commandeinclude));
                SECURISE(localisationfichier_lecture_nom(T_S(CHAMP_STOCKAGE(general, localisation)), R_T_(nomfichierinclude)));
                SECURISE(commandeinclude_definition_inclusion(T_S(commandeinclude), nomfichierinclude));
                $$=commandeinclude;
        };

commande_standard:      TOKEN_STANDARD
        {
                /* L'inclusion est g�r�e par l'analyseur lexical
                 */
                STOCKAGE(CommandeStandard) commandestandard;
                SECURISE(commandestandard_initialisation(T_S(commandestandard)));
                LOCALISATION_COMMANDE(standard, T_S(commandestandard));
                $$=commandestandard;
        };

commande_option:        TOKEN_OPTION parametre_texte
        {
                STOCKAGE(CommandeOption) commandeoption;
                SECURISE(commandeoption_initialisation(T_S(commandeoption)));
                SECURISE(commandeoption_definition_option(T_S(commandeoption), (TRAVAIL_SCALAIRE(NomOption))(T_S_($2))));
                LOCALISATION_COMMANDE(option, T_S(commandeoption));
                free($2);
                $$=commandeoption;
        };

commande_options:       TOKEN_OPTIONS parametre_texte liste_options TOKEN_END
        {
                STOCKAGE(CommandeOptions) commandeoptions;
                STOCKAGE(Flux) fluxvide;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(commandeoptions_initialisation(T_S(commandeoptions)));
                SECURISE(commandeoptions_definition_label(T_S(commandeoptions), T_S_($2)));
                SECURISE(commandeoptions_definition_clauses(T_S(commandeoptions), T_S($3)));
                SECURISE(flux_initialisation(T_S(fluxvide)));
                SECURISE(flux_definition_type(T_S(fluxvide), T_S_(FLUX_OPTIONS)));
                SECURISE(commandeoptions_definition_autres(T_S(commandeoptions), T_S(fluxvide)));
                SECURISE(commandeoptionsclauses_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S(fluxvide)));
                LOCALISATION_COMMANDE(options, T_S(commandeoptions));
                free($2);
                $$=commandeoptions;
        }
|                       TOKEN_OPTIONS parametre_texte liste_options TOKEN_OTHER flux TOKEN_END
        {
                STOCKAGE(CommandeOptions) commandeoptions;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_OTHER
                SECURISE(commandeoptions_initialisation(T_S(commandeoptions)));
                SECURISE(commandeoptions_definition_label(T_S(commandeoptions), T_S_($2)));
                SECURISE(commandeoptions_definition_clauses(T_S(commandeoptions), T_S($3)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_OPTIONS)));
                SECURISE(commandeoptions_definition_autres(T_S(commandeoptions), T_S($5)));
                SECURISE(commandeoptionsclauses_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($5)));
                LOCALISATION_COMMANDE(options, T_S(commandeoptions));
                free($2);
                SECURISE(DEPILE_FLUX());
                $$=commandeoptions;
        };
liste_options:          liste_options option
        {
                SECURISE(commandeoptionsclauses_ajout_clause(T_S($1), T_S($2)));
                SECURISE(commandeoptionsclause_destruction(T_S($2)));
                $$=$1;
        }
|
        {
                STOCKAGE(CommandeOptionsClauses) commandeoptionsclauses;
                SECURISE(commandeoptionsclauses_initialisation(T_S(commandeoptionsclauses)));
                $$=commandeoptionsclauses;
        };
option:                 TOKEN_CASE parametre_texte flux
        {
                STOCKAGE(CommandeOptionsClause) commandeoptionsclause;
                SECURISE(commandeoptionsclause_initialisation(T_S(commandeoptionsclause)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_OPTIONS)));
                SECURISE(commandeoptionsclause_definition(T_S(commandeoptionsclause), T_S_($2), T_S($3)));
                free($2);
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_CASE
                SECURISE(DEPILE_FLUX());
                $$=commandeoptionsclause;
        };

commande_define:        TOKEN_DEFINE parametre_texte parametre_flux
        {
                STOCKAGE(CommandeDefine) commandedefine;
                SECURISE(commandedefine_initialisation(T_S(commandedefine)));
                SECURISE(commandedefine_definition_nom(T_S(commandedefine), (TRAVAIL_SCALAIRE(NomMacro))(T_S_($2))));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_MACRO_DEFINITION)));
                SECURISE(commandedefine_definition_definition(T_S(commandedefine), T_S($3)));
                SECURISE(flux_destruction(T_S($3)));
                LOCALISATION_COMMANDE(define, T_S(commandedefine));
                free($2);
                $$=commandedefine;
        };

commande_warning:       TOKEN_WARNING parametre_flux
        {
                STOCKAGE(CommandeWarning) commandewarning;
                SECURISE(commandewarning_initialisation(T_S(commandewarning)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_COMPILATEUR)));
                SECURISE(commandewarning_definition_avertissement(T_S(commandewarning), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(warning, T_S(commandewarning));
                $$=commandewarning;
        };

commande_error:         TOKEN_ERROR parametre_flux
        {
                STOCKAGE(CommandeError) commandeerror;
                SECURISE(commandeerror_initialisation(T_S(commandeerror)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_COMPILATEUR)));
                SECURISE(commandeerror_definition_erreur(T_S(commandeerror), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(error, T_S(commandeerror));
                $$=commandeerror;
        };

commande_start:         TOKEN_START parametre_flux
        {
                STOCKAGE(CommandeStart) commandestart;
                SECURISE(commandestart_initialisation(T_S(commandestart)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_NIVEAU)));
                SECURISE(commandestart_definition_niveau(T_S(commandestart), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(start, T_S(commandestart));
                $$=commandestart;
        };

commande_head:          TOKEN_HEAD parametre_flux
        {
                STOCKAGE(CommandeHead) commandehead;
                SECURISE(commandehead_initialisation(T_S(commandehead)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_DOCUMENT)));
                SECURISE(commandehead_definition_entete(T_S(commandehead), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(head, T_S(commandehead));
                $$=commandehead;
        };

commande_foot:          TOKEN_FOOT parametre_flux
        {
                STOCKAGE(CommandeFoot) commandefoot;
                SECURISE(commandefoot_initialisation(T_S(commandefoot)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_DOCUMENT)));
                SECURISE(commandefoot_definition_pied(T_S(commandefoot), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(foot, T_S(commandefoot));
                $$=commandefoot;
        };

commande_section:       TOKEN_SECTION parametre_flux parametre_flux parametre_flux parametre_flux parametre_flux
        {
                STOCKAGE(CommandeSection) commandesection;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandesection_initialisation(T_S(commandesection)));
                SECURISE(commandesection_definition_niveau(T_S(commandesection), T_S(fluxnul)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_SECTION_NOM)));
                SECURISE(commandesection_definition_nom(T_S(commandesection), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_SECTION_FORMAT)));
                SECURISE(commandesection_definition_formatavant(T_S(commandesection), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_SECTION_FORMAT)));
                SECURISE(commandesection_definition_formatapres(T_S(commandesection), T_S($4)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_SECTION_SECTION)));
                SECURISE(commandesection_definition_presection(T_S(commandesection), T_S($5)));
                SECURISE(flux_definition_type(T_S($6), T_S_(FLUX_SECTION_SECTION)));
                SECURISE(commandesection_definition_postsection(T_S(commandesection), T_S($6)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(flux_destruction(T_S($5)));
                SECURISE(flux_destruction(T_S($6)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(section, T_S(commandesection));
                $$=commandesection;
        }
|                       TOKEN_SECTION parametre_flux parametre_flux parametre_flux parametre_flux parametre_flux parametre_flux
        {
                STOCKAGE(CommandeSection) commandesection;
                SECURISE(commandesection_initialisation(T_S(commandesection)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_NIVEAU)));
                SECURISE(commandesection_definition_niveau(T_S(commandesection), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_SECTION_NOM)));
                SECURISE(commandesection_definition_nom(T_S(commandesection), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_SECTION_FORMAT)));
                SECURISE(commandesection_definition_formatavant(T_S(commandesection), T_S($4)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_SECTION_FORMAT)));
                SECURISE(commandesection_definition_formatapres(T_S(commandesection), T_S($5)));
                SECURISE(flux_definition_type(T_S($6), T_S_(FLUX_SECTION_SECTION)));
                SECURISE(commandesection_definition_presection(T_S(commandesection), T_S($6)));
                SECURISE(flux_definition_type(T_S($7), T_S_(FLUX_SECTION_SECTION)));
                SECURISE(commandesection_definition_postsection(T_S(commandesection), T_S($7)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(flux_destruction(T_S($5)));
                SECURISE(flux_destruction(T_S($6)));
                SECURISE(flux_destruction(T_S($7)));
                LOCALISATION_COMMANDE(section, T_S(commandesection));
                $$=commandesection;
        };

commande_reference:     TOKEN_REFERENCE parametre_flux
        {
                STOCKAGE(CommandeReference) commandereference;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandereference_initialisation(T_S(commandereference)));
                SECURISE(commandereference_definition_niveau(T_S(commandereference), T_S(fluxnul)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commandereference_definition_format(T_S(commandereference), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(reference, T_S(commandereference));
                $$=commandereference;
        }
|                       TOKEN_REFERENCE parametre_flux parametre_flux
        {
                STOCKAGE(CommandeReference) commandereference;
                SECURISE(commandereference_initialisation(T_S(commandereference)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_NIVEAU)));
                SECURISE(commandereference_definition_niveau(T_S(commandereference), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commandereference_definition_format(T_S(commandereference), T_S($3)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                LOCALISATION_COMMANDE(reference, T_S(commandereference));
                $$=commandereference;
        };

commande_message:       TOKEN_MESSAGE parametre_flux
        {
                STOCKAGE(CommandeMessage) commandemessage;
                SECURISE(commandemessage_initialisation(T_S(commandemessage)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_MESSAGE)));
                SECURISE(commandemessage_definition_message(T_S(commandemessage), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(message, T_S(commandemessage));
                $$=commandemessage;
        };

commande_title:         TOKEN_TITLE
        {
                STOCKAGE(CommandeTitle) commandetitle;
                SECURISE(commandetitle_initialisation(T_S(commandetitle)));
                LOCALISATION_COMMANDE(title, T_S(commandetitle));
                $$=commandetitle;
        };

commande_ref:           TOKEN_REF
        {
                STOCKAGE(CommandeRef) commanderef;
                SECURISE(commanderef_initialisation(T_S(commanderef)));
                LOCALISATION_COMMANDE(ref, T_S(commanderef));
                $$=commanderef;
        };

commande_txt:           TOKEN_TXT
        {
                STOCKAGE(CommandeTxt) commandetxt;
                SECURISE(commandetxt_initialisation(T_S(commandetxt)));
                LOCALISATION_COMMANDE(txt, T_S(commandetxt));
                $$=commandetxt;
        };

commande_sec:           TOKEN_SEC
        {
                STOCKAGE(CommandeSec) commandesec;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandesec_initialisation(T_S(commandesec)));
                SECURISE(commandesec_definition_niveau(T_S(commandesec), T_S(fluxnul)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(sec, T_S(commandesec));
                $$=commandesec;
        }
|                       TOKEN_SEC parametre_flux
        {
                STOCKAGE(CommandeSec) commandesec;
                SECURISE(commandesec_initialisation(T_S(commandesec)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_NIVEAU)));
                SECURISE(commandesec_definition_niveau(T_S(commandesec), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(sec, T_S(commandesec));
                $$=commandesec;
        };

commande_mesg:          TOKEN_MESG
        {
                STOCKAGE(CommandeMesg) commandemesg;
                SECURISE(commandemesg_initialisation(T_S(commandemesg)));
                LOCALISATION_COMMANDE(mesg, T_S(commandemesg));
                $$=commandemesg;
        };

commande_idea:          TOKEN_IDEA parametre_flux flux TOKEN_TEXT flux TOKEN_END
        {
                STOCKAGE(CommandeIdea) commandeidea;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXT
                SECURISE(commandeidea_initialisation(T_S(commandeidea)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE)));
                SECURISE(commandeidea_definition_reference(T_S(commandeidea), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_TITRE)));
                SECURISE(commandeidea_definition_titre(T_S(commandeidea), T_S($3)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_TEXTE)));
                SECURISE(commandeidea_definition_texte(T_S(commandeidea), T_S($5)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($5)));
                LOCALISATION_COMMANDE(idea, T_S(commandeidea));
                SECURISE(DEPILE_FLUX());
                SECURISE(DEPILE_FLUX());
                $$=commandeidea;
        };

commande_missing:       TOKEN_MISSING parametre_flux flux TOKEN_TEXT flux TOKEN_END
        {
                STOCKAGE(CommandeMissing) commandemissing;
                STOCKAGE(Flux) indicenul;
                indicenul=NULL;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXT
                SECURISE(commandemissing_initialisation(T_S(commandemissing)));
                SECURISE(commandemissing_definition_indice(T_S(commandemissing), T_S(indicenul)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                SECURISE(commandemissing_definition_reference(T_S(commandemissing), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_TITRE_AUTOMATIQUE)));
                SECURISE(commandemissing_definition_titre(T_S(commandemissing), T_S($3)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_TEXTE_MANQUANTE)));
                SECURISE(commandemissing_definition_texte(T_S(commandemissing), T_S($5)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($5)));
                LOCALISATION_COMMANDE(missing, T_S(commandemissing));
                SECURISE(DEPILE_FLUX());
                SECURISE(DEPILE_FLUX());
                $$=commandemissing;
        }
|                       TOKEN_MISSING parametre_flux parametre_flux flux TOKEN_TEXT flux TOKEN_END
        {
                STOCKAGE(CommandeMissing) commandemissing;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXT
                SECURISE(commandemissing_initialisation(T_S(commandemissing)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_INDICE)));
                SECURISE(commandemissing_definition_indice(T_S(commandemissing), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                SECURISE(commandemissing_definition_reference(T_S(commandemissing), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_TITRE_AUTOMATIQUE)));
                SECURISE(commandemissing_definition_titre(T_S(commandemissing), T_S($4)));
                SECURISE(flux_definition_type(T_S($6), T_S_(FLUX_TEXTE_MANQUANTE)));
                SECURISE(commandemissing_definition_texte(T_S(commandemissing), T_S($6)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(flux_destruction(T_S($6)));
                LOCALISATION_COMMANDE(missing, T_S(commandemissing));
                SECURISE(DEPILE_FLUX());
                SECURISE(DEPILE_FLUX());
                $$=commandemissing;
        };

commande_generic:       TOKEN_GENERIC parametre_flux flux TOKEN_TEXT flux TOKEN_END
        {
                STOCKAGE(CommandeGeneric) commandegeneric;
                STOCKAGE(Flux) indicenul;
                indicenul=NULL;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXT
                SECURISE(commandegeneric_initialisation(T_S(commandegeneric)));
                SECURISE(commandegeneric_definition_indice(T_S(commandegeneric), T_S(indicenul)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                SECURISE(commandegeneric_definition_reference(T_S(commandegeneric), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_TITRE_AUTOMATIQUE)));
                SECURISE(commandegeneric_definition_titre(T_S(commandegeneric), T_S($3)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_TEXTE_GENERIQUE)));
                SECURISE(commandegeneric_definition_texte(T_S(commandegeneric), T_S($5)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($5)));
                LOCALISATION_COMMANDE(generic, T_S(commandegeneric));
                SECURISE(DEPILE_FLUX());
                SECURISE(DEPILE_FLUX());
                $$=commandegeneric;
        }
|                       TOKEN_GENERIC parametre_flux parametre_flux flux TOKEN_TEXT flux TOKEN_END
        {
                STOCKAGE(CommandeGeneric) commandegeneric;
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_END
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXT
                SECURISE(commandegeneric_initialisation(T_S(commandegeneric)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_INDICE)));
                SECURISE(commandegeneric_definition_indice(T_S(commandegeneric), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                SECURISE(commandegeneric_definition_reference(T_S(commandegeneric), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_TITRE_AUTOMATIQUE)));
                SECURISE(commandegeneric_definition_titre(T_S(commandegeneric), T_S($4)));
                SECURISE(flux_definition_type(T_S($6), T_S_(FLUX_TEXTE_GENERIQUE)));
                SECURISE(commandegeneric_definition_texte(T_S(commandegeneric), T_S($6)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(flux_destruction(T_S($6)));
                LOCALISATION_COMMANDE(generic, T_S(commandegeneric));
                SECURISE(DEPILE_FLUX());
                SECURISE(DEPILE_FLUX());
                $$=commandegeneric;
        };

commande_index:         TOKEN_INDEX
        {
                STOCKAGE(CommandeIndex) commandeindex;
                SECURISE(commandeindex_initialisation(T_S(commandeindex)));
                LOCALISATION_COMMANDE(index, T_S(commandeindex));
                $$=commandeindex;
        };

commande_extref:        TOKEN_EXTREF parametre_flux
        {
                STOCKAGE(CommandeExtRef) commandeextref;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandeextref_initialisation(T_S(commandeextref)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_INDICE)));
                SECURISE(commandeextref_definition_indice(T_S(commandeextref), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(commandeextref_definition_format(T_S(commandeextref), T_S(fluxnul)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(extref, T_S(commandeextref));
                $$=commandeextref;
        }
|                       TOKEN_EXTREF parametre_flux parametre_flux
        {
                STOCKAGE(CommandeExtRef) commandeextref;
                SECURISE(commandeextref_initialisation(T_S(commandeextref)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_INDICE)));
                SECURISE(commandeextref_definition_indice(T_S(commandeextref), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commandeextref_definition_format(T_S(commandeextref), T_S($3)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                LOCALISATION_COMMANDE(extref, T_S(commandeextref));
                $$=commandeextref;
        };

commande_extrefs:       TOKEN_EXTREFS
        {
                STOCKAGE(CommandeExtRefs) commandeextrefs;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandeextrefs_initialisation(T_S(commandeextrefs)));
                SECURISE(commandeextrefs_definition_format(T_S(commandeextrefs), T_S(fluxnul)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(extrefs, T_S(commandeextrefs));
                $$=commandeextrefs;
        }
|                       TOKEN_EXTREFS parametre_flux
        {
                STOCKAGE(CommandeExtRefs) commandeextrefs;
                SECURISE(commandeextrefs_initialisation(T_S(commandeextrefs)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commandeextrefs_definition_format(T_S(commandeextrefs), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(extrefs, T_S(commandeextrefs));
                $$=commandeextrefs;
        };

commande_dep:           TOKEN_DEP parametre_flux parametre_flux parametre_flux
        {
                STOCKAGE(CommandeDep) commandedep;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandedep_initialisation(T_S(commandedep)));
                SECURISE(commandedep_definition_pertinence(T_S(commandedep), T_S(fluxnul)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE)));
                SECURISE(commandedep_definition_destination(T_S(commandedep), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_TEXTE_IRREDUCTIBLE)));
                SECURISE(commandedep_definition_irreductible(T_S(commandedep), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_TEXTE_REDUCTIBLE)));
                SECURISE(commandedep_definition_reductible(T_S(commandedep), T_S($4)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(dep, T_S(commandedep));
                $$=commandedep;
        }
|                       TOKEN_DEP parametre_flux parametre_flux parametre_flux parametre_flux
        {
                STOCKAGE(CommandeDep) commandedep;
                SECURISE(commandedep_initialisation(T_S(commandedep)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_PERTINENCE)));
                SECURISE(commandedep_definition_pertinence(T_S(commandedep), T_S($2)));
                SECURISE(flux_definition_type(T_S($3), T_S_(FLUX_REFERENCE)));
                SECURISE(commandedep_definition_destination(T_S(commandedep), T_S($3)));
                SECURISE(flux_definition_type(T_S($4), T_S_(FLUX_TEXTE_IRREDUCTIBLE)));
                SECURISE(commandedep_definition_irreductible(T_S(commandedep), T_S($4)));
                SECURISE(flux_definition_type(T_S($5), T_S_(FLUX_TEXTE_REDUCTIBLE)));
                SECURISE(commandedep_definition_reductible(T_S(commandedep), T_S($5)));
                SECURISE(flux_destruction(T_S($2)));
                SECURISE(flux_destruction(T_S($3)));
                SECURISE(flux_destruction(T_S($4)));
                SECURISE(flux_destruction(T_S($5)));
                LOCALISATION_COMMANDE(dep, T_S(commandedep));
                $$=commandedep;
        };

commande_depref:        TOKEN_DEPREF
        {
                STOCKAGE(CommandeDepRef) commandedepref;
                STOCKAGE(Flux) fluxnul;
                fluxnul=NULL;
                SECURISE(commandedepref_initialisation(T_S(commandedepref)));
                SECURISE(commandedepref_definition_format(T_S(commandedepref), T_S(fluxnul)));
                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(depref, T_S(commandedepref));
                $$=commandedepref;
        }
|                       TOKEN_DEPREF parametre_flux
        {
                STOCKAGE(CommandeDepRef) commandedepref;
                SECURISE(commandedepref_initialisation(T_S(commandedepref)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commandedepref_definition_format(T_S(commandedepref), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(depref, T_S(commandedepref));
                $$=commandedepref;
        };

commande_comment:       TOKEN_COMMENT parametre_flux
        {
                STOCKAGE(CommandeCommentaire) commandecommentaire;
                SECURISE(commandecommentaire_initialisation(T_S(commandecommentaire)));
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_COMMENTAIRE)));
                SECURISE(commandecommentaire_definition_commentaire(T_S(commandecommentaire), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                LOCALISATION_COMMANDE(commentaire, T_S(commandecommentaire));
                $$=commandecommentaire;
        };

commande_echappement:   TOKEN_ECHAP_TAB
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT('\t');
        }
|                       TOKEN_ECHAP_ESPACE
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT(' ');
        }
|                       TOKEN_ECHAP_RETOURLIGNE
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT('\n');
        }
|                       TOKEN_ECHAP_COMMANDE
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT('#');
        }
|                       TOKEN_ECHAP_PARAM_DEBUT
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT('{');
        }
|                       TOKEN_ECHAP_PARAM_FIN
        {
                $$=DEFINIR_COMMANDE_ECHAPPEMENT('}');
        };

commande_texte:         TOKEN_TEXTE
        {
                STOCKAGE(CommandeTexte) commandetexte;
                TRAVAIL_SCALAIRE(Chaine) texte;
                SECURISE(commandetexte_initialisation(T_S(commandetexte)));
                SECURISE(LIRE_TEXTE(texte));
                SECURISE(commandetexte_definition_texte(T_S(commandetexte), texte));
                LOCALISATION_COMMANDE(texte, T_S(commandetexte));
                free(S_T_(texte));
                $$=commandetexte;
        };

commande_macro:         TOKEN_MACRO liste_parametre
        {
                STOCKAGE(CommandeMacro) commandemacro;
                TRAVAIL_SCALAIRE(Chaine) nommacro;
                TRAVAIL_SCALAIRE(Booleen) appelssuccessifs;
                niveauimbricationmacro--;//on enregistre un appel de macro : on sort d'un appel de macro
                SECURISE(MACRO_DEFINIR_NIVEAU(niveauimbricationmacro));//MaJ niveau sommet de la pile
                SECURISE(MACRO_APPELS_SUCCESSIFS(appelssuccessifs));
                if(appelssuccessifs==T_S_(VRAI))
                {
                        SECURISE(MACRO_ECHANGE());
                }

                SECURISE(commandemacro_initialisation(T_S(commandemacro)));
                SECURISE(LIRE_MACRO(nommacro));
                SECURISE(commandemacro_definition_nom(T_S(commandemacro), nommacro));
                SECURISE(commandemacro_definition_parametres(T_S(commandemacro), T_S($2)));
                SECURISE(commandemacroparametres_destruction(T_S($2)));

                SECURISE(LOCALISATION_ECHANGE());
                LOCALISATION_COMMANDE(macro, T_S(commandemacro));
                free(S_T_(nommacro));
                $$=commandemacro;
        };

liste_parametre:        liste_parametre parametre_flux
        {
                SECURISE(flux_definition_type(T_S($2), T_S_(FLUX_MACRO_PARAMETRE)));
                SECURISE(commandemacroparametres_ajout_parametre(T_S($1), T_S($2)));
                SECURISE(flux_destruction(T_S($2)));
                $$=$1;
        }
|
        {
                STOCKAGE(CommandeMacroParametres) commandemacroparametres;
                SECURISE(commandemacroparametres_initialisation(T_S(commandemacroparametres)));
                niveauimbricationmacro++;//on initialise une liste de parametres : on est dans un appel de macro
                $$=commandemacroparametres;
        };

commande_parametre:     TOKEN_PARAMETRE
        {
                STOCKAGE(CommandeParametre) commandeparametre;
                TRAVAIL_SCALAIRE(Entier) parametre;
                SECURISE(commandeparametre_initialisation(T_S(commandeparametre)));
                SECURISE(LIRE_PARAMETRE(parametre));
                SECURISE(commandeparametre_definition_indice(T_S(commandeparametre), parametre));
                LOCALISATION_COMMANDE(parametre, T_S(commandeparametre));
                $$=commandeparametre;
        };



parametre_flux:         TOKEN_PARAM_DEBUT flux TOKEN_PARAM_FIN
        {
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_PARAM_FIN
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_PARAM_DEBUT
                SECURISE(DEPILE_FLUX());
                $$=$2;
        };
parametre_texte:        TOKEN_PARAM_DEBUT TOKEN_TEXTE TOKEN_PARAM_FIN
        {
                TRAVAIL_SCALAIRE(Chaine) texte;
                SECURISE(LIRE_TEXTE(texte));
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_PARAM_FIN
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_TEXTE
                SECURISE(DETRUIRE_DERNIERE_LOCALISATION());//localisation du TOKEN_PARAM_DEBUT
                $$=texte;
        };

%%
void yyerror()
{
        LOCALISATION_PROBLEME_PARSE(T_S_(PROBLEME_ERREUR_SYNTAXE), T_S_(CHAMP_STOCKAGE(general, erreursyntaxe)));

        /* Normalement on atteint jamais cette portion, une erreur de syntaxe provoquant toujours l'arret du programme.
         */
        fprintf(SORTIEERREUR, "Syntax error. This is not the usual error message, maybe due to unexpected error.\n");
        raise(SIGTERM);
}
