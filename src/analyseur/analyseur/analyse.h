/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#ifndef __ANALYSE__
#define __ANALYSE__

#include <src/global/global.h>

#include <src/analyseur/donnees/pilenomfichier.h>
#include <src/analyseur/donnees/filenomfichier.h>
#include <src/donnees/general/general.h>
#include <src/donnees/environnement/environnement.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/flux/flux.h>

Resultat analyse_analyseur(TRAVAIL(General) general);
/* Analyse les fichiers sources de planfacile.
 * Renvoie une valeur autre que RESULTAT_OK en
 * cas d'erreur. Si un probl�me est d�t�ct�,
 * la fonction ne revient pas. La fonction
 * prend ses param�tres directement dans la
 * variable General *general.
 */

#endif
