/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __MACROSANALYSEUR__
#define __MACROSANALYSEUR__

#include <src/global/global.h>
#include <src/donnees/general/general.h>

#include <src/analyseur/analyseur/aideanalyseur.h>

#include <src/analyseur/donnees/pilelocalisationfichier.h>
#include <src/analyseur/donnees/filenomfichier.h>
#include <src/analyseur/donnees/pilechaine.h>
#include <src/analyseur/donnees/pileentier.h>


/* Localisations
 */

#define LOCALISATION_NULLE_FONCTION(WRAP, FONCTION, ...)                         \
    do {                                                                         \
        STOCKAGE(LocalisationFichier) localisationnulle;                         \
        WRAP(localisationfichier_initialisation(T_S(localisationnulle)));        \
        WRAP(FONCTION(T_S(localisationnulle), ##__VA_ARGS__));                   \
        WRAP(localisationfichier_destruction(T_S(localisationnulle)));           \
    } while(0)

#define LOCALISATION_FONCTION(WRAP, FONCTION, localisation, ...)                 \
    do {                                                                         \
        TRAVAIL_SCALAIRE(Booleen) copielocalisation;                             \
        WRAP(analyseur_verificationcopielocalisation(T_S(CHAMP_STOCKAGE(general, environnement)), R_T_(copielocalisation)));\
        if(copielocalisation==T_S_(VRAI))                                        \
        {                                                                        \
            WRAP(FONCTION(localisation, ##__VA_ARGS__));                         \
        }                                                                        \
        else                                                                     \
        {                                                                        \
            LOCALISATION_NULLE_FONCTION(WRAP, FONCTION, ##__VA_ARGS__);          \
        }                                                                        \
    } while(0)

#define LOCALISATION_PILE_FONCTION(WRAP, FONCTION, ...)                          \
    do {                                                                         \
        STOCKAGE(LocalisationFichier) localisation;                              \
        WRAP(LIRE_LOCALISATION(localisation));                                   \
        LOCALISATION_FONCTION(WRAP, FONCTION, T_S(localisation), ##__VA_ARGS__); \
        WRAP(localisationfichier_destruction(T_S(localisation)));                \
    } while(0)


#define WRAP_NEUTRE(fonction)   fonction
#define WRAP_SECURISE(fonction) SECURISE(fonction)


#define FONCTION_LOCALISATION_PROBLEME(localisation, identifiant, ...)  probleme_probleme(T_S(CHAMP_STOCKAGE(general, environnement)), identifiant, localisation, ##__VA_ARGS__)

#define LOCALISATION_PROBLEME_PARSE(identifiant, valeur)                LOCALISATION_PILE_FONCTION(WRAP_NEUTRE, FONCTION_LOCALISATION_PROBLEME, identifiant, valeur)
#define LOCALISATION_PROBLEME(identifiant, localisation, ...)           LOCALISATION_FONCTION(WRAP_SECURISE, FONCTION_LOCALISATION_PROBLEME, localisation, identifiant, ##__VA_ARGS__)
#define LOCALISATION_NULLE_PROBLEME(identifiant, ...)                   LOCALISATION_NULLE_FONCTION(WRAP_SECURISE, FONCTION_LOCALISATION_PROBLEME, identifiant, ##__VA_ARGS__)



/* Donnees
 */

#define _PILE_AJOUT(type, champ, ...)     pile##type##_ajout(T_S(CHAMP_STOCKAGE(general, champ)), ##__VA_ARGS__)

#define AJOUT_LOCALISATION()              _PILE_AJOUT(localisationfichier, pilelocalisation, T_S(CHAMP_STOCKAGE(general, localisation)))
#define AJOUT_TEXTE(valeur)               _PILE_AJOUT(chaine, piletexte, T_S_(CAST_S_(Chaine, valeur)))
#define AJOUT_PARAMETRE(valeur)           _PILE_AJOUT(entier, pileparametre, T_S_(CAST_S_(Entier, valeur)))
#define AJOUT_MACRO(chaine, niveau)       _PILE_AJOUT(nommacro, pilenommacro, T_S_(CAST_S_(Chaine, chaine)), T_S_(CAST_S_(Entier, niveau)))


#define _PILE_RETRAIT(type, champ, ...)   pile##type##_retrait(T_S(CHAMP_STOCKAGE(general, champ)), ##__VA_ARGS__)

#define LIRE_LOCALISATION(loc)            _PILE_RETRAIT(localisationfichier, pilelocalisation, C_S(loc))
#define LIRE_TEXTE(valeur)                _PILE_RETRAIT(chaine, piletexte, C_S_(valeur))
#define LIRE_PARAMETRE(valeur)            _PILE_RETRAIT(entier, pileparametre, C_S_(valeur))
#define LIRE_MACRO(valeur)                _PILE_RETRAIT(nommacro, pilenommacro, C_S_(valeur))


#define _ECHANGE_PILE(type, champ)        pile##type##_echange(T_S(CHAMP_STOCKAGE(general, champ)))

#define LOCALISATION_ECHANGE()            _ECHANGE_PILE(localisationfichier, pilelocalisation)
#define MACRO_ECHANGE()                   _ECHANGE_PILE(nommacro, pilenommacro)


#define MACRO_DEFINIR_NIVEAU(niveau)      pilenommacro_definition_niveauimbrication(T_S(CHAMP_STOCKAGE(general, pilenommacro)), T_S_(niveau))
#define MACRO_APPELS_SUCCESSIFS(valeur)   pilenommacro_appels_successifs(T_S(CHAMP_STOCKAGE(general, pilenommacro)), R_T_(valeur))

#define DEF_ERREUR_SYNTAXE(chaine)        analyseur_modificationerreursyntaxe(T_S(general), T_S_(CAST_S_(Chaine, chaine)))
#define MODIFICATION_POSITION(position)   localisationfichier_modification(T_S(CHAMP_STOCKAGE(general, localisation)), T_S_(CAST_S_(Entier, position)))

#define DETRUIRE_DERNIERE_LOCALISATION()  analyseur_detruiredernierelocalisation(T_S(CHAMP_STOCKAGE(general, pilelocalisation)))

#define EMPILE_FLUX(valeur)               _PILE_AJOUT(flux, pileflux, valeur)
#define DEPILE_FLUX(valeur)               _PILE_RETRAIT(flux, pileflux)
#define TEST_PILE_FLUX() \
        ({                                                                                   \
                TRAVAIL_SCALAIRE(Booleen) vide;                                              \
                SECURISE(pileflux_vide(T_S(CHAMP_STOCKAGE(general, pileflux)), R_T_(vide))); \
                (vide==T_S_(VRAI)) ? RESULTAT_OK : RESULTAT_ERREUR;                          \
        })

#endif//__MACROSANALYSEUR
