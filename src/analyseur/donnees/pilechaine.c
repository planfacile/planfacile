/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pilechaine.h"

static Resultat pilechaine_copieinterne(TRAVAIL(PileChaine) original, TRAVAIL(PileChaine) copie)
{
        if(S_T(original)==NULL)
                return RESULTAT_OK;
        SECURISE(pilechaine_copieinterne(CHAMP_TRAVAIL(original, suivant),copie));
        SECURISE(pilechaine_ajout(copie,T_S_(CHAMP(original, chaine))));
        return RESULTAT_OK;
}

Resultat pilechaine_initialisation(TRAVAIL(PileChaine) pilechaine)
{
        /* Cr�e une pile de chaines.
         */
        S_T(pilechaine)=NULL;
        return RESULTAT_OK;
}

Resultat pilechaine_ajout(TRAVAIL(PileChaine) pilechaine, TRAVAIL_SCALAIRE(Chaine) chaine)
{
        /* Ajoute une chaine au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileChaine) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileChaine))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pilechaine);
        S_T(pilechaine)=nouveau;

        ASSERTION((CHAMP_STOCKAGE(nouveau, chaine)=DUP_CAST(Chaine, chaine))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        return RESULTAT_OK;
}

Resultat pilechaine_retrait(TRAVAIL(PileChaine) pilechaine, COREFERENCE_SCALAIRE(Chaine) chaine)
{
        /* Retire le sommet de la pile, et renvoye la chaine.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Il faudra lib�rer la chaine renvoy�e � la main.
         */
        STOCKAGE(PileChaine) ancien;
        ASSERTION(S_T(pilechaine)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pilechaine);
        S_T(pilechaine)=CHAMP_STOCKAGE(ancien, suivant);
        S_C_(chaine)=CHAMP_STOCKAGE(ancien, chaine);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pilechaine_vide(TRAVAIL(PileChaine) pilechaine, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pilechaine)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pilechaine_copie(TRAVAIL(PileChaine) pilechaine, TRAVAIL(PileChaine) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(pilechaine_destruction(copie));
        SECURISE(pilechaine_initialisation(copie));
        SECURISE(pilechaine_copieinterne(pilechaine,copie));
        return RESULTAT_OK;
}

Resultat pilechaine_destruction(TRAVAIL(PileChaine) pilechaine)
{
        /* D�truit une pile de chaines.
         */
        if(S_T(pilechaine)==NULL)
                return RESULTAT_OK;
        SECURISE(pilechaine_destruction(CHAMP_TRAVAIL(pilechaine, suivant)));
        free(CHAMP(pilechaine, chaine));
        free(S_T(pilechaine));
        S_T(pilechaine)=NULL;
        return RESULTAT_OK;
}

