/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILECHAINE__
#define __PILECHAINE__

#include <src/global/global.h>

typedef struct pilechaine CONTENEUR(PileChaine);

struct pilechaine
{
        STOCKAGE_SCALAIRE(Chaine) chaine;
        //Chaine � conserver au chaud.
        STOCKAGE(PileChaine) suivant;
        //Pointeur sur le suivant.
};
/* Pile de chaines utilis�es lors de l'analyse de l'entr�e de
 * planfacile, et grrrrmbl � bison et flex !
 */

Resultat pilechaine_initialisation(TRAVAIL(PileChaine) pilechaine);
/* Cr�e une pile de chaines.
 */

Resultat pilechaine_ajout(TRAVAIL(PileChaine) pilechaine, TRAVAIL_SCALAIRE(Chaine) chaine);
/* Ajoute une chaine au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pilechaine_retrait(TRAVAIL(PileChaine) pilechaine, COREFERENCE_SCALAIRE(Chaine) chaine);
/* Retire le sommet de la pile, et renvoye la chaine.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Il faudra lib�rer la chaine renvoy�e � la main.
 */

Resultat pilechaine_vide(TRAVAIL(PileChaine) pilechaine, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pilechaine_copie(TRAVAIL(PileChaine) pilechaine, TRAVAIL(PileChaine) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat pilechaine_destruction(TRAVAIL(PileChaine) pilechaine);
/* D�truit une pile de chaines.
 */

#endif
