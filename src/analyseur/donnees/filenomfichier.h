/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __FILENOMFICHIER__
#define __FILENOMFICHIER__

#include <src/global/global.h>

typedef struct filenomfichier CONTENEUR(FileNomFichier);

typedef struct maillonfilenomfichier CONTENEUR(MaillonFileNomFichier);

struct maillonfilenomfichier
{
        STOCKAGE_SCALAIRE(NomFichier) nom;
        //Nom de fichier � parcourir.
        STOCKAGE(MaillonFileNomFichier) suivant;
        //Pointeur sur le prochain nom de fichier � parcourir.
};
/* Cette structure est destin�e � recevoir les noms de fichiers, ainsi que
 * de respecter l'ordre de leur entr�e.
 */

struct filenomfichier
{
        STOCKAGE(MaillonFileNomFichier) lecture;
        //Indique le prochain nom de fichier � �tre lu.
        //Il correspond � la t�te de la liste.
        STOCKAGE(MaillonFileNomFichier) ecriture;
        //Indique le dernier nom de fichier � avoir �t� �crit.
        //Il correspond au dernier �l�ment du chainage des noms de
        //fichier.
};
/* Cette structure sert � fournir � l'analyseur lexical le nom des fichiers �
 * parcourir en vue de l'analyse.
 */

Resultat filenomfichier_initialisation(TRAVAIL(FileNomFichier) filenomfichier);
/* Cr�e une file de nom de fichiers.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
 */

Resultat filenomfichier_ajout_nomfichier(TRAVAIL(FileNomFichier) filenomfichier, TRAVAIL_SCALAIRE(NomFichier) nomfichier);
/* Ajoute un nom de fichier � la liste de noms de fichiers.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
 * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
 */

Resultat filenomfichier_retrait_nomfichier(TRAVAIL(FileNomFichier) filenomfichier, COREFERENCE_SCALAIRE(NomFichier) nomfichier);
/* Retire un nom de fichier de la file, en le r�cup�rant dans la variable pass�e en parametre.
 * ATTENTION, le nom renvoye devra etre libere avec free() !
 * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
 * Renvoie RESULTAT_ERREUR_NONTROUVE si la file est vide.
 */

Resultat filenomfichier_vide(TRAVAIL(FileNomFichier) filenomfichier, REFERENCE_SCALAIRE(Booleen) vide);
/* Indique si la file de noms de fichiers est vide ou non.
 * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
 */

Resultat filenomfichier_copie(TRAVAIL(FileNomFichier) original, TRAVAIL(FileNomFichier) copie);
/* Cr�e une copie de la file de noms de fichiers donn�e en param�tre.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat filenomfichier_destruction(TRAVAIL(FileNomFichier) filenomfichier);
/* D�truit une file de noms de fichiers.
 */

#endif
