/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pilelocalisationfichier.h"

static Resultat pilelocalisationfichier_copieinterne(TRAVAIL(PileLocalisationFichier) original, TRAVAIL(PileLocalisationFichier) copie)
{
        if(S_T(original)==NULL)
                return RESULTAT_OK;
        SECURISE(pilelocalisationfichier_copieinterne(CHAMP_TRAVAIL(original, suivant),copie));
        SECURISE(pilelocalisationfichier_ajout(copie,CHAMP_TRAVAIL(original, localisationfichier)));
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_initialisation(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier)
{
        /* Cr�e une pile de localisationfichier.
         */
        S_T(pilelocalisationfichier)=NULL;
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_ajout(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, TRAVAIL(LocalisationFichier) localisationfichier)
{
        /* Ajoute une localisationfichier au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileLocalisationFichier) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileLocalisationFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pilelocalisationfichier);
        S_T(pilelocalisationfichier)=nouveau;
        CHAMP_STOCKAGE(nouveau, localisationfichier)=NULL;
        SECURISE(localisationfichier_copie(localisationfichier,T_S(CHAMP_STOCKAGE(nouveau, localisationfichier))));
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_retrait(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, COREFERENCE(LocalisationFichier) localisationfichier)
{
        /* Retire le sommet de la pile, et renvoye la localisationfichier.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Il faudra lib�rer la localisationfichier renvoy�e � la main.
         */
        STOCKAGE(PileLocalisationFichier) ancien;
        ASSERTION(S_T(pilelocalisationfichier)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pilelocalisationfichier);
        S_T(pilelocalisationfichier)=CHAMP_STOCKAGE(ancien, suivant);
        S_C(localisationfichier)=CHAMP_STOCKAGE(ancien, localisationfichier);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_vide(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pilelocalisationfichier)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_echange(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier)
{
        /* Echange les 2 premieres localisations de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(PileLocalisationFichier) temporaire;
        ASSERTION(S_T(pilelocalisationfichier)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(pilelocalisationfichier, suivant)==NULL)
                return RESULTAT_OK;
        temporaire=S_T(pilelocalisationfichier);
        S_T(pilelocalisationfichier)=CHAMP_STOCKAGE(temporaire, suivant);
        CHAMP_STOCKAGE(temporaire, suivant)=CHAMP(pilelocalisationfichier, suivant);
        CHAMP(pilelocalisationfichier, suivant)=temporaire;
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_copie(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, TRAVAIL(PileLocalisationFichier) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(pilelocalisationfichier_destruction(copie));
        SECURISE(pilelocalisationfichier_initialisation(copie));
        SECURISE(pilelocalisationfichier_copieinterne(pilelocalisationfichier,copie));
        return RESULTAT_OK;
}

Resultat pilelocalisationfichier_destruction(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier)
{
        /* D�truit une pile de localisationfichiers.
         */
        if(S_T(pilelocalisationfichier)==NULL)
                return RESULTAT_OK;
        SECURISE(pilelocalisationfichier_destruction(CHAMP_TRAVAIL(pilelocalisationfichier, suivant)));
        SECURISE(localisationfichier_destruction(CHAMP_TRAVAIL(pilelocalisationfichier, localisationfichier)));
        free(S_T(pilelocalisationfichier));
        S_T(pilelocalisationfichier)=NULL;
        return RESULTAT_OK;
}

