/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "filenomfichier.h"

static Resultat filenomfichiermaillon_initialisation(TRAVAIL(MaillonFileNomFichier) maillon)
{
        /* Initialise un maillon.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         */
        ASSERTION((S_T(maillon)=NOUVEAU(MaillonFileNomFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(maillon, nom)=NULL;
        CHAMP(maillon, suivant)=NULL;
        return RESULTAT_OK;
}

static Resultat filenomfichiermaillon_definition_nom(TRAVAIL(MaillonFileNomFichier) maillon, TRAVAIL_SCALAIRE(NomFichier) nomfichier)
{
        /* Place un nom de fichier dans un maillon.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         */
        ASSERTION(S_T(maillon)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(maillon, nom)!=NULL)
                free(CHAMP(maillon, nom));

        ASSERTION((CHAMP(maillon, nom)=DUP_CAST(NomFichier, nomfichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        return RESULTAT_OK;
}

static Resultat filenomfichiermaillon_destruction(TRAVAIL(MaillonFileNomFichier) maillon)
{
        /* D�truit un maillon.
         */
        if(S_T(maillon)==NULL)
                return RESULTAT_OK;
        if(CHAMP(maillon, nom)!=NULL)
                free(CHAMP(maillon, nom));
        free(S_T(maillon));
        S_T(maillon)=NULL;
        return RESULTAT_OK;
}

static Resultat filenomfichier_destruction_interne(TRAVAIL(MaillonFileNomFichier) maillon)
{
        /* D�truit un chainage de maillons.
         */
        if(S_T(maillon)==NULL)
                return RESULTAT_OK;
        SECURISE(filenomfichier_destruction_interne(CHAMP_TRAVAIL(maillon, suivant)));
        SECURISE(filenomfichiermaillon_destruction(maillon));
        return RESULTAT_OK;
}

static Resultat filenomfichiermaillon_copie(TRAVAIL(MaillonFileNomFichier) original, TRAVAIL(MaillonFileNomFichier) copie)
{
        /* Copie juste un maillon.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         */
        ASSERTION(S_T(original)!=NULL, RESULTAT_ERREUR);

        SECURISE(filenomfichiermaillon_destruction(copie));
        SECURISE(filenomfichiermaillon_initialisation(copie));
        SECURISE(filenomfichiermaillon_definition_nom(copie,T_S_(CHAMP(original, nom))));
        return RESULTAT_OK;
}

static Resultat filenomfichier_copie_interne(TRAVAIL(MaillonFileNomFichier) original, TRAVAIL(MaillonFileNomFichier) copie, TRAVAIL(MaillonFileNomFichier) dernier)
{
        /* Copie un chainage de maillons.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec.
         */
        STOCKAGE(MaillonFileNomFichier) nouveau;
        if(S_T(original)==NULL)
        {
                S_T(copie)=NULL;
                return RESULTAT_OK;
        }
        SECURISE(filenomfichier_copie_interne(CHAMP_TRAVAIL(original, suivant),copie,dernier));
        nouveau=NULL;
        SECURISE(filenomfichiermaillon_copie(original,T_S(nouveau)));
        CHAMP_STOCKAGE(nouveau, suivant)=S_T(copie);
        S_T(copie)=nouveau;
        if(CHAMP(original, suivant)==NULL)
                S_T(dernier)=nouveau;
        return RESULTAT_OK;
}

Resultat filenomfichier_initialisation(TRAVAIL(FileNomFichier) filenomfichier)
{
        /* Cr�e une file de nom de fichiers.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         */ 
        ASSERTION((S_T(filenomfichier)=NOUVEAU(FileNomFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(filenomfichier, lecture)=NULL;
        CHAMP(filenomfichier, ecriture)=NULL;
        return RESULTAT_OK;
}

Resultat filenomfichier_ajout_nomfichier(TRAVAIL(FileNomFichier) filenomfichier, TRAVAIL_SCALAIRE(NomFichier) nomfichier)
{
        /* Ajoute un nom de fichier � la liste de noms de fichiers.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
         */
        STOCKAGE(MaillonFileNomFichier) nouveau;
        ASSERTION(S_T(filenomfichier)!=NULL, RESULTAT_ERREUR);

        SECURISE(filenomfichiermaillon_initialisation(T_S(nouveau)));
        SECURISE(filenomfichiermaillon_definition_nom(T_S(nouveau),nomfichier));
        if(CHAMP(filenomfichier, ecriture)!=NULL)
                CHAMP(CHAMP_TRAVAIL(filenomfichier, ecriture), suivant)=nouveau;
        else
                CHAMP(filenomfichier, lecture)=nouveau;
        CHAMP(filenomfichier, ecriture)=nouveau;
        return RESULTAT_OK;
}

Resultat filenomfichier_retrait_nomfichier(TRAVAIL(FileNomFichier) filenomfichier, COREFERENCE_SCALAIRE(NomFichier) nomfichier)
{
        /* Retire un nom de fichier de la file, en le r�cup�rant dans la variable pass�e en parametre.
         * ATTENTION, le nom renvoye devra etre libere avec free() !
         * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
         * Renvoie RESULTAT_ERREUR_NONTROUVE si la file est vide.
         */
        STOCKAGE(MaillonFileNomFichier) supprime;
        ASSERTION(S_T(filenomfichier)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(filenomfichier, lecture)!=NULL, RESULTAT_ERREUR_NONTROUVE);

        supprime=CHAMP(filenomfichier, lecture);
        CHAMP(filenomfichier, lecture)=CHAMP_STOCKAGE(supprime, suivant);
        if(CHAMP(filenomfichier, lecture)==NULL)
                CHAMP(filenomfichier, ecriture)=NULL;
        S_C_(nomfichier)=CHAMP_STOCKAGE(supprime, nom);
        CHAMP_STOCKAGE(supprime, nom)=NULL;//pour eviter la suppression � ce moment la.
        SECURISE(filenomfichiermaillon_destruction(T_S(supprime)));
        return RESULTAT_OK;
}

Resultat filenomfichier_vide(TRAVAIL(FileNomFichier) filenomfichier, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Indique si la file de noms de fichiers est vide ou non.
         * Renvoie RESULTAT_ERREUR si filenomfichier vaut NULL.
         */
        ASSERTION(S_T(filenomfichier)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(filenomfichier, lecture)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat filenomfichier_copie(TRAVAIL(FileNomFichier) original, TRAVAIL(FileNomFichier) copie)
{
        /* Cr�e une copie de la file de noms de fichiers donn�e en param�tre.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         */
        ASSERTION(S_T(original)!=NULL, RESULTAT_ERREUR);

        SECURISE(filenomfichier_destruction(copie));
        SECURISE(filenomfichier_initialisation(copie));
        SECURISE(filenomfichier_copie_interne(CHAMP_TRAVAIL(original, lecture),CHAMP_TRAVAIL(copie, lecture),CHAMP_TRAVAIL(copie, ecriture)));
        return RESULTAT_OK;
}

Resultat filenomfichier_destruction(TRAVAIL(FileNomFichier) filenomfichier)
{
        /* D�truit une file de noms de fichiers.
         */
        if(S_T(filenomfichier)==NULL)
                return RESULTAT_OK;
        SECURISE(filenomfichier_destruction_interne(CHAMP_TRAVAIL(filenomfichier, lecture)));
        free(S_T(filenomfichier));
        S_T(filenomfichier)=NULL;
        return RESULTAT_OK;
}

