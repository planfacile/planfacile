/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2014  Sylvain PLANTEF�VE
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#ifndef __PILEFLUX__
#define __PILEFLUX__

#include <src/global/global.h>

typedef struct pileflux CONTENEUR(PileFlux);

#include <src/donnees/flux/flux.h>

struct pileflux
{
        STOCKAGE(Flux) flux;
        //Pointeur sur le Flux � conserver au chaud.
        STOCKAGE(PileFlux) suivant;
        //Pointeur sur le suivant.
};
/* Pile de flux utilis�e lors de l'analyse de l'entr�e de
 * planfacile
 */

Resultat pileflux_initialisation(TRAVAIL(PileFlux) pileflux);
/* Cr�e une pile de flux.
 */

Resultat pileflux_ajout(TRAVAIL(PileFlux) pileflux, TRAVAIL(Flux) flux);
/* Ajoute un flux au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pileflux_retrait(TRAVAIL(PileFlux) pileflux);
/* Retire le sommet de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pileflux_vide(TRAVAIL(PileFlux) pileflux, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pileflux_destruction(TRAVAIL(PileFlux) pileflux);
/* D�truit une pile de flux.
 */

#endif
