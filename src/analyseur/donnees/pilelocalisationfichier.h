/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#ifndef __PILELOCALISATIONFICHIER__
#define __PILELOCALISATIONFICHIER__

#include <src/global/global.h>

#include <src/donnees/commandes/localisationfichier.h>

typedef struct pilelocalisationfichier CONTENEUR(PileLocalisationFichier);

struct pilelocalisationfichier
{
        STOCKAGE(LocalisationFichier) localisationfichier;
        //Pointeur sur la LocalisationFichier � conserver au chaud.
        STOCKAGE(PileLocalisationFichier) suivant;
        //Pointeur sur le suivant.
};
/* Pile de localisationfichier utilis�e lors de l'analyse de l'entr�e de
 * planfacile
 */

Resultat pilelocalisationfichier_initialisation(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier);
/* Cr�e une pile de localisationfichier.
 */

Resultat pilelocalisationfichier_ajout(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, TRAVAIL(LocalisationFichier) localisationfichier);
/* Ajoute une localisationfichier au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pilelocalisationfichier_retrait(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, COREFERENCE(LocalisationFichier) localisationfichier);
/* Retire le sommet de la pile, et renvoye la localisationfichier.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Il faudra lib�rer la localisationfichier renvoy�e � la main.
 */

Resultat pilelocalisationfichier_vide(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pilelocalisationfichier_echange(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier);
/* Echange les 2 premieres localisations de la pile
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pilelocalisationfichier_copie(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier, TRAVAIL(PileLocalisationFichier) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat pilelocalisationfichier_destruction(TRAVAIL(PileLocalisationFichier) pilelocalisationfichier);
/* D�truit une pile de localisationfichier.
 */

#endif
