/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILENOMFICHIER__
#define __PILENOMFICHIER__

#include <src/global/global.h>

typedef struct pilenomfichier CONTENEUR(PileNomFichier);

struct pilenomfichier
{
        STOCKAGE_SCALAIRE(NomFichier) nomfichier;
        //NomFichier � conserver au chaud.
        STOCKAGE(PileNomFichier) suivant;
        //Pointeur sur le suivant.
};
/* Pile de nom de fichiers utilis�es lors de l'analyse de l'entr�e de
 * planfacile, et grrrrmbl � bison et flex !
 */

Resultat pilenomfichier_initialisation(TRAVAIL(PileNomFichier) pilenomfichier);
/* Cr�e une pile de nom de fichiers.
 */

Resultat pilenomfichier_ajout(TRAVAIL(PileNomFichier) pilenomfichier, TRAVAIL_SCALAIRE(NomFichier) nomfichier);
/* Ajoute un nom de fichier au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pilenomfichier_retrait(TRAVAIL(PileNomFichier) pilenomfichier, COREFERENCE_SCALAIRE(NomFichier) nomfichier);
/* Retire le sommet de la pile, et renvoye le nom de fichier.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 * Il faudra lib�rer le nom de fichier renvoy� � la main.
 */

Resultat pilenomfichier_vide(TRAVAIL(PileNomFichier) pilenomfichier, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pilenomfichier_copie(TRAVAIL(PileNomFichier) pilenomfichier, TRAVAIL(PileNomFichier) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat pilenomfichier_destruction(TRAVAIL(PileNomFichier) pilenomfichier);
/* D�truit une pile de nom de fichiers.
 */

#endif
