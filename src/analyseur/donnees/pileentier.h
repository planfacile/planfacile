/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PILEENTIER__
#define __PILEENTIER__

#include <src/global/global.h>

typedef struct pileentier CONTENEUR(PileEntier);

struct pileentier
{
        STOCKAGE_SCALAIRE(Entier) entier;
        //Entier � conserver au chaud.
        STOCKAGE(PileEntier) suivant;
        //Pointeur sur le suivant.
};
/* Pile d'entiers utilis�es lors de l'analyse de l'entr�e de
 * planfacile, et grrrrmbl � bison et flex !
 */

Resultat pileentier_initialisation(TRAVAIL(PileEntier) pileentier);
/* Cr�e une pile d'entiers.
 */

Resultat pileentier_ajout(TRAVAIL(PileEntier) pileentier, TRAVAIL_SCALAIRE(Entier) entier);
/* Ajoute un entier au sommet de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat pileentier_retrait(TRAVAIL(PileEntier) pileentier, COREFERENCE_SCALAIRE(Entier) entier);
/* Retire le sommet de la pile, et renvoye l'entier.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pileentier_definition_entier(TRAVAIL(PileEntier) pileentier, TRAVAIL_SCALAIRE(Entier) entier);
/* Change la premi�re valeur de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pileentier_lecture_entier(TRAVAIL(PileEntier) pileentier, REFERENCE_SCALAIRE(Entier) entier);
/* Lit la premi�re valeur de la pile.
 * Renvoie RESULTAT_ERREUR si la pile est vide.
 */

Resultat pileentier_vide(TRAVAIL(PileEntier) pileentier, REFERENCE_SCALAIRE(Booleen) vide);
/* Renvoie VRAI dans vide si la pile est vide.
 */

Resultat pileentier_copie(TRAVAIL(PileEntier) pileentier, TRAVAIL(PileEntier) copie);
/* R�alise une copie de la pile.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 * La copie est d�truite si elle est non vide.
 */

Resultat pileentier_destruction(TRAVAIL(PileEntier) pileentier);
/* D�truit une pile d'entiers.
 */

#endif
