/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2014  Sylvain PLANTEF�VE
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pileflux.h"


Resultat pileflux_initialisation(TRAVAIL(PileFlux) pileflux)
{
        /* Cr�e une pile de flux.
         */
        S_T(pileflux)=NULL;
        return RESULTAT_OK;
}

Resultat pileflux_ajout(TRAVAIL(PileFlux) pileflux, TRAVAIL(Flux) flux)
{
        /* Ajoute un flux au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileFlux) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileFlux))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pileflux);
        S_T(pileflux)=nouveau;
        CHAMP_STOCKAGE(nouveau, flux)=S_T(flux);
        return RESULTAT_OK;
}

Resultat pileflux_retrait(TRAVAIL(PileFlux) pileflux)
{
        /* Retire le sommet de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(PileFlux) ancien;
        ASSERTION(S_T(pileflux)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pileflux);
        S_T(pileflux)=CHAMP_STOCKAGE(ancien, suivant);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pileflux_vide(TRAVAIL(PileFlux) pileflux, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pileflux)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pileflux_destruction(TRAVAIL(PileFlux) pileflux)
{
        /* D�truit une pile de fluxs.
         */
        if(S_T(pileflux)==NULL)
                return RESULTAT_OK;
        SECURISE(pileflux_destruction(CHAMP_TRAVAIL(pileflux, suivant)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(pileflux, flux)));
        free(S_T(pileflux));
        S_T(pileflux)=NULL;
        return RESULTAT_OK;
}

