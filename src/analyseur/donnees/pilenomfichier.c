/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pilenomfichier.h"

static Resultat pilenomfichier_copieinterne(TRAVAIL(PileNomFichier) original, TRAVAIL(PileNomFichier) copie)
{
        if(S_T(original)==NULL)
                return RESULTAT_OK;
        SECURISE(pilenomfichier_copieinterne(CHAMP_TRAVAIL(original, suivant),copie));
        SECURISE(pilenomfichier_ajout(copie,T_S_(CHAMP(original, nomfichier))));
        return RESULTAT_OK;
}

Resultat pilenomfichier_initialisation(TRAVAIL(PileNomFichier) pilenomfichier)
{
        /* Cr�e une pile de nom de fichiers.
         */
        S_T(pilenomfichier)=NULL;
        return RESULTAT_OK;
}

Resultat pilenomfichier_ajout(TRAVAIL(PileNomFichier) pilenomfichier, TRAVAIL_SCALAIRE(NomFichier) nomfichier)
{
        /* Ajoute une nom de fichier au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileNomFichier) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileNomFichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pilenomfichier);
        S_T(pilenomfichier)=nouveau;

        ASSERTION((CHAMP_STOCKAGE(nouveau, nomfichier)=DUP_CAST(NomFichier, nomfichier))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        return RESULTAT_OK;
}

Resultat pilenomfichier_retrait(TRAVAIL(PileNomFichier) pilenomfichier, COREFERENCE_SCALAIRE(NomFichier) nomfichier)
{
        /* Retire le sommet de la pile, et renvoye le nom de fichier.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         * Il faudra lib�rer le nom de fichier renvoy�e � la main.
         */
        STOCKAGE(PileNomFichier) ancien;
        ASSERTION(S_T(pilenomfichier)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pilenomfichier);
        S_T(pilenomfichier)=CHAMP_STOCKAGE(ancien, suivant);
        S_C_(nomfichier)=CHAMP_STOCKAGE(ancien, nomfichier);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pilenomfichier_vide(TRAVAIL(PileNomFichier) pilenomfichier, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pilenomfichier)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pilenomfichier_copie(TRAVAIL(PileNomFichier) pilenomfichier, TRAVAIL(PileNomFichier) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(pilenomfichier_destruction(copie));
        SECURISE(pilenomfichier_initialisation(copie));
        SECURISE(pilenomfichier_copieinterne(pilenomfichier,copie));
        return RESULTAT_OK;
}

Resultat pilenomfichier_destruction(TRAVAIL(PileNomFichier) pilenomfichier)
{
        /* D�truit une pile de nom de fichiers.
         */
        if(S_T(pilenomfichier)==NULL)
                return RESULTAT_OK;
        SECURISE(pilenomfichier_destruction(CHAMP_TRAVAIL(pilenomfichier, suivant)));
        free(CHAMP(pilenomfichier, nomfichier));
        free(S_T(pilenomfichier));
        S_T(pilenomfichier)=NULL;
        return RESULTAT_OK;
}

