/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "pileentier.h"

static Resultat pileentier_copieinterne(TRAVAIL(PileEntier) original, TRAVAIL(PileEntier) copie)
{
        if(S_T(original)==NULL)
                return RESULTAT_OK;
        SECURISE(pileentier_copieinterne(CHAMP_TRAVAIL(original, suivant),copie));
        SECURISE(pileentier_ajout(copie,T_S_(CHAMP(original, entier))));
        return RESULTAT_OK;
}

Resultat pileentier_initialisation(TRAVAIL(PileEntier) pileentier)
{
        /* Cr�e une pile d'entiers.
         */
        S_T(pileentier)=NULL;
        return RESULTAT_OK;
}

Resultat pileentier_ajout(TRAVAIL(PileEntier) pileentier, TRAVAIL_SCALAIRE(Entier) entier)
{
        /* Ajoute un entier au sommet de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        STOCKAGE(PileEntier) nouveau;
        ASSERTION((nouveau=NOUVEAU(PileEntier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(nouveau, suivant)=S_T(pileentier);
        S_T(pileentier)=nouveau;
        CHAMP_STOCKAGE(nouveau, entier)=S_T_(entier);
        return RESULTAT_OK;
}

Resultat pileentier_retrait(TRAVAIL(PileEntier) pileentier, COREFERENCE_SCALAIRE(Entier) entier)
{
        /* Retire le sommet de la pile, et renvoye l'entier.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        STOCKAGE(PileEntier) ancien;
        ASSERTION(S_T(pileentier)!=NULL, RESULTAT_ERREUR);

        ancien=S_T(pileentier);
        S_T(pileentier)=CHAMP_STOCKAGE(ancien, suivant);
        S_C_(entier)=CHAMP_STOCKAGE(ancien, entier);
        free(ancien);
        return RESULTAT_OK;
}

Resultat pileentier_definition_entier(TRAVAIL(PileEntier) pileentier, TRAVAIL_SCALAIRE(Entier) entier)
{
        /* Change la premi�re valeur de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        ASSERTION(S_T(pileentier)!=NULL, RESULTAT_ERREUR);

        CHAMP(pileentier, entier)=S_T_(entier);
        return RESULTAT_OK;
}

Resultat pileentier_lecture_entier(TRAVAIL(PileEntier) pileentier, REFERENCE_SCALAIRE(Entier) entier)
{
        /* Lit la premi�re valeur de la pile.
         * Renvoie RESULTAT_ERREUR si la pile est vide.
         */
        ASSERTION(S_T(pileentier)!=NULL, RESULTAT_ERREUR);

        T_R_(entier)=T_S_(CHAMP(pileentier, entier));
        return RESULTAT_OK;
}

Resultat pileentier_vide(TRAVAIL(PileEntier) pileentier, REFERENCE_SCALAIRE(Booleen) vide)
{
        /* Renvoie VRAI dans vide si la pile est vide.
         */
        if(S_T(pileentier)==NULL)
                T_R_(vide)=T_S_(VRAI);
        else
                T_R_(vide)=T_S_(FAUX);
        return RESULTAT_OK;
}

Resultat pileentier_copie(TRAVAIL(PileEntier) pileentier, TRAVAIL(PileEntier) copie)
{
        /* R�alise une copie de la pile.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         * La copie est d�truite si elle est non vide.
         */
        SECURISE(pileentier_destruction(copie));
        SECURISE(pileentier_initialisation(copie));
        SECURISE(pileentier_copieinterne(pileentier,copie));
        return RESULTAT_OK;
}

Resultat pileentier_destruction(TRAVAIL(PileEntier) pileentier)
{
        /* D�truit une pile d'entiers.
         */
        if(S_T(pileentier)==NULL)
                return RESULTAT_OK;
        SECURISE(pileentier_destruction(CHAMP_TRAVAIL(pileentier, suivant)));
        free(S_T(pileentier));
        S_T(pileentier)=NULL;
        return RESULTAT_OK;
}

