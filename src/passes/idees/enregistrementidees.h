/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ENREGISTREMENTIDEES__
#define __ENREGISTREMENTIDEES__

#include <src/global/global.h>

typedef struct enregistrementidees CONTENEUR(EnregistrementIdees);

#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/general/general.h>
#include <src/problemes/probleme/probleme.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/commandedep.h>
#include <src/donnees/commandes/commandemissing.h>
#include <src/donnees/commandes/commandegeneric.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

struct enregistrementidees
{
        STOCKAGE(ActionFlux) actionenregistrementidee;
        //Action de flux servant � enregistrer
        //les id�es. Il y aura deux passages :
        //1) pour enregister les
        //   id�es normales ;
        //2) pour enregister les
        //   id�es automatiques.
        STOCKAGE_SCALAIRE(Indice) ideepourdependances;
        //Indice dans la structure d'id�e
        //pour l'affectation des d�pendances
        //On ne g�re pas ca sous forme de pile,
        //car une seule id�e peut �tre examin�e
        STOCKAGE_SCALAIRE(Chaine) message;
        //Chaine utilis�e pour les messages
        //d'erreur.
};
/* Structure utilis�e pour les besoin internes de
 * l'enregistrement des id�es. Les champs de cette
 * structure seront utilis�s directement.
 * La structure general doit �tre connue globalement.
 */

Resultat enregistrementidees_initialisation(TRAVAIL(EnregistrementIdees) enregistrementidees);
/* Cr�e un structure d'enregistrement d'idees.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat enregistrementidees_enregistrementidees(TRAVAIL(General) general);
/* Enregistre toutes les commandes de style pr�sentes
 * dans les flux.
 * Les formats de section sont d'abord enregistr�s,
 * puis toutes les autres sont enregistr�es, avec
 * la gestion des noms de section.
 */

Resultat enregistrementidees_copie(TRAVAIL(EnregistrementIdees) enregistrementidees, TRAVAIL(EnregistrementIdees) copie);
/* R�alise une copie d'une structure d'enregistrement d'idees.
 * Renvoie RESULTAT_ERREUR si enregistrementidees est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat enregistrementidees_destruction(TRAVAIL(EnregistrementIdees) enregistrementidees);
/* D�truit une structure d'enregistrement d'idees.
 */

#endif
