/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "enregistrementidees.h"

#define HORS_IDEE                -1
#define PERTINENCE_DEFAUT        0

Resultat enregistrementidees_initialisation(TRAVAIL(EnregistrementIdees) enregistrementidees)
{
        /* Cr�e un structure d'enregistrement d'idees.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION((S_T(enregistrementidees)=NOUVEAU(EnregistrementIdees))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(enregistrementidees, actionenregistrementidee)));
        CHAMP(enregistrementidees, ideepourdependances)=HORS_IDEE;
        CHAMP(enregistrementidees, message)=NULL;
        return RESULTAT_OK;
}

static Resultat enregistrementidees_idea(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une id�e.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        TRAVAIL(Flux) reference;
        TRAVAIL_SCALAIRE(Chaine) referencetexte;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL(Flux) texte;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(reference)));
        SECURISE(flux_texte(reference,R_T_(referencetexte)));
        if(STRLEN(referencetexte)==T_S_(0))
        {
                free(S_T_(referencetexte));
                SECURISE(commandeidea_lecture_localisationfichier(commandeidea,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_REFERENCE_VIDE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                S_C_(arretcontexte)=VRAI;
                return RESULTAT_OK;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_IDEE),referencetexte));
        free(S_T_(referencetexte));
        SECURISE(idees_idees_definition_idee(CHAMP_TRAVAIL(general, idees),commandeidea,R_T_(indice),C_S_(remplacement)));
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                SECURISE(commandeidea_lecture_reference(commandeidea,R_T(reference)));
                SECURISE(flux_texte(reference,R_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message))));
                SECURISE(commandeidea_lecture_localisationfichier(commandeidea,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_IDEE),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message))));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                free(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message));
                CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message)=NULL;
        }
        CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), ideepourdependances)=S_T_(indice);
        SECURISE(commandeidea_lecture_texte(commandeidea,R_T(texte)));
        SECURISE(flux_parcours(texte,actionflux,general));
        CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), ideepourdependances)=HORS_IDEE;
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat enregistrementidees_dep(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une d�pendance, pour la construction
         * du graphe.
         */
        TRAVAIL(Flux) fluxargument;
        TRAVAIL_SCALAIRE(Chaine) destination;
        TRAVAIL_SCALAIRE(Pertinence) pertinence;
        STOCKAGE_SCALAIRE(Booleen) nombre;
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);
        ASSERTION(T_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), ideepourdependances))!=T_S_(HORS_IDEE), RESULTAT_ERREUR);

        flux_equivalent=NULL;
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandedep_lecture_localisationfichier(commandedep,R_T(localisationcommande)));
        SECURISE(commandedep_lecture_destination(commandedep,R_T(fluxargument)));
        SECURISE(flux_texte(fluxargument,R_T_(destination)));
        SECURISE(commandedep_lecture_pertinence(commandedep,R_T(fluxargument)));
        if(S_T(fluxargument)!=NULL)
        {
                SECURISE(flux_nombre(fluxargument,C_S_(nombre),R_T_(pertinence)));
        }
        else
        {
                nombre=VRAI;
                S_T_(pertinence)=PERTINENCE_DEFAUT;
        }
        if((T_S_(nombre)==T_S_(FAUX))||(pertinence<T_S_(0)))
        {
                STOCKAGE(LocalisationFichier) localisation;
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_PERTINENCE_INCORRECTE),T_S(localisation),destination));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                free(S_T_(destination));
                return RESULTAT_OK;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_DEPENDANCE),destination));
        SECURISE(idees_idees_ajout_dependance(CHAMP_TRAVAIL(general, idees),T_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), ideepourdependances)),destination,pertinence,localisationcommande));
        free(S_T_(destination));
        return RESULTAT_OK;
}

static Resultat enregistrementidees_missing(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une id�e.
         */
        TRAVAIL_SCALAIRE(Indice) indice;
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE_SCALAIRE(Booleen) defaut;
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL(Flux) reference;
        TRAVAIL_SCALAIRE(Chaine) referencetexte;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemissing_lecture_reference(commandemissing,R_T(reference)));
        SECURISE(flux_reference_manquante(reference,T_S_(0),R_T_(referencetexte)));
        /*On suppose que tous les indices valent 0 :
         * de toute fa�on, on regarde juste si la chaine est vide au final.
         */
        if(STRLEN(referencetexte)==T_S_(0))
        {
                free(S_T_(referencetexte));
                SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_MANQUANTE_REFERENCE_VIDE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                S_C_(arretcontexte)=VRAI;
                return RESULTAT_OK;
        }
        free(S_T_(referencetexte));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(idees_manquantes_definition(CHAMP_TRAVAIL(general, idees),commandemissing,C_S_(correct),C_S_(defaut),R_T_(indice),C_S_(remplacement)));
        if(T_S_(correct)==T_S_(FAUX))
        {
                SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_INDICE_IDEE_MANQUANTE_INCORRECT),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                return RESULTAT_OK;
        }
        if(T_S_(defaut)==T_S_(VRAI))
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_MANQUANTE_DEFAUT)));
        }
        else
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_MANQUANTE_EXPLICITE),indice));
        }
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                if(T_S_(defaut)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE_DEFAUT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
                else
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_IDEE_MANQUANTE),T_S(localisation),indice));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
        }
        return RESULTAT_OK;
}

static Resultat enregistrementidees_generic(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une id�e.
         */
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE_SCALAIRE(Booleen) defaut;
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL(Flux) reference;
        TRAVAIL_SCALAIRE(Chaine) referencetexte;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandegeneric_lecture_reference(commandegeneric,R_T(reference)));
        SECURISE(flux_reference_generique(reference,T_S_("_"),R_T_(referencetexte)));
        /* On consid�re que l'id�e parente est non vide :
         * en fait, cette valeur ne sert qu'� v�rifier
         * que la r�f�rence est non vide.
         */
        if(STRLEN(referencetexte)==T_S_(0))
        {
                free(S_T_(referencetexte));
                SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_GENERIQUE_REFERENCE_VIDE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                S_C_(arretcontexte)=VRAI;
                return RESULTAT_OK;
        }
        free(S_T_(referencetexte));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(idees_generiques_definition(CHAMP_TRAVAIL(general, idees),commandegeneric,C_S_(correct),C_S_(defaut),C_S_(remplacement)));
        if(T_S_(correct)==T_S_(FAUX))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_INDICE_IDEE_GENERIQUE_INCORRECT),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                return RESULTAT_OK;
        }
        if(T_S_(defaut)==T_S_(VRAI))
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_GENERIQUE_DEFAUT)));
        }
        else
        {
                TRAVAIL_SCALAIRE(Chaine) referenceparente;
                TRAVAIL(Flux) parente;
                SECURISE(commandegeneric_lecture_indice(commandegeneric,R_T(parente)));
                SECURISE(flux_texte(parente,R_T_(referenceparente)));
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_GENERIQUE_EXPLICITE),referenceparente));
                free(S_T_(referenceparente));
        }
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                if(T_S_(defaut)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE_DEFAUT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
                else
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        TRAVAIL(Flux) indice;
                        SECURISE(commandegeneric_lecture_indice(commandegeneric,R_T(indice)));
                        SECURISE(flux_texte(indice,R_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message))));
                        SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_IDEE_GENERIQUE),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message))));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        free(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message));
                        CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), message)=NULL;
                }
        }
        return RESULTAT_OK;
}

Resultat enregistrementidees_enregistrementidees(TRAVAIL(General) general)
{
        /* Enregistre toutes les commandes de style pr�sentes
         * dans les flux.
         * Les formats de section sont d'abord enregistr�s,
         * puis toutes les autres sont enregistr�es, avec
         * la gestion des noms de section.
         */
        STOCKAGE(ActionCommande) actioncommande;
        STOCKAGE(ProcessusFlux) processusflux;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES)));
        SECURISE(idees_initialisation(CHAMP_TRAVAIL(general, idees)));
        SECURISE(enregistrementidees_initialisation(CHAMP_TRAVAIL(general, enregistrementidees)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_PRESENTES)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_idea(T_S(actioncommande),enregistrementidees_idea,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_dep(T_S(actioncommande),enregistrementidees_dep,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),general));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_IDEES_AUTOMATIQUES)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_missing(T_S(actioncommande),enregistrementidees_missing,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_generic(T_S(actioncommande),enregistrementidees_generic,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementidees), actionenregistrementidee)),general));
        SECURISE(enregistrementidees_destruction(CHAMP_TRAVAIL(general, enregistrementidees)));
        return RESULTAT_OK;
}

Resultat enregistrementidees_copie(TRAVAIL(EnregistrementIdees) enregistrementidees, TRAVAIL(EnregistrementIdees) copie)
{
        /* R�alise une copie d'une structure d'enregistrement d'idees.
         * Renvoie RESULTAT_ERREUR si enregistrementidees est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(enregistrementidees)!=NULL, RESULTAT_ERREUR);

        SECURISE(enregistrementidees_destruction(copie));
        SECURISE(enregistrementidees_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(enregistrementidees, actionenregistrementidee),CHAMP_TRAVAIL(copie, actionenregistrementidee)));
        CHAMP(copie, ideepourdependances)=CHAMP(enregistrementidees, ideepourdependances);
        if(CHAMP(enregistrementidees, message)==NULL)
                CHAMP(copie, message)=NULL;
        else
        {
                ASSERTION((CHAMP(copie, message)=DUP_CAST(Chaine, T_S_(CHAMP(enregistrementidees, message))))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        return RESULTAT_OK;
}

Resultat enregistrementidees_destruction(TRAVAIL(EnregistrementIdees) enregistrementidees)
{
        /* D�truit une structure d'enregistrement d'idees.
         */
        if(S_T(enregistrementidees)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(enregistrementidees, actionenregistrementidee)));
        if(CHAMP(enregistrementidees, message)!=NULL)
                free(CHAMP(enregistrementidees, message));
        free(S_T(enregistrementidees));
        S_T(enregistrementidees)=NULL;
        return RESULTAT_OK;
}

