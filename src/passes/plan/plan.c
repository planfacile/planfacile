/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "plan.h"

static Resultat calculplan_calcul_denomination(COREFERENCE_SCALAIRE(ListeIdees) listeidees, TRAVAIL_SCALAIRE(IdIdee) ididee, TRAVAIL(DenominationIdee) denomination, TRAVAIL(General) general)
{
        /* Renseigne une d�nomination d'id�e � partir de l'identifiant
         * de l'id�e vis�e.
         */
        STOCKAGE_SCALAIRE(Idee) idee;
        TRAVAIL(Flux) fluxreference;
        STOCKAGE(Flux) fluxreferencetemp;
        TRAVAIL(CommandeIdea) commandeidea;
        TRAVAIL(CommandeMissing) commandemissing;
        TRAVAIL(CommandeGeneric) commandegeneric;
        STOCKAGE(DenominationIdee) denominationdestination;
        TRAVAIL_SCALAIRE(TypeIdee) typeidee;
        TRAVAIL_SCALAIRE(IdIdee) presente;
        TRAVAIL_SCALAIRE(Indice) manquante;
        TRAVAIL_SCALAIRE(Chaine) referenceparente;
        TRAVAIL_SCALAIRE(Chaine) reference;
        STOCKAGE_SCALAIRE(Booleen) correct;
        SECURISE(lectureidee_listeidees(listeidees,ididee,C_S_(idee)));
        switch(idee.type)
        {
                case T_S_(IDEE_PRESENTE):
                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),ididee,R_T(commandeidea)));
                        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(fluxreference)));
                        SECURISE(denominationidee_definition_ideepresente(denomination,ididee,fluxreference));
                        break;
                case T_S_(IDEE_MANQUANTE):
                        SECURISE(idees_manquantes_lecture(CHAMP_TRAVAIL(general, idees),T_S_(CHAMP_STOCKAGE_(idee, manquante)),C_S_(correct),R_T(commandemissing)));
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                SECURISE(commandemissing_lecture_reference(commandemissing,R_T(fluxreference)));
                                SECURISE(denominationidee_definition_ideemanquante(denomination,T_S_(CHAMP_STOCKAGE_(idee, manquante)),fluxreference));
                        }
                        else
                        {
                                SECURISE(flux_initialisation(T_S(fluxreferencetemp)));
                                SECURISE(flux_definition_type(T_S(fluxreferencetemp),T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                                SECURISE(denominationidee_definition_ideemanquante(denomination,T_S_(CHAMP_STOCKAGE_(idee, manquante)),T_S(fluxreferencetemp)));
                                SECURISE(flux_destruction(T_S(fluxreferencetemp)));
                        }
                        break;
                case T_S_(IDEE_GENERIQUE):
                        SECURISE(denominationidee_initialisation(T_S(denominationdestination)));
                        SECURISE(calculplan_calcul_denomination(listeidees,T_S_(CHAMP_STOCKAGE_(idee, generique)),T_S(denominationdestination),general));
                        SECURISE(denominationidee_lecture(T_S(denominationdestination),R_T_(typeidee),R_T_(presente),R_T_(manquante),R_T_(referenceparente),R_T_(reference)));
                        SECURISE(idees_generiques_lecture(CHAMP_TRAVAIL(general, idees),reference,C_S_(correct),R_T(commandegeneric)));
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                SECURISE(commandegeneric_lecture_reference(commandegeneric,R_T(fluxreference)));
                                SECURISE(denominationidee_definition_ideegenerique(denomination,reference,fluxreference));
                        }
                        else
                        {
                                SECURISE(flux_initialisation(T_S(fluxreferencetemp)));
                                SECURISE(flux_definition_type(T_S(fluxreferencetemp),T_S_(FLUX_REFERENCE_AUTOMATIQUE)));
                                SECURISE(denominationidee_definition_ideegenerique(denomination,reference,T_S(fluxreferencetemp)));
                                SECURISE(flux_destruction(T_S(fluxreferencetemp)));
                        }
                        SECURISE(denominationidee_destruction(T_S(denominationdestination)));
                        break;
                case T_S_(IDEE_RACINE):
                        SECURISE(denominationidee_definition_ideeracine(denomination));
                        break;
                case T_S_(IDEE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        return RESULTAT_OK;
}

static Resultat calculplan_transformation(COREFERENCE_SCALAIRE(ListeIdees) listeidees, TRAVAIL_SCALAIRE(NiveauHierarchique) niveauracine, TRAVAIL_SCALAIRE(Plan) sectionalgo, TRAVAIL(ArbreIdees) section, TRAVAIL(General) general)
{
        /* Transforme le plan enregistr� dans la structure renvoy�e
         * par l'algorithme en une structure utilisable par l'�tage
         * de sortie.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation
         * m�moire.
         */
        TRAVAIL_SCALAIRE(IdIdee) idsection;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveausection;
        STOCKAGE(DenominationIdee) denominationsection;
        STOCKAGE(ReferencesIdee) references;
        TRAVAIL_SCALAIRE(Chaine) referenceverbeux;
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Taille) taille;
        STOCKAGE_SCALAIRE(Reference) referencesalgo;
        TRAVAIL_SCALAIRE(TypeReference) typereference;
        STOCKAGE_SCALAIRE(Section*) destination;
        STOCKAGE_SCALAIRE(Contenu) soussectionsalgo;
        STOCKAGE_SCALAIRE(Section*) soussectionalgo;
        ASSERTION(sectionalgo!=NULL, RESULTAT_ERREUR);

        //Cr�ation de la section, et d�nomination de l'id�e.
        SECURISE(arbreidees_initialisation(section));
        SECURISE(lectureididee_section(sectionalgo,R_T_(idsection)));
        SECURISE(lectureniveauhierarchique_section(sectionalgo,R_T_(niveausection)));
        SECURISE(denominationidee_initialisation(T_S(denominationsection)));
        SECURISE(calculplan_calcul_denomination(listeidees,idsection,T_S(denominationsection),general));
        SECURISE(denominationidee_lecture_reference(T_S(denominationsection),R_T_(referenceverbeux)));
        if(S_T_(referenceverbeux)==NULL)
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEEVIDE)));
        }
        else
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_TRANSFORMATION_IDEE),referenceverbeux));
        }
        SECURISE(arbreidees_creation_idee(section,T_S(denominationsection),niveauracine+niveausection));
        SECURISE(denominationidee_destruction(T_S(denominationsection)));
        //Renseignement des r�f�rences.
        SECURISE(referencesidee_initialisation(T_S(references)));
        SECURISE(lecturereferences_section(sectionalgo,C_S_(referencesalgo)));
        SECURISE(taille_reference(C_S_(referencesalgo),R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                SECURISE(lecture_reference(C_S_(referencesalgo),T_S_(indice),R_T_(typereference),C_S_(destination)));
                SECURISE(denominationidee_initialisation(T_S(denominationsection)));
                SECURISE(lectureididee_section(destination,R_T_(idsection)));
                SECURISE(lectureniveauhierarchique_section(destination,R_T_(niveausection)));
                SECURISE(calculplan_calcul_denomination(listeidees,idsection,T_S(denominationsection),general));
                SECURISE(denominationidee_lecture_reference(T_S(denominationsection),R_T_(referenceverbeux)));
                if(S_T_(referenceverbeux)==NULL)
                {
                        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCEVIDE)));
                }
                else
                {
                        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_TRANSFORMATION_REFERENCE),referenceverbeux));
                }
                SECURISE(referencesidee_ajout(T_S(references),typereference,T_S(denominationsection),niveauracine+niveausection));
                SECURISE(denominationidee_destruction(T_S(denominationsection)));
        }
        SECURISE(arbreidees_definition_references(section,T_S(references)));
        SECURISE(referencesidee_destruction(T_S(references)));
        //Calcul des sous-sections, dans l'ordre.
        SECURISE(lecturesoussections_section(sectionalgo,C_S_(soussectionsalgo)));
        SECURISE(taille_contenu(C_S_(soussectionsalgo),R_T_(taille)));
        SECURISE(arbreidees_definition_nombresoussections(section,taille));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                STOCKAGE(ArbreIdees) soussection;
                SECURISE(lecture_contenu(C_S_(soussectionsalgo),T_S_(indice),C_S_(soussectionalgo)));
                SECURISE(calculplan_transformation(listeidees,niveauracine,soussectionalgo,T_S(soussection),general));
                SECURISE(arbreidees_definition_soussection(section,T_S_(indice),T_S(soussection)));
        }
        return RESULTAT_OK;
}

Resultat calculplan_initialisation(TRAVAIL(CalculPlan) calculplan)
{
        /* Cr�e une structure de calcul de plan.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(calculplan)=NOUVEAU(CalculPlan))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(calculplan, message)=NULL;
        return RESULTAT_OK;
}

Resultat calculplan_plan(TRAVAIL(General) general)
{
        /* Calcule le plan du document � partir des informations
         * contenues dans la structure d'enregistrement d'id�es.
         * Le r�sultat est plac� dans une structure appropri�e.
         */
        TRAVAIL_SCALAIRE(Taille) nombreideespresentes;
        STOCKAGE_SCALAIRE(Taille) nombredependancespresentes;
        STOCKAGE_SCALAIRE(Taille) nombredependancesinutiles;
        STOCKAGE_SCALAIRE(Idee) idee;
        STOCKAGE_SCALAIRE(ListeIdees) listeidees;
        STOCKAGE_SCALAIRE(Graphe) graphe;
        STOCKAGE_SCALAIRE(Relations) dependances;
        STOCKAGE_SCALAIRE(Marqueurs) orphelines;
        STOCKAGE_SCALAIRE(Marqueurs) atteignables;
        TRAVAIL_SCALAIRE(Indice) nombreorphelines;
        STOCKAGE_SCALAIRE(IdIdee) ididee;
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE_SCALAIRE(Chaine) dependance;
        STOCKAGE_SCALAIRE(Pertinence) pertinence;
        TRAVAIL(LocalisationFichier) localisation;
        STOCKAGE(LocalisationFichier) localisationdependance;
        STOCKAGE_SCALAIRE(Statistiques) statistiques;
        TRAVAIL_SCALAIRE(Plan) plan;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauracine;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN)));
        SECURISE(calculplan_initialisation(CHAMP_TRAVAIL(general, calculplan)));
        SECURISE(arbreidees_initialisation(CHAMP_TRAVAIL(general, arbreidees)));
        SECURISE(performancesplan_initialisation(CHAMP_TRAVAIL(general, performancesplan)));
        SECURISE(idees_idees_nombre(CHAMP_TRAVAIL(general, idees),R_T_(nombreideespresentes)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_GRAPHE)));
        if(nombreideespresentes==0)
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_GRAPHE_VIDE)));
                CHAMP_STOCKAGE_(statistiques, nombreideestotal)=0;
                CHAMP_STOCKAGE_(statistiques, nombreideesmanquantes)=0;
                CHAMP_STOCKAGE_(statistiques, nombreideesgeneralites)=0;
                CHAMP_STOCKAGE_(statistiques, nombrereferencestotal)=0;
                CHAMP_STOCKAGE_(statistiques, nombrereferencesirreductibles)=0;
                SECURISE(performancesplan_renseignement(CHAMP_TRAVAIL(general, performancesplan),T_S_(statistiques)));
                return RESULTAT_OK;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_GRAPHE_IDEES),nombreideespresentes));
        SECURISE(creation_marqueurs(C_S_(orphelines),nombreideespresentes));
        SECURISE(creation_listeidees(C_S_(listeidees)));
        SECURISE(creation_graphe(C_S_(graphe),nombreideespresentes));
        SECURISE(lecturedependances_graphe(C_S_(graphe),C_S_(dependances)));
        SECURISE(creation_idee(C_S_(idee),T_S_(IDEE_PRESENTE),T_S_(INDICE_IDEE_INVALIDE),T_S_(INDICE_IDEE_INVALIDE)));
        nombredependancespresentes=0;
        nombredependancesinutiles=0;
        for(ididee=0 ; T_S_(ididee)<nombreideespresentes ; T_S_(ididee)++)
        {
                SECURISE(ajoutidee_listeidees(C_S_(listeidees),idee));
                for(;;)
                {
                        TRAVAIL_SCALAIRE(IdIdee) destination;
                        SECURISE(idees_idees_retrait_dependance(CHAMP_TRAVAIL(general, idees),T_S_(ididee),C_S_(correct),C_S_(dependance),C_S_(pertinence),C_S(localisationdependance)));
                        if(T_S_(correct)==T_S_(FAUX))
                                break;
                        T_S_(nombredependancespresentes)++;
                        if(STRLEN(T_S_(dependance))==T_S_(0))
                        {
                                T_S_(nombredependancesinutiles)++;
                                free(dependance);
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_DEPENDANCE_VIDE_INUTILE),T_S(localisationdependance)));
                                SECURISE(localisationfichier_destruction(T_S(localisationdependance)));
                                continue;
                        }
                        SECURISE(idees_idees_recherche_idee(CHAMP_TRAVAIL(general, idees),T_S_(dependance),R_S_(correct),R_T_(destination)));
                        if((T_S_(correct)==T_S_(VRAI))&&(T_S_(ididee)!=destination))
                        {
                                SECURISE(ajoutrelation_relations(C_S_(dependances),destination,T_S_(ididee),T_S_(pertinence)));
                                //Attention, pour les d�pendances, les relations sont invers�es !
                                free(dependance);
                                SECURISE(coche_marqueurs(C_S_(orphelines),T_S_(ididee)));
                        }
                        else
                        {
                                STOCKAGE(LocalisationFichier) localisationcopie;
                                T_S_(nombredependancesinutiles)++;
                                localisationcopie=NULL;
                                SECURISE(localisationfichier_copie(T_S(localisationdependance),T_S(localisationcopie)));
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_DEPENDANCE_INUTILE),T_S(localisationcopie),T_S_(dependance)));
                                SECURISE(localisationfichier_destruction(T_S(localisationcopie)));
                                free(dependance);
                        }
                        SECURISE(localisationfichier_destruction(T_S(localisationdependance)));
                }
        }
        SECURISE(destruction_idee(C_S_(idee)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_GRAPHE_DEPENDANCES),T_S_(nombredependancespresentes),T_S_(nombredependancesinutiles)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_RACINE)));
        SECURISE(nombre_marqueurs(C_S_(orphelines),R_T_(nombreorphelines)));
        nombreorphelines=nombreideespresentes-nombreorphelines;
        switch(nombreorphelines)
        {
                case T_S_(0):
                        {
                                STOCKAGE(LocalisationFichier) localisationcopie;
                                localisationcopie=NULL;
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_SANS_IDEE_ORPHELINE),T_S(localisationcopie)));
                        }
                        break;
                case T_S_(1):
                        {
                                STOCKAGE_SCALAIRE(IdIdee) ididee;
                                TRAVAIL_SCALAIRE(Booleen) racine;
                                for(ididee=0 ; T_S_(ididee)<nombreideespresentes ; T_S_(ididee)++)
                                {
                                        SECURISE(verification_marqueurs(C_S_(orphelines),T_S_(ididee),R_T_(racine)));
                                        if(racine==T_S_(FAUX))
                                                break;
                                }
                                CHAMP_STOCKAGE_(graphe, depart)=ididee;
                        }
                        break;
                default:
                        {
                                STOCKAGE(LocalisationFichier) localisationcopie;
                                localisationcopie=NULL;
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_AJOUT_RACINE),T_S(localisationcopie)));
                                SECURISE(ajouteracine_algo(C_S_(listeidees),C_S_(graphe),C_S_(orphelines),C_S_(CHAMP_STOCKAGE_(graphe, depart))));
                                nombreideespresentes++;
                        }
                        break;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_ATTEIGNABLES)));
        SECURISE(creation_marqueurs(C_S_(atteignables),nombreideespresentes));
        SECURISE(rechercheaccessibilite_algo(C_S_(graphe),C_S_(atteignables)));
        for(ididee=0 ; T_S_(ididee)<nombreideespresentes ; T_S_(ididee)++)
        {
                SECURISE(verification_marqueurs(C_S_(atteignables),T_S_(ididee),R_S_(correct)));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        TRAVAIL(CommandeIdea) commandeidea;
                        TRAVAIL(Flux) fluxreference;
                        STOCKAGE(LocalisationFichier) localisationcopie;
                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),T_S_(ididee),R_T(commandeidea)));
                        localisationcopie=NULL;
                        SECURISE(commandeidea_lecture_localisationfichier(commandeidea,R_T(localisation)));
                        SECURISE(localisationfichier_copie(localisation,T_S(localisationcopie)));
                        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(fluxreference)));
                        SECURISE(flux_texte(fluxreference,R_S_(CHAMP_STOCKAGE(CHAMP(general, calculplan), message))));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_NON_ATTEIGNABLE),T_S(localisationcopie),T_S_(CHAMP_STOCKAGE(CHAMP(general, calculplan), message))));
                        SECURISE(localisationfichier_destruction(T_S(localisationcopie)));
                }
        }
        SECURISE(destruction_marqueurs(C_S_(atteignables)));
        SECURISE(destruction_marqueurs(C_S_(orphelines)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_CALCUL)));
        SECURISE(rechercheplan_algo(C_S_(listeidees),C_S_(graphe),R_T_(plan),C_S_(statistiques)));
        SECURISE(performancesplan_renseignement(CHAMP_TRAVAIL(general, performancesplan),T_S_(statistiques)));
        SECURISE(styles_racine_lecture(CHAMP_TRAVAIL(general, styles),R_T_(niveauracine)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_PLAN_TRANSFORMATION)));
        SECURISE(calculplan_transformation(C_S_(listeidees),niveauracine,plan,CHAMP_TRAVAIL(general, arbreidees),general));
        SECURISE(destruction_section(plan));
        SECURISE(destruction_listeidees(C_S_(listeidees)));
        SECURISE(calculplan_destruction(CHAMP_TRAVAIL(general, calculplan)));
        return RESULTAT_OK;
}

Resultat calculplan_copie(TRAVAIL(CalculPlan) calculplan, TRAVAIL(CalculPlan) copie)
{
        /* R�alise une copie d'une structure de calcul du plan.
         * Renvoie RESULTAT_ERREUR si calculplan est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(calculplan)!=NULL, RESULTAT_ERREUR);

        SECURISE(calculplan_destruction(copie));
        SECURISE(calculplan_initialisation(copie));
        if(CHAMP(calculplan, message)==NULL)
                CHAMP(copie, message)=NULL;
        else
                ASSERTION((CHAMP(copie, message)=DUP_CAST(Chaine, T_S_(CHAMP(calculplan, message))))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        return RESULTAT_OK;
}

Resultat calculplan_destruction(TRAVAIL(CalculPlan) calculplan)
{
        /* D�truit une structure de calcul du plan.
         */
        if(S_T(calculplan)==NULL)
                return RESULTAT_OK;
        if(CHAMP(calculplan, message)!=NULL)
                free(CHAMP(calculplan, message));
        free(S_T(calculplan));
        S_T(calculplan)=NULL;
        return RESULTAT_OK;
}

