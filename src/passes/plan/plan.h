/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PLAN__
#define __PLAN__

#include <src/global/global.h>

typedef struct calculplan CONTENEUR(CalculPlan);

#include <src/donnees/general/general.h>
#include <src/donnees/idees/idees.h>
#include <src/donnees/styles/styles.h>
#include <src/donnees/sortie/arbreidees.h>
#include <src/donnees/sortie/referencesidee.h>
#include <src/donnees/sortie/performancesplan.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/commandes/commandeidea.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

#include <src/algo/algo/algo.h>
#include <src/algo/donnees/listeidees.h>
#include <src/algo/donnees/graphe.h>
#include <src/algo/donnees/relations.h>
#include <src/algo/donnees/marqueurs.h>
#include <src/algo/donnees/section.h>
#include <src/algo/donnees/contenu.h>

struct calculplan
{
        STOCKAGE_SCALAIRE(Chaine) message;
        //Chaine utilis�e pour les messages.
};
/* Structure utilis�e pour le calcul du plan.
 * Le champ sera directement utilis�.
 */

Resultat calculplan_initialisation(TRAVAIL(CalculPlan) calculplan);
/* Cr�e une structure de calcul de plan.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat calculplan_plan(TRAVAIL(General) general);
/* Calcule le plan du document � partir des informations
 * contenues dans la structure d'enregistrement d'id�es.
 * Le r�sultat est plac� dans une structure appropri�e.
 */

Resultat calculplan_copie(TRAVAIL(CalculPlan) calculplan, TRAVAIL(CalculPlan) copie);
/* R�alise une copie d'une structure de calcul du plan.
 * Renvoie RESULTAT_ERREUR si calculplan est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat calculplan_destruction(TRAVAIL(CalculPlan) calculplan);
/* D�truit une structure de calcul du plan.
 */

#endif
