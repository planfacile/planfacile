/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "reductionoptions.h"

#define NIVEAUIMBRICATIONFACTICE        0

Resultat reductionoptions_initialisation(TRAVAIL(ReductionOptions) reductionoptions)
{
        /* Cr�e une structure de reduction des options.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(reductionoptions)=NOUVEAU(ReductionOptions))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(reductionoptions, actionreductionoptions)));
        SECURISE(option_initialisation(CHAMP_TRAVAIL(reductionoptions, attente)));
        SECURISE(option_initialisation(CHAMP_TRAVAIL(reductionoptions, global)));
        SECURISE(pilenommacro_initialisation(CHAMP_TRAVAIL(reductionoptions, pilenommacro)));
        CHAMP(reductionoptions, retardtraitementoptions)=FAUX;
        return RESULTAT_OK;
}

static Resultat reductionoptions_recherche_clause_executable(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Execute la premi�re clause �x�cutable.
         */
        TRAVAIL_SCALAIRE(NomOption) nomoption;
        TRAVAIL(Flux) clause;
        TRAVAIL_SCALAIRE(Booleen) execution;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        if(T_S_(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), retardtraitementoptions))==T_S_(VRAI))
        {
                S_C(flux)=NULL;
                S_C_(arretcontexte)=VRAI;
                return RESULTAT_OK;
        }
        SECURISE(commandeoptions_lecture_clauseexecutable(commandeoptions,T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), global)),R_T_(nomoption),R_T(clause),R_T_(execution)));
        SECURISE(flux_parcours(clause,actionflux,general));
        if(nomoption==T_S_(NULL))
        {
                SECURISE(commandeoptions_definition_executionautres(commandeoptions,T_S_(VRAI)));
        }
        else
        {
                SECURISE(commandeoptions_definition_executionclause(commandeoptions,nomoption,T_S_(VRAI)));
        }
        S_C(flux)=NULL;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionoptions_gestion_definitionmacro(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale le passage dans une macro en
         * posant le nom de la macro dans la pile.
         */
        TRAVAIL_SCALAIRE(NomMacro) nommacro;
        STOCKAGE_SCALAIRE(NomMacro) nommacroretour;
        TRAVAIL(Flux) definition;
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedefine_lecture_nom(commandedefine,R_T_(nommacro)));
        SECURISE(pilenommacro_ajout(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), pilenommacro)),nommacro,T_S_(NIVEAUIMBRICATIONFACTICE)));
        SECURISE(commandedefine_lecture_definition(commandedefine,R_T(definition)));
        SECURISE(flux_parcours(definition,actionflux,general));
        SECURISE(pilenommacro_retrait(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), pilenommacro)),C_S_(nommacroretour)));
        free(nommacroretour);
        return RESULTAT_OK;
}

static Resultat reductionoptions_gestion_appelmacro(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale le passage dans une macro en
         * posant le nom de la macro dans la pile.
         */
        TRAVAIL_SCALAIRE(NomMacro) nommacro;
        STOCKAGE_SCALAIRE(NomMacro) nommacroretour;
        TRAVAIL(Flux) parametre;
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Taille) taille;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacro_lecture_nom(commandemacro,R_T(nommacro)));
        SECURISE(pilenommacro_ajout(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), pilenommacro)),nommacro,T_S_(NIVEAUIMBRICATIONFACTICE)));
        SECURISE(commandemacro_lecture_tailleparametre(commandemacro,R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                SECURISE(commandemacro_lecture_parametre(commandemacro,T_S_(indice),R_T(parametre)));
                SECURISE(flux_parcours(parametre,actionflux,general));
        }
        SECURISE(pilenommacro_retrait(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), pilenommacro)),C_S_(nommacroretour)));
        free(nommacroretour);
        return RESULTAT_OK;
}

static Resultat reductionoptions_enregistrement_option(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre les options du flux dans la liste d'attente.
         */
        TRAVAIL(Option) option;
        TRAVAIL_SCALAIRE(Taille) taille;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_lecture_option(flux,R_T(option)));
        SECURISE(option_lecture_taille(option,R_T_(taille)));
        if(taille>T_S_(0))
        {
                TRAVAIL_SCALAIRE(Taille) taille;
                STOCKAGE_SCALAIRE(Indice) indice;
                TRAVAIL_SCALAIRE(NomOption) nomoption;
                TRAVAIL(CommandeOption) commandeoption;
                SECURISE(option_lecture_taille(option,R_T_(taille)));
                for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
                {
                        SECURISE(option_lecture_option(option,T_S_(indice),R_T(commandeoption)));
                        SECURISE(commandeoption_lecture_option(commandeoption,R_T_(nomoption)));
                        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_ENREGISTREMENT),nomoption));
                }//Dommage de mettre une boucle ici, juste pour le mode verbeux. Mais bon...
                SECURISE(option_fusion(CHAMP_TRAVAIL(CHAMP_TRAVAIL(general, reductionoptions), attente),option));
                SECURISE(option_destruction(option));
                SECURISE(option_initialisation(option));
        }
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionoptions_elimination_option(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Elimine la liste d'option silencieusement.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        TRAVAIL(Option) option;
        TRAVAIL_SCALAIRE(Taille) taille;
        SECURISE(flux_lecture_option(flux,R_T(option)));
        SECURISE(option_lecture_taille(option,R_T_(taille)));
        if(taille>T_S_(0))
        {
                TRAVAIL_SCALAIRE(Taille) taille;
                STOCKAGE_SCALAIRE(Indice) indice;
                TRAVAIL_SCALAIRE(NomOption) nomoption;
                TRAVAIL(CommandeOption) commandeoption;
                SECURISE(option_lecture_taille(option,R_T_(taille)));
                for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
                {
                        SECURISE(option_lecture_option(option,T_S_(indice),R_T(commandeoption)));
                        SECURISE(commandeoption_lecture_option(commandeoption,R_T_(nomoption)));
                        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_COMMENTAIRE),nomoption));
                }//Dommage de mettre une boucle ici, juste pour le mode verbeux. Mais bon... //FIXME
                SECURISE(option_destruction(option));
                SECURISE(option_initialisation(option));
        }
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionoptions_problemeoptionmacro(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale les options d�clar�es dans les
         * macros, et les �limine.
         */
        TRAVAIL_SCALAIRE(Taille) taille;
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(NomOption) nomoption;
        TRAVAIL_SCALAIRE(NomMacro) nommacro;
        TRAVAIL(Option) option;
        TRAVAIL(CommandeOption) commandeoption;
        TRAVAIL(LocalisationFichier) localisationfichier;
        STOCKAGE(LocalisationFichier) localisation;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_lecture_option(flux,R_T(option)));
        SECURISE(option_lecture_taille(option,R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                SECURISE(option_lecture_option(option,T_S_(indice),R_T(commandeoption)));
                SECURISE(commandeoption_lecture_option(commandeoption,R_T_(nomoption)));
                SECURISE(commandeoption_lecture_localisationfichier(commandeoption,R_T(localisationfichier)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationfichier,T_S(localisation)));
                SECURISE(pilenommacro_lecture_nom(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), pilenommacro)),R_T_(nommacro)));
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_MACRO),nomoption,nommacro));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement), T_S_(PROBLEME_OPTION_DANS_MACRO), T_S(localisation), nomoption, nommacro));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
        }
        SECURISE(option_destruction(option));
        SECURISE(option_initialisation(option));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionoptions_premierepasse(TRAVAIL(General) general)
{
        /* Recherche les options r�ellement d�finies, et
         * marque les clauses des commandes #options qui
         * doivent �tre conserv�es.
         */
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, reductionoptions)!=NULL, RESULTAT_ERREUR);

        STOCKAGE(ProcessusFlux) processusflux;
        STOCKAGE(ActionCommande) actioncommande;
        TRAVAIL_SCALAIRE(Taille) taille;
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_RECHERCHE)));
        SECURISE(option_fusion(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), global)),CHAMP_TRAVAIL(general, option)));
        SECURISE(option_destruction(CHAMP_TRAVAIL(general, option)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        
        SECURISE(actioncommande_definition_options(T_S(actioncommande),reductionoptions_recherche_clause_executable,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_define(T_S(actioncommande),reductionoptions_gestion_definitionmacro,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_macro(T_S(actioncommande),reductionoptions_gestion_appelmacro,T_S_(PARCOURS_MANUEL)));

        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));

        SECURISE(processusflux_definition_contexte(T_S(processusflux),T_S_(CONTEXTE_CONTEXTE)));
        SECURISE(processusflux_definition_preaction(T_S(processusflux),reductionoptions_enregistrement_option));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(processusflux_definition_preaction(T_S(processusflux),reductionoptions_elimination_option));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(processusflux_definition_preaction(T_S(processusflux),reductionoptions_problemeoptionmacro));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));

        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_RECHERCHE_PREMIERE)));
        CHAMP_STOCKAGE(CHAMP(general, reductionoptions), retardtraitementoptions)=VRAI;
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),general));
        SECURISE(option_lecture_taille(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente)),R_T_(taille)));
        if(taille!=T_S_(0))
        {
                SECURISE(option_fusion(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), global)),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
                SECURISE(option_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
                SECURISE(option_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
        }
        CHAMP_STOCKAGE(CHAMP(general, reductionoptions), retardtraitementoptions)=FAUX;
        for(;;)
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_RECHERCHE_NOUVELLE)));
                SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),general));
                SECURISE(option_lecture_taille(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente)),R_T_(taille)));
                if(taille==T_S_(0))
                        break;
                SECURISE(option_fusion(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), global)),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
                SECURISE(option_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
                SECURISE(option_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), attente))));
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_RECHERCHE_FIN)));

        SECURISE(actionflux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions))));
        return RESULTAT_OK;
}

static Resultat reductionoptions_reductioncommandeoptions(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace les commandes #options par le flux �quivalent.
         */
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        STOCKAGE(Flux) flux_equivalent;
        TRAVAIL(Macro) macro;
        TRAVAIL_SCALAIRE(Label) label;
        flux_equivalent=NULL;
        SECURISE(commandeoptions_lecture_label(commandeoptions,R_T_(label)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_REDUCTION_OPTIONS),label));
        SECURISE(commandeoptions_extraction_fluxexecutable(commandeoptions,T_S(flux_equivalent),general));
        SECURISE(flux_lecture_macro(T_S(flux_equivalent),R_T(macro)));
        SECURISE(macro_definition_comportement(macro,T_S_(MACROAJOUT_CONSERVE),T_S_(MACROPROBLEME_VERBEUX)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionoptions_secondepasse(TRAVAIL(General) general)
{
        /* Remplacement dans les flux des commandes #options
         * par les clauses s�lectionn�es � l'aide des commandes
         * #option.
         */
        ASSERTION(CHAMP(general, principal)!=NULL, RESULTAT_ERREUR);

        STOCKAGE(ProcessusFlux) processusflux;
        STOCKAGE(ActionCommande) actioncommande;
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS_REDUCTION)));
        SECURISE(actionflux_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions))));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));

        SECURISE(actioncommande_definition_options(T_S(actioncommande),reductionoptions_reductioncommandeoptions,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));

        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_contexte(T_S(processusflux),T_S_(CONTEXTE_LOCAL)));

        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),T_S(processusflux)));

        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));

        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions)),general));

        SECURISE(actionflux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionoptions), actionreductionoptions))));
        return RESULTAT_OK;
}

Resultat reductionoptions_reductionoptions(TRAVAIL(General) general)
{
        /* Calcule quelles sont les options qui sont r�ellement
         * d�finies dans le flux, et en d�duit les clauses � conserver
         * dans le flux. L'int�gration des clauses est �galement
         * g�r�e.
         */
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_OPTIONS)));
        SECURISE(reductionoptions_initialisation(CHAMP_TRAVAIL(general, reductionoptions)));
        SECURISE(reductionoptions_premierepasse(general));
        SECURISE(reductionoptions_secondepasse(general));
        SECURISE(reductionoptions_destruction(CHAMP_TRAVAIL(general, reductionoptions)));
        return RESULTAT_OK;
}

Resultat reductionoptions_copie(TRAVAIL(ReductionOptions) reductionoptions, TRAVAIL(ReductionOptions) copie)
{
        /* R�alise une copie d'une structure de reduction des options.
         * Renvoie RESULTAT_ERREUR si reductionoptions est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(reductionoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(reductionoptions_destruction(copie));
        SECURISE(reductionoptions_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(reductionoptions, actionreductionoptions), CHAMP_TRAVAIL(copie, actionreductionoptions)));
        SECURISE(option_copie(CHAMP_TRAVAIL(reductionoptions, attente), CHAMP_TRAVAIL(copie, attente)));
        SECURISE(option_copie(CHAMP_TRAVAIL(reductionoptions, global), CHAMP_TRAVAIL(copie, global)));
        SECURISE(pilenommacro_copie(CHAMP_TRAVAIL(reductionoptions, pilenommacro), CHAMP_TRAVAIL(copie, pilenommacro)));
        CHAMP(copie, retardtraitementoptions)=CHAMP(reductionoptions, retardtraitementoptions);
        return RESULTAT_OK;
}

Resultat reductionoptions_destruction(TRAVAIL(ReductionOptions) reductionoptions)
{
        /* D�truit une structure de reduction des options.
         */
        if(S_T(reductionoptions)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(reductionoptions, actionreductionoptions)));
        SECURISE(option_destruction(CHAMP_TRAVAIL(reductionoptions, attente)));
        SECURISE(option_destruction(CHAMP_TRAVAIL(reductionoptions, global)));
        SECURISE(pilenommacro_destruction(CHAMP_TRAVAIL(reductionoptions, pilenommacro)));
        free(S_T(reductionoptions));
        S_T(reductionoptions)=NULL;
        return RESULTAT_OK;
}

