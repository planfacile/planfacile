/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __REDUCTIONOPTIONS__
#define __REDUCTIONOPTIONS__

#include <src/global/global.h>

typedef struct reductionoptions CONTENEUR(ReductionOptions);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/flux/option.h>
#include <src/donnees/flux/macro.h>
#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/analyseur/donnees/pileentier.h>
#include <src/analyseur/donnees/pilechaine.h>
#include <src/donnees/passes/pilenommacro.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

struct reductionoptions
{
        STOCKAGE(ActionFlux) actionreductionoptions;
        //Sp�cifie les actions � r�aliser pour
        //r�duire les options.
        STOCKAGE(Option) attente;
        //Options en attente d'�tre d�finies.
        STOCKAGE(Option) global;
        //Options effectivement d�finies.
        STOCKAGE(PileNomMacro) pilenommacro;
        //Donne les noms des macros utilis�es
        //dans le flux.
        STOCKAGE_SCALAIRE(Booleen) retardtraitementoptions;
        //Permet de retarder les tests concernant les commandes
        //#options.
};
/* Structure utilis�e pour les besoin internes de la
 * r�duction des options. Les champs de cette structure
 * seront utilis�s directement.
 * La structure general doit �tre connue globalement.
 */

Resultat reductionoptions_initialisation(TRAVAIL(ReductionOptions) reductionoptions);
/* Cr�e une structure de reduction des options.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat reductionoptions_reductionoptions(TRAVAIL(General) general);
/* Calcule quelles sont les options qui sont r�ellement
 * d�finies dans le flux, et en d�duit les clauses � conserver
 * dans le flux. L'int�gration des clauses est �galement
 * g�r�e.
 */

Resultat reductionoptions_copie(TRAVAIL(ReductionOptions) reductionoptions, TRAVAIL(ReductionOptions) copie);
/* R�alise une copie d'une structure de reduction des options.
 * Renvoie RESULTAT_ERREUR si reductionoptions est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat reductionoptions_destruction(TRAVAIL(ReductionOptions) reductionoptions);
/* D�truit une structure de reduction des options.
 */

#endif
