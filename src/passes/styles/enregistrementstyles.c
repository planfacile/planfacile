/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "enregistrementstyles.h"

Resultat enregistrementstyles_initialisation(TRAVAIL(EnregistrementStyles) enregistrementstyles)
{
        /* Cr�e un structure d'enregistrement des styles.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION((S_T(enregistrementstyles)=NOUVEAU(EnregistrementStyles))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(enregistrementstyles, actionenregistrement)));
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_section(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une commande de section.
         */
        TRAVAIL(Flux) niveau;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandesection_lecture_niveau(commandesection,R_T(niveau)));
        if(S_T(niveau)==NULL)
        {
                STOCKAGE_SCALAIRE(Booleen) remplacement;
                STOCKAGE_SCALAIRE(Booleen) correct;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_SECTION_DEFAUT)));
                SECURISE(styles_section_definition(CHAMP_TRAVAIL(general, styles),T_S_(NIVEAU_PAR_DEFAUT),commandesection,C_S_(correct),C_S_(remplacement)));
                if(T_S_(remplacement)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandesection_lecture_localisationfichier(commandesection,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_SECTION_DEFAUT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
        }
        else
        {
                STOCKAGE_SCALAIRE(Booleen) remplacement;
                STOCKAGE_SCALAIRE(Booleen) correct;
                TRAVAIL_SCALAIRE(NiveauHierarchique) niveaureel;
                SECURISE(styles_section_recherche_niveau(CHAMP_TRAVAIL(general, styles),niveau,T_S_(VRAI),T_S_(VRAI),C_S_(correct),R_T_(niveaureel)));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandesection_lecture_localisationfichier(commandesection,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_SECTION_NIVEAU_INCORRECT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        return RESULTAT_OK;
                }
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_SECTION_EXPLICITE),niveaureel));
                SECURISE(styles_section_definition(CHAMP_TRAVAIL(general, styles),niveaureel,commandesection,C_S_(correct),C_S_(remplacement)));
                if(T_S_(remplacement)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandesection_lecture_localisationfichier(commandesection,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_SECTION),T_S(localisation),(TRAVAIL_SCALAIRE(Entier))(niveaureel)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
        }
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_reference(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre une commande de reference.
        */
        TRAVAIL(Flux) niveau;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandereference_lecture_niveau(commandereference,R_T(niveau)));
        if(S_T(niveau)==NULL)
        {
                STOCKAGE_SCALAIRE(Booleen) remplacement;
                STOCKAGE_SCALAIRE(Booleen) correct;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_REFERENCE_DEFAUT)));
                SECURISE(styles_reference_definition(CHAMP_TRAVAIL(general, styles),T_S_(NIVEAU_PAR_DEFAUT),commandereference,C_S_(correct),C_S_(remplacement)));
                if(T_S_(remplacement)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandereference_lecture_localisationfichier(commandereference,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_REFERENCE_DEFAUT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
        }
        else
        {
                STOCKAGE_SCALAIRE(Booleen) remplacement;
                STOCKAGE_SCALAIRE(Booleen) correct;
                TRAVAIL_SCALAIRE(NiveauHierarchique) niveaureel;
                SECURISE(styles_section_recherche_niveau(CHAMP_TRAVAIL(general, styles),niveau,T_S_(FAUX),T_S_(VRAI),C_S_(correct),R_T_(niveaureel)));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandereference_lecture_localisationfichier(commandereference,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REFERENCE_NIVEAU_INCORRECT),T_S(localisation)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        return RESULTAT_OK;
                }
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_REFERENCE_EXPLICITE),niveaureel));
                SECURISE(styles_reference_definition(CHAMP_TRAVAIL(general, styles),niveaureel,commandereference,C_S_(correct),C_S_(remplacement)));
                if(T_S_(remplacement)==T_S_(VRAI))
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandereference_lecture_localisationfichier(commandereference,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_REFERENCE),T_S(localisation),(TRAVAIL_SCALAIRE(Entier))(niveaureel)));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                }
        }
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_message(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre le format de message.
         */
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        TRAVAIL(Flux) message;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandemessage_lecture_message(commandemessage,R_T(message)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_MESSAGE)));
        SECURISE(styles_message_definition(CHAMP_TRAVAIL(general, styles),message,C_S_(remplacement)));
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandemessage_lecture_localisationfichier(commandemessage,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_MESSAGE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
        }
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_head(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre l'en-t�te du document.
         */
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        TRAVAIL(Flux) entete;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandehead_lecture_entete(commandehead,R_T(entete)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_ENTETE)));
        SECURISE(styles_entete_definition(CHAMP_TRAVAIL(general, styles),entete,C_S_(remplacement)));
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandehead_lecture_localisationfichier(commandehead,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_ENTETE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
        }
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_foot(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre l'en-t�te du document.
         */
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        TRAVAIL(Flux) pied;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandefoot_lecture_pied(commandefoot,R_T(pied)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_PIED)));
        SECURISE(styles_pied_definition(CHAMP_TRAVAIL(general, styles),pied,C_S_(remplacement)));
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandefoot_lecture_localisationfichier(commandefoot,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_PIED),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
        }
        return RESULTAT_OK;
}

static Resultat enregistrementstyles_start(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre le niveau racine.
         */
        TRAVAIL_SCALAIRE(NiveauHierarchique) racine;
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE_SCALAIRE(Booleen) remplacement;
        TRAVAIL(Flux) niveau;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        SECURISE(commandestart_lecture_niveau(commandestart,R_T(niveau)));
        SECURISE(styles_section_recherche_niveau(CHAMP_TRAVAIL(general, styles),niveau,T_S_(FAUX),T_S_(VRAI),C_S_(correct),R_T_(racine)));
        if(T_S_(correct)==T_S_(FAUX))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandestart_lecture_localisationfichier(commandestart,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_RACINE_NIVEAU_INCORRECT),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                return RESULTAT_OK;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_RACINE),racine));
        SECURISE(styles_racine_definition(CHAMP_TRAVAIL(general, styles),racine,C_S_(remplacement)));
        if(T_S_(remplacement)==T_S_(VRAI))
        {
                STOCKAGE(LocalisationFichier) localisation;
                TRAVAIL(LocalisationFichier) localisationcommande;
                SECURISE(commandestart_lecture_localisationfichier(commandestart,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REDEFINITION_RACINE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
        }
        return RESULTAT_OK;
}

Resultat enregistrementstyles_enregistrementstyles(TRAVAIL(General) general)
{
        /* Enregistre toutes les commandes de style pr�sentes
         * dans les flux.
         * Les formats de section sont d'abord enregistr�s,
         * puis toutes les autres sont enregistr�es, avec
         * la gestion des noms de section.
         */
        STOCKAGE(ActionCommande) actioncommande;
        STOCKAGE(ProcessusFlux) processusflux;
        TRAVAIL_SCALAIRE(Booleen) niveauracine;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES)));
        SECURISE(styles_initialisation(CHAMP_TRAVAIL(general, styles)));
        SECURISE(enregistrementstyles_initialisation(CHAMP_TRAVAIL(general, enregistrementstyles)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_SECTION)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(actioncommande_definition_section(T_S(actioncommande),enregistrementstyles_section,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),general));
        SECURISE(enregistrementstyles_destruction(CHAMP_TRAVAIL(general, enregistrementstyles)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_STYLES_AUTRES)));
        SECURISE(enregistrementstyles_initialisation(CHAMP_TRAVAIL(general, enregistrementstyles)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(actioncommande_definition_reference(T_S(actioncommande),enregistrementstyles_reference,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_message(T_S(actioncommande),enregistrementstyles_message,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_head(T_S(actioncommande),enregistrementstyles_head,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_foot(T_S(actioncommande),enregistrementstyles_foot,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_start(T_S(actioncommande),enregistrementstyles_start,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, enregistrementstyles), actionenregistrement)),general));
        SECURISE(enregistrementstyles_destruction(CHAMP_TRAVAIL(general, enregistrementstyles)));
        SECURISE(styles_correction(CHAMP_TRAVAIL(general, styles),R_T_(niveauracine)));
        if(niveauracine==T_S_(FAUX))
        {
                STOCKAGE(LocalisationFichier) localisation;
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_RACINE_NIVEAU_NON_DEFINI),T_S(localisation)));
        }
        return RESULTAT_OK;
}

Resultat enregistrementstyles_copie(TRAVAIL(EnregistrementStyles) enregistrementstyles, TRAVAIL(EnregistrementStyles) copie)
{
        /* R�alise une copie d'une structure d'enregistrement des styles.
         * Renvoie RESULTAT_ERREUR si enregistrementstyles est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(enregistrementstyles)!=NULL, RESULTAT_ERREUR);

        SECURISE(enregistrementstyles_destruction(copie));
        SECURISE(enregistrementstyles_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(enregistrementstyles, actionenregistrement),CHAMP_TRAVAIL(copie, actionenregistrement)));
        return RESULTAT_OK;
}

Resultat enregistrementstyles_destruction(TRAVAIL(EnregistrementStyles) enregistrementstyles)
{
        /* D�truit une structure d'enregistrement des styles.
         */
        if(S_T(enregistrementstyles)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(enregistrementstyles, actionenregistrement)));
        free(S_T(enregistrementstyles));
        S_T(enregistrementstyles)=NULL;
        return RESULTAT_OK;
}

