/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ENREGISTREMENTSTYLES__
#define __ENREGISTREMENTSTYLES__

#include <src/global/global.h>

typedef struct enregistrementstyles CONTENEUR(EnregistrementStyles);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/commandes/commandesection.h>
#include <src/donnees/commandes/commandereference.h>
#include <src/donnees/commandes/commandemessage.h>
#include <src/donnees/commandes/commandehead.h>
#include <src/donnees/commandes/commandefoot.h>
#include <src/donnees/commandes/commandestart.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

struct enregistrementstyles
{
        STOCKAGE(ActionFlux) actionenregistrement;
        //Cette structure d'action sera utilis�e
        //pour les deux passages sur les flux :
        //1) pour r�cup�rer les sections ;
        //2) pour r�cup�rer les autres.
};
/* Structure utilis�e pour les besoin internes de
 * l'enregistrement des styles. Les champs de cette
 * structure seront utilis�s directement.
 * La structure general doit �tre connue globalement.
 */

Resultat enregistrementstyles_initialisation(TRAVAIL(EnregistrementStyles) enregistrementstyles);
/* Cr�e un structure d'enregistrement des styles.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat enregistrementstyles_enregistrementstyles(TRAVAIL(General) general);
/* Enregistre toutes les commandes de style pr�sentes
 * dans les flux.
 * Les formats de section sont d'abord enregistr�s,
 * puis toutes les autres sont enregistr�es, avec
 * la gestion des noms de section.
 */

Resultat enregistrementstyles_copie(TRAVAIL(EnregistrementStyles) enregistrementstyles, TRAVAIL(EnregistrementStyles) copie);
/* R�alise une copie d'une structure d'enregistrement des styles.
 * Renvoie RESULTAT_ERREUR si enregistrementstyles est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat enregistrementstyles_destruction(TRAVAIL(EnregistrementStyles) enregistrementstyles);
/* D�truit une structure d'enregistrement des styles.
 */

#endif
