/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "sortie.h"

#define INDICEMANQUANTETEXTELONGUEUR    128+1
#define NIVEAU_INCORRECT                -1

static Resultat sortie_renseignement_reference(TRAVAIL(DenominationIdee) denomination, TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique, TRAVAIL(General) general)
{
        /* Renseigne les champs de destination de
         * r�f�rence.
         */
        TRAVAIL_SCALAIRE(TypeIdee) typeidee;
        TRAVAIL_SCALAIRE(IdIdee) idideepresente;
        TRAVAIL_SCALAIRE(Indice) indicemanquante;
        TRAVAIL_SCALAIRE(Chaine) referenceparentegenerique;
        TRAVAIL_SCALAIRE(Chaine) reference;
        TRAVAIL(CommandeIdea) commandeidea;
        TRAVAIL(CommandeMissing) commandemissing;
        TRAVAIL(CommandeGeneric) commandegeneric;
        TRAVAIL(Flux) commandeargument;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL(CommandeSection) commandesection;
        TRAVAIL_SCALAIRE(Chaine) argumenttexte;
        ASSERTION(CHAMP(general, idees)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, styles)!=NULL, RESULTAT_ERREUR);

        SECURISE(denominationidee_lecture(denomination,R_T_(typeidee),R_T_(idideepresente),R_T_(indicemanquante),R_T_(referenceparentegenerique),R_T_(reference)));
        switch(typeidee)
        {
                case T_S_(IDEE_PRESENTE):
                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),idideepresente,R_T(commandeidea)));
                        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference))));
                        SECURISE(commandeidea_lecture_titre(commandeidea,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre))));
                        break;
                case T_S_(IDEE_MANQUANTE):
                        SECURISE(idees_manquantes_lecture(CHAMP_TRAVAIL(general, idees),indicemanquante,C_S_(correct),R_T(commandemissing)));
                        ASSERTION(T_S_(correct)==T_S_(VRAI), RESULTAT_ERREUR);

                        SECURISE(commandemissing_lecture_indice(commandemissing,R_T(commandeargument)));
                        if(S_T(commandeargument)==NULL)
                        {
                                S_T_(argumenttexte)=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(INDICEMANQUANTETEXTELONGUEUR)));
                                ASSERTION(S_T_(argumenttexte)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                SNPRINTF(argumenttexte, T_S_(INDICEMANQUANTETEXTELONGUEUR), T_S_("%d"), indicemanquante);
                        }
                        else
                        {
                                SECURISE(flux_texte(commandeargument,R_T_(argumenttexte)));
                        }
                        CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)=DUP_CAST(Chaine, argumenttexte);
                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        free(S_T_(argumenttexte));
                        SECURISE(commandemissing_lecture_reference(commandemissing,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference))));
                        SECURISE(commandemissing_lecture_titre(commandemissing,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre))));
                        break;
                case T_S_(IDEE_GENERIQUE):
                        SECURISE(idees_generiques_lecture(CHAMP_TRAVAIL(general, idees),referenceparentegenerique,C_S_(correct),R_T(commandegeneric)));
                        ASSERTION(T_S_(correct)==T_S_(VRAI), RESULTAT_ERREUR);

                        SECURISE(commandegeneric_lecture_indice(commandegeneric,R_T(commandeargument)));
                        if(S_T(commandeargument)==NULL)
                        {
                                CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)=DUP_CAST(Chaine, referenceparentegenerique);
                                ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);
                        }
                        else
                        {
                                SECURISE(flux_texte(commandeargument,R_T_(argumenttexte)));
                                CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)=DUP_CAST(Chaine, argumenttexte);
                                ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                free(S_T_(argumenttexte));
                        }
                        SECURISE(commandegeneric_lecture_reference(commandegeneric,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference))));
                        SECURISE(commandegeneric_lecture_titre(commandegeneric,R_T(commandeargument)));
                        SECURISE(flux_copie(commandeargument,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre))));
                        break;
                case T_S_(IDEE_RACINE):
                case T_S_(IDEE_VIDE):
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        SECURISE(styles_section_lecture(CHAMP_TRAVAIL(general, styles),niveauhierarchique,C_S_(correct),R_T(commandesection)));
        if(T_S_(correct)==T_S_(VRAI))
        {
                SECURISE(commandesection_lecture_nom(commandesection,R_T(commandeargument)));
                SECURISE(flux_texte(commandeargument,R_T_(argumenttexte)));
                CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection)=DUP_CAST(Chaine, argumenttexte);
                ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                free(S_T_(argumenttexte));
        }
        CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine)=DUP_CAST(Chaine, reference);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)=S_T_(niveauhierarchique);
        return RESULTAT_OK;
}

static Resultat sortie_suppression_renseignement_reference(TRAVAIL(General) general)
{
        /* Supprime les renseignements cr�es par la fonction
         * pr�c�dente.
         */
        if(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)!=NULL)
        {
                free(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice));
                CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)=NULL;
        }
        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference))));
        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre))));
        if(CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection)!=NULL)
        {
                free(CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection));
                CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection)=NULL;
        }
        CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)=NIVEAU_INCORRECT;
        if(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine)!=NULL)
        {
                free(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine));
                CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine)=NULL;
        }
        return RESULTAT_OK;
}

Resultat sortie_initialisation(TRAVAIL(Sortie) sortie)
{
        /* Cr�e une structure de g�n�ration du document.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(sortie)=NOUVEAU(Sortie))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(sortie, actiongenerationsortie)=NULL;
        CHAMP(sortie, erreur)=NULL;
        CHAMP(sortie, message)=NULL;
        CHAMP(sortie, ideeindice)=NULL;
        CHAMP(sortie, ideereference)=NULL;
        CHAMP(sortie, ideetitre)=NULL;
        CHAMP(sortie, ideetexte)=NULL;
        CHAMP(sortie, typeidee)=IDEE_VIDE;
        CHAMP(sortie, ideereferencesidee)=NULL;
        CHAMP(sortie, referenceindice)=NULL;
        CHAMP(sortie, referencereference)=NULL;
        CHAMP(sortie, referencereferencechaine)=NULL;
        CHAMP(sortie, referencetitre)=NULL;
        CHAMP(sortie, referencesection)=NULL;
        CHAMP(sortie, niveaureferencesection)=NIVEAU_INCORRECT;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_mesg(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace la commande mesg par sa valeur.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), message)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), message)),T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_commentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace un commentaire par sa valeur.
         */
        TRAVAIL(Flux) commentaire;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL(Options) options;
        TRAVAIL_SCALAIRE(Booleen) option;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(commandecommentaire_lecture_commentaire(commandecommentaire,R_T(commentaire)));
        SECURISE(flux_copie(commentaire,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), message))));
        SECURISE(styles_message_lecture(CHAMP_TRAVAIL(general, styles),C_S_(correct),R_T(commentaire)));
        SECURISE(environnement_lecture_options(CHAMP_TRAVAIL(general, environnement),R_T(options)));
        SECURISE(options_lecture_commentaires(options,R_T_(option)));
        if((T_S_(correct)==T_S_(VRAI))&&(option==T_S_(VRAI)))
        {
                SECURISE(flux_copie(commentaire,T_S(flux_equivalent)));
                SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        }
        else
        {
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
        }
        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), message))));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_section_ref(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une r�f�rence d'id�e dans un format de section.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference)),T_S(flux_equivalent)));
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_section_title(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une r�f�rence d'id�e dans un format de section.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre)),T_S(flux_equivalent)));
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_section_txt(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une r�f�rence d'id�e dans un format de section.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte)),T_S(flux_equivalent)));
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_reference_ref(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une r�f�rence dans un format de r�f�rence.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereference)),T_S(flux_equivalent)));
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_reference_title(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace un titre d'id�e dans un format de r�f�rence.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(flux_copie(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), referencetitre)),T_S(flux_equivalent)));
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_reference_sec(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace un nom de section d'id�e dans un format de r�f�rence.
         */
        TRAVAIL(Flux) niveau;
        STOCKAGE(CommandeTexte) commandetexte;
        STOCKAGE(Commande) commande;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL(CommandeSection) commandesection;
        TRAVAIL(Flux) fluxsection;
        TRAVAIL_SCALAIRE(Chaine) nomsection;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, styles)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(commandesec_lecture_niveau(commandesec,R_T(niveau)));
        if(S_T(niveau)==NULL)
        {
                if(CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection)!=NULL)
                {
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_REFERENCE_FORMAT)));
                        SECURISE(commande_initialisation(T_S(commande)));
                        SECURISE(commandetexte_initialisation(T_S(commandetexte)));
                        SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
                        SECURISE(commandetexte_definition_localisationfichier(T_S(commandetexte),localisationcommande));
                        SECURISE(commandetexte_definition_texte(T_S(commandetexte),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), referencesection))));
                        SECURISE(commande_definition_texte(T_S(commande),T_S(commandetexte)));
                        SECURISE(flux_ajout_commande(T_S(flux_equivalent),T_S(commande),general));
                        SECURISE(commande_destruction(T_S(commande)));
                        SECURISE(commandetexte_destruction(T_S(commandetexte)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                else
                {
                        SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        ASSERTION(T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection))!=T_S_(NIVEAU_INCORRECT), RESULTAT_ERREUR);

                        CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(INDICEMANQUANTETEXTELONGUEUR)));
                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        SNPRINTF(T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)), T_S_(INDICEMANQUANTETEXTELONGUEUR), T_S_("%d"),
                                T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_NOM_NIVEAU_INCORRECT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
        }
        else
        {
                SECURISE(styles_section_recherche_niveau(CHAMP_TRAVAIL(general, styles),T_S_(niveau),T_S_(FAUX),T_S_(FAUX),C_S_(correct),R_T_(niveauhierarchique)));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        localisation=FAUX;
                        SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(flux_texte(niveau,R_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_NOM_NIVEAU_INCORRECT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        free(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur));
                        CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=NULL;
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                SECURISE(styles_section_lecture(CHAMP_TRAVAIL(general, styles),niveauhierarchique,C_S_(correct),R_T(commandesection)));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        localisation=FAUX;
                        SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(flux_texte(niveau,R_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_NOM_NIVEAU_INCORRECT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        free(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur));
                        CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=NULL;
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_REFERENCE_FORMAT)));
                SECURISE(commande_initialisation(T_S(commande)));
                SECURISE(commandetexte_initialisation(T_S(commandetexte)));
                SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
                SECURISE(commandetexte_definition_localisationfichier(T_S(commandetexte),localisationcommande));
                SECURISE(commandesection_lecture_nom(commandesection,R_T(fluxsection)));
                SECURISE(flux_texte(fluxsection,R_T_(nomsection)));
                SECURISE(commandetexte_definition_texte(T_S(commandetexte),nomsection));
                SECURISE(commande_definition_texte(T_S(commande),T_S(commandetexte)));
                free(S_T_(nomsection));
                SECURISE(flux_ajout_commande(T_S(flux_equivalent),T_S(commande),general));
                SECURISE(commande_destruction(T_S(commande)));
                SECURISE(commandetexte_destruction(T_S(commandetexte)));
        }
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_dep_depref(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une commande #depref par la r�f�rence
         * qu'elle repr�sente.
         */
        TRAVAIL(Flux) reference;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL(CommandeReference) commandereference;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, styles)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_DEPREF)));
        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(commandedepref_lecture_format(commandedepref,R_T(reference)));
        if(S_T(reference)==NULL)
        {
                SECURISE(styles_reference_lecture(CHAMP_TRAVAIL(general, styles),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)),C_S_(correct),R_T(commandereference)));
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandereference_lecture_format(commandereference,R_T(reference)));
                        SECURISE(flux_copie(reference,T_S(flux_equivalent)));
                }
                else
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(commandedepref_lecture_localisationfichier(commandedepref,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REFERENCE_STYLE_MANQUANT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine))));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_REFERENCE_FORMAT)));
                }
        }
        else
        {
                SECURISE(flux_copie(reference,T_S(flux_equivalent)));
        }
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_texte_dep(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace la commande de d�pendance par sa valeur.
         */
        TRAVAIL(Flux) destination;
        TRAVAIL_SCALAIRE(Chaine) destinationtexte;
        TRAVAIL_SCALAIRE(Chaine) referenceidee;
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE_SCALAIRE(Booleen) recherche;
        TRAVAIL_SCALAIRE(TypeReference) typereference;
        STOCKAGE(DenominationIdee) denomination;
        TRAVAIL(DenominationIdee) denominationidee;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL(Flux) reference;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, idees)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        SECURISE(commandedep_lecture_destination(commandedep,R_T(destination)));
        SECURISE(flux_texte(destination,R_T_(destinationtexte)));
        if(STRLEN(destinationtexte)==T_S_(0))
        {
                free(S_T_(destinationtexte));
                SECURISE(commandedep_lecture_localisationfichier(commandedep,R_T(localisationcommande)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_DEPENDANCE_VIDE),T_S(localisation)));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                return RESULTAT_OK;
        }
        SECURISE(referencesidee_recherche(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)),CHAMP_TRAVAIL(general, idees),destinationtexte,C_S_(recherche),R_T_(typereference),R_T(denominationidee),R_T_(niveauhierarchique)));
        if(T_S_(recherche)==T_S_(FAUX))
        {
                SECURISE(flux_texte(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference)),R_T_(referenceidee)));
                denomination=NULL;
                SECURISE(arbreidees_recherche_reference(CHAMP_TRAVAIL(general, arbreidees),destinationtexte,C_S_(correct),T_S(denomination),R_T_(niveauhierarchique),referenceidee,T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), typeidee)),R_T_(typereference)));
                free(S_T_(referenceidee));
                if(T_S_(correct)==T_S_(FAUX))
                {
                        SECURISE(commandedep_lecture_localisationfichier(commandedep,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=DUP_CAST(Chaine, destinationtexte);
                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                        free(S_T_(destinationtexte));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_DEPENDANCE_INCORRECTE),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                        free(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur));
                        CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=NULL;
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                denominationidee=T_S(denomination);
        }
        SECURISE(sortie_renseignement_reference(denominationidee,niveauhierarchique,general));
        if(T_S_(recherche)==T_S_(FAUX))
        {
                SECURISE(denominationidee_destruction(denominationidee));
        }
        switch(typereference)
        {
                case T_S_(REFERENCE_IRREDUCTIBLE):
                        if(S_T_(destinationtexte)==NULL)
                        {
                                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_REFIRRVIDE)));
                        }
                        else
                        {
                                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_REFIRR),destinationtexte));
                        }
                        SECURISE(commandedep_lecture_irreductible(commandedep,R_T(reference)));
                        SECURISE(flux_copie(reference,T_S(flux_equivalent)));
                        break;
                case T_S_(REFERENCE_REDUCTIBLE):
                        if(S_T_(destinationtexte)==NULL)
                        {
                                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_REFREDVIDE)));
                        }
                        else
                        {
                                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_REFRED),destinationtexte));
                        }
                        SECURISE(commandedep_lecture_reductible(commandedep,R_T(reference)));
                        SECURISE(flux_copie(reference,T_S(flux_equivalent)));
                        break;
                default:
                        return RESULTAT_ERREUR_DOMAINE;
                        break;
        }
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        SECURISE(sortie_suppression_renseignement_reference(general));
        free(S_T_(destinationtexte));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_texte_extref(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace une commande #extref par la r�f�rence qu'elle repr�sente.
         */
        TRAVAIL(Flux) fluxindice;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL_SCALAIRE(Indice) indice;
        TRAVAIL_SCALAIRE(Taille) taille;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL_SCALAIRE(TypeReference) typereference;
        TRAVAIL(DenominationIdee) denomination;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique;
        TRAVAIL(Flux) reference;
        TRAVAIL(CommandeReference) commandereference;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        SECURISE(commandeextref_lecture_indice(commandeextref,R_T(fluxindice)));
        SECURISE(flux_nombre(fluxindice,C_S_(correct),R_T_(indice)));
        if(T_S_(correct)==T_S_(FAUX))
        {
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(commandeextref_lecture_localisationfichier(commandeextref,R_T(localisationcommande)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(flux_texte(fluxindice,R_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_INDICE_REFERENCE_INCORRECT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                free(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur));
                CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=NULL;
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                return RESULTAT_OK;
        }
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_EXTREF),indice));
        indice--;//Les indices de la commande #extref commencent � 1.
        SECURISE(referencesidee_taille(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)),R_T_(taille)));
        if((indice<T_S_(0))||(indice>=taille))
        {
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(commandeextref_lecture_localisationfichier(commandeextref,R_T(localisationcommande)));
                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                SECURISE(flux_texte(fluxindice,R_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_INDICE_REFERENCE_INCORRECT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur))));
                free(CHAMP_STOCKAGE(CHAMP(general, sortie), erreur));
                CHAMP_STOCKAGE(CHAMP(general, sortie), erreur)=NULL;
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                return RESULTAT_OK;
        }
        SECURISE(referencesidee_lecture(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)),indice,R_T_(typereference),R_T(denomination),R_T_(niveauhierarchique)));
        SECURISE(sortie_renseignement_reference(denomination,niveauhierarchique,general));
        SECURISE(commandeextref_lecture_format(commandeextref,R_T(reference)));
        if(S_T(reference)==NULL)
        {
                SECURISE(styles_reference_lecture(CHAMP_TRAVAIL(general, styles),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)),C_S_(correct),R_T(commandereference)));
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandereference_lecture_format(commandereference,R_T(reference)));
                        flux_equivalent=NULL;
                        SECURISE(flux_copie(reference,T_S(flux_equivalent)));
                }
                else
                {
                        SECURISE(commandeextref_lecture_localisationfichier(commandeextref,R_T(localisationcommande)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REFERENCE_STYLE_MANQUANT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine))));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_REFERENCE_FORMAT)));
                }
        }
        else
        {
                flux_equivalent=NULL;
                SECURISE(flux_copie(reference,T_S(flux_equivalent)));
        }
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        SECURISE(sortie_suppression_renseignement_reference(general));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_remplacement_texte_extrefs(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace la commande #extrefs par les r�f�rences.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        TRAVAIL(Flux) reference;
        STOCKAGE(Flux) referencetravail;
        TRAVAIL_SCALAIRE(Taille) taille;
        STOCKAGE_SCALAIRE(Booleen) correct;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommande;
        TRAVAIL_SCALAIRE(TypeReference) typereference;
        TRAVAIL(DenominationIdee) denomination;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique;
        TRAVAIL(CommandeReference) commandereference;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_EXTREFS)));
        S_C_(arretcontexte)=VRAI;
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_REFERENCE_FORMAT)));
        SECURISE(referencesidee_taille(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)),R_T_(taille)));
        for(indice=0 ; T_S_(indice)<taille ; T_S_(indice)++)
        {
                referencetravail=NULL;
                SECURISE(referencesidee_lecture(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee)),T_S_(indice),R_T_(typereference),R_T(denomination),R_T_(niveauhierarchique)));
                SECURISE(sortie_renseignement_reference(denomination,niveauhierarchique,general));
                SECURISE(commandeextrefs_lecture_format(commandeextrefs,R_T(reference)));
                if(S_T(reference)==NULL)
                {
                        SECURISE(styles_reference_lecture(CHAMP_TRAVAIL(general, styles),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), niveaureferencesection)),C_S_(correct),R_T(commandereference)));
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                SECURISE(commandereference_lecture_format(commandereference,R_T(reference)));
                                SECURISE(flux_copie(reference,T_S(referencetravail)));
                        }
                        else
                        {
                                SECURISE(commandeextrefs_lecture_localisationfichier(commandeextrefs,R_T(localisationcommande)));
                                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_REFERENCE_STYLE_MANQUANT),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), referencereferencechaine))));
                                SECURISE(localisationfichier_destruction(T_S(localisation)));
                                SECURISE(flux_initialisation(T_S(referencetravail)));
                                SECURISE(flux_definition_type(T_S(referencetravail),T_S_(FLUX_REFERENCE_FORMAT)));
                        }
                }
                else
                {
                        SECURISE(flux_copie(reference,T_S(referencetravail)));
                }
                SECURISE(flux_parcours(T_S(referencetravail),actionflux,general));
                SECURISE(flux_concatenation(T_S(flux_equivalent),T_S(referencetravail),general));
                SECURISE(flux_destruction(T_S(referencetravail)));
                SECURISE(sortie_suppression_renseignement_reference(general));
        }
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}
static Resultat sortie_remplacement_automatique_index(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace un indice dans une r�f�rence automatique.
         */
        STOCKAGE(CommandeTexte) commandetexte;
        STOCKAGE(Commande) commande;
        TRAVAIL(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        SECURISE(commandetexte_initialisation(T_S(commandetexte)));
        SECURISE(commandeindex_lecture_localisationfichier(commandeindex,R_T(localisation)));
        SECURISE(commandetexte_definition_localisationfichier(T_S(commandetexte),localisation));
        if(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice)!=NULL)
        {//on est dans une r�f�rence, donc, on privil�gie la r�f�rence.
                SECURISE(commandetexte_definition_texte(T_S(commandetexte),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), referenceindice))));
        }
        else
        {
                ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL,  RESULTAT_ERREUR);

                SECURISE(commandetexte_definition_texte(T_S(commandetexte),T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice))));
        }
        SECURISE(commande_initialisation(T_S(commande)));
        SECURISE(commande_definition_texte(T_S(commande),T_S(commandetexte)));
        SECURISE(flux_ajout_commande(T_S(flux_equivalent),T_S(commande),general));
        SECURISE(commande_destruction(T_S(commande)));
        SECURISE(commandetexte_destruction(T_S(commandetexte)));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_parcours_idees(TRAVAIL(ArbreIdees) arbreidees, TRAVAIL(General) general)
{
        /* Parcours des id�es dans l'ordre sp�cifi� par le plan.
         * Toutes les valeurs n�c�ssaires sont puis�es dans la
         * structure general.
         */
        TRAVAIL(DenominationIdee) denomination;
        TRAVAIL_SCALAIRE(TypeIdee) typeidee;
        TRAVAIL_SCALAIRE(IdIdee) idideepresente;
        TRAVAIL_SCALAIRE(Indice) indicemanquante;
        TRAVAIL_SCALAIRE(Chaine) referencegenerique;
        TRAVAIL_SCALAIRE(Chaine) reference;
        TRAVAIL_SCALAIRE(NiveauHierarchique) niveauhierarchique;
        TRAVAIL(CommandeSection) commandesection;
        TRAVAIL(CommandeIdea) commandeidea;
        TRAVAIL(CommandeMissing) commandemissing;
        TRAVAIL(CommandeGeneric) commandegeneric;
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL(Flux) section;
        STOCKAGE(Flux) sectionidee;
        TRAVAIL(Flux) fluxidee;
        TRAVAIL_SCALAIRE(Taille) nombresoussections;
        STOCKAGE_SCALAIRE(Indice) indicesoussection;
        if(S_T(arbreidees)==NULL)
                return RESULTAT_OK;
        SECURISE(arbreidees_lecture_denomination(arbreidees,R_T(denomination)));
        SECURISE(denominationidee_lecture(denomination,R_T_(typeidee),R_T_(idideepresente),R_T_(indicemanquante),R_T_(referencegenerique),R_T_(reference)));
        CHAMP_STOCKAGE(CHAMP(general, sortie), typeidee)=S_T_(typeidee);
        ASSERTION(typeidee!=T_S_(IDEE_VIDE), RESULTAT_ERREUR_DOMAINE);

        if(S_T_(reference)==NULL)
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_IDEEVIDE)));
        }
        else
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_IDEE),reference));
        }
        SECURISE(arbreidees_lecture_niveau(arbreidees,R_T_(niveauhierarchique)));
        if(typeidee!=T_S_(IDEE_RACINE))
        {
                SECURISE(styles_section_lecture(CHAMP_TRAVAIL(general, styles),niveauhierarchique,C_S_(correct),R_T(commandesection)));
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandesection_lecture_formatavant(commandesection,R_T(section)));
                        sectionidee=NULL;
                        SECURISE(flux_copie(section,T_S(sectionidee)));
                        switch(typeidee)
                        {
                                case T_S_(IDEE_PRESENTE):
                                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),idideepresente,R_T(commandeidea)));
                                        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandeidea_lecture_titre(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandeidea_lecture_texte(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        correct=VRAI;
                                        break;
                                case T_S_(IDEE_MANQUANTE):
                                        SECURISE(idees_manquantes_lecture(CHAMP_TRAVAIL(general, idees),indicemanquante,C_S_(correct),R_T(commandemissing)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                        {
                                                STOCKAGE(LocalisationFichier) localisation;
                                                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_MANQUANTE_MANQUANTE),T_S(localisation),indicemanquante));
                                                break;
                                        }
                                        CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(INDICEMANQUANTETEXTELONGUEUR)));
                                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                        SNPRINTF(T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)), T_S_(INDICEMANQUANTETEXTELONGUEUR), T_S_("%d"), indicemanquante);
                                        SECURISE(commandemissing_lecture_reference(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandemissing_lecture_titre(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandemissing_lecture_texte(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        break;
                                case T_S_(IDEE_GENERIQUE):
                                        SECURISE(idees_generiques_lecture(CHAMP_TRAVAIL(general, idees),referencegenerique,C_S_(correct),R_T(commandegeneric)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                        {
                                                STOCKAGE(LocalisationFichier) localisation;
                                                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_IDEE_GENERIQUE_MANQUANTE),T_S(localisation),referencegenerique));
                                                break;
                                        }
                                        CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=DUP_CAST(Chaine, referencegenerique);
                                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                        SECURISE(commandegeneric_lecture_reference(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandegeneric_lecture_titre(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandegeneric_lecture_texte(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        break;
                                default:
                                        return RESULTAT_ERREUR_DOMAINE;
                                        break;
                        }
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                TRAVAIL(ReferencesIdee) referencesidee;
                                SECURISE(arbreidees_lecture_references(arbreidees,R_T(referencesidee)));
                                SECURISE(referencesidee_copie(referencesidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee))));
                                SECURISE(flux_parcours(T_S(sectionidee),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
                                SECURISE(referencesidee_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee))));
                                SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),T_S(sectionidee),general));
                        }
                        SECURISE(flux_destruction(T_S(sectionidee)));
                        if(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL)
                        {
                                free(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice));
                                CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=NULL;
                        }
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                }
                else
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        TRAVAIL(LocalisationFichier) localisationcommande;
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        switch(typeidee)
                        {
                                case T_S_(IDEE_PRESENTE):
                                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),idideepresente,R_T(commandeidea)));
                                        SECURISE(commandeidea_lecture_localisationfichier(commandeidea,R_T(localisationcommande)));
                                        correct=VRAI;
                                        break;
                                case T_S_(IDEE_MANQUANTE):
                                        SECURISE(idees_manquantes_lecture(CHAMP_TRAVAIL(general, idees),indicemanquante,C_S_(correct),R_T(commandemissing)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                                break;//cas o� le format est non d�fini sur une id�e qui n'existe pas !
                                        SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
                                        break;
                                case T_S_(IDEE_GENERIQUE):
                                        SECURISE(idees_generiques_lecture(CHAMP_TRAVAIL(general, idees),referencegenerique,C_S_(correct),R_T(commandegeneric)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                                break;//cas o� le format est non d�fini sur une id�e qui n'existe pas !
                                        SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
                                        break;
                                default:
                                        return RESULTAT_ERREUR_DOMAINE;
                                        break;
                        }
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_SECTION_STYLE_MANQUANT),T_S(localisation),reference));
                                SECURISE(localisationfichier_destruction(T_S(localisation)));
                        }
                }
        }
        CHAMP_STOCKAGE(CHAMP(general, sortie), typeidee)=IDEE_VIDE;
        SECURISE(arbreidees_lecture_nombresoussections(arbreidees,R_T_(nombresoussections)));
        if(nombresoussections>T_S_(0))
        {
                TRAVAIL(Flux) presection;
                TRAVAIL(Flux) postsection;
                TRAVAIL(ArbreIdees) soussection;
                SECURISE(styles_section_lecture(CHAMP_TRAVAIL(general, styles),niveauhierarchique+T_S_(1),C_S_(correct),R_T(commandesection)));
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandesection_lecture_presection(commandesection,R_T(presection)));
                        SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),presection,general));
                }
                for(indicesoussection=0 ; T_S_(indicesoussection)<nombresoussections ; T_S_(indicesoussection)++)
                {
                        SECURISE(arbreidees_lecture_soussection(arbreidees,T_S_(indicesoussection),R_T(soussection)));
                        SECURISE(sortie_parcours_idees(soussection,general));
                }
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandesection_lecture_postsection(commandesection,R_T(postsection)));
                        SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),postsection,general));
                }
        }
        SECURISE(arbreidees_lecture_denomination(arbreidees,R_T(denomination)));
        SECURISE(denominationidee_lecture(denomination,R_T_(typeidee),R_T_(idideepresente),R_T_(indicemanquante),R_T_(referencegenerique),R_T_(reference)));
        SECURISE(arbreidees_lecture_niveau(arbreidees,R_T_(niveauhierarchique)));
        if(typeidee!=T_S_(IDEE_RACINE))
        {
                SECURISE(styles_section_lecture(CHAMP_TRAVAIL(general, styles),niveauhierarchique,C_S_(correct),R_T(commandesection)));
                if(T_S_(correct)==T_S_(VRAI))
                {
                        SECURISE(commandesection_lecture_formatapres(commandesection,R_T(section)));
                        sectionidee=NULL;
                        SECURISE(flux_copie(section,T_S(sectionidee)));
                        switch(typeidee)
                        {
                                case T_S_(IDEE_PRESENTE):
                                        SECURISE(idees_idees_lecture_idee(CHAMP_TRAVAIL(general, idees),idideepresente,R_T(commandeidea)));
                                        SECURISE(commandeidea_lecture_reference(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandeidea_lecture_titre(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandeidea_lecture_texte(commandeidea,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        correct=VRAI;
                                        break;
                                case T_S_(IDEE_MANQUANTE):
                                        SECURISE(idees_manquantes_lecture(CHAMP_TRAVAIL(general, idees),indicemanquante,C_S_(correct),R_T(commandemissing)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                                break;
                                        CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=CAST_S_(Chaine, ALLOCATION_N_(Caractere, T_S_(INDICEMANQUANTETEXTELONGUEUR)));
                                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                        SNPRINTF(T_S_(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)), T_S_(INDICEMANQUANTETEXTELONGUEUR), T_S_("%d"), indicemanquante);
                                        SECURISE(commandemissing_lecture_reference(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandemissing_lecture_titre(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandemissing_lecture_texte(commandemissing,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        break;
                                case T_S_(IDEE_GENERIQUE):
                                        SECURISE(idees_generiques_lecture(CHAMP_TRAVAIL(general, idees),referencegenerique,C_S_(correct),R_T(commandegeneric)));
                                        if(T_S_(correct)==T_S_(FAUX))
                                                break;
                                        CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=DUP_CAST(Chaine, referencegenerique);
                                        ASSERTION(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                        SECURISE(commandegeneric_lecture_reference(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                                        SECURISE(commandegeneric_lecture_titre(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                                        SECURISE(commandegeneric_lecture_texte(commandegeneric,R_T(fluxidee)));
                                        SECURISE(flux_copie(fluxidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                                        break;
                                default:
                                        return RESULTAT_ERREUR_DOMAINE;
                                        break;
                        }
                        if(T_S_(correct)==T_S_(VRAI))
                        {
                                TRAVAIL(ReferencesIdee) referencesidee;
                                SECURISE(arbreidees_lecture_references(arbreidees,R_T(referencesidee)));
                                SECURISE(referencesidee_copie(referencesidee,T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee))));
                                SECURISE(flux_parcours(T_S(sectionidee),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
                                SECURISE(referencesidee_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereferencesidee))));
                                SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),T_S(sectionidee),general));
                        }
                        SECURISE(flux_destruction(T_S(sectionidee)));
                        if(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)!=NULL)
                        {
                                free(CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice));
                                CHAMP_STOCKAGE(CHAMP(general, sortie), ideeindice)=NULL;
                        }
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideereference))));
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetitre))));
                        SECURISE(flux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), ideetexte))));
                }
        }
        return RESULTAT_OK;
}

static Resultat sortie_generation_echappement(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* G�n�re un caract�re d'echapement dans le fichier final.
         */
        TRAVAIL_SCALAIRE(Caractere) caractere;
        TRAVAIL_SCALAIRE(DescripteurFichier) sortie;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_sortie(CHAMP_TRAVAIL(general, environnement),R_T_(sortie)));
        SECURISE(commandeechappement_lecture_caractere(commandeechappement,R_T_(caractere)));
        fprintf(sortie,"%c",(char)(caractere));
        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}        

static Resultat sortie_generation_texte(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* G�n�re une portion du texte dans le fichier final.
         */
        TRAVAIL_SCALAIRE(Chaine) texte;
        TRAVAIL_SCALAIRE(DescripteurFichier) sortie;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_sortie(CHAMP_TRAVAIL(general, environnement),R_T_(sortie)));
        SECURISE(commandetexte_lecture_texte(commandetexte,R_T_(texte)));
        fprintf(sortie,"%s",(char*)(texte));
        S_C_(arretcontexte)=VRAI;
        flux_equivalent=NULL;
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}        

static Resultat sortie_generation_mesg(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* G�n�re le message de statistiques du document.
         */
        TRAVAIL_SCALAIRE(DescripteurFichier) sortie;
        STOCKAGE(MessageParametres) messageparametres;
        TRAVAIL_SCALAIRE(Indice) indice;
        STOCKAGE(Flux) flux_equivalent;
        TRAVAIL_SCALAIRE(Chaine) date;
        TRAVAIL_SCALAIRE(Chaine) heure;
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, performancesplan)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        SECURISE(environnement_lecture_sortie(CHAMP_TRAVAIL(general, environnement),R_T_(sortie)));
        SECURISE(messageparametres_initialisation(T_S(messageparametres)));
        SECURISE(messageparametres_ajout_chaine(T_S(messageparametres), T_S_(PROJECT_VERSION)));
        SECURISE(aideenvironnement_lecture_date(R_T_(date),R_T_(heure)));
        SECURISE(messageparametres_ajout_chaine(T_S(messageparametres),date));
        SECURISE(messageparametres_ajout_chaine(T_S(messageparametres),heure));
        free(S_T_(date));
        free(S_T_(heure));
        SECURISE(performancesplan_lecture_nombreideestotal(CHAMP_TRAVAIL(general, performancesplan),R_T_(indice)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres),indice));
        SECURISE(performancesplan_lecture_nombreideesmanquantes(CHAMP_TRAVAIL(general, performancesplan),R_T_(indice)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres),indice));
        SECURISE(performancesplan_lecture_nombreideesgeneralites(CHAMP_TRAVAIL(general, performancesplan),R_T_(indice)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres),indice));
        SECURISE(performancesplan_lecture_nombrereferencestotal(CHAMP_TRAVAIL(general, performancesplan),R_T_(indice)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres),indice));
        SECURISE(performancesplan_lecture_nombrereferencesirreductibles(CHAMP_TRAVAIL(general, performancesplan),R_T_(indice)));
        SECURISE(messageparametres_ajout_entier(T_S(messageparametres),indice));
        SECURISE(messages_message(T_S(CHAMP(general, environnement)), sortie, T_S_(MESSAGE_STATISTIQUES), T_S(messageparametres)));
        SECURISE(messageparametres_destruction(T_S(messageparametres)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat sortie_generation_commentaire_erreur(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_define_erreur(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_dep_erreur(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_depref_erreur(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_echappement_erreur(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_error_erreur(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_extref_erreur(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_extrefs_erreur(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_foot_erreur(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_generic_erreur(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_head_erreur(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_idea_erreur(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_include_erreur(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_index_erreur(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_macro_erreur(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_mesg_erreur(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_message_erreur(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_missing_erreur(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_option_erreur(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_options_erreur(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_parametre_erreur(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_reference_erreur(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_ref_erreur(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_sec_erreur(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_section_erreur(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_standard_erreur(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_start_erreur(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_texte_erreur(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_title_erreur(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_txt_erreur(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

static Resultat sortie_generation_warning_erreur(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Signale une erreur de g�n�ration de texte.
         */
        return RESULTAT_ERREUR;
}

Resultat sortie_sortie(TRAVAIL(General) general)
{
        /* G�n�re la sortie � partir des informations
         * contenues dans general.
         */
        STOCKAGE_SCALAIRE(Booleen) correct;
        TRAVAIL(Flux) document;
        TRAVAIL(Flux) fluxmessage;
        STOCKAGE(Flux) fluxstatistiques;
        STOCKAGE(ActionCommande) actioncommande;
        STOCKAGE(ProcessusFlux) processusflux;
        TRAVAIL_SCALAIRE(NomFichier) nomfichier;
        STOCKAGE_SCALAIRE(DescripteurFichier) sortie;
        STOCKAGE_SCALAIRE(struct stat) infosfichier;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, environnement)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, styles)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE)));
        SECURISE(sortie_initialisation(CHAMP_TRAVAIL(general, sortie)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(general, principal)));
        SECURISE(flux_initialisation(CHAMP_TRAVAIL(general, principal)));
        SECURISE(flux_definition_type(CHAMP_TRAVAIL(general, principal),T_S_(FLUX_PRINCIPAL)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION)));
        SECURISE(actionflux_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_ref(T_S(actioncommande),sortie_remplacement_section_ref,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_title(T_S(actioncommande),sortie_remplacement_section_title,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_txt(T_S(actioncommande),sortie_remplacement_section_txt,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_index(T_S(actioncommande),sortie_remplacement_automatique_index,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_reference_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_mesg(T_S(actioncommande),sortie_remplacement_mesg,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_dep(T_S(actioncommande),sortie_remplacement_texte_dep,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_depref(T_S(actioncommande),sortie_remplacement_dep_depref,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_ref(T_S(actioncommande),sortie_remplacement_reference_ref,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_title(T_S(actioncommande),sortie_remplacement_reference_title,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_sec(T_S(actioncommande),sortie_remplacement_reference_sec,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_remplacement_commentaire,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_extref(T_S(actioncommande),sortie_remplacement_texte_extref,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_extrefs(T_S(actioncommande),sortie_remplacement_texte_extrefs,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(styles_entete_lecture(CHAMP_TRAVAIL(general, styles),C_S_(correct),R_T(document)));
        if(T_S_(correct)==T_S_(VRAI))
        {
                STOCKAGE(Flux) entete;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_ENTETE)));
                entete=NULL;
                SECURISE(flux_copie(document,T_S(entete)));
                SECURISE(flux_parcours(T_S(entete),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
                SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),T_S(entete),general));
                SECURISE(flux_destruction(T_S(entete)));
        }
        SECURISE(sortie_parcours_idees(CHAMP_TRAVAIL(general, arbreidees),general));
        SECURISE(styles_pied_lecture(CHAMP_TRAVAIL(general, styles),C_S_(correct),R_T(document)));
        if(T_S_(correct)==T_S_(VRAI))
        {
                STOCKAGE(Flux) pied;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_GENERATION_PIED)));
                pied=NULL;
                SECURISE(flux_copie(document,T_S(pied)));
                SECURISE(flux_parcours(T_S(pied),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
                SECURISE(flux_concatenation(CHAMP_TRAVAIL(general, principal),T_S(pied),general));
                SECURISE(flux_destruction(T_S(pied)));
        }
        SECURISE(actionflux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
        SECURISE(sortie_destruction(CHAMP_TRAVAIL(general, sortie)));
        SECURISE(sortie_initialisation(CHAMP_TRAVAIL(general, sortie)));
        //Sortie du document dans le fichier.
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_PRODUCTION)));
        SECURISE(environnement_lecture_fichiersortie(CHAMP_TRAVAIL(general, environnement),R_T_(nomfichier)));
        if(nomfichier!=T_S_(NULL))
        {
                if(STAT(nomfichier, C_S_(infosfichier))==T_S_(0))
                {
                        if((S_ISDIR(CHAMP_STOCKAGE_(infosfichier, st_mode)))||(S_ISBLK(CHAMP_STOCKAGE_(infosfichier, st_mode))))
                        {
                                /* Le nom de fichier d�signe un r�pertoire ou un fichier de type block.
                                */
                                STOCKAGE(LocalisationFichier) localisation;
                                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_OUVERTURE_SORTIE),T_S(localisation),nomfichier));
                                return RESULTAT_ERREUR;
                        }
                }
                if((sortie=FOPEN(nomfichier, T_S_("w")))==NULL)
                {
                        STOCKAGE(LocalisationFichier) localisation;
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_OUVERTURE_SORTIE),T_S(localisation),nomfichier));
                        return RESULTAT_ERREUR;
                }
                SECURISE(environnement_definition_sortie(CHAMP_TRAVAIL(general, environnement),sortie));
        }
        else
        {
                SECURISE(environnement_definition_sortie(CHAMP_TRAVAIL(general, environnement),T_S_(SORTIESTANDARD)));
        }
        //Document.
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_PRODUCTION_DOCUMENT)));
        SECURISE(actionflux_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(actioncommande_definition_echappement(T_S(actioncommande),sortie_generation_echappement,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_texte(T_S(actioncommande),sortie_generation_texte,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_commentaire(T_S(actioncommande),sortie_generation_commentaire_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_define(T_S(actioncommande),sortie_generation_define_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_dep(T_S(actioncommande),sortie_generation_dep_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_depref(T_S(actioncommande),sortie_generation_depref_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_error(T_S(actioncommande),sortie_generation_error_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_extref(T_S(actioncommande),sortie_generation_extref_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_extrefs(T_S(actioncommande),sortie_generation_extrefs_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_foot(T_S(actioncommande),sortie_generation_foot_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_generic(T_S(actioncommande),sortie_generation_generic_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_head(T_S(actioncommande),sortie_generation_head_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_idea(T_S(actioncommande),sortie_generation_idea_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_include(T_S(actioncommande),sortie_generation_include_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_index(T_S(actioncommande),sortie_generation_index_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_macro(T_S(actioncommande),sortie_generation_macro_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_mesg(T_S(actioncommande),sortie_generation_mesg_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_message(T_S(actioncommande),sortie_generation_message_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_missing(T_S(actioncommande),sortie_generation_missing_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_option(T_S(actioncommande),sortie_generation_option_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_options(T_S(actioncommande),sortie_generation_options_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_parametre(T_S(actioncommande),sortie_generation_parametre_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_reference(T_S(actioncommande),sortie_generation_reference_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_ref(T_S(actioncommande),sortie_generation_ref_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_sec(T_S(actioncommande),sortie_generation_sec_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_section(T_S(actioncommande),sortie_generation_section_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_standard(T_S(actioncommande),sortie_generation_standard_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_start(T_S(actioncommande),sortie_generation_start_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_title(T_S(actioncommande),sortie_generation_title_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_txt(T_S(actioncommande),sortie_generation_txt_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_warning(T_S(actioncommande),sortie_generation_warning_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));        
        SECURISE(actionflux_definition_principal(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        
        SECURISE(actioncommande_definition_echappement(T_S(actioncommande),sortie_generation_echappement_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_texte(T_S(actioncommande),sortie_generation_texte_erreur,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));        
        SECURISE(actionflux_definition_commentaire(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_options(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_message(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_document(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
        SECURISE(actionflux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
        //Statistiques.
        SECURISE(styles_message_lecture(CHAMP_TRAVAIL(general, styles),C_S_(correct),R_T(fluxmessage)));
        if(T_S_(correct)==T_S_(VRAI))
        {
                STOCKAGE(Commande) commande;
                STOCKAGE(CommandeTexte) commandetexte;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_SORTIE_PRODUCTION_STATS)));
                SECURISE(actionflux_initialisation(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
                SECURISE(processusflux_initialisation(T_S(processusflux)));
                SECURISE(actioncommande_initialisation(T_S(actioncommande)));
                SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
                SECURISE(actionflux_definition_principal(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_commentaire(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_compilateur(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_options(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_macro_definition(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_macro_parametre(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_document(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_niveau(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_pertinence(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_section_nom(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_section_format(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_section_section(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_reference_format(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_reference(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_reference_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_titre(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_titre_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_texte(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_texte_reductible(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_texte_irreductible(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_texte_manquante(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_texte_generique(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actionflux_definition_indice(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                SECURISE(actioncommande_definition_echappement(T_S(actioncommande),sortie_generation_echappement,T_S_(PARCOURS_MANUEL)));
                SECURISE(actioncommande_definition_texte(T_S(actioncommande),sortie_generation_texte,T_S_(PARCOURS_MANUEL)));
                SECURISE(actioncommande_definition_mesg(T_S(actioncommande),sortie_generation_mesg,T_S_(PARCOURS_MANUEL)));
                SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
                SECURISE(actionflux_definition_message(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),T_S(processusflux)));
                fluxstatistiques=NULL;
                SECURISE(flux_copie(fluxmessage,T_S(fluxstatistiques)));
                SECURISE(commandetexte_initialisation(T_S(commandetexte)));
                SECURISE(commandetexte_definition_texte(T_S(commandetexte),T_S_((STOCKAGE_SCALAIRE(Chaine))("\n"))));
                SECURISE(commande_initialisation(T_S(commande)));
                SECURISE(commande_definition_texte(T_S(commande),T_S(commandetexte)));
                SECURISE(flux_ajout_commande(T_S(fluxstatistiques),T_S(commande),general));
                SECURISE(commande_destruction(T_S(commande)));
                SECURISE(commandetexte_destruction(T_S(commandetexte)));
                SECURISE(flux_parcours(T_S(fluxstatistiques),T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie)),general));
                SECURISE(flux_destruction(T_S(fluxstatistiques)));
                SECURISE(actioncommande_destruction(T_S(actioncommande)));
                SECURISE(processusflux_destruction(T_S(processusflux)));
                SECURISE(actionflux_destruction(T_S(CHAMP_STOCKAGE(CHAMP(general, sortie), actiongenerationsortie))));
        }
        if(nomfichier!=T_S_(NULL))
        {
                SECURISE(environnement_lecture_sortie(CHAMP_TRAVAIL(general, environnement),R_T_(sortie)));
                fclose(sortie);
        }
        SECURISE(sortie_destruction(CHAMP_TRAVAIL(general, sortie)));
        return RESULTAT_OK;
}

Resultat sortie_copie(TRAVAIL(Sortie) sortie, TRAVAIL(Sortie) copie)
{
        /* R�alise une copie d'une structure de g�n�ration du
         * document.
         * Renvoie RESULTAT_ERREUR si sortie est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(sortie)!=NULL, RESULTAT_ERREUR);

        SECURISE(sortie_destruction(copie));
        SECURISE(sortie_initialisation(copie));
        if(CHAMP(sortie, actiongenerationsortie)!=NULL)
        {
                SECURISE(actionflux_copie(CHAMP_TRAVAIL(sortie, actiongenerationsortie),CHAMP_TRAVAIL(copie, actiongenerationsortie)));
        }
        if(CHAMP(sortie, erreur)!=NULL)
        {
                CHAMP(copie, erreur)=DUP_CAST(Chaine, T_S_(CHAMP(sortie, erreur)));
                ASSERTION(CHAMP(copie, erreur)!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        if(CHAMP(sortie, message)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, message),CHAMP_TRAVAIL(copie, message)));
        }
        if(CHAMP(sortie, ideeindice)!=NULL)
        {
                CHAMP(copie, ideeindice)=DUP_CAST(Chaine, T_S_(CHAMP(sortie, ideeindice)));
                ASSERTION(CHAMP(copie, ideeindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        if(CHAMP(sortie, ideereference)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, ideereference),CHAMP_TRAVAIL(copie, ideereference)));
        }
        if(CHAMP(sortie, ideetitre)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, ideetitre),CHAMP_TRAVAIL(copie, ideetitre)));
        }
        if(CHAMP(sortie, ideetexte)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, ideetexte),CHAMP_TRAVAIL(copie, ideetexte)));
        }
        CHAMP(copie, typeidee)=CHAMP(sortie, typeidee);
        if(CHAMP(sortie, ideereferencesidee)!=NULL)
        {
                SECURISE(referencesidee_copie(CHAMP_TRAVAIL(sortie, ideereferencesidee),CHAMP_TRAVAIL(copie, ideereferencesidee)));
        }
        if(CHAMP(sortie, referenceindice)!=NULL)
        {
                CHAMP(copie, referenceindice)=DUP_CAST(Chaine, T_S_(CHAMP(sortie, referenceindice)));
                ASSERTION(CHAMP(copie, referenceindice)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        }
        if(CHAMP(sortie, referencereference)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, referencereference),CHAMP_TRAVAIL(copie, referencereference)));
        }
        if(CHAMP(sortie, referencereferencechaine)!=NULL)
        {
                CHAMP(copie, referencereferencechaine)=DUP_CAST(Chaine, T_S_(CHAMP(sortie, referencereferencechaine)));
                ASSERTION(CHAMP(copie, referencereferencechaine)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        }
        if(CHAMP(sortie, referencetitre)!=NULL)
        {
                SECURISE(flux_copie(CHAMP_TRAVAIL(sortie, referencetitre),CHAMP_TRAVAIL(copie, referencetitre)));
        }
        if(CHAMP(sortie, referencesection)!=NULL)
        {
                CHAMP(copie, referencesection)=DUP_CAST(Chaine, T_S_(CHAMP(sortie, referencesection)));
                ASSERTION(CHAMP(copie, referencesection)!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        CHAMP(copie, niveaureferencesection)=CHAMP(sortie, niveaureferencesection);
        return RESULTAT_OK;
}

Resultat sortie_destruction(TRAVAIL(Sortie) sortie)
{
        /* D�truit une structure de g�n�ration du document.
         */
        if(S_T(sortie)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(sortie, actiongenerationsortie)));
        if(CHAMP(sortie, erreur)!=NULL)
                free(CHAMP(sortie, erreur));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, message)));
        if(CHAMP(sortie, ideeindice)!=NULL)
                free(CHAMP(sortie, ideeindice));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, ideereference)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, ideetitre)));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, ideetexte)));
        SECURISE(referencesidee_destruction(CHAMP_TRAVAIL(sortie, ideereferencesidee)));
        if(CHAMP(sortie, referenceindice)!=NULL)
                free(CHAMP(sortie, referenceindice));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, referencereference)));
        if(CHAMP(sortie, referencereferencechaine)!=NULL)
                free(CHAMP(sortie, referencereferencechaine));
        SECURISE(flux_destruction(CHAMP_TRAVAIL(sortie, referencetitre)));
        if(CHAMP(sortie, referencesection)!=NULL)
                free(CHAMP(sortie, referencesection));
        free(S_T(sortie));
        S_T(sortie)=NULL;
        return RESULTAT_OK;
}

