/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __SORTIE__
#define __SORTIE__

#include <src/global/global.h>

typedef struct sortie CONTENEUR(Sortie);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>

#include <src/donnees/sortie/denominationidee.h>
#include <src/donnees/sortie/referencesidee.h>
#include <src/donnees/styles/styles.h>
#include <src/donnees/idees/idees.h>

struct sortie
{
        STOCKAGE(ActionFlux) actiongenerationsortie;
        //Action de flux servant � g�n�rer le
        //document.
        STOCKAGE_SCALAIRE(Chaine) erreur;
        //Message d'erreur.
        STOCKAGE(Flux) message;
        //Flux contenant un message � transmettre
        //dans le document.
        STOCKAGE_SCALAIRE(Chaine) ideeindice;
        //Chaine repr�sentant l'indice de l'id�e
        //automatique en cours de parcours.
        STOCKAGE(Flux) ideereference;
        //Flux d�crivant la r�f�rence de l'id�e parcourue.
        STOCKAGE(Flux) ideetitre;
        //Flux repr�sentant le titre de l'id�e parcourue.
        STOCKAGE(Flux) ideetexte;
        //Flux repr�sentant le texte de l'id�e parcourue.
        STOCKAGE_SCALAIRE(TypeIdee) typeidee;
        //Type de l'id�e parcourue
        STOCKAGE(ReferencesIdee) ideereferencesidee;
        //Structure de r�f�rence de l'id�e parcourue.
        STOCKAGE_SCALAIRE(Chaine) referenceindice;
        //Chaine repr�sentant l'indice de l'id�e
        //automatique destination de la r�f�rence.
        STOCKAGE(Flux) referencereference;
        //Flux d�crivant la r�f�rence de l'id�e destination
        //de la r�f�rence.
        STOCKAGE_SCALAIRE(Chaine) referencereferencechaine;
        //Chaine repr�sentant la r�f�rence, pour les messages
        //d'erreur.
        STOCKAGE(Flux) referencetitre;
        //Flux repr�sentant le titre de l'id�e destination
        //de la r�f�rence.
        STOCKAGE_SCALAIRE(Chaine) referencesection;
        //Chaine repr�sentant le nom du niveau de la section
        //de l'id�e destination de la r�f�rence.
        STOCKAGE_SCALAIRE(NiveauHierarchique) niveaureferencesection;
        //Niveau correspondant au nom de la section de l'id�e
        //destination de la r�f�rence.
};
/* Structure utilis�e pour g�n�rer le document.
 * Les divers champs de cette structure seront
 * directement utilis�s en interne.
 */

Resultat sortie_initialisation(TRAVAIL(Sortie) sortie);
/* Cr�e une structure de g�n�ration du document.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat sortie_sortie(TRAVAIL(General) general);
/* G�n�re la sortie � partir des informations
 * contenues dans general.
 */

Resultat sortie_copie(TRAVAIL(Sortie) sortie, TRAVAIL(Sortie) copie);
/* R�alise une copie d'une structure de g�n�ration du
 * document.
 * Renvoie RESULTAT_ERREUR si sortie est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat sortie_destruction(TRAVAIL(Sortie) sortie);
/* D�truit une structure de g�n�ration du document.
 */

#endif
