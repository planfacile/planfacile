/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "retouches.h"

static Resultat retouches_comptage_parametres(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Sert � indiquer le nombre de param�tres d'une
         * d�finition de macro.
         */
        TRAVAIL_SCALAIRE(Entier) parametre;
        TRAVAIL_SCALAIRE(Entier) nombre;
        TRAVAIL_SCALAIRE(Booleen) vide;
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeparametre_lecture_indice(commandeparametre,R_T_(parametre)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_PARAMETRE),parametre));
        SECURISE(pileentier_vide(T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), nombreparametres)),R_T_(vide)));
        if(vide==T_S_(VRAI))
        {
                S_C(flux)=NULL;
                S_C_(arretcontexte)=VRAI;
                return RESULTAT_OK;
        }
        SECURISE(pileentier_lecture_entier(T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), nombreparametres)),R_T_(nombre)));
        if((parametre==T_S_(0))&&(nombre>=T_S_(0)))
                nombre=-(nombre+T_S_(1));
        else
        {
                if(nombre>=T_S_(0))
                {
                        if(parametre>nombre)
                                nombre=parametre;
                }
                else
                {
                        if(parametre>(-(nombre+T_S_(1))))
                                nombre=-(parametre+T_S_(1));
                }
        }                
        SECURISE(pileentier_definition_entier(T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), nombreparametres)),nombre));
        S_C(flux)=NULL;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_nombre_parametres(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Sert � mettre � jour les indications de nombre
         * de param�tres d'une macro.
         * Au passage, on indique le niveau de la d�claration
         * de la macro.
         */
        TRAVAIL(Flux) definition;
        STOCKAGE_SCALAIRE(Entier) nombre;
        TRAVAIL_SCALAIRE(Chaine) nommacro;
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedefine_lecture_nom(commandedefine,R_T_(nommacro)));
        SECURISE(pileentier_ajout(T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), nombreparametres)),T_S_(0)));
        SECURISE(commandedefine_lecture_definition(commandedefine,R_T(definition)));
        SECURISE(flux_parcours(definition,actionflux,general));
        SECURISE(pileentier_retrait(T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), nombreparametres)),C_S_(nombre)));
        if(T_S_(nombre)<T_S_(0))
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_MACRO_BOUCLE),nommacro,-(T_S_(nombre)+T_S_(1))));
                SECURISE(commandedefine_definition_parametres(commandedefine,-(T_S_(nombre)+T_S_(1)),T_S_(VRAI)));
        }
        else
        {
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_MACRO_NORMALE),nommacro,T_S_(nombre)));
                SECURISE(commandedefine_definition_parametres(commandedefine,T_S_(nombre),T_S_(FAUX)));
        }
        SECURISE(commandedefine_definition_niveau(commandedefine,T_S_(CHAMP_STOCKAGE(CHAMP(general, retouches), niveau))));
        S_C(flux)=NULL;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_augmentation_niveau(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Ajoute un niveau d'imbrication de commande
         * #options. En fait, l'augmentation se fait
         * niveau de l'entr�e d'une clause.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        T_S_(CHAMP_STOCKAGE(CHAMP(general, retouches), niveau))++;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_diminution_niveau(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enl�ve un niveau d'imbrication de commande
         * #options. En fait, la diminution se fait
         * niveau de l'entr�e d'une clause, avec une
         * d�fragmentation.
         */
        TRAVAIL_SCALAIRE(TypeFlux) type;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        T_S_(CHAMP_STOCKAGE(CHAMP(general, retouches), niveau))--;
        SECURISE(flux_lecture_type(flux,R_T_(type)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_DEFRAGMENTATION),type));
        SECURISE(flux_defragmentation(flux));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_defragmentation(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* D�fragmente les flux.
         */
        TRAVAIL_SCALAIRE(TypeFlux) type;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_lecture_type(flux,R_T_(type)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_DEFRAGMENTATION),type));
        SECURISE(flux_defragmentation(flux));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_elimination_include(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace la commande include par un flux vide.
         */
        STOCKAGE(Flux) flux_vide;
        TRAVAIL_SCALAIRE(NomFichier) nomfichier;
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeinclude_lecture_inclusion(commandeinclude,R_T_(nomfichier)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_INCLUDE),nomfichier));
        SECURISE(flux_initialisation(T_S(flux_vide)));
        S_C(flux)=flux_vide;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat retouches_elimination_standard(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Remplace la commande standard par un flux vide.
         */
        STOCKAGE(Flux) flux_vide;
        ASSERTION(S_T(commandestandard)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES_STANDARD)));
        SECURISE(flux_initialisation(T_S(flux_vide)));
        S_C(flux)=flux_vide;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

Resultat retouches_initialisation(TRAVAIL(Retouches) retouches)
{
        /* Cr�e une structure de retouches.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        ASSERTION((S_T(retouches)=NOUVEAU(Retouches))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(retouches, actionretouches)));
        SECURISE(pileentier_initialisation(CHAMP_TRAVAIL(retouches, nombreparametres)));
        CHAMP(retouches, niveau)=0;
        return RESULTAT_OK;
}

Resultat retouches_retouches(TRAVAIL(General) general)
{
        /* R�organise le flux de commandes en :
         * - d�fragmentant le texte ;
         * - mettant � jour les informations de param�tres
         *   dans les d�finitions de macro.
         * - �liminant les commandes #include et #standard.
         * Renvoie RESULTAT_ERREUR si le flux principal
         * est NULL.
         */
        STOCKAGE(ProcessusFlux) processusflux;
        STOCKAGE(ActionCommande) actioncommande;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);
        ASSERTION(CHAMP(general, principal)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_RETOUCHES)));
        SECURISE(retouches_initialisation(CHAMP_TRAVAIL(general, retouches)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));

        SECURISE(actioncommande_definition_include(T_S(actioncommande),retouches_elimination_include,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_standard(T_S(actioncommande),retouches_elimination_standard,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_define(T_S(actioncommande),retouches_nombre_parametres,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_parametre(T_S(actioncommande),retouches_comptage_parametres,T_S_(PARCOURS_MANUEL)));
        
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction(T_S(processusflux),retouches_defragmentation));
        SECURISE(processusflux_definition_contexte(T_S(processusflux),T_S_(CONTEXTE_LOCAL)));
        
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));
        
        SECURISE(processusflux_definition_preaction(T_S(processusflux),retouches_augmentation_niveau));
        SECURISE(processusflux_definition_postaction(T_S(processusflux),retouches_diminution_niveau));

        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),T_S(processusflux)));

        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, retouches), actionretouches)),general));
        SECURISE(retouches_destruction(CHAMP_TRAVAIL(general, retouches)));
        return RESULTAT_OK;
}

Resultat retouches_copie(TRAVAIL(Retouches) retouches, TRAVAIL(Retouches) copie)
{
        /* R�alise une copie d'une structure de retouches.
         * Renvoie RESULTAT_ERREUR si retouches est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(retouches)!=NULL, RESULTAT_ERREUR);

        SECURISE(retouches_destruction(copie));
        SECURISE(retouches_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(retouches, actionretouches),CHAMP_TRAVAIL(copie, actionretouches)));
        SECURISE(pileentier_copie(CHAMP_TRAVAIL(retouches, nombreparametres),CHAMP_TRAVAIL(copie, nombreparametres)));
        CHAMP(copie, niveau)=CHAMP(retouches, niveau);
        return RESULTAT_OK;
}

Resultat retouches_destruction(TRAVAIL(Retouches) retouches)
{
        /* D�truit une structure de retouches.
        */
        if(S_T(retouches)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(retouches, actionretouches)));
        SECURISE(pileentier_destruction(CHAMP_TRAVAIL(retouches, nombreparametres)));
        free(S_T(retouches));
        S_T(retouches)=NULL;
        return RESULTAT_OK;
}

