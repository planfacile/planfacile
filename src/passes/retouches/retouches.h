/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __RETOUCHES__
#define __RETOUCHES__

#include <src/global/global.h>

typedef struct retouches CONTENEUR(Retouches);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/flux/option.h>
#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandeoption.h>
#include <src/donnees/commandes/commandeparametre.h>
#include <src/donnees/commandes/commandeinclude.h>
#include <src/donnees/commandes/commandestandard.h>
#include <src/analyseur/donnees/pileentier.h>
#include <src/analyseur/donnees/pilechaine.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

struct retouches
{
        STOCKAGE(ActionFlux) actionretouches;
        //Action de flux pour la gestion des
        //retouches.
        STOCKAGE(PileEntier) nombreparametres;
        //Sert � calculer le nombre de parametres
        //d'une macro. Si le nombre est n�gatif,
        //cela signifie que la d�finition contient
        //le param�tre #0. Attention, la valeur
        //r�elle est inf�rieure d'une unit� en
        //valeur absolue, dans le cas n�gatif.
        STOCKAGE_SCALAIRE(NiveauHierarchique) niveau;
        //Ce niveau correspond au niveau d'imbrication
        //des commandes #options, utilis� pour renseigner
        //le niveau des d�finitions de macros.
        //Pour le flux principal, le niveau est nul.
};
/* Structure utilis�e pour les besoin internes
 * des retouches. Les champs de cette structure
 * seront utilis�s directement, puisqu'ils ne le sont
 * qu'en "interne".
 */

Resultat retouches_initialisation(TRAVAIL(Retouches) retouches);
/* Cr�e une structure de retouches.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat retouches_retouches(TRAVAIL(General) general);
/* R�organise le flux de commandes en :
 * - d�fragmentant le texte ;
 * - mettant � jour les informations de param�tres
 *   dans les d�finitions de macro.
 * - �liminant les commandes #include et #standard.
 * Renvoie RESULTAT_ERREUR si le flux principal
 * est NULL.
 */

Resultat retouches_copie(TRAVAIL(Retouches) retouches, TRAVAIL(Retouches) copie);
/* R�alise une copie d'une structure de retouches.
 * Renvoie RESULTAT_ERREUR si retouches est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat retouches_destruction(TRAVAIL(Retouches) retouches);
/* D�truit une structure de retouches.
 */

#endif
