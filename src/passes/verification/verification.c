/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "verification.h"

Resultat verification_initialisation(TRAVAIL(Verification) verification)
{
        /* Cr�e un structure de v�rification de flux.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION((S_T(verification)=NOUVEAU(Verification))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(verification, actionverification)));
        CHAMP(verification, message)=NULL;
        return RESULTAT_OK;
}

static Resultat verification_defragmentation(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* R�alise la d�fragmentation des flux.
         */
        TRAVAIL_SCALAIRE(TypeFlux) type;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_lecture_type(flux,R_T_(type)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_VERIFICATION_DEFRAGMENTATION),type));
        SECURISE(flux_defragmentation(flux));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_avertissement(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Affiche un avertissement utilisateur */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(Flux) avertissement;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_VERIFICATION_AVERTISSEMENT)));
        SECURISE(commandewarning_lecture_avertissement(commandewarning,R_T(avertissement)));
        SECURISE(flux_texte(avertissement,R_S_(CHAMP_STOCKAGE(CHAMP(general, verification), message))));
        SECURISE(commandewarning_lecture_localisationfichier(commandewarning,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_AVERTISSEMENT_UTILISATEUR),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, verification), message))));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        free(CHAMP_STOCKAGE(CHAMP(general, verification), message));
        CHAMP_STOCKAGE(CHAMP(general, verification), message)=NULL;
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_erreur(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Affiche une erreur utilisateur */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(Flux) erreur;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_VERIFICATION_ERREUR)));
        SECURISE(commandeerror_lecture_erreur(commandeerror,R_T(erreur)));
        SECURISE(flux_texte(erreur,R_S_(CHAMP_STOCKAGE(CHAMP(general, verification), message))));
        SECURISE(commandeerror_lecture_localisationfichier(commandeerror,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_ERREUR_UTILISATEUR),T_S(localisation),T_S_(CHAMP_STOCKAGE(CHAMP(general, verification), message))));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        free(CHAMP_STOCKAGE(CHAMP(general, verification), message));
        CHAMP_STOCKAGE(CHAMP(general, verification), message)=NULL;
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_erreur_commandecommentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande commentaire.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandecommentaire_lecture_localisationfichier(commandecommentaire,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#comment")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandecommentaire(TRAVAIL(CommandeCommentaire) commandecommentaire, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande commentaire.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandecommentaire)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeechappement(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande echappement.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL_SCALAIRE(Caractere) echappement;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeechappement)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeechappement_lecture_localisationfichier(commandeechappement,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(commandeechappement_lecture_caractere(commandeechappement,R_T_(echappement)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_ECHAPPEMENT_INCORRECTE),T_S(localisation),echappement));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

//static Resultat verification_suppression_commandeechappement(TRAVAIL(CommandeEchappement) commandeechappement, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
//{
//        /* Supprime silencieusement la commande echappement.
//         */
//        STOCKAGE(Flux) flux_equivalent;
//        SECURISE(flux_initialisation(T_S(flux_equivalent)));
//        S_C(flux)=flux_equivalent;
//        S_C_(arretcontexte)=VRAI;
//        return RESULTAT_OK;        
//}

static Resultat verification_erreur_commandetexte(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande texte.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetexte_lecture_localisationfichier(commandetexte,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_TEXTE_INCORRECTE),T_S(localisation)));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

//static Resultat verification_suppression_commandetexte(TRAVAIL(CommandeTexte) commandetexte, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
//{
//        /* Supprime silencieusement la commande texte.
//         */
//        STOCKAGE(Flux) flux_equivalent;
//        ASSERTION(S_T(commandetexte)!=NULL, RESULTAT_ERREUR);
//
//        SECURISE(flux_initialisation(T_S(flux_equivalent)));
//        S_C(flux)=flux_equivalent;
//        S_C_(arretcontexte)=VRAI;
//        return RESULTAT_OK;        
//}

//static Resultat verification_erreur_commandewarning(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
//{
//        /* Notifie une erreur sur la commande warning.
//         */
//        TRAVAIL(LocalisationFichier) localisationcommande;
//        STOCKAGE(LocalisationFichier) localisation;
//        STOCKAGE(Flux) flux_equivalent;
//        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);
//
//        SECURISE(commandewarning_lecture_localisationfichier(commandewarning,R_T(localisationcommande)));
//        SECURISE(localisationfichier_initialisation(T_S(localisation)));
//        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
//        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#warning")));
//        SECURISE(localisationfichier_destruction(T_S(localisation)));
//        SECURISE(flux_initialisation(T_S(flux_equivalent)));
//        S_C(flux)=flux_equivalent;
//        S_C_(arretcontexte)=VRAI;
//        return RESULTAT_OK;
//}

static Resultat verification_suppression_commandewarning(TRAVAIL(CommandeWarning) commandewarning, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande warning.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandewarning)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

//static Resultat verification_erreur_commandeerror(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
//{
//        /* Notifie une erreur sur la commande error.
//         */
//        TRAVAIL(LocalisationFichier) localisationcommande;
//        STOCKAGE(LocalisationFichier) localisation;
//        STOCKAGE(Flux) flux_equivalent;
//        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);
//
//        SECURISE(commandeerror_lecture_localisationfichier(commandeerror,R_T(localisationcommande)));
//        SECURISE(localisationfichier_initialisation(T_S(localisation)));
//        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
//        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#error")));
//        SECURISE(localisationfichier_destruction(T_S(localisation)));
//        SECURISE(flux_initialisation(T_S(flux_equivalent)));
//        S_C(flux)=flux_equivalent;
//        S_C_(arretcontexte)=VRAI;
//        return RESULTAT_OK;
//}

static Resultat verification_suppression_commandeerror(TRAVAIL(CommandeError) commandeerror, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande error.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeerror)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeinclude(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande include.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeinclude_lecture_localisationfichier(commandeinclude,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#include")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeinclude(TRAVAIL(CommandeInclude) commandeinclude, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande include.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeinclude)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandestandard(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande standard.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandestandard)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandestandard_lecture_localisationfichier(commandestandard,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#standard")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandestandard(TRAVAIL(CommandeStandard) commandestandard, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande standard.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandestandard)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeoption(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande option.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoption_lecture_localisationfichier(commandeoption,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#option")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeoption(TRAVAIL(CommandeOption) commandeoption, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande option.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeoption)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeoptions(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande options.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeoptions_lecture_localisationfichier(commandeoptions,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#options")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeoptions(TRAVAIL(CommandeOptions) commandeoptions, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande options.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeoptions)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandedefine(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande define.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedefine_lecture_localisationfichier(commandedefine,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#define")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandedefine(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande define.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeparametre(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande parametre.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL_SCALAIRE(Indice) numeroparametre;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeparametre_lecture_localisationfichier(commandeparametre,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(commandeparametre_lecture_indice(commandeparametre,R_T_(numeroparametre)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_PARAMETRE_INCORRECTE),T_S(localisation),numeroparametre));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeparametre(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande parametre.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandemacro(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande macro.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL_SCALAIRE(NomMacro) nommacro;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemacro_lecture_localisationfichier(commandemacro,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(commandemacro_lecture_nom(commandemacro,R_T_(nommacro)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_MACRO_INCORRECTE),T_S(localisation),nommacro));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandemacro(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande macro.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandemessage(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande message.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemessage_lecture_localisationfichier(commandemessage,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#message")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandemessage(TRAVAIL(CommandeMessage) commandemessage, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande message.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemessage)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandemesg(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande mesg.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemesg_lecture_localisationfichier(commandemesg,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#mesg")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandemesg(TRAVAIL(CommandeMesg) commandemesg, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande mesg.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemesg)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandehead(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande head.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandehead_lecture_localisationfichier(commandehead,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#head")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandehead(TRAVAIL(CommandeHead) commandehead, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande head.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandehead)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandefoot(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande foot.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandefoot_lecture_localisationfichier(commandefoot,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#foot")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandefoot(TRAVAIL(CommandeFoot) commandefoot, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande foot.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandefoot)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandesection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande section.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandesection_lecture_localisationfichier(commandesection,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#section")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandesection(TRAVAIL(CommandeSection) commandesection, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande section.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesection)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandereference(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande reference.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandereference_lecture_localisationfichier(commandereference,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#reference")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandereference(TRAVAIL(CommandeReference) commandereference, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande reference.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandereference)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandetitle(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande title.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetitle_lecture_localisationfichier(commandetitle,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#title")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandetitle(TRAVAIL(CommandeTitle) commandetitle, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande title.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetitle)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commanderef(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande ref.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        SECURISE(commanderef_lecture_localisationfichier(commanderef,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#ref")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commanderef(TRAVAIL(CommandeRef) commanderef, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande ref.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commanderef)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandesec(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande sec.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandesec_lecture_localisationfichier(commandesec,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#sec")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandesec(TRAVAIL(CommandeSec) commandesec, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande sec.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandesec)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandetxt(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande txt.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandetxt_lecture_localisationfichier(commandetxt,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#txt")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandetxt(TRAVAIL(CommandeTxt) commandetxt, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande txt.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandetxt)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandestart(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande start.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandestart_lecture_localisationfichier(commandestart,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#start")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandestart(TRAVAIL(CommandeStart) commandestart, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande start.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandestart)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeidea(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande idea.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeidea_lecture_localisationfichier(commandeidea,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#idea")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeidea(TRAVAIL(CommandeIdea) commandeidea, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande idea.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeidea)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandemissing(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande missing.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandemissing_lecture_localisationfichier(commandemissing,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#missing")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandemissing(TRAVAIL(CommandeMissing) commandemissing, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande missing.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandemissing)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandegeneric(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande generic.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandegeneric_lecture_localisationfichier(commandegeneric,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#generic")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandegeneric(TRAVAIL(CommandeGeneric) commandegeneric, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande generic.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandegeneric)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandedep(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande dep.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedep_lecture_localisationfichier(commandedep,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#dep")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandedep(TRAVAIL(CommandeDep) commandedep, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande dep.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedep)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandedepref(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande depref.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandedepref_lecture_localisationfichier(commandedepref,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#depref")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandedepref(TRAVAIL(CommandeDepRef) commandedepref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande depref.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandedepref)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeextrefs(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande extrefs.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeextrefs_lecture_localisationfichier(commandeextrefs,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#extrefs")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeextrefs(TRAVAIL(CommandeExtRefs) commandeextrefs, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande extrefs.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextrefs)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeextref(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande extref.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeextref_lecture_localisationfichier(commandeextref,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#extref")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeextref(TRAVAIL(CommandeExtRef) commandeextref, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande extref.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeextref)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

static Resultat verification_erreur_commandeindex(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Notifie une erreur sur la commande index.
         */
        TRAVAIL(LocalisationFichier) localisationcommande;
        STOCKAGE(LocalisationFichier) localisation;
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeindex_lecture_localisationfichier(commandeindex,R_T(localisationcommande)));
        SECURISE(localisationfichier_initialisation(T_S(localisation)));
        SECURISE(localisationfichier_copie(localisationcommande,T_S(localisation)));
        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_COMMANDE_INCORRECTE),T_S(localisation),T_S_("#index")));
        SECURISE(localisationfichier_destruction(T_S(localisation)));
        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat verification_suppression_commandeindex(TRAVAIL(CommandeIndex) commandeindex, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Supprime silencieusement la commande index.
         */
        STOCKAGE(Flux) flux_equivalent;
        ASSERTION(S_T(commandeindex)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_initialisation(T_S(flux_equivalent)));
        S_C(flux)=flux_equivalent;
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;        
}

Resultat verification_verification(TRAVAIL(General) general)
{
        /* V�rifie les flux, en g�n�rant un probl�me
         * � chaque commande mal plac�e.
         * Les commandes #warning et #error sont �galement trait�es.
         * Enfin, une d�fragmentation des flux est faite.
         */
        STOCKAGE(ActionCommande) actioncommande;
        STOCKAGE(ProcessusFlux) processusflux;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_VERIFICATION)));
        SECURISE(verification_initialisation(CHAMP_TRAVAIL(general, verification)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement   ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                       ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre     ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),NULL                                      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),verification_erreur_commandetexte         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_principal(T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_suppression_commandecommentaire      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_suppression_commandedefine           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_suppression_commandedep              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_suppression_commandedepref           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_suppression_commandeerror            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_suppression_commandeextref           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_suppression_commandeextrefs          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_suppression_commandefoot             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_suppression_commandegeneric          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_suppression_commandehead             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_suppression_commandeidea             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_suppression_commandeinclude          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_suppression_commandeindex            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_suppression_commandemacro            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_suppression_commandemesg             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_suppression_commandemessage          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_suppression_commandemissing          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_suppression_commandeoption           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_suppression_commandeoptions          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_suppression_commandeparametre        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_suppression_commandereference        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_suppression_commanderef              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_suppression_commandesec              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_suppression_commandesection          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_suppression_commandestandard         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_suppression_commandestart            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_suppression_commandetitle            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_suppression_commandetxt              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_suppression_commandewarning          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction(T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_commentaire      (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_suppression_commandecommentaire      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_suppression_commandedefine           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_suppression_commandedep              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_suppression_commandedepref           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_suppression_commandeerror            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_suppression_commandeextref           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_suppression_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_suppression_commandefoot             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_suppression_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_suppression_commandehead             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_suppression_commandeidea             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_suppression_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_suppression_commandeindex            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_suppression_commandemacro            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_suppression_commandemesg             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_suppression_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_suppression_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_suppression_commandeoption           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_suppression_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_suppression_commandeparametre        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_suppression_commandereference        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_suppression_commanderef              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_suppression_commandesec              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_suppression_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_suppression_commandestandard        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_suppression_commandestart            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_suppression_commandetitle            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_suppression_commandetxt              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_suppression_commandewarning        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_compilateur      (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),verification_erreur_commandetexte        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction(T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_options          (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),verification_erreur_commandetexte        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_macro_definition (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),verification_erreur_commandetexte        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_macro_parametre  (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_suppression_commandecommentaire  ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep               ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                           ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea              ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),NULL                                          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef               ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec               ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle             ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt               ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                    ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_message          (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_document         (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_niveau           (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_pertinence       (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_section_nom      (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_section_format   (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_section_section  (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_reference_format (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_reference        (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_reference_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_titre            (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_titre_automatique(T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_texte            (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_texte_reductible (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_texte_irreductible(T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_texte_manquante  (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_texte_generique  (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(actioncommande_initialisation(T_S(actioncommande)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_definition_commentaire  (T_S(actioncommande),verification_erreur_commandecommentaire    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_define       (T_S(actioncommande),verification_erreur_commandedefine         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_dep          (T_S(actioncommande),verification_erreur_commandedep            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_depref       (T_S(actioncommande),verification_erreur_commandedepref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_echappement  (T_S(actioncommande),verification_erreur_commandeechappement    ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_error        (T_S(actioncommande),verification_erreur                        ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(actioncommande_definition_extref       (T_S(actioncommande),verification_erreur_commandeextref         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_extrefs      (T_S(actioncommande),verification_erreur_commandeextrefs        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_foot         (T_S(actioncommande),verification_erreur_commandefoot           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_generic      (T_S(actioncommande),verification_erreur_commandegeneric        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_head         (T_S(actioncommande),verification_erreur_commandehead           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_idea         (T_S(actioncommande),verification_erreur_commandeidea           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_include      (T_S(actioncommande),verification_erreur_commandeinclude        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_index        (T_S(actioncommande),verification_erreur_commandeindex          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_macro        (T_S(actioncommande),verification_erreur_commandemacro          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_mesg         (T_S(actioncommande),verification_erreur_commandemesg           ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_message      (T_S(actioncommande),verification_erreur_commandemessage        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_missing      (T_S(actioncommande),verification_erreur_commandemissing        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_option       (T_S(actioncommande),verification_erreur_commandeoption         ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_options      (T_S(actioncommande),verification_erreur_commandeoptions        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_parametre    (T_S(actioncommande),verification_erreur_commandeparametre      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_reference    (T_S(actioncommande),verification_erreur_commandereference      ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_ref          (T_S(actioncommande),verification_erreur_commanderef            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_sec          (T_S(actioncommande),verification_erreur_commandesec            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_section      (T_S(actioncommande),verification_erreur_commandesection        ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_standard     (T_S(actioncommande),verification_erreur_commandestandard       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_start        (T_S(actioncommande),verification_erreur_commandestart          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_texte        (T_S(actioncommande),NULL                                       ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_title        (T_S(actioncommande),verification_erreur_commandetitle          ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_txt          (T_S(actioncommande),verification_erreur_commandetxt            ,T_S_(PARCOURS_AUTOMATIQUE_APRES)));
        SECURISE(actioncommande_definition_warning      (T_S(actioncommande),verification_avertissement                 ,T_S_(PARCOURS_AUTOMATIQUE_AVANT)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction    (T_S(processusflux),verification_defragmentation));
        SECURISE(actionflux_definition_indice           (T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),T_S(processusflux)));
        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));

        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, verification), actionverification)),general));
        SECURISE(verification_destruction(CHAMP_TRAVAIL(general, verification)));
        return RESULTAT_OK;
}

Resultat verification_copie(TRAVAIL(Verification) verification, TRAVAIL(Verification) copie)
{
        /* R�alise une copie d'une structure de v�rification de flux.
         * Renvoie RESULTAT_ERREUR si verification est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(verification)!=NULL, RESULTAT_ERREUR);

        SECURISE(verification_destruction(copie));
        SECURISE(verification_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(verification, actionverification),CHAMP_TRAVAIL(copie, actionverification)));
        if(CHAMP(verification, message)!=NULL)
        {
                CHAMP(copie, message)=DUP_CAST(Chaine, T_S_(CHAMP(verification, message)));
                ASSERTION(CHAMP(copie, message)!=NULL, RESULTAT_ERREUR_MEMOIRE);
        }
        else
                CHAMP(copie, message)=NULL;
        return RESULTAT_OK;
}

Resultat verification_destruction(TRAVAIL(Verification) verification)
{
        /* D�truit une structure de v�rification de flux.
         */
        if(S_T(verification)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(verification, actionverification)));
        if(CHAMP(verification, message)!=NULL)
                free(CHAMP(verification, message));
        free(S_T(verification));
        S_T(verification)=NULL;
        return RESULTAT_OK;
}

