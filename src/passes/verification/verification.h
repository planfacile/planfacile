/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __VERIFICATION__
#define __VERIFICATION__

#include <src/global/global.h>

typedef struct verification CONTENEUR(Verification);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/commandes/commande.h>
#include <src/problemes/verbeux/verbeux.h>

struct verification
{
        STOCKAGE(ActionFlux) actionverification;
        //Action sur les flux g�rant la
        //v�rification.
        STOCKAGE_SCALAIRE(Chaine) message;
        //Message des commandes #warning et
        //#error.
};
/* Structure utilis�e pour les besoin internes de la
 * v�rification des flux. Les champs de cette structure
 * seront utilis�s directement.
 * La structure general doit �tre connue globalement.
 */

Resultat verification_initialisation(TRAVAIL(Verification) verification);
/* Cr�e un structure de v�rification de flux.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat verification_verification(TRAVAIL(General) general);
/* V�rifie les flux, en g�n�rant un probl�me
 * � chaque commande mal plac�e.
 * Les commandes #warning et #error sont �galement trait�es.
 * Enfin, une d�fragmentation des flux est faite.
 */

Resultat verification_copie(TRAVAIL(Verification) verification, TRAVAIL(Verification) copie);
/* R�alise une copie d'une structure de v�rification de flux.
 * Renvoie RESULTAT_ERREUR si verification est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat verification_destruction(TRAVAIL(Verification) verification);
/* D�truit une structure de v�rification de flux.
 */

#endif
