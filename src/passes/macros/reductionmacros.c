/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "reductionmacros.h"

Resultat reductionmacros_initialisation(TRAVAIL(ReductionMacros) reductionmacros)
{
        /* Initialise une structure de r�duction de macro.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation.
         */
        ASSERTION((S_T(reductionmacros)=NOUVEAU(ReductionMacros))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(reductionmacros, actionreductionmacros)));
        SECURISE(actionflux_initialisation(CHAMP_TRAVAIL(reductionmacros, actionremplacementparametres)));
        SECURISE(piledefinitionmacro_initialisation(CHAMP_TRAVAIL(reductionmacros, piledefinitionmacro)));
        SECURISE(pileappelmacro_initialisation(CHAMP_TRAVAIL(reductionmacros, pileappelmacro)));
        return RESULTAT_OK;
}

static Resultat reductionmacros_ajout_definitionmacro(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Enregistre les d�finitions de macros d'un flux.
         */
        TRAVAIL(Macro) macro;
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(flux_lecture_macro(flux,R_T(macro)));
        SECURISE(piledefinitionmacro_ajout_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro)),macro));
        SECURISE(macro_destruction(macro));
        SECURISE(macro_initialisation(macro));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionmacros_retrait_definitionmacro(TRAVAIL(Flux) flux, TRAVAIL(General) general, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Retire les d�finitions de macros d'un flux.
         */
        ASSERTION(S_T(flux)!=NULL, RESULTAT_ERREUR);

        SECURISE(piledefinitionmacro_retrait_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro))));
        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionmacros_reduction_macro(TRAVAIL(CommandeMacro) commandemacro, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* R�alise le remplacement d'un appel de macro par
         * le flux �quivalent.
         */
        TRAVAIL_SCALAIRE(NomMacro) nommacro;
        STOCKAGE(Flux) flux_equivalent;
        TRAVAIL(Flux) definition;
        TRAVAIL_SCALAIRE(Taille) parametres;
        TRAVAIL_SCALAIRE(Taille) parametresattendus;
        TRAVAIL_SCALAIRE(Booleen) trouve;
        TRAVAIL_SCALAIRE(Booleen) boucle;
        TRAVAIL_SCALAIRE(Booleen) appel;
        STOCKAGE(LocalisationFichier) localisation;
        TRAVAIL(LocalisationFichier) localisationcommandemacro;
        STOCKAGE_SCALAIRE(Indice) indiceparametre;
        ASSERTION(S_T(commandemacro)!=NULL, RESULTAT_ERREUR);

        S_C(arretcontexte)=VRAI;
        SECURISE(commandemacro_lecture_nom(commandemacro,R_T_(nommacro)));
        SECURISE(commandemacro_lecture_tailleparametre(commandemacro,R_T_(parametres)));
        SECURISE(piledefinitionmacro_evaluation_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro)),nommacro,
                R_T_(trouve),R_T(definition),R_T_(parametresattendus),R_T_(boucle),R_T_(appel)));
        if(trouve==T_S_(FAUX))
        {
                SECURISE(commandemacro_lecture_localisationfichier(commandemacro,R_T(localisationcommandemacro)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommandemacro,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_MACRO_INCONNUE),T_S(localisation),nommacro));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                return RESULTAT_OK;
        }
        if(appel==T_S_(VRAI))
        {
                SECURISE(commandemacro_lecture_localisationfichier(commandemacro,R_T(localisationcommandemacro)));
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_copie(localisationcommandemacro,T_S(localisation)));
                SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_RECURSIVITE_MACRO),T_S(localisation),nommacro));
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                S_C(flux)=flux_equivalent;
                return RESULTAT_OK;
        }
        if(boucle==T_S_(FAUX))
        {
                STOCKAGE(Flux) vide;
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_MACROS_NORMALE),nommacro));
                SECURISE(flux_initialisation(T_S(vide)));
                SECURISE(flux_definition_type(T_S(vide),T_S_(FLUX_MACRO_PARAMETRE)));
                if(parametres!=parametresattendus)
                {
                        SECURISE(commandemacro_lecture_localisationfichier(commandemacro,R_T(localisationcommandemacro)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommandemacro,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_PARAMETRES_MACRO),T_S(localisation),nommacro,parametres,parametresattendus));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        for( ; parametres<parametresattendus ; parametres++)
                        {
                                SECURISE(commandemacro_ajout_parametre(commandemacro,T_S(vide)));
                        }
                }
                SECURISE(commandemacro_decalage_parametres(commandemacro));
                SECURISE(commandemacro_definition_parametre(commandemacro,T_S_(0),T_S(vide)));
                SECURISE(pileappelmacro_ajout_appelmacro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro)),commandemacro));
                flux_equivalent=NULL;
                SECURISE(flux_copie(definition,T_S(flux_equivalent)));
                SECURISE(flux_parcours(T_S(flux_equivalent),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres)),general));
                SECURISE(pileappelmacro_retrait_appelmacro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro))));
                SECURISE(flux_destruction(T_S(vide)));
        }
        else
        {
                STOCKAGE(Flux) iteration;
                TRAVAIL(Flux) valeurparametre;
                //On a un flux � construire que si parametres>parametresattendus.
                if(parametres<parametresattendus)
                {
                        SECURISE(commandemacro_lecture_localisationfichier(commandemacro,R_T(localisationcommandemacro)));
                        SECURISE(localisationfichier_initialisation(T_S(localisation)));
                        SECURISE(localisationfichier_copie(localisationcommandemacro,T_S(localisation)));
                        SECURISE(probleme_probleme(CHAMP_TRAVAIL(general, environnement),T_S_(PROBLEME_PARAMETRES_MACRO),T_S(localisation),nommacro,parametres,parametresattendus));
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(piledefinitionmacro_liberation_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro)),nommacro));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                if(parametres==parametresattendus)
                {
                        SECURISE(piledefinitionmacro_liberation_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro)),nommacro));
                        SECURISE(flux_initialisation(T_S(flux_equivalent)));
                        S_C(flux)=flux_equivalent;
                        return RESULTAT_OK;
                }
                SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_MACROS_BOUCLE),nommacro));
                SECURISE(flux_initialisation(T_S(flux_equivalent)));
                SECURISE(flux_definition_type(T_S(flux_equivalent),T_S_(FLUX_MACRO_DEFINITION)));
                SECURISE(commandemacro_decalage_parametres(commandemacro));
                SECURISE(commandemacro_definition_parametre(commandemacro,T_S_(0),T_S(flux_equivalent)));
                parametresattendus++;
                parametres++;
                indiceparametre=1;
                //Ce type est temporaire, il sera �limin� lors de la fusion au niveau sup�rieur.
                SECURISE(pileappelmacro_ajout_appelmacro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro)),commandemacro));
                for( ; parametresattendus<parametres ; parametresattendus++)
                {
                        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_MACROS_ITERATION),nommacro,T_S_(indiceparametre)++));
                        SECURISE(commandemacro_lecture_parametre(commandemacro,parametresattendus,R_T(valeurparametre)));
                        SECURISE(pileappelmacro_definition_parametre(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro)),T_S_(0),valeurparametre));
                        iteration=NULL;
                        SECURISE(flux_copie(definition,T_S(iteration)));
                        SECURISE(flux_parcours(T_S(iteration),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres)),general));
                        SECURISE(flux_concatenation(T_S(flux_equivalent),T_S(iteration),general));
                        SECURISE(flux_destruction(T_S(iteration)));
                }
                SECURISE(pileappelmacro_retrait_appelmacro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro))));
        }
        //parcours r�cursif du flux g�n�r�
        SECURISE(flux_parcours(T_S(flux_equivalent),actionflux,general));
        SECURISE(piledefinitionmacro_liberation_macro(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), piledefinitionmacro)),nommacro));
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

static Resultat reductionmacros_remplacement_parametres_echap(TRAVAIL(CommandeDefine) commandedefine, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* Evite le remplacement de param�tres des macros
         * incluses dans une macro.
         */
        ASSERTION(S_T(commandedefine)!=NULL, RESULTAT_ERREUR);

        S_C_(arretcontexte)=VRAI;
        return RESULTAT_OK;
}

static Resultat reductionmacros_remplacement_parametres(TRAVAIL(CommandeParametre) commandeparametre, TRAVAIL(ActionFlux) actionflux, TRAVAIL(General) general, COREFERENCE(Flux) flux, COREFERENCE_SCALAIRE(Booleen) arretcontexte)
{
        /* R�alise le remplacement d'un param�tre
         * par sa valeur.
         */
        TRAVAIL_SCALAIRE(Indice) numeroparametre;
        STOCKAGE(Flux) flux_equivalent;
        TRAVAIL(Flux) valeurparametre;
        ASSERTION(S_T(commandeparametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(commandeparametre_lecture_indice(commandeparametre,R_T_(numeroparametre)));
        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_MACROS_PARAMETRE),numeroparametre));
        SECURISE(pileappelmacro_lecture_parametre(T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), pileappelmacro)),numeroparametre,R_T(valeurparametre)));
        flux_equivalent=NULL;
        SECURISE(flux_copie(valeurparametre,T_S(flux_equivalent)));
        S_C_(arretcontexte)=VRAI;
        S_C(flux)=flux_equivalent;
        return RESULTAT_OK;
}

Resultat reductionmacros_reductionmacros(TRAVAIL(General) general)
{
        /* Calcule quelles sont les macros qui sont r�ellement
         * d�finies dans le flux, et remplace chaque appel
         * par le flux correspondant.
         */
        STOCKAGE(ProcessusFlux) processusflux;
        STOCKAGE(ActionCommande) actioncommande;
        ASSERTION(S_T(general)!=NULL, RESULTAT_ERREUR);

        SECURISE(verbeux_verbeux(CHAMP_TRAVAIL(general, environnement),T_S_(VERBEUX_MACROS)));
        SECURISE(reductionmacros_initialisation(CHAMP_TRAVAIL(general, reductionmacros)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));

        SECURISE(actioncommande_definition_define(T_S(actioncommande),reductionmacros_remplacement_parametres_echap,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));
        
        SECURISE(actioncommande_definition_parametre(T_S(actioncommande),reductionmacros_remplacement_parametres,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_contexte(T_S(processusflux),T_S_(CONTEXTE_CONTEXTE)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionremplacementparametres))
                ,T_S(processusflux)));

        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        SECURISE(processusflux_initialisation(T_S(processusflux)));
        SECURISE(actioncommande_initialisation(T_S(actioncommande)));

        SECURISE(processusflux_definition_preaction(T_S(processusflux),reductionmacros_ajout_definitionmacro));
        SECURISE(actioncommande_definition_define(T_S(actioncommande),NULL,T_S_(PARCOURS_MANUEL)));
        SECURISE(actioncommande_definition_macro(T_S(actioncommande),reductionmacros_reduction_macro,T_S_(PARCOURS_MANUEL)));
        SECURISE(processusflux_definition_actioncommande(T_S(processusflux),T_S(actioncommande)));
        SECURISE(processusflux_definition_postaction(T_S(processusflux),reductionmacros_retrait_definitionmacro));
        
        SECURISE(actionflux_definition_principal                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_commentaire              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_compilateur              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_options                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_macro_definition         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_macro_parametre          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_message                  (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_document                 (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_niveau                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_pertinence               (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_nom              (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_format           (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_section_section          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference_format         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference                (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_reference_automatique    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_titre                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_titre_automatique        (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte                    (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_reductible         (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_irreductible       (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_manquante          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_texte_generique          (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));
        SECURISE(actionflux_definition_indice                   (T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros))
                ,T_S(processusflux)));

        SECURISE(actioncommande_destruction(T_S(actioncommande)));
        SECURISE(processusflux_destruction(T_S(processusflux)));
        
        SECURISE(flux_parcours(CHAMP_TRAVAIL(general, principal),T_S(CHAMP_STOCKAGE(CHAMP(general, reductionmacros), actionreductionmacros)),general));
        SECURISE(reductionmacros_destruction(CHAMP_TRAVAIL(general, reductionmacros)));
        return RESULTAT_OK;
}

Resultat reductionmacros_copie(TRAVAIL(ReductionMacros) reductionmacros, TRAVAIL(ReductionMacros) copie)
{
        /* R�alise une copie d'une structure de reduction des macros.
         * Renvoie RESULTAT_ERREUR si reductionmacros est NULL.
         * Si *copie est non NULL, la copie est d�truite.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
         * �choue.
         */
        ASSERTION(S_T(reductionmacros)!=NULL, RESULTAT_ERREUR);

        SECURISE(reductionmacros_destruction(copie));
        SECURISE(reductionmacros_initialisation(copie));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(reductionmacros, actionreductionmacros),CHAMP_TRAVAIL(copie, actionreductionmacros)));
        SECURISE(actionflux_copie(CHAMP_TRAVAIL(reductionmacros, actionremplacementparametres),CHAMP_TRAVAIL(copie, actionremplacementparametres)));
        SECURISE(piledefinitionmacro_copie(CHAMP_TRAVAIL(reductionmacros, piledefinitionmacro),CHAMP_TRAVAIL(copie, piledefinitionmacro)));
        SECURISE(pileappelmacro_copie(CHAMP_TRAVAIL(reductionmacros, pileappelmacro),CHAMP_TRAVAIL(copie, pileappelmacro)));
        return RESULTAT_OK;
}

Resultat reductionmacros_destruction(TRAVAIL(ReductionMacros) reductionmacros)
{
        /* D�truit une structure de reduction des macros.
         */
        if(S_T(reductionmacros)==NULL)
                return RESULTAT_OK;
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(reductionmacros, actionreductionmacros)));
        SECURISE(actionflux_destruction(CHAMP_TRAVAIL(reductionmacros, actionremplacementparametres)));
        SECURISE(piledefinitionmacro_destruction(CHAMP_TRAVAIL(reductionmacros, piledefinitionmacro)));
        SECURISE(pileappelmacro_destruction(CHAMP_TRAVAIL(reductionmacros, pileappelmacro)));
        free(S_T(reductionmacros));
        S_T(reductionmacros)=NULL;
        return RESULTAT_OK;
}

