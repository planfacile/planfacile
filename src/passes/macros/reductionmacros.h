/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __REDUCTIONMACROS__
#define __REDUCTIONMACROS__

#include <src/global/global.h>

typedef struct reductionmacros CONTENEUR(ReductionMacros);

#include <src/donnees/general/general.h>
#include <src/donnees/flux/actionflux.h>
#include <src/donnees/flux/processusflux.h>
#include <src/donnees/commandes/actioncommande.h>
#include <src/donnees/flux/flux.h>
#include <src/donnees/flux/macro.h>
#include <src/donnees/commandes/commandedefine.h>
#include <src/donnees/commandes/commandemacro.h>
#include <src/donnees/commandes/commandeparametre.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/donnees/passes/piledefinitionmacro.h>
#include <src/donnees/passes/pileappelmacro.h>
#include <src/problemes/probleme/probleme.h>
#include <src/problemes/verbeux/verbeux.h>

struct reductionmacros
{
        STOCKAGE(ActionFlux) actionreductionmacros;
        //Action de flux permettant la r�duction
        //des macros.
        STOCKAGE(ActionFlux) actionremplacementparametres;
        //Action de flux permettant le remplacement
        //des param�tres d'une d�finition de macro.
        STOCKAGE(PileDefinitionMacro) piledefinitionmacro;
        //Pile des d�finitions de macro en cours de
        //traitement.
        STOCKAGE(PileAppelMacro) pileappelmacro;
        //Pile des appels de macros en cours de
        //traitement. Ceux-ci servent au remplacement
        //des param�tres d'une d�finition de macro.
};
/* Structure utilis�e pour les besoin internes de la
 * r�duction des macros. Les champs de cette structure
 * seront utilis�s directement.
 * La structure general doit �tre connue globalement.
 */

Resultat reductionmacros_initialisation(TRAVAIL(ReductionMacros) reductionmacros);
/* Initialise une structure de r�duction de macro.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec d'une allocation.
 */

Resultat reductionmacros_reductionmacros(TRAVAIL(General) general);
/* Calcule quelles sont les macros qui sont r�ellement
 * d�finies dans le flux, et remplace chaque appel
 * par le flux correspondant.
 */

Resultat reductionmacros_copie(TRAVAIL(ReductionMacros) reductionmacros, TRAVAIL(ReductionMacros) copie);
/* R�alise une copie d'une structure de reduction des macros.
 * Renvoie RESULTAT_ERREUR si reductionmacros est NULL.
 * Si *copie est non NULL, la copie est d�truite.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation
 * �choue.
 */

Resultat reductionmacros_destruction(TRAVAIL(ReductionMacros) reductionmacros);
/* D�truit une structure de reduction des macros.
 */

#endif
