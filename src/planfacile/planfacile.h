/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PLANFACILE__
#define __PLANFACILE__

#include <src/global/global.h>
#include <src/donnees/general/general.h>


Resultat planfacile_planfacile(TRAVAIL(General) general, int argc, char *argv[]);
/* Fonction de compilation de planfacile.
 * Cette fonction est appell�e directement par main()
 * et s'occupe de tout.
 * La fonction renvoit un r�sultat diff�rent de RESULTAT_OK
 * en cas d'erreur.
 */

#endif
