/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "planfacile.h"
#include <src/analyseur/analyseur/analyse.h>
#include <src/messages/special/aide.h>
#include <src/messages/special/licence.h>


Resultat planfacile_planfacile(TRAVAIL(General) general, int argc, char *argv[])
{
        /* Fonction de compilation de planfacile.
         * Cette fonction est appell�e directement par main()
         * et s'occupe de tout.
         * La fonction renvoit un r�sultat diff�rent de RESULTAT_OK
         * en cas d'erreur.
         */
        TRAVAIL(Options) options;
        TRAVAIL_SCALAIRE(Booleen) option;
        SECURISE(general_initialisation(general));
        SECURISE(environnement_initialisation(CHAMP_TRAVAIL(general, environnement)));
        SECURISE(messages_localisation(general));
        SECURISE(general_ligne_commande(general,argc,argv));
        SECURISE(environnement_lecture_options(CHAMP_TRAVAIL(general, environnement),R_T(options)));
        SECURISE(options_lecture_aide(options,R_T_(option)));
        if(option==T_S_(VRAI))
        {
                SECURISE(aide_aide(CHAMP_TRAVAIL(general, environnement)));
                SECURISE(general_destruction(general));
                return RESULTAT_OK;
        }
        SECURISE(options_lecture_licence(options,R_T_(option)));
        if(option==T_S_(VRAI))
        {
                SECURISE(licence_licence(CHAMP_TRAVAIL(general, environnement)));
                SECURISE(general_destruction(general));
                return RESULTAT_OK;
        }
        SECURISE(analyse_analyseur(general));
        SECURISE(retouches_retouches(general));
        SECURISE(reductionoptions_reductionoptions(general));
        SECURISE(reductionmacros_reductionmacros(general));
        SECURISE(verification_verification(general));
        SECURISE(enregistrementstyles_enregistrementstyles(general));
        SECURISE(enregistrementidees_enregistrementidees(general));
        SECURISE(calculplan_plan(general));
        SECURISE(sortie_sortie(general));
        SECURISE(general_destruction(general));
        return RESULTAT_OK;
}

