/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __RELATIONS__
#define __RELATIONS__

#include <src/global/global.h>

#define SANSRELATION        -1

typedef struct
{
        Taille taille;
        Pertinence **relations;
} Relations;

//relations est dans le sens [source][destination]
//la taille sera lue (et uniquement lu directement)

Resultat creation_relations(Relations *relations, Taille taille);
//cree une table de relations vide a la taille demandee

Resultat destruction_relations(Relations *relations);
//destruction des relations

Resultat ajoutidees_relations(Relations *relations, Taille ajout);
//ajoute des relations dans la table.
//si taille est negative l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee

Resultat ajoutrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination, Pertinence pertinence);
//ajoute une relation dans la table
//si un des indices est incorrect, l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee
//si la pertinence est negative, l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee

Resultat suppressionrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination);
//supprime une relation
//renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect

Resultat testrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination, Booleen *relation, Pertinence *pertinence);
//place VRAI dans relation si la relation existe et fixe pertinence avec la valeur de pertinence,
//sauf si le pointeur est NULL
//place FAUX dans relation si la relation n'existe pas. dans ce cas, pertinence n'est pas touchee

#endif
