/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "marqueurs.h"

Resultat creation_marqueurs(Marqueurs *marqueurs, Taille taille)
{
        //initialise le tableau de marques
        //si taille<0, erreur RESULTAT_ERREUR_DOMAINE
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        marqueurs->taille=taille;
        if((marqueurs->marques=((Booleen*)(malloc(sizeof(Booleen)*taille))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        SECURISE(nettoyage_marqueurs(marqueurs));
        return RESULTAT_OK;
}

Resultat copie_marqueurs(Marqueurs marqueurs, Marqueurs *copie)
{
        Indice indice;
        SECURISE(creation_marqueurs(copie, marqueurs.taille));
        copie->nombremarques=marqueurs.nombremarques;
        for(indice=0 ; indice<marqueurs.taille ; ++indice)
                (copie->marques)[indice]=(marqueurs.marques)[indice];
        return RESULTAT_OK;
}

Resultat destruction_marqueurs(Marqueurs *marqueurs)
{
        //detruit les marqueurs
        free(marqueurs->marques);
        marqueurs->taille=-1;
        marqueurs->nombremarques=0;
        return RESULTAT_OK;
}

Resultat agrandissement_marqueurs(Marqueurs *marqueurs, Taille taille)
{
        //agrandissement de la taille du tableau de marques
        //taille<0 => rien ne se passe, erreur RESULTAT_ERREUR_DOMAINE
        Booleen *marquestemp;
        IdIdee ididee;
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(taille==0)
                return RESULTAT_OK;
        if((marquestemp=((Booleen*)(realloc(marqueurs->marques,sizeof(Booleen)*(marqueurs->taille+taille)))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        marqueurs->marques=marquestemp;
        for(ididee=marqueurs->taille ; ididee<marqueurs->taille+taille ; ididee++)
                marqueurs->marques[ididee]=FAUX;
        marqueurs->taille += taille;
        return RESULTAT_OK;
}

Resultat nettoyage_marqueurs(Marqueurs *marqueurs)
{
        //decoche toutes les marques
        Indice indice;
        marqueurs->nombremarques=0;
        for(indice=0 ; indice<marqueurs->taille ; indice++)
                marqueurs->marques[indice]=FAUX;
        return RESULTAT_OK;
}

Resultat nombre_marqueurs(Marqueurs *marqueurs, Indice *nombre)
{
        //place dans nombre le nombre de marques cochees
        *nombre=marqueurs->nombremarques;
        return RESULTAT_OK;
}

Resultat verification_marqueurs(Marqueurs *marqueurs, Indice indice, Booleen *marque)
{
        //place dans marque la valeur de la marque d'indice donne
        //indice incorrect => RESULTAT_ERREUR_DEPASSEMENT
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=marqueurs->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *marque=marqueurs->marques[indice];
        return RESULTAT_OK;
}

Resultat coche_marqueurs(Marqueurs *marqueurs, Indice indice)
{
        //coche une marque d'indice donne
        //indice incorrect => RESULTAT_ERREUR_DEPASSEMENT
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=marqueurs->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(marqueurs->marques[indice]==FAUX)
                marqueurs->nombremarques++;
        marqueurs->marques[indice]=VRAI;
        return RESULTAT_OK;
}

Resultat decoche_marqueurs(Marqueurs *marqueurs, Indice indice)
{
        //decoche une marque d'indice donne
        //indice incorrect => RESULTAT_ERREUR_DEPASSEMENT
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=marqueurs->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(marqueurs->marques[indice]==VRAI)
                marqueurs->nombremarques--;
        marqueurs->marques[indice]=FAUX;
        return RESULTAT_OK;
}

Resultat inclusion_marqueurs(Marqueurs *marqueursinclus, Marqueurs *marqueurs, Booleen *comparaison)
{
        //teste si marqueursinclus est inclus dans marqueurs.
        //si les marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee
        Indice indice;
        if(marqueursinclus->taille!=marqueurs->taille)
                return RESULTAT_ERREUR_TAILLE;
        *comparaison=VRAI;
        for(indice=0 ; indice<marqueurs->taille ; indice++)
                if((marqueursinclus->marques[indice]==VRAI)&&(marqueurs->marques[indice]==FAUX))
                {
                        *comparaison=FAUX;
                        return RESULTAT_OK;
                }
        return RESULTAT_OK;
}

Resultat egales_marqueurs(Marqueurs *marqueurs1, Marqueurs *marqueurs2, Booleen *comparaison)
{
        //teste si les deux ensembles de marques sont egales
        //si les marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee
        Indice indice;
        if(marqueurs1->taille!=marqueurs2->taille)
                return RESULTAT_ERREUR_TAILLE;
        *comparaison=VRAI;
        for(indice=0 ; indice<marqueurs1->taille ; indice++)
                if(marqueurs1->marques[indice]!=marqueurs2->marques[indice])
                {
                        *comparaison=FAUX;
                        return RESULTAT_OK;
                }
        return RESULTAT_OK;
}

Resultat cumul_marqueurs(Marqueurs *marqueurssomme, Marqueurs *marqueurs)
{
        //ajoute les marqueurs de marqueurs a ceux de marqueurssomme
        //si les deux marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee
        Indice indice;
        if(marqueurs->taille!=marqueurssomme->taille)
                return RESULTAT_ERREUR_TAILLE;
        for(indice=0 ; indice<marqueurs->taille ; indice++)
                if((marqueurs->marques[indice]==VRAI)&&(marqueurssomme->marques[indice]==FAUX))
                {
                        marqueurssomme->marques[indice]=VRAI;
                        marqueurssomme->nombremarques++;
                }
        return RESULTAT_OK;
}

Resultat commun_marqueurs(Marqueurs *marqueurs1, Marqueurs *marqueurs2, Indice *commun)
{
        //place dans commun le nombre de marques communes aux deux ensembles
        //si leurs tailles sont differentes, une erreur RESULTAT_ERREUR_TAILLE est renvoyee
        Indice indice;
        if(marqueurs1->taille!=marqueurs2->taille)
                return RESULTAT_ERREUR_TAILLE;
        *commun=0;
        for(indice=0 ; indice<marqueurs1->taille ; indice++)
                if((marqueurs1->marques[indice]==VRAI)&&(marqueurs2->marques[indice]==VRAI))
                        (*commun)++;
        return RESULTAT_OK;
}

Resultat affiche_marqueurs(Marqueurs marqueurs)
{
        //affiche le contenu des marqueurs /!\ DEBUG !
        Indice indice;
        printf("Marqueurs : %d/%d marques :",marqueurs.nombremarques,marqueurs.taille);
        for(indice=0 ; indice<marqueurs.taille ; indice++)
                if(marqueurs.marques[indice]==VRAI)
                        printf(" %d",indice);
        printf("\n");
        return RESULTAT_OK;
}

