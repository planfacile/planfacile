/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "contenu.h"

Resultat creation_contenu(Contenu *contenu)
{
        //creation d'un contenu vide
        if((contenu->sections=(PSection*)(malloc(sizeof(PSection)*TAILLEINIT)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        contenu->memoire=TAILLEINIT;
        contenu->taille=0;
        return RESULTAT_OK;
}

Resultat destruction_contenu(Contenu *contenu)
{
        //detruit le contenu
        free(contenu->sections);
        return RESULTAT_OK;
}

Resultat taille_contenu(Contenu *contenu, Taille *taille)
{
        //renvoie le nombre d'elements du contenu
        *taille=contenu->taille;
        return RESULTAT_OK;
}

static Resultat ajoutfinvide_contenu(Contenu *contenu)
{
        //fait en sorte de laisser un emplacement disponible dans la liste
        PSection *sectionstemp;
        if(contenu->taille==contenu->memoire)
        {
                //plus assez d'emplacements : on en redemande...
                if((sectionstemp=(PSection*)(realloc(contenu->sections,sizeof(PSection)*(contenu->memoire)*MULTTAILLE)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                contenu->sections=sectionstemp;
                contenu->memoire *= MULTTAILLE;
        }
        //la, on est surs d'avoir la place qu'il faut
        return RESULTAT_OK;
}

Resultat ajoutsection_contenu(Contenu *contenu, PSection section)
{
        //ajoute une section a la fin du contenu
        SECURISE(ajoutfinvide_contenu(contenu));
        contenu->sections[contenu->taille++]=section;
        return RESULTAT_OK;
}

Resultat echange_contenu(Contenu *contenu, Indice indice1, Indice indice2)
{
        //echange les deux sections placees aux indices indiques
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si un des indices est incorrect
        PSection sectiontemp;
        if(indice1<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice1>=contenu->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice2<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice2>=contenu->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        sectiontemp=contenu->sections[indice1];
        contenu->sections[indice1]=contenu->sections[indice2];
        contenu->sections[indice2]=sectiontemp;
        return RESULTAT_OK;
}

Resultat lecture_contenu(Contenu *contenu, Indice indice, PSection *section)
{
        //place la valeur de la section dans le pointeur
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=contenu->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *section=contenu->sections[indice];
        return RESULTAT_OK;
}

Resultat suppression_contenu(Contenu *contenu, Indice indice)
{
        //supprime le lien sur la section dans le contenu
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=contenu->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        contenu->taille--;
        for( ; indice<contenu->taille ; indice++)
                contenu->sections[indice]=contenu->sections[indice+1];
        return RESULTAT_OK;
}


