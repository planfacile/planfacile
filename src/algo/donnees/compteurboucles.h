/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __COMPTEURBOUCLES__
#define __COMPTEURBOUCLES__

#include <src/global/global.h>

#include <src/algo/donnees/marqueurs.h>

typedef struct
{
        Taille taille;
        //taille
        Indice *compteurs;
        //compteurs
} CompteurBoucles;

Resultat creation_compteurboucles(CompteurBoucles *compteurboucles, Taille taille);
//cree une liste de compteurs de boucle
//renvoie RESULTAT_ERREUR_DOMAINE en cas de taille negative

Resultat destruction_compteurboucles(CompteurBoucles *compteurboucles);
//detruit un compteur de boucle

Resultat ajoutcompteurs_compteurboucles(CompteurBoucles *compteurboucles, Taille ajout);
//ajoute des compteurs dans la liste
//renvoie RESULTAT_ERREUR_DOMAINE en cas d'ajout negatif

Resultat lecturecompteur_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice *compteur);
//lit un compteur
//renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect

Resultat ecriturecompteur_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice compteur);
//ecrit un compteur
//renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
//renvoie RESULTAT_ERREUR_DOMAINE en cas de compteur negatif

Resultat incremente_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee);
//incremente un compteur de boucle
//renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect

Resultat decremente_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee);
//decremente un compteur de boucle
//renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
//renvoie RESULTAT_ERREUR_DOMAINE en cas de compteur nul

Resultat ajoute_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice ajout);
//ajoute la valeur ajout au compteur
//si l'indice est incorrect, RESULTAT_ERREUR_DEPASSEMENT est renvoyee
//si le compteur devient negatif, RESULTAT_ERREUR_DOMAINE est renvoyee

Resultat incrementeboucle_compteurboucles(CompteurBoucles *compteurboucles, Marqueurs boucle);
//incremente un compteur de boucle sur toute une boucle
//renvoie RESULTAT_ERREUR_TAILLE en cas de tailles differentes

Resultat cumul_compteurboucles(CompteurBoucles *compteurbouclessomme, CompteurBoucles *compteurboucles);
//ajoute les valeurs des compteurs au compteurs somme
//renvoie RESULTAT_ERREUR_TAILLE si les compteurs n'ont pas la meme taille

Resultat commun_compteurboucles(CompteurBoucles *compteurboucles, Marqueurs *marqueurs, Indice *commun);
//place dans commun la somme des compteurs dont une marque est presente dans la liste des marqueurs
//renvoie RESULTAT_ERREUR_TAILLE en cas de taille differentes

Resultat affiche_compteurboucles(CompteurBoucles compteurboucles, Taille bloc);
//affiche le contenu des compteurs de boucle /!\ DEBUG

#endif
