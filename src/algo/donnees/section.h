/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __SECTION__
#define __SECTION__

#include <src/global/global.h>
#include <src/algo/donnees/contenu.h>
#include <src/algo/donnees/reference.h>
#include <src/algo/donnees/liens.h>

typedef struct section
{
        IdIdee ididee;
        //indice de l'idee dans idees
        NiveauHierarchique niveauhierarchique;
        //niveau hierarchique dans le plan
        Contenu soussections;
        //vecteur des sous sections de la section en cours
        Reference references;
        //vecteur des references emanant de cette section
} Section;

typedef Section* Plan;

Resultat creation_section(Section **section, IdIdee ididee, Contenu soussections, Reference references);
//cree une section a partir de ses elements constitutifs

Resultat destruction_section(Plan plan);
//detruit le plan donne = detruit la section et tout le sous arbre

Resultat lectureididee_section(Section *section, IdIdee *ididee);
//lit l'identificateur de l'idee de la section

Resultat lectureniveauhierarchique_section(Section *section, NiveauHierarchique *niveauhierarchique);
//lecture du niveau hierarchique

Resultat ecritureniveauhierarchique_section(Section *section, NiveauHierarchique niveauhierarchique);
//ecriture du niveau hierarchique

Resultat lecturesoussections_section(Section *section, Contenu *soussections);
//lecture des sous sections

Resultat ecrituresoussections_section(Section *section, Contenu soussections);
//ecriture des sous sections => remplacement propre

Resultat lecturereferences_section(Section *section, Reference *references);
//lecture des references

Resultat ecriturereferences_section(Section *section, Reference references);
//ecriture des references => remplacement propre

#endif
