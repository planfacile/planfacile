/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "liens.h"

Resultat creation_liens(Liens *liens, Taille taille)
{
        //cree les liens avec la taille specifiee
        Indice indice, indiceecrit;
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        liens->taille=taille;
        if((liens->liste=(Indice*)(malloc(sizeof(Indice)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=0 ; indice<taille ; indice++)
                liens->liste[indice]=indice;
        if((liens->table=(References**)(malloc(sizeof(References*)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=0 ; indice<taille ; indice++)
        {
                if((liens->table[indice]=(References*)(malloc(sizeof(References)*taille)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                for(indiceecrit=0 ; indiceecrit<liens->taille ; indiceecrit++)
                {
                        liens->table[indice][indiceecrit].referencesreductibles=0;
                        liens->table[indice][indiceecrit].referencesirreductibles=0;
                }
        }
        liens->references.referencesreductibles=0;
        liens->references.referencesirreductibles=0;
        return RESULTAT_OK;
}

Resultat destruction_liens(Liens *liens)
{
        //detruit les liens
        Indice indice;
        for(indice=0 ; indice<liens->taille ; indice++)
                free(liens->table[indice]);
        free(liens->table);
        free(liens->liste);
        return RESULTAT_OK;
}

static Resultat ajoutreferences_liens(References *references, References avant, References apres)
{
        references->referencesreductibles += apres.referencesreductibles-avant.referencesreductibles;
        references->referencesirreductibles += apres.referencesirreductibles-avant.referencesirreductibles;
        return RESULTAT_OK;
}

Resultat lecture_liens(Liens *liens, Indice source, Indice destination, References *references)
{
        //lit une valeur de references entre deux sous arbres
        //RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect
        if(source<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(source>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        source=liens->liste[source];
        destination=liens->liste[destination];
        if(source<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(source>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *references=liens->table[source][destination];
        return RESULTAT_OK;

}

Resultat ecriture_liens(Liens *liens, Indice source, Indice destination, References references)
{
        //ecrit une valeur de references entre deux sous arbres
        //RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect
        if(source<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(source>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(destination>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        SECURISE(ajoutreferences_liens(&(liens->references),liens->table[source][destination],references));
        liens->table[source][destination]=references;
        return RESULTAT_OK;
}

Resultat test_liens(Liens *liens, Indice avant, Indice apres, Booleen *comparaison)
{
        //teste si deux contenus ont interet a etre interchanges
        //RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect
        References refavant, refapres;
        Indice resultatfonctioncaracteristique;
        Indice indice;
        if(avant<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(avant>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(apres<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(apres>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        avant=liens->liste[avant];
        apres=liens->liste[apres];
        if(avant<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(avant>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(apres<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(apres>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        refavant=liens->table[avant][apres];
        refapres=liens->table[apres][avant];
        //*comparaison=((refapres.referencesirreductibles+refavant.referencesirreductibles)>=(refavant.referencesreductibles+refapres.referencesreductibles))?VRAI:FAUX;
        //test local
        resultatfonctioncaracteristique=(refapres.referencesirreductibles+refavant.referencesirreductibles)-(refavant.referencesreductibles+refapres.referencesreductibles);
        if(resultatfonctioncaracteristique>0)
        {
                *comparaison=VRAI;
                return RESULTAT_OK;
        }
        if(resultatfonctioncaracteristique<0)
        {
                *comparaison=FAUX;
                return RESULTAT_OK;
        }
        //test global
        refavant.referencesreductibles=0;
        refavant.referencesirreductibles=0;
        for(indice=0 ; indice<liens->taille ; indice++)
        {
                if(indice!=avant)
                {
                        refavant.referencesreductibles+=liens->table[avant][indice].referencesreductibles;
                        refavant.referencesirreductibles+=liens->table[avant][indice].referencesirreductibles;
                }
        }
        refapres.referencesreductibles=0;
        refapres.referencesirreductibles=0;
        for(indice=0 ; indice<liens->taille ; indice++)
        {
                if(indice!=apres)
                {
                        refapres.referencesreductibles+=liens->table[apres][indice].referencesreductibles;
                        refapres.referencesirreductibles+=liens->table[apres][indice].referencesirreductibles;
                }
        }
        resultatfonctioncaracteristique=((refavant.referencesirreductibles-refavant.referencesreductibles)+(refapres.referencesreductibles-refapres.referencesreductibles));
        if(resultatfonctioncaracteristique>0)
        {
                *comparaison=VRAI;
        }        
        else
        {
                *comparaison=FAUX;
        }
        return RESULTAT_OK;
}

Resultat echange_liens(Liens *liens, Indice contenu1, Indice contenu2)
{
        //echange deux contenus dans les liens
        //RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect
        Indice reftemp;
        Indice indicecontenu1, indicecontenu2;
        References refcontenu1, refcontenu2;
        if(contenu1<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu1>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu2<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu2>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        indicecontenu1=contenu1;
        contenu1=liens->liste[contenu1];
        indicecontenu2=contenu2;
        contenu2=liens->liste[contenu2];
        if(contenu1<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu1>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu2<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(contenu2>=liens->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        //inverse les valeurs
        refcontenu1=liens->table[contenu1][contenu2];
        reftemp=refcontenu1.referencesreductibles;
        refcontenu1.referencesreductibles=refcontenu1.referencesirreductibles;
        refcontenu1.referencesirreductibles=reftemp;
        refcontenu2=liens->table[contenu2][contenu1];
        reftemp=refcontenu2.referencesreductibles;
        refcontenu2.referencesreductibles=refcontenu2.referencesirreductibles;
        refcontenu2.referencesirreductibles=reftemp;
        //met la table a jour ainsi que la somme des references
        SECURISE(ajoutreferences_liens(&(liens->references),liens->table[contenu1][contenu2],refcontenu1));
        liens->table[contenu1][contenu2]=refcontenu1;
        SECURISE(ajoutreferences_liens(&(liens->references),liens->table[contenu2][contenu1],refcontenu2));
        liens->table[contenu2][contenu1]=refcontenu2;
        //permute dans  la liste
        reftemp=liens->liste[indicecontenu1];
        liens->liste[indicecontenu1]=liens->liste[indicecontenu2];
        liens->liste[indicecontenu2]=reftemp;
        return RESULTAT_OK;
}

Resultat references_liens(Liens *liens, References *references)
{
        //place dans references la somme des references des liens
        *references=liens->references;
        return RESULTAT_OK;
}

Resultat affiche_liens(Liens liens)
{
        //affiche le contenu des liens /!\ DEBUG
        Indice indice1, indice2;
        References references;
        printf("Liens : (taille=%d) : References = (%3d ; %3d)\nListe :",liens.taille,liens.references.referencesreductibles,liens.references.referencesirreductibles);
        for(indice1=0 ; indice1<liens.taille ; indice1++)
                printf(" %d",liens.liste[indice1]);
        printf("\n");
        for(indice1=0 ; indice1<liens.taille ; indice1++)
        {
                for(indice2=0 ; indice2<liens.taille ; indice2++)
                {
                        references=liens.table[liens.liste[indice1]][liens.liste[indice2]];
                        printf("(%3d ; %3d) ",references.referencesreductibles,references.referencesirreductibles);
                }
                printf("\n");
        }
        printf("Fin des liens.\n");
        return RESULTAT_OK;
}

