/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LISTEPEREPERTINENT__
#define __LISTEPEREPERTINENT__

#include <src/global/global.h>

#define ORPHELIN        -1

typedef struct
{
        IdIdee idperepertinent;
        Pertinence pertinence;
} PerePertinent;

typedef struct
{
        Taille taille;
        //taille du tableau
        PerePertinent *perepertinent;
        //pour chaque ididee, donne l'id de l'idee parente la plus
        //pertinente. Si l'idee est orpheline, la pertinence est
        //indefinie
} ListePerePertinent;

Resultat creation_listeperepertinent(ListePerePertinent *listeperepertinent, Taille taille);
//cree la liste avec la taille specifiee
//l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee en cas de taille negative

Resultat destruction_listeperepertinent(ListePerePertinent *listeperepertinent);
//detruit la liste

Resultat ajoutidee_listeperepertinent(ListePerePertinent *listeperepertinent, Taille ajout);
//a priori inutile, mais bon
//ajoute des idees dans la liste
//l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee en cas d'ajout negatif

Resultat ecriturepere_listeperepertinent(ListePerePertinent *listeperepertinent, IdIdee idfils, IdIdee pere, Pertinence pertinence);
//met a jour la liste des peres pertinents si la pertinence est egale ou superieure pour ce pere, ou si
//le fils etait orphelin
//renvoie une erreur RESULTAT_ERREUR_DEPASSEMENT si l'idfils n'est pas correct, et RESULTAT_ERREUR_DOMAINE si le pere ou la pertinence
//sont incorrects

Resultat lecturepere_listeperepertinent(ListePerePertinent *listeperepertinent, IdIdee fils, Booleen *orphelin, IdIdee *pere);
//renvoie le pere le plus pertinent du fils indique
//si orphelin est VRAI, le contenu de pere est indefini, sinon, sauf si pere est egal a NULL
//pere contient la valeur du pere le plus pertinent
//renvoie l'erreur RESULTAT_ERREUR_DEPASSEMENT si l'identificateur du fils est incorrect

#endif
