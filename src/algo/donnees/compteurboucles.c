/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "compteurboucles.h"

Resultat creation_compteurboucles(CompteurBoucles *compteurboucles, Taille taille)
{
        //cree une liste de compteurs de boucle
        //renvoie RESULTAT_ERREUR_DOMAINE en cas de taille negative
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        compteurboucles->taille=taille;
        if((compteurboucles->compteurs=(Indice*)(malloc(sizeof(Indice)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for( ; taille>0 ; taille--)
                compteurboucles->compteurs[taille-1]=0;
        return RESULTAT_OK;
}

Resultat destruction_compteurboucles(CompteurBoucles *compteurboucles)
{
        //detruit un compteur de boucle
        free(compteurboucles->compteurs);
        return RESULTAT_OK;
}

Resultat ajoutcompteurs_compteurboucles(CompteurBoucles *compteurboucles, Taille ajout)
{
        //ajoute des compteurs dans la liste
        //renvoie RESULTAT_ERREUR_DOMAINE en cas d'ajout negatif
        IdIdee ididee;
        if(ajout<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(ajout==0)
                return RESULTAT_OK;
        if((compteurboucles->compteurs=(Indice*)(realloc(compteurboucles->compteurs,sizeof(Indice)*(compteurboucles->taille+ajout))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(ididee=compteurboucles->taille ; ididee<compteurboucles->taille+ajout ; ididee++)
                compteurboucles->compteurs[ididee]=0;
        compteurboucles->taille += ajout;
        return RESULTAT_OK;
}

Resultat lecturecompteur_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice *compteur)
{
        //lit un compteur
        //renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=compteurboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *compteur=compteurboucles->compteurs[ididee];
        return RESULTAT_OK;
}

Resultat ecriturecompteur_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice compteur)
{
        //ecrit un compteur
        //renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
        //renvoie RESULTAT_ERREUR_DOMAINE en cas de compteur negatif
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=compteurboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        compteurboucles->compteurs[ididee]=compteur;
        return RESULTAT_OK;
}

Resultat incremente_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee)
{
        //incremente un compteur de boucle
        //renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=compteurboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        compteurboucles->compteurs[ididee]++;
        return RESULTAT_OK;
}

Resultat decremente_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee)
{
        //decremente un compteur de boucle
        //renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
        //renvoie RESULTAT_ERREUR_DOMAINE en cas de compteur nul
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=compteurboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(compteurboucles->compteurs[ididee]==0)
                return RESULTAT_ERREUR_DOMAINE;
        compteurboucles->compteurs[ididee]--;
        return RESULTAT_OK;
}

Resultat ajoute_compteurboucles(CompteurBoucles *compteurboucles, IdIdee ididee, Indice ajout)
{
        //ajoute la valeur ajout au compteur
        //si l'indice est incorrect, RESULTAT_ERREUR_DEPASSEMENT est renvoyee
        //si le compteur devient negatif, RESULTAT_ERREUR_DOMAINE est renvoyee
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=compteurboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(compteurboucles->compteurs[ididee]+ajout<0)
                return RESULTAT_ERREUR_DOMAINE;
        compteurboucles->compteurs[ididee]+=ajout;
        return RESULTAT_OK;
}

Resultat incrementeboucle_compteurboucles(CompteurBoucles *compteurboucles, Marqueurs boucle)
{
        //incremente un compteur de boucle sur toute une boucle
        //renvoie RESULTAT_ERREUR_TAILLE en cas de tailles differentes
        IdIdee ididee;
        if(boucle.taille!=compteurboucles->taille)
                return RESULTAT_ERREUR_TAILLE;
        for(ididee=0 ; ididee<compteurboucles->taille ; ididee++)
        {
                Booleen test;
                SECURISE(verification_marqueurs(&boucle,ididee,&test));
                if(test==VRAI)
                        compteurboucles->compteurs[ididee]++;
        }
        return RESULTAT_OK;
}

Resultat cumul_compteurboucles(CompteurBoucles *compteurbouclessomme, CompteurBoucles *compteurboucles)
{
        //ajoute les valeurs des compteurs au compteurs somme
        //renvoie RESULTAT_ERREUR_TAILLE si les compteurs n'ont pas la meme taille
        Indice indice;
        if(compteurbouclessomme->taille!=compteurboucles->taille)
                return RESULTAT_ERREUR_TAILLE;
        for(indice=0 ; indice<compteurboucles->taille ; indice++)
                compteurbouclessomme->compteurs[indice] += compteurboucles->compteurs[indice];
        return RESULTAT_OK;
}

Resultat commun_compteurboucles(CompteurBoucles *compteurboucles, Marqueurs *marqueurs, Indice *commun)
{
        //place dans commun la somme des compteurs dont une marque est presente dans la liste des marqueurs
        //renvoie RESULTAT_ERREUR_TAILLE en cas de taille differentes
        Indice indice;
        if(marqueurs->taille!=compteurboucles->taille)
                return RESULTAT_ERREUR_TAILLE;
        *commun=0;
        for(indice=0 ; indice<compteurboucles->taille ; indice++)
        {
                Booleen test;
                SECURISE(verification_marqueurs(marqueurs,(IdIdee)(indice),&test));
                if(test==VRAI)
                        *commun += compteurboucles->compteurs[indice];
        }
        return RESULTAT_OK;
}

Resultat affiche_compteurboucles(CompteurBoucles compteurboucles, Taille bloc)
{
        //affiche le contenu des compteurs de boucle /!\ DEBUG
        Indice indice;
        printf("Compteurs de boucle (taille=%d) :\n",compteurboucles.taille);
        for(indice=0 ; indice<compteurboucles.taille ; indice++)
                printf("%3d%c",compteurboucles.compteurs[indice],((indice+1)%bloc==0)?'\n':' ');
        printf("\nFin des compteurs de boucle\n");
        return RESULTAT_OK;
}
