/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __IDEE__
#define __IDEE__

#include <src/global/global.h>

#define INDICE_IDEE_INVALIDE        -1

typedef struct
{
        TypeIdee type;
        //Type de l'idee.
        Indice manquante;
        //Indice dans le cas
        //ou l'idee est manquante.
        IdIdee generique;
        //Identifiant de l'idee parente
        //dans le cas ou l'idee est
        //generique.
} Idee;

Resultat creation_idee(Idee *idee, TypeIdee type, Indice manquante, IdIdee generique);
//cree une idee

Resultat destruction_idee(Idee *idee);
//detruit l'idee

//les operations d'ecriture ne sont pas permises...
//la lecture se fera directement sur la structure

#endif
