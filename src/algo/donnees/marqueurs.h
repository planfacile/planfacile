/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __MARQUEURS__
#define __MARQUEURS__

#include <src/global/global.h>

typedef struct
{
        Taille taille;
        //nombre de marques possible (lue en direct, mais non modifiable)
        Taille nombremarques;
        //nombre de marques actuellement prises
        Booleen *marques;
        //tableau de marques
} Marqueurs;

Resultat creation_marqueurs(Marqueurs *marqueurs, Taille taille);
//initialise le tableau de marques
//si taille<0, erreur RESULTAT_ERREUR_DOMAINE

Resultat copie_marqueurs(Marqueurs marqueurs, Marqueurs *copie);
//copie le tableau de marques

Resultat destruction_marqueurs(Marqueurs *marqueurs);
//detruit les marqueurs

Resultat agrandissement_marqueurs(Marqueurs *marqueurs, Taille taille);
//agrandissement de la taille du tableau de marques
//taille<0 => rien ne se passe, erreur RESULTAT_ERREUR_DOMAINE

Resultat nettoyage_marqueurs(Marqueurs *marqueurs);
//decoche toutes les marques

Resultat nombre_marqueurs(Marqueurs *marqueurs, Indice *nombre);
//place dans nombre le nombre de marques cochees

Resultat verification_marqueurs(Marqueurs *marqueurs, Indice indice, Booleen *marque);
//place dans marque la valeur de la marque d'indice donne
//indice incorrect => RESULTAT_ERREUR_DEPASSEMENT

Resultat coche_marqueurs(Marqueurs *marqueurs, Indice indice);
//coche une marque d'indice donne
//indice incorrect => RESULTAT_ERREUR_DEPASSEMENT

Resultat decoche_marqueurs(Marqueurs *marqueurs, Indice indice);
//decoche une marque d'indice donne
//indice incorrect => RESULTAT_ERREUR_DEPASSEMENT

Resultat inclusion_marqueurs(Marqueurs *marqueursinclus, Marqueurs *marqueurs, Booleen *comparaison);
//teste si marqueursinclus est inclus dans marqueurs.
//si les marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee

Resultat egales_marqueurs(Marqueurs *marqueurs1, Marqueurs *marqueurs2, Booleen *comparaison);
//teste si les deux ensembles de marques sont egales
//si les marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee

Resultat cumul_marqueurs(Marqueurs *marqueurssomme, Marqueurs *marqueurs);
//ajoute les marqueurs de marqueurs a ceux de marqueurssomme
//si les deux marqueurs n'ont pas la meme taille, une erreur RESULTAT_ERREUR_TAILLE est renvoyee

Resultat commun_marqueurs(Marqueurs *marqueurs1, Marqueurs *marqueurs2, Indice *commun);
//place dans commun le nombre de marques communes aux deux ensembles
//si leurs tailles sont differentes, une erreur RESULTAT_ERREUR_TAILLE est renvoyee

Resultat affiche_marqueurs(Marqueurs marqueurs);
//affiche le contenu des marqueurs /!\ DEBUG !

#endif
