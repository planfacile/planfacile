/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "reference.h"

Resultat creation_reference(Reference *reference)
{
        //creation d'un reference vide
        if((reference->destination=(ReferenceComplete*)(malloc(sizeof(ReferenceComplete)*TAILLEINIT)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        reference->memoire=TAILLEINIT;
        reference->taille=0;
        return RESULTAT_OK;
}

Resultat destruction_reference(Reference *reference)
{
        //detruit le reference
        free(reference->destination);
        return RESULTAT_OK;
}

Resultat taille_reference(Reference *reference, Taille *taille)
{
        //renvoie le nombre d'elements du reference
        *taille=reference->taille;
        return RESULTAT_OK;
}

static Resultat ajoutfinvide_reference(Reference *reference)
{
        //fait en sorte de laisser un emplacement disponible dans la liste
        ReferenceComplete *destinationtemp;
        if(reference->taille==reference->memoire)
        {
                //plus assez d'emplacements : on en redemande...
                if((destinationtemp=(ReferenceComplete*)(realloc(reference->destination,sizeof(ReferenceComplete)*(reference->memoire)*MULTTAILLE)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                reference->destination=destinationtemp;
                reference->memoire *= MULTTAILLE;
        }
        //la, on est surs d'avoir la place qu'il faut
        return RESULTAT_OK;
}

Resultat ajoutreference_reference(Reference *reference, TypeReference type, P2Section section)
{
        //ajoute une section a la fin du reference
        SECURISE(ajoutfinvide_reference(reference));
        reference->destination[reference->taille].type=type;
        reference->destination[reference->taille++].section=section;
        return RESULTAT_OK;
}

Resultat echange_reference(Reference *reference, Indice indice1, Indice indice2)
{
        //echange les deux destination placees aux indices indiques
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si un des indices est incorrect
        ReferenceComplete sectiontemp;
        if(indice1<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice1>=reference->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice2<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice2>=reference->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        sectiontemp=reference->destination[indice1];
        reference->destination[indice1]=reference->destination[indice2];
        reference->destination[indice2]=sectiontemp;
        return RESULTAT_OK;
}

Resultat lecture_reference(Reference *reference, Indice indice, TypeReference *type, P2Section *section)
{
        //place la valeur de la section dans le pointeur
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=reference->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *type=reference->destination[indice].type;
        *section=reference->destination[indice].section;
        return RESULTAT_OK;
}

Resultat ecrituretype_reference(Reference *reference, Indice indice, TypeReference type)
{
        //ecrit un type de reference
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=reference->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        reference->destination[indice].type=type;
        return RESULTAT_OK;
}

Resultat suppression_reference(Reference *reference, Indice indice)
{
        //supprime le lien sur la section dans le reference
        //renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=reference->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        reference->taille--;
        for( ; indice<reference->taille ; indice++)
                reference->destination[indice]=reference->destination[indice+1];
        return RESULTAT_OK;
}


