/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LISTEIDEES__
#define __LISTEIDEES__

#include <src/global/global.h>

#include <src/algo/donnees/idee.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

typedef struct
{
        Taille memoire;
        //place prise en memoire
        Taille taille;
        //nombre d'elements utilises
        Idee *idees;
        //vecteur d'idees
} ListeIdees;

Resultat creation_listeidees(ListeIdees *listeidees);
//cree une liste d'idees

Resultat destruction_listeidees(ListeIdees *listeidees);
//detruit la liste d'idees et les idees contenues

Resultat taille_listeidees(ListeIdees *listeidees, Taille *taille);
//renvoie la taille de la liste

Resultat ajoutidee_listeidees(ListeIdees *listeidees, Idee idee);
//ajoute une idee a la liste

Resultat lectureidee_listeidees(ListeIdees *listeidees, IdIdee ididee, Idee *idee);
//place dans la variable idee la valeur de l'idee contenue a l'emplacement ididee
//si l'indice est incorrect, une erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee

Resultat ecritureidee_listeidees(ListeIdees *listeidees, IdIdee ididee, Idee idee);
//ecrit l'idee a l'emplacement ididee dans listeidees
//si l'indice est incorrect, une erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee

#endif
