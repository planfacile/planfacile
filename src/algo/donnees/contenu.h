/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __CONTENU__
#define __CONTENU__

#include <src/global/global.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

typedef struct section* PSection;

typedef struct contenu
{
        Taille memoire;
        //taille prise en memoire
        Taille taille;
        //taille reellement prise
        PSection *sections;
} Contenu;

Resultat creation_contenu(Contenu *contenu);
//creation d'un contenu vide

Resultat destruction_contenu(Contenu *contenu);
//detruit le contenu

Resultat taille_contenu(Contenu *contenu, Taille *taille);
//renvoie le nombre d'elements du contenu

Resultat ajoutsection_contenu(Contenu *contenu, PSection section);
//ajoute une section a la fin du contenu

Resultat echange_contenu(Contenu *contenu, Indice indice1, Indice indice2);
//echange les deux sections placees aux indices indiques
//renvoie RESULTAT_ERREUR_DEPASSEMENT si un des indices est incorrect

Resultat lecture_contenu(Contenu *contenu, Indice indice, PSection *section);
//place la valeur de la section dans le pointeur
//renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect

Resultat suppression_contenu(Contenu *contenu, Indice indice);
//supprime le lien sur la section dans le contenu
//renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect

#endif
