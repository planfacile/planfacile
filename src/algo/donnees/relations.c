/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "relations.h"

Resultat creation_relations(Relations *relations, Taille taille)
{
        //cree une table de relations vide a la taille demandee
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        Indice indice, indiceecrit;
        if((relations->relations=(Pertinence**)(malloc(sizeof(Pertinence*)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=0 ; indice<taille ; indice++)
        {
                if((relations->relations[indice]=(Pertinence*)(malloc(sizeof(Pertinence)*taille)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                for(indiceecrit=0 ; indiceecrit<taille ; indiceecrit++)
                        relations->relations[indice][indiceecrit]=SANSRELATION;
        }
        relations->taille=taille;
        return RESULTAT_OK;
}

Resultat destruction_relations(Relations *relations)
{
        //destruction des relations
        Indice indice;
        for(indice=0 ; indice<relations->taille ; indice++)
                free(relations->relations[indice]);
        free(relations->relations);
        return RESULTAT_OK;
}

Resultat ajoutidees_relations(Relations *relations, Taille ajout)
{
        //ajoute des relations dans la table.
        //si taille est negative l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee
        Indice indice, indiceecrit;
        Taille nouvelletaille;
        if(ajout<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(ajout==0)
                return RESULTAT_OK;
        nouvelletaille=relations->taille+ajout;
        if((relations->relations=(Pertinence**)(realloc(relations->relations,sizeof(Pertinence*)*nouvelletaille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=0 ; indice<relations->taille ; indice++)
        {
                if((relations->relations[indice]=(Pertinence*)(realloc(relations->relations[indice],sizeof(Pertinence)*nouvelletaille)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                for(indiceecrit=relations->taille ; indiceecrit<nouvelletaille ; indiceecrit++)
                        relations->relations[indice][indiceecrit]=SANSRELATION;
        }
        for(indice=relations->taille ; indice<nouvelletaille ; indice++)
        {
                if((relations->relations[indice]=(Pertinence*)(malloc(sizeof(Pertinence)*nouvelletaille)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                for(indiceecrit=0 ; indiceecrit<nouvelletaille ; indiceecrit++)
                        relations->relations[indice][indiceecrit]=SANSRELATION;
        }
        relations->taille=nouvelletaille;
        return RESULTAT_OK;
}

Resultat ajoutrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination, Pertinence pertinence)
{
        //ajoute une relation dans la table
        //si un des indices est incorrect, l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee
        //si la pertinence est negative, l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee
        if(pertinence<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(ideesource<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideesource>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(relations->relations[ideesource][ideedestination]==SANSRELATION)
                relations->relations[ideesource][ideedestination]=pertinence;
        else
                relations->relations[ideesource][ideedestination] += pertinence;
        return RESULTAT_OK;
}

Resultat suppressionrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination)
{
        //supprime une relation
        //renvoie RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrect
        if(ideesource<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideesource>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        relations->relations[ideesource][ideedestination]=SANSRELATION;
        return RESULTAT_OK;
}

Resultat testrelation_relations(Relations *relations, IdIdee ideesource, IdIdee ideedestination, Booleen *relation, Pertinence *pertinence)
{
        //place VRAI dans relation si la relation existe et fixe pertinence avec la valeur de pertinence,
        //sauf si le pointeur est NULL
        //place FAUX dans relation si la relation n'existe pas. dans ce cas, pertinence n'est pas touchee
        if(ideesource<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideesource>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ideedestination>=relations->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(relations->relations[ideesource][ideedestination]==SANSRELATION)
                *relation=FAUX;
        else
        {
                if(pertinence!=NULL)
                        *pertinence=relations->relations[ideesource][ideedestination];
                *relation=VRAI;
        }
        return RESULTAT_OK;
}

