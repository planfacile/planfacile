/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "listedependence.h"

Resultat creation_listedependences(ListeDependences *listedependences)
{
        //cree une liste de dependences...
        //bon, j'admets, une fonction pour ca, c'est se foutre de la gueule du monde.
        //mais ca fait plus propre
        *listedependences=NULL;
        return RESULTAT_OK;
}

Resultat destruction_listedependences(ListeDependences *listedependences)
{
        //detruit la liste des dependences
        if(*listedependences!=NULL)
                SECURISE(destruction_listedependences(&((*listedependences)->precedente)));
        free(*listedependences);
        return RESULTAT_OK;
}

Resultat empiledependence_listedependences(ListeDependences *listedependences, IdIdee idideesource, IdIdee idideedestination, Pertinence pertinence)
{
        //empile une dependence
        MaillonDependences *maillon;
        if((maillon=(MaillonDependences*)(malloc(sizeof(MaillonDependences))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        maillon->idideesource=idideesource;
        maillon->idideedestination=idideedestination;
        maillon->pertinence=pertinence;
        maillon->precedente=*listedependences;
        *listedependences=maillon;
        return RESULTAT_OK;
}

Resultat depiledependence_listedependences(ListeDependences *listedependences, IdIdee *idideesource, IdIdee *idideedestination, Pertinence *pertinence)
{
        //depile une dependence
        MaillonDependences *maillon=*listedependences;
        *listedependences=maillon->precedente;
        *idideesource=maillon->idideesource;
        *idideedestination=maillon->idideedestination;
        *pertinence=maillon->pertinence;
        free(maillon);
        return RESULTAT_OK;
}


//empile une dependence

//depile une dependence

