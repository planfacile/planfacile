/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __PARCOURS__
#define __PARCOURS__

#include <src/global/global.h>

#include <src/algo/donnees/marqueurs.h>

typedef struct
{
        Taille memoire;
        //taille en memoire
        Taille taille;
        //nombre de places prises
        IdIdee *ididee;
        //les ididees parcourus
} Parcours;

Resultat creation_parcours(Parcours *parcours, Taille taille);
//cree un parcours vide
//si taille est negatif, une erreur RESULTAT_ERREUR_DOMAINE est renvoyee

Resultat destruction_parcours(Parcours *parcours);
//supprime le parcours

Resultat empileididee_parcours(Parcours *parcours, IdIdee ididee);
//empile un identificateur d'idee
//renvoie RESULTAT_ERREUR_DEPASSEMENT si on depasse la taille maximale

Resultat depileididee_parcours(Parcours *parcours);
//libere le dernier identificateur d'idee empile
//renvoie RESULTAT_ERREUR_DEPASSEMENT si il n'y a plus d'identificateur a depiler

Resultat marqueboucle_parcours(Parcours *parcours, IdIdee idideedepart, Marqueurs *boucle);
//place a vrai les marques des idees formant la boucle dont idideedepart est le depart
//renvoie RESULTAT_ERREUR_NONTROUVE si aucune boucle n'est detectee

Resultat affiche_parcours(Parcours parcours);
//affiche le contenu d'un parcours /!\ DEBUG

#endif
