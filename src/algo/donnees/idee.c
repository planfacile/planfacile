/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "idee.h"

Resultat creation_idee(Idee *idee, TypeIdee type, Indice manquante, IdIdee generique)
{
        //cree une idee
        idee->type=type;
        idee->manquante=manquante;
        idee->generique=generique;
        return RESULTAT_OK;
}

Resultat destruction_idee(Idee *idee)
{
        //detruit l'idee
        return RESULTAT_OK;
}

//les operations d'ecriture ne sont pas permises...
//la lecture se fera directement sur la structure

