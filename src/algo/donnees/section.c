/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "section.h"

Resultat creation_section(Section **section, IdIdee ididee, Contenu soussections, Reference references)
{
        //cree une section a partir de ses elements constitutifs
        if((*section=(Section*)(malloc(sizeof(Section))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        (*section)->ididee=ididee;
        (*section)->niveauhierarchique=0;
//        (*section)->hauteur=0;
        (*section)->soussections=soussections;
        (*section)->references=references;
        return RESULTAT_OK;
}

Resultat destruction_section(Plan plan)
{
        //detruit le plan donne = detruit la section et tout le sous arbre
        Indice indice;
        Taille taille;
        SECURISE(destruction_reference(&(plan->references)));
        SECURISE(taille_contenu(&(plan->soussections),&taille));
        for(indice=0 ; indice<taille ; indice++)
        {
                Section *section;
                SECURISE(lecture_contenu(&(plan->soussections),indice,&section));
                SECURISE(destruction_section(section));
        }
        SECURISE(destruction_contenu(&(plan->soussections)));
        free(plan);
        return RESULTAT_OK;        
}

Resultat lectureididee_section(Section *section, IdIdee *ididee)
{
        //lit l'identificateur de l'idee de la section
        *ididee=section->ididee;
        return RESULTAT_OK;
}

Resultat lectureniveauhierarchique_section(Section *section, NiveauHierarchique *niveauhierarchique)
{
        //lecture du niveau hierarchique
        *niveauhierarchique=section->niveauhierarchique;
        return RESULTAT_OK;
}

Resultat ecritureniveauhierarchique_section(Section *section, NiveauHierarchique niveauhierarchique)
{
        //ecriture du niveau hierarchique
        section->niveauhierarchique=niveauhierarchique;
        return RESULTAT_OK;
}

Resultat lecturesoussections_section(Section *section, Contenu *soussections)
{
        //lecture des sous sections
        *soussections=section->soussections;
        return RESULTAT_OK;
}

Resultat ecrituresoussections_section(Section *section, Contenu soussections)
{
        //ecriture des sous sections => remplacement propre
        //SECURISE(destruction_contenu(&(section->soussections)));
        section->soussections=soussections;
        return RESULTAT_OK;
}

Resultat lecturereferences_section(Section *section, Reference *references)
{
        //lecture des references
        *references=section->references;
        return RESULTAT_OK;
}

Resultat ecriturereferences_section(Section *section, Reference references)
{
        //ecriture des references => remplacement propre
        //SECURISE(destruction_contenu(&(section->references)));
        section->references=references;
        return RESULTAT_OK;
}

