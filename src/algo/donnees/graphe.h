/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __GRAPHE__
#define __GRAPHE__

#include <src/global/global.h>
#include <src/algo/donnees/relations.h>
#include <src/algo/donnees/compteurboucles.h>

typedef struct
{
        IdIdee depart;
        //idee de depart
        //lectures et ecritures en direct dessus
        Relations dependances;
        //ceci contient les dependances entre les idees du systeme logique propose
        //par l'utilisateur
        Relations references;
        //ceci contient les references 
        CompteurBoucles compteurboucles;
        //indique pour chaque noeud le nombre de boucles qui passent par lui
} Graphe;

Resultat creation_graphe(Graphe *graphe, Taille taille);
//cree le graphe avec la taille specifiee

Resultat destruction_graphe(Graphe *graphe);
//detruit tout ce qui reste dans le graphe

Resultat ajoutidee_graphe(Graphe *graphe, Taille taille);
//ajoute des relations pour des idees supplementaires
//renvoie RESULTAT_ERREUR_DOMAINE si taille<0

Resultat lecturedependances_graphe(Graphe *graphe, Relations *dependances);
//renvoie la table de dependances
//les ecritures se feront directement sur le resultat final => pas besoin de tout
//reecrire apres. Cependant, apres un ajout d'idee, un acces au pointeur trouve ici est indefini

Resultat lecturereferences_graphe(Graphe *graphe, Relations *references);
//renvoie la table de references
//les ecritures se feront directement sur le resultat final => pas besoin de tout
//reecrire apres. Cependant, apres un ajout d'idee, un acces au pointeur trouve ici est indefini

Resultat lecturecompteurboucles_graphe(Graphe *graphe, CompteurBoucles *compteurboucles);
//renvoie la table des compteurs de boucles
//les ecritures se feront directement sur le resultat final => pas besoin de tout
//reecrire apres. Cependant, apres un ajout d'idee, un acces au pointeur trouve ici est indefini

#endif
