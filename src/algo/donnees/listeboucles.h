/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LISTEBOUCLES__
#define __LISTEBOUCLES__

#include <src/global/global.h>
#include <src/algo/donnees/marqueurs.h>
#include <src/algo/donnees/parcours.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

typedef struct
{
        Taille memoire;
        //nombre d'elements pris en memoire
        Taille taille;
        //nombre d'elements reellement utilises
        Marqueurs *boucles;        
        //Les boucles sont representees par des marqueurs
} ListeBoucles;

Resultat creation_listeboucles(ListeBoucles *listeboucles);
//cree une liste de boucles vide

Resultat destruction_listeboucles(ListeBoucles *listeboucles);
//detruit une listeboucles et les boucles contenues

Resultat taille_listeboucles(ListeBoucles *listeboucles, Taille *taille);
//renvoie la taille d'une liste de boucles

Resultat agrandissement_listeboucles(ListeBoucles *listeboucles, Taille ajout);
//agrandi chaque marqueurs de la taille specifiee
//les parcours ne necessitants pas cette operations sont laisses inchanges

Resultat ajoutfin_listeboucles(ListeBoucles *listeboucles, Marqueurs *boucle);
//ajoute une boucle a la fin de la liste

Resultat insertiontriee_listeboucles(ListeBoucles *listeboucles, Marqueurs *boucle, Booleen *test);
//insere dans la liste triee par ordre d'inclusion decroissant la boucle specifiee
//test est mis a VRAI si la boucle est effectivement inseree, et a FAUX dans le cas contraire

Resultat lecture_listeboucles(ListeBoucles *listeboucles, Indice indice, Marqueurs *marqueurs);
//renvoie dans marqueurs la boucle d'indice donne et/ou la liste des noeuds
//pour cela, il suffit de passer a la fonction un pointeur non NULL
//l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect

Resultat ecriture_listeboucles(ListeBoucles *listeboucles, Indice indice, Marqueurs *marqueurs);
//ecrit la boucle a l'indice donne
//l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect

Resultat supprime_listeboucles(ListeBoucles *listeboucles, Indice indice);
//supprime la boucle a l'indice donne
//l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect

Resultat miseajourtopologie_listeboucles(ListeBoucles *listeboucles, IdIdee ideeboucle, IdIdee ideenouvelle, Indice *compteur);
//met a jour la topologie en remplacant ideeboucle dans les boucles ou il est present
//par ideenouvelle
//compteur contient le nombre de fois ou ideenouvelle a ete ajoutee
//renvoie une erreur RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrects

#endif
