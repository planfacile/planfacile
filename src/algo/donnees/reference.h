/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __REFERENCE__
#define __REFERENCE__

#include <src/global/global.h>

#define TAILLEINIT        5
#define MULTTAILLE        2

typedef struct section* P2Section;

typedef struct referencecomplete
{
        TypeReference type;
        //type de la reference
        P2Section section;
        //section destination
} ReferenceComplete;

typedef struct reference
{
        Taille memoire;
        //taille prise en memoire
        Taille taille;
        //taille reellement prise
        ReferenceComplete *destination;
} Reference;

Resultat creation_reference(Reference *reference);
//creation d'un reference vide

Resultat destruction_reference(Reference *reference);
//detruit le reference

Resultat taille_reference(Reference *reference, Taille *taille);
//renvoie le nombre d'elements du reference

Resultat ajoutreference_reference(Reference *reference, TypeReference type, P2Section section);
//ajoute une section a la fin du reference

Resultat echange_reference(Reference *reference, Indice indice1, Indice indice2);
//echange les deux sections placees aux indices indiques
//renvoie RESULTAT_ERREUR_DEPASSEMENT si un des indices est incorrect

Resultat lecture_reference(Reference *reference, Indice indice, TypeReference *type, P2Section *section);
//place la valeur de la section dans le pointeur
//renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect

Resultat ecrituretype_reference(Reference *reference, Indice indice, TypeReference type);
//ecrit un type de reference
//renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect

Resultat suppression_reference(Reference *reference, Indice indice);
//supprime le lien sur la section dans le reference
//renvoie RESULTAT_ERREUR_DEPASSEMENT si l'indice est incorrect

#endif
