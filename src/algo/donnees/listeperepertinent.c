/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "listeperepertinent.h"

Resultat creation_listeperepertinent(ListePerePertinent *listeperepertinent, Taille taille)
{
        //cree la liste avec la taille specifiee
        //l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee en cas de taille negative
        Indice indice;
        if(taille<0)
                return RESULTAT_ERREUR_DOMAINE;
        if((listeperepertinent->perepertinent=(PerePertinent*)(malloc(sizeof(PerePertinent)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=0 ; indice<taille ; indice++)
                listeperepertinent->perepertinent[indice].idperepertinent=ORPHELIN;
        listeperepertinent->taille=taille;
        return RESULTAT_OK;
}

Resultat destruction_listeperepertinent(ListePerePertinent *listeperepertinent)
{
        //detruit la liste
        free(listeperepertinent->perepertinent);
        return RESULTAT_OK;
}

Resultat ajoutidee_listeperepertinent(ListePerePertinent *listeperepertinent, Taille ajout)
{
        //a priori inutile, mais bon
        //ajoute des idees dans la liste
        //l'erreur RESULTAT_ERREUR_DOMAINE est renvoyee en cas d'ajout negatif
        Indice indice;
        if(ajout<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(ajout==0)
                return RESULTAT_OK;
        if((listeperepertinent->perepertinent=(PerePertinent*)(realloc(listeperepertinent->perepertinent,sizeof(PerePertinent)*(listeperepertinent->taille+ajout))))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        for(indice=listeperepertinent->taille ; indice<listeperepertinent->taille+ajout ; indice++)
                listeperepertinent->perepertinent[indice].idperepertinent=ORPHELIN;
        listeperepertinent->taille += ajout;
        return RESULTAT_OK;
}

Resultat ecriturepere_listeperepertinent(ListePerePertinent *listeperepertinent, IdIdee idfils, IdIdee pere, Pertinence pertinence)
{
        //met a jour la liste des peres pertinents si la pertinence est egale ou superieure pour ce pere, ou si
        //le fils etait orphelin
        //renvoie une erreur RESULTAT_ERREUR_DEPASSEMENT si l'idfils n'est pas correct, et RESULTAT_ERREUR_DOMAINE si le pere ou la pertinence
        //sont incorrects
        if(idfils<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(idfils>listeperepertinent->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(pere<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(pere>listeperepertinent->taille)
                return RESULTAT_ERREUR_DOMAINE;
        if(pertinence<0)
                return RESULTAT_ERREUR_DOMAINE;
        if(listeperepertinent->perepertinent[idfils].idperepertinent==ORPHELIN)
        {
                listeperepertinent->perepertinent[idfils].idperepertinent=pere;
                listeperepertinent->perepertinent[idfils].pertinence=pertinence;
                return RESULTAT_OK;
        }
        if(pertinence>=listeperepertinent->perepertinent[idfils].pertinence)
        {
                listeperepertinent->perepertinent[idfils].idperepertinent=pere;
                listeperepertinent->perepertinent[idfils].pertinence=pertinence;
                return RESULTAT_OK;
        }
        return RESULTAT_OK;
}

Resultat lecturepere_listeperepertinent(ListePerePertinent *listeperepertinent, IdIdee fils, Booleen *orphelin, IdIdee *pere)
{
        //renvoie le pere le plus pertinent du fils indique
        //si orphelin est VRAI, le contenu de pere est indefini, sinon, sauf si pere est egal a NULL
        //pere contient la valeur du pere le plus pertinent
        //renvoie l'erreur RESULTAT_ERREUR_DEPASSEMENT si l'identificateur du fils est incorrect
        if(fils<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(fils>listeperepertinent->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(listeperepertinent->perepertinent[fils].idperepertinent==ORPHELIN)
        {
                *orphelin=VRAI;
                return RESULTAT_OK;
        }
        *orphelin=FAUX;
        if(pere!=NULL)
                *pere=listeperepertinent->perepertinent[fils].idperepertinent;
        return RESULTAT_OK;
}

