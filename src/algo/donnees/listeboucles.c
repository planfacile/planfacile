/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "listeboucles.h"

Resultat creation_listeboucles(ListeBoucles *listeboucles)
{
        //cree une liste de boucles vide
        if((listeboucles->boucles=(Marqueurs*)(malloc(sizeof(Marqueurs)*TAILLEINIT)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        listeboucles->memoire=TAILLEINIT;
        listeboucles->taille=0;
        return RESULTAT_OK;
}

Resultat destruction_listeboucles(ListeBoucles *listeboucles)
{
        //detruit une listeboucles et les boucles contenues
        Indice indice;
        for(indice=0 ; indice<listeboucles->taille ; indice++)
                SECURISE(destruction_marqueurs(&(listeboucles->boucles[indice])));
        free(listeboucles->boucles);
        return RESULTAT_OK;
}

Resultat taille_listeboucles(ListeBoucles *listeboucles, Taille *taille)
{
        //renvoie la taille d'une liste de boucles
        *taille=listeboucles->taille;
        return RESULTAT_OK;
}

Resultat agrandissement_listeboucles(ListeBoucles *listeboucles, Taille ajout)
{
        //agrandi chaque marqueur de la taille specifiee
        Indice indice;
        for(indice=0 ; indice<listeboucles->taille ; indice++)
                SECURISE(agrandissement_marqueurs(&(listeboucles->boucles[indice]),ajout));
        return RESULTAT_OK;
}

static Resultat ajoutfinvide_listeboucles(ListeBoucles *listeboucles)
{
        //fait en sorte de laisser un emplacement disponible dans la liste
        Marqueurs *bouclestemp;
        if(listeboucles->taille==listeboucles->memoire)
        {
                //plus assez d'emplacements : on en redemande...
                if((bouclestemp=(Marqueurs*)(realloc(listeboucles->boucles,sizeof(Marqueurs)*(listeboucles->memoire)*MULTTAILLE)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                listeboucles->boucles=bouclestemp;
                listeboucles->memoire *= MULTTAILLE;
        }
        //la, on est surs d'avoir la place qu'il faut
        return RESULTAT_OK;
}

Resultat ajoutfin_listeboucles(ListeBoucles *listeboucles, Marqueurs *boucle)
{
        //ajoute une boucle a la fin de la liste
        SECURISE(ajoutfinvide_listeboucles(listeboucles));
        listeboucles->boucles[listeboucles->taille++]=*boucle;
        return RESULTAT_OK;
}

Resultat insertiontriee_listeboucles(ListeBoucles *listeboucles, Marqueurs *boucle, Booleen *test)
{
        //insere dans la liste triee par ordre d'inclusion decroissant la boucle specifiee
        Indice indice, indiceecrit;
        Marqueurs nouveau;
        *test=FAUX;
        for(indice=listeboucles->taille ; indice>0 ; indice--)
        {
                Booleen compare;
                SECURISE(egales_marqueurs(&(listeboucles->boucles[indice-1]),boucle,&compare));
                if(compare==VRAI)
                        return RESULTAT_OK;
                SECURISE(inclusion_marqueurs(boucle,&(listeboucles->boucles[indice-1]),&compare));
                if(compare==VRAI)
                        break;
        }
        SECURISE(copie_marqueurs(*boucle, &nouveau));
        if(indice==listeboucles->taille)
        {
                SECURISE(ajoutfin_listeboucles(listeboucles,&nouveau));
        }
        else
        {
                SECURISE(ajoutfinvide_listeboucles(listeboucles));
                for(indiceecrit=listeboucles->taille ; indiceecrit>indice ; indiceecrit--)
                        listeboucles->boucles[indiceecrit]=listeboucles->boucles[indiceecrit-1];
                listeboucles->taille++;
                SECURISE(ecriture_listeboucles(listeboucles,indice,&nouveau));
        }
        *test=VRAI;
        return RESULTAT_OK;
}

Resultat lecture_listeboucles(ListeBoucles *listeboucles, Indice indice, Marqueurs *marqueurs)
{
        //renvoie dans marqueurs la boucle d'indice donne
        //l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=listeboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *marqueurs=listeboucles->boucles[indice];
        return RESULTAT_OK;
}

Resultat ecriture_listeboucles(ListeBoucles *listeboucles, Indice indice, Marqueurs *marqueurs)
{
        //ecrit la boucle a l'indice donne
        //l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=listeboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        listeboucles->boucles[indice]=*marqueurs;
        return RESULTAT_OK;
}

Resultat supprime_listeboucles(ListeBoucles *listeboucles, Indice indice)
{
        //supprime la boucle a l'indice donne
        //l'erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee si l'indice est incorrect
        if(indice<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(indice>=listeboucles->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        SECURISE(destruction_marqueurs(&(listeboucles->boucles[indice])));
        listeboucles->taille--;
        for( ; indice<listeboucles->taille ; indice++)
                listeboucles->boucles[indice]=listeboucles->boucles[indice+1];
        return RESULTAT_OK;
}

Resultat miseajourtopologie_listeboucles(ListeBoucles *listeboucles, IdIdee ideeboucle, IdIdee ideenouvelle, Indice *compteur)
{
        //met a jour la topologie en remplacant ideeboucle dans les boucles ou il est present
        //par ideenouvelle
        //compteur contient le nombre de fois ou ideenouvelle a ete ajoutee
        //renvoie une erreur RESULTAT_ERREUR_DEPASSEMENT en cas d'indice incorrects
        Indice indice;
        *compteur=0;
        for(indice=0 ; indice<listeboucles->taille ; indice++)
        {
                Booleen test;
                SECURISE(verification_marqueurs(&(listeboucles->boucles[indice]),ideeboucle,&test));
                if(test==VRAI)
                {
                        SECURISE(decoche_marqueurs(&(listeboucles->boucles[indice]),ideeboucle));
                        SECURISE(verification_marqueurs(&(listeboucles->boucles[indice]),ideenouvelle,&test));
                        if(test==FAUX)
                                (*compteur)++;
                        SECURISE(coche_marqueurs(&(listeboucles->boucles[indice]),ideenouvelle));
                }
        }
        return RESULTAT_OK;
}

