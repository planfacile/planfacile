/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LIENS__
#define __LIENS__

#include <src/global/global.h>

typedef struct
{
        Taille taille;
        //taille des liens
        References **table;
        //table donnant les divers types de references entre deux sous arbres du plan
        //indices table[source][destination]
        Indice *liste;
        //liste des indices de la table pour les indices dans les contenus
        References references;
        //nombre de references reductibles et de references irreductibles
} Liens;

Resultat creation_liens(Liens *liens, Taille taille);
//cree les liens avec la taille specifiee

Resultat destruction_liens(Liens *liens);
//detruit les liens

Resultat lecture_liens(Liens *liens, Indice source, Indice destination, References *references);
//lit une valeur de references entre deux sous arbres
//RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect

Resultat ecriture_liens(Liens *liens, Indice source, Indice destination, References references);
//ecrit une valeur de references entre deux sous arbres
//RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect

Resultat test_liens(Liens *liens, Indice contenu1, Indice contenu2, Booleen *comparaison);
//teste si deux contenus ont interet a etre interchanges
//RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect

Resultat echange_liens(Liens *liens, Indice contenu1, Indice contenu2);
//echange deux contenus dans les liens
//RESULTAT_ERREUR_DEPASSEMENT est renvoye en cas d'indice incorrect

Resultat references_liens(Liens *liens, References *references);
//place dans references la somme des references des liens

Resultat affiche_liens(Liens liens);
//affiche le contenu des liens /!\ DEBUG

#endif
