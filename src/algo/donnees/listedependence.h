/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LISTEDEPENDENCES__
#define __LISTEDEPENDENCES__

#include <src/global/global.h>

#define ILLIMITE        -1

typedef struct maillondependence
{
        IdIdee idideesource;
        IdIdee idideedestination;
        Pertinence pertinence;
        struct maillondependence *precedente;
        //attention, les dependences sont empilees a l'envers...
} MaillonDependences;

typedef MaillonDependences* ListeDependences;

Resultat creation_listedependences(ListeDependences *listedependences);
//cree une liste de dependences...
//bon, j'admets, une fonction pour ca, c'est se foutre de la gueule du monde.
//mais ca fait plus propre

Resultat destruction_listedependences(ListeDependences *listedependences);
//detruit la liste des dependences

Resultat empiledependence_listedependences(ListeDependences *listedependences, IdIdee idideesource, IdIdee idideedestination, Pertinence pertinence);
//empile une dependence

Resultat depiledependence_listedependences(ListeDependences *listedependences, IdIdee *idideesource, IdIdee *idideedestination, Pertinence *pertinence);
//depile une dependence

#endif
