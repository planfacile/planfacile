/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "listeidees.h"

Resultat creation_listeidees(ListeIdees *listeidees)
{
        //cree une liste d'idees
        if((listeidees->idees=(Idee*)(malloc(sizeof(Idee)*TAILLEINIT)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;
        listeidees->memoire=TAILLEINIT;
        listeidees->taille=0;
        return RESULTAT_OK;
}

Resultat destruction_listeidees(ListeIdees *listeidees)
{
        //detruit la liste d'idees et les idees contenues
        Indice indice;
        for(indice=0 ; indice<listeidees->taille ; indice++)
                SECURISE(destruction_idee(&(listeidees->idees[indice])));
        free(listeidees->idees);
        return RESULTAT_OK;
}

static Resultat ajoutfinvide_listeidees(ListeIdees *listeidees)
{
        //fait en sorte de laisser un emplacement disponible dans la liste
        Idee *ideestemp;
        if(listeidees->taille==listeidees->memoire)
        {
                //plus assez d'emplacements : on en redemande...
                if((ideestemp=(Idee*)(realloc(listeidees->idees,sizeof(Idee)*(listeidees->memoire)*MULTTAILLE)))==NULL)
                        return RESULTAT_ERREUR_MEMOIRE;
                listeidees->idees=ideestemp;
                listeidees->memoire *= MULTTAILLE;
        }
        //la, on est surs d'avoir la place qu'il faut
        return RESULTAT_OK;
}

Resultat taille_listeidees(ListeIdees *listeidees, Taille *taille)
{
        //renvoie la taille de la liste
        *taille=listeidees->taille;
        return RESULTAT_OK;
}

Resultat ajoutidee_listeidees(ListeIdees *listeidees, Idee idee)
{
        //ajoute une idee a la liste
        SECURISE(ajoutfinvide_listeidees(listeidees));
        listeidees->idees[listeidees->taille++]=idee;
        return RESULTAT_OK;
}

Resultat lectureidee_listeidees(ListeIdees *listeidees, IdIdee ididee, Idee *idee)
{
        //place dans la variable idee la valeur de l'idee contenue a l'emplacement ididee
        //si l'indice est incorrect, une erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=listeidees->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        *idee=listeidees->idees[ididee];
        return RESULTAT_OK;
}

Resultat ecritureidee_listeidees(ListeIdees *listeidees, IdIdee ididee, Idee idee)
{
        //ecrit l'idee a l'emplacement ididee dans listeidees
        //si l'indice est incorrect, une erreur RESULTAT_ERREUR_DEPASSEMENT est renvoyee
        if(ididee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(ididee>=listeidees->taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        listeidees->idees[ididee]=idee;
        return RESULTAT_OK;
}

