/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "algo.h"

#define NIVEAU_RACINE        0

#define PERTINENCE_RACINE        0

static Resultat recherchebouclesinternes_algo(Graphe *graphe, ListeBoucles *listeboucles, IdIdee idee, Marqueurs *marqueurs, Parcours *parcours)
{
        IdIdee ideedependante;
        Booleen test;
        Relations dependances;
        CompteurBoucles compteurboucles;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(lecturecompteurboucles_graphe(graphe,&compteurboucles));
        SECURISE(verification_marqueurs(marqueurs,idee,&test));
        if(test==VRAI)
        {
                //on vient de trouver une boucle, on l'enregistre
                Marqueurs nouvelleboucle;
                SECURISE(creation_marqueurs(&nouvelleboucle,dependances.taille));
                //construit la boucle
                SECURISE(marqueboucle_parcours(parcours,idee,&nouvelleboucle));
                //l'insere dans la liste triee
                SECURISE(insertiontriee_listeboucles(listeboucles,&nouvelleboucle,&test));
                //met a jour les compteurs de boucle
                if(test==VRAI)
                        SECURISE(incrementeboucle_compteurboucles(&compteurboucles,nouvelleboucle));
                SECURISE(destruction_marqueurs(&nouvelleboucle));
        }
        else
        {
                //on s'occupe de chercher les boucles
                SECURISE(coche_marqueurs(marqueurs,idee));
                SECURISE(empileididee_parcours(parcours,idee));
                for(ideedependante=0 ; ideedependante<dependances.taille ; ideedependante++)
                {
                        SECURISE(testrelation_relations(&dependances,idee,ideedependante,&test,NULL));
                        if(test==VRAI)        //on a une dependance ?
                                SECURISE(recherchebouclesinternes_algo(graphe,listeboucles,ideedependante,marqueurs,parcours));
                }
                SECURISE(decoche_marqueurs(marqueurs,idee));
                SECURISE(depileididee_parcours(parcours));
        }
        return RESULTAT_OK;
}

static Resultat rechercheboucles_algo(Graphe *graphe, ListeBoucles *listeboucles)
{
        Marqueurs marqueurs;
        Parcours parcours;
        Relations dependances;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(creation_marqueurs(&marqueurs,dependances.taille));
        SECURISE(creation_parcours(&parcours,dependances.taille));
        //on cherche les boucles
        SECURISE(recherchebouclesinternes_algo(graphe,listeboucles,graphe->depart,&marqueurs,&parcours));
        SECURISE(destruction_parcours(&parcours));
        SECURISE(destruction_marqueurs(&marqueurs));
        return RESULTAT_OK;
}

static Resultat regroupementboucles_algo(ListeIdees *listeidees, Graphe *graphe, ListeBoucles *listeboucles)
{
        Taille nombreboucles, tailledependances, tailleinitiale;
        Indice indiceideemanquante;
        IdIdee ididee,idideedependance,idnouvelleidee;
        Relations dependances, references;
        CompteurBoucles compteurboucles;
        //ajoute les idees
        SECURISE(taille_listeboucles(listeboucles,&nombreboucles));
        SECURISE(taille_listeidees(listeidees,&tailleinitiale));
        SECURISE(ajoutidee_graphe(graphe,nombreboucles));
        SECURISE(agrandissement_listeboucles(listeboucles,nombreboucles));
        for(indiceideemanquante=1 ; indiceideemanquante<=nombreboucles ; indiceideemanquante++)
        {
                Idee idee;
                SECURISE(creation_idee(&idee,IDEE_MANQUANTE,indiceideemanquante,INDICE_IDEE_INVALIDE));
                SECURISE(ajoutidee_listeidees(listeidees,idee));
        }
        SECURISE(taille_listeidees(listeidees,&tailledependances));
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(lecturereferences_graphe(graphe,&references));
        SECURISE(lecturecompteurboucles_graphe(graphe,&compteurboucles));
        //elimine les boucles
        for( idnouvelleidee=tailleinitiale ; ; idnouvelleidee++)
        {//pour chaque boucle de la liste
                Marqueurs boucle;
                Indice compteur;
                Indice nombrenoeudsboucle;
                SECURISE(taille_listeboucles(listeboucles,&nombreboucles));
                if(nombreboucles==0)
                        return RESULTAT_OK;
                SECURISE(lecture_listeboucles(listeboucles,nombreboucles-1,&boucle));
                //deja, on teste si la boucle n'est pas un doublon forme par recombinaison
                SECURISE(nombre_marqueurs(&boucle,&nombrenoeudsboucle));
                if(nombrenoeudsboucle>1)
                {
                        //reorganisation de la boucle
                        for(ididee=0 ; ididee<tailledependances ; ididee++)
                        {
                                Booleen test, test2;
                                SECURISE(verification_marqueurs(&boucle,ididee,&test));
                                if((test==VRAI)&&(ididee!=idnouvelleidee))
                                {
                                        //pour chaque idee de la boucle
                                        for(idideedependance=0 ; idideedependance<tailledependances ; idideedependance++)
                                        {
                                                Pertinence pertinence;
                                                //pour le test... faut etre croyant, hein...
                                                //en fait, ca marche car la topologie des boucles change : on ne peut pas prendre deux fois un
                                                //noeud donne
                                                SECURISE(verification_marqueurs(&boucle,idideedependance,&test));
                                                SECURISE(testrelation_relations(&dependances,ididee,idideedependance,&test2,&pertinence));
                                                if((test==VRAI)&&(test2==VRAI))
                                                {
                                                        //transformation de la dependance sur le suivant en reference
                                                        SECURISE(suppressionrelation_relations(&dependances,ididee,idideedependance));
                                                        SECURISE(ajoutrelation_relations(&references,idideedependance,ididee,pertinence));
                                                        //ajout de la dependance depuis le noeud supplementaire sur le suivant pour le respect de la pertinence ! << inutile, mais plus propre ^_^"
                                                        SECURISE(ajoutrelation_relations(&dependances,idnouvelleidee,idideedependance,pertinence));
                                                        //attention, si le noeud dans la boucle etait le noeud de depart, il faut changer le noeud de depart
                                                        //En fait, non, on va obliger a avoir une seule idee orpheline qui sera le noeud de base
                                                        break;
                                                }
                                        }
                                        //decrementation du compteur du noeud de la boucle
                                        //en fait, apres le traitement, le noeud se trouve dans aucune boucle
                                        SECURISE(ecriturecompteur_compteurboucles(&compteurboucles,ididee,0));
                                }
                        }
                        //decouplage des boucles superieures et transfert des dependances sur le nouveau noeud
                        for(ididee=0 ; ididee<tailledependances ; ididee++)
                        {
                                Booleen test;
                                SECURISE(verification_marqueurs(&boucle,ididee,&test));
                                if((test==VRAI)&&(ididee!=idnouvelleidee))//faut pas traiter le nouveau noeud !
                                {        //pour chaque noeud de la boucle
                                        //transformation des peres des noeuds
                                        for(idideedependance=0 ; idideedependance<tailledependances ; idideedependance++)
                                        {//idideedependance contiendra l'id du pere ici
                                                Pertinence pertinence;
                                                //Indice compteurlocal;
                                                SECURISE(testrelation_relations(&dependances,idideedependance,ididee,&test,&pertinence));
                                                if((test==VRAI)&&(idideedependance!=idnouvelleidee))//attention a pas traiter le pere de la boucle !
                                                {
                                                        //applique la transformation sur le pere
                                                        SECURISE(suppressionrelation_relations(&dependances,idideedependance,ididee));
                                                        SECURISE(ajoutrelation_relations(&dependances,idideedependance,idnouvelleidee,pertinence));
                                                        SECURISE(ajoutrelation_relations(&references,ididee,idideedependance,pertinence));
                                                }
                                        }
                                        //transformation des fils des noeuds
                                        if(ididee<tailleinitiale) //attention, faut pas traiter les noeuds des boucles deja traitees !
                                                for(idideedependance=0 ; idideedependance<tailledependances ; idideedependance++)
                                                {
                                                        Pertinence pertinence;
                                                        Booleen test;
                                                        SECURISE(testrelation_relations(&dependances,ididee,idideedependance,&test,&pertinence));
                                                        if(test==VRAI)
                                                        {
                                                                Indice compteurfils;
                                                                SECURISE(lecturecompteur_compteurboucles(&compteurboucles,idideedependance,&compteurfils));
                                                                if(compteurfils>0)
                                                                {
                                                                        //applique la transformation sur le fils
                                                                        SECURISE(suppressionrelation_relations(&dependances,ididee,idideedependance));
                                                                        SECURISE(ajoutrelation_relations(&dependances,idnouvelleidee,idideedependance,pertinence));
                                                                        SECURISE(ajoutrelation_relations(&references,idideedependance,ididee,pertinence));
                                                                        //mise a jour des compteurs
                                                                        //ben, yena pas !
                                                                        //pas de changement de topologie ici, car elle deja ete faite
                                                                }
                                                        }
                                                }
                                        //mise a jour de la topologie des boucles
                                        SECURISE(miseajourtopologie_listeboucles(listeboucles,ididee,idnouvelleidee,&compteur));
                                        SECURISE(ajoute_compteurboucles(&compteurboucles,idnouvelleidee,compteur));
                                }
                        }
                        //on marque le fait que l'on vient de supprimer une boucle en decrementant LE noeud representant la boucle
                        SECURISE(decremente_compteurboucles(&compteurboucles,idnouvelleidee));
                }
                SECURISE(supprime_listeboucles(listeboucles,nombreboucles-1));
        }
        return RESULTAT_OK;
}

static Resultat regroupementidees_algo(ListeIdees *listeidees, Graphe *graphe, Statistiques *statistiques)
{
        //recherche les idees qui vont ensemble et les regroupe dans une meme sous section
        ListeBoucles listeboucles;
        SECURISE(creation_listeboucles(&listeboucles));
        //on cherche les boucles
        SECURISE(rechercheboucles_algo(graphe,&listeboucles));
        SECURISE(taille_listeboucles(&listeboucles,&(statistiques->nombreideesmanquantes)));
        //on reconnecte tout proprement
        SECURISE(regroupementboucles_algo(listeidees,graphe,&listeboucles));
        SECURISE(destruction_listeboucles(&listeboucles));
        return RESULTAT_OK;
}

static Resultat rechercheperespertinents_algo(Graphe *graphe, ListePerePertinent *listeperepertinent)
{
        //place dans listeperepertinent les peres pertinents de chaque idee
        IdIdee idfils, idpere;
        Taille taille;
        Relations dependances;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        taille=dependances.taille;//licite
        //une double boucle revient a un parcours en profondeur du graphe
        //avec le meme ordre dans la visite des noeuds d'un meme niveau
        //alors, on use de cette astuce... sans parcimonie !
        for(idfils=0 ; idfils<taille ; idfils++)
                for(idpere=0 ; idpere<taille ; idpere++)
                {
                        Pertinence pertinence;
                        Booleen test;
                        SECURISE(testrelation_relations(&dependances,idpere,idfils,&test,&pertinence));
                        if(test==VRAI)
                                SECURISE(ecriturepere_listeperepertinent(listeperepertinent,idfils,idpere,pertinence));//meme pas drole, tout est fait...
                }
        return RESULTAT_OK;
}

static Resultat transformationdependancesplan_algo(Graphe *graphe, ListePerePertinent *listeperepertinent)
{
        //reparcourt le graphe, et transforme en references toutes les dependances autres que celle avec le
        //pere pertinent
        Relations dependances, references;
        Taille taille;
        IdIdee idfils, idpere;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(lecturereferences_graphe(graphe,&references));
        taille=dependances.taille;
        for(idfils=0 ; idfils<taille ; idfils++)
                for(idpere=0 ; idpere<taille ; idpere++)
                {
                        Pertinence pertinence;
                        Booleen test;
                        SECURISE(testrelation_relations(&dependances,idpere,idfils,&test,&pertinence));
                        if(test==VRAI)
                        {
                                IdIdee idperepertinent;
                                SECURISE(lecturepere_listeperepertinent(listeperepertinent,idfils,&test,&idperepertinent));
                                if(test==VRAI)//gneeee ?!? (ca veut dire orphelin, ce qui est theoriquement impossible ici)
                                        return RESULTAT_ERREUR;
                                if(idperepertinent!=idpere)
                                {
                                        //on transforme en reference
                                        SECURISE(suppressionrelation_relations(&dependances,idpere,idfils));
                                        SECURISE(ajoutrelation_relations(&references,idfils,idpere,pertinence));
                                }
                        }
                }
        return RESULTAT_OK;
}

static Resultat generationplan_algo(Graphe *graphe, Plan *plan)
{
        //copie les informations de dependances et de references dans le plan
        Section **sections;
        Relations dependances, references;
        Taille taille;
        IdIdee ididee, idfils, idpere;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(lecturereferences_graphe(graphe,&references));
        taille=dependances.taille;
        //on commence par creer les noeuds sans references ni fils
        if((sections=(Section**)(malloc(sizeof(Section*)*taille)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;//oui, bon. j'aime pas ce malloc au milieu, mais le faire
                //dans un .c et .h a part serait demesure
        for(ididee=0 ; ididee<taille ; ididee++)
        {
                Contenu dependancesplan;
                Reference referencesplan;
                SECURISE(creation_contenu(&dependancesplan));
                SECURISE(creation_reference(&referencesplan));
                SECURISE(creation_section(&(sections[ididee]),ididee,dependancesplan,referencesplan));
        }
        //ben, on copie joyeusement alors...
        for(idpere=0 ; idpere<taille ; idpere++)
                for(idfils=0 ; idfils<taille ; idfils++)
                {
                        Booleen test;
                        //on se moque pas mal de la pertinence, ici
                        SECURISE(testrelation_relations(&dependances,idpere,idfils,&test,NULL));
                        if(test==VRAI)
                        {
                                //la, on ajoute un fils
                                Contenu dependancesplan;
                                SECURISE(lecturesoussections_section(sections[idpere],&dependancesplan));
                                SECURISE(ajoutsection_contenu(&dependancesplan,sections[idfils]));
                                SECURISE(ecrituresoussections_section(sections[idpere],dependancesplan));
                        }
                        SECURISE(testrelation_relations(&references,idpere,idfils,&test,NULL));
                        if(test==VRAI)
                        {
                                //la, on ajoute une reference
                                Reference referencesplan;
                                SECURISE(lecturereferences_section(sections[idpere],&referencesplan));
                                SECURISE(ajoutreference_reference(&referencesplan,REFERENCE_INDEFINIE,sections[idfils]));
                                SECURISE(ecriturereferences_section(sections[idpere],referencesplan));
                        }
                }
        *plan=sections[graphe->depart];
        //on donne la racine de l'arbre et on s'en va !
        free(sections);//faut pas oublier...
        return RESULTAT_OK;
}

static Resultat transformationplan_algo(Graphe *graphe, Plan *plan)
{
        //on tranfere les informations de dependances du systeme logique dans un plan non
        //optimise
        //Un graphe s'eteint, un plan s'eveille
        ListePerePertinent listeperepertinent;
        Relations dependances;
        SECURISE(lecturedependances_graphe(graphe,&dependances));
        SECURISE(creation_listeperepertinent(&listeperepertinent,dependances.taille));
        //recherche les parents les plus pertinents
        SECURISE(rechercheperespertinents_algo(graphe,&listeperepertinent));
        //transforme les dependances non pertinentes en references
        SECURISE(transformationdependancesplan_algo(graphe,&listeperepertinent));
        //copie le graphe dans l'arbre
        SECURISE(generationplan_algo(graphe,plan));
        //detruit le graphe
        SECURISE(destruction_graphe(graphe));
        SECURISE(destruction_listeperepertinent(&listeperepertinent));
        return RESULTAT_OK;
}

static Resultat trisoussections_algo(Liens *liens, Contenu *soussections)
{
        //effectue le tri des sous-sections
        //avec un tri par insertion, si si ! il est ici en O(n^2) au lieu des autres qui seraient
        //en O(n^2*ln(n)), du au fait que la fonction de comparaison n'est en O(1) que pour
        //des elements consecutifs !
        Indice soussectionsource, soussectiondestination /*, soussectiondeplace*/;
        Taille taille;
        taille=liens->taille;
        if(taille<2)
                return RESULTAT_OK;
        for(soussectionsource=0 ; soussectionsource<taille ; soussectionsource++)
        {
                for(soussectiondestination=soussectionsource ; soussectiondestination>0 ; soussectiondestination--)
                {
                        Booleen test;
                        SECURISE(test_liens(liens,soussectiondestination-1,soussectiondestination,&test));
                        if(test==VRAI)
                        {
                                SECURISE(echange_liens(liens,soussectiondestination-1,soussectiondestination));
                                SECURISE(echange_contenu(soussections,soussectiondestination-1,soussectiondestination));
                        }
                }
        }
        return RESULTAT_OK;
}

static Resultat optimisationplaninterne_algo(Section *section, Taille taille, Marqueurs *parcours, Marqueurs *noeudscontenus, CompteurBoucles *destinationsreferences, References *statsreferences)
{
        //trie les fils de la section en cours
        IdIdee ididee;
        Liens liens;
        Indice idsoussection, idsoussectiontable, idreferences;
        Taille nombresoussections;
        Contenu soussections;
        Reference references;
        Marqueurs *noeudscontenussoussection;
        CompteurBoucles *destinationsreferencessoussection;//utilisation non std de ces compteurs
        SECURISE(lectureididee_section(section,&ididee));
        SECURISE(lecturesoussections_section(section,&soussections));
        SECURISE(lecturereferences_section(section,&references));
        //on marque notre passage
        SECURISE(coche_marqueurs(parcours,ididee));
        //recupere le nombre de sous-sections
        SECURISE(taille_contenu(&soussections,&nombresoussections));
        //on construit les tableaux pour recueillir les informations
        if((noeudscontenussoussection=(Marqueurs*)(malloc(sizeof(Marqueurs)*nombresoussections)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;//meme remarque pour le malloc
        if((destinationsreferencessoussection=(CompteurBoucles*)(malloc(sizeof(CompteurBoucles)*nombresoussections)))==NULL)
                return RESULTAT_ERREUR_MEMOIRE;//meme remarque pour le malloc
        //construit la table des liens
        SECURISE(creation_liens(&liens,nombresoussections));
        //on visite les sous-sections et on recupere les infos qui nous y interessent
        for(idsoussection=0 ; idsoussection<nombresoussections ; idsoussection++)
        {
                References referencessoussection;
                Section *sectiontemp;
                referencessoussection.referencesreductibles=0;
                referencessoussection.referencesirreductibles=0;
                SECURISE(creation_marqueurs(&(noeudscontenussoussection[idsoussection]),taille));
                SECURISE(creation_compteurboucles(&(destinationsreferencessoussection[idsoussection]),taille));
                SECURISE(lecture_contenu(&soussections,idsoussection,&sectiontemp));
                SECURISE(optimisationplaninterne_algo(sectiontemp,taille,parcours,&(noeudscontenussoussection[idsoussection]),&(destinationsreferencessoussection[idsoussection]),&referencessoussection));
                SECURISE(cumul_marqueurs(noeudscontenus,&(noeudscontenussoussection[idsoussection])));
                SECURISE(cumul_compteurboucles(destinationsreferences,&(destinationsreferencessoussection[idsoussection])));
                SECURISE(ecriture_liens(&liens,idsoussection,idsoussection,referencessoussection));
        }
        //remplit la table des liens
        for(idsoussection=0 ; idsoussection<nombresoussections ; idsoussection++)
                for(idsoussectiontable=0 ; idsoussectiontable<nombresoussections ; idsoussectiontable++)
                {
                        References referencessoussection;
                        Indice commun;
                        referencessoussection.referencesreductibles=0;
                        referencessoussection.referencesirreductibles=0;
                        if(idsoussectiontable==idsoussection)
                                continue;//les informations sont deja dans la table
                        SECURISE(commun_compteurboucles(&(destinationsreferencessoussection[idsoussection]),&(noeudscontenussoussection[idsoussectiontable]),&commun));
                        if(idsoussectiontable<idsoussection)
                                referencessoussection.referencesreductibles=commun;
                        else
                                referencessoussection.referencesirreductibles=commun;
                        SECURISE(ecriture_liens(&liens,idsoussection,idsoussectiontable,referencessoussection));
                }
        //tri selon les informations contenues dans la table des liens
        SECURISE(trisoussections_algo(&liens,&soussections));
        //ajout des informations de la racine
        SECURISE(coche_marqueurs(noeudscontenus,section->ididee));
        for(idreferences=0 ; idreferences<references.taille ; idreferences++)
        {
                Section *destinationreference;
                IdIdee ididee;
                TypeReference type;
                SECURISE(lecture_reference(&references,idreferences,&type,&destinationreference));
                SECURISE(lectureididee_section(destinationreference,&ididee));
                SECURISE(incremente_compteurboucles(destinationsreferences,ididee));
        }        
        //recupere la somme des liens
        SECURISE(references_liens(&liens,statsreferences));
        //destruction de la table des liens
        SECURISE(destruction_liens(&liens));
        //il faut supprimer les tableaux crees
        for(idsoussection=0 ; idsoussection<nombresoussections ; idsoussection++)
        {
                SECURISE(destruction_marqueurs(&(noeudscontenussoussection[idsoussection])));
                SECURISE(destruction_compteurboucles(&(destinationsreferencessoussection[idsoussection])));
        }
        free(noeudscontenussoussection);
        free(destinationsreferencessoussection);
        return RESULTAT_OK;
}

static Resultat optimisationplan_algo(Plan *plan, Taille taille, References *references)
{
        //optimise le plan en triant les differentes parties de facon a ce que
        //le nombre de references irreductibles soit le plus petit possible
        Marqueurs parcours, noeudscontenus;
        CompteurBoucles destinationsreferences;
        SECURISE(creation_marqueurs(&parcours,taille));
        SECURISE(creation_marqueurs(&noeudscontenus,taille));
        SECURISE(creation_compteurboucles(&destinationsreferences,taille));
        references->referencesreductibles=0;
        references->referencesirreductibles=0;
        //ben, on y va...
        SECURISE(optimisationplaninterne_algo(*plan,taille,&parcours,&noeudscontenus,&destinationsreferences,references));
        SECURISE(destruction_marqueurs(&parcours));
        SECURISE(destruction_marqueurs(&noeudscontenus));
        SECURISE(destruction_compteurboucles(&destinationsreferences));
        return RESULTAT_OK;
}

static Resultat reductionreferencespeaufinageinterne_algo(Section *section, ListeIdees *listeidees, Marqueurs *parcours, NiveauHierarchique niveau, Statistiques *statistiques)
{
        //nettoie les references reductibles de la section en cours
        Contenu soussections;
        Reference references;
        Taille taillesoussections, taillereferences ;
        IdIdee ididee;
        Indice indice;
        SECURISE(ecritureniveauhierarchique_section(section,niveau));
        SECURISE(lecturesoussections_section(section,&soussections));
        SECURISE(lecturereferences_section(section,&references));
        SECURISE(taille_contenu(&soussections,&taillesoussections));
        SECURISE(taille_reference(&references,&taillereferences));
        SECURISE(lectureididee_section(section,&ididee));
        SECURISE(coche_marqueurs(parcours,ididee));
        statistiques->nombrereferencestotal += taillereferences;
        for(indice=0 ; indice<taillereferences ; indice++)
        {
                IdIdee idideedestinataire;
                Section *destination;
                TypeReference type;
                Booleen test;
                SECURISE(lecture_reference(&references,indice,&type,&destination));
                SECURISE(lectureididee_section(destination,&idideedestinataire));
                SECURISE(verification_marqueurs(parcours,idideedestinataire,&test));
                if(test==VRAI)
                {        //reference reductible
                        SECURISE(ecrituretype_reference(&references,indice,REFERENCE_REDUCTIBLE));
                }
                else
                {        //reference irreductible
                        SECURISE(ecrituretype_reference(&references,indice,REFERENCE_IRREDUCTIBLE));
                        (statistiques->nombrereferencesirreductibles)++;
                }
        }
        for(indice=0 ; indice<taillesoussections ; indice++)
        {        //et on parcourt toutes les sous sections...
                Section *soussection;
                SECURISE(lecture_contenu(&soussections,indice,&soussection));
                SECURISE(reductionreferencespeaufinageinterne_algo(soussection,listeidees,parcours,niveau+1,statistiques));
        }
        //ajoute une sous section en cas de sous section unique
        //ca marche car les nouvelles sections que l'on ajoute sont a la fin de la liste des idees
        //et qu'aucune reference n'est faite dessus
        if(taillesoussections==1)
        {//on doit ajouter une generalite
                Idee nouvelleidee;
                Section *nouvellesection;
                Contenu soussoussections;
                Reference sousreferences;
                IdIdee idideecourante, idnouvelleidee;
                SECURISE(lectureididee_section(section,&idideecourante));
                SECURISE(creation_idee(&nouvelleidee,IDEE_GENERIQUE,INDICE_IDEE_INVALIDE,idideecourante));
                SECURISE(ajoutidee_listeidees(listeidees,nouvelleidee));
                SECURISE(creation_contenu(&soussoussections));
                SECURISE(creation_reference(&sousreferences));
                SECURISE(taille_listeidees(listeidees,&idnouvelleidee));
                SECURISE(creation_section(&nouvellesection,idnouvelleidee-1,soussoussections,sousreferences));
                SECURISE(ecritureniveauhierarchique_section(nouvellesection,niveau+1));
                SECURISE(ajoutsection_contenu(&soussections,nouvellesection));
                SECURISE(echange_contenu(&soussections,0,1));
                SECURISE(ecrituresoussections_section(section,soussections));
                (statistiques->nombreideesgeneralites)++;
        }
        return RESULTAT_OK;
}

static Resultat reductionreferencespeaufinage_algo(Plan *plan, ListeIdees *listeidees, Statistiques *statistiques)
{
        //elimine les references reductibles
        Marqueurs parcours;
        Taille tailleidees;
        statistiques->nombreideesgeneralites=0;
        statistiques->nombrereferencestotal=0;
        statistiques->nombrereferencesirreductibles=0;
        SECURISE(taille_listeidees(listeidees,&tailleidees));
        SECURISE(creation_marqueurs(&parcours,tailleidees));
        SECURISE(reductionreferencespeaufinageinterne_algo(*plan,listeidees,&parcours,NIVEAU_RACINE,statistiques));
        SECURISE(destruction_marqueurs(&parcours));
        return RESULTAT_OK;
}


Resultat rechercheplan_algo(ListeIdees *listeidees, Graphe *dependances, Plan *plan, Statistiques *statistiques)
{
        //A partir des idees et de leurs dependances, contruit le plan.
        //L'algorithme detruit le graphe de dependances et cree de lui meme le plan
        References references;
        Taille taillelisteidees;
        //phase 1 : regroupement des idees communes
        SECURISE(regroupementidees_algo(listeidees,dependances,statistiques));
        //phase 2 : transformation du systeme en plan
        SECURISE(transformationplan_algo(dependances,plan));
        //phase 3 : reorganisation du plan
        SECURISE(taille_listeidees(listeidees,&taillelisteidees));
        SECURISE(optimisationplan_algo(plan,taillelisteidees,&references));
        //phase 4 : elimination depoussierage du plan trouve
        SECURISE(reductionreferencespeaufinage_algo(plan,listeidees,statistiques));
        SECURISE(taille_listeidees(listeidees,&(statistiques->nombreideestotal)));
        return RESULTAT_OK;
}

static Resultat rechercheaccessibiliteinterne_algo(Graphe *dependances, IdIdee idee, Marqueurs *marqueurs)
{
        //Renvoie la liste des sommets accessibles depuis l'idee indiquee.
        Relations dependance;
        IdIdee fils;
        Booleen relation;
        Booleen accessible;
        Pertinence pertinence;
        SECURISE(lecturedependances_graphe(dependances,&dependance));
        if(idee<0)
                return RESULTAT_ERREUR_DEPASSEMENT;
        if(idee>=dependance.taille)
                return RESULTAT_ERREUR_DEPASSEMENT;
        SECURISE(coche_marqueurs(marqueurs,idee));
        for(fils=0 ; fils<dependance.taille ; fils++)
        {
                SECURISE(testrelation_relations(&dependance,idee,fils,&relation,&pertinence));
                SECURISE(verification_marqueurs(marqueurs,fils,&accessible));
                if((relation==VRAI)&&(accessible==FAUX))
                {
                        SECURISE(rechercheaccessibiliteinterne_algo(dependances,fils,marqueurs));
                }
        }
        return RESULTAT_OK;
}

Resultat rechercheaccessibilite_algo(Graphe *dependances, Marqueurs *marqueurs)
{
        //Renvoie la liste des sommets accessibles depuis la racine.
        SECURISE(rechercheaccessibiliteinterne_algo(dependances,dependances->depart,marqueurs));
        return RESULTAT_OK;
}

Resultat ajouteracine_algo(ListeIdees *listeidees, Graphe *dependances, Marqueurs *orphelines, IdIdee *ididee)
{
        //Ajoute une idee racine au graphe de dependances. Les dependances entre
        //l'idee ajoutee et les idees orphelines sont ajoutees.
        Idee idee;
        IdIdee idorpheline;
        Booleen orpheline;
        Relations dependance;
        SECURISE(ajoutidee_graphe(dependances,1));//Une seule idee racine, vu que c'est le but recherche...
        SECURISE(lecturedependances_graphe(dependances,&dependance));
        SECURISE(taille_listeidees(listeidees,ididee));
        SECURISE(creation_idee(&idee,IDEE_RACINE,INDICE_IDEE_INVALIDE,INDICE_IDEE_INVALIDE));
        SECURISE(ajoutidee_listeidees(listeidees,idee));
        for(idorpheline=0 ; idorpheline<(*ididee) ; idorpheline++)
        {
                SECURISE(verification_marqueurs(orphelines,idorpheline,&orpheline));
                if(orpheline==FAUX)
                {
                        SECURISE(ajoutrelation_relations(&dependance,*ididee,idorpheline,PERTINENCE_RACINE));
                }
        }
        return RESULTAT_OK;
}

