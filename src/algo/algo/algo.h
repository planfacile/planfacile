/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __ALGOPLAN__
#define __ALGOPLAN__

#include <src/global/global.h>
#include <src/algo/donnees/contenu.h>
#include <src/algo/donnees/compteurboucles.h>
#include <src/algo/donnees/graphe.h>
#include <src/algo/donnees/idee.h>
#include <src/algo/donnees/liens.h>
#include <src/algo/donnees/listeboucles.h>
#include <src/algo/donnees/listeidees.h>
#include <src/algo/donnees/listeperepertinent.h>
#include <src/algo/donnees/listedependence.h>
#include <src/algo/donnees/marqueurs.h>
#include <src/algo/donnees/parcours.h>
#include <src/algo/donnees/reference.h>
#include <src/algo/donnees/relations.h>
#include <src/algo/donnees/section.h>

Resultat rechercheplan_algo(ListeIdees *listeidees, Graphe *dependances, Plan *plan, Statistiques *statistiques);
//A partir des idees et de leurs dependances, contruit le plan.
//L'algorithme detruit le graphe de dependances et cree de lui meme le plan
//la fonction affichemessage sert a imprimer les statistiques sur l'algorithme
//Attention : Cette partie provient de la version 1.0 de PlanFacile
//La gestion de la m�moire �tant diff�rente, l'acc�s � la variable
//General *general est strictement interdite. De plus, l'appel de cette
//fonction devra �tre am�nag� par une fonction r�alisant la transition
//entre les deux gestions de m�moire.

Resultat rechercheaccessibilite_algo(Graphe *dependances, Marqueurs *marqueurs);
//Renvoie la liste des sommets accessibles depuis la racine.
//Attention : Cette partie provient de la version 1.0 de PlanFacile
//La gestion de la m�moire �tant diff�rente, l'acc�s � la variable
//General *general est strictement interdite. De plus, l'appel de cette
//fonction devra �tre am�nag� par une fonction r�alisant la transition
//entre les deux gestions de m�moire.

Resultat ajouteracine_algo(ListeIdees *listeidees, Graphe *dependances, Marqueurs *orphelines, IdIdee *ididee);
//Ajoute une idee racine au graphe de dependances. Les dependances entre
//l'idee ajoutee et les idees orphelines sont ajoutees.
//Attention : Cette partie provient de la version 1.0 de PlanFacile
//La gestion de la m�moire �tant diff�rente, l'acc�s � la variable
//General *general est strictement interdite. De plus, l'appel de cette
//fonction devra �tre am�nag� par une fonction r�alisant la transition
//entre les deux gestions de m�moire.

#endif
