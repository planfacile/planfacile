/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

%{
#include <src/global/global.h>
#include <src/donnees/environnement/aideenvironnement.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/analyseur/analyseur/pilebuffer.h>
#include <src/analyseur/donnees/pilenomfichier.h>

#include <src/messages/messages/compilateur/langue.h>
#include <src/messages/messages/compilateur/listelangues.h>
#include <src/messages/messages/compilateur/donneesmessages.h>

#include "syntaxique.h"


extern STOCKAGE(LocalisationFichier) localisation;
extern STOCKAGE(PileNomFichier) pilerepertoire;
%}

%option nounput

%%

#" "                    {
                                return TOKEN_ESPACE;
                        }
#\t                     {
                                return TOKEN_TAB;
                        }
#\{                     {
                                return TOKEN_ACCOUV;
                        }
#\}                     {
                                return TOKEN_ACCFER;
                        }
##                      {
                                return TOKEN_DIESE;
                        }
#\n                     {
                        }

#output                 {
                                return TOKEN_OUTPUT;
                        }
#function               {
                                return TOKEN_FUNCTION;
                        }
#lang                   {
                                return TOKEN_LANG;
                        }
#charset                {
                                return TOKEN_CHARSET;
                        }
#mesg                   {
                                return TOKEN_MESG;
                        }
#end                    {
                                return TOKEN_FIN;
                        }
#default_lang           {
                                return TOKEN_DEFAUT_LANG;
                        }
#default_charset        {
                                return TOKEN_DEFAUT_CHARSET;
                        }
#default_mesg           {
                                return TOKEN_DEFAUT_MESG;
                        }
#[0-9]+                 {
                                yylval._Entier=atoi(yytext+1)-1;
                                return TOKEN_PARAM;
                        }

#include[ \t\n]+[^\n]+  {
                                STOCKAGE_SCALAIRE(Chaine) include;
                                TRAVAIL_SCALAIRE(NomFichier) fichier;
                                TRAVAIL_SCALAIRE(NomFichier) cheminabsolu;
                                TRAVAIL_SCALAIRE(Booleen) cheminvalide;
                                STOCKAGE_SCALAIRE(DescripteurFichier) fichierinclus;
                                REFERENCE_SCALAIRE(Caractere) parcours;

                                include=DUP_CAST(Chaine, T_S_(yytext));
                                ASSERTION(include!=NULL, RESULTAT_ERREUR_MEMOIRE);

                                parcours=CAST_R_(Caractere, include)+8;
                                while(T_R_(parcours)==T_S_(' ')||T_R_(parcours)==T_S_('\t')||T_R_(parcours)==T_S_('\n'))
                                        parcours++;
                                fichier=CAST_T_(NomFichier, parcours);
                                SECURISE(aideenvironnement_ajout_repertoirefichier(T_S(pilerepertoire), T_S_(fichier), R_T_(cheminabsolu), R_T_(cheminvalide)));
                                if(cheminvalide==T_S_(FAUX))
                                {
                                        fprintf(SORTIEERREUR, "Error: Invalid filename '%s'.\n", T_S_(fichier));
                                }
                                else if((fichierinclus=FOPEN(cheminabsolu, T_S_("r")))==NULL)
                                {
                                        fprintf(SORTIEERREUR, "Error: Cannot open file '%s'.\n", cheminabsolu);
                                }
                                else
                                {
                                        SECURISE(localisationfichier_ajout(T_S(localisation),T_S_(cheminabsolu),T_S_(NULL),T_S_(fichierinclus),T_S_(1)));
                                        T_S_(yyin)=fichierinclus;
                                        SECURISE(buffer_empile());
                                        yy_switch_to_buffer(yy_create_buffer(T_S_(yyin),YY_BUF_SIZE));
                                }
                                free(include);
                        }

\{[^\n \t}]*\}          {
                                yylval._Chaine=DUP_CAST(Chaine, yytext+1);
                                yylval._Chaine[STRLEN(yylval._Chaine)-T_S_(1)]='\0';
                                return TOKEN_PARAM_NOM;
                        }
\{[^}]*\}               {
                                yylval._Chaine=DUP_CAST(Chaine, yytext+1);
                                yylval._Chaine[STRLEN(yylval._Chaine)-T_S_(1)]='\0';
                                return TOKEN_PARAM_FICHIER;
                        }
[^#{ \t\n\/][^#\n]*     {
                                yylval._Chaine=DUP_CAST(Chaine, yytext);
                                return TOKEN_TEXTE;
                        }
[ \t\n]*                {
                        }
#pass[^\n]*             {
                                printf("%s\n",yytext+1);
                                fflush(SORTIESTANDARD);
                        }
\/[^\n]*\n              {
                        }

%%

int yywrap( void )
{
        TRAVAIL_SCALAIRE(Booleen) vide;
        fclose(T_S_(yyin));
        SECURISE(aideenvironnement_retrait_repertoirefichier(T_S(pilerepertoire)));
        SECURISE(localisationfichier_retrait(T_S(localisation)));
        SECURISE(localisationfichier_vide(T_S(localisation), R_T_(vide)));
        if(vide==T_S_(VRAI))
        {
                yylex_destroy();
                return 1;
        }
        SECURISE(localisationfichier_lecture_descripteur(T_S(localisation), R_S_(yyin)));
        SECURISE(buffer_depile());
        return 0;
}
