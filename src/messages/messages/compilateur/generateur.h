/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013        Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __GENERATEUR__
#define __GENERATEUR__

#include <src/global/global.h>
#include <src/messages/messages/compilateur/donneesmessages.h>


Resultat generateur_genererfichier_entete(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) repertoiresortie);

Resultat generateur_genererfichier_source(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) repertoiresortie);

#endif
