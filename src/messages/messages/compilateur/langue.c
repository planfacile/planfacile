/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#include "langue.h"
#include "listelangues.h"


#define _DEFINITION(type, nom, typechamp, champ, TR, MOD_CHAMP) \
        Resultat nom##_definition_##champ (TRAVAIL(type) nom, TR(typechamp) champ)        \
        {                                                                                 \
                ASSERTION(S_T(nom)!=NULL, RESULTAT_ERREUR);                               \
                CHAMP(nom, champ)=MOD_CHAMP(champ);                                       \
                return RESULTAT_OK;                                                       \
        }
#define DEFINITION_CHAMP(type, nom, typechamp, champ) \
        _DEFINITION(type, nom, typechamp, champ, TRAVAIL, S_T)
#define DEFINITION_CHAMP_SCALAIRE(type, nom, typechamp, champ) \
        _DEFINITION(type, nom, typechamp, champ, TRAVAIL_SCALAIRE, S_T_)

#define _LECTURE(type, nom, typechamp, champ, REF, MOD_REFCHAMP, MOD_CHAMP) \
        Resultat nom##_lecture_##champ (TRAVAIL(type) nom, REF(typechamp) champ)        \
        {                                                                               \
                ASSERTION(S_T(nom)!=NULL, RESULTAT_ERREUR);                             \
                MOD_REFCHAMP(champ)=MOD_CHAMP(CHAMP(nom, champ));                       \
                return RESULTAT_OK;                                                     \
        }
#define LECTURE_CHAMP(type, nom, typechamp, champ) \
        _LECTURE(type, nom, typechamp, champ, REFERENCE, T_R, T_S)
#define LECTURE_CHAMP_SCALAIRE(type, nom, typechamp, champ) \
        _LECTURE(type, nom, typechamp, champ, REFERENCE_SCALAIRE, T_R_, T_S_)

#define DEFINITION_LECTURE_CHAMP(type, nom, typechamp, champ) \
        DEFINITION_CHAMP(type, nom, typechamp, champ)         \
        LECTURE_CHAMP(type, nom, typechamp, champ)
#define DEFINITION_LECTURE_CHAMP_SCALAIRE(type, nom, typechamp, champ) \
        DEFINITION_CHAMP_SCALAIRE(type, nom, typechamp, champ)         \
        LECTURE_CHAMP_SCALAIRE(type, nom, typechamp, champ)


Resultat texte_initialisation(TRAVAIL(Texte) texte)
{
        ASSERTION((S_T(texte)=NOUVEAU(Texte))!=NULL, RESULTAT_ERREUR_MEMOIRE);
        CHAMP(texte, contenu)=NULL;
        CHAMP(texte, type)=TYPETEXTE_TEXTE;
        return RESULTAT_OK;
}

#define TEXTE_DEFINITION_LECTURE_CHAMP_SCALAIRE(type, champ) \
        DEFINITION_LECTURE_CHAMP_SCALAIRE(Texte, texte, type, champ)

TEXTE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, contenu)
TEXTE_DEFINITION_LECTURE_CHAMP_SCALAIRE(TypeTexte, type)

Resultat texte_copie(TRAVAIL(Texte) texte, TRAVAIL(Texte) copie)
{
        ASSERTION(S_T(texte)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T(copie)!=NULL, RESULTAT_ERREUR);

        SECURISE(texte_initialisation(copie));
        SECURISE(texte_definition_contenu(copie, T_S_(DUP_CAST(Chaine, T_S_(CHAMP(texte, contenu))))));
        SECURISE(texte_definition_type(copie, T_S_(CHAMP(texte, type))));
        return RESULTAT_OK;
}

Resultat texte_destruction(TRAVAIL(Texte) texte)
{
        ASSERTION(S_T(texte)!=NULL, RESULTAT_ERREUR);

        if(CHAMP(texte, contenu)!=NULL)
                free(CHAMP(texte, contenu));
        free(S_T(texte));
        S_T(texte)=NULL;
        return RESULTAT_OK;
}

Resultat parametre_initialisation(TRAVAIL(Parametre) parametre)
{
        ASSERTION((S_T(parametre)=NOUVEAU(Parametre))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(parametre, indice)=0;
        CHAMP(parametre, type)=TYPEPARAMETRE_CHAINE;
        SECURISE(texte_initialisation(CHAMP_TRAVAIL(parametre, format)));
        return RESULTAT_OK;
}

#define PARAMETRE_DEFINITION_LECTURE_CHAMP(type, champ) \
        DEFINITION_LECTURE_CHAMP(Parametre, parametre, type, champ)
#define PARAMETRE_DEFINITION_LECTURE_CHAMP_SCALAIRE(type, champ) \
        DEFINITION_LECTURE_CHAMP_SCALAIRE(Parametre, parametre, type, champ)

PARAMETRE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Entier, indice)
PARAMETRE_DEFINITION_LECTURE_CHAMP_SCALAIRE(TypeParametre, type)
PARAMETRE_DEFINITION_LECTURE_CHAMP(Texte, format)

Resultat parametre_destruction(TRAVAIL(Parametre) parametre)
{
        ASSERTION(S_T(parametre)!=NULL, RESULTAT_ERREUR);

        SECURISE(texte_destruction(CHAMP_TRAVAIL(parametre, format)));
        free(S_T(parametre));
        S_T(parametre)=NULL;
        return RESULTAT_OK;
}

Resultat message_initialisation(TRAVAIL(Message) message)
{
        ASSERTION((S_T(message)=NOUVEAU(Message))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(message, identifiant)=NULL;
        CHAMP(message, detection)=NULL;
        SECURISE(listetexte_initialisation(CHAMP_TRAVAIL(message, texte)));
        SECURISE(listeparametre_initialisation(CHAMP_TRAVAIL(message, parametres)));
        return RESULTAT_OK;
}

#define MESSAGE_DEFINITION_LECTURE_CHAMP(type, champ) \
        DEFINITION_LECTURE_CHAMP(Message, message, type, champ)
#define MESSAGE_DEFINITION_LECTURE_CHAMP_SCALAIRE(type, champ) \
        DEFINITION_LECTURE_CHAMP_SCALAIRE(Message, message, type, champ)

MESSAGE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, identifiant)
MESSAGE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, detection)
MESSAGE_DEFINITION_LECTURE_CHAMP(ListeTexte, texte)
MESSAGE_DEFINITION_LECTURE_CHAMP(ListeParametre, parametres)

Resultat message_destruction(TRAVAIL(Message) message)
{
        ASSERTION(S_T(message)!=NULL, RESULTAT_ERREUR);

        SECURISE(listetexte_destruction(CHAMP_TRAVAIL(message, texte)));
        SECURISE(listeparametre_destruction(CHAMP_TRAVAIL(message, parametres)));
        if(CHAMP(message, identifiant)!=NULL)
                free(CHAMP(message, identifiant));
        if(CHAMP(message, detection)!=NULL)
                free(CHAMP(message, detection));
        free(S_T(message));
        S_T(message)=NULL;
        return RESULTAT_OK;
}

Resultat charset_initialisation(TRAVAIL(Charset) charset)
{
        ASSERTION((S_T(charset)=NOUVEAU(Charset))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(charset, identifiant)=NULL;
        CHAMP(charset, detection)=NULL;
        CHAMP(charset, messagedefaut)=NULL;
        SECURISE(listemessage_initialisation(CHAMP_TRAVAIL(charset, messages)));
        return RESULTAT_OK;
}

#define CHARSET_DEFINITION_LECTURE_CHAMP(type, champ) \
        DEFINITION_LECTURE_CHAMP(Charset, charset, type, champ)
#define CHARSET_DEFINITION_LECTURE_CHAMP_SCALAIRE(type, champ) \
        DEFINITION_LECTURE_CHAMP_SCALAIRE(Charset, charset, type, champ)

CHARSET_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, identifiant)
CHARSET_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, detection)
CHARSET_DEFINITION_LECTURE_CHAMP(ListeMessage, messages)
CHARSET_DEFINITION_LECTURE_CHAMP(Message, messagedefaut)

Resultat charset_destruction(TRAVAIL(Charset) charset)
{
        ASSERTION(S_T(charset)!=NULL, RESULTAT_ERREUR);

        SECURISE(listemessage_destruction(CHAMP_TRAVAIL(charset, messages)));
        if(CHAMP(charset, identifiant)!=NULL)
                free(CHAMP(charset, identifiant));
        if(CHAMP(charset, detection)!=NULL)
                free(CHAMP(charset, detection));
        free(S_T(charset));
        S_T(charset)=NULL;
        return RESULTAT_OK;
}

Resultat langue_initialisation(TRAVAIL(Langue) langue)
{
        ASSERTION((S_T(langue)=NOUVEAU(Langue))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(langue, identifiant)=NULL;
        CHAMP(langue, detection)=NULL;
        CHAMP(langue, charsetdefaut)=NULL;
        SECURISE(listecharset_initialisation(CHAMP_TRAVAIL(langue, charsets)));
        return RESULTAT_OK;
}

#define LANGUE_DEFINITION_LECTURE_CHAMP(type, champ) \
        DEFINITION_LECTURE_CHAMP(Langue, langue, type, champ)
#define LANGUE_DEFINITION_LECTURE_CHAMP_SCALAIRE(type, champ) \
        DEFINITION_LECTURE_CHAMP_SCALAIRE(Langue, langue, type, champ)

LANGUE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, identifiant)
LANGUE_DEFINITION_LECTURE_CHAMP_SCALAIRE(Chaine, detection)
LANGUE_DEFINITION_LECTURE_CHAMP(ListeCharset, charsets)
LANGUE_DEFINITION_LECTURE_CHAMP(Charset, charsetdefaut)

Resultat langue_destruction(TRAVAIL(Langue) langue)
{
        ASSERTION(S_T(langue)!=NULL, RESULTAT_ERREUR);

        SECURISE(listecharset_destruction(CHAMP_TRAVAIL(langue, charsets)));
        if(CHAMP(langue, identifiant)!=NULL)
                free(CHAMP(langue, identifiant));
        if(CHAMP(langue, detection)!=NULL)
                free(CHAMP(langue, detection));
        free(S_T(langue));
        S_T(langue)=NULL;
        return RESULTAT_OK;
}
