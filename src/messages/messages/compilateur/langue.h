/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013 Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LANGUE__
#define __LANGUE__

#include <src/global/global.h>

typedef struct listetexte CONTENEUR(ListeTexte);
typedef struct listeparametre CONTENEUR(ListeParametre);
typedef struct listemessage CONTENEUR(ListeMessage);
typedef struct listecharset CONTENEUR(ListeCharset);
typedef struct listelangue CONTENEUR(ListeLangue);


typedef enum
{
        TYPEPARAMETRE_ENTIER,
        TYPEPARAMETRE_FLOTTANT,
        TYPEPARAMETRE_DOUBLE,
        TYPEPARAMETRE_CARACTERE,
        TYPEPARAMETRE_CHAINE,
        TYPEPARAMETRE_POINTEUR
} TypeParametre;

typedef enum
{
        TYPETEXTE_TEXTE,
        TYPETEXTE_PARAMETRE
} TypeTexte;


typedef struct texte CONTENEUR(Texte);
typedef struct parametre CONTENEUR(Parametre);
typedef struct message CONTENEUR(Message);
typedef struct charset CONTENEUR(Charset);
typedef struct langue CONTENEUR(Langue);

struct texte
{
        STOCKAGE_SCALAIRE(Chaine) contenu;
        STOCKAGE_SCALAIRE(TypeTexte) type;
};

struct parametre
{
        STOCKAGE_SCALAIRE(Entier) indice;
        STOCKAGE_SCALAIRE(TypeParametre) type;
        STOCKAGE(Texte) format;
};

struct message
{
        STOCKAGE_SCALAIRE(Chaine) identifiant;
        STOCKAGE_SCALAIRE(Chaine) detection;
        STOCKAGE(ListeTexte) texte;
        STOCKAGE(ListeParametre) parametres;
};

struct charset
{
        STOCKAGE_SCALAIRE(Chaine) identifiant;
        STOCKAGE_SCALAIRE(Chaine) detection;
        //Cette chaine doit etre liberee a la destruction
        STOCKAGE(ListeMessage) messages;
        STOCKAGE(Message) messagedefaut;
};

struct langue
{
        STOCKAGE_SCALAIRE(Chaine) identifiant;
        STOCKAGE_SCALAIRE(Chaine) detection;
        //Cette chaine doit etre liberee a la destruction
        STOCKAGE(ListeCharset) charsets;
        STOCKAGE(Charset) charsetdefaut;
};


Resultat texte_initialisation(TRAVAIL(Texte) texte);
Resultat texte_definition_contenu(TRAVAIL(Texte) texte, TRAVAIL_SCALAIRE(Chaine) contenu);
Resultat texte_definition_type(TRAVAIL(Texte) texte, TRAVAIL_SCALAIRE(TypeTexte) type);
Resultat texte_lecture_contenu(TRAVAIL(Texte) texte, REFERENCE_SCALAIRE(Chaine) contenu);
Resultat texte_lecture_type(TRAVAIL(Texte) texte, REFERENCE_SCALAIRE(TypeTexte) type);
Resultat texte_copie(TRAVAIL(Texte) texte, TRAVAIL(Texte) copie);
Resultat texte_destruction(TRAVAIL(Texte) texte);

Resultat parametre_initialisation(TRAVAIL(Parametre) parametre);
Resultat parametre_definition_indice(TRAVAIL(Parametre) parametre, TRAVAIL_SCALAIRE(Entier) indice);
Resultat parametre_definition_type(TRAVAIL(Parametre) parametre, TRAVAIL_SCALAIRE(TypeParametre) type);
Resultat parametre_definition_format(TRAVAIL(Parametre) parametre, TRAVAIL(Texte) format);
Resultat parametre_lecture_indice(TRAVAIL(Parametre) parametre, REFERENCE_SCALAIRE(Entier) indice);
Resultat parametre_lecture_type(TRAVAIL(Parametre), REFERENCE_SCALAIRE(TypeParametre) type);
Resultat parametre_lecture_format(TRAVAIL(Parametre), REFERENCE(Texte) format);
Resultat parametre_destruction(TRAVAIL(Parametre) parametre);

Resultat message_initialisation(TRAVAIL(Message) message);
Resultat message_definition_identifiant(TRAVAIL(Message) message, TRAVAIL_SCALAIRE(Chaine) identifiant);
Resultat message_definition_detection(TRAVAIL(Message) message, TRAVAIL_SCALAIRE(Chaine) detection);
Resultat message_definition_texte(TRAVAIL(Message) message, TRAVAIL(ListeTexte) texte);
Resultat message_definition_parametres(TRAVAIL(Message) message, TRAVAIL(ListeParametre) parametres);
Resultat message_lecture_identifiant(TRAVAIL(Message) message, REFERENCE_SCALAIRE(Chaine) identifiant);
Resultat message_lecture_detection(TRAVAIL(Message) message, REFERENCE_SCALAIRE(Chaine) detection);
Resultat message_lecture_texte(TRAVAIL(Message) message, REFERENCE(ListeTexte) texte);
Resultat message_lecture_parametres(TRAVAIL(Message) message, REFERENCE(ListeParametre) parametres);
Resultat message_destruction(TRAVAIL(Message) message);

Resultat charset_initialisation(TRAVAIL(Charset) charset);
Resultat charset_definition_identifiant(TRAVAIL(Charset) charset, TRAVAIL_SCALAIRE(Chaine) identifiant);
Resultat charset_definition_detection(TRAVAIL(Charset) charset, TRAVAIL_SCALAIRE(Chaine) detection);
Resultat charset_definition_messages(TRAVAIL(Charset) charset, TRAVAIL(ListeMessage) messages);
Resultat charset_definition_messagedefaut(TRAVAIL(Charset) charset, TRAVAIL(Message) messagedefaut);
Resultat charset_lecture_identifiant(TRAVAIL(Charset) charset, REFERENCE_SCALAIRE(Chaine) identifiant);
Resultat charset_lecture_detection(TRAVAIL(Charset) charset, REFERENCE_SCALAIRE(Chaine) detection);
Resultat charset_lecture_messages(TRAVAIL(Charset) charset, REFERENCE(ListeMessage) messages);
Resultat charset_lecture_messagedefaut(TRAVAIL(Charset) charset, REFERENCE(Message) messagedefaut);
Resultat charset_destruction(TRAVAIL(Charset) charset);

Resultat langue_initialisation(TRAVAIL(Langue) langue);
Resultat langue_definition_identifiant(TRAVAIL(Langue) langue, TRAVAIL_SCALAIRE(Chaine) identifiant);
Resultat langue_definition_detection(TRAVAIL(Langue) langue, TRAVAIL_SCALAIRE(Chaine) detection);
Resultat langue_definition_charsets(TRAVAIL(Langue) langue, TRAVAIL(ListeCharset) charsets);
Resultat langue_definition_charsetdefaut(TRAVAIL(Langue) langue, TRAVAIL(Charset) charsetdefaut);
Resultat langue_lecture_identifiant(TRAVAIL(Langue) langue, REFERENCE_SCALAIRE(Chaine) identifiant);
Resultat langue_lecture_detection(TRAVAIL(Langue) langue, REFERENCE_SCALAIRE(Chaine) detection);
Resultat langue_lecture_charsets(TRAVAIL(Langue) langue, REFERENCE(ListeCharset) charsets);
Resultat langue_lecture_charsetdefaut(TRAVAIL(Langue) langue, REFERENCE(Charset) charsetdefaut);
Resultat langue_destruction(TRAVAIL(Langue) langue);

#endif
