/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013  Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __DONNEESLANGUES__
#define __DONNEESLANGUES__

#include <src/global/global.h>
#include <src/messages/messages/compilateur/listelangues.h>


typedef struct donneesmessages CONTENEUR(DonneesMessages);

struct donneesmessages
{
        STOCKAGE_SCALAIRE(Chaine) nomfichier;
        STOCKAGE_SCALAIRE(Chaine) nomfonctionmessage;
        STOCKAGE_SCALAIRE(Chaine) nomfonctionlocalisation;
        STOCKAGE_SCALAIRE(Chaine) nomfonctionparametres;
        STOCKAGE(ListeLangue) langues;
        STOCKAGE(Langue) languedefaut;
        STOCKAGE(ListeCharset) detectioncharsets;
        STOCKAGE(ListeMessage) detectionmessages;
};


Resultat donneesmessages_initialisation(TRAVAIL(DonneesMessages) donnees);

Resultat donneesmessages_definition_nomfichier(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) nomfichier);
Resultat donneesmessages_definition_nomfonctionmessage(TRAVAIL(DonneesMessages) donneed, TRAVAIL_SCALAIRE(Chaine) nomfonctionmessage);
Resultat donneesmessages_definition_nomfonctionlocalisation(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) nomfonctionlocalisation);
Resultat donneesmessages_definition_nomfonctionparametres(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) nomfonctionparametres);
Resultat donneesmessages_definition_langues(TRAVAIL(DonneesMessages) donnees, TRAVAIL(ListeLangue) langues);
Resultat donneesmessages_definition_languedefaut(TRAVAIL(DonneesMessages) donnees, TRAVAIL(Langue) languedefaut);
Resultat donneesmessages_ajout_detectioncharset(TRAVAIL(DonneesMessages) donnees, TRAVAIL(Charset) charset);
Resultat donneesmessages_ajout_detectionmessage(TRAVAIL(DonneesMessages) donnees, TRAVAIL(Message) message);

Resultat donneesmessages_lecture_nomfichier(TRAVAIL(DonneesMessages) donnees, REFERENCE_SCALAIRE(Chaine) nomfichier);
Resultat donneesmessages_lecture_nomfonctionmessage(TRAVAIL(DonneesMessages) donnees, REFERENCE_SCALAIRE(Chaine) nomfonctionmessage);
Resultat donneesmessages_lecture_nomfonctionlocalisation(TRAVAIL(DonneesMessages) donnees, REFERENCE_SCALAIRE(Chaine) nomfonctionlocalisation);
Resultat donneesmessages_lecture_nomfonctionparametres(TRAVAIL(DonneesMessages) donnees, REFERENCE_SCALAIRE(Chaine) nomfonctionparametres);
Resultat donneesmessages_lecture_langues(TRAVAIL(DonneesMessages) donnees, REFERENCE(ListeLangue) langues);
Resultat donneesmessages_lecture_languedefaut(TRAVAIL(DonneesMessages) donnees, REFERENCE(Langue) languedefaut);
Resultat donneesmessages_lecture_detectioncharsets(TRAVAIL(DonneesMessages) donnees, REFERENCE(ListeCharset) detectioncharsets);
Resultat donneesmessages_lecture_detectionmessages(TRAVAIL(DonneesMessages) donnees, REFERENCE(ListeMessage) detectionmessages);

Resultat donneesmessages_destruction(TRAVAIL(DonneesMessages) donnees);

#endif
