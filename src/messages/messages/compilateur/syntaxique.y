/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *  Copyright (C) 2013        Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

%{
#include <src/global/global.h>
#include <src/donnees/environnement/aideenvironnement.h>
#include <src/donnees/commandes/localisationfichier.h>
#include <src/analyseur/donnees/pilenomfichier.h>

#include <src/messages/messages/compilateur/donneesmessages.h>
#include <src/messages/messages/compilateur/langue.h>
#include <src/messages/messages/compilateur/listelangues.h>
#include <src/messages/messages/compilateur/generateur.h>

#define YYDEBUG 1

#define DEFINIR_TEXTE(texte, chaine) \
        do {\
                SECURISE(texte_initialisation(texte));\
                SECURISE(texte_definition_contenu(texte, T_S_(chaine)));\
                SECURISE(texte_definition_type(texte, T_S_(TYPETEXTE_TEXTE)));\
        } while(0)

extern DescripteurFichier yyin;
extern int yylex(void);

void yyerror(TRAVAIL(DonneesMessages), TRAVAIL_SCALAIRE(Chaine));


STOCKAGE(LocalisationFichier) localisation;
STOCKAGE(PileNomFichier) pilerepertoire;
%}

%union
{
        STOCKAGE_SCALAIRE(Chaine) _Chaine;
        STOCKAGE_SCALAIRE(Entier) _Entier;
        STOCKAGE(Message)         _Message;
        STOCKAGE(Texte)           _Texte;
        STOCKAGE(Parametre)       _Parametre;
        STOCKAGE(Charset)         _Charset;
        STOCKAGE(Langue)          _Langue;
        STOCKAGE(ListeLangue)     _ListeLangue;
        STOCKAGE(ListeCharset)    _ListeCharset;
        STOCKAGE(ListeMessage)    _ListeMessage;
}

%token TOKEN_OUTPUT
%token TOKEN_FUNCTION
%token TOKEN_LANG
%token TOKEN_CHARSET
%token TOKEN_MESG
%token <_Entier> TOKEN_PARAM
%token TOKEN_ESPACE
%token TOKEN_TAB
%token TOKEN_DIESE
%token TOKEN_ACCOUV
%token TOKEN_ACCFER
%token <_Chaine> TOKEN_PARAM_NOM
%token <_Chaine> TOKEN_PARAM_FICHIER
%token TOKEN_FIN
%token TOKEN_DEFAUT_LANG
%token TOKEN_DEFAUT_CHARSET
%token TOKEN_DEFAUT_MESG
%token <_Chaine> TOKEN_TEXTE

%type <_Chaine> nomfichier
%type <_Chaine> output
%type <_Chaine> functionmessage
%type <_Chaine> functionlocalisation
%type <_Chaine> functionparametres

%type <_Texte> mot
%type <_Parametre> parametre
%type <_Chaine> default_lang
%type <_Chaine> default_charset
%type <_Message> default_mesg
%type <_Message> message
%type <_Message> mesg
%type <_Charset> charset
%type <_Langue> lang
%type <_ListeLangue> liste_lang
%type <_ListeCharset> liste_charset
%type <_ListeMessage> liste_mesg

%start fichier

%parse-param {TRAVAIL(DonneesMessages) donnees}

%%

fichier: output functionmessage functionlocalisation functionparametres liste_lang default_lang
{
        TRAVAIL(Langue) langue;
        SECURISE(donneesmessages_definition_nomfichier(donnees, T_S_($1)));
        SECURISE(donneesmessages_definition_nomfonctionmessage(donnees, T_S_($2)));
        SECURISE(donneesmessages_definition_nomfonctionlocalisation(donnees, T_S_($3)));
        SECURISE(donneesmessages_definition_nomfonctionparametres(donnees, T_S_($4)));
        SECURISE(donneesmessages_definition_langues(donnees, T_S($5)));
        SECURISE(listelangue_recherche_langue(T_S($5), T_S_($6), R_T(langue)));
        SECURISE(donneesmessages_definition_languedefaut(donnees, langue));
};

output: TOKEN_OUTPUT nomfichier
{
        $$=$2;
};

nomfichier: TOKEN_PARAM_NOM
{
        $$=$1;
}
|        TOKEN_PARAM_FICHIER
{
        $$=$1;
};

functionmessage: TOKEN_FUNCTION TOKEN_PARAM_NOM
{
        $$=$2;
};

functionlocalisation: TOKEN_FUNCTION TOKEN_PARAM_NOM
{
        $$=$2;
};

functionparametres: TOKEN_FUNCTION TOKEN_PARAM_NOM
{
        $$=$2;
};

liste_lang:
{
        STOCKAGE(ListeLangue) langues;
        SECURISE(listelangue_initialisation(T_S(langues)));
        $$=langues;
}
|        liste_lang lang
{
        SECURISE(listelangue_ajout_langue(T_S($1), T_S($2)));
        $$=$1;
};

lang: TOKEN_LANG TOKEN_PARAM_NOM liste_charset default_charset TOKEN_FIN
{
        STOCKAGE(Langue) langue;
        TRAVAIL(Charset) charsetdefaut;
        SECURISE(langue_initialisation(T_S(langue)));
        SECURISE(langue_definition_identifiant(T_S(langue), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        SECURISE(langue_definition_charsets(T_S(langue), T_S($3)));
        SECURISE(listecharset_recherche_charset(T_S($3), T_S_($4), R_T(charsetdefaut)));
        SECURISE(langue_definition_charsetdefaut(T_S(langue), charsetdefaut));
        $$=langue;
}
|        TOKEN_LANG TOKEN_PARAM_NOM TOKEN_PARAM_NOM liste_charset default_charset TOKEN_FIN
{
        STOCKAGE(Langue) langue;
        TRAVAIL(Charset) charsetdefaut;
        SECURISE(langue_initialisation(T_S(langue)));
        SECURISE(langue_definition_identifiant(T_S(langue), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        SECURISE(langue_definition_detection(T_S(langue), (TRAVAIL_SCALAIRE(Chaine))(T_S_($3))));
        SECURISE(langue_definition_charsets(T_S(langue), T_S($4)));
        SECURISE(listecharset_recherche_charset(T_S($4), T_S_($5), R_T(charsetdefaut)));
        SECURISE(langue_definition_charsetdefaut(T_S(langue), charsetdefaut));
        $$=langue;
}


liste_charset:
{
        STOCKAGE(ListeCharset) charsets;
        SECURISE(listecharset_initialisation(T_S(charsets)));
        $$=charsets;
}
|        liste_charset charset
{
        SECURISE(listecharset_ajout_charset(T_S($1), T_S($2)));
        SECURISE(donneesmessages_ajout_detectioncharset(donnees, T_S($2)));
        $$=$1;
};

charset: TOKEN_CHARSET TOKEN_PARAM_NOM liste_mesg default_mesg TOKEN_FIN
{
        STOCKAGE(Charset) charset;        
        SECURISE(charset_initialisation(T_S(charset)));
        SECURISE(charset_definition_identifiant(T_S(charset), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        SECURISE(charset_definition_messages(T_S(charset), T_S($3)));
        SECURISE(charset_definition_messagedefaut(T_S(charset), T_S($4)));
        $$=charset;
}
|        TOKEN_CHARSET TOKEN_PARAM_NOM TOKEN_PARAM_NOM liste_mesg default_mesg TOKEN_FIN
{
        STOCKAGE(Charset) charset;
        SECURISE(charset_initialisation(T_S(charset)));
        SECURISE(charset_definition_identifiant(T_S(charset), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        SECURISE(charset_definition_detection(T_S(charset), (TRAVAIL_SCALAIRE(Chaine))(T_S_($3))));
        SECURISE(charset_definition_messages(T_S(charset), T_S($4)));
        SECURISE(charset_definition_messagedefaut(T_S(charset), T_S($5)));
        $$=charset;
};

liste_mesg:
{
        STOCKAGE(ListeMessage) listemessage;
        SECURISE(listemessage_initialisation(T_S(listemessage)));
        $$=listemessage;
}
|        liste_mesg mesg
{
        SECURISE(listemessage_ajout_message(T_S($1), T_S($2)));
        SECURISE(donneesmessages_ajout_detectionmessage(donnees, T_S($2)));
        $$=$1;
};

mesg: TOKEN_MESG TOKEN_PARAM_NOM message TOKEN_FIN
{
        SECURISE(message_definition_identifiant(T_S($3), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        $$=$3;
}
|        TOKEN_MESG TOKEN_PARAM_NOM TOKEN_PARAM_NOM message TOKEN_FIN
{
        SECURISE(message_definition_identifiant(T_S($4), (TRAVAIL_SCALAIRE(Chaine))(T_S_($2))));
        SECURISE(message_definition_detection(T_S($4), (TRAVAIL_SCALAIRE(Chaine))(T_S_($3))));
        $$=$4;
};

message:
{
        STOCKAGE(Message) message;        
        SECURISE(message_initialisation(T_S(message)));
        $$=message;
}
|        message mot
{
        TRAVAIL(ListeTexte) listetexte;
        SECURISE(message_lecture_texte(T_S($1), R_T(listetexte)));
        SECURISE(listetexte_ajout_texte(listetexte, T_S($2)));
        $$=$1;
}
|        message parametre
{
        TRAVAIL(ListeTexte) listetexte;
        TRAVAIL(ListeParametre) listeparametres;
        TRAVAIL(Texte) format;
        STOCKAGE(Texte) texte;
        SECURISE(parametre_lecture_format(T_S($2), R_T(format)));
        SECURISE(texte_copie(format, T_S(texte)));
        SECURISE(message_lecture_texte(T_S($1), R_T(listetexte)));
        SECURISE(listetexte_ajout_texte(listetexte, T_S(texte)));
        SECURISE(message_lecture_parametres(T_S($1), R_T(listeparametres)));
        SECURISE(listeparametre_ajout_parametre(listeparametres, T_S($2)));
        $$=$1;
};

mot:        TOKEN_TEXTE
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), yylval._Chaine);
        $$=mot;
}
|        TOKEN_ESPACE
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), STRDUP(" "));
        $$=mot;
}
|        TOKEN_TAB
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), STRDUP("\t"));
        $$=mot;
}
|        TOKEN_DIESE
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), STRDUP("#"));
        $$=mot;
}
|        TOKEN_ACCOUV
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), STRDUP("{"));
        $$=mot;
}
|        TOKEN_ACCFER
{
        STOCKAGE(Texte) mot;
        DEFINIR_TEXTE(T_S(mot), STRDUP("}"));
        $$=mot;
};

parametre:        TOKEN_PARAM TOKEN_PARAM_NOM
{
        STOCKAGE(Parametre) parametre;
        STOCKAGE(Texte) texte;
        TRAVAIL_SCALAIRE(TypeParametre) type;
        STOCKAGE_SCALAIRE(Chaine) format;
        TRAVAIL_SCALAIRE(Entier) tailleformat;
        REFERENCE_SCALAIRE(Caractere) parcours;

        tailleformat=STRLEN(T_S_($2))+T_S_(2);
        format=CAST_S_(Chaine, ALLOCATION_N_(Caractere, tailleformat));
        ASSERTION(format!=NULL, RESULTAT_ERREUR_MEMOIRE);
        SNPRINTF(T_S_(format), tailleformat, T_S_("%%%s"), T_S_($2));
        parcours=CAST_R_(Caractere, format)+(STRLEN(format)-1);
        switch(T_R_(parcours))
        {
                case T_S_('d'):
                case T_S_('i'):
                case T_S_('o'):
                case T_S_('u'):
                case T_S_('x'):
                case T_S_('X'):
                        type=T_S_(TYPEPARAMETRE_ENTIER);
                        break;
                case T_S_('f'):
                case T_S_('F'):
                        type=T_S_(TYPEPARAMETRE_FLOTTANT);
                        break;
                case T_S_('e'):
                case T_S_('E'):
                case T_S_('g'):
                case T_S_('G'):
                        type=T_S_(TYPEPARAMETRE_DOUBLE);
                        break;
                case T_S_('c'):
                        type=T_S_(TYPEPARAMETRE_CARACTERE);
                        break;
                case T_S_('s'):
                        type=T_S_(TYPEPARAMETRE_CHAINE);
                        break;
                default:
                        type=T_S_(TYPEPARAMETRE_POINTEUR);
                        break;
        }

        SECURISE(texte_initialisation(T_S(texte)));
        SECURISE(texte_definition_contenu(T_S(texte), T_S_(format)));
        SECURISE(texte_definition_type(T_S(texte), T_S_(TYPETEXTE_PARAMETRE)));

        SECURISE(parametre_initialisation(T_S(parametre)));
        SECURISE(parametre_definition_indice(T_S(parametre), (TRAVAIL_SCALAIRE(Entier))(T_S_($1))));
        SECURISE(parametre_definition_type(T_S(parametre), type));
        SECURISE(parametre_definition_format(T_S(parametre), T_S(texte)));
        $$=parametre;
};

default_mesg: TOKEN_DEFAUT_MESG message TOKEN_FIN
{
        $$=$2;
};

default_charset: TOKEN_DEFAUT_CHARSET TOKEN_PARAM_NOM
{
        $$=$2;
};

default_lang: TOKEN_DEFAUT_LANG TOKEN_PARAM_NOM
{
        $$=$2;
};

%%

void yyerror(TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) erreur)
{
        printf("Syntax error near symbol '%s'\n", erreur);
}

Resultat lire_entree(TRAVAIL_SCALAIRE(Chaine) nomfichier, TRAVAIL(DonneesMessages) donnees)
{
        STOCKAGE_SCALAIRE(DescripteurFichier) entree;
        if((entree=FOPEN(nomfichier, T_S_("r")))==NULL)
        {
                fprintf(SORTIEERREUR, "Unable to open file '%s' for reading.\n", nomfichier);
                return RESULTAT_ERREUR;
        }
        else
        {
                SECURISE(localisationfichier_initialisation(T_S(localisation)));
                SECURISE(localisationfichier_ajout(T_S(localisation), nomfichier, T_S_(NULL), entree, T_S_(0)));
                T_S_(yyin)=entree;
                if(yyparse(donnees)!=0)
                {
                        fprintf(SORTIEERREUR, "An error occured while reading file '%s'.\n", nomfichier);
                        SECURISE(localisationfichier_destruction(T_S(localisation)));
                        SECURISE(donneesmessages_destruction(donnees));
                        return RESULTAT_ERREUR;
                }
                SECURISE(localisationfichier_destruction(T_S(localisation)));
                /* Note: yywrap s'occupe de fermer le descripteur de fichier */
        }
        return RESULTAT_OK;
}

int main(int argc, char *argv[])
{
        STOCKAGE(DonneesMessages) donnees;
        TRAVAIL_SCALAIRE(Chaine) repertoiresortie;
        TRAVAIL_SCALAIRE(NomFichier) cheminabsolu;
        TRAVAIL_SCALAIRE(Booleen) cheminvalide;
        switch(argc)
        {
                case 2:         repertoiresortie=T_S_(NULL);                                       break;
                case 3:         repertoiresortie=(TRAVAIL_SCALAIRE(Chaine))(T_S_(argv[2]));        break;
                default:        fprintf(SORTIEERREUR, "Error: misuse.\nUsage : ./compilateur <file> [<output_dir>]\n");
                                return 1;
        }

        yydebug=0;
        SECURISE(donneesmessages_initialisation(T_S(donnees)));

        SECURISE(aideenvironnement_ajout_repertoirefichier(T_S(pilerepertoire), (TRAVAIL_SCALAIRE(NomFichier))(T_S_(argv[1])), R_T_(cheminabsolu), R_T_(cheminvalide)));
        if(cheminvalide==T_S_(FAUX))
        {
                fprintf(SORTIEERREUR, "Error: '%s' is not a valid filename.", argv[1]);
                return 1;
        }

        SECURISE(lire_entree(cheminabsolu, T_S(donnees)));

        SECURISE(generateur_genererfichier_entete(T_S(donnees), repertoiresortie));
        SECURISE(generateur_genererfichier_source(T_S(donnees), repertoiresortie));

        SECURISE(donneesmessages_destruction(T_S(donnees)));
        return 0;
}
