/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2013  Sylvain Plantefève
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#include "donneesmessages.h"


Resultat donneesmessages_initialisation(TRAVAIL(DonneesMessages) donnees)
{
        ASSERTION((S_T(donnees)=NOUVEAU(DonneesMessages))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(donnees, nomfichier)=NULL;
        CHAMP(donnees, nomfonctionmessage)=NULL;
        CHAMP(donnees, nomfonctionlocalisation)=NULL;
        CHAMP(donnees, nomfonctionparametres)=NULL;
        CHAMP(donnees, langues)=NULL;
        CHAMP(donnees, languedefaut)=NULL;
        SECURISE(listecharset_initialisation(CHAMP_TRAVAIL(donnees, detectioncharsets)));
        SECURISE(listemessage_initialisation(CHAMP_TRAVAIL(donnees, detectionmessages)));
        return RESULTAT_OK;
}

#define DEFINITION_CHAINE(champ) \
        Resultat donneesmessages_definition_##champ (TRAVAIL(DonneesMessages) donnees, TRAVAIL_SCALAIRE(Chaine) champ) \
        {                                                                                                              \
                ASSERTION(S_T(donnees)!=NULL, RESULTAT_ERREUR);                                                        \
                if(CHAMP(donnees, champ)!=NULL)                                                                        \
                        free(CHAMP(donnees, champ));                                                                   \
                if(S_T_(champ)!=NULL)                                                                                  \
                {                                                                                                      \
                        CHAMP(donnees, champ)=DUP_CAST(Chaine, champ);                                                 \
                        ASSERTION(CHAMP(donnees, champ)!=NULL, RESULTAT_ERREUR_MEMOIRE);                               \
                }                                                                                                      \
                else                                                                                                   \
                        CHAMP(donnees, champ)=NULL;                                                                    \
                return RESULTAT_OK;                                                                                    \
        }

#define DEFINITION_CHAMP(type, champ) \
        Resultat donneesmessages_definition_##champ (TRAVAIL(DonneesMessages) donnees, TRAVAIL(type) champ) \
        {                                                                                                   \
                ASSERTION(S_T(donnees)!=NULL, RESULTAT_ERREUR);                                             \
                CHAMP(donnees, champ)=S_T(champ);                                                           \
                return RESULTAT_OK;                                                                         \
        }

DEFINITION_CHAINE(nomfichier)
DEFINITION_CHAINE(nomfonctionmessage)
DEFINITION_CHAINE(nomfonctionlocalisation)
DEFINITION_CHAINE(nomfonctionparametres)
DEFINITION_CHAMP(ListeLangue, langues)
DEFINITION_CHAMP(Langue, languedefaut)

Resultat donneesmessages_ajout_detectioncharset(TRAVAIL(DonneesMessages) donnees, TRAVAIL(Charset) charset)
{
        ASSERTION(S_T(donnees)!=NULL, RESULTAT_ERREUR);

        SECURISE(listecharset_ajout_detectioncharset(CHAMP_TRAVAIL(donnees, detectioncharsets), charset));
        return RESULTAT_OK;
}

Resultat donneesmessages_ajout_detectionmessage(TRAVAIL(DonneesMessages) donnees, TRAVAIL(Message) message)
{
        ASSERTION(S_T(donnees)!=NULL, RESULTAT_ERREUR);

        SECURISE(listemessage_ajout_detectionmessage(CHAMP_TRAVAIL(donnees, detectionmessages), message));
        return RESULTAT_OK;
}

#define _LECTURE_CHAMP(type, champ, REF, MOD_REFCHAMP, MOD_CHAMP) \
        Resultat donneesmessages_lecture_##champ (TRAVAIL(DonneesMessages) donnees, REF(type) champ) \
        {                                                                                            \
                ASSERTION(S_T(donnees)!=NULL, RESULTAT_ERREUR);                                      \
                MOD_REFCHAMP(champ)=MOD_CHAMP(CHAMP(donnees, champ));                                \
                return RESULTAT_OK;                                                                  \
        }
#define LECTURE_CHAMP(type, champ) \
        _LECTURE_CHAMP(type, champ, REFERENCE, T_R, T_S)
#define LECTURE_CHAMP_SCALAIRE(type, champ) \
        _LECTURE_CHAMP(type, champ, REFERENCE_SCALAIRE, T_R_, T_S_)

LECTURE_CHAMP_SCALAIRE(Chaine, nomfichier)
LECTURE_CHAMP_SCALAIRE(Chaine, nomfonctionmessage)
LECTURE_CHAMP_SCALAIRE(Chaine, nomfonctionlocalisation)
LECTURE_CHAMP_SCALAIRE(Chaine, nomfonctionparametres)
LECTURE_CHAMP(ListeLangue, langues)
LECTURE_CHAMP(Langue, languedefaut)
LECTURE_CHAMP(ListeCharset, detectioncharsets)
LECTURE_CHAMP(ListeMessage, detectionmessages)

Resultat donneesmessages_destruction(TRAVAIL(DonneesMessages) donnees)
{
        if(S_T(donnees)==NULL)
                return RESULTAT_OK;
        if(CHAMP(donnees, nomfichier)!=NULL)
                free(CHAMP(donnees, nomfichier));
        if(CHAMP(donnees, nomfonctionmessage)!=NULL)
                free(CHAMP(donnees, nomfonctionmessage));
        if(CHAMP(donnees, nomfonctionlocalisation)!=NULL)
                free(CHAMP(donnees, nomfonctionlocalisation));
        if(CHAMP(donnees, nomfonctionparametres)!=NULL)
                free(CHAMP(donnees, nomfonctionparametres));
        SECURISE(listelangue_destruction(CHAMP_TRAVAIL(donnees, langues)));
        free(S_T(donnees));
        S_T(donnees)=NULL;
        return RESULTAT_OK;
}
