/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __LISTEMESSAGE__
#define __LISTEMESSAGE__

#include <src/global/global.h>
#include <src/messages/messages/compilateur/langue.h>


typedef struct listetexte CONTENEUR(ListeTexte);
typedef struct listeparametre CONTENEUR(ListeParametre);
typedef struct listemessage CONTENEUR(ListeMessage);
typedef struct listecharset CONTENEUR(ListeCharset);
typedef struct listelangue CONTENEUR(ListeLangue);
//Pour ces trois dernieres structures, si suivant == NULL, cela signifie que
//le maillon en question correspond a la valeur par defaut.

struct listetexte
{
        STOCKAGE(Texte) texte;
        STOCKAGE(ListeTexte) suivant;
};

struct listeparametre
{
        STOCKAGE(Parametre) parametre;
        STOCKAGE(ListeParametre) suivant;
};

struct listemessage
{
        STOCKAGE(Message) message;
        STOCKAGE(ListeMessage) suivant;
};

struct listecharset
{
        STOCKAGE(Charset) charset;
        STOCKAGE(ListeCharset) suivant;
};

struct listelangue
{
        STOCKAGE(Langue) langue;
        STOCKAGE(ListeLangue) suivant;
};

Resultat listetexte_initialisation(TRAVAIL(ListeTexte) listetexte);
Resultat listetexte_ajout_texte(TRAVAIL(ListeTexte) listetexte, TRAVAIL(Texte) texte);
Resultat listetexte_destruction(TRAVAIL(ListeTexte) listetexte);


Resultat listeparametre_initialisation(TRAVAIL(ListeParametre) listeparametre);
Resultat listeparametre_ajout_parametre(TRAVAIL(ListeParametre) listeparametre, TRAVAIL(Parametre) parametre);
Resultat listeparametre_destruction(TRAVAIL(ListeParametre) listeparametre);


Resultat listemessage_initialisation(TRAVAIL(ListeMessage) listemessage);
Resultat listemessage_ajout_message(TRAVAIL(ListeMessage) listemessage, TRAVAIL(Message) message);
Resultat listemessage_ajout_detectionmessage(TRAVAIL(ListeMessage) listemessage, TRAVAIL(Message) message);
Resultat listemessage_destruction(TRAVAIL(ListeMessage) listemessage);


Resultat listecharset_initialisation(TRAVAIL(ListeCharset) listecharset);
Resultat listecharset_ajout_charset(TRAVAIL(ListeCharset) listecharset, TRAVAIL(Charset) charset);
Resultat listecharset_ajout_detectioncharset(TRAVAIL(ListeCharset) listecharset, TRAVAIL(Charset) charset);
Resultat listecharset_recherche_charset(TRAVAIL(ListeCharset) listecharset, TRAVAIL_SCALAIRE(Chaine) id, REFERENCE(Charset) charset);
Resultat listecharset_destruction(TRAVAIL(ListeCharset) listecharset);


Resultat listelangue_initialisation(TRAVAIL(ListeLangue) listelangue);
Resultat listelangue_ajout_langue(TRAVAIL(ListeLangue) listelangue, TRAVAIL(Langue) langue);
Resultat listelangue_ajout_detectionlangue(TRAVAIL(ListeLangue) listelangue, TRAVAIL(Langue) langue);
Resultat listelangue_recherche_langue(TRAVAIL(ListeLangue) listelangue, TRAVAIL_SCALAIRE(Chaine) id, REFERENCE(Langue) langue);
Resultat listelangue_destruction(TRAVAIL(ListeLangue) listelangue);


#define TEXTE(pointeur)         CHAMP_TRAVAIL(pointeur, texte)
#define PARAM(pointeur)         CHAMP_TRAVAIL(pointeur, parametre)
#define MESSAGE(pointeur)       CHAMP_TRAVAIL(pointeur, message)
#define CHARSET(pointeur)       CHAMP_TRAVAIL(pointeur, charset)
#define LANGUE(pointeur)        CHAMP_TRAVAIL(pointeur, langue)
#define SUIVANT(pointeur)       CHAMP_TRAVAIL(pointeur, suivant)
#define AU_SUIVANT(pointeur)    pointeur=SUIVANT(pointeur)

#endif
