/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "listelangues.h"

Resultat listetexte_initialisation(TRAVAIL(ListeTexte) listetexte)
{ 
        S_T(listetexte)=NULL;
        return RESULTAT_OK;
}

Resultat listetexte_ajout_texte(TRAVAIL(ListeTexte) listetexte, TRAVAIL(Texte) texte)
{ 
        if(S_T(listetexte)==NULL)
        {
                ASSERTION((S_T(listetexte)=NOUVEAU(ListeTexte))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                CHAMP(listetexte, texte)=S_T(texte);
                CHAMP(listetexte, suivant)=NULL;
        }
        else
        {
                SECURISE(listetexte_ajout_texte(SUIVANT(listetexte), texte));
        }
        return RESULTAT_OK;
}

Resultat listetexte_destruction(TRAVAIL(ListeTexte) listetexte)
{ 
        if(S_T(listetexte)==NULL)
                return RESULTAT_OK;
        SECURISE(texte_destruction(TEXTE(listetexte)));
        SECURISE(listetexte_destruction(SUIVANT(listetexte)));
        free(S_T(listetexte));
        S_T(listetexte)=NULL;
        return RESULTAT_OK;
}

Resultat listeparametre_initialisation(TRAVAIL(ListeParametre) listeparametre)
{ 
        S_T(listeparametre)=NULL;
        return RESULTAT_OK;
}

Resultat listeparametre_ajout_parametre(TRAVAIL(ListeParametre) listeparametre, TRAVAIL(Parametre) parametre)
{ 
        if(S_T(listeparametre)==NULL)
        {
                ASSERTION((S_T(listeparametre)=NOUVEAU(ListeParametre))!=NULL, RESULTAT_ERREUR_MEMOIRE);

                CHAMP(listeparametre, parametre)=S_T(parametre);
                CHAMP(listeparametre, suivant)=NULL;
        }
        else
        {
                SECURISE(listeparametre_ajout_parametre(SUIVANT(listeparametre), parametre));
        }
        return RESULTAT_OK;
}

Resultat listeparametre_destruction(TRAVAIL(ListeParametre) listeparametre)
{ 
        if(S_T(listeparametre)==NULL)
                return RESULTAT_OK;
        SECURISE(parametre_destruction(PARAM(listeparametre)));
        SECURISE(listeparametre_destruction(SUIVANT(listeparametre)));
        free(S_T(listeparametre));
        S_T(listeparametre)=NULL;
        return RESULTAT_OK;
}

Resultat listemessage_initialisation(TRAVAIL(ListeMessage) listemessage)
{ 
        S_T(listemessage)=NULL;
        return RESULTAT_OK;
}

Resultat listemessage_ajout_message(TRAVAIL(ListeMessage) listemessage, TRAVAIL(Message) message)
{ 
        if(S_T(listemessage)==NULL)
        {
                ASSERTION((S_T(listemessage)=NOUVEAU(ListeMessage))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                CHAMP(listemessage, message)=S_T(message);
                CHAMP(listemessage, suivant)=NULL;
        }
        else
        {
                SECURISE(listemessage_ajout_message(SUIVANT(listemessage), message));
        }
        return RESULTAT_OK;
}

Resultat listemessage_ajout_detectionmessage(TRAVAIL(ListeMessage) listemessage, TRAVAIL(Message) message)
{ 
        TRAVAIL_SCALAIRE(Chaine) identifiantmessage;
        TRAVAIL(ListeMessage) pointeurmessage;
        TRAVAIL_SCALAIRE(Booleen) ajout=T_S_(VRAI);
        SECURISE(message_lecture_identifiant(message, R_T_(identifiantmessage)));
        for(pointeurmessage=listemessage ; S_T(pointeurmessage)!=NULL ; AU_SUIVANT(pointeurmessage))
        {
                TRAVAIL_SCALAIRE(Entier) comparaison;
                TRAVAIL_SCALAIRE(Chaine) identifiantcourant;
                SECURISE(message_lecture_identifiant(MESSAGE(pointeurmessage), R_T_(identifiantcourant)));
                comparaison=STRCMP(identifiantcourant, identifiantmessage);
                if(comparaison>T_S_(0))
                        break;
                else if(comparaison<T_S_(0))
                        continue;
                else // comparaison==T_S_(0)
                {
                        TRAVAIL_SCALAIRE(Chaine) detectioncourante;
                        ajout=T_S_(FAUX);
                        SECURISE(message_lecture_detection(MESSAGE(pointeurmessage), R_T_(detectioncourante)));
                        if(detectioncourante==T_S_(NULL))
                        {
                                TRAVAIL_SCALAIRE(Chaine) detectionmessage;
                                SECURISE(message_lecture_detection(message, R_T_(detectionmessage)));
                                if(detectionmessage!=T_S_(NULL))
                                        CHAMP(pointeurmessage, message)=S_T(message);
                        }
                }
        }
        if(ajout==T_S_(VRAI))
        {
                STOCKAGE(ListeMessage) nouveau;
                ASSERTION((nouveau=NOUVEAU(ListeMessage))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                CHAMP_STOCKAGE(nouveau, message)=S_T(message);
                CHAMP_STOCKAGE(nouveau, suivant)=S_T(pointeurmessage);
                S_T(pointeurmessage)=nouveau;
        }
        return RESULTAT_OK;
}

Resultat listemessage_destruction(TRAVAIL(ListeMessage) listemessage)
{ 
        if(S_T(listemessage)==NULL)
                return RESULTAT_OK;
        SECURISE(message_destruction(MESSAGE(listemessage)));
        SECURISE(listemessage_destruction(SUIVANT(listemessage)));
        free(S_T(listemessage));
        S_T(listemessage)=NULL;
        return RESULTAT_OK;
}

Resultat listecharset_initialisation(TRAVAIL(ListeCharset) listecharset)
{ 
        S_T(listecharset)=NULL;
        return RESULTAT_OK;
}

Resultat listecharset_ajout_charset(TRAVAIL(ListeCharset) listecharset, TRAVAIL(Charset) charset)
{ 
        if(S_T(listecharset)==NULL)
        {
                ASSERTION((S_T(listecharset)=NOUVEAU(ListeCharset))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                CHAMP(listecharset, charset)=S_T(charset);
                CHAMP(listecharset, suivant)=NULL;
        }
        else
        {
                SECURISE(listecharset_ajout_charset(SUIVANT(listecharset), charset));
        }
        return RESULTAT_OK;
}

Resultat listecharset_ajout_detectioncharset(TRAVAIL(ListeCharset) listecharset, TRAVAIL(Charset) charset)
{
        TRAVAIL_SCALAIRE(Chaine) identifiantcharset;
        TRAVAIL(ListeCharset) pointeurcharset;
        TRAVAIL_SCALAIRE(Booleen) ajout=T_S_(VRAI);
        SECURISE(charset_lecture_identifiant(charset, R_T_(identifiantcharset)));
        for(pointeurcharset=listecharset ; S_T(pointeurcharset)!=NULL ; AU_SUIVANT(pointeurcharset))
        {
                TRAVAIL_SCALAIRE(Chaine) identifiantcourant;
                TRAVAIL_SCALAIRE(Entier) comparaison;
                SECURISE(charset_lecture_identifiant(CHARSET(pointeurcharset), R_T_(identifiantcourant)));
                comparaison=STRCMP(identifiantcourant, identifiantcharset);
                if(comparaison>T_S_(0))
                        break;
                else if(comparaison<T_S_(0))
                        continue;
                else // comparaison==T_S_(0)
                {
                        TRAVAIL_SCALAIRE(Chaine) detectioncourante;
                        ajout=T_S_(FAUX);
                        SECURISE(charset_lecture_detection(CHARSET(pointeurcharset), R_T_(detectioncourante)));
                        if(detectioncourante==T_S_(NULL))
                        {
                                TRAVAIL_SCALAIRE(Chaine) detectioncharset;
                                SECURISE(charset_lecture_detection(charset, R_T_(detectioncharset)));
                                if(detectioncharset!=T_S_(NULL))
                                        CHAMP(pointeurcharset, charset)=S_T(charset);
                        }
                }
        }
        if(ajout==T_S_(VRAI))
        {
                STOCKAGE(ListeCharset) nouveau;
                ASSERTION((nouveau=NOUVEAU(ListeCharset))!=NULL, RESULTAT_ERREUR_MEMOIRE);

                CHAMP_STOCKAGE(nouveau, charset)=S_T(charset);
                CHAMP_STOCKAGE(nouveau, suivant)=S_T(pointeurcharset);
                S_T(pointeurcharset)=nouveau;
        }
        return RESULTAT_OK;
}

Resultat listecharset_recherche_charset(TRAVAIL(ListeCharset) listecharset, TRAVAIL_SCALAIRE(Chaine) identifiant, REFERENCE(Charset) charset)
{
        TRAVAIL_SCALAIRE(Chaine) identifiantcourant;
        ASSERTION(S_T(listecharset)!=NULL, RESULTAT_ERREUR_NONTROUVE);

        SECURISE(charset_lecture_identifiant(CHARSET(listecharset), R_T_(identifiantcourant)));
        if(STRCMP(identifiantcourant, identifiant)==T_S_(0))
                T_R(charset)=CHARSET(listecharset);
        else
                return listecharset_recherche_charset(SUIVANT(listecharset), identifiant, charset);
        return RESULTAT_OK;
}

Resultat listecharset_destruction(TRAVAIL(ListeCharset) listecharset)
{ 
        if(S_T(listecharset)==NULL)
                return RESULTAT_OK;
        SECURISE(charset_destruction(CHARSET(listecharset)));
        SECURISE(listecharset_destruction(SUIVANT(listecharset)));
        free(S_T(listecharset));
        S_T(listecharset)=NULL;
        return RESULTAT_OK;
}

Resultat listelangue_initialisation(TRAVAIL(ListeLangue) listelangue)
{ 
        S_T(listelangue)=NULL;
        return RESULTAT_OK;
}

Resultat listelangue_ajout_langue(TRAVAIL(ListeLangue) listelangue, TRAVAIL(Langue) langue)
{ 
        if(S_T(listelangue)==NULL)
        {
                ASSERTION((S_T(listelangue)=NOUVEAU(ListeLangue))!=NULL, RESULTAT_ERREUR_MEMOIRE);
                CHAMP(listelangue, langue)=S_T(langue);
                CHAMP(listelangue, suivant)=NULL;
        }
        else
        {
                SECURISE(listelangue_ajout_langue(SUIVANT(listelangue), langue));
        }
        return RESULTAT_OK;
}

Resultat listelangue_ajout_detectionlangue(TRAVAIL(ListeLangue) listelangue, TRAVAIL(Langue) langue)
{ 
        TRAVAIL_SCALAIRE(Chaine) identifiantlangue;
        TRAVAIL(ListeLangue) pointeurlangue;
        TRAVAIL_SCALAIRE(Booleen) ajout=T_S_(VRAI);
        SECURISE(langue_lecture_identifiant(langue, R_T_(identifiantlangue)));
        for(pointeurlangue=listelangue ; S_T(pointeurlangue)!=NULL ; AU_SUIVANT(pointeurlangue))
        {
                TRAVAIL_SCALAIRE(Chaine) identifiantcourant;
                TRAVAIL_SCALAIRE(Entier) comparaison;
                SECURISE(langue_lecture_identifiant(LANGUE(pointeurlangue), R_T_(identifiantcourant)));
                comparaison=STRCMP(identifiantcourant, identifiantlangue);
                if(comparaison>T_S_(0))
                        break;
                else if(comparaison<T_S_(0))
                        continue;
                else // comparaison==T_S_(0)
                {
                        TRAVAIL_SCALAIRE(Chaine) detectioncourante;
                        ajout=T_S_(FAUX);
                        SECURISE(langue_lecture_detection(LANGUE(pointeurlangue), R_T_(detectioncourante)));
                        if(detectioncourante==T_S_(NULL))
                        {
                                TRAVAIL_SCALAIRE(Chaine) detectionlangue;
                                SECURISE(langue_lecture_detection(langue, R_T_(detectionlangue)));
                                if(detectionlangue!=T_S_(NULL))
                                        CHAMP(pointeurlangue, langue)=S_T(langue);
                        }
                }
        }
        if(ajout==T_S_(VRAI))
        {
                STOCKAGE(ListeLangue) nouveau;
                ASSERTION((nouveau=NOUVEAU(ListeLangue))!=NULL, RESULTAT_ERREUR_MEMOIRE);

                CHAMP_STOCKAGE(nouveau, langue)=S_T(langue);
                CHAMP_STOCKAGE(nouveau, suivant)=S_T(pointeurlangue);
                S_T(pointeurlangue)=nouveau;
        }
        return RESULTAT_OK;
}

Resultat listelangue_recherche_langue(TRAVAIL(ListeLangue) listelangue, TRAVAIL_SCALAIRE(Chaine) identifiant, REFERENCE(Langue) langue)
{
        TRAVAIL_SCALAIRE(Chaine) identifiantcourant;
        ASSERTION(S_T(listelangue)!=NULL, RESULTAT_ERREUR_NONTROUVE);

        SECURISE(langue_lecture_identifiant(LANGUE(listelangue), R_T_(identifiantcourant)));
        if(STRCMP(identifiantcourant, identifiant)==T_S_(0))
                T_R(langue)=LANGUE(listelangue);
        else
                return listelangue_recherche_langue(SUIVANT(listelangue), identifiant, langue);
        return RESULTAT_OK;
}

Resultat listelangue_destruction(TRAVAIL(ListeLangue) listelangue)
{ 
        if(S_T(listelangue)==NULL)
                return RESULTAT_OK;
        SECURISE(langue_destruction(LANGUE(listelangue)));
        SECURISE(listelangue_destruction(SUIVANT(listelangue)));
        free(S_T(listelangue));
        S_T(listelangue)=NULL;
        return RESULTAT_OK;
}
