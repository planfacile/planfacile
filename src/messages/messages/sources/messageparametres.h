/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __MESSAGEPARAMETRES__
#define __MESSAGEPARAMETRES__

#include <src/global/global.h>

typedef struct messageparametres CONTENEUR(MessageParametres);

struct messageparametres
{
        STOCKAGE_SCALAIRE(Taille) memoire;
        //Taille allou�e en m�moire pour le tableau.
        STOCKAGE_SCALAIRE(Taille) taille;
        //Taille r�ellement utilis�e.
        TABLEAU_(STOCKAGE_SCALAIRE(Pointeur)) parametre;
        //Param�tres du message.
};
/* Cette structure simple sert simplement � stocker des param�tres servant
 * � construire des messages d'erreur. Les �l�ments du tableau seront lus
 * directement dans le tableau, ce qui pourra provoquer une erreur si
 * le tableau est mal renseign�.
 */

Resultat messageparametres_initialisation(TRAVAIL(MessageParametres) messageparametres);
/* Cr�e une structure de param�tres de message.
 * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
 */

Resultat messageparametres_ajout_entier(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Entier) entier);
Resultat messageparametres_ajout_reel(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Reel) reel);
Resultat messageparametres_ajout_caractere(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Caractere) caractere);
Resultat messageparametres_ajout_chaine(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Chaine) chaine);
Resultat messageparametres_ajout_pointeur(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Pointeur) pointeur);

/* Ces fonctions permettent d'assigner une valeur � un param�tre de message.
 * Une copie est toujours r�alis�e, sauf dans le cas du pointeur,
 * et RESULTAT_ERREUR_MEMOIRE est renvoy� dans
 * le cas o� une allocation m�moire �choue.
 * RESULTAT_ERREUR est renvoy� si messageparametres est NULL.
 */

Resultat messageparametres_copie(TRAVAIL(MessageParametres) messageparametres, TRAVAIL(MessageParametres) copie);
/* Cr�e une copie de la structure de param�tres de message.
 * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
 * Renvoie RESULTAT_ERREUR si messageparametres est NULL.
 * Attention ! Si *copie est diff�rent de NULL, la copie tente une
 * destruction pr�alable de la valeur pr�sum�e dans la copie.
 */

Resultat messageparametres_destruction(TRAVAIL(MessageParametres) messageparametres);
/* D�truit une structure de param�tres de message.
 */

#endif
