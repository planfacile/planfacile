/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "messageparametres.h"

#define TAILLEINIT        5
#define MULTTAILLE        2


static Resultat messageparametres_ajout(TRAVAIL(MessageParametres) messageparametres)
{
        /* Ajoute des positions m�moire, de sorte qu'une place libre se trouve en
         * fin de tableau.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         */
        TABLEAU_(STOCKAGE_SCALAIRE(Pointeur)) nouveau;
        if(T_S_(CHAMP(messageparametres, taille))>=T_S_(CHAMP(messageparametres, memoire)))
        {
                nouveau=REALLOCATION_CAST_(CHAMP(messageparametres, parametre), Pointeur, T_S_(CHAMP(messageparametres, memoire))*T_S_(MULTTAILLE));
                ASSERTION(nouveau!=NULL, RESULTAT_ERREUR_MEMOIRE);

                CHAMP(messageparametres, memoire)*=T_S_(MULTTAILLE);
                CHAMP(messageparametres, parametre)=nouveau;
        }
        return RESULTAT_OK;
}

Resultat messageparametres_initialisation(TRAVAIL(MessageParametres) messageparametres)
{
        /* Cr�e une structure de param�tres de message.
         * Renvoie RESULTAT_ERREUR_MEMOIRE si une allocation �choue.
         */
        S_T(messageparametres)=NOUVEAU(MessageParametres);
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(messageparametres, parametre)=NOUVEAUX_(Pointeur, T_S_(TAILLEINIT));
        ASSERTION(CHAMP(messageparametres, parametre)!=NULL, RESULTAT_ERREUR_MEMOIRE);

        CHAMP(messageparametres, memoire)=TAILLEINIT;
        CHAMP(messageparametres, taille)=0;
        return RESULTAT_OK;
}

Resultat messageparametres_ajout_entier(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Entier) entier)
{
        COREFERENCE_SCALAIRE(Entier) pointeur;
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_ajout(messageparametres));
        ASSERTION((pointeur=NOUVEAU_(Entier))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        S_C_(pointeur)=entier;
        ELEMENT(CHAMP(messageparametres, parametre), T_S_(CHAMP(messageparametres, taille)))=(STOCKAGE_SCALAIRE(Pointeur))(pointeur);
        T_S_(CHAMP(messageparametres, taille))++;
        return RESULTAT_OK;
}

Resultat messageparametres_ajout_reel(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Reel) reel)
{
        COREFERENCE_SCALAIRE(Reel) pointeur;
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_ajout(messageparametres));
        ASSERTION((pointeur=NOUVEAU_(Reel))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        S_C_(pointeur)=reel;
        ELEMENT(CHAMP(messageparametres, parametre), T_S_(CHAMP(messageparametres, taille)))=(STOCKAGE_SCALAIRE(Pointeur))(pointeur);
        T_S_(CHAMP(messageparametres, taille))++;
        return RESULTAT_OK;
}

Resultat messageparametres_ajout_caractere(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Caractere) caractere)
{
        COREFERENCE_SCALAIRE(Caractere) pointeur;
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_ajout(messageparametres));
        ASSERTION((pointeur=NOUVEAU_(Caractere))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        S_C_(pointeur)=caractere;
        ELEMENT(CHAMP(messageparametres, parametre), T_S_(CHAMP(messageparametres, taille)))=(STOCKAGE_SCALAIRE(Pointeur))(pointeur);
        T_S_(CHAMP(messageparametres, taille))++;
        return RESULTAT_OK;
}

Resultat messageparametres_ajout_chaine(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Chaine) chaine)
{
        STOCKAGE_SCALAIRE(Chaine) pointeur;
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T_(chaine)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_ajout(messageparametres));
        ASSERTION((pointeur=DUP_CAST(Chaine, chaine))!=NULL, RESULTAT_ERREUR_MEMOIRE);

        ELEMENT(CHAMP(messageparametres, parametre), T_S_(CHAMP(messageparametres, taille)))=(STOCKAGE_SCALAIRE(Pointeur))(pointeur);
        T_S_(CHAMP(messageparametres, taille))++;
        return RESULTAT_OK;
}

Resultat messageparametres_ajout_pointeur(TRAVAIL(MessageParametres) messageparametres, TRAVAIL_SCALAIRE(Pointeur) pointeur)
{
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);
        ASSERTION(S_T_(pointeur)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_ajout(messageparametres));
        ELEMENT(CHAMP(messageparametres, parametre), T_S_(CHAMP(messageparametres, taille)))=(STOCKAGE_SCALAIRE(Pointeur))(pointeur);
        T_S_(CHAMP(messageparametres, taille))++;
        return RESULTAT_OK;
}


/* Ces fonctions permettent d'assigner une valeur � un param�tre de message.
 * Une copie est toujours r�alis�e, sauf dans le cas du pointeur,
 * et RESULTAT_ERREUR_MEMOIRE est renvoy� dans
 * le cas o� une allocation m�moire �choue.
 * RESULTAT_ERREUR est renvoy� si messageparametres est NULL.
 */

Resultat messageparametres_copie(TRAVAIL(MessageParametres) messageparametres, TRAVAIL(MessageParametres) copie)
{
        /* Cr�e une copie de la structure de param�tres de message.
         * Renvoie RESULTAT_ERREUR_MEMOIRE en cas d'�chec de l'allocation.
         * Renvoie RESULTAT_ERREUR si messageparametres est NULL.
         * Attention ! Si *copie est diff�rent de NULL, la copie tente une
         * destruction pr�alable de la valeur pr�sum�e dans la copie.
         * Attention ! les zones point�es (les param�tres eux-m�mes) ne sont
         * pas dupliqu�s.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        ASSERTION(S_T(messageparametres)!=NULL, RESULTAT_ERREUR);

        SECURISE(messageparametres_destruction(copie));
        SECURISE(messageparametres_initialisation(copie));
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(messageparametres, taille)) ; T_S_(indice)++)
        {
                SECURISE(messageparametres_ajout_pointeur(copie,T_S_(ELEMENT(CHAMP(messageparametres, parametre), T_S_(indice)))));
        }
        return RESULTAT_OK;
}

Resultat messageparametres_destruction(TRAVAIL(MessageParametres) messageparametres)
{
        /* D�truit une structure de param�tres de message.
         */
        STOCKAGE_SCALAIRE(Indice) indice;
        if(S_T(messageparametres)==NULL)
                return RESULTAT_OK;
        for(indice=0 ; T_S_(indice)<T_S_(CHAMP(messageparametres, taille)) ; T_S_(indice)++)
                free(ELEMENT(CHAMP(messageparametres, parametre), T_S_(indice)));
        free(CHAMP(messageparametres, parametre));
        free(S_T(messageparametres));
        S_T(messageparametres)=NULL;
        return RESULTAT_OK;
}

