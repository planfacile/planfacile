/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */

#include "aide.h"

Resultat aide_aide(TRAVAIL(Environnement) environnement)
{
        /* Donne sur la sortie d'erreur le texte de l'aide, dans la
         * langue et le jeu de caractères choisi.
         * Renvoie RESULTAT_ERREUR si environnement est NULL.
         */
        TRAVAIL_SCALAIRE(DescripteurFichier) erreur;
        STOCKAGE(MessageParametres) parametres;
        ASSERTION(S_T(environnement)!=NULL, RESULTAT_ERREUR);

        SECURISE(environnement_lecture_erreur(environnement, R_T_(erreur)));
        fprintf(erreur, "%s version %s\n", PROJECT_NAME, VERSION_FULL);
        SECURISE(messageparametres_initialisation(T_S(parametres)));
        SECURISE(messageparametres_ajout_chaine(T_S(parametres), (TRAVAIL_SCALAIRE(Chaine))(T_S_(SOFTWARE_NAME))));
        SECURISE(messages_message(environnement, erreur, T_S_(MESSAGE_AIDE), T_S(parametres)));
        SECURISE(messageparametres_destruction(T_S(parametres)));
        return RESULTAT_OK;
}

