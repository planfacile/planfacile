/*
 *  PlanFacile (Easy plan, in french) is a small tool to help people to
 *  write a document on a particular subject.
 *  Copyright (C) 2005  Julien BRUGUIER
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licences/>.
 */
#ifndef __AIDE__
#define __AIDE__

#include <src/global/global.h>
#include <src/donnees/environnement/environnement.h>


Resultat aide_aide(TRAVAIL(Environnement) environnement);
/* Donne sur la sortie d'erreur le texte de l'aide, dans la
 * langue et le jeu de caractères choisi.
 * Renvoie RESULTAT_ERREUR si environnement est NULL.
 */

#endif
