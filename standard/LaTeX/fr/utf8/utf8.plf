#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#head{
\documentclass[a4paper#
#options{Taille police}
#case{10pt},10pt#
#case{11pt},11pt#
#case{12pt},12pt#
#other
	#warning{Taille de police réglée à 11 points par défaut}
	#option{11pt}
#end
]#{
#options{Classe de document}
#case{article}
	article#
#case{rapport}
	report#
#case{livre}
	book#
#other
	#error{Classe de document non sélectionnée}
#end
#}#
\usepackage[latin1]#{inputenc#}#
\usepackage[french]#{babel#}#
\usepackage#{indentfirst#}#
#options{Inclusion images}
#case{inclusion image}
	\usepackage#{graphicx#}#
#end
#options{Packages personnels}
#case{packages personnels}
	#packages
#end
#options{Titre}
#case{titre}
	\title#{#titre#}#
#end
#options{Auteur}
#case{auteur}
	\author#{#auteur#}#
#end
#options{Date de compilation}
#case{date}
	\date#{#date#}#
#end
\begin#{document#}#
#options{Page de garde}
#case{page de garde}
	\maketitle
#end
#options{Table des matières}
#case{table des matières}
	\tableofcontents
#end#
}
#section{0}{document}
{}
{}
{}
{}
#reference{document}{}
#options{Classe de document}
#case{article}
	#define{classedocument}{article}
	#start{chapitre}
	#section{1}{partie}
	{}
	{}
	{}
	{}
	#reference{partie}{}
	#section{2}{chapitre}
	{}
	{}
	{}
	{}
	#reference{chapitre}{}
	#reference{sous-section}{
		\footnote#{Voir #sec{section}# (\ref#{sec:#ref#}), #sec# « #title# ».#}
	}
	#reference{
		\footnote#{Voir #sec{section}# (\ref#{sec:#ref#}), #sec# « #title# ».#}
	}
#case{rapport}
	#define{classedocument}{report}
	#start{partie}
	#section{1}{partie}
	{}
	{}
	{}
	{}
	#reference{partie}{}
	#section{2}{chapitre}{
	\chapter#{#title#}#
	\label#{sec:#ref#}#
	#txt#
	#
	}
	{% Fin #title#
	}
	{}
	{}
	#reference{chapitre}{
		\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
	}
	#reference{sous-section}{
		\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
	}
	#reference{
		\footnote#{Voir #sec{sous-section}# (\ref#{sec:#ref#}), #sec# « #title# ».#}
	}
#case{livre}
	#define{classedocument}{book}
	#start{document}
	#section{1}{partie}{
		\part#{#title#}#
		\label#{sec:#ref#}#
		#
	}
	{% Fin #title#
	}
	{}
	{}
	#reference{partie}{
		\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
	}
	#section{2}{chapitre}{
	\chapter#{#title#}#
	\label#{sec:#ref#}#
	#txt#
	#
	}
	{% Fin #title#
	}
	{}
	{}
	#reference{chapitre}{
		\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
	}
	#reference{sous-section}{
		\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
	}
	#reference{
		\footnote#{Voir #sec{sous-section}# (\ref#{sec:#ref#}), #sec# « #title# ».#}
	}
#other
	#warning{Classe de document non sélectionnée}
	#start{document}
#end
#section{3}{section}{
\section#{#title#}#
\label#{sec:#ref#}#
#txt#
#
}
{% Fin #title#
}
{}
{}
#reference{section}{
	\footnote#{Voir #sec# (\ref#{sec:#ref#}).#}
}
#section{4}{sous-section}{
\subsection#{#title#}#
\label#{sec:#ref#}#
#txt#
#
}
{% Fin #title#
}
{}
{}
#section{5}{sous-sous-section}{\subsubsection#{#title#}#
	\label#{sec:#ref#}#
	#txt#
	#
}
{% Fin #title#
}
{}
{}
#section{6}{paragraphe}{\paragraph#{#title#}#
	\label#{sec:#ref#}#
	#txt#
	#
}
{% Fin #title#
}
{}
{}
#section{7}{sous-paragraphe}{\subparagraph#{#title#}#
	\label#{sec:#ref#}#
	#txt#
	#
}
{% Fin #title#
}
{}
{}
#section{idée}{
\item [#title ] \label#{sec:#ref#} #txt#
}
{% Fin #title#
}
{\begin#{description#}#
}
{\end#{description#}#
}
#message{%#mesg#
}
#foot{
\end#{document#}#
}
#define{listeapuce}{
\begin#{itemize#}#
#1
\end#{itemize#}#
	#define{items}{
	\item #0#
	}
}
#define{enumeration}{
\begin#{enumerate#}#
#1
\end#{enumerate#}#
	#define{items}{
	\item #0#
	}
}
#define{description}{
\begin#{description#}#
#1
\end#{description#}#
	#define{items}{
	#0
	}
	#define{item}{
		\item[#1] #2#
	}
}
#define{verbatim}{\verb#1#2#1}
#define{verbatimlong}{\begin#{verbatim#}#
#1#
\end#{verbatim#}#
	#define{verbligne}{#
	}
}
#define{oe}{\oe }
#define{...}{\ldots}
#define{espace}{\ }
#define{apposition}{# --- #1# --- }
#define{cesure}{\-}
#define{§}{#
}
#define{dièse}{\##}
#define{accouvrante}{\#{}
#define{accfermante}{\#}}
#define{citation}{\begin#{quotation#}
#1#
\end#{quotation#}}
#options{Inclusion images}
#case{inclusion image}
	#define{image}{
		\begin#{figure#}[htbp]
		\begin#{center#}#
		\includegraphics[#5]#{#1.eps#}#
		\end#{center#}#
		\caption#{#3#}#
		\label#{fig:#6#}#
		\end#{figure#}#
	}
#end
#missing{idee_manquante_#index}Idée manquante #index#text
Ceci est une idée automatiquement ajoutée par #PlanFacile . Vous devriez utiliser la commande missing correspondant à cet index
pour remplacer ce texte par celui de votre document.
Cette idée contient des références vers : #extrefs{(#ref# ; #title )}.#
#define{PlanFacile}{#{\sc PlanFacile#}}
#end
#options{Idées génériques}
#case{sans générique}
	#warning{Veuillez compiler avec l'option de ligne de commande -A}
#other
	#generic{idee_generique_#index}Idée générique #index#text
	Ceci est une idée automatiquement ajoutée par #PlanFacile . Vous devriez utiliser la commande generic correspondant à cet index
	pour remplacer ce texte par celui de votre document.#
	#define{PlanFacile}{#{\sc PlanFacile#}}
	#end
#end

