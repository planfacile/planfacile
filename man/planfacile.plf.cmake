#comment{
 PlanFacile (Easy plan, in french) is a small tool to help people to
 write a document on a particular subject.
 Copyright (C) 2005  Julien BRUGUIER

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licences/>.
}
#start{0}
#message{.\"#mesg#
}
#section{0}{document}{.TH #title#
}{.\"End of document #ref#
}{}{}

#section{1}{chapter}{.SH #title#
#txt#
}{.\"End of chapter #ref#
}{}{}

#section{2}{section}{.SS #title#
#txt#
}{.\"End of section #ref#
}{}{}

#section{paragraph}{.BR #title#
#txt#
}{.\"End of paragraph #ref#
}{}{}

#reference{# (See #sec# #title )}

#define{specialbold}{#
.B #1#
}
#define{specialitalic}{#
.I #1#
}
#define{specialalternate}{#
.BI #1#
}
#define{§}{#
#
}
#define{bold}{\fB#1\fP}
#define{italic}{\fI#1\fP}

#define{PlanFacile}{#bold{@PROJECT_NAME@}}

#idea{manpage} @PROJECT_NAME@ 1 "@BUILD_DATE@" "Version @VERSION@" "Linux user manual"#text
#end

#idea{synopsis}SYNOPSIS#text#dep{1}{manpage}{}{}#dep{1}{name}{}{}
#specialbold{@SOFTWARE_NAME@}
[options]
[files]
#end

#idea{description}DESCRIPTION#text#dep{synopsis}{}{}
#PlanFacile# is a document compiler, designed for table of contents computation.
More precisely, #PlanFacile# considers that a document is a set of concepts
linked by dependencies. #PlanFacile# generates a document using those ideas,
sorted in order to read it from the begininng to the end with a minimum of cross
references to further parts of the document.
#§
#PlanFacile# generates only non-formated documents, such as HTML or LaTeX sources.
#end

#idea{options}OPTIONS#text#dep{description}{}{}
#PlanFacile# uses 36 options to modify its behavior. Listing all of these here is
not interesting, as the #italic{-h}# option gives some help about all options.
#§
Some options are often useful :
#specialalternate{-o "" " " "output file"} specify the output file, to write the
document ;
#specialalternate{-O "" " " option} define a compiler option, as the
#italic{##option}# command. Then the user has the possibility to set some
parameters at the time of compilation ;
#specialalternate{-d} ignore invalid dependencies. Useful when the writer wants
to compile a part of a document, to check if this part is error free ;
#specialalternate{-A} ignore undefined automatic ideas. Sometimes, #PlanFacile#
adds some ideas to complete the document. This option avoids an error when an
automatic idea appears.
#end

#idea{grammar}BASIC GRAMAR#text#dep{description}{}{}
For a common use of #PlanFacile , it is not necessary to know all the language
of #PlanFacile . The goal of this section is to present the minimum necessary to
know how to write a document.
#end

#idea{dependency}Dependencies#text
In a document with ideas#dep{1}{idea}{#depref}{}# about the same subject, it is
quite natural that ideas need some of the others to be understood by the reader.
This relation is a dependency between ideas.
#§
If the writer puts a dependency in the text of idea A to the idea B, this means that
the reader will need to understand idea B before reading idea A in order to
understand this last idea.
#§
The syntax of a dependency, in its simplest form, is :
#specialalternate{##dep#{# reference #}#{##depref#}#{#}}
#§
However, the writer may need to add a positive number to specify the weighting of
the dependency if the organisation of the document is not the right one, even if
the writer has put the correct dependencies :
#specialalternate{##dep#{# pertinence #}#{# reference #}#{##depref#}#{#}}
#§
The #italic{reference}# must be one of the words used in the #italic{##idea}#
command.
#end

#idea{idea}Ideas#text
A document, as said#dep{description}{# after}{# before}, is a set of ideas the
author wants to communicate to the reader. One idea is a part of the document
that can be represented by a title. For example, if the writer sees that one
title does not accurately represent the text of the same idea, he should just
make sure that two concepts are not put in one #italic{##idea}# command... The
syntax#dep{1}{grammar}{#depref}{}# of an idea is quite simple :
#specialalternate{##idea#{# reference #}# title ##text}
#specialalternate{"" text}
#specialalternate{##end}
#§
Also, some ideas are dependent in one document, so they are linked by
dependencies#dep{dependency}{#depref}{}.
#end

#missing{1}{document}Document#text
#end

#idea{source}Complete source code#text
The ideas#dep{idea}{#depref}{}# are not sufficient to produce a document.
To complete a source code, the writer needs to learn a bit more about the
language#dep{1}{grammar}{#depref}{}# of #PlanFacile .
#end

#idea{seealso}SEE ALSO#text#dep{1}{manpage}{}{}#dep{description}{}{}
See also #italic{latex}(1), #italic{xpdf}(1), #italic{firefox}(1),
#italic{lynx}(1).
#end

#idea{contacts}CONTACTS#text#dep{seealso}{}{}
If you have a problem that you can not solve by reading the documentation, or if you
find a bug, you can send an email to @PROJECT_EMAIL@ and we will send a
reply as quickly as possible.
#end

#idea{standard}Standard#text
The first command of the source#dep{1}{source}{#depref}{}# can be
#italic{##standard}, that indicates to #PlanFacile# to load standard definitions,
to be able to produce a document in LaTeX or in HTML (more formats will be
present in the future).
#end

#idea{optionsmacros}"Options and macros"#text
When the writer uses the command #italic{##standard}#dep{standard}{#depref}{},
the compiler will be able to generate all formats. The writer must indicate his
choice with some commands#dep{1}{source}{#depref}{}.
#§
A choice, such as the output format, can be set with an option :
#specialalternate{##option#{# optionname #}}
#§
and a text parameter can be set with a macro, for a very specific use :
#specialalternate{##define#{# parametername #}#{# value #}}
#§
#end

#idea{automatic}"Automatic ideas"#text
Sometimes, #PlanFacile# adds an idea#dep{idea}{#depref}{}# to the
document#dep{1}{document}{#depref}{}. In that case, the writer can replace the
default text by an idea that suits the document. To do this, there are two
commands, which are like #italic{##idea}# :
#specialalternate{##missing#{# index #}#{# reference #}# title ##text}
#specialalternate{"" text}
#specialalternate{##end}
#§
The #italic{index}# parameter is given by #PlanFacile , in the default
message...
#§
In the same way :
#specialalternate{##generic#{# "parent reference" #}#{# reference #}# title ##text}
#specialalternate{"" text}
#specialalternate{##end}
#end

#idea{name}NAME#text#dep{1}{manpage}{}{}
@SOFTWARE_NAME@ \- A document compiler.
#end

