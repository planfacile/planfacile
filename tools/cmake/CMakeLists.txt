#
# This file is part of PlanFacile
#
# Copyright (C) 2014  Sylvain Plantefève
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licences/>.

#--------------
# CONFIGURATION
#--------------
set( FINDPACKAGE_TEMPLATE "${CMAKE_CURRENT_SOURCE_DIR}/FindPackage.cmake.in" )

string( REPLACE "FindPackage" "Find${PROJECT_NAME}" FINDPACKAGE_FILE ${FINDPACKAGE_TEMPLATE} )
string( REPLACE ".in" "" FINDPACKAGE_FILE ${FINDPACKAGE_FILE} )
string( REPLACE ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR} FINDPACKAGE_FILE ${FINDPACKAGE_FILE} )
configure_file( ${FINDPACKAGE_TEMPLATE} ${FINDPACKAGE_FILE} @ONLY )


#--------
# INSTALL
#--------
install( FILES ${FINDPACKAGE_FILE} DESTINATION "${DATA_PREFIX}/cmake/" )
